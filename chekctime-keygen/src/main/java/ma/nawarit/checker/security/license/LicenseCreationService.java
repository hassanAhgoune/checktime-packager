package ma.nawarit.checker.security.license;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.licensor.*;
import org.apache.commons.codec.binary.*;
import org.apache.commons.io.FileUtils;

public class LicenseCreationService {
	
	private byte[] licenseData;
	private String trns;
	
	public LicenseCreationService() {
		// TODO Auto-generated constructor stub
		LicenseCreatorProperties.setPrivateKeyDataProvider(new PrivateKeyProvider());
        LicenseCreatorProperties.setPrivateKeyPasswordProvider(new PrivatePasswordProvider());
        LicenseCreator.getInstance();
	}
	public String createLicense() {
		Calendar cal = new GregorianCalendar();
		cal.add(Calendar.MONTH, 3);
		License license = new License.Builder().
	                withProductKey("5565-1039-AF89-GGX7-TN31-14AL").
	                withHolder("ONP").
	                withGoodBeforeDate(cal.getTimeInMillis()).
	                withFeature("00-15-5D-7A-7E-11").
	                build();
	        
		 licenseData = LicenseCreator.getInstance().signAndSerializeLicense(license, "pupd".toCharArray());
		 trns = Base64.encodeBase64String(licenseData);
		 File fileLi = new File("ctm.li");
		 try {
//			FileUtils.writeByteArrayToFile(fileLi, licenseData);
			 FileUtils.writeStringToFile(fileLi, trns);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		 return trns;
	}
	
	public byte[] getLicenseData() {
		return licenseData;
	}
	public String getTrns() {
		return trns;
	}
	public static void main(String[] args) {
		LicenseCreationService license = new LicenseCreationService();
	}
}
