package ma.nawarit.checker.security.license;

import net.nicholaswilliams.java.licensing.encryption.*;
import net.nicholaswilliams.java.licensing.exception.*;
import java.security.KeyPair;
import static net.nicholaswilliams.java.licensing.encryption.RSAKeyPairGeneratorInterface.GeneratedClassDescriptor;
public class KeyGenerationExample {
    public static void main(String[] arguments) {
        RSAKeyPairGenerator generator = new RSAKeyPairGenerator();
        
        KeyPair keyPair;
        try {
            keyPair = generator.generateKeyPair();
        } catch(RSA2048NotSupportedException e) { return; }
        
        GeneratedClassDescriptor rkd = new GeneratedClassDescriptor().
            setPackageName("ma.nawarit.checker.security.license").setClassName("PrivateKeyProvider");
        
        GeneratedClassDescriptor ukd = new GeneratedClassDescriptor().
            setPackageName("ma.nawarit.checker.security.license").setClassName("PublicKeyProvider");
        
        GeneratedClassDescriptor pd1 = new GeneratedClassDescriptor().
            setPackageName("ma.nawarit.checker.security.license").setClassName("PrivatePasswordProvider");
        
        GeneratedClassDescriptor pd2 = new GeneratedClassDescriptor().
            setPackageName("ma.nawarit.checker.security.license").setClassName("PublicPasswordProvider");
        
        try {
            generator.saveKeyPairToProviders(keyPair, rkd, ukd, "prpd".toCharArray(), "pupd".toCharArray());
            generator.savePasswordToProvider("prpd".toCharArray(), pd1);
            generator.savePasswordToProvider("pupd".toCharArray(), pd2);
        } catch(AlgorithmNotSupportedException  e){ 
        	return; 
        } catch(InappropriateKeyException e) { 
        	return; 
        } catch(InappropriateKeySpecificationException e)
        { return; }
    }
}
