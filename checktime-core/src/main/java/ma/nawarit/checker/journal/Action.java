package ma.nawarit.checker.journal;

import java.io.Serializable;
import java.util.Date;
import ma.nawarit.checker.compagnie.User;

public class Action implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5166899761638515577L;
	
	private java.lang.String date;
	
	private java.lang.String name;
	
	private java.lang.String parameters;
	
	private java.lang.String result;
	
	private java.lang.String user;

	public java.lang.String getDate() {
		return date;
	}

	public void setDate(java.lang.String date) {
		this.date = date;
	}

	public java.lang.String getName() {
		return name;
	}

	public void setName(java.lang.String name) {
		this.name = name;
	}

	public java.lang.String getParameters() {
		return parameters;
	}

	public void setParameters(java.lang.String parameters) {
		this.parameters = parameters;
	}

	public java.lang.String getResult() {
		return result;
	}

	public void setResult(java.lang.String result) {
		this.result = result;
	}

	public java.lang.String getUser() {
		return user;
	}

	public void setUser(java.lang.String user) {
		this.user = user;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
