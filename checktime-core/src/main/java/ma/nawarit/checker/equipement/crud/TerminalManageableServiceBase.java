/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.equipement.crud;

import java.util.List;

public final class TerminalManageableServiceBase
    implements TerminalManageableService
{
    private ma.nawarit.checker.equipement.TerminalDao dao;

    public void setDao(ma.nawarit.checker.equipement.TerminalDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.equipement.TerminalDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.equipement.Terminal load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.equipement.crud.TerminalManageableService.delete(ma.nawarit.checker.equipement.Terminal entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    public List loadTerminalsBySite(int siteId) throws Exception{
    	return dao.loadTerminalsBySite(siteId);
    }
    public List loadTerminalsByCode(String Code) throws Exception {
    	return dao.loadTerminalsByCode(Code);
    }

}
