/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.equipement.crud;

public final class TerminalTypeManageableServiceBase
    implements TerminalTypeManageableService
{
    private ma.nawarit.checker.equipement.TerminalTypeDao dao;

    public void setDao(ma.nawarit.checker.equipement.TerminalTypeDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.equipement.TerminalTypeDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.equipement.TerminalType entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.equipement.TerminalType load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.equipement.TerminalType entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.equipement.TerminalType entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.equipement.crud.TerminalTypeManageableService.delete(ma.nawarit.checker.equipement.TerminalType entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
