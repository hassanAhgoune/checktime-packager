/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.equipement.crud;

import java.util.List;

public interface TerminalManageableService
{
    public void create(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception;

    public ma.nawarit.checker.equipement.Terminal load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception;

    public void delete(ma.nawarit.checker.equipement.Terminal entity)
        throws Exception;
    
    public List loadTerminalsBySite(int siteId) throws Exception;
    
    public List loadTerminalsByCode(String Code)throws Exception;
}
