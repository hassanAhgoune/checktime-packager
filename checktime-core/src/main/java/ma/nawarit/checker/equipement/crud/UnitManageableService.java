/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.equipement.crud;

public interface UnitManageableService
{
    public ma.nawarit.checker.equipement.Unit create(ma.nawarit.checker.equipement.Unit entity)
        throws Exception;

    public ma.nawarit.checker.equipement.Unit load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;

    public java.util.Map unitsByCode()
        throws Exception;


    public void update(ma.nawarit.checker.equipement.Unit entity)
        throws Exception;

    public void delete(ma.nawarit.checker.equipement.Unit entity)
        throws Exception;

}
