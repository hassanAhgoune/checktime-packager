/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.equipement;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.equipement.TerminalType</code>.
 * </p>
 *
 * @see ma.nawarit.checker.equipement.TerminalType
 */
public abstract class TerminalTypeDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.equipement.TerminalTypeDao
{

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.equipement.TerminalTypeImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.equipement.TerminalType)entity);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#load(int)
     */
    public ma.nawarit.checker.equipement.TerminalType load(int id)
    {
        return (ma.nawarit.checker.equipement.TerminalType)this.load(TRANSFORM_NONE, id);
    	
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.equipement.TerminalTypeImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(ma.nawarit.checker.equipement.TerminalType)
     */
    public ma.nawarit.checker.equipement.TerminalType create(ma.nawarit.checker.equipement.TerminalType terminalType)
    {
        return (ma.nawarit.checker.equipement.TerminalType)this.create(TRANSFORM_NONE, terminalType);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(int transform, ma.nawarit.checker.equipement.TerminalType)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.equipement.TerminalType terminalType)
    {
        if (terminalType == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.create - 'terminalType' can not be null");
        }
        this.getHibernateTemplate().save(terminalType);
        return this.transformEntity(transform, terminalType);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.equipement.TerminalType)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(java.lang.String, java.lang.String)
     */
    public ma.nawarit.checker.equipement.TerminalType create(
		java.lang.String libelle,
        java.lang.String name,
        java.lang.String packageTerm)
    {
        return (ma.nawarit.checker.equipement.TerminalType)this.create(TRANSFORM_NONE, libelle, name, packageTerm);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#create(int, java.lang.String, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String libelle,
        java.lang.String name,
        java.lang.String packageTerm)
    {
        ma.nawarit.checker.equipement.TerminalType entity = new ma.nawarit.checker.equipement.TerminalTypeImpl();
        entity.setLibelle(libelle);
        entity.setName(name);
        entity.setPackageTerm(packageTerm);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#update(ma.nawarit.checker.equipement.TerminalType)
     */
    public void update(ma.nawarit.checker.equipement.TerminalType terminalType)
    {
        if (terminalType == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.update - 'terminalType' can not be null");
        }
        this.getHibernateTemplate().merge(terminalType);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.equipement.TerminalType)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#remove(ma.nawarit.checker.equipement.TerminalType)
     */
    public void remove(ma.nawarit.checker.equipement.TerminalType terminalType)
    {
        if (terminalType == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.remove - 'terminalType' can not be null");
        }
        this.getHibernateTemplate().clear();
        this.getHibernateTemplate().delete(terminalType);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.equipement.TerminalType entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalTypeDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "TerminalType.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.equipement.TerminalTypeDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.equipement.TerminalTypeDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.equipement.TerminalType entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.equipement.TerminalType)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.equipement.TerminalTypeDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.equipement.TerminalType)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.equipement.TerminalTypeImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.equipement.TerminalTypeImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }



}