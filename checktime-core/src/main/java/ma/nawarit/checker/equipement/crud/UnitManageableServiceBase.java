/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.equipement.crud;

public final class UnitManageableServiceBase
    implements UnitManageableService
{
    private ma.nawarit.checker.equipement.UnitDao dao;

    public void setDao(ma.nawarit.checker.equipement.UnitDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.equipement.UnitDao getDao()
    {
        return this.dao;
    }

    public ma.nawarit.checker.equipement.Unit create(ma.nawarit.checker.equipement.Unit entity)
        throws Exception
    {

       return dao.create(entity);
    }

    
     public ma.nawarit.checker.equipement.Unit load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }

    public java.util.Map unitsByCode()
        throws Exception
    {
        return dao.unitsByCode();
    }


    public void update(ma.nawarit.checker.equipement.Unit entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.equipement.Unit entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.equipement.crud.UnitManageableService.delete(ma.nawarit.checker.equipement.Unit entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
