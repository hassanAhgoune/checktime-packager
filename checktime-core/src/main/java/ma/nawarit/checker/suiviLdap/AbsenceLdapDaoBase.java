/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.suiviLdap;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;
import ma.nawarit.checker.compagnie.UserDaoImpl;
import ma.nawarit.checker.compagnie.crud.UserLdapManageableService;
import ma.nawarit.checker.compagnie.crud.UserLdapManageableServiceBase;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableServiceBase;
import ma.nawarit.checker.compagnieLdap.UserLdapDao;
import ma.nawarit.checker.compagnieLdap.UserLdapDaoImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceDao;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.TypeAbsence;

import org.andromda.spring.CommonCriteria;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.suivi.Absence</code>.
 * </p>
 *
 * @see ma.nawarit.checker.suivi.Absence
 */
public abstract class AbsenceLdapDaoBase extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
		implements ma.nawarit.checker.suiviLdap.AbsenceLdapDao {
	private UserDao userDao = new UserDaoImpl();
	private UserLdapDao userLdapDao = new UserLdapDaoImpl();

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#load(int, int)
	 */
	public java.lang.Object load(final int transform, final int id) {
		final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.suivi.AbsenceImpl.class,
				new java.lang.Integer(id));
		return transformEntity(transform, (ma.nawarit.checker.suivi.Absence) entity);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#load(int)
	 */
	public ma.nawarit.checker.suivi.Absence load(int id) {
		Absence abs = (ma.nawarit.checker.suivi.Absence) this.load(TRANSFORM_NONE, id);

		try {
			abs.setCollaborateur(userDao.load(abs.getCollaborateur().getId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return abs;
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#loadAll()
	 */
	public java.util.Collection loadAll() {
		return this.loadAll(TRANSFORM_NONE);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#loadAll(int)
	 */
	public java.util.Collection loadAll(final int transform) {
		final java.util.Collection results = this.getHibernateTemplate()
				.loadAll(ma.nawarit.checker.suivi.AbsenceImpl.class);
		this.transformEntities(transform, results);
		return results;
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(ma.nawarit.checker.suivi.Absence)
	 */
	public ma.nawarit.checker.suivi.Absence create(ma.nawarit.checker.suivi.Absence absence) {
		return (ma.nawarit.checker.suivi.Absence) this.create(TRANSFORM_NONE, absence);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(int transform,
	 *      ma.nawarit.checker.suivi.Absence)
	 */
	public java.lang.Object create(final int transform, final ma.nawarit.checker.suivi.Absence absence) {
		if (absence == null) {
			throw new IllegalArgumentException("Absence.create - 'absence' can not be null");
		}
		this.getHibernateTemplate().save(absence);
		return this.transformEntity(transform, absence);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(java.util.Collection)
	 */
	public java.util.Collection create(final java.util.Collection entities) {
		return create(TRANSFORM_NONE, entities);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(int, java.util.Collection)
	 */
	public java.util.Collection create(final int transform, final java.util.Collection entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Absence.create - 'entities' can not be null");
		}
		this.getHibernateTemplate().execute(new org.springframework.orm.hibernate3.HibernateCallback() {
			public java.lang.Object doInHibernate(org.hibernate.Session session)
					throws org.hibernate.HibernateException {
				for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();) {
					create(transform, (ma.nawarit.checker.suivi.Absence) entityIterator.next());
				}
				return null;
			}
		});
		return entities;
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(java.util.Date, double,
	 *      java.util.Date, java.util.Date, java.util.Date)
	 */
	public ma.nawarit.checker.suivi.Absence create(java.util.Date dateReprise, double Duree, double heureDebut,
			double heureReprise, java.util.Date dateDebut, String statut) {
		return (ma.nawarit.checker.suivi.Absence) this.create(TRANSFORM_NONE, dateReprise, Duree, heureDebut,
				heureReprise, dateDebut, statut);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(int, java.util.Date, double,
	 *      java.util.Date, java.util.Date, java.util.Date)
	 */
	public java.lang.Object create(final int transform, java.util.Date dateReprise, double Duree, double heureDebut,
			double heureReprise, java.util.Date dateDebut, String statut) {
		ma.nawarit.checker.suivi.Absence entity = new ma.nawarit.checker.suivi.AbsenceImpl();
		entity.setDateReprise(dateReprise);
		entity.setDuree(Duree);
		entity.setDateDebut(dateDebut);
		entity.setStatut(statut);
		return this.create(transform, entity);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(ma.nawarit.checker.compagnie.User,
	 *      java.util.Date, java.util.Date, double, java.util.Date, java.util.Date,
	 *      ma.nawarit.checker.suivi.TypeAbsence)
	 */
	public ma.nawarit.checker.suivi.Absence create(ma.nawarit.checker.compagnie.User collaborateur,
			java.util.Date dateDebut, java.util.Date dateReprise, double Duree, double heureDebut, double heureReprise,
			String statut, ma.nawarit.checker.suivi.TypeAbsence typeAbsence) {
		return (ma.nawarit.checker.suivi.Absence) this.create(TRANSFORM_NONE, collaborateur, dateDebut, dateReprise,
				Duree, heureDebut, heureReprise, statut, typeAbsence);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#create(int,
	 *      ma.nawarit.checker.compagnie.User, java.util.Date, java.util.Date,
	 *      double, java.util.Date, java.util.Date,
	 *      ma.nawarit.checker.suivi.TypeAbsence)
	 */
	public java.lang.Object create(final int transform, ma.nawarit.checker.compagnie.User collaborateur,
			java.util.Date dateDebut, java.util.Date dateReprise, double Duree, double heureDebut, double heureReprise,
			String statut, ma.nawarit.checker.suivi.TypeAbsence typeAbsence) {
		ma.nawarit.checker.suivi.Absence entity = new ma.nawarit.checker.suivi.AbsenceImpl();
		entity.setCollaborateur(collaborateur);
		entity.setDateDebut(dateDebut);
		entity.setDateReprise(dateReprise);
		entity.setDuree(Duree);
		entity.setStatut(statut);
		entity.setTypeAbsence(typeAbsence);
		return this.create(transform, entity);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#update(ma.nawarit.checker.suivi.Absence)
	 */
	public void update(ma.nawarit.checker.suivi.Absence absence) {
		if (absence == null) {
			throw new IllegalArgumentException("Absence.update - 'absence' can not be null");
		}
		this.getHibernateTemplate().update(absence);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#update(java.util.Collection)
	 */
	public void update(final java.util.Collection entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Absence.update - 'entities' can not be null");
		}
		this.getHibernateTemplate().execute(new org.springframework.orm.hibernate3.HibernateCallback() {
			public java.lang.Object doInHibernate(org.hibernate.Session session)
					throws org.hibernate.HibernateException {
				for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();) {
					update((ma.nawarit.checker.suivi.Absence) entityIterator.next());
				}
				return null;
			}
		});
	}

	public double readCumulJ(CommonCriteria crit) {
		final Session session = getSession(false);

		try {
			String query = "SELECT a.id FROM ABSENCE a, USER u, TYPE_ABSENCE t"
					+ " WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK=t.ID";

			String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
			String strDate2 = Utils.convertDate2Str(crit.getDateFin(), "yyyy-MM-dd") + " 23:59";
			query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 + "' AND '" + strDate2 + "')";

			query += " AND (UPPER(t.LIBELLE) like UPPER('" + crit.getMotif() + "') )";

			SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();
			double some = 0;
			List result = null, resultFinal = null;
			if (myList != null && myList.size() >= 1) {
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();

				while (iter.hasNext()) {
					id = (Integer) iter.next();
					result.add(this.load(id.intValue()));
					// some+= (this.load(id.intValue())).getDuree();

				}

			}
			resultFinal = isBadgeCriteria(result, crit.getNumBadge());
			Iterator<Absence> it = resultFinal.iterator();
			while (it.hasNext()) {
				some += (it.next()).getDuree();
			}
			return some;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	public double readCumulSJ(CommonCriteria crit) {
		final Session session = getSession(false);

		try {
			String query = "SELECT a.id FROM ABSENCE a, USER u, TYPE_ABSENCE t"
					+ " WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK=t.ID";

			String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
			String strDate2 = Utils.convertDate2Str(crit.getDateFin(), "yyyy-MM-dd") + " 23:59";
			query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 + "' AND '" + strDate2 + "')";
			query += " AND (a.statut <> 'workflow_status_elapsed' )";
			query += " AND (UPPER(t.LIBELLE) like UPPER('" + crit.getMotif() + "') )";

			SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();
			double some = 0;
			List result = null, resultFinal = null;
			if (myList != null && myList.size() >= 1) {
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();

				while (iter.hasNext()) {
					id = (Integer) iter.next();
					result.add(this.load(id.intValue()));
					// some+= (this.load(id.intValue())).getDuree();

				}

			}
			resultFinal = isBadgeCriteria(result, crit.getNumBadge());
			Iterator<Absence> it = resultFinal.iterator();
			while (it.hasNext()) {
				some += (it.next()).getDuree();
			}
			return some;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#remove(ma.nawarit.checker.suivi.Absence)
	 */
	public void remove(ma.nawarit.checker.suivi.Absence absence) {
		if (absence == null) {
			throw new IllegalArgumentException("Absence.remove - 'absence' can not be null");
		}
		this.getHibernateTemplate().delete(absence);
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#remove(int)
	 */
	public void remove(int id) {
		ma.nawarit.checker.suivi.Absence entity = this.load(id);
		if (entity != null) {
			this.remove(entity);
		}
	}

	/**
	 * @see ma.nawarit.checker.suivi.AbsenceDao#remove(java.util.Collection)
	 */
	public void remove(java.util.Collection entities) {
		if (entities == null) {
			throw new IllegalArgumentException("Absence.remove - 'entities' can not be null");
		}
		this.getHibernateTemplate().deleteAll(entities);
	}

	/**
	 * Allows transformation of entities into value objects (or something else for
	 * that matter), when the <code>transform</code> flag is set to one of the
	 * constants defined in <code>ma.nawarit.checker.suivi.AbsenceDao</code>, please
	 * note that the {@link #TRANSFORM_NONE} constant denotes no transformation, so
	 * the entity itself will be returned.
	 *
	 * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
	 *
	 * @param transform
	 *            one of the constants declared in
	 *            {@link ma.nawarit.checker.suivi.AbsenceDao}
	 * @param entity
	 *            an entity that was found
	 * @return the transformed entity (i.e. new value object, etc)
	 * @see #transformEntities(int,java.util.Collection)
	 */
	protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.suivi.Absence entity) {
		java.lang.Object target = null;
		if (entity != null) {
			switch (transform) {
			case TRANSFORM_NONE: // fall-through
			default:
				target = entity;
			}
		}
		return target;
	}

	/**
	 * Transforms a collection of entities using the
	 * {@link #transformEntity(int,ma.nawarit.checker.suivi.Absence)} method. This
	 * method does not instantiate a new collection.
	 * <p/>
	 * This method is to be used internally only.
	 *
	 * @param transform
	 *            one of the constants declared in
	 *            <code>ma.nawarit.checker.suivi.AbsenceDao</code>
	 * @param entities
	 *            the collection of entities to transform
	 * @see #transformEntity(int,ma.nawarit.checker.suivi.Absence)
	 */
	protected void transformEntities(final int transform, final java.util.Collection entities) {
		switch (transform) {
		case TRANSFORM_NONE: // fall-through
		default:
			// do nothing;
		}
	}

	// public java.util.List read(java.util.Hashtable properties)
	// {
	// final Session session = getSession(false);
	//
	// try
	// {
	// final Criteria criteria =
	// session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
	//
	// Set e = properties.keySet();
	// Iterator it = e.iterator();
	// String tab[];
	// Criteria crit[];
	// int i=0;
	// while(it.hasNext()){
	// String tmp = (String)it.next();
	// tab=tmp.split("\\.");
	// i=0;
	// if(tab.length>1){
	// crit=new Criteria[tab.length-1];
	// crit[0]=criteria.createCriteria(tab[0]);
	// for(i=1;i<tab.length-1;i++)
	// crit[i]=crit[i-1].createCriteria(tab[i]);
	// i--;
	// crit[i].add( Expression.eq(tab[i+1], properties.get(tmp)));
	// }else
	// criteria.add( Expression.eq(tmp,properties.get(tmp)));
	// }
	// return criteria.list();
	//
	// }
	// catch (org.hibernate.HibernateException ex)
	// {
	// throw super.convertHibernateAccessException(ex);
	// }
	// }
	public java.util.List read(java.util.Hashtable properties) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			String tab[];
			Criteria crit[];
			int i = 0;
			while (it.hasNext()) {
				String tmp = (String) it.next();
				tab = tmp.split("\\.");
				i = 0;
				if (tab.length > 1) {
					crit = new Criteria[tab.length - 1];
					crit[0] = criteria.createCriteria(tab[0]);
					for (i = 1; i < tab.length - 1; i++)
						crit[i] = crit[i - 1].createCriteria(tab[i]);
					i--;
					crit[i].add(Expression.eq(tab[i + 1], properties.get(tmp)));
				} else
					criteria.add(Expression.eq(tmp, properties.get(tmp)));
			}
			Iterator<Absence> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			Absence abs;
			User use;
			Hashtable hash = new Hashtable();
			while (iter.hasNext()) {
				abs = iter.next();
				use = userDao.load(abs.getCollaborateur().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if (!listLdap.isEmpty() && listLdap != null) {
					abs.setCollaborateur(use);
					list.add(abs);
				}

			}
			return list;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		} catch (Exception e) {

		}
		return null;
	}

	public java.util.List readAll() {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
			List l = criteria.list();
			Iterator<Absence> iter = l.iterator();
			List list = new ArrayList();
			List listLdap;
			Absence abs;
			User use;
			Hashtable hash = new Hashtable();
			while (iter.hasNext()) {
				abs = iter.next();
				use = userDao.load(abs.getCollaborateur().getId());
				hash.put("user", use);

				listLdap = userLdapDao.readUser(hash);
				if (!listLdap.isEmpty() && listLdap != null) {
					abs.setCollaborateur(use);
					list.add(abs);
				}

			}
			return list;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		} catch (Exception e) {

		}
		return null;
	}

	public java.util.List findBySH(int id) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class)

					.createCriteria("collaborateur").createAlias("supH", "sup")
					.add(Expression.eq("sup.id", Integer.valueOf(id)));
			Absence abs;
			User use;
			Hashtable hash = new Hashtable();
			List list = new ArrayList();
			List listLdap;
			Iterator<Absence> it = criteria.list().iterator();
			while (it.hasNext()) {
				abs = it.next();
				use = userDao.load(abs.getCollaborateur().getId());
				hash.put("user", use);

				listLdap = userLdapDao.readUser(hash);
				if (!listLdap.isEmpty() && listLdap != null) {
					abs.setCollaborateur(use);
					list.add(abs);
				}
			}
			return list;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}

	}

	public java.util.List readByCriteria(Date d1, Date d2) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
			criteria.add(Restrictions.between("dateDebut", d1, d2));
			return criteria.list();
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	public java.util.List readByCriteria(CommonCriteria crit) {
		final Session session = getSession(false);

		try {
			String query = "SELECT a.id FROM ABSENCE a, USER u, TYPE_ABSENCE t"
					+ " WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK=t.ID";
			if (crit.getDateDebut() != null && crit.getDateFin() != null) {
				String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
				String strDate2 = Utils.convertDate2Str(crit.getDateFin(), "yyyy-MM-dd") + " 23:59";
				query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 + "' AND '" + strDate2 + "')";
			}
			if (crit.getNoeudId() != null && !"".equals(crit.getNoeudId().trim())) {
				query += " AND (u.NOEUD_FK= " + crit.getNoeudId() + " )";
			}
			if (crit.getStatut() != null && !"".equals(crit.getStatut().trim())) {
				query += " AND (a.STATUT= " + crit.getStatut() + " )";
			}
			if (crit.getMotif() != null && !"".equals(crit.getMotif().trim())) {
				query += " AND (UPPER(t.LIBELLE) like UPPER('" + crit.getMotif() + "%') )";
			}
			if (crit.isProcessActive())
				query += " AND (a.ID_PROCESS>0)";
			query += " ORDER BY id desc";
			SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();

			List result = null;
			if (myList != null && myList.size() >= 1) {
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer) iter.next();
					result.add(this.load(id.intValue()));

				}

			}

			return isMatriculeCriteria(result, crit.getMatriculeInf(), crit.getMatriculeSup());
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	List isMatriculeCriteria(List list, String matInf, String matSup) {
		Iterator<Absence> iter = list.iterator();
		Absence abs;
		User use;
		List listTemp = new ArrayList();
		while (iter.hasNext()) {
			abs = iter.next();
			use = userLdapDao.loadLdap(abs.getCollaborateur().getId());
			if (use != null)
				if ((use.getMatricule()).compareTo(matInf) >= 0 && (use.getMatricule()).compareTo(matSup) <= 0) {
					listTemp.add(abs);
				}
		}
		return listTemp;

	}

	List isBadgeCriteria(List list, String badge) {
		Iterator<Absence> iter = list.iterator();
		Absence abs;
		User use;
		List listTemp = new ArrayList();
		while (iter.hasNext()) {
			abs = iter.next();
			use = userLdapDao.loadLdap(abs.getCollaborateur().getId());
			if (use != null)
				if (use.getBadge().equals(badge)) {
					listTemp.add(abs);
				}
		}
		return listTemp;

	}

	public java.util.List readByCriteria(String ded, User user) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
			criteria.add(Restrictions.like("collaborateur", user)).createCriteria("typeAbsence")
					.add(Restrictions.like("deduir", ded));
			Absence abs;
			User use;
			Hashtable hash = new Hashtable();
			List list = new ArrayList();
			List listLdap;
			Iterator<Absence> it = criteria.list().iterator();
			while (it.hasNext()) {
				abs = it.next();
				use = userDao.load(abs.getCollaborateur().getId());
				hash.put("user", use);

				listLdap = userLdapDao.readUser(hash);
				if (!listLdap.isEmpty() && listLdap != null) {
					abs.setCollaborateur(use);
					list.add(abs);
				}
			}
			return list;
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserLdapDao getUserLdapDao() {
		return userLdapDao;
	}

	public void setUserLdapDao(UserLdapDao userLdapDao) {
		this.userLdapDao = userLdapDao;
	}

	// public UserDao getUserDao() {
	// return userDao;
	// }
	//
	// public void setUserDao(UserDao userDao) {
	// this.userDao = userDao;
	// }
}