/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Aug 06 08:22:22 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.suiviLdap;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.evenement.PliColiReception;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;
import ma.nawarit.checker.compagnie.UserDaoImpl;
import ma.nawarit.checker.compagnieLdap.UserLdapDao;
import ma.nawarit.checker.compagnieLdap.UserLdapDaoImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Retard;

import org.andromda.spring.CommonCriteria;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.suivi.Retard</code>.
 * </p>
 *
 * @see ma.nawarit.checker.suivi.Retard
 */
public abstract class RetardLdapDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.suiviLdap.RetardLdapDao
{
	private UserDao userDao = new UserDaoImpl();
	private UserLdapDao userLdapDao = new UserLdapDaoImpl();
    public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserLdapDao getUserLdapDao() {
		return userLdapDao;
	}

	public void setUserLdapDao(UserLdapDao userLdapDao) {
		this.userLdapDao = userLdapDao;
	}

	/**
     * @see ma.nawarit.checker.suivi.RetardDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.suivi.RetardImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.suivi.Retard)entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#load(int)
     */
    public ma.nawarit.checker.suivi.Retard load(int id)
    {
        Retard ret = (Retard)this.load(TRANSFORM_NONE, id);
    	try {
    		ret.setUser(userDao.load(ret.getUser().getId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return ret;
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.suivi.RetardImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(ma.nawarit.checker.suivi.Retard)
     */
    public ma.nawarit.checker.suivi.Retard create(ma.nawarit.checker.suivi.Retard retard)
    {
        return (ma.nawarit.checker.suivi.Retard)this.create(TRANSFORM_NONE, retard);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(int transform, ma.nawarit.checker.suivi.Retard)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.suivi.Retard retard)
    {
        if (retard == null)
        {
            throw new IllegalArgumentException(
                "Retard.create - 'retard' can not be null");
        }
        this.getHibernateTemplate().save(retard);
        return this.transformEntity(transform, retard);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(int, java.util.Collection)
     */
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Retard.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.suivi.Retard)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(java.lang.String, java.util.Date, java.lang.String)
     */
    public ma.nawarit.checker.suivi.Retard create(
        java.lang.String matricule,
        java.util.Date date,
        java.lang.String description)
    {
        return (ma.nawarit.checker.suivi.Retard)this.create(TRANSFORM_NONE, matricule, date, description);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(int, java.lang.String, java.util.Date, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String matricule,
        java.util.Date date,
        java.lang.String description)
    {
        ma.nawarit.checker.suivi.Retard entity = new ma.nawarit.checker.suivi.RetardImpl();
        entity.setMatricule(matricule);
        entity.setDate(date);
        entity.setDescription(description);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(java.util.Date, java.lang.String, java.lang.String, ma.nawarit.checker.suivi.User)
     */
    public ma.nawarit.checker.suivi.Retard create(
        java.util.Date date,
        java.lang.String description,
        java.lang.String matricule,
        ma.nawarit.checker.compagnie.UserImpl user)
    {
        return (ma.nawarit.checker.suivi.Retard)this.create(TRANSFORM_NONE, date, description, matricule, user);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#create(int, java.util.Date, java.lang.String, java.lang.String, ma.nawarit.checker.suivi.User)
     */
    public java.lang.Object create(
        final int transform,
        java.util.Date date,
        java.lang.String description,
        java.lang.String matricule,
        ma.nawarit.checker.compagnie.UserImpl user)
    {
        ma.nawarit.checker.suivi.Retard entity = new ma.nawarit.checker.suivi.RetardImpl();
        entity.setDate(date);
        entity.setDescription(description);
        entity.setMatricule(matricule);
        entity.setUser(user);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#update(ma.nawarit.checker.suivi.Retard)
     */
    public void update(ma.nawarit.checker.suivi.Retard retard)
    {
        if (retard == null)
        {
            throw new IllegalArgumentException(
                "Retard.update - 'retard' can not be null");
        }
        this.getHibernateTemplate().update(retard);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#update(java.util.Collection)
     */
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Retard.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.suivi.Retard)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#remove(ma.nawarit.checker.suivi.Retard)
     */
    public void remove(ma.nawarit.checker.suivi.Retard retard)
    {
        if (retard == null)
        {
            throw new IllegalArgumentException(
                "Retard.remove - 'retard' can not be null");
        }
        this.getHibernateTemplate().delete(retard);
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.suivi.Retard entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.suivi.RetardDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Retard.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.suivi.RetardDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.suivi.RetardDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.suivi.Retard entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.suivi.Retard)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.suivi.RetardDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.suivi.Retard)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.RetardImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			Iterator<Retard> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			Retard ret ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				ret = iter.next();
				use=userDao.load(ret.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					ret.setUser(use);
					list.add(ret);
				}
				
			}
			 return list;
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.RetardImpl.class);

            Iterator<Retard> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			Retard ret ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				ret = iter.next();
				use=userDao.load(ret.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					ret.setUser(use);
					list.add(ret);
				}
				
			}
			 return list;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }

    public java.util.List readByCriteria(Date d1, Date d2)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.RetardImpl.class);
            criteria.add( Restrictions.between("date", d1, d2) );
            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    
    public java.util.List readByCriteria(CommonCriteria crit)
    {
        final Session session = getSession(false);

        try
        {
        	String query = "SELECT r.ID FROM RETARD r, USER u "
        				  +" WHERE r.USER_FK=u.ID ";
        	if (crit.getDateDebut() != null && crit.getDateFin()!=null){
            	String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
            	String strDate2 = Utils.convertDate2Str(crit.getDateFin(),"yyyy-MM-dd") +" 23:59";		 
        		query += " AND (r.DATE BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')";       		
        	}
        	
        	if(crit.getNoeudId() != null && !"".equals(crit.getNoeudId().trim())){
        		query += " AND (u.NOEUD_FK= " + crit.getNoeudId()+ " )";
        	}

        	SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();

			List result =null;
			if(myList != null && myList.size() >= 1){
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer)iter.next();
					result.add(this.load(id.intValue()));
					
				}
				
			}
			
			return isMatriculeCriteria(result,crit.getMatriculeInf(),crit.getMatriculeSup());
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    List isMatriculeCriteria(List list,String matInf,String matSup){
    	Iterator<Retard> iter = list.iterator();
    	Retard ret;
    	User use;
    	List listTemp = new ArrayList();
    	while(iter.hasNext()){
    		ret = iter.next();
    		use = userLdapDao.loadLdap(ret.getUser().getId());
    		if(use!=null)
	    		if((use.getMatricule()).compareTo(matInf)>=0 &&(use.getMatricule()).compareTo(matSup)<=0){
	    			listTemp.add(ret);
	    		}
    	}
    	return listTemp;
    		
    }

}