package ma.nawarit.checker.common;

public interface DayDataBase {

	public void rollupsEngine();
	
	public java.util.Date getDate();
	
	public double getCol1();

	public double getCol2ht();
	
	public double getCol3();

	public double getCol4();
	
	public double getCol5();
	
	public double getCol10();
	
	public double getCol11();
	
	public double getCol12();
	
	public double getTcol8();
	
	public double getTcol9();

}
