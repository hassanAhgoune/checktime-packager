package ma.nawarit.checker.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementCaxImpl;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.Retard;

public class DayData extends Day implements DayDataBase, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type= "";
	private Calendar dateCal;
	private int sm;
	private List<String> inOuts = new ArrayList<String>(); 
	private String exceptNode;
	private boolean withAno;
	
	private double col1; // HN
	private double col2; // HNT
	private double col2cop; // HN copag
	private double col2ht; // HT (entre mvts)
	private double col2bis; // JT
	private Integer[] col2prm = {0,0,0}; // prime
	private double col3; // R Tol
	private double col4; // R NTol
	private double col5; // Inter
	private double col6; // Sortie avant
	private List<Conge> col7 = new ArrayList<Conge>();
	private List<Absence> col8 = new ArrayList<Absence>();
	private List<Absence> col9 = new ArrayList<Absence>();
	private double tcol7; // nbre jours Conge
	private double tcol8; // Abs P
	private double tcol9; // Abs NP
	private double col10; // HS 125
	private double col11; // HS 150
	private double col12; // HS 200
	private double col13;
	private double col14;
	private double col15;
	private double col16;
	private double col17;
	private double col18;
	private double col19;
	private double col20;
	//private int col14;
	private boolean coloredCol2 = true;
	private boolean coloredCol3 = true;
	private boolean coloredCol4 = true;
	private boolean coloredCol5 = true;
	private boolean coloredCol6 = true;
	private boolean coloredCol7 = true;
	private boolean coloredCol8 = true;
	private boolean coloredCol9 = true;
	private boolean coloredCol10 = true;
	private boolean coloredCol11 = true;
	private boolean coloredCol12 = true;
	private boolean coloredCol13 = true;
	private boolean coloredCol14 = true;
	private boolean coloredCol15 = true;
	private boolean coloredCol16 = true;
	private boolean coloredCol17 = true;
	private boolean coloredCol18 = true;
	private boolean coloredCol19 = true;
	private boolean coloredCol20 = true;
	private String colColor2 = "#ffff";
	private String colColor3 = "#ffff";
	private String colColor4 = "#ffff";
	private String colColor5 = "#ffff";
	private String colColor6 = "#ffff";
	private String colColor7 = "#ffff";
	private String colColor8 = "#ffff";
	private String colColor9 = "#ffff";
	private String colColor10 = "#ffff";
	private String colColor11 = "#ffff";
	private String colColor12 = "#ffff";
	private String colColor13 = "#ffff";
	private String colColor14 = "#ffff";
	private String colColor15 = "#ffff";
	private String colColor16 = "#ffff";
	private String colColor17 = "#ffff";
	private String colColor18 = "#ffff";
	private String colColor19 = "#ffff";
	private String colColor20 = "#ffff";
	private String diffHsupp25 = null;
	private String diffHsupp50 = null;
	private String diffHsupp100 = null;
	
	public DayData() {
		super();
	}
	
	public DayData(Date date, String rptHNuit,
			Map<Planning, List<Date>> smartHor) {
		super(date, rptHNuit, smartHor);
		// TODO Auto-generated constructor stub
	}

	public DayData(DayData d) {
		super();
//		this.date = d.getDate();
		this.sm = d.getSm();
		this.col1 = d.getCol1();
		this.col2 = d.getCol2();
		this.col2ht = d.getCol2ht();
		this.col2bis = d.getCol2bis();
		this.col2prm[0] = d.getCol2prm()[0];
		this.col2prm[1] = d.getCol2prm()[1];
		this.col3 = d.getCol3();
		this.col4 = d.getCol4();
		this.col5 = d.getCol5();
		this.col6 = d.getCol6();
		this.col7 = d.getCol7();
		this.col8 = d.getCol8();
		this.col9 = d.getCol9();
		this.tcol7 = d.getTcol7();
		this.tcol8 = d.getTcol8();
		this.tcol9 = d.getTcol9();
		this.col10 = d.getCol10();
		this.col11 = d.getCol11();
		this.col12 = d.getCol12();
		this.col13 = d.getCol13();
		this.col14 = d.getCol14();
		this.col15 = d.getCol15();
		this.col16 = d.getCol16();
		this.col17 = d.getCol17();
	}

	public void rollupsEngine() {

		double count1 = 0;
		double count2 = 0;
		
		// Calcule de retard
		if (!this.getRetardsEffectues().isEmpty())
			for (Retard r: this.getRetardsEffectues()) {
				if (r.getRetardTolere() > 0)
					count1 += r.getRetardTolere();
				else if (r.getRetardNonTolere() > 0)
					count2 += r.getRetardNonTolere();
			}
		this.col3 = Utils.convertMin2Hour(count1);
		this.col4 = Utils.convertMin2Hour(count2);
		
		// Calcule d'interruption
		count1 = 0;
		if (!this.getInterruptionsEffectues().isEmpty())
			for (Interruption i: this.getInterruptionsEffectues())
				count1 = Utils.additionHeures(count1, i.getDuree());
		this.col5 = count1;
		
		// Calcule des absences
		count1 = 0;
		count2 = 0;
		if (!this.getAbsencesEffectues().isEmpty())
			for (Absence a: this.getAbsencesEffectues()) {
				System.out.println("motif :" + a.getTypeAbsence().getLibelle());
				if (a.getTypeAbsence().isDeduir())
					count1 += a.getDuree();
				else
					count2 += a.getDuree();
			}
		this.tcol8 = count1;
		this.tcol9 = count2;

		// Calcule des conges
		if (this.getNature()!= null && !this.getNature().equals("conge") && this.getMvtsAllDay().isEmpty())
			this.tcol7 = 1;
		
		// Calcule des Heures supplimentaires
		count1 = 0;
		count2 = 0;
		int count3 = 0;
		if (!this.getHrSuppsEffectues().isEmpty())
			for (HeurSupp h: this.getHrSuppsEffectues())
				if (h.getAnnotation().getTaux() == 100)
					count1 += h.getDuree();
				else if (h.getAnnotation().getTaux() == 25)
					count2 += h.getDuree();
				else if (h.getAnnotation().getTaux() == 50)
					count3 += h.getDuree();
		this.col10 = count1;
		this.col11 = count2;
		this.col12 = count3;
					
		//calcule des heures normales
		count1 = this.col1 - this.tcol8;
		if (count1 <= 0)
			this.col1 = 0;
			
	}

	public String getColColor3() {
		return colColor3;
	}


	public void setColColor3(String colColor3) {
		this.colColor3 = colColor3;
	}


	public String getColColor4() {
		return colColor4;
	}


	public void setColColor4(String colColor4) {
		this.colColor4 = colColor4;
	}


	public String getColColor5() {
		return colColor5;
	}


	public void setColColor5(String colColor5) {
		this.colColor5 = colColor5;
	}


	public String getColColor6() {
		return colColor6;
	}


	public void setColColor6(String colColor6) {
		this.colColor6 = colColor6;
	}


	public String getColColor7() {
		return colColor7;
	}


	public void setColColor7(String colColor7) {
		this.colColor7 = colColor7;
	}


	public String getColColor8() {
		return colColor8;
	}


	public void setColColor8(String colColor8) {
		this.colColor8 = colColor8;
	}


	public String getColColor9() {
		return colColor9;
	}


	public void setColColor9(String colColor9) {
		this.colColor9 = colColor9;
	}


	public String getColColor10() {
		return colColor10;
	}


	public void setColColor10(String colColor10) {
		this.colColor10 = colColor10;
	}


	public String getColColor11() {
		return colColor11;
	}


	public void setColColor11(String colColor11) {
		this.colColor11 = colColor11;
	}


	public String getColColor12() {
		return colColor12;
	}


	public void setColColor12(String colColor12) {
		this.colColor12 = colColor12;
	}


	public String getColColor13() {
		return colColor13;
	}


	public void setColColor13(String colColor13) {
		this.colColor13 = colColor13;
	}

	public String getColColor14() {
		return colColor14;
	}
	public void setColColor14(String colColor14) {
		this.colColor14 = colColor14;
	}
	public String getColColor15() {
		return colColor15;
	}
	public void setColColor15(String colColor15) {
		this.colColor15 = colColor15;
	}
	public String getColColor16() {
		return colColor16;
	}
	public void setColColor16(String colColor16) {
		this.colColor16 = colColor16;
	}	
	public String getColColor17() {
		return colColor17;
	}
	public void setColColor17(String colColor17) {
		this.colColor17 = colColor17;
	}	
	public String getColColor18() {
		return colColor18;
	}
	public void setColColor18(String colColor18) {
		this.colColor18 = colColor18;
	}
	public String getColColor19() {
		return colColor19;
	}
	public void setColColor19(String colColor19) {
		this.colColor19 = colColor19;
	}
	public String getColColor20() {
		return colColor20;
	}
	public void setColColor20(String colColor20) {
		this.colColor20 = colColor20;
	}
	public boolean isCorrect(){
		return coloredCol3 && 
			   coloredCol4 &&
			   coloredCol5 &&
			   coloredCol6 &&
			   coloredCol7 &&
			   coloredCol9 &&
			   coloredCol9 &&
			   coloredCol10 &&
			   coloredCol11 &&
			   coloredCol12 &&
			   coloredCol13;
	}
	
	/**
	 * HN 
	 */
	public double getCol1() {
		return getP_tmcontract();
	}
	public void setCol1(double col1) {
		this.col1 = col1;
		setP_tmcontract(col1);
	}
	/**
	 * HNT
	 */
	public double getCol2() {
		return col2;
	}
	public void setCol2(double col2) {
		this.col2 = col2;
	}
	/**
	 * HN copag
	 * @return
	 */
	public double getCol2cop() {
		if (getTmworked() > getP_tmcontract())
			return getP_tmcontract();
		else
			return getTmworked();
	}
	public void setCol2cop(double col2cop) {
		this.col2cop = col2cop;
	}
	/**
	 * HT (entre mvts)
	 */
	public double getCol2ht() {
		return this.getTmworked();
	}
	public void setCol2ht(double col2ht) {
		this.col2ht = col2ht;
		this.setTmworked(col2ht);
	}
	/**
	 * R Tol
	 * @return
	 */
	public double getCol3() {
		return col3;
	}
	public void setCol3(double col3) {
		this.col3 = col3;
	}
	/**
	 * R NTol
	 * @return
	 */
	public double getCol4() {
		return col4;
	}
	public void setCol4(double col4) {
		this.col4 = col4;
	}
	public double getCol5() {
		return col5;
	}
	public void setCol5(double col5) {
		this.col5 = col5;
	}
	public double getCol6() {
		return col6;
	}
	public void setCol6(double col6) {
		this.col6 = col6;
	}
	/**
	 * HS 125
	 * @return
	 */
	public double getCol10() {
		return col10;
	}
	public void setCol10(double col10) {
		this.col10 = col10;
	}
	/**
	 * HS 150
	 * @return
	 */
	public double getCol11() {
		return col11;
	}
	public void setCol11(double col11) {
		this.col11 = col11;
	}
	/**
	 * HS 200
	 * @return
	 */
	public double getCol12() {
		return col12;
	}
	public void setCol12(double col12) {
		this.col12 = col12;
	}
	public double getCol13() {
		return col13;
	}
	public void setCol13(double col13) {
		this.col13 = col13;
	}
	public int getSm() {
		return sm;
	}
	public void setSm(int sm) {
		this.sm = sm;
	}
	public List<Conge> getCol7() {
		return col7;
	}
	public void setCol7(List<Conge> col7) {
		this.col7 = col7;
	}
	public List<Absence> getCol8() {
		return col8;
	}
	public void setCol8(List<Absence> col8) {
		this.col8 = col8;
	}
	public List<Absence> getCol9() {
		return col9;
	}
	public void setCol9(List<Absence> col9) {
		this.col9 = col9;
	}
	public double getTcol7() {
		return tcol7;
	}
	public void setTcol7(double tcol7) {
		this.tcol7 = tcol7;
	}
	/**
	 * Abs P
	 * @return
	 */
	public double getTcol8() {
		return tcol8;
	}
	public void setTcol8(double tcol8) {
		this.tcol8 = tcol8;
	}
	/**
	 * Abs NP
	 * @return
	 */
	public double getTcol9() {
		return tcol9;
	}
	public void setTcol9(double tcol9) {
		this.tcol9 = tcol9;
	}
	public String getExceptNode() {
		return exceptNode;
	}
	public void setExceptNode(String exceptNode) {
		this.exceptNode = exceptNode;
	}
	public boolean isWithAno() {
		return withAno;
	}
	public void setWithAno(boolean withAno) {
		this.withAno = withAno;
	}
	public boolean isColoredCol3() {
		return coloredCol3;
	}
	public void setColoredCol3(boolean coloredCol3) {
		this.setColColor3(coloredCol3? "#f0b7c5":"#ffff");
		this.coloredCol3 = coloredCol3;
	}
	public boolean isColoredCol4() {
		return coloredCol4;
	}
	public void setColoredCol4(boolean coloredCol4) {
		this.setColColor4(coloredCol4? "#f0b7c5":"#ffff");
		this.coloredCol4 = coloredCol4;
	}
	public boolean isColoredCol5() {
		return coloredCol5;
	}
	public void setColoredCol5(boolean coloredCol5) {
		this.setColColor5(coloredCol5? "#f0b7c5":"#ffff");
		this.coloredCol5 = coloredCol5;
	}
	public boolean isColoredCol6() {
		return coloredCol6;
	}
	public void setColoredCol6(boolean coloredCol6) {
		this.setColColor6(coloredCol6? "#f0b7c5":"#ffff");
		this.coloredCol6 = coloredCol6;
	}
	public boolean isColoredCol7() {
		return coloredCol7;
	}
	public void setColoredCol7(boolean coloredCol7) {
		this.setColColor7(coloredCol7? "#f0b7c5":"#ffff");
		this.coloredCol7 = coloredCol7;
	}
	public boolean isColoredCol8() {
		return coloredCol8;
	}
	public void setColoredCol8(boolean coloredCol8) {
		this.setColColor8(coloredCol8? "#f0b7c5":"#ffff");
		this.coloredCol8 = coloredCol8;
	}
	public boolean isColoredCol9() {
		return coloredCol9;
	}
	public void setColoredCol9(boolean coloredCol9) {
		this.setColColor9(coloredCol9? "#f0b7c5":"#ffff");
		this.coloredCol9 = coloredCol9;
	}
	public boolean isColoredCol10() {
		return coloredCol10;
	}
	public void setColoredCol10(boolean coloredCol10) {
		this.setColColor10(coloredCol10? "#f0b7c5":"#ffff");
		this.coloredCol10 = coloredCol10;
	}
	public boolean isColoredCol11() {
		return coloredCol11;
	}
	public void setColoredCol11(boolean coloredCol11) {
		this.setColColor11(coloredCol11? "#f0b7c5":"#ffff");
		this.coloredCol11 = coloredCol11;
	}
	public boolean isColoredCol12() {
		return coloredCol12;
	}
	public void setColoredCol12(boolean coloredCol12) {
		this.setColColor12(coloredCol12? "#f0b7c5":"#ffff");
		this.coloredCol12 = coloredCol12;
	}
	public boolean isColoredCol13() {
		return coloredCol13;
	}
	public void setColoredCol13(boolean coloredCol13) {
		this.setColColor13(coloredCol13? "#f0b7c5":"#ffff");
		this.coloredCol13 = coloredCol13;
	}
	public boolean isColoredCol2() {
		return coloredCol2;
	}
	public void setColoredCol2(boolean coloredCol2) {
		this.setColColor2(coloredCol2? "#f0b7c5":"#ffff");
		this.coloredCol2 = coloredCol2;
	}
	public String getColColor2() {
		return colColor2;
	}
	public void setColColor2(String colColor2) {
		this.colColor2 = colColor2;
	}
	
	public boolean isColoredCol14() {
		return coloredCol14;
	}
	public void setColoredCol14(boolean coloredCol14) {
		this.setColColor14(coloredCol14? "#f0b7c5":"#ffff");
		this.coloredCol14 = coloredCol14;
	}
	public boolean isColoredCol15() {
		return coloredCol15;
	}
	public void setColoredCol15(boolean coloredCol15) {
		this.setColColor14(coloredCol15? "#f0b7c5":"#ffff");
		this.coloredCol15 = coloredCol15;
	}
	public boolean isColoredCol16() {
		return coloredCol16;
	}
	public void setColoredCol16(boolean coloredCol16) {
		this.setColColor16(coloredCol16? "#f0b7c5":"#ffff");
		this.coloredCol16 = coloredCol16;
	}	
	public boolean isColoredCol17() {
		return coloredCol17;
	}
	public void setColoredCol17(boolean coloredCol17) {
		this.setColColor17(coloredCol17? "#f0b7c5":"#ffff");
		this.coloredCol17 = coloredCol17;
	}
	
	public boolean isColoredCol18() {
		return coloredCol18;
	}
	public void setColoredCol18(boolean coloredCol18) {
		this.setColColor18(coloredCol18? "#f0b7c5":"#ffff");
		this.coloredCol18 = coloredCol18;
	}
	public boolean isColoredCol19() {
		return coloredCol19;
	}
	public void setColoredCol19(boolean coloredCol19) {
		this.setColColor19(coloredCol19? "#f0b7c5":"#ffff");
		this.coloredCol19 = coloredCol19;
	}
	public boolean isColoredCol20() {
		return coloredCol20;
	}
	public void setColoredCol20(boolean coloredCol20) {
		this.setColColor20(coloredCol20? "#f0b7c5":"#ffff");
		this.coloredCol20 = coloredCol20;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	/**
	 * // JT
	 * @return
	 */
	public double getCol2bis() {
		if (this.getTmworked() > 0)
			col2bis = 1;
		return col2bis;
	}
	public void setCol2bis(double col2bis) {
		this.col2bis = col2bis;
	}
	public Integer[] getCol2prm() {
		return col2prm;
	}
	public void setCol2prm(Integer[] col2prm) {
		this.col2prm = col2prm;
	}
//	public int getCol14() {
//		return col14;
//	}
//	public void setCol14(int col14) {
//		this.col14 = col14;
//	}
	
	public double getCol14() {
		return col14;
	}
	public void setCol14(double col14) {
		this.col14 = col14;
	}
	public double getCol15() {
		return col15;
	}
	public void setCol15(double col15) {
		this.col15 = col15;
	}
	public double getCol16() {
		return col16;
	}
	public void setCol16(double col16) {
		this.col16 = col16;
	}
	public double getCol17() {
		return col17;
	}
	public void setCol17(double col17) {
		this.col17 = col17;
	}
	public double getCol18() {
		return col18;
	}
	public void setCol18(double col18) {
		this.col18 = col18;
	}
	public double getCol19() {
		return col19;
	}
	public void setCol19(double col19) {
		this.col19 = col19;
	}
	public double getCol20() {
		return col20;
	}
	public void setCol20(double col20) {
		this.col20 = col20;
	}
	public List<String> getInOuts() {
		return inOuts;
	}
	public void setInOuts(List<String> inOuts) {
		this.inOuts = inOuts;
	}
	public Calendar getDateCal() {
		return dateCal;
	}
	public void setDateCal(Calendar dateCal) {
		this.dateCal = dateCal;
	}
	public String getDiffHsupp25() {
		return diffHsupp25;
	}
	public void setDiffHsupp25(String diffHsupp25) {
		this.diffHsupp25 = diffHsupp25;
	}
	public String getDiffHsupp50() {
		return diffHsupp50;
	}
	public void setDiffHsupp50(String diffHsupp50) {
		this.diffHsupp50 = diffHsupp50;
	}
	public String getDiffHsupp100() {
		return diffHsupp100;
	}
	public void setDiffHsupp100(String diffHsupp100) {
		this.diffHsupp100 = diffHsupp100;
	}
	
}
