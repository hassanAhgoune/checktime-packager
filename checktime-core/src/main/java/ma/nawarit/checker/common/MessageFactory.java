package ma.nawarit.checker.common;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;



public class MessageFactory {	

	static ResourceBundle bundle = ResourceBundle.getBundle("engine-settings");
	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String getMessage(String key) {
		return bundle.getString(key);
	}
	public static double getTimeElapsed(double startTime)
	{
		Date d = new Date();
		return (d.getMinutes() - startTime);
	}
	public static String getMessage(String key, Object arg1) {
		return getMessage(key, new Object[] {arg1});
	}
	public static String getMessage(String key, Object arg1, Object arg2) {
		return getMessage(key, new Object[] {arg1, arg2});
	}
	public static String getMessage(String key, Object[] args) {
		if (args == null || args.length == 0) {
			return bundle.getString(key);
		}
		MessageFormat fmt = new MessageFormat(bundle.getString(key));
		
		return fmt.format(args);
		
	}
}
