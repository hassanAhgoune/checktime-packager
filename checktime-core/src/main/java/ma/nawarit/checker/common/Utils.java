package ma.nawarit.checker.common;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


public class Utils {
	
	/**
     * Retourner la partie decimal d'un double
     * @param d
     * @return
     */
	 public static int decimal (double d) {
		// on cr�e un DecimalFormat pour formater le double en chaine :
		DecimalFormat df = new DecimalFormat();
		df.setGroupingUsed(false); // Pas de regroupement dans la partie enti�re
		df.setMinimumFractionDigits(2); // Au minimum 1 d�cimale
		df.setMaximumFractionDigits(2); // Au maximum 340 d�cimales (valeur max. pour les doubles / voir la doc)

		// On formate le double en chaine
		String str = df.format(d);
		// On r�cup�re le caract�re s�parateur entre la partie enti�re et d�cimale :
		char separator = df.getDecimalFormatSymbols().getDecimalSeparator();
		// On ne r�cup�re que la partie d�cimale :
		str = str.substring( str.indexOf(separator) + 1 );
		// Que l'on transforme en int :
		return Integer.parseInt(str);
	}
	 
	 /**
     * Retourner la valeur un parametre donne d'un mouvement donn�
     * @param mvmnt
     * @param param
     * @return
     */
//    public static String getValueOfParam(Mouvement mvmnt, String param){
//    	String value = null;
//    	for (Iterator iter = mvmnt.getMvmntParams().iterator(); iter.hasNext();) {
//			MvmntParams mvtpms = (MvmntParams) iter.next();
//			if(mvtpms.getName().equalsIgnoreCase(param)) 
//				value = mvtpms.getValue();    	
//		}
//    	return value;
//    }
    
//    public static Date getTimeOfMvmnt(Mouvement mvmnt){
//    	Date d = new Date();
//    	d.setDate(mvmnt.getDate().getDate());
//		String hrMvmnt = getValueOfParam(mvmnt, "heure");
//		String s = hrMvmnt.substring(0,2);	
//		d.setHours(Integer.parseInt(s));
//		s = hrMvmnt.substring(3, 5);
//		d.setMinutes(Integer.parseInt(s));
//		return d;
//    }
    
    public static String getHourDateFormat(Date d){
    	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    	return dateFormat.format(d);
    }
    
    public static Double getHourDoubleFormat(Date d){
    	SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
    	return new Double( dateFormat.format(d).replace(':', '.'));
    }
    
    public static Date getCompleteDate(Date d, Double h1) {
    	Calendar cal1 = new GregorianCalendar();
		cal1.setTime(d);
		cal1.set(Calendar.HOUR_OF_DAY, h1.intValue());
		cal1.set(Calendar.MINUTE, Utils.decimal(h1));
		return cal1.getTime();
    }
    
    /**
     * Retourner la difference entre les dates en terme d'heure et minute
     * @param d1
     * @param d2
     * @return
     */
    public static int[] differenceDates(Date d1, Date d2, double taux){
    	
    	Calendar cal = Calendar.getInstance();
    	long diff;
    	if (d1.after(d2)) {
    		diff = d1.getTime() - d2.getTime();
    	} else {
    		diff = d2.getTime() - d1.getTime();
    	}
    	Double Diff = diff * taux; 
    	cal.setTimeInMillis(Diff.longValue());
    	String s = getHourDateFormat(cal.getTime());
    	int[] result = {Integer.parseInt(s.substring(0, 2)), Integer.parseInt(s.substring(3, 5))};
    	return result;
    }
	 
	 public static Calendar getMondayOfCurrentWeek (){
		Calendar c = new GregorianCalendar ();
		c.setTime (new java.util.Date ());
		
		int year = c.get (Calendar.YEAR);
		int week = c.get (Calendar.WEEK_OF_YEAR);

		c = new GregorianCalendar ();
		c.set (Calendar.YEAR, year);
		c.set (Calendar.WEEK_OF_YEAR, week);
		c.set (Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

		return c;
	 }
	 
	 public static Calendar getFirstDayOfCurrentMonth (){
		Calendar c = new GregorianCalendar ();
		c.setTime (new java.util.Date ());
		
		int year = c.get (Calendar.YEAR);
		int month = c.get (Calendar.MONTH);

		c = new GregorianCalendar ();
		c.set (Calendar.YEAR, year);
		c.set (Calendar.MONTH, month);
		c.set (Calendar.DAY_OF_MONTH, 1);
		c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

		return c;
	 }
	 
	 public static int differenceDates(Calendar d1, Calendar d2) {
    	Calendar cal = Calendar.getInstance();
    	long diff;
    	if (d1.after(d2)) {
    		diff = d1.getTimeInMillis() - d2.getTimeInMillis();
    	} else {
    		diff = d2.getTimeInMillis() - d1.getTimeInMillis();
    	}
    	cal.setTimeInMillis(diff);
    	String s = toStrDate(cal.getTime(), "dd");
    	return Integer.parseInt(s);
    	
    }
	 
	 public static double differenceHeures(Double h1, Double h2) {
    	Calendar cal = Calendar.getInstance();
    	long diff;
    	Calendar d1 = new GregorianCalendar();
    	d1.set(Calendar.HOUR_OF_DAY, h1.intValue());
    	d1.set(Calendar.MINUTE, decimal(h1));
    	Calendar d2 = new GregorianCalendar();
    	d2.set(Calendar.HOUR_OF_DAY, h2.intValue());
    	d2.set(Calendar.MINUTE, decimal(h2));
    	if (d1.after(d2)) {
    		diff = d1.getTimeInMillis() - d2.getTimeInMillis();
    	} else {
    		diff = d2.getTimeInMillis() - d1.getTimeInMillis();
    	}
    	cal.setTimeInMillis(diff);
    	String s = getHourDateFormat(cal.getTime());
    	return new Double(s.substring(0, 2) + "." + s.substring(3, 5));
    	
    }
	
	 public static double additionHeures(Double h1, Double h2) {
    	Calendar cal = Calendar.getInstance();
    	long diff;
    	Calendar d1 = new GregorianCalendar();
    	d1.set(Calendar.HOUR_OF_DAY, h1.intValue());
    	d1.set(Calendar.MINUTE, decimal(h1));
    	Calendar d2 = new GregorianCalendar();
    	d2.set(Calendar.HOUR_OF_DAY, h2.intValue());
    	d2.set(Calendar.MINUTE, decimal(h2));
    	diff = d1.getTimeInMillis() + d2.getTimeInMillis();
    	cal.setTimeInMillis(diff);
    	String s = getHourDateFormat(cal.getTime());
    	return new Double(s.substring(0, 2) + "." + s.substring(3, 5));	    	
    }
	 
	 public static String toStrDate(Date date, String pattern){
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		return sdf.format(date);		
	}
	 
	 public static boolean isWkd(Date d) {
	    	Calendar cal = new GregorianCalendar();
	    	cal.setTime(d);
	    	return ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || 
					   (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) ? true :false;
	    }
	 
	 public static void addToSum(DayData sum, DayData d) {
			sum.setCol1(additionHeures(sum.getCol1(), d.getCol1()));
			sum.setCol2(additionHeures(sum.getCol2(), d.getCol2()));
			sum.setCol2cop(additionHeures(sum.getCol2cop(), d.getCol2cop()));
			sum.setCol2ht(additionHeures(sum.getCol2ht(), d.getCol2ht()));
			sum.setCol2bis(sum.getCol2bis() + d.getCol2bis());
			sum.getCol2prm()[0] += d.getCol2prm()[0];
			sum.getCol2prm()[1] += d.getCol2prm()[1];
			sum.getCol2prm()[2] += d.getCol2prm()[2];
			sum.setCol3(additionHeures(sum.getCol3(), d.getCol3()));
			sum.setCol4(additionHeures(sum.getCol4(), d.getCol4()));
			sum.setCol5(additionHeures(sum.getCol5(), d.getCol5()));
			sum.setCol6(additionHeures(sum.getCol6(), d.getCol6()));
			sum.setTcol7(sum.getTcol7()+ d.getTcol7());
			sum.setTcol8(additionHeures(sum.getTcol8(), d.getTcol8()));
			sum.setTcol9(additionHeures(sum.getTcol9(), d.getTcol9()));
			sum.setCol10(additionHeures(sum.getCol10(), d.getCol10()));
			sum.setCol11(additionHeures(sum.getCol11(), d.getCol11()));
			sum.setCol12(additionHeures(sum.getCol12(), d.getCol12()));
			sum.setCol13(additionHeures(sum.getCol13(), d.getCol13()));	
			sum.setCol14(additionHeures(sum.getCol14(), d.getCol14()));
			sum.setCol15(additionHeures(sum.getCol15(), d.getCol15()));
			sum.setCol16(additionHeures(sum.getCol16(), d.getCol16()));
			sum.setCol17(additionHeures(sum.getCol17(), d.getCol17()));
			sum.setCol18(additionHeures(sum.getCol18(), d.getCol18()));
			sum.setCol19(additionHeures(sum.getCol19(), d.getCol19()));
			sum.setCol20(additionHeures(sum.getCol20(), d.getCol20()));

		}

}
