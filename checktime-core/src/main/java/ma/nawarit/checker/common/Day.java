package ma.nawarit.checker.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.configuration.DymHoraire;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.configuration.Jour;
import ma.nawarit.checker.configuration.Period;
import ma.nawarit.checker.configuration.PlageHoraire;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningCycl;
import ma.nawarit.checker.configuration.PlanningHebdo;
import ma.nawarit.checker.configuration.SmplHoraire;
import ma.nawarit.checker.core.common.Plage;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.suivi.*;

import org.springframework.beans.BeanUtils;



public class Day implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5362402443376423624L;

	/**
	 * 
	 */

	private java.util.Date date;
	
	private Planning planning;
	
	private Horaire horaire;
	
	private List<Plage> hrSupps = new ArrayList<Plage>();	
	
	private List<Plage> hrFixes = new ArrayList<Plage>();
	
	private List<Mouvement> mouvements = new ArrayList<Mouvement>();
	private List<Mouvement> mvtIns = new ArrayList<Mouvement>();
	private List<Mouvement> mvtOuts = new ArrayList<Mouvement>();
	
	private List<Mouvement> mvtsAllDay = new ArrayList<Mouvement>();
	
	private boolean exceptDay;
	
	private boolean hrSuppDay;
	
	private boolean jrFerieHrSuppDay;
	
	private boolean compensedDay;
	
	private boolean recuperedDay;
	
	private boolean AbsCompensedDay;
	
	private boolean AbsRecuperedDay;
	
	private boolean jrRepos;
	
	private boolean jrAbsence;
	
	private boolean horaireNuit;
	
	private boolean flexible;
	
	private boolean sansPoi;
	
	private String rptHNuit = "";
	
	private Object nature;
	
	private String libelle = "";
	
	private boolean jrDeMoins = false;

	private static final String ANNOT1 = "annot1";	

	private final String HORAIRE_LIBRE ="horaireLibre";
	
	private final String SANS_POI_COMPTEUR ="sansPointage";
	
	private final String OUT_SANS_POI_COMPTEUR ="sortieSansPointage";
	
	private Double p_tmcontract = 8.0;
	
	private Double txDayWorked;
	
	private Double txHalfDayWorked;
	
	private Double tmworked;
	
	private int smOffset;
	
	private List<Absence> legalAbs = new ArrayList<Absence>();	

	private Object exceptP;
	
	private Map<Planning, List<Date>> smartHor = new HashMap<Planning, List<Date>>();
	
	private boolean enMasse = false;
	
	private boolean horContinu = false;
	
	private boolean payed = false;
	
	private boolean cumulTrait = true;
	private boolean liveState = false;
	private boolean active = true;
	
	private Collection<Absence> absencesEffectues = new ArrayList<Absence>();
	private Collection<Conge> congesEffectues = new ArrayList<Conge>();

	private Collection<Retard> retardsEffectues = new ArrayList<Retard>();
	private Collection<Interruption> interruptionsEffectues = new ArrayList<Interruption>();
	private Collection<HeurSupp> hrSuppsEffectues = new ArrayList<HeurSupp>();
	
	public void cleanDay() {
		this.planning = null;
		this.horaire = null;
		this.hrSupps = new ArrayList<Plage>();
		this.hrFixes = new ArrayList<Plage>();
		this.mouvements = new ArrayList<Mouvement>();
		this.mvtsAllDay = new ArrayList<Mouvement>();
		this.exceptDay = false;
		this.hrSuppDay = false;
		this.compensedDay = false;
		this.recuperedDay = false;
		AbsCompensedDay = false;
		AbsRecuperedDay = false;
		this.jrRepos = false;
		this.jrAbsence = false;
		this.horaireNuit = false;
		this.flexible = false;
		this.sansPoi = false;
		this.nature = null;
		this.libelle = "";
		this.jrDeMoins = false;
		this.p_tmcontract = 8.0;
		this.txDayWorked = 0.0;
		this.txHalfDayWorked = 0.0;
		this.tmworked = 0.0;
		this.smOffset = 0;
		this.legalAbs = new ArrayList<Absence>();
		this.exceptP = null;
		//########### ?????? TEST AJOUTE PAR KHALID
		//this.smartHor = new HashMap<Planning, List<Plage>>();
		this.enMasse = false;
		this.horContinu = false;
		this.payed = false;
		this.cumulTrait = true;
		this.liveState = false;
	}
	
	public Day() {
		// TODO Auto-generated constructor stub
	}

	public Day(Date date, String rptHNuit, Map<Planning, List<Date>> smartHor) {
		super();
		this.date = new Date(date.getTime());
		this.rptHNuit = rptHNuit;
		if (smartHor != null && !smartHor.isEmpty()) 
			this.smartHor = smartHor;
	}



	public Map<Planning, List<Date>> getSmartHor() {
		return smartHor;
	}

	public void setSmartHor(Map<Planning, List<Date>> smartHor) {
		this.smartHor = smartHor;
	}

	public Double getP_tmcontract() {
		return p_tmcontract;
	}

	public void setP_tmcontract(Double p_tmcontract) {
		this.p_tmcontract = p_tmcontract;
	}

	public java.util.Date getDate() {
		return date;
	}

	public void setDate(java.util.Date date) {
		this.date = date;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		Calendar laDate = new GregorianCalendar ();
		laDate.setTime(this.date);
		laDate.set(Calendar.HOUR_OF_DAY, 0);
		laDate.set(Calendar.MINUTE, 0);
		laDate.set(Calendar.SECOND, 0);
		laDate.set(Calendar.MILLISECOND, 0);
		if(planning!=null){
			if (planning.getType().equalsIgnoreCase("Hebdomadaire")){
				int lejr;
				if (laDate.get(Calendar.DAY_OF_WEEK)== 0)
					lejr = 7;
				else
					lejr = laDate.get(Calendar.DAY_OF_WEEK);					
	    		PlanningHebdo plHebdo = (PlanningHebdo)planning;
				List<Jour> jrs = (List)plHebdo.getJours();
				//Jour jour = jrs.get(lejr - 1); 
				this.setHoraire(jrs.get(lejr - 1).getHoraire());			
			} else if (planning.getType().equalsIgnoreCase("Cyclique")) { 
				// cas de planning cyclique
				try {
					PlanningCycl plCycl = (PlanningCycl)planning;		
		    		Calendar date1 = new GregorianCalendar();
		    		if (laDate.getTime().before(plCycl.getDateDebut())) 
		    			return;
		    		date1.setTime(plCycl.getDateDebut());
		    		date1.set(Calendar.HOUR_OF_DAY, 0);
		    		date1.set(Calendar.MINUTE, 0);
		    		date1.set(Calendar.SECOND, 0);
		    		date1.set(Calendar.MILLISECOND, 0);
		    		
		    		while (laDate.compareTo(date1) >= 0) {
						date1.add(Calendar.DAY_OF_MONTH, plCycl.getPeriodicite());
					}
		    		date1.add(Calendar.DAY_OF_MONTH, -plCycl.getPeriodicite());
		    		for (Iterator it = plCycl.getPeriods().iterator(); it
							.hasNext();) {
						Period p = (Period) it.next();
						date1.add(Calendar.DAY_OF_MONTH, p.getNbreJr());
						if (laDate.compareTo(date1) < 0) {
							this.setHoraire(p.getHoraire());					
							break;
						}
					}
				}catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
			}
		}
		this.planning = planning;
	}

	public Horaire getHoraire() {
		return horaire;
	}

	public void setHoraire(Horaire horaire) {
		horaire = Utils.deproxy(horaire, Horaire.class);
		hrSupps = new ArrayList<Plage>();
		hrFixes = new ArrayList<Plage>();
		if(horaire.getType().equals("Repos")){
			this.libelle = "repos";
			this.jrRepos = true;
			this.p_tmcontract = 0.0;
		} else if(horaire.getType().equals("Repos En Masse")){
			this.libelle = "repos";
			this.jrRepos = true;
			this.p_tmcontract = 0.0;
			this.enMasse = true;
		} else if (horaire.getType().equalsIgnoreCase("Horaire Simple")) {			
			SmplHoraire smplHor = (SmplHoraire)horaire;
			if (smplHor.getTmContract() > 0)
				this.p_tmcontract = smplHor.getTmContract();
			if (smplHor.getTauxFull() > 0)
				this.txDayWorked = smplHor.getTauxFull();
			if (smplHor.getTauxHalf() > 0)
				this.txHalfDayWorked = smplHor.getTauxHalf();
			buildPlagesSimpleHor(smplHor);	
			if (!this.libelle.equals("Z_EN_MASSE"))
				this.libelle = "simple";
			if(!this.hrFixes.isEmpty()) {
				Plage theFirst = this.hrFixes.get(0);
				if (theFirst.isPlageNuit())
					this.setHoraireNuit(true);
				else
					this.setHoraireNuit(false);
			}
		} else if (horaire.getType().equalsIgnoreCase("Horaire Dynamique")){
			this.libelle = "dynamique";
    		DymHoraire dymHoraire = (DymHoraire)horaire;
    		dymHoraire = Utils.deproxy(dymHoraire, DymHoraire.class);
    		if (dymHoraire.getSmplHoraires() != null && !dymHoraire.getSmplHoraires().isEmpty()) {
	    		for (Iterator<SmplHoraire> it = dymHoraire.getSmplHoraires().iterator(); it.hasNext();) {
	    			SmplHoraire smplHor = (SmplHoraire) Utils.deproxy(it.next(), SmplHoraire.class);	  
	    			hrSupps = new ArrayList<Plage>();
	    			hrFixes = new ArrayList<Plage>();
					buildPlagesSimpleHor(smplHor);
					smplHor.setMark(this.convenablePlages());
					getHrSupps().clear();
					getHrFixes().clear();	
				}
	    		hrSupps = new ArrayList<Plage>();
	    		hrFixes = new ArrayList<Plage>();
	    		dymHoraire.sortHorSmpls();
	    		int size = dymHoraire.getSmplHoraires().size();
	    		SmplHoraire h1 = (SmplHoraire)Utils.deproxy(((List)dymHoraire.getSmplHoraires()).get(size - 1),SmplHoraire.class);
	    		SmplHoraire h2 = (SmplHoraire)Utils.deproxy(((List)dymHoraire.getSmplHoraires()).get(size - 2),SmplHoraire.class);
	    		if (h1.getMark() == h2.getMark()) {
	    			buildPlagesSimpleHor(h2);
					horaire = h2;
					this.p_tmcontract = h2.getTmContract();
					this.txDayWorked = h2.getTauxFull();
					this.txHalfDayWorked = h2.getTauxHalf();
	    		}else {
	    			buildPlagesSimpleHor(h1);
	    			horaire = h1;
	    			this.p_tmcontract = h1.getTmContract();
	    			this.txDayWorked = h1.getTauxFull();
	    			this.txHalfDayWorked = h1.getTauxHalf();
	    		}
	    		if(!this.hrFixes.isEmpty()) {
	    			Plage theFirst = this.hrFixes.get(0);
					if (theFirst.isPlageNuit())
						this.setHoraireNuit(true);
					else
						this.setHoraireNuit(false);
				}
    		}
    	}		
		this.horaire = horaire;
	}
    
    public void calculTpConract() {
    	Calendar debut = null;
    	long d = 0;
    	Calendar cal = Calendar.getInstance();
    	mvtIns = new ArrayList<Mouvement>();
    	mvtOuts = new ArrayList<Mouvement>();
    	for (Mouvement mvt : mouvements) {
    		if (debut == null) {
    			if (mvt.getType() == null || mvt.getType().trim().equals("")) 
    				mvt.setType("IN");
    			if (mvt.getType().trim().equals("IN")) {
					debut = new GregorianCalendar();
					debut.setTime(mvt.getDate());
					mvtIns.add(mvt);
				}
    		} else {
    			if (mvt.getType() == null || mvt.getType().trim().equals(""))
    				mvt.setType("OUT");
    			if (mvt.getType().trim().equals("OUT")) {
					d = d + Utils.differenceDates(debut.getTime(), mvt.getDate());
					mvtOuts.add(mvt);
					debut = null;
				}
    		}
		}
    	cal.setTimeInMillis(d);
    	this.tmworked = Utils.getHourDoubleFormat(cal.getTime());
    	if(this.tmworked == null)
    		this.tmworked = 0.0;
    	if(this.tmworked != 0)
    		setJrAbsence(false);
    	else if (mouvements.isEmpty())
    		setJrAbsence(true);
    }
    
    public void saveBornes() {
    	Calendar debut = null;
    	mvtIns = new ArrayList<Mouvement>();
    	mvtOuts = new ArrayList<Mouvement>();
    	for (Mouvement mvt : mouvements) {
    		if (debut == null) {
    			if (mvt.getType() == null || mvt.getType().trim().equals("")) 
    				mvt.setType("IN");
    			if (mvt.getType().trim().equals("IN")) {
					debut = new GregorianCalendar();
					debut.setTime(mvt.getDate());
					mvtIns.add(mvt);
				} else if (mvt.getType().trim().equals("OUT")) {
//					mvtIns.add(new MouvementImpl());
					mvtOuts.add(mvt);
				}
    		} else {
    			if (mvt.getType() == null || mvt.getType().trim().equals(""))
    				mvt.setType("OUT");
    			if (mvt.getType().trim().equals("OUT")) {
					mvtOuts.add(mvt);
					debut = null;
				} else if (mvt.getType().trim().equals("IN")) {
//					mvtOuts.add(new MouvementImpl());
					mvtIns.add(mvt);
				}
    		}
		}
    }
    
    public void calculBornesTpConract() {
    	Calendar debut = null;
    	long d = 0;
    	Calendar cal = Calendar.getInstance();
    	for (Mouvement mvt : mouvements) {
    		if (debut == null) {
    			if (mvt.getType() == null || mvt.getType().trim().equals("")) 
    				mvt.setType("IN");
    			if (mvt.getType().trim().equals("IN")) {
					debut = new GregorianCalendar();
					debut.setTime(mvt.getDate());
				}
    		} else {
    			if (mvt.getType() == null || mvt.getType().trim().equals(""))
    				mvt.setType("OUT");
    			if (mvt.getType().trim().equals("OUT")) {
					d = d + Utils.differenceDates(debut.getTime(), mvt.getDate());
					debut = null;
				}
    		}
		}
    	cal.setTimeInMillis(d);
    	this.tmworked = Utils.getHourDoubleFormat(cal.getTime());
    	if(this.tmworked == null)
    		this.tmworked = 0.0;
    	if(this.tmworked != 0)
    		setJrAbsence(false);
    	else if (mouvements.isEmpty())
    		setJrAbsence(true);
    }

    public Day getNextDay(List<Day> days) {
    	if (days.indexOf(this) <= days.size() - 2)
    		return days.get(days.indexOf(this) +1);
    	else 
    		return null;
    }
    
    public Day getLastDay(List<Day> days) {
    	if (days.indexOf(this) > 0)
    		return days.get(days.indexOf(this) - 1);
    	else 
    		return null;
    }
    
	public List<Plage> getHrSupps() {
		return hrSupps;
	}

	public void setHrSupps(List<Plage> hrSupps) {
		this.hrSupps = hrSupps;
	}

	public Double getTxDayWorked() {
		return txDayWorked;
	}

	public void setTxDayWorked(Double txDayWorked) {
		this.txDayWorked = txDayWorked;
	}

	public Double getTxHalfDayWorked() {
		return txHalfDayWorked;
	}

	public void setTxHalfDayWorked(Double txHalfDayWorked) {
		this.txHalfDayWorked = txHalfDayWorked;
	}

	public List<Plage> getHrFixes() {
		return hrFixes;
	}

	public void setHrFixes(List<Plage> hrFixes) {
		this.hrFixes = hrFixes;
	}

	public boolean isExceptDay() {
		return exceptDay;
	}

	public void setExceptDay(boolean exceptDay) {
		this.exceptDay = exceptDay;
	}

	public boolean isHrSuppDay() {
		return hrSuppDay;
	}

	public void setHrSuppDay(boolean hrSuppDay) {
		this.hrSuppDay = hrSuppDay;
	}

	public boolean isJrFerieHrSuppDay() {
		return jrFerieHrSuppDay;
	}

	public void setJrFerieHrSuppDay(boolean jrFerieHrSuppDay) {
		this.jrFerieHrSuppDay = jrFerieHrSuppDay;
	}

	public boolean isJrRepos() {
		return jrRepos;
	}

	public void setJrRepos(boolean jrRepos) {
		this.jrRepos = jrRepos;
	}

    public Object getNature() {
		return nature;
	}

	public void setNature(Object nature) {
		this.nature = nature;
	}

	public boolean isJrDeMoins() {
		return jrDeMoins;
	}

	public void setJrDeMoins(boolean jrDeMoins) {
		this.jrDeMoins = jrDeMoins;
	}

	public void buildPlagesSimpleHor(SmplHoraire smplHor){
		int size = smplHor.getPlageHoraires().size();
		int count = 0;
		PlageHoraire plageHor;
		for (Iterator itera = smplHor.getPlageHoraires().iterator(); itera
				.hasNext();) {
			plageHor = (PlageHoraire) itera.next();
			if (plageHor.getAnnotation().getCode().equals("Continu"))
				horContinu = true;
			if (plageHor.getAnnotation().getCode().equals("massivite")) {
				enMasse = true;
				if (!smartHor.isEmpty() && smartHor.containsKey(planning)) {
					List<Date> inOut = smartHor.get(planning);
					Plage plage;
					if (!inOut.isEmpty() && cumulTrait)
						p_tmcontract = 0.0;
					for (int i = 0; i < inOut.size(); i++) {
						if(i%2 == 1) {
							plage = new Plage(inOut.get(i - 1), inOut.get(i), plageHor);
							this.hrFixes.add(plage);
							if (cumulTrait)
								p_tmcontract = Utils.additionHeures(p_tmcontract, Utils.differenceHeures(Utils.getHourDoubleFormat(inOut.get(i - 1)),Utils.getHourDoubleFormat(inOut.get(i))));
						}
					}
					if (inOut.size()%2 == 1 && liveState) {
						plage = new Plage(inOut.get(inOut.size() - 1), null, plageHor);
						this.hrFixes.add(plage);
					}
				}
				break;
			}
			Plage plage = new Plage(this.getDate(), plageHor);
			if (plageHor.getDebut() > plageHor.getFin()) {
				if (rptHNuit.equals("no"))
					plage.getDebut().add(Calendar.DAY_OF_MONTH, -1);
		    	else if (rptHNuit.equals("yes"))
					plage.getFin().add(Calendar.DAY_OF_MONTH, 1);
	    		plage.setPlageNuit(true);
			} else
	    		plage.setPlageNuit(false);
			String typeAnnotation = plage.getPlageHor().getAnnotation().getTypeAnnotation().getLibelle();
			
			if (typeAnnotation.equalsIgnoreCase(MessageFactory.getMessage(ANNOT1))
					|| typeAnnotation.equalsIgnoreCase(MessageFactory.getMessage(HORAIRE_LIBRE))) 
				hrFixes.add(plage);
			else if (typeAnnotation.equalsIgnoreCase(MessageFactory.getMessage(SANS_POI_COMPTEUR))) {
				count++;
				plage.setEntree(true);
				plage.setSortie(true);
				hrFixes.add(plage);				
			} else if (typeAnnotation.equalsIgnoreCase(MessageFactory.getMessage(OUT_SANS_POI_COMPTEUR))) {
				plage.setEntree(false);
				plage.setSortie(true);
				hrFixes.add(plage);				
			} else
				hrSupps.add(plage);
		}
		if (count == size)
			this.setSansPoi(true);
		sortPlages(hrFixes);
		sortPlages(hrSupps);
    }
    
	public String getRptHNuit() {
		return rptHNuit;
	}

	public void setRptHNuit(String rptHNuit) {
		this.rptHNuit = rptHNuit;
	}

	public void addTmsToPlages(List<Timing> tmgs, ProfilMetier pr, Noeud po) {
		Calendar c1 = new GregorianCalendar();
		Calendar c2 = new GregorianCalendar();
		c1.setTime(date);
		c2.setTime(date);
		if(pr != null)
			exceptP = pr;
		else if (po != null)
			exceptP = po;
			
		for (Timing t : tmgs) {
			c1.add(Calendar.HOUR_OF_DAY, new Double(t.getHreDebut()).intValue());
			c1.add(Calendar.MINUTE, Utils.decimal(t.getHreDebut()));
//			c2.add(Calendar.HOUR_OF_DAY, new Double(t.getHreFin()).intValue());
//			c2.add(Calendar.MINUTE, Utils.decimal(t.getHreFin()));
			for(Plage plg: hrFixes)
				if (c1.compareTo(plg.getDebut()) >= 0 && c1.compareTo(plg.getFin()) <= 0) 
					plg.getTmgs().add(t);			
		}
		if (hrFixes != null)
			for(Plage plg: hrFixes)
				if (plg.getTmgs() != null && !plg.getTmgs().isEmpty()) 
					sortTimings(plg.getTmgs());
	}
	
	public void sortTimings(List<Timing> timings){    	
    	Comparator comparator = new Comparator() // compare deux mouvement suivant le temps
        {
    		public int compare(Object o1, Object o2)
            {
    			Timing t1 = (Timing) o1;
    			Timing t2 = (Timing) o2;
				
				return t1.getHreDebut() <= t2.getHreDebut() ? 1:0;
			}
        };
    	Collections.sort((List)timings, comparator);
    }
	
    public void sortPlages(List<Plage> plages){    	
    	Comparator comparator = new Comparator() // compare deux mouvement suivant le temps
        {
    		public int compare(Object o1, Object o2)
            {
                Plage plage1 = (Plage) o1;
            	Plage plage2 = (Plage) o2;
				
				return plage1.getDebut().compareTo(plage2.getDebut());
			}
        };
    	Collections.sort((List)plages, comparator);
    } 
	
	
	public boolean isJrAbsence() {
		return this.sansPoi? false:this.mouvements.isEmpty();
	}

	public void setJrAbsence(boolean jrAbsence) {
		this.jrAbsence = jrAbsence;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public boolean isHoraireNuit() {
		if (hrFixes != null)
			for (Plage p: hrFixes)
				if (p.isPlageNuit()) {
					horaireNuit = true;
					break;
				}
		return horaireNuit;
	}

	public void setHoraireNuit(boolean horaireNuit) {
		this.horaireNuit = horaireNuit;
	}

	public List<Mouvement> getMouvements() {
		return mouvements;
	}

	public void setMouvements(List<Mouvement> mouvements) {
		this.mouvements = mouvements;
	} 

	public List<Mouvement> getMvtsAllDay() {
		return mvtsAllDay;
	}

	public void setMvtsAllDay(List<Mouvement> mvtsAllDay) {
		this.mvtsAllDay = mvtsAllDay;
	}

	/**
     * tester si le planning simple en parametre est convenablement adapt�
     * au mvmnts de pointage
     */
    public int convenablePlages() {
    	List<Mouvement> list = new ArrayList<Mouvement>(this.getMouvements());
		int nivConv = 0;
		if (hrFixes != null)
			for (Plage plage : getHrFixes()) {
				// projeter la liste des mvts sur la plage horaire
				plage.projectMvts(list);
				if (!plage.isEntree())
					nivConv--;
				else
					nivConv++;
				if (!plage.isSortie())
					nivConv--;
				else
					nivConv++;
			}
		return nivConv;	    	
    }

	public Double getTmworked() {
		return (tmworked!=null)?tmworked:0;
	}
	
	public int getMinworked() {
		return (tmworked!=null)?Utils.doubleHour2Min(tmworked):0;
	}

	public void setTmworked(Double tmworked) {
		this.tmworked = tmworked;
	}

	public boolean isCompensedDay() {
		return compensedDay;
	}

	public void setCompensedDay(boolean compensedDay) {
		this.compensedDay = compensedDay;
	}

	public boolean isRecuperedDay() {
		return recuperedDay;
	}

	public void setRecuperedDay(boolean recuperedDay) {
		this.recuperedDay = recuperedDay;
	}

	public boolean isAbsCompensedDay() {
		return AbsCompensedDay;
	}

	public void setAbsCompensedDay(boolean absCompensedDay) {
		AbsCompensedDay = absCompensedDay;
	}

	public boolean isAbsRecuperedDay() {
		return AbsRecuperedDay;
	}

	public void setAbsRecuperedDay(boolean absRecuperedDay) {
		AbsRecuperedDay = absRecuperedDay;
	}

	public boolean isFlexible() {
		if (hrFixes != null)
			for (Plage plage : this.getHrFixes()) 
				if (plage.getPlageHor() != null 
						&& (plage.getPlageHor().getFlexDebut() != 0.0 
								|| plage.getPlageHor().getFlexFin() != 0.0)) 
					return true;
		return false;
	}

	public void setFlexible(boolean flexible) {
		this.flexible = flexible;
	}

	public List<Absence> getLegalAbs() {
		return legalAbs;
	}

	public void setLegalAbs(List<Absence> legalAbs) {
		this.legalAbs = legalAbs;
	}

	public boolean isSansPoi() {
		return sansPoi;
	}

	public void setSansPoi(boolean sansPoi) {
		this.sansPoi = sansPoi;
	}

	public static String getANNOT1() {
		return ANNOT1;
	}

	public int getSmOffset() {
		return smOffset;
	}

	public void setSmOffset(int smOffset) {
		this.smOffset = smOffset;
	}

	public Object getExceptP() {
		return exceptP;
	}

	public void setExceptP(Object exceptP) {
		this.exceptP = exceptP;
	}

	public boolean isEnMasse() {
		return enMasse;
	}

	public void setEnMasse(boolean enMasse) {
		this.enMasse = enMasse;
	}

	public boolean isHorContinu() {
		return horContinu;
	}

	public void setHorContinu(boolean horContinu) {
		this.horContinu = horContinu;
	}
	
	public boolean isPayed() {
		return payed;
	}

	public void setPayed(boolean payed) {
		this.payed = payed;
	}

	public boolean isCumulTrait() {
		return cumulTrait;
	}

	public void setCumulTrait(boolean cumulTrait) {
		this.cumulTrait = cumulTrait;
	}
	
	public boolean isLiveState() {
		return liveState;
	}
	
	public void setLiveState(boolean liveState) {
		this.liveState = liveState;
	}

	public Collection<Absence> getAbsencesEffectues() {
		return absencesEffectues;
	}
	

	public Collection<Retard> getRetardsEffectues() {
		return retardsEffectues;
	}

	public Collection<Interruption> getInterruptionsEffectues() {
		return interruptionsEffectues;
	}

	public Collection<HeurSupp> getHrSuppsEffectues() {
		return hrSuppsEffectues;
	}

	public List<Mouvement> getMvtIns() {
		return mvtIns;
	}

	public void setMvtIns(List<Mouvement> mvtIns) {
		this.mvtIns = mvtIns;
	}

	public List<Mouvement> getMvtOuts() {
		return mvtOuts;
	}

	public void setMvtOuts(List<Mouvement> mvtOuts) {
		this.mvtOuts = mvtOuts;
	}

	public Collection<Conge> getCongesEffectues() {
		return congesEffectues;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
