/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jan 14 15:35:04 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.evenement;

/**
 * 
 */
public abstract class Visiteur
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 3254949648353438693L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private java.lang.String nom;

    /**
     * 
     */
    public java.lang.String getNom()
    {
        return this.nom;
    }

    public void setNom(java.lang.String nom)
    {
        this.nom = nom;
    }

    private java.lang.String prenom;

    /**
     * 
     */
    public java.lang.String getPrenom()
    {
        return this.prenom;
    }

    public void setPrenom(java.lang.String prenom)
    {
        this.prenom = prenom;
    }

    private java.lang.String societe;

    /**
     * 
     */
    public java.lang.String getSociete()
    {
        return this.societe;
    }

    public void setSociete(java.lang.String societe)
    {
        this.societe = societe;
    }

    private java.lang.String fonction;

    /**
     * 
     */
    public java.lang.String getFonction()
    {
        return this.fonction;
    }

    public void setFonction(java.lang.String fonction)
    {
        this.fonction = fonction;
    }

    private java.lang.String tel;

    /**
     * 
     */
    public java.lang.String getTel()
    {
        return this.tel;
    }

    public void setTel(java.lang.String tel)
    {
        this.tel = tel;
    }

    private java.lang.String email;

    /**
     * 
     */
    public java.lang.String getEmail()
    {
        return this.email;
    }

    public void setEmail(java.lang.String email)
    {
        this.email = email;
    }

    private java.lang.String codeBadge;

    /**
     * 
     */
    public java.lang.String getCodeBadge()
    {
        return this.codeBadge;
    }

    public void setCodeBadge(java.lang.String codeBadge)
    {
        this.codeBadge = codeBadge;
    }

    private java.lang.String vihiculImm;

    /**
     * 
     */
    public java.lang.String getVihiculImm()
    {
        return this.vihiculImm;
    }

    public void setVihiculImm(java.lang.String vihiculImm)
    {
        this.vihiculImm = vihiculImm;
    }

    private boolean blackList;

    /**
     * 
     */
    public boolean isBlackList()
    {
        return this.blackList;
    }

    public void setBlackList(boolean blackList)
    {
        this.blackList = blackList;
    }

    private java.lang.String activite;

    /**
     * 
     */
    public java.lang.String getActivite()
    {
        return this.activite;
    }

    public void setActivite(java.lang.String activite)
    {
        this.activite = activite;
    }

    private ma.nawarit.checker.evenement.PieceIdentite pieceIdentite = new ma.nawarit.checker.evenement.PieceIdentiteImpl();

    /**
     * 
     */
    public ma.nawarit.checker.evenement.PieceIdentite getPieceIdentite()
    {
        return this.pieceIdentite;
    }

    public void setPieceIdentite(ma.nawarit.checker.evenement.PieceIdentite pieceIdentite)
    {
        this.pieceIdentite = pieceIdentite;
    }

    /**
     * Returns <code>true</code> if the argument is an Visiteur instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Visiteur))
        {
            return false;
        }
        final Visiteur that = (Visiteur)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.evenement.Visiteur}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.evenement.Visiteur}.
         */
        public static ma.nawarit.checker.evenement.Visiteur newInstance()
        {
            return new ma.nawarit.checker.evenement.VisiteurImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.evenement.Visiteur}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.evenement.Visiteur newInstance(java.lang.String nom, java.lang.String prenom, java.lang.String societe, java.lang.String fonction, java.lang.String tel, java.lang.String email, java.lang.String codeBadge, java.lang.String vihiculImm, boolean blackList, java.lang.String activite, ma.nawarit.checker.evenement.PieceIdentite pieceIdentite)
        {
            final ma.nawarit.checker.evenement.Visiteur entity = new ma.nawarit.checker.evenement.VisiteurImpl();
            entity.setNom(nom);
            entity.setPrenom(prenom);
            entity.setSociete(societe);
            entity.setFonction(fonction);
            entity.setTel(tel);
            entity.setEmail(email);
            entity.setCodeBadge(codeBadge);
            entity.setVihiculImm(vihiculImm);
            entity.setBlackList(blackList);
            entity.setActivite(activite);
            entity.setPieceIdentite(pieceIdentite);
            return entity;
        }
    }
    
// HibernateEntity.vsl merge-point
}