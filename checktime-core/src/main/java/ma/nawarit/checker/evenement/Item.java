package ma.nawarit.checker.evenement; 
 
public class Item { 
  private String name; 
  private Long price; 
  private Long id; 
 
  public Item() { 
  } 
 
  public Item(String name, Long price) { 
    this.name = name; 
    this.price = price; 
  } 
 
  public String getName() { 
    return name; 
  } 
 
  public void setName(String name) { 
    this.name = name; 
  } 
 
  public Long getPrice() { 
    return price; 
  } 
 
  public void setPrice(Long price) { 
    this.price = price; 
  } 
 
  public Long getId() { 
    return id; 
  } 
 
  public void setId(Long id) { 
    this.id = id; 
  } 
}