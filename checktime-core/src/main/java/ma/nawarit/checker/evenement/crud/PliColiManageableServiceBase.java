/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jan 09 17:07:34 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.evenement.crud;

public final class PliColiManageableServiceBase
    implements PliColiManageableService
{
    private ma.nawarit.checker.evenement.PliColiDao dao;

    public void setDao(ma.nawarit.checker.evenement.PliColiDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.evenement.PliColiDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.evenement.PliColi load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.evenement.crud.PliColiManageableService.delete(ma.nawarit.checker.evenement.PliColi entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
