/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jan 14 15:35:04 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.evenement.crud;

public final class PieceIdentiteManageableServiceBase
    implements PieceIdentiteManageableService
{
    private ma.nawarit.checker.evenement.PieceIdentiteDao dao;

    public void setDao(ma.nawarit.checker.evenement.PieceIdentiteDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.evenement.PieceIdentiteDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.evenement.PieceIdentite entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.evenement.PieceIdentite load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.evenement.PieceIdentite entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.evenement.PieceIdentite entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.evenement.crud.PieceIdentiteManageableService.delete(ma.nawarit.checker.evenement.PieceIdentite entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public java.util.List readDistinct() throws Exception{
    	return dao.readDistinct();
    }
    

}
