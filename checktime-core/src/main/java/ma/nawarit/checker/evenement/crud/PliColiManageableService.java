/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jan 09 17:07:34 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.evenement.crud;

public interface PliColiManageableService
{
    public void create(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception;

    public ma.nawarit.checker.evenement.PliColi load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception;

    public void delete(ma.nawarit.checker.evenement.PliColi entity)
        throws Exception;

}
