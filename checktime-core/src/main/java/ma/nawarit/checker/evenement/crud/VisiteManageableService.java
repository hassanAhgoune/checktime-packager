/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Jan 13 10:30:36 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.evenement.crud;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.VisiteCriteria;

public interface VisiteManageableService
{
    public void create(ma.nawarit.checker.evenement.Visite entity)
        throws Exception;

    public ma.nawarit.checker.evenement.Visite load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.evenement.Visite entity)
        throws Exception;

    public void delete(ma.nawarit.checker.evenement.Visite entity)
        throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
}
