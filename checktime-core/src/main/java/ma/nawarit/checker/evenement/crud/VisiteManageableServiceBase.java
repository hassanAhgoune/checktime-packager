/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Jan 13 10:30:36 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author H.Bousnguar
 */
package ma.nawarit.checker.evenement.crud;

import ma.nawarit.checker.compagnie.LdapConstants;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.VisiteCriteria;

public final class VisiteManageableServiceBase
    implements VisiteManageableService
{
    private ma.nawarit.checker.evenement.VisiteDao dao;
    private ma.nawarit.checker.evenementLdap.VisiteLdapDao ldapDao;
    private LdapConstants ldapConstants;
    public void setDao(ma.nawarit.checker.evenement.VisiteDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.evenement.VisiteDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.evenement.Visite entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.evenement.Visite load(int id)
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
     		return ldapDao.load(id);
     	else
     		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.read(properties);
    	else
    		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readAll();
    	else
    		return dao.readAll();
    }


    public void update(ma.nawarit.checker.evenement.Visite entity)
        throws Exception
    {

        dao.update(entity);
    }
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }
    public void delete(ma.nawarit.checker.evenement.Visite entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.evenement.crud.VisiteManageableService.delete(ma.nawarit.checker.evenement.Visite entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

	public ma.nawarit.checker.evenementLdap.VisiteLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(ma.nawarit.checker.evenementLdap.VisiteLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}


    

}
