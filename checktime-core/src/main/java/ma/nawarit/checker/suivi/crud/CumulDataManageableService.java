/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Aug 06 08:22:22 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;


import ma.nawarit.checker.suivi.CumulData;

import org.andromda.spring.CommonCriteria;

public interface CumulDataManageableService
{
    public void create(ma.nawarit.checker.suivi.CumulData entity)
        throws Exception;

    public ma.nawarit.checker.suivi.CumulData load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.CumulData entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.CumulData entity)
        throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public int getRowCountByCriteria(CommonCriteria crit);

}
