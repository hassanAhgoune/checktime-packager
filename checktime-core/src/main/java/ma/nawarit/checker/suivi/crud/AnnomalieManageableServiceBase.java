/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jul 11 07:50:37 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.LdapConstants;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Annomalie;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.PeriodCriteria;

public final class AnnomalieManageableServiceBase
    implements AnnomalieManageableService
{
    private ma.nawarit.checker.suivi.AnnomalieDao dao;
    private ma.nawarit.checker.suiviLdap.AnnomalieLdapDao ldapDao;
    private LdapConstants ldapConstants;
    public void setDao(ma.nawarit.checker.suivi.AnnomalieDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.AnnomalieDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception
    {

       dao.create(entity);
    }
    
    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception{
    	return dao.create(entities);
	}

    
     public ma.nawarit.checker.suivi.Annomalie load(int id)
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
     		return ldapDao.load(id);
     	else
     		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.read(properties);
    	else
    		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readAll();
    	else
    		return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.AnnomalieManageableService.delete(ma.nawarit.checker.suivi.Annomalie entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception{
    	return dao.readByCriteria(d1, d2);
    }
    
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }

    public void removeCriteria(CommonCriteria crit) throws Exception{
    	dao.removeCriteria(crit);
    }
    
    public void removeAllCriteria(PeriodCriteria crit) throws Exception{
    	dao.removeAllCriteria(crit);
    }

	public ma.nawarit.checker.suiviLdap.AnnomalieLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(ma.nawarit.checker.suiviLdap.AnnomalieLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}
    public List<Annomalie> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) throws Exception{
    	return dao.getItemsByrange(startPk, numberOfRows, sortField, ascending);
    }
    public int getRowCount(){
    	return dao.getRowCount();
    }
    public int getRowCountByCriteria(CommonCriteria crit){
    	return dao.getRowCountByCriteria(crit);
    }

	public List readByDate(Date d1) throws Exception {
		return dao.readByDate(d1);
	}
}
