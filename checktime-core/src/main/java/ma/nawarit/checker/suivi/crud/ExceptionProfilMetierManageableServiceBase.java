/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Mar 02 10:11:06 ACT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import org.andromda.spring.CommonCriteria;

public final class ExceptionProfilMetierManageableServiceBase
    implements ExceptionProfilMetierManageableService
{
    private ma.nawarit.checker.suivi.ExceptionProfilMetierDao dao;

    public void setDao(ma.nawarit.checker.suivi.ExceptionProfilMetierDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.ExceptionProfilMetierDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.suivi.ExceptionProfilMetier entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.suivi.ExceptionProfilMetier load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }

    public java.util.List readByCriteria(CommonCriteria crit)
	throws Exception
	{
        return dao.readByCriteria(crit);
    }

    public void update(ma.nawarit.checker.suivi.ExceptionProfilMetier entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.ExceptionProfilMetier entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.ExceptionProfilMetierManageableService.delete(ma.nawarit.checker.suivi.ExceptionProfilMetier entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
