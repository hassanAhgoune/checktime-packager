/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.LdapConstants;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.suivi.Absence;

import org.andromda.spring.CommonCriteria;

public final class AbsenceManageableServiceBase
    implements AbsenceManageableService
{
    private ma.nawarit.checker.suivi.AbsenceDao dao;
    private ma.nawarit.checker.suiviLdap.AbsenceLdapDao ldapDao;
    private LdapConstants ldapConstants;

    public void setDao(ma.nawarit.checker.suivi.AbsenceDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.AbsenceDao getDao()
    {
        return this.dao;
    }

    public ma.nawarit.checker.suivi.Absence create(ma.nawarit.checker.suivi.Absence entity)
        throws Exception
    {

       return dao.create(entity);
    }
    
    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception{
    	return dao.create(entities);
	}

    
     public ma.nawarit.checker.suivi.Absence load(int id)
        throws Exception
    {
		if(ldapConstants.isLdapEnabled())
    		return ldapDao.load(id);
    	else
    		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.read(properties);
    	else
    		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readAll();
    	else
    		return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.Absence entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.Absence entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.AbsenceManageableService.delete(ma.nawarit.checker.suivi.Absence entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    public java.util.List findBySH(int i)
    throws Exception
	{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.findBySH(i);
    	else
    		return dao.findBySH(i);
	}
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception{
    	return dao.readByCriteria(d1, d2);
    }
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }
    public java.util.List readByCriteria(String ded,User user)  throws Exception{
    	return dao.readByCriteria(ded,user);
    }
    public java.util.List readByAdvancedCriteria(Date dateRetard,double durre,String Matricule,String Motif){
    	return dao.readByAdvancedCriteria(dateRetard, durre, Matricule, Motif);
    }
    public BigInteger countAbsentsPerMounth(CommonCriteria crit)throws Exception{
    	return dao.countAbsentsPerMounth(crit);
    }
	public ma.nawarit.checker.suiviLdap.AbsenceLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(ma.nawarit.checker.suiviLdap.AbsenceLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}
	public double readCumulJ(CommonCriteria crit) throws Exception{
		if(ldapConstants.isLdapEnabled())
			return ldapDao.readCumulJ(crit);
		else
			return dao.readCumulJ(crit);
		
	}
	   public double readCumulSJ(CommonCriteria crit) throws Exception{
		   if(ldapConstants.isLdapEnabled())
				return ldapDao.readCumulSJ(crit);
			else
				return dao.readCumulSJ(crit);
	   }
	    public List<Absence> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) throws Exception{
	    	return dao.getItemsByrange(startPk, numberOfRows, sortField, ascending);
	    }
	    public int getRowCount(){
	    	return dao.getRowCount();
	    }
	    public int getRowCountByCriteria(CommonCriteria crit){
	    	return dao.getRowCountByCriteria(crit);
	    }
}
