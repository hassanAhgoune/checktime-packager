/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.suivi;
import java.math.BigInteger;
import java.nio.file.OpenOption;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.compagnie.User;

import org.andromda.spring.CommonCriteria;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.suivi.Absence</code>.
 * </p>
 *
 * @see ma.nawarit.checker.suivi.Absence
 */
public abstract class AbsenceDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.suivi.AbsenceDao
{

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.suivi.AbsenceImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.suivi.Absence)entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#load(int)
     */
    public ma.nawarit.checker.suivi.Absence load(int id)
    {
        return (ma.nawarit.checker.suivi.Absence)this.load(TRANSFORM_NONE, id);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.suivi.AbsenceImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(ma.nawarit.checker.suivi.Absence)
     */
    public ma.nawarit.checker.suivi.Absence create(ma.nawarit.checker.suivi.Absence absence)
    {
        return (ma.nawarit.checker.suivi.Absence)this.create(TRANSFORM_NONE, absence);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(int transform, ma.nawarit.checker.suivi.Absence)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.suivi.Absence absence)
    {
        if (absence == null)
        {
            throw new IllegalArgumentException(
                "Absence.create - 'absence' can not be null");
        }
        this.getHibernateTemplate().save(absence);
        return this.transformEntity(transform, absence);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(int, java.util.Collection)
     */
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Absence.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.suivi.Absence)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(java.util.Date, double, java.util.Date, java.util.Date, java.util.Date)
     */
    public ma.nawarit.checker.suivi.Absence create(
        java.util.Date dateReprise,
        double Duree,
        double heureDebut,
        double heureReprise,
        java.util.Date dateDebut,
        String statut)
    {
        return (ma.nawarit.checker.suivi.Absence)this.create(TRANSFORM_NONE, dateReprise, Duree, heureDebut, heureReprise, dateDebut, statut);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(int, java.util.Date, double, java.util.Date, java.util.Date, java.util.Date)
     */
    public java.lang.Object create(
        final int transform,
        java.util.Date dateReprise,
        double Duree,
        double heureDebut,
        double heureReprise,
        java.util.Date dateDebut,
       String statut)
    {
        ma.nawarit.checker.suivi.Absence entity = new ma.nawarit.checker.suivi.AbsenceImpl();
        entity.setDateReprise(dateReprise);
        entity.setDuree(Duree);
        entity.setDateDebut(dateDebut);
        entity.setStatut(statut);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(ma.nawarit.checker.compagnie.User, java.util.Date, java.util.Date, double, java.util.Date, java.util.Date, ma.nawarit.checker.suivi.TypeAbsence)
     */
    public ma.nawarit.checker.suivi.Absence create(
        ma.nawarit.checker.compagnie.User collaborateur,
        java.util.Date dateDebut,
        java.util.Date dateReprise,
        double Duree,
        double heureDebut,
        double heureReprise,
        String statut,
        ma.nawarit.checker.suivi.TypeAbsence typeAbsence)
    {
        return (ma.nawarit.checker.suivi.Absence)this.create(TRANSFORM_NONE, collaborateur, dateDebut, dateReprise, Duree, heureDebut, heureReprise, statut, typeAbsence);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#create(int, ma.nawarit.checker.compagnie.User, java.util.Date, java.util.Date, double, java.util.Date, java.util.Date, ma.nawarit.checker.suivi.TypeAbsence)
     */
    public java.lang.Object create(
        final int transform,
        ma.nawarit.checker.compagnie.User collaborateur,
        java.util.Date dateDebut,
        java.util.Date dateReprise,
        double Duree,
        double heureDebut,
        double heureReprise,
        String statut,
        ma.nawarit.checker.suivi.TypeAbsence typeAbsence)
    {
        ma.nawarit.checker.suivi.Absence entity = new ma.nawarit.checker.suivi.AbsenceImpl();
        entity.setCollaborateur(collaborateur);
        entity.setDateDebut(dateDebut);
        entity.setDateReprise(dateReprise);
        entity.setDuree(Duree);
        entity.setStatut(statut);
        entity.setTypeAbsence(typeAbsence);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#update(ma.nawarit.checker.suivi.Absence)
     */
    public void update(ma.nawarit.checker.suivi.Absence absence)
    {
        if (absence == null)
        {
            throw new IllegalArgumentException(
                "Absence.update - 'absence' can not be null");
        }
        this.getHibernateTemplate().update(absence);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#update(java.util.Collection)
     */
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Absence.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.suivi.Absence)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#remove(ma.nawarit.checker.suivi.Absence)
     */
    public void remove(ma.nawarit.checker.suivi.Absence absence)
    {
        if (absence == null)
        {
            throw new IllegalArgumentException(
                "Absence.remove - 'absence' can not be null");
        }
        this.getHibernateTemplate().delete(absence);
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.suivi.Absence entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.suivi.AbsenceDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Absence.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.suivi.AbsenceDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.suivi.AbsenceDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.suivi.Absence entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.suivi.Absence)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.suivi.AbsenceDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.suivi.Absence)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }

    public double readCumulJ(CommonCriteria crit){
    	   final Session session = getSession(false);

           try
           {
           	String query = "SELECT SUM(duree) FROM ABSENCE a, USER u, TYPE_ABSENCE t"
           				  +" WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK=t.ID";
           	
               	String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
               	String strDate2 = Utils.convertDate2Str(crit.getDateFin(),"yyyy-MM-dd") +" 23:59";		 
           		query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')";       		
           
           
               	
           		query += " AND (u.badge = '"+ crit.getNumBadge()+"')";
           	
           		query += " AND (a.statut = 'workflow_status_elapsed' )";
           	
           		query += " AND (UPPER(t.LIBELLE) NOT like UPPER('" + crit.getMotif()+ "') )";
         


           	SQLQuery q = session.createSQLQuery(query);
           	Double result = (Double)q.uniqueResult();
           
           	if (result == null) return 0; // Traitement � d�finir : retour de -1 ou lever une exception...
           	return result.doubleValue();
           }
           catch (org.hibernate.HibernateException ex)
           {
               throw super.convertHibernateAccessException(ex);
           }
    }
    public double readCumulSJ(CommonCriteria crit){
 	   final Session session = getSession(false);

        try
        {
        	String query = "SELECT SUM(duree) FROM ABSENCE a, USER u, TYPE_ABSENCE t"
        				  +" WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK=t.ID";
        	
            	String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
            	String strDate2 = Utils.convertDate2Str(crit.getDateFin(),"yyyy-MM-dd") +" 23:59";		 
        		query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')";       		
        
        
            	
        		query += " AND (u.badge = '"+ crit.getNumBadge()+"')";
        	
        		query += " AND (a.statut = 'workflow_status_elapsed' )";
        	
        		query += " AND (UPPER(t.LIBELLE) like UPPER('" + crit.getMotif()+ "') )";
      

        	
        	SQLQuery q = session.createSQLQuery(query);
        	Double result = (Double)q.uniqueResult();
        
			
        	if (result == null) return 0; // Traitement � d�finir : retour de -1 ou lever une exception...
           	return result.doubleValue();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
 }
    
    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			String tab[];
			Criteria crit[];
			int i=0;
			while(it.hasNext()){
				String tmp = (String)it.next();
				tab=tmp.split("\\.");
				i=0;
				if(tab.length>1){
				crit=new Criteria[tab.length-1];
				crit[0]=criteria.createCriteria(tab[0]);
				for(i=1;i<tab.length-1;i++)
					crit[i]=crit[i-1].createCriteria(tab[i]);
				i--;
				crit[i].add( Expression.eq(tab[i+1], properties.get(tmp)));
				}else
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}	
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }

    public java.util.List findBySH(int id){
        final Session session = getSession(false);

           try
           {
        final Criteria criteria = 
         session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class)
             
           .createCriteria("collaborateur")
           .createAlias("supH","sup")
           .add( Expression.eq("sup.id", Integer.valueOf(id)));

        return criteria.list();
           }
           catch (org.hibernate.HibernateException ex)
           {
            throw super.convertHibernateAccessException(ex);
           }

       }
    public java.util.List readByCriteria(Date d1, Date d2)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
            criteria.add( Restrictions.between("dateDebut", d1, d2) );
            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    private String buildQueryByCriteria(CommonCriteria crit){
    	String query = " AND u.DROIT_PAIE=1 ";
    	if (crit.getDateDebut() != null && crit.getDateFin()!=null){
        	String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
        	String strDate2 = Utils.convertDate2Str(crit.getDateFin(),"yyyy-MM-dd") +" 23:59";		 
    		query += " AND ((a.DATE_DEBUT BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')"
					+ " OR (a.DATE_REPRISE BETWEEN '" + strDate1 +"' AND '" + strDate2 +"'))";       		
    	}
    	if((crit.getMatriculeInf() != null && !"".equals(crit.getMatriculeInf().trim())) 
        		&& crit.getMatriculeSup() != null && !"".equals(crit.getMatriculeSup().trim())){
    		query += " AND (cast(u.MATRICULE as SIGNED) >= '"+crit.getMatriculeInf()+"'";
    		query += " AND cast(u.MATRICULE as SIGNED) <= '"+crit.getMatriculeSup()+"')";
    	}
    	if (crit.getIndexs() != null && !crit.getIndexs().isEmpty()) {
    		query += " AND (u.ID in " + crit.getIndexs().toString().replace('[', '(').replace(']', ')') + ")";
    	}
    	if(crit.getNoeudId() != null && !"".equals(crit.getNoeudId().trim())
    			&& crit.getProfilId() != null && !"".equals(crit.getProfilId().trim())){
    		query += " AND (u.NOEUD_FK = n.id AND u.PROFIL_METIER_FK= prf.id)";
    	}
    	if(crit.getNoeudId() != null && !"".equals(crit.getNoeudId().trim())){
    		query += " AND (u.NOEUD_FK= " + crit.getNoeudId()+ " )";
    	}
    	if(crit.getMotif() != null && !"".equals(crit.getMotif().trim())){
    		query += " AND (UPPER(t.libelle) like UPPER('" + crit.getMotif()+ "') )";
    	}
    	if(crit.getStatut() != null && !"".equals(crit.getStatut().trim())){
    		query += " AND (a.STATUT like '" + crit.getStatut()+ "')";
    	}
    	if(crit.getSiteId() != null && !"".equals(crit.getSiteId().trim())){
    		query += " AND (u.UNIT_FK='" + crit.getSiteId()+ "')";
    	}
		if (crit.isProcessActive()) {
			query += " AND (a.ID_PROCESS>0)";
		}

    	return query;
    }
    public java.util.List readByCriteria(CommonCriteria crit)
    {
        final Session session = getSession(false);

        try
        {
        	String query = "SELECT distinct(a.id) FROM ABSENCE a, TYPE_ABSENCE t, USER u " 
        		+ "WHERE a.USER_FK=u.ID AND a.TYPE_ABSENCE_FK = t.ID";
        	query += buildQueryByCriteria(crit);
        	if(crit.getNumOfRows()>0)
    			query +=" LIMIT "+crit.getNumOfRows()+" OFFSET "+crit.getFirstRow();
        	query +=" ORDER BY id desc";
        	SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();
			List result =null;
			if(myList != null && myList.size() >= 1){
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer)iter.next();
					result.add(this.load(id.intValue()));
					
				}
				
			}
			
			return result;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    public java.util.List readByCriteria(String ded,User user)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.suivi.AbsenceImpl.class);
            criteria
            .add(Restrictions.like("collaborateur", user))
            .createCriteria("typeAbsence")
            	.add(Restrictions.like("deduir", ded));
            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    
    public java.util.List readByAdvancedCriteria(Date dateDebut,double durre,String Matricule,String Motif)
    {
        final Session session = getSession(false);
        try
        {
        	String strDate1 = Utils.convertDate2Str(dateDebut, "yyyy-MM-dd")+" 00:00:00";
        	String strDate2 = Utils.convertDate2Str(dateDebut,"yyyy-MM-dd")+" 23:59:00";	
        	String query = "SELECT distinct(a.ID) FROM ABSENCE AS a "
        			+ " LEFT JOIN USER AS u ON u.ID=a.USER_FK"
        			+ " LEFT JOIN TYPE_ABSENCE AS t ON t.ID=a.TYPE_ABSENCE_FK "
        				  +" WHERE u.MATRICULE='"+Matricule.trim()+"' AND (a.DATE_DEBUT BETWEEN '"+
        			strDate1+"' AND '"+strDate2+"') AND a.DUREE='"+durre+"' AND t.LIBELLE='"+Motif+"'";
        	SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();

			List result =null;
			if(myList != null && myList.size() >= 1){
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer)iter.next();
					result.add(this.load(id.intValue()));
				}
			}
			
			return result;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    
    
    public BigInteger countAbsentsPerMounth(CommonCriteria crit){
    	try{
	    	final Session session = getSession(false);
	    	String query = "SELECT count(distinct(u.id)) FROM ABSENCE a, USER u"
				  +" WHERE a.USER_FK=u.ID AND a.STATUT='workflow_status_elapsed' ";
			if (crit.getDateDebut() != null && crit.getDateFin()!=null){
				String strDate1 = Utils.convertDate2Str(crit.getDateDebut(), "yyyy-MM-dd") + " 00:00";
				String strDate2 = Utils.convertDate2Str(crit.getDateFin(),"yyyy-MM-dd") +" 23:59";		 
				query += " AND (a.DATE_DEBUT BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')";       		
			}
			SQLQuery q = session.createSQLQuery(query);
			List results = q.list();
			if (results != null && results.size() > 0){
				return (BigInteger)results.get(0);
			}
			return BigInteger.valueOf(0);
    }catch (org.hibernate.HibernateException ex){
        throw super.convertHibernateAccessException(ex);
    }
   }
    
 
	public List<Absence> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) {
		final Session session = getSession(false);
		String query ="SELECT DISTINCT(ab.id) FROM ABSENCE ab ";
		if(numberOfRows>0)
			query +=" LIMIT "+numberOfRows+" OFFSET "+startPk;
		SQLQuery q = session.createSQLQuery(query);
		List myList = q.list();
		List result =null;
		if(myList != null && myList.size() >= 1){
			Iterator<Integer> iter = myList.iterator();
			Integer id;
			result = new ArrayList();
			while (iter.hasNext()) {
				id = (Integer)iter.next();
				result.add(this.load(id.intValue()));
				
			}
			
		}
		return result;

	}
 
 
	public int getRowCount() {
		final Session session = getSession(false);
		String query ="SELECT COUNT(distinct(ab.id)) FROM Absence ab";
		SQLQuery q = session.createSQLQuery(query);
		BigInteger tmp = (BigInteger) q.uniqueResult();
		return tmp.intValue();
	}
    public int getRowCountByCriteria(CommonCriteria crit)
    {
        final Session session = getSession(false);

        try
        {
        	String query = "SELECT COUNT(distinct(a.id)) FROM ABSENCE a, TYPE_ABSENCE t, USER u WHERE a.USER_FK=u.ID And a.TYPE_ABSENCE_FK = t.ID";
        	query += buildQueryByCriteria(crit);
        	SQLQuery q = session.createSQLQuery(query);
        	BigInteger tmp = (BigInteger) q.uniqueResult();
    		return tmp.intValue();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
}