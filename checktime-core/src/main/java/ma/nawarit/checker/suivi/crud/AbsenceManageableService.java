/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.suivi.Absence;

import org.andromda.spring.CommonCriteria;

public interface AbsenceManageableService
{
    public ma.nawarit.checker.suivi.Absence create(ma.nawarit.checker.suivi.Absence entity)
        throws Exception;
    
    public java.util.Collection create(final java.util.Collection entities)
        throws Exception;

    public ma.nawarit.checker.suivi.Absence load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.Absence entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Absence entity)
        throws Exception;
    public java.util.List findBySH(int i)
    throws Exception;
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public java.util.List readByCriteria(String ded,User user) throws Exception;
    public java.util.List readByAdvancedCriteria(Date dateRetard,double durre,String Matricule,String Motif);
    public BigInteger countAbsentsPerMounth(CommonCriteria crit)throws Exception;
    public double readCumulJ(CommonCriteria crit) throws Exception;
    public double readCumulSJ(CommonCriteria crit) throws Exception;
    public List<Absence> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) throws Exception;
    public int getRowCount() ;
    public int getRowCountByCriteria(CommonCriteria crit);
}
