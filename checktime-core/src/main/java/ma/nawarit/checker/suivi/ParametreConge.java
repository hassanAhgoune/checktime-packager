/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Oct 29 10:53:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.suivi;

/**
 * 
 */
public abstract class ParametreConge
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = -6436122800484466995L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private java.lang.String libelle;

    /**
     * 
     */
    public java.lang.String getLibelle()
    {
        return this.libelle;
    }

    public void setLibelle(java.lang.String libelle)
    {
        this.libelle = libelle;
    }
    
    private boolean defaultRegle;

    private double valBase;

    /**
     * 
     */
    public double getValBase()
    {
        return this.valBase;
    }

    public void setValBase(double valBase)
    {
        this.valBase = valBase;
    }

    private int anciennete;

    /**
     * 
     */
    public int getAnciennete()
    {
        return this.anciennete;
    }

    public void setAnciennete(int anciennete)
    {
        this.anciennete = anciennete;
    }

    private java.lang.String increment;

    /**
     * 
     */
    public java.lang.String getIncrement()
    {
        return this.increment;
    }

    public void setIncrement(java.lang.String increment)
    {
        this.increment = increment;
    }

    private double incrementVal;

    /**
     * 
     */
    public double getIncrementVal()
    {
        return this.incrementVal;
    }

    public void setIncrementVal(double incrementVal)
    {
        this.incrementVal = incrementVal;
    }

    private double maxVal;

    /**
     * 
     */
    public double getMaxVal()
    {
        return this.maxVal;
    }

    public void setMaxVal(double maxVal)
    {
        this.maxVal = maxVal;
    }

    private ma.nawarit.checker.compagnie.ProfilMetier profilMetier ;

    /**
     * 
     */
    public ma.nawarit.checker.compagnie.ProfilMetier getProfilMetier()
    {
        return this.profilMetier;
    }

    public void setProfilMetier(ma.nawarit.checker.compagnie.ProfilMetier profilMetier)
    {
        this.profilMetier = profilMetier;
    }

    /**
     * Returns <code>true</code> if the argument is an ParametreConge instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof ParametreConge))
        {
            return false;
        }
        final ParametreConge that = (ParametreConge)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.suivi.ParametreConge}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.ParametreConge}.
         */
        public static ma.nawarit.checker.suivi.ParametreConge newInstance()
        {
            return new ma.nawarit.checker.suivi.ParametreCongeImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.ParametreConge}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.suivi.ParametreConge newInstance(java.lang.String libelle, boolean defaultRegle, double valBase, int anciennete, java.lang.String increment, double incrementVal, double maxVal, ma.nawarit.checker.compagnie.ProfilMetier profilMetier)
        {
            final ma.nawarit.checker.suivi.ParametreConge entity = new ma.nawarit.checker.suivi.ParametreCongeImpl();
            entity.setLibelle(libelle);
            entity.setValBase(valBase);
            entity.setAnciennete(anciennete);
            entity.setIncrement(increment);
            entity.setIncrementVal(incrementVal);
            entity.setMaxVal(maxVal);
            entity.setDefaultRegle(defaultRegle);
            entity.setProfilMetier(profilMetier);
            return entity;
        }
    }

	public boolean isDefaultRegle() {
		return defaultRegle;
	}

	public void setDefaultRegle(boolean defaultRegle) {
		this.defaultRegle = defaultRegle;
	}
    
// HibernateEntity.vsl merge-point
}