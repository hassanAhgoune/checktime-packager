/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Oct 28 09:14:38 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

public final class TypeAbsenceManageableServiceBase
    implements TypeAbsenceManageableService
{
    private ma.nawarit.checker.suivi.TypeAbsenceDao dao;

    public void setDao(ma.nawarit.checker.suivi.TypeAbsenceDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.TypeAbsenceDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.suivi.TypeAbsence entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.suivi.TypeAbsence load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.TypeAbsence entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.TypeAbsence entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService.delete(ma.nawarit.checker.suivi.TypeAbsence entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
