/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import ma.nawarit.checker.compagnie.LdapConstants;

import org.andromda.spring.CommonCriteria;

public final class HeurSuppManageableServiceBase
    implements HeurSuppManageableService
{
    private ma.nawarit.checker.suivi.HeurSuppDao dao;
    private ma.nawarit.checker.suiviLdap.HeurSuppLdapDao ldapDao;
    private LdapConstants ldapConstants;

    public void setDao(ma.nawarit.checker.suivi.HeurSuppDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.HeurSuppDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.suivi.HeurSupp entity)
        throws Exception
    {

       dao.create(entity);
    }
    
    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception{
    	return dao.create(entities);
	}

    
     public ma.nawarit.checker.suivi.HeurSupp load(int id)
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
    		 return ldapDao.load(id);
    	 else
    		 return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
    		 return ldapDao.read(properties);
    	 else
    		 return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
    		 return ldapDao.readAll();
    	 else
    		 return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.HeurSupp entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.HeurSupp entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.HeurSuppManageableService.delete(ma.nawarit.checker.suivi.HeurSupp entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public java.util.List findBySH(int id)
    throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
    		 return ldapDao.findBySH(id);
    	 else
    		 return dao.findBySH(id);
	}
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception{
    	 if(ldapConstants.isLdapEnabled())
    		 return ldapDao.readByCriteria(crit);
    	 else
    		 return dao.readByCriteria(crit);
    }

	public ma.nawarit.checker.suiviLdap.HeurSuppLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(ma.nawarit.checker.suiviLdap.HeurSuppLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}
	public double readCumul(CommonCriteria crit) throws Exception{
		 if(ldapConstants.isLdapEnabled())
			 return ldapDao.readCumul(crit);
		 else
			 return dao.readCumul(crit);
	}
	public double readCumulManque(CommonCriteria crit) throws Exception{
		 if(ldapConstants.isLdapEnabled())
			 return ldapDao.readCumulManque(crit);
		 else
			 return dao.readCumulManque(crit);
	}
	 public double countHeurSuppByTaux(CommonCriteria crit, int taux) throws Exception{
		 return dao.countHeurSuppByTaux(crit, taux);
	 }
	 
}
