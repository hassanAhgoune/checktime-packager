/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Aug 06 08:22:22 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Retard;

import org.andromda.spring.CommonCriteria;

public interface RetardManageableService
{
    public void create(ma.nawarit.checker.suivi.Retard entity)
        throws Exception;
    
    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception;
        	
    public ma.nawarit.checker.suivi.Retard load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.Retard entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Retard entity)
        throws Exception;
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public java.util.List readByAdvancedCriteria(CommonCriteria crit) throws Exception;
    public java.util.List readByAdvancedCriteria(String Matricule,Date dateRetard,int durre);
    public BigInteger countRetardsPerMounth(CommonCriteria crit) throws Exception;
    public double CountRetardNonTolere(CommonCriteria crit) throws Exception;
    public double CountRetardTolere(CommonCriteria crit) throws Exception;
    public List<Retard> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) throws Exception;
    public int getRowCount() ;
    public int getRowCountByCriteria(CommonCriteria crit);

}
