package ma.nawarit.checker.suivi;

import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.crud.MouvementManageableService;

public class MvtObje {
	
	private Date dateMvt;
	private Collection<Date> listeHeur;
	private MouvementManageableService mouvementService;
	
	
	public Date getDateMvt() {
		return dateMvt;
	}
	
	public void setDateMvt(Date dateMvt) {
		this.dateMvt = dateMvt;
	}
	
	public Collection<Date> getListeHeur() {
		return listeHeur;
	}
	
	public void setListeHeur(Collection<Date> listeHeur) {
		this.listeHeur = listeHeur;
	} 
//	public Collection<Date> getMouvementDates(){
//		Collection<Date> dateListe=null;
//		Collection<Mouvement> mouvements=null;
//			try {
//				mouvements = mouvementService.readAll();
//				Iterator<Mouvement> it=mouvements.iterator();
//				while(it.hasNext()){
//					dateListe.add((it.next()).getDate());
//				}
//						
//			} catch (Exception e) {
//				
//			}
//		return dateListe;
//	}
	
//	Collection<Date> getMouvementHeure(Date d){
//		Collection<Date> heurListe=null;
//		try {
//			Hashtable hash=new Hashtable();
//			hash.put("date", d);
//			List liste = mouvementService.read(hash);
//			Iterator<Mouvement> it=liste.iterator();
//			while(it.hasNext()){
//				heurListe.add((it.next()).getHeure());
//			}
//			} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//			return heurListe;
//	}

	public MouvementManageableService getMouvementService() {
		return mouvementService;
	}

	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}
}
