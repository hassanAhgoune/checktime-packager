/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Mar 16 05:29:04 ACT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import org.andromda.spring.CommonCriteria;

public interface ExceptionPositionManageableService
{
    public void create(ma.nawarit.checker.suivi.ExceptionPosition entity)
        throws Exception;

    public ma.nawarit.checker.suivi.ExceptionPosition load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;
    
    public java.util.List readByCriteria(CommonCriteria crit)
    	throws Exception;

    public void update(ma.nawarit.checker.suivi.ExceptionPosition entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.ExceptionPosition entity)
        throws Exception;

}
