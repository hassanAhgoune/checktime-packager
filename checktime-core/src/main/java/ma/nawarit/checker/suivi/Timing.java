/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Mar 02 10:11:06 ACT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.suivi;

/**
 * 
 */
public abstract class Timing
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = -5227107031206692978L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private double hreDebut;

    /**
     * 
     */
    public double getHreDebut()
    {
        return this.hreDebut;
    }

    public void setHreDebut(double hreDebut)
    {
        this.hreDebut = hreDebut;
    }

    private double hreFin;

    /**
     * 
     */
    public double getHreFin()
    {
        return this.hreFin;
    }

    public void setHreFin(double hreFin)
    {
        this.hreFin = hreFin;
    }

    /**
     * Returns <code>true</code> if the argument is an Timing instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Timing))
        {
            return false;
        }
        final Timing that = (Timing)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.suivi.Timing}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.Timing}.
         */
        public static ma.nawarit.checker.suivi.Timing newInstance()
        {
            return new ma.nawarit.checker.suivi.TimingImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.Timing}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.suivi.Timing newInstance(double hreDebut, double hreFin)
        {
            final ma.nawarit.checker.suivi.Timing entity = new ma.nawarit.checker.suivi.TimingImpl();
            entity.setHreDebut(hreDebut);
            entity.setHreFin(hreFin);
            return entity;
        }
    }
    
// HibernateEntity.vsl merge-point
}