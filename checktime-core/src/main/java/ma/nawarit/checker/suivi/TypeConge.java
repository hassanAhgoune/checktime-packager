/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Nov 10 11:11:42 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.suivi;

/**
 * 
 */
public abstract class TypeConge
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 6211703536921141927L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private java.lang.String libelle;

    /**
     * 
     */
    public java.lang.String getLibelle()
    {
        return this.libelle;
    }

    public void setLibelle(java.lang.String libelle)
    {
        this.libelle = libelle;
    }

    private java.lang.String code;

    /**
     * 
     */
    public java.lang.String getCode()
    {
        return this.code;
    }

    public void setCode(java.lang.String code)
    {
        this.code = code;
    }

    private boolean annuel;

    /**
     * 
     */
    public boolean isAnnuel()
    {
        return this.annuel;
    }

    public void setAnnuel(boolean annuel)
    {
        this.annuel = annuel;
    }

    private int nbrJour;

    /**
     * 
     */
    public int getNbrJour()
    {
        return this.nbrJour;
    }

    public void setNbrJour(int nbrJour)
    {
        this.nbrJour = nbrJour;
    }

    private boolean paye;

    /**
     * 
     */
    public boolean getPaye()
    {
        return this.paye;
    }

    public void setPaye(boolean paye)
    {
        this.paye = paye;
    }

    /**
     * Returns <code>true</code> if the argument is an TypeConge instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof TypeConge))
        {
            return false;
        }
        final TypeConge that = (TypeConge)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.suivi.TypeConge}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.TypeConge}.
         */
        public static ma.nawarit.checker.suivi.TypeConge newInstance()
        {
            return new ma.nawarit.checker.suivi.TypeCongeImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.suivi.TypeConge}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.suivi.TypeConge newInstance(java.lang.String libelle, java.lang.String code, boolean annuel, int nbrJour, boolean paye)
        {
            final ma.nawarit.checker.suivi.TypeConge entity = new ma.nawarit.checker.suivi.TypeCongeImpl();
            entity.setLibelle(libelle);
            entity.setCode(code);
            entity.setAnnuel(annuel);
            entity.setNbrJour(nbrJour);
            entity.setPaye(paye);
            return entity;
        }
    }
    
// HibernateEntity.vsl merge-point
}