/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 25 10:00:23 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.math.BigInteger;
import java.util.Date;

import ma.nawarit.checker.compagnie.LdapConstants;
import ma.nawarit.checker.compagnie.User;

import org.andromda.spring.CommonCriteria;

public final class AbsenceEffManageableServiceBase
    implements AbsenceEffManageableService
{
    private ma.nawarit.checker.suivi.AbsenceEffDao dao;

    public void setDao(ma.nawarit.checker.suivi.AbsenceEffDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.AbsenceEffDao getDao()
    {
        return this.dao;
    }

    public ma.nawarit.checker.suivi.AbsenceEff create(ma.nawarit.checker.suivi.AbsenceEff entity)
        throws Exception
    {

       return dao.create(entity);
    }

    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception{
    	return dao.create(entities);
	}
    
     public ma.nawarit.checker.suivi.AbsenceEff load(int id)
        throws Exception
    {
    		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    		return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.AbsenceEff entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.AbsenceEff entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.AbsenceEffManageableService.delete(ma.nawarit.checker.suivi.AbsenceEff entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    public java.util.List findBySH(int i)
    throws Exception
	{
    		return dao.findBySH(i);
	}
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception{
    	return dao.readByCriteria(d1, d2);
    }
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	
    		return dao.readByCriteria(crit);
    }
    public java.util.List readByCriteria(String ded,User user)  throws Exception{
    	return dao.readByCriteria(ded,user);
    }
    public BigInteger countAbsentsPerMounth(CommonCriteria crit)throws Exception{
    	return dao.countAbsentsPerMounth(crit);
    }
}
