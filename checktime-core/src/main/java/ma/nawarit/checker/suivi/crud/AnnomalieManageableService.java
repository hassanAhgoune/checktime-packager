/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jul 11 07:50:37 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.util.Date;
import java.util.List;

import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Annomalie;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.PeriodCriteria;

public interface AnnomalieManageableService
{
    public void create(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception;
    
    public java.util.Collection create(final java.util.Collection entities)
        throws Exception;
    
    public ma.nawarit.checker.suivi.Annomalie load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;
    public java.util.List readByDate(Date d1)
            throws Exception;


    public void update(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Annomalie entity)
        throws Exception;
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public void removeCriteria(CommonCriteria crit) throws Exception;
    public void removeAllCriteria(PeriodCriteria crit) throws Exception;
    public List<Annomalie> getItemsByrange(Integer startPk, int numberOfRows, String sortField, boolean ascending) throws Exception;
    public int getRowCount() ;
    public int getRowCountByCriteria(CommonCriteria crit);
}
