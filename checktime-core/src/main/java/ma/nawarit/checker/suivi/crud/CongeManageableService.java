/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Nov 10 14:04:48 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.math.BigInteger;
import java.util.Date;

import ma.nawarit.checker.compagnie.User;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.CumulCriteria;

public interface CongeManageableService
{
    public void create(ma.nawarit.checker.suivi.Conge entity)
        throws Exception;
    
    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception;

    public ma.nawarit.checker.suivi.Conge load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.Conge entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Conge entity)
        throws Exception;
public java.util.List findBySH(int i)
    throws Exception;
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public java.util.List readByCustomPointageCriteria(CommonCriteria crit) throws Exception;
    public java.util.List readByAdvancedCriteria(Date dateDebut,double nbrJour,String Matricule,String Motif);
    public java.util.List readByCriteria(String pay,User user) throws Exception;
    public java.util.List readByCriteriaListe(CumulCriteria crit) throws Exception;
    public BigInteger countCongesPerMounth(CommonCriteria crit)throws Exception;
    public double readCumulYear(CommonCriteria crit)throws Exception;
}
