/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Sep 15 13:46:46 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

import java.util.Date;

import org.andromda.spring.CommonCriteria;

public interface InterruptionManageableService
{
    public void create(ma.nawarit.checker.suivi.Interruption entity)
        throws Exception;
    
    public java.util.Collection create(final java.util.Collection entities)
        throws Exception;

    public ma.nawarit.checker.suivi.Interruption load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.Interruption entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Interruption entity)
        throws Exception;
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
    public java.util.List findBySH(int id) throws Exception;
}
