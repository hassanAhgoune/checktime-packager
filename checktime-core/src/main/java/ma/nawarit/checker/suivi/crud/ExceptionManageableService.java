/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jul 04 09:10:16 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;

public interface ExceptionManageableService
{
    public void create(ma.nawarit.checker.suivi.Except entity)
        throws Exception;
    
    public ma.nawarit.checker.suivi.Except load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.suivi.Except entity)
        throws Exception;

    public void delete(ma.nawarit.checker.suivi.Except entity)
        throws Exception;

}
