/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Nov 10 14:04:48 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.suivi.crud;
import java.math.BigInteger;
import java.util.Date;

import ma.nawarit.checker.compagnie.LdapConstants;
import ma.nawarit.checker.compagnie.User;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.CumulCriteria;
public final class CongeManageableServiceBase
    implements CongeManageableService
{
    private ma.nawarit.checker.suivi.CongeDao dao;
    private ma.nawarit.checker.suiviLdap.CongeLdapDao ldapDao;
    private LdapConstants ldapConstants;
    public void setDao(ma.nawarit.checker.suivi.CongeDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.suivi.CongeDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.suivi.Conge entity)
        throws Exception
    {

       dao.create(entity);
    }

    public java.util.Collection create(final java.util.Collection entities)
    	throws Exception{
    	return dao.create(entities);
	}
    
     public ma.nawarit.checker.suivi.Conge load(int id)
        throws Exception
    {
    	 if(ldapConstants.isLdapEnabled())
     		return ldapDao.load(id);
     	else
     		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.read(properties);
    	else
    		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readAll();
    	else
    		return dao.readAll();
    }


    public void update(ma.nawarit.checker.suivi.Conge entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.suivi.Conge entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.suivi.crud.CongeManageableService.delete(ma.nawarit.checker.suivi.Conge entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    public java.util.List findBySH(int i) throws Exception
	{
		 if(ldapConstants.isLdapEnabled())
	 		return ldapDao.findBySH(i);
	 	else
	 		return dao.findBySH(i);
	}
    public java.util.List readByCriteria(Date d1, Date d2)  throws Exception{
    	return dao.readByCriteria(d1, d2);
    }
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }
    
    public java.util.List readByCustomPointageCriteria(CommonCriteria crit){
    	return ldapDao.readByCustomPointageCriteria(crit);
    }
    
    public java.util.List readByCriteria(String pay,User user) throws Exception{
    	return dao.readByCriteria(pay,user);
    }
  
    public java.util.List readByCriteriaListe(CumulCriteria crit) throws Exception{
    	return dao.readByCriteriaListe(crit);
    }
    
    public java.util.List readByAdvancedCriteria(Date dateDebut,double nbrJour,String Matricule,String Motif){
    	return dao.readByAdvancedCriteria(dateDebut, nbrJour, Matricule, Motif);
    }
    
    public BigInteger countCongesPerMounth(CommonCriteria crit)throws Exception{
    	return dao.countCongesPerMounth(crit);
    }
	public ma.nawarit.checker.suiviLdap.CongeLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(ma.nawarit.checker.suiviLdap.CongeLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}
	public double readCumulYear(CommonCriteria crit){
		return dao.readCumulYear(crit);
	}
}