/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jul 05 16:06:07 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.traymonitor.crud;

public interface InjectorManageableService
{
    public void create(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception;

    public ma.nawarit.checker.traymonitor.Injector load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception;

    public void delete(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception;

}
