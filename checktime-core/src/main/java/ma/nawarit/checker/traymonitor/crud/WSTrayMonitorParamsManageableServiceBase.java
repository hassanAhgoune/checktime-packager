/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Oct 14 12:32:03 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.traymonitor.crud;

import java.util.Collection;

public final class WSTrayMonitorParamsManageableServiceBase
    implements WSTrayMonitorParamsManageableService
{
    private ma.nawarit.checker.traymonitor.WSTrayMonitorParamsDao dao;

    public void setDao(ma.nawarit.checker.traymonitor.WSTrayMonitorParamsDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.traymonitor.WSTrayMonitorParamsDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.traymonitor.WSTrayMonitorParams load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.traymonitor.crud.WSTrayMonitorParamsManageableService.delete(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public void create(Collection entities)
    throws Exception
{

   dao.create(entities);
}
    

}
