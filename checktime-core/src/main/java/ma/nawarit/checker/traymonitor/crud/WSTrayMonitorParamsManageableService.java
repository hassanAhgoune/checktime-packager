/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Oct 14 12:32:03 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.traymonitor.crud;

import java.util.Collection;

public interface WSTrayMonitorParamsManageableService
{
    public void create(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception;

    public ma.nawarit.checker.traymonitor.WSTrayMonitorParams load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception;

    public void delete(ma.nawarit.checker.traymonitor.WSTrayMonitorParams entity)
        throws Exception;
    
    public void create(Collection entities)
    throws Exception;
}
