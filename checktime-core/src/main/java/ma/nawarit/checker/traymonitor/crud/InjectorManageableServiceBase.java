/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jul 05 16:06:07 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.traymonitor.crud;

public final class InjectorManageableServiceBase
    implements InjectorManageableService
{
    private ma.nawarit.checker.traymonitor.InjectorDao dao;

    public void setDao(ma.nawarit.checker.traymonitor.InjectorDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.traymonitor.InjectorDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.traymonitor.Injector load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.traymonitor.Injector entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.traymonitor.crud.InjectorManageableService.delete(ma.nawarit.checker.traymonitor.Injector entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
