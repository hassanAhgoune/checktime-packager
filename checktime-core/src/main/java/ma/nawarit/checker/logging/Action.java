/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 11 12:24:14 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.logging;

/**
 * 
 */
public abstract class Action
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 5423843766073381061L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private java.lang.String actionName;

    /**
     * 
     */
    public java.lang.String getActionName()
    {
        return this.actionName;
    }

    public void setActionName(java.lang.String actionName)
    {
        this.actionName = actionName;
    }

    /**
     * Returns <code>true</code> if the argument is an Action instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Action))
        {
            return false;
        }
        final Action that = (Action)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.logging.Action}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.logging.Action}.
         */
        public static ma.nawarit.checker.logging.Action newInstance()
        {
            return new ma.nawarit.checker.logging.ActionImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.logging.Action}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.logging.Action newInstance(java.lang.String actionName)
        {
            final ma.nawarit.checker.logging.Action entity = new ma.nawarit.checker.logging.ActionImpl();
            entity.setActionName(actionName);
            return entity;
        }
    }
    
// HibernateEntity.vsl merge-point
}