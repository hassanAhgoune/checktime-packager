/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 11 12:24:14 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.logging.crud;

public interface ActionManageableService
{
    public void create(ma.nawarit.checker.logging.Action entity)
        throws Exception;

    public ma.nawarit.checker.logging.Action load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.logging.Action entity)
        throws Exception;

    public void delete(ma.nawarit.checker.logging.Action entity)
        throws Exception;

}
