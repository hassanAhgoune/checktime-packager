/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 11 12:24:14 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.logging.crud;

import org.andromda.spring.CommonCriteria;

public final class LoggingManageableServiceBase
    implements LoggingManageableService
{
    private ma.nawarit.checker.logging.LoggingDao dao;

    public void setDao(ma.nawarit.checker.logging.LoggingDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.logging.LoggingDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.logging.Logging entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.logging.Logging load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }

    public java.util.List readByCriteria(org.andromda.spring.CommonCriteria crit)
    	throws Exception
    {
        return dao.readByCriteria(crit);
    }

    public void update(ma.nawarit.checker.logging.Logging entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.logging.Logging entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.logging.crud.LoggingManageableService.delete(ma.nawarit.checker.logging.Logging entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
