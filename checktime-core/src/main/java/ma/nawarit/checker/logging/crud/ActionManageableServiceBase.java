/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Sep 11 12:24:14 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.logging.crud;

public final class ActionManageableServiceBase
    implements ActionManageableService
{
    private ma.nawarit.checker.logging.ActionDao dao;

    public void setDao(ma.nawarit.checker.logging.ActionDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.logging.ActionDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.logging.Action entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.logging.Action load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.logging.Action entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.logging.Action entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.logging.crud.ActionManageableService.delete(ma.nawarit.checker.logging.Action entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
