/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Jan 13 10:30:36 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author H.Bousnguar
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.evenementLdap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.evenement.Visite;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;
import ma.nawarit.checker.compagnie.UserDaoImpl;
import ma.nawarit.checker.compagnieLdap.UserLdapDao;
import ma.nawarit.checker.compagnieLdap.UserLdapDaoImpl;
import ma.nawarit.checker.suivi.Absence;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.VisiteCriteria;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.evenement.Visite</code>.
 * </p>
 *
 * @see ma.nawarit.checker.evenement.Visite
 */
public abstract class VisiteLdapDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.evenementLdap.VisiteLdapDao
{
	private UserDao userDao = new UserDaoImpl();
	private UserLdapDao userLdapDao = new UserLdapDaoImpl();
    public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserLdapDao getUserLdapDao() {
		return userLdapDao;
	}

	public void setUserLdapDao(UserLdapDao userLdapDao) {
		this.userLdapDao = userLdapDao;
	}

	/**
     * @see ma.nawarit.checker.evenement.VisiteDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.evenement.VisiteImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.evenement.Visite)entity);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#load(int)
     */
    public ma.nawarit.checker.evenement.Visite load(int id)
    {
        Visite vis = (Visite)this.load(TRANSFORM_NONE, id);
    	try {
    		vis.setUser(userDao.load(vis.getUser().getId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return vis;
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.evenement.VisiteImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(ma.nawarit.checker.evenement.Visite)
     */
    public ma.nawarit.checker.evenement.Visite create(ma.nawarit.checker.evenement.Visite visite)
    {
        return (ma.nawarit.checker.evenement.Visite)this.create(TRANSFORM_NONE, visite);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(int transform, ma.nawarit.checker.evenement.Visite)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.evenement.Visite visite)
    {
        if (visite == null)
        {
            throw new IllegalArgumentException(
                "Visite.create - 'visite' can not be null");
        }
        this.getHibernateTemplate().save(visite);
        return this.transformEntity(transform, visite);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Visite.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.evenement.Visite)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String)
     */
    public ma.nawarit.checker.evenement.Visite create(
        java.lang.String type,
        java.lang.String lieu,
        java.util.Date dateVisite,
        java.lang.String motif,
        java.lang.String statut)
    {
        return (ma.nawarit.checker.evenement.Visite)this.create(TRANSFORM_NONE, type, lieu, dateVisite, motif, statut);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(int, java.lang.String, java.lang.String, java.util.Date, java.lang.String, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String type,
        java.lang.String lieu,
        java.util.Date dateVisite,
        java.lang.String motif,
        java.lang.String statut)
    {
        ma.nawarit.checker.evenement.Visite entity = new ma.nawarit.checker.evenement.VisiteImpl();
        entity.setType(type);
        entity.setLieu(lieu);
        entity.setDateVisite(dateVisite);
        entity.setMotif(motif);
        entity.setStatut(statut);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(java.util.Date, java.lang.String, java.lang.String, java.lang.String, java.lang.String, ma.nawarit.checker.compagnie.User, ma.nawarit.checker.evenement.Visiteur)
     */
    public ma.nawarit.checker.evenement.Visite create(
        java.util.Date dateVisite,
        java.lang.String lieu,
        java.lang.String motif,
        java.lang.String statut,
        java.lang.String type,
        ma.nawarit.checker.compagnie.User user,
        ma.nawarit.checker.evenement.Visiteur visiteur)
    {
        return (ma.nawarit.checker.evenement.Visite)this.create(TRANSFORM_NONE, dateVisite, lieu, motif, statut, type, user, visiteur);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#create(int, java.util.Date, java.lang.String, java.lang.String, java.lang.String, java.lang.String, ma.nawarit.checker.compagnie.User, ma.nawarit.checker.evenement.Visiteur)
     */
    public java.lang.Object create(
        final int transform,
        java.util.Date dateVisite,
        java.lang.String lieu,
        java.lang.String motif,
        java.lang.String statut,
        java.lang.String type,
        ma.nawarit.checker.compagnie.User user,
        ma.nawarit.checker.evenement.Visiteur visiteur)
    {
        ma.nawarit.checker.evenement.Visite entity = new ma.nawarit.checker.evenement.VisiteImpl();
        entity.setDateVisite(dateVisite);
        entity.setLieu(lieu);
        entity.setMotif(motif);
        entity.setStatut(statut);
        entity.setType(type);
        entity.setUser(user);
        entity.setVisiteur(visiteur);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#update(ma.nawarit.checker.evenement.Visite)
     */
    public void update(ma.nawarit.checker.evenement.Visite visite)
    {
        if (visite == null)
        {
            throw new IllegalArgumentException(
                "Visite.update - 'visite' can not be null");
        }
        this.getHibernateTemplate().update(visite);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Visite.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.evenement.Visite)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#remove(ma.nawarit.checker.evenement.Visite)
     */
    public void remove(ma.nawarit.checker.evenement.Visite visite)
    {
        if (visite == null)
        {
            throw new IllegalArgumentException(
                "Visite.remove - 'visite' can not be null");
        }
        this.getHibernateTemplate().delete(visite);
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.evenement.Visite entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.evenement.VisiteDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Visite.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.evenement.VisiteDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.evenement.VisiteDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.evenement.Visite entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.evenement.Visite)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.evenement.VisiteDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.evenement.Visite)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.evenement.VisiteImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			Iterator<Visite> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			Visite vis ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				vis = iter.next();
				use=userDao.load(vis.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					vis.setUser(use);
					list.add(vis);
				}
				
			}
			 return list;
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.evenement.VisiteImpl.class);

            Iterator<Visite> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			Visite vis ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				vis = iter.next();
				use=userDao.load(vis.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					vis.setUser(use);
					list.add(vis);
				}
				
			}
			 return list;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }

    public java.util.List readByCriteria(CommonCriteria crit)
    {
        final Session session = getSession(false);

        try
        {
        	String query = "SELECT v.id FROM VISITE v, VISITEUR r,USER u"
        				  +" where r.id = v.VISITEUR_FK and u.id=v.USER_FK ";
        	if (crit.getDateVisite() != null ){
            	String strDate1 = Utils.convertDate2Str(crit.getDateVisite(), "yyyy-MM-dd") + " 00:00";
            	String strDate2 = Utils.convertDate2Str(crit.getDateVisite(), "yyyy-MM-dd") + " 23:59";
            	query += " AND (v.DATE_VISITE BETWEEN '" + strDate1 +"' AND '"+ strDate2+ "')";       		
        	}
//        	if((crit.getNomVisite() != null && !"".equals(crit.getNomVisite().trim())) 
//            		){
//        		query += " AND u.nom ='"+crit.getNomVisite()+"'";
//        	}
//        	if((crit.getPrenomVisite() != null && !"".equals(crit.getPrenomVisite().trim())) 
//    		){
//        		query += " AND u.prenom ='"+crit.getPrenomVisite()+"'";
//        	}
        	if((crit.getNomVisiteur() != null && !"".equals(crit.getNomVisiteur().trim())) 
    		){
        		query += " AND r.nom ='"+crit.getNomVisiteur()+"'";
        	}
        	if((crit.getPrenomVisiteur() != null && !"".equals(crit.getPrenomVisiteur().trim())) 
    		){
        		query += " AND r.prenom ='"+crit.getPrenomVisiteur()+"'";
        	}
        	if((crit.getStatut() != null && !"".equals(crit.getStatut().trim())) 
    		){
        		query += " AND v.statut ='"+crit.getStatut()+"'";
        	}
        	if((crit.getNumBadge() != null && !"".equals(crit.getNumBadge().trim())) 
    		){
        		query += " AND r.code_badge ='"+crit.getNumBadge()+"'";
        	}
        	SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();

			List result =null;
			if(myList != null && myList.size() >= 1){
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer)iter.next();
					result.add(this.load(id.intValue()));
					
				}
				
			}
			
			return UsreFiltre(result,crit);
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    List UsreFiltre(List list,CommonCriteria crit){
    	List listTemp = new ArrayList();
    	if((crit.getNomVisite() != null && crit.getPrenomVisite() != null)&& list!=null){
    	Iterator<Visite> iter = list.iterator();
    	Visite vis;
    	User use;
    	
    	boolean nomB,prenomB;
    	while(iter.hasNext()){
    		vis = iter.next();
    		use = userLdapDao.loadLdap(vis.getUser().getId());
    		if(use!=null)
    		{
    			if(crit.getNomVisite()!=null&&!"".equals(crit.getNomVisite())){
    				if(crit.getNomVisite().equals(use.getNom())){
    					nomB = true;
    				}
    				else 
    					nomB = false;
    			}else{ nomB = true;
    			}
    			if(crit.getPrenomVisite()!=null&&!"".equals(crit.getPrenomVisite())){
    				if(crit.getPrenomVisite().equals(use.getPrenom())){
    					prenomB = true;
    				}
    				else
    					prenomB = false;
    			}else{ prenomB = true;
    			}
    			if(nomB && prenomB){
    				listTemp.add(vis);
    			}
    	
    		
    		
    		}
    			
	    		
    		}
    	}else
    		listTemp = list;
    	
    	return listTemp;
    		
    }
}