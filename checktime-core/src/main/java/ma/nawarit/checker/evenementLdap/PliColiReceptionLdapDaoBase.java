/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Tue Jan 13 10:30:36 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author H.Bousnguar
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.evenementLdap;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.evenement.PliColi;
import ma.nawarit.checker.evenement.PliColiReception;
import ma.nawarit.checker.evenement.PliColiReceptionImpl;
import ma.nawarit.checker.evenement.Visite;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;
import ma.nawarit.checker.compagnie.UserDaoImpl;
import ma.nawarit.checker.compagnieLdap.UserLdapDao;
import ma.nawarit.checker.compagnieLdap.UserLdapDaoImpl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.evenement.PliColiReception</code>.
 * </p>
 *
 * @see ma.nawarit.checker.evenement.PliColiReception
 */
public abstract class PliColiReceptionLdapDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao
{
	private UserDao userDao = new UserDaoImpl();
	private UserLdapDao userLdapDao = new UserLdapDaoImpl();
    public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public UserLdapDao getUserLdapDao() {
		return userLdapDao;
	}

	public void setUserLdapDao(UserLdapDao userLdapDao) {
		this.userLdapDao = userLdapDao;
	}

	/**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.evenement.PliColiReceptionImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.evenement.PliColiReception)entity);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#load(int)
     */
    public ma.nawarit.checker.evenement.PliColiReception load(int id)
    {
        PliColiReception pliC = (PliColiReception)this.load(TRANSFORM_NONE, id);
    	try {
    		pliC.setUser(userDao.load(pliC.getUser().getId()));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return pliC;
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.evenement.PliColiReceptionImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(ma.nawarit.checker.evenement.PliColiReception)
     */
    public ma.nawarit.checker.evenement.PliColiReception create(ma.nawarit.checker.evenement.PliColiReception pliColiReception)
    {
        return (ma.nawarit.checker.evenement.PliColiReception)this.create(TRANSFORM_NONE, pliColiReception);
    }

    /**
     * @see ma.nawarit.checker.evenement.PliColiReceptionDao#create(int transform, ma.nawarit.checker.evenement.PliColiReception)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.evenement.PliColiReception pliColiReception)
    {
        if (pliColiReception == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.create - 'pliColiReception' can not be null");
        }
        this.getHibernateTemplate().save(pliColiReception);
        return this.transformEntity(transform, pliColiReception);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.evenement.PliColiReception)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(int, java.util.Date, boolean, java.lang.String)
     */
    public ma.nawarit.checker.evenement.PliColiReception create(
        int quantite,
        java.util.Date dateReception,
        boolean recept,
        java.lang.String source)
    {
        return (ma.nawarit.checker.evenement.PliColiReception)this.create(TRANSFORM_NONE, quantite, dateReception, recept, source);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(int, int, java.util.Date, boolean, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        int quantite,
        java.util.Date dateReception,
        boolean recept,
        java.lang.String source)
    {
        ma.nawarit.checker.evenement.PliColiReception entity = new ma.nawarit.checker.evenement.PliColiReceptionImpl();
        entity.setQuantite(quantite);
        entity.setDateReception(dateReception);
        entity.setRecept(recept);
        entity.setSource(source);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(java.util.Date, ma.nawarit.checker.evenement.PliColi, int, boolean, java.lang.String, ma.nawarit.checker.compagnie.User)
     */
    public ma.nawarit.checker.evenement.PliColiReception create(
        java.util.Date dateReception,
        ma.nawarit.checker.evenement.PliColi pliColi,
        int quantite,
        boolean recept,
        java.lang.String source,
        ma.nawarit.checker.compagnie.User user)
    {
        return (ma.nawarit.checker.evenement.PliColiReception)this.create(TRANSFORM_NONE, dateReception, pliColi, quantite, recept, source, user);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#create(int, java.util.Date, ma.nawarit.checker.evenement.PliColi, int, boolean, java.lang.String, ma.nawarit.checker.compagnie.User)
     */
    public java.lang.Object create(
        final int transform,
        java.util.Date dateReception,
        ma.nawarit.checker.evenement.PliColi pliColi,
        int quantite,
        boolean recept,
        java.lang.String source,
        ma.nawarit.checker.compagnie.User user)
    {
        ma.nawarit.checker.evenement.PliColiReception entity = new ma.nawarit.checker.evenement.PliColiReceptionImpl();
        entity.setDateReception(dateReception);
        entity.setPliColi(pliColi);
        entity.setQuantite(quantite);
        entity.setRecept(recept);
        entity.setSource(source);
        entity.setUser(user);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#update(ma.nawarit.checker.evenement.PliColiReception)
     */
    public void update(ma.nawarit.checker.evenement.PliColiReception pliColiReception)
    {
        if (pliColiReception == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.update - 'pliColiReception' can not be null");
        }
        this.getHibernateTemplate().update(pliColiReception);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.evenement.PliColiReception)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#remove(ma.nawarit.checker.evenement.PliColiReception)
     */
    public void remove(ma.nawarit.checker.evenement.PliColiReception pliColiReception)
    {
        if (pliColiReception == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.remove - 'pliColiReception' can not be null");
        }
        this.getHibernateTemplate().delete(pliColiReception);
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.evenement.PliColiReception entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "PliColiReception.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.evenement.PliColiReceptionDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.evenementLdap.PliColiReceptionLdapDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.evenement.PliColiReception entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.evenement.PliColiReception)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.evenement.PliColiReceptionDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.evenement.PliColiReception)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.evenement.PliColiReceptionImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			Iterator<PliColiReception> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			PliColiReception pliC ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				pliC = iter.next();
				use=userDao.load(pliC.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					pliC.setUser(use);
					list.add(pliC);
				}
				
			}
			 return list;
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.evenement.PliColiReceptionImpl.class);

            Iterator<PliColiReception> iter = criteria.list().iterator();
			List list = new ArrayList();
			List listLdap;
			PliColiReception pliC ;
			User use;
			Hashtable hash = new Hashtable();
			while(iter.hasNext()){
				pliC = iter.next();
				use=userDao.load(pliC.getUser().getId());
				hash.put("user", use);
				listLdap = userLdapDao.readUser(hash);
				if(!listLdap.isEmpty() && listLdap!=null){
					pliC.setUser(use);
					list.add(pliC);
				}
				
			}
			 return list;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }



}