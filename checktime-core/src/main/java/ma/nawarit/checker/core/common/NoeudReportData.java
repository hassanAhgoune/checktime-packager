package ma.nawarit.checker.core.common;

import java.io.Serializable;
import java.util.List;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;


public class NoeudReportData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Noeud node;
	private List<User> users;
	public Noeud getNode() {
		return node;
	}
	public void setNode(Noeud node) {
		this.node = node;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
}