package ma.nawarit.checker.core.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.configuration.PlageHoraire;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.Timing;
import ma.nawarit.checker.suivi.TimingImpl;

import org.springframework.beans.BeanUtils;


public class Plage {
	
	private Calendar debut;
	private Calendar fin;
	private Calendar mrd;
	private Calendar finMoins;
	private Calendar debutPlus;
	private PlageHoraire plageHor;
	private Calendar debutFlex;
	private Calendar finFlex;
	private Calendar zoneOut;
	private boolean entree = false;
	private boolean sortie = false;
	private String compteur;
	private final String SANS_POI_COMPTEUR ="sansPointage";	
	private final String OUT_SANS_POI_COMPTEUR ="sortieSansPointage";
	private int index;
	private List<Timing> tmgs = new ArrayList<Timing>();
	private boolean isDebutTmg;
	private boolean isFinTmg;
	private boolean plageNuit;

	public Plage() {
		super();
	}

	public Plage (Date d, PlageHoraire plageHor) {
		this.plageHor = plageHor;
		// le debut de plage horaire
		Double Debut = new Double(plageHor.getDebut());
		debut = new GregorianCalendar ();
    	debut.setTime(d);
    	debut.set(Calendar.HOUR_OF_DAY, Debut.intValue());
    	debut.set(Calendar.MINUTE, Utils.decimal(Debut));
    	
    	
    	Double DebutFlex = new Double(plageHor.getFlexDebut());
    	if (plageHor.getFlexDebut() == 0)
    		this.debutFlex = null;
    	else {
	    	debutFlex = new GregorianCalendar ();
	    	debutFlex.setTime(debut.getTime());
	    	debutFlex.add(Calendar.HOUR_OF_DAY, DebutFlex.intValue());
	    	debutFlex.add(Calendar.MINUTE, Utils.decimal(DebutFlex));
    	}
    	
    	compteur = plageHor.getAnnotation().getTypeAnnotation().getLibelle();
    	if (compteur.equalsIgnoreCase(MessageFactory.getMessage(SANS_POI_COMPTEUR))) {
    		this.entree = true;
    		this.sortie = true;
    	} else if (compteur.equalsIgnoreCase(MessageFactory.getMessage(OUT_SANS_POI_COMPTEUR)))
    		this.sortie = true;
    	
    	// la fin de plage horaire
    	Double Fin = new Double(plageHor.getFin());
    	fin = new GregorianCalendar ();
    	fin.setTime(d);
    	fin.set(Calendar.HOUR_OF_DAY, Fin.intValue());
    	fin.set(Calendar.MINUTE, Utils.decimal(Fin));	
    	
    	zoneOut = new GregorianCalendar();
    	zoneOut.setTime(fin.getTime());
    	
    	mrd = new GregorianCalendar();
    	int h = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[0])).intValue();
		int m = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[1])).intValue();
    	mrd.setTime(debut.getTime());
    	mrd.add(Calendar.HOUR_OF_DAY, h);
    	mrd.add(Calendar.MINUTE, m);
    	
    	Double FinFlex = new Double(plageHor.getFlexFin());
    	if (plageHor.getFlexFin() == 0)
    		this.finFlex = null;
    	else {
	    	finFlex = new GregorianCalendar ();
	    	finFlex.setTime(fin.getTime());
	    	finFlex.add(Calendar.HOUR_OF_DAY, FinFlex.intValue());
	    	finFlex.add(Calendar.MINUTE, Utils.decimal(FinFlex));
    	}   	
    	
    	// le debut de plage horaire plus la tolerance d'entree
    	debutPlus = new GregorianCalendar ();
    	debutPlus.setTime(debut.getTime());
    	debutPlus.add(Calendar.MINUTE, plageHor.getTolEntree());
    	
    	// la fin de plage horaire moins la tolerance de sortie
    	finMoins = new GregorianCalendar ();
    	finMoins.setTime(fin.getTime());
    	finMoins.add(Calendar.MINUTE, - plageHor.getTolSortie());
	}
	
	public Plage (Date d, HeurSupp hrSup) {
		// le debut de plage horaire
		Double Debut = new Double(hrSup.getHeureDebut());
		debut = new GregorianCalendar ();
    	debut.setTime(d);
    	debut.set(Calendar.HOUR_OF_DAY, Debut.intValue());
    	debut.set(Calendar.MINUTE, Utils.decimal(Debut));
    	
    	
    	// la fin de plage horaire
    	Double Fin = new Double(hrSup.getHeureFin());
    	fin = new GregorianCalendar ();
    	fin.setTime(d);
    	fin.set(Calendar.HOUR_OF_DAY, Fin.intValue());
    	fin.set(Calendar.MINUTE, Utils.decimal(Fin));	
    	
    	zoneOut = new GregorianCalendar();
    	zoneOut.setTime(fin.getTime());
    	
    	mrd = new GregorianCalendar();
    	int h = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[0])).intValue();
		int m = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[1])).intValue();
    	mrd.setTime(debut.getTime());
    	mrd.add(Calendar.HOUR_OF_DAY, h);
    	mrd.add(Calendar.MINUTE, m);
    	
    	debutPlus = new GregorianCalendar ();
    	debutPlus.setTime(debut.getTime());
    	debutPlus.add(Calendar.MINUTE, 5);

    	finMoins = new GregorianCalendar ();
    	finMoins.setTime(fin.getTime());
    	finMoins.add(Calendar.MINUTE, - 5);

	}
	
	public Plage(Date d1, Date d2, PlageHoraire plageHor) {
		this.plageHor = plageHor;
		// le debut de plage horaire
		debut = new GregorianCalendar ();
    	debut.setTime(d1);
    	
    	// la fin de plage horaire
    	fin = new GregorianCalendar ();
    	if (d2 == null) {
    		fin.setTime(d1);
    		fin.set(Calendar.HOUR_OF_DAY, new Double(plageHor.getFin()).intValue());
    		fin.set(Calendar.MINUTE, Utils.decimal(plageHor.getFin()));
    	} else
    		fin.setTime(d2);
    	zoneOut = new GregorianCalendar();
    	zoneOut.setTime(fin.getTime());
    	
    	mrd = new GregorianCalendar();
    	int h = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[0])).intValue();
		int m = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[1])).intValue();
    	mrd.setTime(debut.getTime());
    	mrd.add(Calendar.HOUR_OF_DAY, h);
    	mrd.add(Calendar.MINUTE, m);
    	
    	debutPlus = new GregorianCalendar ();
    	debutPlus.setTime(debut.getTime());
    	debutPlus.add(Calendar.MINUTE, plageHor.getTolEntree());

    	finMoins = new GregorianCalendar ();
    	finMoins.setTime(fin.getTime());
    	debutPlus.add(Calendar.MINUTE, plageHor.getTolSortie());
	}
	
	public void setPlageNuit(boolean plageNuit) {
		this.plageNuit = plageNuit;
	}

	public Date [] getValideInterp(Date t1, Date t2, Double dt) {
		Calendar c1 = new GregorianCalendar();
		c1.setTime(t1);
		Calendar c2 = new GregorianCalendar();
		c2.setTime(t2);
		Calendar mf = new GregorianCalendar();
		mf.setTime(mrd.getTime());
		mf.add(Calendar.HOUR_OF_DAY, dt.intValue());
		mf.add(Calendar.MINUTE, Utils.decimal(dt));
		Date[] inters = null;
		int pause = this.plageHor.getPause();
		if (c1.compareTo(mrd) >= 0 && c2.compareTo(mf) <= 0) {			
			inters = new Date [2];
			c1.add(Calendar.MINUTE, pause);
			inters[0] = c1.getTime(); 
			inters[1] = c2.getTime();
		} else if ((c1.compareTo(mrd) < 0 && c2.compareTo(mrd) <= 0)
				|| (c1.compareTo(mf) >= 0 && c2.compareTo(mf) > 0)){
			inters = new Date [2];
			inters[0] = c1.getTime(); 
			inters[1] = c2.getTime();
		} else if (c1.compareTo(mrd) < 0 && c2.compareTo(mrd) > 0 && c2.compareTo(mf) <= 0) {
			int[] diff = Utils.differenceDates(mrd.getTime(), c2.getTime(), 1);
			int mn = diff[0]*60 + diff[1];
			if (mn <= pause)
				c2.add(Calendar.MINUTE, -mn);
			else
				c2.add(Calendar.MINUTE, -pause);
			inters = new Date[2];
			inters[0] = c1.getTime(); 
			inters[1] = c2.getTime();
		} else if (c1.compareTo(mrd) >= 0 && c1.compareTo(mf) < 0 && c2.compareTo(mf) > 0) {
			int[] diff = Utils.differenceDates(mrd.getTime(), c2.getTime(), 1);
			int mn = diff[0]*60 + diff[1];
			if (mn <= pause)
				c1.add(Calendar.MINUTE, mn);
			else
				c1.add(Calendar.MINUTE, pause);
			inters = new Date [2];
			inters[0] = c1.getTime(); 
			inters[1] = c2.getTime();
		}
		return inters;
	}
	
	
	public Calendar getDebut() {
		return debut;
	}


	public void setDebut(Calendar debut) {
		this.debut = debut;
	}


	public Calendar getFin() {
		return fin;
	}


	public void setFin(Calendar fin) {
		this.fin = fin;
	}


	public Calendar getDebutPlus() {
		return debutPlus;
	}

	public void setDebutPlus(Calendar debutPlus) {
		this.debutPlus = debutPlus;
	}	

	public PlageHoraire getPlageHor() {
		return plageHor;
	}

	public void setPlageHor(PlageHoraire plageHor) {
		this.plageHor = plageHor;
	}
	
	public boolean isPlageNuit(){
		return plageNuit;
	}
	
	public String getStartHour(){
		return debut.get(Calendar.HOUR_OF_DAY)+":"+debut.get(Calendar.MINUTE);
	}
	
	public String getEndHour(){
		return fin.get(Calendar.HOUR_OF_DAY)+":"+fin.get(Calendar.MINUTE);
	}
	
	public Calendar getDebut1() {
		return debut;
	}

	public Calendar getFin1() {
		return fin;
	}

	public Calendar getMeridian() {
		return mrd;
	}

	public void projectMvts(List<Mouvement> list) {
    	List<Mouvement> mvmnts = new ArrayList<Mouvement>(list);
    	Calendar laDate = new GregorianCalendar ();    	
    	Calendar d1 = new GregorianCalendar ();
    	Calendar d2 = new GregorianCalendar ();
    	int h = (new Double(Utils.differenceDates(debut.getTime(), fin.getTime(), 0.5)[0])).intValue();
    	d1.setTime(debut.getTime());
		d1.add(Calendar.HOUR_OF_DAY, - h);
		d2.setTime(fin.getTime());
    	d2.add(Calendar.HOUR_OF_DAY, h);
    	
		for (Iterator it = mvmnts.iterator(); it.hasNext();) {
			Mouvement mvmnt = (Mouvement) it.next();
			
			// on construit laDate par la date, l'heure et minute de pointage
			laDate.setTime(mvmnt.getDate());
			
			if ((laDate.compareTo(d1) >= 0) && (laDate.compareTo(mrd) <= 0)) {	
				this.setEntree(true);
				list.remove(mvmnt);
			}else if ((laDate.compareTo(mrd) >= 0) && (laDate.compareTo(d2) <= 0)) {	
				if (!this.isEntree())
					this.setEntree(true);
				else if (!this.isSortie())
					this.setSortie(true);
				else 
					continue;
				list.remove(mvmnt);
			}
		}
	}
	

	
	public Plage getLastPlage(List<Plage> hrFixes) {
		Plage pl = null;
		if (hrFixes.indexOf(this) > 0)
			pl = hrFixes.get(hrFixes.indexOf(this) - 1);
		return pl;
	}
	
	public Plage getFolowPlage(List<Plage> hrFixes) {
		Plage pl = null;
		if (hrFixes.size() > hrFixes.indexOf(this) + 1)
			pl = hrFixes.get(hrFixes.indexOf(this) + 1);
		return pl;
	}
	
	public Calendar getFinMoins() {
		return finMoins;
	}

	public void setFinMoins(Calendar finMoins) {
		this.finMoins = finMoins;
	}

	public Calendar getDebutFlex() {
		return debutFlex;
	}

	public void setDebutFlex(Calendar debutFlex) {
		this.debutFlex = debutFlex;
	}

	public Calendar getFinFlex() {
		return finFlex;
	}

	public void setFinFlex(Calendar finFlex) {
		this.finFlex = finFlex;
	}

	public String getCompteur() {
		return compteur;
	}

	public static void main(String[] args) {
		Hashtable<Integer, String> map = new Hashtable<Integer, String>();
		map.put(2, "deuxieme");
		map.put(1, "premier");
		map.put(1, "premiere");
		map.put(4, "quatrieme");
		map.put(3, "troisieme");
		System.out.println(map.values().toString());
		
		Calendar c = new GregorianCalendar();
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 61909);
		c.set(Calendar.MILLISECOND, 0);
		System.out.println(c.getTime());
	}

	public boolean isEntree() {
		return entree;
	}

	public void setEntree(boolean entree) {
		this.entree = entree;
	}

	public boolean isSortie() {
		return sortie;
	}

	public void setSortie(boolean sortie) {
		this.sortie = sortie;
	}
	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public Calendar getZoneOut() {
		return zoneOut;
	}

	public void setZoneOut(Calendar zoneOut) {
		this.zoneOut = zoneOut;
	}
	
	public List<Timing> getEffTmgs (Date d,Calendar debut, Calendar fin) {
		Calendar c1 = new GregorianCalendar();
		Calendar c2 = new GregorianCalendar();
		c1.setTime(d);
		c2.setTime(d);
		Timing newt;
		List newts = new ArrayList();
		for(Timing t: tmgs) {
			c1.add(Calendar.HOUR_OF_DAY, new Double(t.getHreDebut()).intValue());
			c1.add(Calendar.MINUTE, Utils.decimal(t.getHreDebut()));
			c2.add(Calendar.HOUR_OF_DAY, new Double(t.getHreFin()).intValue());
			c2.add(Calendar.MINUTE, Utils.decimal(t.getHreFin()));
			if (t.getHreDebut() > t.getHreFin())
				c1.add(Calendar.DAY_OF_MONTH, -1);
			if (debut.compareTo(c1) >= 0 && debut.compareTo(c2) < 0) {
				newt = new TimingImpl();
				if (fin.compareTo(c2) <= 0) {
					newt.setHreDebut(Utils.getHourDoubleFormat(debut.getTime()));
					newt.setHreFin(Utils.getHourDoubleFormat(fin.getTime()));
					newts.add(newt);
				} else {
					newt.setHreDebut(Utils.getHourDoubleFormat(debut.getTime()));
					newt.setHreFin(Utils.getHourDoubleFormat(c2.getTime()));
					newts.add(newt);
				}
			} else if (debut.compareTo(c1) < 0 && fin.compareTo(c1) > 0 && fin.compareTo(c2) <= 0) {
				newt = new TimingImpl();
				newt.setHreDebut(Utils.getHourDoubleFormat(c1.getTime()));
				newt.setHreFin(Utils.getHourDoubleFormat(fin.getTime()));
				newts.add(newt);
			} else if (debut.compareTo(c1) < 0 && fin.compareTo(c2) > 0) {
				newt = new TimingImpl();
				newt.setHreDebut(Utils.getHourDoubleFormat(c1.getTime()));
				newt.setHreFin(Utils.getHourDoubleFormat(c2.getTime()));
				newts.add(newt);
			}
		}
		return newts;
	}
	
	public List<Timing> getTmgs() {
		return tmgs;
	}

	public void setTmgs(List<Timing> tmgs) {
		this.tmgs = tmgs;
	}

	public boolean isDebutTmg() {
		if (this.tmgs != null && !this.tmgs.isEmpty() && this.plageHor.getDebut() == this.tmgs.get(0).getHreDebut())
			isDebutTmg = true;
		return isDebutTmg;
	}

	public void setDebutTmg(boolean isDebutTmg) {
		this.isDebutTmg = isDebutTmg;
	}

	public boolean isFinTmg() {
		if (this.tmgs != null && !this.tmgs.isEmpty() && this.plageHor.getFin() == this.tmgs.get(tmgs.size() - 1).getHreFin())
			isDebutTmg = true;
		return isFinTmg;
	}

	public void setFinTmg(boolean isFinTmg) {
		this.isFinTmg = isFinTmg;
	}
}
