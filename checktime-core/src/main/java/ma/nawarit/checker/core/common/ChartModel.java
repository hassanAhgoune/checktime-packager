package ma.nawarit.checker.core.common;

public class ChartModel {
	
	private String key;
	private Double value;
	private String color;
	

	public ChartModel(String key, Double value, String color) {
		super();
		this.key = key;
		this.value = value;
		this.color = color;
	}
	
	
	public String getColor() {
		return color;
	}


	public void setColor(String color) {
		this.color = color;
	}


	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}

}
