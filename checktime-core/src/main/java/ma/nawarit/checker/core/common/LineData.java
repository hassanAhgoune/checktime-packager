package ma.nawarit.checker.core.common;

import java.util.List;

import ma.nawarit.checker.compagnie.User;

public class LineData {
	
	private User user;
	private List<LineDay> lineDays;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<LineDay> getLineDays() {
		return lineDays;
	}
	public void setLineDays(List<LineDay> lineDays) {
		this.lineDays = lineDays;
	}

}
