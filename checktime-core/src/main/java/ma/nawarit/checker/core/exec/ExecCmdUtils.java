/*
 * Cr�� le 27 juin 06
 *
 * Pour changer le mod�le de ce fichier g�n�r�, allez � :
 * Fen�tre&gt;Pr�f�rences&gt;Java&gt;G�n�ration de code&gt;Code et commentaires
 */
package ma.nawarit.checker.core.exec;

import java.io.IOException;



import org.apache.log4j.Logger;

/**
 * BATCH Command launcher under MS-DOS (r) Microsoft
 * @author , K.Lamhaddab
 */
public class ExecCmdUtils {
	/**
	  * Launch a command and render the output on a logger.
	  * @param s java.lang.String to execute representing the command line command.
	  * @param logger org.apache.log4j.Logger : Logger to send the output, 
	  * by default output are logged into the "info" stream and errors are logged into the "error" stream.
	  */
	public static void exec(String s, Logger logger) {
		exec(s, logger, null,new StringBuffer());
	}
	public static void exec(String s, Logger logger, StringBuffer buffer) {
		exec(s, logger, null,buffer);
	}


	/**
	 * Launch a command and render the output on a logger.
	 * @param s java.lang.String to execute representing the command line command.
	 * @param logger org.apache.log4j.Logger : Logger to send the output, 
	 * @param env Array of environnement variables.
	 * by default output are logged into the "info" stream and errors are logged into the "error" stream.
	 */
	public static void exec(String s, Logger logger, String[] env,StringBuffer buffer) {
		String s1 = System.getProperty("os.name");
		String as[] = new String[3];
		if (s1.equals("Windows 2000")
			|| s1.equals("Windows Vista")
			|| s1.equals("Windows NT")
			|| s1.equals("Windows XP")
			|| s1.equals("Windows 2003")
			|| s1.equals("Windows 2008")) {
			as[0] = "cmd";
			as[1] = "/c";
			as[2] = s;
		} else if (s1.equals("Windows 95")) {
			as[0] = "command.com";
			as[1] = "/C";
			as[2] = s;
		}

		try {
			Runtime runtime = Runtime.getRuntime();

			Process process = null;
			process = runtime.exec(as, env);

			

			new InputStreamHandler(
				process.getInputStream(),
				logger,
				InputStreamHandler.OUT_TYPE,
				buffer);
			new InputStreamHandler(
				process.getErrorStream(),
				logger,
				InputStreamHandler.ERR_TYPE,
				buffer);

			process.waitFor();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
       public static void main(String[] args) {
    	   Logger log = Logger.getLogger(ExecCmdUtils.class);
    	   StringBuffer buffer= new StringBuffer();
    	   ExecCmdUtils.exec("ls c:",
    			   log, buffer);
	}    
}