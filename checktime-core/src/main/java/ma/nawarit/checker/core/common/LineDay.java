package ma.nawarit.checker.core.common;

import java.util.Date;

import java.util.List;

public class LineDay {
	
	private Date date;
	private List<String> mouvements;
	private List<String> retards;
	private List<String> absences;
	private List<String> conges;
	private List<String> justification;
	
	public LineDay(Date _date) {
		this.date=_date;
	}
	
	public LineDay(Date _date,List<String> mouvements, List<String> retards, List<String> absences, List<String> conges, List<String> justification) {
		this.date=_date;
		this.mouvements = mouvements;
		this.retards = retards;
		this.absences = absences;
		this.conges = conges;
		this.justification = justification;
	}

	public LineDay(List<String> mouvements, List<String> retards, List<String> absences, List<String> conges, List<String> justification) {
		this.mouvements = mouvements;
		this.retards = retards;
		this.absences = absences;
		this.conges = conges;
		this.justification = justification;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	public List<String> getConges() {
		return conges;
	}
	
	public void setConges(List<String> conges) {
		this.conges = conges;
	}
	
	public List<String> getMouvements() {
		return mouvements;
	}
	
	public void setMouvements(List<String> mouvements) {
		this.mouvements = mouvements;
	}
	
	public List<String> getRetards() {
		return retards;
	}
	
	public void setRetards(List<String> retards) {
		this.retards = retards;
	}
	
	public List<String> getAbsences() {
		return absences;
	}
	
	public void setAbsences(List<String> absences) {
		this.absences = absences;
	}
	
	public List<String> getJustification() {
		return justification;
	}
	
	public void setJustification(List<String> justification) {
		this.justification = justification;
	}
}
