package ma.nawarit.checker.core.exec;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


import org.apache.log4j.Logger;
/**
 * Manage the streams output and error output for a runtime.exec(...). 
 * @author K.Lamhaddab
 */
class InputStreamHandler extends Thread {
	
	/**
	 * Stream being read
	 */
	private InputStream m_stream;

	/**
	 * Type of stream Err or standar out.
	 */
	private String m_type;
	
	/**
	 * Logger
	 */
	private Logger m_logger;
	
	/**
	 * Buffer
	 */
	private StringBuffer m_buffer;
	
	/**
	 * Error stream type.
	 */
	public static String ERR_TYPE = "ERR";
	
	/**
	 * Standard out stream type.
	 */
	public static String OUT_TYPE = "OUT";

	/**
	 * Initialise and launch the stream reader.
	 * The streams are send to the logger.
	 * @param stream Stream to be logged.
	 * @param logger Logger.
	 * @param type Type of stream.
	 */
	InputStreamHandler(InputStream stream, Logger logger, String type, StringBuffer buffer) {
		m_stream = stream;
		m_type = type;
		m_logger = logger;
		m_buffer = buffer;
		start();
	}

	/**
	 * Stream the data.
	 */
	public void run() {
		try {
			BufferedReader standardOut = new BufferedReader(new InputStreamReader(m_stream));
			String message;
			while( (message = standardOut.readLine()) != null) 
			{
				if(!m_type.equals(ERR_TYPE)){
					m_buffer.append(message);
					m_logger.info(message);
				}else{
					m_buffer.append(message);
					m_logger.error(message);
				}
			} 
		} catch (IOException ioe) {
			m_logger.error(ioe.getMessage());
		}
	}
}
