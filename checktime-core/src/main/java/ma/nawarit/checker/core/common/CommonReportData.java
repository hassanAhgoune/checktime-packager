package ma.nawarit.checker.core.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.PlageHoraire;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Conge;


public class CommonReportData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private Date datemMvt;
	private Date dateJ;
	private List<Conge> congeData;
	private List<Absence> absencePData; 
	private List<Absence> absenceNPData;
	private List results;
	private String cumulRetardTolere;
	private String cumulRetardNonTolere;
	private String cumulCongePaye;
	private String cumulCongeNonPaye;
	private String cumulAbsenceAuto;
	private String cumulAbsenceNonAuto;
	private String cumulHeurSuppPaye25;
	private String cumulHeurSuppPaye50;
	private String cumulHeurSuppPaye100;	
	private String cumulHeurSuppNonPaye;
	private String cumulInterruption;
	private String cumulTrProd;
	private String cumulExcpNode;
	private List<CommonReportData> detail;
	private String module;
	private boolean annomalieTest;
	private String horaireLibelle;
	private double nbHour4Horaire;
	private double nbHourTravail;
	private double nbRetardTol;
	private double nbRetardNTol;
	private double nbInterruption;
	private double nbSAH;
	private double nbRealHour;
	
	private double totalAbsenceAut = 0;
	private double totalConge = 0;
	private double totalAbsenceNAut = 0;
	private double totalRetardTol = 0;
	private double totalRetardNTol = 0;
	private double totalInterruption = 0;
	private double totalSAH = 0;
	private double totalHeurSupp25 = 0;
	private double totalHeurSupp50 = 0;
	private double totalHeurSupp100 = 0;
	private double totalHeurNormal = 0;
	private double totalHeurTravail = 0;
	private double totalTrProd = 0;
	
	private Horaire horaire;
	private JrFerie jrFerie;
	private List<PlageHoraire> plages;
	

	public Horaire getHoraire() {
		return horaire;
	}
	public void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDatemMvt() {
		return datemMvt;
	}
	public void setDatemMvt(Date datemMvt) {
		this.datemMvt = datemMvt;
	}
	public Date getDateJ() {
		return dateJ;
	}
	public void setDateJ(Date dateJ) {
		this.dateJ = dateJ;
	}
	public java.util.List getResults() {
		return results;
	}
	public void setResults(java.util.List results) {
		this.results = results;
	}
	public String getCumulRetardTolere() {
		return cumulRetardTolere;
	}
	public void setCumulRetardTolere(String cumulRetardTolere) {
		this.cumulRetardTolere = cumulRetardTolere;
	}
	public String getCumulRetardNonTolere() {
		return cumulRetardNonTolere;
	}
	public void setCumulRetardNonTolere(String cumulRetardNonTolere) {
		this.cumulRetardNonTolere = cumulRetardNonTolere;
	}
	public String getCumulCongePaye() {
		return cumulCongePaye;
	}
	public void setCumulCongePaye(String cumulCongePaye) {
		this.cumulCongePaye = cumulCongePaye;
	}
	public String getCumulCongeNonPaye() {
		return cumulCongeNonPaye;
	}
	public void setCumulCongeNonPaye(String cumulCongeNonPaye) {
		this.cumulCongeNonPaye = cumulCongeNonPaye;
	}
	public String getCumulAbsenceAuto() {
		return cumulAbsenceAuto;
	}
	public void setCumulAbsenceAuto(String cumulAbsenceAuto) {
		this.cumulAbsenceAuto = cumulAbsenceAuto;
	}
	public String getCumulAbsenceNonAuto() {
		return cumulAbsenceNonAuto;
	}
	public void setCumulAbsenceNonAuto(String cumulAbsenceNonAuto) {
		this.cumulAbsenceNonAuto = cumulAbsenceNonAuto;
	}

	public String getCumulInterruption() {
		return cumulInterruption;
	}
	public void setCumulInterruption(String cumulInterruption) {
		this.cumulInterruption = cumulInterruption;
	}
	public java.util.List<CommonReportData> getDetail() {
		return detail;
	}
	public void setDetail(java.util.List<CommonReportData> detail) {
		this.detail = detail;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}


	public String getHoraireLibelle() {
		return horaireLibelle;
	}
	public void setHoraireLibelle(String horaireLibelle) {
		this.horaireLibelle = horaireLibelle;
	}
	public double getNbHour4Horaire() {
		return nbHour4Horaire;
	}
	public void setNbHour4Horaire(double nbHour4Horaire) {
		this.nbHour4Horaire = nbHour4Horaire;
	}
	public double getNbRetardTol() {
		return nbRetardTol;
	}
	public void setNbRetardTol(double nbRetardTol) {
		this.nbRetardTol = nbRetardTol;
	}
	public double getNbRetardNTol() {
		return nbRetardNTol;
	}
	public void setNbRetardNTol(double nbRetardNTol) {
		this.nbRetardNTol = nbRetardNTol;
	}
	public double getNbInterruption() {
		return nbInterruption;
	}
	public void setNbInterruption(double nbInterruption) {
		this.nbInterruption = nbInterruption;
	}
	public double getNbRealHour() {
		this.nbRealHour = Utils.convertMin2Hour(((nbHour4Horaire *60) - (nbRetardTol + nbRetardNTol + nbInterruption)));
		return nbRealHour;
	}
	public void setNbRealHour(double nbRealHour) {
		this.nbRealHour = nbRealHour;
	}
	public String getCumulHeurSuppNonPaye() {
		return cumulHeurSuppNonPaye;
	}
	public void setCumulHeurSuppNonPaye(String cumulHeurSuppNonPaye) {
		this.cumulHeurSuppNonPaye = cumulHeurSuppNonPaye;
	}
	public String getCumulHeurSuppPaye25() {
		return cumulHeurSuppPaye25;
	}
	public void setCumulHeurSuppPaye25(String cumulHeurSuppPaye25) {
		this.cumulHeurSuppPaye25 = cumulHeurSuppPaye25;
	}
	public String getCumulHeurSuppPaye50() {
		return cumulHeurSuppPaye50;
	}
	public void setCumulHeurSuppPaye50(String cumulHeurSuppPaye50) {
		this.cumulHeurSuppPaye50 = cumulHeurSuppPaye50;
	}
	public String getCumulHeurSuppPaye100() {
		return cumulHeurSuppPaye100;
	}
	public void setCumulHeurSuppPaye100(String cumulHeurSuppPaye100) {
		this.cumulHeurSuppPaye100 = cumulHeurSuppPaye100;
	}	
	public String getCumulTrProd() {
		return cumulTrProd;
	}
	public void setCumulTrProd(String cumulTrProd) {
		this.cumulTrProd = cumulTrProd;
	}
	public String getCumulExcpNode() {
		return cumulExcpNode;
	}
	public void setCumulExcpNode(String cumulExcpNode) {
		this.cumulExcpNode = cumulExcpNode;
	}
	public double getTotalAbsenceAut() {
		return totalAbsenceAut;
	}
	public void setTotalAbsenceAut(double totalAbsenceAut) {
		this.totalAbsenceAut = totalAbsenceAut;
	}
	public double getTotalAbsenceNAut() {
		return totalAbsenceNAut;
	}
	public void setTotalAbsenceNAut(double totalAbsenceNAut) {
		this.totalAbsenceNAut = totalAbsenceNAut;
	}
	public double getTotalRetardTol() {
		return totalRetardTol;
	}
	public void setTotalRetardTol(double totalRetardTol) {
		this.totalRetardTol = totalRetardTol;
	}
	public double getTotalRetardNTol() {
		return totalRetardNTol;
	}
	public void setTotalRetardNTol(double totalRetardNTol) {
		this.totalRetardNTol = totalRetardNTol;
	}
	public double getTotalInterruption() {
		return totalInterruption;
	}
	public void setTotalInterruption(double totalInterruption) {
		this.totalInterruption = totalInterruption;
	}
	public double getTotalHeurSupp25() {
		return totalHeurSupp25;
	}
	public void setTotalHeurSupp25(double totalHeurSupp25) {
		this.totalHeurSupp25 = totalHeurSupp25;
	}
	public double getTotalHeurSupp50() {
		return totalHeurSupp50;
	}
	public void setTotalHeurSupp50(double totalHeurSupp50) {
		this.totalHeurSupp50 = totalHeurSupp50;
	}
	public double getTotalHeurSupp100() {
		return totalHeurSupp100;
	}
	public void setTotalHeurSupp100(double totalHeurSupp100) {
		this.totalHeurSupp100 = totalHeurSupp100;
	}
	public double getNbHourTravail() {
		return nbHourTravail;
	}
	public void setNbHourTravail(double nbHourTravail) {
		this.nbHourTravail = nbHourTravail;
	}
	public double getTotalHeurNormal() {
		return totalHeurNormal;
	}
	public void setTotalHeurNormal(double totalHeurNormal) {
		this.totalHeurNormal = totalHeurNormal;
	}
	public double getTotalHeurTravail() {
		return totalHeurTravail;
	}
	public void setTotalHeurTravail(double totalHeurTravail) {
		this.totalHeurTravail = totalHeurTravail;
	}
	public JrFerie getJrFerie() {
		return jrFerie;
	}
	public void setJrFerie(JrFerie jrFerie) {
		this.jrFerie = jrFerie;
	}
	public double getTotalTrProd() {
		return totalTrProd;
	}
	public void setTotalTrProd(double totalTrProd) {
		this.totalTrProd = totalTrProd;
	}
	public List<Conge> getCongeData() {
		return congeData;
	}
	public void setCongeData(List<Conge> congeData) {
		this.congeData = congeData;
	}
	public List<Absence> getAbsencePData() {
		return absencePData;
	}
	public void setAbsencePData(List<Absence> absencePData) {
		this.absencePData = absencePData;
	}
	public List<Absence> getAbsenceNPData() {
		return absenceNPData;
	}
	public void setAbsenceNPData(List<Absence> absenceNPData) {
		this.absenceNPData = absenceNPData;
	}
	public double getTotalConge() {
		return totalConge;
	}
	public void setTotalConge(double totalConge) {
		this.totalConge = totalConge;
	}
	public boolean isAnnomalieTest() {
		return annomalieTest;
	}
	public void setAnnomalieTest(boolean annomalieTest) {
		this.annomalieTest = annomalieTest;
	}
	public double getTotalSAH() {
		return totalSAH;
	}
	public void setTotalSAH(double totalSAH) {
		this.totalSAH = totalSAH;
	}
	public double getNbSAH() {
		return nbSAH;
	}
	public void setNbSAH(double nbSAH) {
		this.nbSAH = nbSAH;
	}
	public List<PlageHoraire> getPlages() {
		return plages;
	}
	public void setPlages(List<PlageHoraire> plages) {
		this.plages = plages;
	}
	



}