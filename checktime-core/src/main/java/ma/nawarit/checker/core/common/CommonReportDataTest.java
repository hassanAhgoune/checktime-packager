package ma.nawarit.checker.core.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.AnnomalieImpl;


public class CommonReportDataTest {
	/**
	 * @param args
	 */
	public static void main(String[] args) {


		List<CommonReportData> report = getBeanCollection();
		
		
		for ( CommonReportData data: report) {
			
			List<Annomalie> anos = data.getResults();
			for ( Annomalie ano: anos) {
				System.out.println(" -- " + ano.getDateAnomalie().toGMTString() + " :: " + ano.getDescription());
				
			}
			
			
		}
		
		

	}
	
	
	public static List<CommonReportData> getBeanCollection() {
		
		List<Annomalie> anos = new ArrayList<Annomalie>();
		Annomalie ano = new AnnomalieImpl();
		ano.setDateAnomalie(new Date());
		ano.setDescription("anomalie 1");
		anos.add(ano);
		
		ano = new AnnomalieImpl();
		ano.setDateAnomalie(new Date());
		ano.setDescription("anomalie 2");
		anos.add(ano);
		
		//Create Person
		
		User user = new UserImpl();
		user.setNom("Khalid");
		user.setPrenom("Khaldoune");
		
		CommonReportData data = new CommonReportData();
		data.setUser(user);
		data.setResults(anos);
		
		List<CommonReportData> report = new ArrayList<CommonReportData>();
		report.add(data);

		return report;
				
	}


}