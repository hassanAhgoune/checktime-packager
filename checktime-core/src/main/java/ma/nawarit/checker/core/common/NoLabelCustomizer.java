package ma.nawarit.checker.core.common;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.ChartChangeEventType;
import org.jfree.chart.event.PlotChangeEvent;
import org.jfree.chart.event.PlotChangeListener;

import net.sf.jasperreports.engine.JRChart;
import net.sf.jasperreports.engine.JRChartCustomizer;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.PieDataset;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

public class NoLabelCustomizer implements JRChartCustomizer {
	private Map<String, Color> colorMap = new HashMap<String, Color>();
	private PiePlot piePlot;

	public void customize(JFreeChart chart, JRChart jasperChart) {
		// TODO Auto-generated method stub
		piePlot = (PiePlot) chart.getPlot();
		piePlot.setLabelOutlinePaint(null);
		piePlot.setIgnoreZeroValues(false);
		piePlot.setLabelShadowPaint(null);
		piePlot.setOutlineStroke(new BasicStroke(2f));
		piePlot.setOutlinePaint(Color.white);
		chart.getLegend().setBorder(0, 0, 0, 0);
		
		colorMap.put("Heures travaillées", Color.decode("#1E88E5"));
		colorMap.put("Retard", Color.decode("#FFEB3B"));
		colorMap.put("Absence justifié", Color.decode("#FF5722"));
		colorMap.put("Absence non justifié", Color.decode("#880E4F"));
		colorMap.put("Congé", Color.decode("#00ACC1"));
		colorMap.put("Interruption", Color.decode("#EE0D0D"));

		final PieDataset dataset = piePlot.getDataset();
		
		for (int i = 0; i < dataset.getItemCount(); i++) {
			piePlot.setSectionPaint(dataset.getKey(i), colorMap.get(dataset.getKey(i)));
		}

//		piePlot.addChangeListener(new PlotChangeListener() {
//
//			public void plotChanged(PlotChangeEvent event) {
//				// TODO Auto-generated method stub
//
//				if (event.getType() == ChartChangeEventType.DATASET_UPDATED) {
//					// Assign color to each section of pie chart based on value of key
//					for (int i = 0; i < dataset.getItemCount(); i++) {
//						piePlot.setSectionPaint(dataset.getKey(i), colorMap.get(dataset.getKey(i)));
//					}
//				}
//			}
//		});

	}
}
