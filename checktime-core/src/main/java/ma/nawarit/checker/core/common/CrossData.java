package ma.nawarit.checker.core.common;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.compagnie.User;

public class CrossData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;
	private Date date;
	private String mvts = "";
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getMvts() {
		return mvts;
	}
	public void setMvts(String mvts) {
		this.mvts = mvts;
	}
}
