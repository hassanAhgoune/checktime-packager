package ma.nawarit.checker.core.mail;

import javax.mail.internet.*;
import javax.mail.*;
import javax.activation.*;
import javax.swing.*;
import java.util.*;

public class MailSend {
	private static String serveur;

	private final static String MAILER_VERSION = "Java";

	public MailSend(String serveur1) {
		serveur = serveur1;

	}

	public static void envoyerMailSMTP(boolean debug, String sender,
			String receiver, String subject, String messages, String piece)
			throws MessagingException {
		
		Properties prop = System.getProperties();
		prop.put("mail.smtp.host", serveur);
		Session session = Session.getDefaultInstance(prop, null);
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(sender));
		InternetAddress[] internetAddresses = new InternetAddress[1];
		internetAddresses[0] = new InternetAddress(receiver);
		message.setRecipients(Message.RecipientType.TO, internetAddresses);
		message.setSubject(subject);
		message.setText(messages);
		message.setContent(messages, "text/html");	
		if (piece != "") {
			Multipart multipart = new MimeMultipart();
			


			// creation partie principale du message
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(messages);
			multipart.addBodyPart(messageBodyPart);

			// creation et ajout de la piece jointe
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(piece);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("Piece1");
			multipart.addBodyPart(messageBodyPart);

			// ajout des �l�ments au mail
			message.setContent(multipart);
		}
		message.setHeader("X-Mailer", MAILER_VERSION);
		message.setSentDate(new Date());
		session.setDebug(debug);
		Transport.send(message);
		
	}

}
