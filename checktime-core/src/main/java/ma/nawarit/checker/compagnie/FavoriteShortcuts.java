/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jul 09 16:20:33 GMT 20112 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.compagnie;

/**
 * 
 */
public abstract class FavoriteShortcuts
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 8404607329761743407L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private ma.nawarit.checker.compagnie.User user;

    /**
     * 
     */
    public ma.nawarit.checker.compagnie.User getUser()
    {
        return this.user;
    }

    public void setUser(ma.nawarit.checker.compagnie.User user)
    {
        this.user = user;
    }

    private ma.nawarit.checker.compagnie.ShortCuts shortCuts;

    /**
     * 
     */
    public ma.nawarit.checker.compagnie.ShortCuts getShortCuts()
    {
        return this.shortCuts;
    }

    public void setShortCuts(ma.nawarit.checker.compagnie.ShortCuts shortCuts)
    {
        this.shortCuts = shortCuts;
    }

    /**
     * Returns <code>true</code> if the argument is an FavoriteShortcuts instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof FavoriteShortcuts))
        {
            return false;
        }
        final FavoriteShortcuts that = (FavoriteShortcuts)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.compagnie.FavoriteShortcuts}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.compagnie.FavoriteShortcuts}.
         */
        public static ma.nawarit.checker.compagnie.FavoriteShortcuts newInstance()
        {
            return new ma.nawarit.checker.compagnie.FavoriteShortcutsImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.compagnie.FavoriteShortcuts}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.compagnie.FavoriteShortcuts newInstance(ma.nawarit.checker.compagnie.User user, ma.nawarit.checker.compagnie.ShortCuts shortCuts)
        {
            final ma.nawarit.checker.compagnie.FavoriteShortcuts entity = new ma.nawarit.checker.compagnie.FavoriteShortcutsImpl();
            entity.setUser(user);
            entity.setShortCuts(shortCuts);
            return entity;
        }
    }
    
// HibernateEntity.vsl merge-point
}