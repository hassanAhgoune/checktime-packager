/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

public final class ProfilMetierManageableServiceBase
    implements ProfilMetierManageableService
{
    private ma.nawarit.checker.compagnie.ProfilMetierDao dao;

    public void setDao(ma.nawarit.checker.compagnie.ProfilMetierDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.ProfilMetierDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.ProfilMetier entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.ProfilMetier load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }
    public java.util.Map profilsByCode()
        throws Exception
    {
        return dao.profilsByCode();
    }


    public void update(ma.nawarit.checker.compagnie.ProfilMetier entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.ProfilMetier entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService.delete(ma.nawarit.checker.compagnie.ProfilMetier entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
