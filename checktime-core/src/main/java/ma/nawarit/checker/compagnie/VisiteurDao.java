/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringDao.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.compagnie;

/**
 * @see ma.nawarit.checker.compagnie.Visiteur
 */
public interface VisiteurDao
{
	
	public final static int TRANSFORM_NONE = 0;
    /**
     * Loads an instance of ma.nawarit.checker.compagnie.Visiteur from the persistent store.
     */
    public ma.nawarit.checker.compagnie.Visiteur load(int Id);

    /**
     * <p>
     * Does the same thing as {@link #load(int)} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong> be transformed. If this flag is any of the other constants
     * defined in this class then the result <strong>WILL BE</strong> passed through an operation which can
     * optionally transform the entity (into a value object for example). By default, transformation does
     * not occur.
     * </p>
     *
     * @param Id the identifier of the entity to load.
     * @return either the entity or the object transformed from the entity.
     */
    public Object load(int transform, int Id);

    /**
     * Loads all entities of type {@link ma.nawarit.checker.compagnie.Visiteur}.
     *
     * @return the loaded entities.
     */
    public java.util.Collection loadAll();

    /**
     * <p>
     * Does the same thing as {@link #loadAll()} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong> be transformed. If this flag is any of the other constants
     * defined here then the result <strong>WILL BE</strong> passed through an operation which can optionally
     * transform the entity (into a value object for example). By default, transformation does
     * not occur.
     * </p>
     *
     * @param transform the flag indicating what transformation to use.
     * @return the loaded entities.
     */
    public java.util.Collection loadAll(final int transform);

    /**
     * Creates an instance of ma.nawarit.checker.compagnie.Visiteur and adds it to the persistent store.
     */
    public ma.nawarit.checker.compagnie.Visiteur create(ma.nawarit.checker.compagnie.Visiteur visiteur);

    /**
     * <p>
     * Does the same thing as {@link #create(ma.nawarit.checker.compagnie.Visiteur)} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong> be transformed. If this flag is any of the other constants
     * defined here then the result <strong>WILL BE</strong> passed through an operation which can optionally
     * transform the entity (into a value object for example). By default, transformation does
     * not occur.
     * </p>
     */
    public Object create(int transform, ma.nawarit.checker.compagnie.Visiteur visiteur);

    /**
     * Creates a new instance of ma.nawarit.checker.compagnie.Visiteur and adds
     * from the passed in <code>entities</code> collection
     *
     * @param entities the collection of ma.nawarit.checker.compagnie.Visiteur
     * instances to create.
     *
     * @return the created instances.
     */
    public java.util.Collection create(java.util.Collection entities);

    /**
     * <p>
     * Does the same thing as {@link #create(ma.nawarit.checker.compagnie.Visiteur)} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong> be transformed. If this flag is any of the other constants
     * defined here then the result <strong>WILL BE</strong> passed through an operation which can optionally
     * transform the entities (into value objects for example). By default, transformation does
     * not occur.
     * </p>
     */
    public java.util.Collection create(int transform, java.util.Collection entities);

    /**
     * <p>
     * Creates a new <code>ma.nawarit.checker.compagnie.Visiteur</code>
     * instance from <strong>all</strong> attributes and adds it to
     * the persistent store.
     * </p>
     */
    public ma.nawarit.checker.compagnie.Visiteur create(
        java.lang.String societe,
        java.lang.String fonction,
        java.util.Date dateNss,
        java.lang.String pays,
        java.lang.String city,
        java.lang.String adresse,
        java.lang.String sexe,
        java.lang.String nom,
        java.lang.String prenom,
        java.lang.String numIdent,
        java.lang.String mail);

    /**
     * <p>
     * Does the same thing as {@link #create(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong> be transformed. If this flag is any of the other constants
     * defined here then the result <strong>WILL BE</strong> passed through an operation which can optionally
     * transform the entity (into a value object for example). By default, transformation does
     * not occur.
     * </p>
     */
    public Object create(
        int transform,
        java.lang.String societe,
        java.lang.String fonction,
        java.util.Date dateNss,
        java.lang.String pays,
        java.lang.String city,
        java.lang.String adresse,
        java.lang.String sexe,
        java.lang.String nom,
        java.lang.String prenom,
        java.lang.String numIdent,
        java.lang.String mail);

    /**
     * <p>
     * Creates a new <code>ma.nawarit.checker.compagnie.Visiteur</code>
     * instance from only <strong>required</strong> properties (attributes
     * and association ends) and adds it to the persistent store.
     * </p>
     */
    public ma.nawarit.checker.compagnie.Visiteur create(
        java.lang.String fonction,
        java.lang.String mail,
        java.util.Date dateNss,
        java.lang.String pays,
        java.lang.String city,
        java.lang.String adresse,
        java.lang.String sexe,
        java.lang.String nom,
        java.lang.String prenom,
        java.lang.String numIdent,
        java.lang.String societe,
        ma.nawarit.checker.compagnie.Visite visite);

    /**
     * <p>
     * Does the same thing as {@link #create(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)} with an
     * additional flag called <code>transform</code>. If this flag is set to <code>TRANSFORM_NONE</code> then
     * the returned entity will <strong>NOT</strong be transformed. If this flag is any of the other constants
     * defined here then the result <strong>WILL BE</strong> passed through an operation which can optionally
     * transform the entity (into a value object for example). By default, transformation does
     * not occur.
     * </p>
     */
    public Object create(
        int transform,
        java.lang.String fonction,
        java.lang.String mail,
        java.util.Date dateNss,
        java.lang.String pays,
        java.lang.String city,
        java.lang.String adresse,
        java.lang.String sexe,
        java.lang.String nom,
        java.lang.String prenom,
        java.lang.String numIdent,
        java.lang.String societe,
        ma.nawarit.checker.compagnie.Visite visite);

    /**
     * Updates the <code>visiteur</code> instance in the persistent store.
     */
    public void update(ma.nawarit.checker.compagnie.Visiteur visiteur);

    /**
     * Updates all instances in the <code>entities</code> collection in the persistent store.
     */
    public void update(java.util.Collection entities);

    /**
     * Removes the instance of ma.nawarit.checker.compagnie.Visiteur from the persistent store.
     */
    public void remove(ma.nawarit.checker.compagnie.Visiteur visiteur);

    /**
     * Removes the instance of ma.nawarit.checker.compagnie.Visiteur having the given
     * <code>identifier</code> from the persistent store.
     */
    public void remove(int Id);

    /**
     * Removes all entities in the given <code>entities<code> collection.
     */
    public void remove(java.util.Collection entities);

 public java.util.List read(java.util.Hashtable properties);
 public java.util.List readAll();
}
