package ma.nawarit.checker.compagnie.crud;

import java.util.List;

public final class BadgeManageableServiceBase implements BadgeManageableService {

	public BadgeManageableServiceBase() {
		// TODO Auto-generated constructor stub
	}
	private ma.nawarit.checker.compagnie.BadgeDao dao;

    public void setDao(ma.nawarit.checker.compagnie.BadgeDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.BadgeDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.Badge entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.Badge load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.Badge entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.Badge entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.BadgeManageableService.delete(ma.nawarit.checker.compagnie.Badge entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }
    
    public java.util.List ReadByColaborateurID(int id)  throws Exception {
    	return dao.readByCollaborateursID(id);
    }
    
    public java.util.List ReadByBadgeNumber(String number) throws Exception{
    	return dao.readByBadgeNUmber(number);
    }
}
