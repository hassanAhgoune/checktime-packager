/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jul 09 16:20:33 GMT 20112 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

public final class ShortCutsManageableServiceBase
    implements ShortCutsManageableService
{
    private ma.nawarit.checker.compagnie.ShortCutsDao dao;

    public void setDao(ma.nawarit.checker.compagnie.ShortCutsDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.ShortCutsDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.ShortCuts load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.ShortCutsManageableService.delete(ma.nawarit.checker.compagnie.ShortCuts entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public java.util.List readByUser(int userId)
    throws Exception
	{ 
	    return dao.readByUser(userId);
	}
    
    public java.util.List readFreeShortCutsUser(int userId)
    throws Exception
	{ 
	    return dao.readFreeShortCutsUser(userId);
	}

}
