package ma.nawarit.checker.compagnie.crud;

public interface BadgeManageableService {
	
	
	public void create(ma.nawarit.checker.compagnie.Badge entity)
	        throws Exception;

	    public ma.nawarit.checker.compagnie.Badge load(int id)
	        throws Exception;
	    
	    public java.util.List read(java.util.Hashtable properties)
	        throws Exception;

	    public java.util.List readAll()
	        throws Exception;


	    public void update(ma.nawarit.checker.compagnie.Badge entity)
	        throws Exception;

	    public void delete(ma.nawarit.checker.compagnie.Badge entity)
	        throws Exception;
	    
	    public java.util.List ReadByColaborateurID(int id) throws Exception;
	    
	    public java.util.List ReadByBadgeNumber(String number) throws Exception;
}
