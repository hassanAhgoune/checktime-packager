package ma.nawarit.checker.compagnie;

import org.andromda.spring.CommonCriteria;

public interface BadgeDao {
	
	public final static int TRANSFORM_NONE = 0;
	
	 public ma.nawarit.checker.compagnie.Badge load(int id);
	 
	 public Object load(int transform, int id);
	 
	 public java.util.Collection loadAll();
	 
	 public java.util.Collection loadAll(final int transform);
	 
	 public ma.nawarit.checker.compagnie.Badge create(ma.nawarit.checker.compagnie.Badge badge);
	 
	 public Object create(int transform, ma.nawarit.checker.compagnie.Badge badge);
	 
	 public java.util.Collection create(java.util.Collection entities);
	 
	 public java.util.Collection create(int transform, java.util.Collection entities);
	 
	 public ma.nawarit.checker.compagnie.Badge create(java.lang.String Number, ma.nawarit.checker.compagnie.User Collaborateur);
	 
	 public ma.nawarit.checker.compagnie.Badge create(int transform, java.lang.String Number, ma.nawarit.checker.compagnie.User Collaborateur);
	 
	 public void update(ma.nawarit.checker.compagnie.Badge badge);
	 
	 public void remove(ma.nawarit.checker.compagnie.Badge badge);
	 
	 public void remove(int Id);
	 
	 public int getRowCount() ;
	 
	 public java.util.List readByCollaborateursID(int id);
	 
	 public java.util.List read(java.util.Hashtable properties);
	 
	 public java.util.List readAll();
	 
	 public java.util.List readByBadgeNUmber(java.lang.String number);
}
