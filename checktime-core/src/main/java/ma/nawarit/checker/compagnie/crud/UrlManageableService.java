/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>Checktime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jul 25 12:22:29 GMT 2012 </DD>
 * </DL>
 * @since 01/04/2012
 * @version 1.0
 * @author NAWAR TEAM
 */
package ma.nawarit.checker.compagnie.crud;

public interface UrlManageableService
{
    public void create(ma.nawarit.checker.compagnie.Url entity)
        throws Exception;

    public ma.nawarit.checker.compagnie.Url load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.compagnie.Url entity)
        throws Exception;

    public void delete(ma.nawarit.checker.compagnie.Url entity)
        throws Exception;

}
