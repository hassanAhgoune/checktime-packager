/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.compagnie;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Compensation;
import ma.nawarit.checker.suivi.Except;
import ma.nawarit.checker.suivi.ExceptionPosition;
import ma.nawarit.checker.suivi.ExceptionProfilMetier;
import ma.nawarit.checker.suivi.Retard;




/**
 * 
 */
public abstract class User
	implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 2268240279795279273L;
    
    private int Id;

    /**
     * 
     */
    public int getId()
    {
        return this.Id;
    }

    public void setId(int Id)
    {
        this.Id = Id;
    }

    private boolean flag;
    private java.lang.String nom;

    /**
     * 
     */
    public java.lang.String getNom()
    {
        return this.nom;
    }

    public void setNom(java.lang.String nom)
    {
        this.nom = nom;
    }

    private java.lang.String prenom;

    /**
     * 
     */
    public java.lang.String getPrenom()
    {
        return this.prenom;
    }

    public void setPrenom(java.lang.String prenom)
    {
        this.prenom = prenom;
    }

    private java.lang.String mail;

    /**
     * 
     */
    public java.lang.String getMail()
    {
        return this.mail;
    }

    public void setMail(java.lang.String mail)
    {
        this.mail = mail;
    }
    
    private java.util.Date dateEmb;

    /**
     * 
     */
    public java.util.Date getDateEmb()
    {
        return this.dateEmb;
    }

    public void setDateEmb(java.util.Date dateEmb)
    {
        this.dateEmb = dateEmb;
    }
    
    private java.util.Date dateQuit;

    /**
     * 
     */
    public java.util.Date getDateQuit()
    {
        return this.dateQuit;
    }

    public void setDateQuit(java.util.Date dateQuit)
    {
        this.dateQuit = dateQuit;
    }
    
    public boolean isleft() {
    	return (this.dateQuit != null && this.dateQuit.before(new Date()))? true :false;
    }
    
    private java.util.Collection<ma.nawarit.checker.compagnie.Role> roles = new HashSet<Role>();

    public java.util.Collection<ma.nawarit.checker.compagnie.Role> getRoles() {
		return roles;
	}

	public void setRoles(java.util.Collection<ma.nawarit.checker.compagnie.Role> roles) {
		this.roles = roles;
	}
	
	private ma.nawarit.checker.equipement.Unit unit = new ma.nawarit.checker.equipement.UnitImpl();

    /**
     * 
     */
    public ma.nawarit.checker.equipement.Unit getUnit()
    {
        return this.unit;
    }

    public void setUnit(ma.nawarit.checker.equipement.Unit unit)
    {
        this.unit = unit;
    }

    private java.lang.String login;

    /**
     * 
     */
    public java.lang.String getLogin()
    {
        return this.login;
    }

    public void setLogin(java.lang.String login)
    {
        this.login = login;
    }

    private java.lang.String password;

    /**
     * 
     */
    public java.lang.String getPassword()
    {
        return this.password;
    }

    public void setPassword(java.lang.String password)
    {
        this.password = password;
    }

    private java.lang.String matricule;

    /**
     * 
     */
    public java.lang.String getMatricule()
    {
        return this.matricule;
    }

    public void setMatricule(java.lang.String matricule)
    {
        this.matricule = matricule;
    }
    
    private java.lang.String badge;

    /**
     * 
     */
    public java.lang.String getBadge()
    {
        return this.badge;
    }

    public void setBadge(java.lang.String badge)
    {
        this.badge = badge;
    }
    
    private long nbCongeInitial;
    
    
    private java.lang.String modePaiement;

    /**
     * 
     */
    public java.lang.String getModePaiement()
    {
        return this.modePaiement;
    }

    public void setModePaiement(java.lang.String modePaiement)
    {
        this.modePaiement = modePaiement;
    }
    
    private java.lang.String typeContrat;
    
    /**
     * 
     */
    public java.lang.String getTypeContrat() {
		return typeContrat;
	}

	public void setTypeContrat(java.lang.String typeContrat) {
		this.typeContrat = typeContrat;
	}
	public boolean droitPaie;
	
	public boolean active;
	
	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	/**
	 * 
	 */
	private java.util.Collection cartes = new java.util.HashSet<Badge>();
    
    public void setCartes(java.util.Collection Cartes) {
    	this.cartes=Cartes;
    }
    
    public java.util.Collection getCartes() {
    	return this.cartes;
    }
	/**
	*
	*/
	public boolean isDroitPaie() {
	return droitPaie;
	}

	public void setDroitPaie(boolean droitPaie) {
	this.droitPaie = droitPaie;
	}


	private java.lang.String adressPostale;

    /**
     * 
     */
    public java.lang.String getAdressPostale()
    {
        return this.adressPostale;
    }

    public void setAdressPostale(java.lang.String adressPostale)
    {
        this.adressPostale = adressPostale;
    }
    
    private java.lang.String cinNum;

    /**
     * 
     */
    public java.lang.String getCinNum()
    {
        return this.cinNum;
    }

    public void setCinNum(java.lang.String cinNum)
    {
        this.cinNum = cinNum;
    }
    
    private java.lang.String cnssNum;

    /**
     * 
     */
    public java.lang.String getCnssNum()
    {
        return this.cnssNum;
    }

    public void setCnssNum(java.lang.String cnssNum)
    {
        this.cnssNum = cnssNum;
    }
    
    
    private java.util.Date dateNaiss;

    /**
     * 
     */
    public java.util.Date getDateNaiss()
    {
        return this.dateNaiss;
    }

    public void setDateNaiss(java.util.Date dateNaiss)
    {
        this.dateNaiss = dateNaiss;
    }
    
    private java.lang.String naissLoc;

    /**
     * 
     */
    public java.lang.String getNaissLoc()
    {
        return this.naissLoc;
    }

    public void setNaissLoc(java.lang.String naissLoc)
    {
        this.naissLoc = naissLoc;
    }
    
    private java.lang.String observations;

    /**
     * 
     */
    public java.lang.String getObservations()
    {
        return this.observations;
    }

    public void setObservations(java.lang.String observations)
    {
        this.observations = observations;
    }

    private ma.nawarit.checker.compagnie.ProfilMetier profilMetier = new ma.nawarit.checker.compagnie.ProfilMetierImpl();

    /**
     * 
     */
    public ma.nawarit.checker.compagnie.ProfilMetier getProfilMetier()
    {
        return this.profilMetier;
    }

    public void setProfilMetier(ma.nawarit.checker.compagnie.ProfilMetier profilMetier)
    {
        this.profilMetier = profilMetier;
    }

    private ma.nawarit.checker.compagnie.Noeud noeud;

    /**
     * 
     */
    public ma.nawarit.checker.compagnie.Noeud getNoeud()
    {
        return this.noeud;
    }

    public void setNoeud(ma.nawarit.checker.compagnie.Noeud noeud)
    {
        this.noeud = noeud;
    }

    private ma.nawarit.checker.compagnie.ProfilApp profilApp;
    
    /**
     * 
     */
    
    public ma.nawarit.checker.compagnie.ProfilApp getProfilApp()
    {
        if(profilApp == null && this.roles != null && !this.roles.isEmpty()) 
        	profilApp = roles.iterator().next().getProfilApp();     
    	return profilApp;
    }

    private ma.nawarit.checker.configuration.Planning planning;
    
    public ma.nawarit.checker.configuration.Planning getPlanning() {
    	return this.planning;
    }
    /**
     * 
     */
	public ma.nawarit.checker.configuration.Planning getPlanningAffected() {
		Planning pl = this.planning;
		if (this.planning == null) {
			if(this.getNoeud() != null)
    			pl = Utils.findPlanningByNode(null, this.getNoeud()) ;
		}
		if (pl == null)
			pl = new PlanningImpl();
//    	if (pl.getLibelle() == null)
//    		pl.setLibelle("");
    	return pl;
	}

	public void setPlanning(ma.nawarit.checker.configuration.Planning planning) {
		this.planning = planning;
	}
	
	private java.util.Collection<Except> exceptions = new java.util.HashSet<Except>();

    /**
     * 
     */
    public java.util.Collection<Except> getExceptions()
    {
        return this.exceptions;
    }

    public void setExceptions(java.util.Collection<Except> exceptions)
    {
        this.exceptions = exceptions;
    }
	
    private ma.nawarit.checker.compagnie.User supH;
	
    public ma.nawarit.checker.compagnie.User getSupH() {
		return supH;
	}

	public void setSupH(ma.nawarit.checker.compagnie.User supH) {
		this.supH = supH;
	}   
	 private java.util.Collection annomalies = new java.util.HashSet();

    /**
     * 
     */
    public java.util.Collection getAnnomalies()
    {
        return this.annomalies;
    }

    public void setAnnomalies(java.util.Collection annomalies)
    {
        this.annomalies = annomalies;
    }

    private java.util.Collection retards = new java.util.HashSet<Retard>();

    /**
     * 
     */
    public java.util.Collection getRetards()
    {
        return this.retards;
    }

    public void setRetards(java.util.Collection retards)
    {
        this.retards = retards;
    }

    private java.util.Collection absences = new java.util.HashSet<Absence>();

    /**
     * 
     */
    public java.util.Collection getAbsences()
    {
        return this.absences;
    }

    public void setAbsences(java.util.Collection absences)
    {
        this.absences = absences;
    }
    
    private java.util.Collection<ExceptionProfilMetier> exceptionProfilMetiers = new java.util.ArrayList<ExceptionProfilMetier>();

    /**
     * 
     */
    public java.util.Collection<ExceptionProfilMetier> getExceptionProfilMetiers()
    {
        return this.exceptionProfilMetiers;
    }

    public void setExceptionProfilMetiers(java.util.Collection<ExceptionProfilMetier> exceptionProfilMetiers)
    {
        this.exceptionProfilMetiers = exceptionProfilMetiers;
    }

    private java.util.Collection<ExceptionPosition> exceptionPositions = new java.util.ArrayList<ExceptionPosition>();

    /**
     * 
     */
    public java.util.Collection<ExceptionPosition> getExceptionPositions()
    {
        return this.exceptionPositions;
    }

    public void setExceptionPositions(java.util.Collection<ExceptionPosition> exceptionPositions)
    {
        this.exceptionPositions = exceptionPositions;
    }
    
    private java.util.Collection userBadgeTraces = new java.util.ArrayList();

    /**
     * 
     */
    public java.util.Collection getUserBadgeTraces()
    {
        return this.userBadgeTraces;
    }

    public void setUserBadgeTraces(java.util.Collection userBadgeTraces)
    {
        this.userBadgeTraces = userBadgeTraces;
    }
    private ma.nawarit.checker.security.ProfilAuthorize profilAuthorize;

    /**
     * 
     */
    public ma.nawarit.checker.security.ProfilAuthorize getProfilAuthorize()
    {
        return this.profilAuthorize;
    }

    public void setProfilAuthorize(ma.nawarit.checker.security.ProfilAuthorize profilAuthorize)
    {
        this.profilAuthorize = profilAuthorize;
    }
    
    private java.lang.String grade;
    
    public java.lang.String getGrade() {
		return grade;
	}

	public void setGrade(java.lang.String grade) {
		this.grade = grade;
	}

	public Object getMemberByType(Type t) {
    	if (t instanceof ProfilMetier)
    		return this.profilMetier;
    	return null;
    }
    /**
     * This entity does not have any identifiers
     * but since it extends the <code>ma.nawarit.checker.compagnie.PersonneImpl</code> class
     * it will simply delegate the call up there.
     *
     * @see ma.nawarit.checker.compagnie.Personne#equals(Object)
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof User))
        {
            return false;
        }
        final User that = (User)object;
        if (this.Id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * This entity does not have any identifiers
     * but since it extends the <code>ma.nawarit.checker.compagnie.PersonneImpl</code> class
     * it will simply delegate the call up there.
     *
     * @see ma.nawarit.checker.compagnie.Personne#hashCode()
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)Id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link ma.nawarit.checker.compagnie.User}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link ma.nawarit.checker.compagnie.User}.
         */
        public static ma.nawarit.checker.compagnie.User newInstance()
        {
            return new ma.nawarit.checker.compagnie.UserImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.compagnie.User}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.compagnie.User newInstance(long nbCongeInitial, java.util.Date dateEmb, java.util.Date dateQuit, java.lang.String login, java.lang.String password, java.lang.String matricule, java.lang.String nom, java.lang.String prenom, java.lang.String mail,java.lang.String modePaiement, 
        		java.lang.String adressPostale, java.lang.String cinNum, java.lang.String cnssNum, java.lang.String naissLoc, java.lang.String observations, java.util.Date dateNaiss,boolean droitPaie,	
        		ma.nawarit.checker.compagnie.ProfilMetier profilMetier, ma.nawarit.checker.compagnie.Noeud noeud, ma.nawarit.checker.compagnie.ProfilApp profilApp, ma.nawarit.checker.configuration.Planning planning, java.util.Collection<Except> exceptions, java.util.Collection<ExceptionProfilMetier> exceptionProfilMetiers, java.util.Collection<ExceptionPosition> exceptionPositions, java.util.Collection users, 
        		ma.nawarit.checker.compagnie.User user,java.util.Collection annomalies, java.util.Collection retards, java.util.Collection absences,
        		java.util.Collection userBadgeTraces)
        {
            final ma.nawarit.checker.compagnie.User entity = new ma.nawarit.checker.compagnie.UserImpl();
            entity.setDateEmb(dateEmb);
            entity.setDateQuit(dateQuit);
            entity.setLogin(login);
            entity.setPassword(password);
            entity.setMatricule(matricule);
            entity.setNom(nom);
            entity.setPrenom(prenom);
            entity.setMail(mail);
            entity.setProfilMetier(profilMetier);
            entity.setNoeud(noeud);
            entity.setPlanning(planning);
            entity.setExceptions(exceptions);
            entity.setExceptionProfilMetiers(exceptionProfilMetiers);
            entity.setExceptionPositions(exceptionPositions);
            entity.setSupH(user);
            entity.setAnnomalies(annomalies);
            entity.setRetards(retards);
            entity.setAbsences(absences);
            entity.setNbCongeInitial(nbCongeInitial);
            entity.setModePaiement(modePaiement);
            
            entity.setAdressPostale(adressPostale);
            entity.setCinNum(cinNum);
            entity.setCnssNum(cnssNum);
            entity.setDateNaiss(dateNaiss);
            entity.setNaissLoc(naissLoc);
            entity.setObservations(observations);
            entity.setDroitPaie(droitPaie);
            entity.setUserBadgeTraces(userBadgeTraces);

            
            return entity;
        }
    }

	public long getNbCongeInitial() {
		return nbCongeInitial;
	}

	public void setNbCongeInitial(long nbCongeInitial) {
		this.nbCongeInitial = nbCongeInitial;
	}

	public boolean isFlag() {
		return flag;
	}

	public void setFlag(boolean flag) {
		this.flag = flag;
	}



    
// HibernateEntity.vsl merge-point
}