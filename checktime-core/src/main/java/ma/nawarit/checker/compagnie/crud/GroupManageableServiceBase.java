/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

public final class GroupManageableServiceBase
    implements GroupManageableService
{
    private ma.nawarit.checker.compagnie.GroupDao dao;

    public void setDao(ma.nawarit.checker.compagnie.GroupDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.GroupDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.Group entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.Group load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.Group entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.Group entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.GroupManageableService.delete(ma.nawarit.checker.compagnie.Group entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
