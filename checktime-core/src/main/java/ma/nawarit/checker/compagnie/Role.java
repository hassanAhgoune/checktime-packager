/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.compagnie;

/**
 * 
 */
public abstract class Role
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 2295171155577950797L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

//    private java.lang.String name;
//
//    /**
//     * 
//     */
//    public java.lang.String getName()
//    {
//        return this.name;
//    }
//
//    public void setName(java.lang.String name)
//    {
//        this.name = name;
//    }

  private ma.nawarit.checker.compagnie.ProfilApp profilApp ;
    
        /**
         * 
         */
        public ma.nawarit.checker.compagnie.ProfilApp getProfilApp()
        {
            return this.profilApp;
        }
    
        public void setProfilApp(ma.nawarit.checker.compagnie.ProfilApp profilApp)
        {
            this.profilApp = profilApp;
        }
     /**
     * Returns <code>true</code> if the argument is an ProfilApp instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Role))
        {
            return false;
        }
        final Role that = (Role)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link Role}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link Role}.
         */
        public static Role newInstance()
        {
            return new ma.nawarit.checker.compagnie.RoleImpl();
        }
        /**
         * Constructs a new instance of {@link Role}.
         */
        public static Role newInstance(ProfilApp profil)
        {
        	 final Role entity = new RoleImpl();
             entity.setProfilApp(profil);
             return entity;
        }

       
    }
    
// HibernateEntity.vsl merge-point
}