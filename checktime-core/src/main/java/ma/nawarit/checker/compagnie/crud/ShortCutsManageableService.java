/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jul 09 16:20:33 GMT 20112 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

public interface ShortCutsManageableService
{
    public void create(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception;

    public ma.nawarit.checker.compagnie.ShortCuts load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception;

    public void delete(ma.nawarit.checker.compagnie.ShortCuts entity)
        throws Exception;
    
    public java.util.List readByUser(int userId)
    	throws Exception;
    public java.util.List readFreeShortCutsUser(int userId)
    	throws Exception;

}
