/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

import java.util.Date;
import java.util.List;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.CumulCriteria;

import ma.nawarit.checker.compagnie.LdapConstants;
import ma.nawarit.checker.compagnie.User;

public final class UserManageableServiceBase
    implements UserManageableService
{
    private ma.nawarit.checker.compagnie.UserDao dao;
    private ma.nawarit.checker.compagnieLdap.UserLdapDao ldapDao;
    private LdapConstants ldapConstants;

   
	public void setLdapDao(ma.nawarit.checker.compagnieLdap.UserLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}

	public void setDao(ma.nawarit.checker.compagnie.UserDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.UserDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.User entity)
        throws Exception
    {

       dao.create(entity);
    }
    public void createLdap(ma.nawarit.checker.compagnieLdap.UserLdap entity)
    throws Exception
	{
	   ldapDao.create(entity);
	}
    
    public ma.nawarit.checker.compagnie.User load(int id)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return (ma.nawarit.checker.compagnie.User)ldapDao.loadLdap(id);
    	else
    		return (ma.nawarit.checker.compagnie.User)dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.read(properties);
    	else
    		return dao.read(properties);
    }
    public java.util.List readDaoUser(java.util.Hashtable properties)
	    throws Exception
	{
			return dao.read(properties);
	}
    public java.util.List readAll()
        throws Exception
    {
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readAll();
    	else
    		return dao.readAll();
    }
    public ma.nawarit.checker.compagnieLdap.UserLdap create(
            java.lang.String uid,
            ma.nawarit.checker.compagnie.User user) throws Exception {
    	return (ma.nawarit.checker.compagnieLdap.UserLdap)ldapDao.create(uid,user);
    }
    public ma.nawarit.checker.compagnieLdap.UserLdap create(
            java.lang.String uid)  throws Exception
        {
    	return (ma.nawarit.checker.compagnieLdap.UserLdap)ldapDao.create(uid);
        }
    public void update(ma.nawarit.checker.compagnie.User entity)
	    throws Exception
	{
	
	    dao.update(entity);
	}

	public void update(List<ma.nawarit.checker.compagnie.User> entities) throws Exception {
		dao.update(entities);
	}
	
	public void delete(ma.nawarit.checker.compagnie.User entity)
	    throws Exception
	{
	    if (entity == null)
	    {
	        throw new IllegalArgumentException(
	            "ma.nawarit.checker.compagnie.crud.VisiteManageableService.delete(ma.nawarit.checker.compagnie.Visite entity) - 'entity' can not be null");
	    }
	
	    dao.remove(entity);
	}

	public java.sql.ResultSet executeQuery(User entity) throws Exception {
		return dao.executeQuery(entity);
		
	}

	public java.util.List find(User entity) throws Exception {
		return dao.find(entity);
		
	}

	public void delete(int id) throws Exception {
		dao.delete(id);
		
	} 
	public java.util.List countUserByNoeud(int noeudId)  throws Exception{
		return dao.countUserByNoeud(noeudId);
	}
	 public java.util.List readByCriteria(Date d1, Date d2)  throws Exception{
	    	return dao.readByCriteria(d1, d2);
	    }
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }    
    public double readByCriteria(CumulCriteria crit) throws Exception{
    	if(ldapConstants.isLdapEnabled())
    		return ldapDao.readByCriteria(crit);
    	else
    		return dao.readByCriteria(crit);
    }
    public java.util.List readUser(java.util.Hashtable properties)throws Exception{
    	
    		return ldapDao.readUser(properties);
    }
	public java.util.List countUsersByHierarchie(int hierarchieId)  throws Exception{
		return dao.countUsersByHierarchie(hierarchieId);
	}
	public java.util.List readUserByCriteria(CommonCriteria crit)   throws Exception{
		if(ldapConstants.isLdapEnabled())
    		return ldapDao.readUserByCriteria(crit);
    	else
    		return dao.readUserByCriteria(crit);
	}
	public User readUserByBadge(String badge){
		return dao.readUserByBadge(badge);
	}
	public java.util.List readUserListByOffset(int offset, int rowNumber){
		return dao.readUserListByOffset(offset,rowNumber);
	}
	public java.util.List readUserIdsList(){
		return dao.readUserIdsList();
	}
	public java.util.List getUsersByQuery(String query, List<String> values) {
		return dao.getUsersByQuery(query, values);
	}
	
	public List getUsersByMatricule(String Matricule) {
		// TODO Auto-generated method stub
		return dao.getUserByMatricule(Matricule);
	}
	
}
