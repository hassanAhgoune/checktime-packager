package ma.nawarit.checker.compagnie;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Restrictions;

public abstract class BadgeDaoBase extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
		implements ma.nawarit.checker.compagnie.BadgeDao {

	public ma.nawarit.checker.compagnie.Badge load(int id) {
		return (ma.nawarit.checker.compagnie.Badge) this.load(TRANSFORM_NONE, id);
	}

	public Object load(int transform, int id) {
		final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.compagnie.BadgeImpl.class,
				new java.lang.Integer(id));
		return transformEntity(transform, (ma.nawarit.checker.compagnie.Badge) entity);

	}

	public java.util.Collection loadAll() {
		return this.loadAll(TRANSFORM_NONE);
	}

	public java.util.Collection loadAll(final int transform) {
		final java.util.Collection results = this.getHibernateTemplate()
				.loadAll(ma.nawarit.checker.compagnie.VisiteImpl.class);
		this.transformEntities(transform, results);
		return results;
	}

	public ma.nawarit.checker.compagnie.Badge create(ma.nawarit.checker.compagnie.Badge badge) {
		return (ma.nawarit.checker.compagnie.Badge) this.create(TRANSFORM_NONE, badge);
	}

	public Object create(int transform, ma.nawarit.checker.compagnie.Badge badge) {
		if (badge == null) {
			throw new IllegalArgumentException("Badge.create - 'badge' can not be null");
		}
		this.getHibernateTemplate().save(badge);
		return this.transformEntity(transform, badge);
	}

	public java.util.Collection create(java.util.Collection entities) {
		return create(TRANSFORM_NONE, entities);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public java.util.Collection create(final int transform, final java.util.Collection entities) {
		if (entities == null) {
			throw new IllegalArgumentException("badge.create - 'entities' can not be null");
		}
		this.getHibernateTemplate().execute(new org.springframework.orm.hibernate3.HibernateCallback() {
			public java.lang.Object doInHibernate(org.hibernate.Session session)
					throws org.hibernate.HibernateException {
				for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();) {
					create(transform, (ma.nawarit.checker.compagnie.Badge) entityIterator.next());
				}
				return null;
			}
		});
		return entities;
	}

	public ma.nawarit.checker.compagnie.Badge create(java.lang.String Number,
			ma.nawarit.checker.compagnie.User Collaborateur) {
		return (ma.nawarit.checker.compagnie.Badge) this.create(TRANSFORM_NONE, Number, Collaborateur);
	}

	public ma.nawarit.checker.compagnie.Badge create(int transform, java.lang.String Number,
			ma.nawarit.checker.compagnie.User Collaborateur) {
		ma.nawarit.checker.compagnie.Badge entity = new ma.nawarit.checker.compagnie.BadgeImpl();
		entity.setNumber(Number);
		entity.setCollaborateur(Collaborateur);
		return (Badge) this.create(transform, entity);
	}

	public void update(ma.nawarit.checker.compagnie.Badge badge) {
		if (badge == null) {
			throw new IllegalArgumentException("Badge.update - 'badge' can not be null");
		}
		this.getHibernateTemplate().update(badge);
	}

	public void remove(ma.nawarit.checker.compagnie.Badge badge) {
		if (badge == null) {
			throw new IllegalArgumentException("Badge.remove - 'badge' can not be null");
		}
		this.getHibernateTemplate().delete(badge);
	}

	public void remove(int Id) {
		ma.nawarit.checker.compagnie.Badge entity = this.load(Id);
		if (entity != null) {
			this.remove(entity);
		}
	}

	public int getRowCount() {
		final Session session = getSession(false);
		String query = "SELECT COUNT(distinct(ab.id)) FROM Badge ab";
		SQLQuery q = session.createSQLQuery(query);
		BigInteger tmp = (BigInteger) q.uniqueResult();
		return tmp.intValue();
	};

	public java.util.List readByCollaborateursID(int id) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.BadgeImpl.class).add(Expression.eq("collaborateur.id", Integer.valueOf(id)));

			return criteria.list();
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	public java.util.List readByBadgeNUmber(String number) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.BadgeImpl.class).add(Expression.eq("number", number));

			return criteria.list();
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.compagnie.Badge entity) {
		java.lang.Object target = null;
		if (entity != null) {
			switch (transform) {
			case TRANSFORM_NONE: // fall-through
			default:
				target = entity;
			}
		}
		return target;
	}

	protected void transformEntities(final int transform, final java.util.Collection entities) {
		switch (transform) {
		case TRANSFORM_NONE: // fall-through
		default:
			// do nothing;
		}
	}

	public java.util.List read(java.util.Hashtable properties) {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.BadgeImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while (it.hasNext()) {
				String tmp = (String) it.next();
				criteria.add(Expression.eq(tmp, properties.get(tmp)));
			}
			return criteria.list();

		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}

	public java.util.List readAll() {
		final Session session = getSession(false);

		try {
			final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.BadgeImpl.class);

			return criteria.list();
		} catch (org.hibernate.HibernateException ex) {
			throw super.convertHibernateAccessException(ex);
		}
	}
}
