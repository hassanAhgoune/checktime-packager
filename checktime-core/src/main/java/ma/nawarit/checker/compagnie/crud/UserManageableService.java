/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

import java.util.Date;
import java.util.List;

import ma.nawarit.checker.compagnie.User;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.CumulCriteria;

public interface UserManageableService
{
    public void create(ma.nawarit.checker.compagnie.User entity)
        throws Exception;

    public ma.nawarit.checker.compagnie.User load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;
    public ma.nawarit.checker.compagnieLdap.UserLdap create(
            java.lang.String uid)  throws Exception;
    public java.util.List readAll()
        throws Exception;

    public void update(ma.nawarit.checker.compagnie.User entity)
	    throws Exception;

	public void update(List<ma.nawarit.checker.compagnie.User> entities)
			throws Exception;

	public void delete(ma.nawarit.checker.compagnie.User entity)
	    throws Exception;
	

	public java.sql.ResultSet executeQuery(ma.nawarit.checker.compagnie.User entity)
	    throws Exception;
	public java.util.List find(ma.nawarit.checker.compagnie.User entity)
    throws Exception;
	public void delete(int id)throws Exception;
	public java.util.List countUserByNoeud(int noeudId)  throws Exception;
	public java.util.List readByCriteria(Date d1, Date d2)  throws Exception;
	public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
	 public double readByCriteria(CumulCriteria crit) throws Exception;
	 public java.util.List readUser(java.util.Hashtable properties)throws Exception;
	 public java.util.List readDaoUser(java.util.Hashtable properties)
	    throws Exception;
	 public void createLdap(ma.nawarit.checker.compagnieLdap.UserLdap entity)
	    throws Exception;
	 public ma.nawarit.checker.compagnieLdap.UserLdap create(
	            java.lang.String uid,
	            ma.nawarit.checker.compagnie.User user) throws Exception ;
	public java.util.List countUsersByHierarchie(int hierarchieId)  throws Exception;
	 public java.util.List readUserByCriteria(CommonCriteria crit)   throws Exception;
	 public User readUserByBadge(String badge);
	 public java.util.List readUserListByOffset(int offset, int rowNumber);
	 public java.util.List readUserIdsList();
	 public java.util.List getUsersByQuery(String query, List<String> values);
	 public java.util.List getUsersByMatricule(String Matricule);
}
