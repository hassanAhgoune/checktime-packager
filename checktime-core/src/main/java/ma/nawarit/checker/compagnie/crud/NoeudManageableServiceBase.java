/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

import ma.nawarit.checker.compagnie.LdapConstants;

public final class NoeudManageableServiceBase
    implements NoeudManageableService
{
    private ma.nawarit.checker.compagnie.NoeudDao dao;
    private ma.nawarit.checker.compagnieLdap.NoeudLdapDao ldapDao;
    private LdapConstants ldapConstants;
    public void setDao(ma.nawarit.checker.compagnie.NoeudDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.NoeudDao getDao()
    {
        return this.dao;
    }

    public ma.nawarit.checker.compagnie.Noeud  create(ma.nawarit.checker.compagnie.Noeud entity)
        throws Exception
    {

       return dao.create(entity);
    }
    
    public java.util.Map readBackingLists()
	    throws Exception
	{
	    return getDao().readBackingLists();
	}
    public java.util.Map noeudsByCode()
	    throws Exception
	{
	    return getDao().noeudsByCode();
	}

    
     public ma.nawarit.checker.compagnie.Noeud load(int id)
        throws Exception
    {
    	 if(ldapConstants != null && ldapConstants.isLdapEnabled())
      		return ldapDao.load(id);
      	else
      		return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
    	 if(ldapConstants != null && ldapConstants.isLdapEnabled())
      		return ldapDao.read(properties);
      	else
      		return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
    	 if(ldapConstants != null && ldapConstants.isLdapEnabled())
      		return ldapDao.readAll();
      	else
      		return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.Noeud entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.Noeud entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.NoeudManageableService.delete(ma.nawarit.checker.compagnie.Noeud entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

	public ma.nawarit.checker.compagnieLdap.NoeudLdapDao getLdapDao() {
		return ldapDao;
	}

	public void setLdapDao(
			ma.nawarit.checker.compagnieLdap.NoeudLdapDao ldapDao) {
		this.ldapDao = ldapDao;
	}

	public LdapConstants getLdapConstants() {
		return ldapConstants;
	}

	public void setLdapConstants(LdapConstants ldapConstants) {
		this.ldapConstants = ldapConstants;
	}


    

}
