/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

import ma.nawarit.checker.compagnie.Visiteur;

public final class VisiteurManageableServiceBase
    implements VisiteurManageableService
{
    private ma.nawarit.checker.compagnie.VisiteurDao dao;

    public void setDao(ma.nawarit.checker.compagnie.VisiteurDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.VisiteurDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.Visiteur entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.Visiteur load(int id)
        throws Exception
    {
        return  (ma.nawarit.checker.compagnie.Visiteur)dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }



    

}
