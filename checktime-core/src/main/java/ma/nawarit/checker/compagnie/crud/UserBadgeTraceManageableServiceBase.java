/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>COPROP<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Dec 15 11:27:11 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author team
 */
package ma.nawarit.checker.compagnie.crud;

public final class UserBadgeTraceManageableServiceBase
    implements UserBadgeTraceManageableService
{
    private ma.nawarit.checker.compagnie.UserBadgeTraceDao dao;

    public void setDao(ma.nawarit.checker.compagnie.UserBadgeTraceDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.UserBadgeTraceDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.UserBadgeTrace load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.UserBadgeTraceManageableService.delete(ma.nawarit.checker.compagnie.UserBadgeTrace entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
