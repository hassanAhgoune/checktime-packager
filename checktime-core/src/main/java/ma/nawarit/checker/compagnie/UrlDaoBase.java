/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>Checktime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jul 25 12:22:29 GMT 2012 </DD>
 * </DL>
 * @since 01/04/2012
 * @version 1.0
 * @author NAWAR TEAM
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.compagnie;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.compagnie.Url</code>.
 * </p>
 *
 * @see ma.nawarit.checker.compagnie.Url
 */
public abstract class UrlDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.compagnie.UrlDao
{

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.compagnie.UrlImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.compagnie.Url)entity);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#load(int)
     */
    public ma.nawarit.checker.compagnie.Url load(int id)
    {
        return (ma.nawarit.checker.compagnie.Url)this.load(TRANSFORM_NONE, id);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.compagnie.UrlImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(ma.nawarit.checker.compagnie.Url)
     */
    public ma.nawarit.checker.compagnie.Url create(ma.nawarit.checker.compagnie.Url url)
    {
        return (ma.nawarit.checker.compagnie.Url)this.create(TRANSFORM_NONE, url);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(int transform, ma.nawarit.checker.compagnie.Url)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.compagnie.Url url)
    {
        if (url == null)
        {
            throw new IllegalArgumentException(
                "Url.create - 'url' can not be null");
        }
        this.getHibernateTemplate().save(url);
        return this.transformEntity(transform, url);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(int, java.util.Collection)
     */
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Url.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.compagnie.Url)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(java.lang.String)
     */
    public ma.nawarit.checker.compagnie.Url create(
        java.lang.String name)
    {
        return (ma.nawarit.checker.compagnie.Url)this.create(TRANSFORM_NONE, name);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#create(int, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String name)
    {
        ma.nawarit.checker.compagnie.Url entity = new ma.nawarit.checker.compagnie.UrlImpl();
        entity.setName(name);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#update(ma.nawarit.checker.compagnie.Url)
     */
    public void update(ma.nawarit.checker.compagnie.Url url)
    {
        if (url == null)
        {
            throw new IllegalArgumentException(
                "Url.update - 'url' can not be null");
        }
        this.getHibernateTemplate().update(url);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#update(java.util.Collection)
     */
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Url.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.compagnie.Url)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#remove(ma.nawarit.checker.compagnie.Url)
     */
    public void remove(ma.nawarit.checker.compagnie.Url url)
    {
        if (url == null)
        {
            throw new IllegalArgumentException(
                "Url.remove - 'url' can not be null");
        }
        this.getHibernateTemplate().delete(url);
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.compagnie.Url entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.compagnie.UrlDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Url.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.compagnie.UrlDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.compagnie.UrlDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.compagnie.Url entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.compagnie.Url)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.compagnie.UrlDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.compagnie.Url)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.UrlImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.UrlImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }



}