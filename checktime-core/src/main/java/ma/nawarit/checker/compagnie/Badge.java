/**
 * 
 */
package ma.nawarit.checker.compagnie;

/**
 * @author AYOUB-PC
 *
 */
public abstract class Badge implements java.io.Serializable{


	/**
	 * 
	 */
	private static final long serialVersionUID = -7920161989437844706L;

	/**
	 * 
	 */
	private int id;
	private java.lang.String number;
	private ma.nawarit.checker.compagnie.User collaborateur = new ma.nawarit.checker.compagnie.UserImpl();
	
	public Badge() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public java.lang.String getNumber() {
		return number;
	}

	public void setNumber(java.lang.String number) {
		this.number = number;
	}

	public ma.nawarit.checker.compagnie.User getCollaborateur() {
		return collaborateur;
	}

	public void setCollaborateur(ma.nawarit.checker.compagnie.User collaborateur) {
		this.collaborateur = collaborateur;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Badge other = (Badge) obj;
		if (id != other.id)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}
	
	 public static final class Factory
	    {
	        public static ma.nawarit.checker.compagnie.Badge newInstance()
	        {
	            return new ma.nawarit.checker.compagnie.BadgeImpl();
	        }
	        
	        public static ma.nawarit.checker.compagnie.Badge newInstance(java.lang.String Number, ma.nawarit.checker.compagnie.User collaborateur)
	        {
	            final ma.nawarit.checker.compagnie.Badge entity = new ma.nawarit.checker.compagnie.BadgeImpl();
	            entity.setNumber(Number);
	            entity.setCollaborateur(collaborateur);
	            return entity;
	        }
	    }
}
