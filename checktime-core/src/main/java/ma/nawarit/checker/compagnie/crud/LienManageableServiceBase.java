/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>Checktime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jul 25 12:22:29 GMT 2012 </DD>
 * </DL>
 * @since 01/04/2012
 * @version 1.0
 * @author NAWAR TEAM
 */
package ma.nawarit.checker.compagnie.crud;

public final class LienManageableServiceBase
    implements LienManageableService
{
    private ma.nawarit.checker.compagnie.LienDao dao;

    public void setDao(ma.nawarit.checker.compagnie.LienDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.compagnie.LienDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.compagnie.Lien entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.compagnie.Lien load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.compagnie.Lien entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.compagnie.Lien entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.compagnie.crud.LienManageableService.delete(ma.nawarit.checker.compagnie.Lien entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
