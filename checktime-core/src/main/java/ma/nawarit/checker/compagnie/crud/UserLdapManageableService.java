/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Feb 11 09:59:41 GMT 2009 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.compagnie.crud;

public interface UserLdapManageableService
{
    public void create(ma.nawarit.checker.compagnieLdap.UserLdap entity)
        throws Exception;

    public ma.nawarit.checker.compagnieLdap.UserLdap load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.compagnieLdap.UserLdap entity)
        throws Exception;

    public void delete(ma.nawarit.checker.compagnieLdap.UserLdap entity)
        throws Exception;
    public java.util.List readUser(java.util.Hashtable properties)
    	throws Exception;
}
