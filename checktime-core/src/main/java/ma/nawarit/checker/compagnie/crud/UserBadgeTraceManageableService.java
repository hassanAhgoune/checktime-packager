/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>COPROP<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Dec 15 11:27:11 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author team
 */
package ma.nawarit.checker.compagnie.crud;

public interface UserBadgeTraceManageableService
{
    public void create(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception;

    public ma.nawarit.checker.compagnie.UserBadgeTrace load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception;

    public void delete(ma.nawarit.checker.compagnie.UserBadgeTrace entity)
        throws Exception;

}
