/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Apr 07 10:16:29 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.compagnie;
import java.util.Iterator;
import java.util.Set;

import ma.nawarit.checker.suivi.Except;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.compagnie.Noeud</code>.
 * </p>
 *
 * @see ma.nawarit.checker.compagnie.Noeud
 */
public abstract class NoeudDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.compagnie.NoeudDao
{

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int Id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.compagnie.NoeudImpl.class, new java.lang.Integer(Id));
        return transformEntity(transform, (ma.nawarit.checker.compagnie.Noeud)entity);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#load(int)
     */
    public ma.nawarit.checker.compagnie.Noeud load(int Id)
    {
        return (ma.nawarit.checker.compagnie.Noeud)this.load(TRANSFORM_NONE, Id);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.compagnie.NoeudImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(ma.nawarit.checker.compagnie.Noeud)
     */
    public ma.nawarit.checker.compagnie.Noeud create(ma.nawarit.checker.compagnie.Noeud noeud)
    {
        return (ma.nawarit.checker.compagnie.Noeud)this.create(TRANSFORM_NONE, noeud);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(int transform, ma.nawarit.checker.compagnie.Noeud)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.compagnie.Noeud noeud)
    {
        if (noeud == null)
        {
            throw new IllegalArgumentException(
                "Noeud.create - 'noeud' can not be null");
        }
        this.getHibernateTemplate().save(noeud);
        return this.transformEntity(transform, noeud);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Noeud.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.compagnie.Noeud)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(java.lang.String, java.lang.String)
     */
    public ma.nawarit.checker.compagnie.Noeud create(
        java.lang.String code,
        java.lang.String libelle)
    {
        return (ma.nawarit.checker.compagnie.Noeud)this.create(TRANSFORM_NONE, code, libelle);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(int, java.lang.String, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String code,
        java.lang.String libelle)
    {
        ma.nawarit.checker.compagnie.Noeud entity = new ma.nawarit.checker.compagnie.NoeudImpl();
        entity.setCode(code);
        entity.setLibelle(libelle);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(java.lang.String, java.lang.String, ma.nawarit.checker.compagnie.Niveau, ma.nawarit.checker.compagnie.Noeud)
     */
    public ma.nawarit.checker.compagnie.Noeud create(
        java.lang.String code,
        java.lang.String libelle,
        ma.nawarit.checker.compagnie.Noeud noeud,
        ma.nawarit.checker.configuration.PlanningImpl planning,
        java.util.Collection<Except> exceptions)
    {
        return (ma.nawarit.checker.compagnie.Noeud)this.create(TRANSFORM_NONE, code, libelle, noeud, planning, exceptions);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#create(int, java.lang.String, java.lang.String, ma.nawarit.checker.compagnie.Niveau, ma.nawarit.checker.compagnie.Noeud)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String code,
        java.lang.String libelle,
        ma.nawarit.checker.compagnie.Noeud noeud,
        ma.nawarit.checker.configuration.PlanningImpl planning,
        java.util.Collection<Except> exceptions)
    {
        ma.nawarit.checker.compagnie.Noeud entity = new ma.nawarit.checker.compagnie.NoeudImpl();
        entity.setCode(code);
        entity.setLibelle(libelle);
        entity.setNoeud(noeud);
        entity.setPlanning(planning);
        entity.setExceptions(exceptions);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#update(ma.nawarit.checker.compagnie.Noeud)
     */
    public void update(ma.nawarit.checker.compagnie.Noeud noeud)
    {
        if (noeud == null)
        {
            throw new IllegalArgumentException(
                "Noeud.update - 'noeud' can not be null");
        }
        this.getHibernateTemplate().update(noeud);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Noeud.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.compagnie.Noeud)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#remove(ma.nawarit.checker.compagnie.Noeud)
     */
    public void remove(ma.nawarit.checker.compagnie.Noeud noeud)
    {
        if (noeud == null)
        {
            throw new IllegalArgumentException(
                "Noeud.remove - 'noeud' can not be null");
        }
        this.getHibernateTemplate().delete(noeud);
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#remove(int)
     */
    public void remove(int Id)
    {
        ma.nawarit.checker.compagnie.Noeud entity = this.load(Id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.compagnie.NoeudDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Noeud.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.compagnie.NoeudDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.compagnie.NoeudDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.compagnie.Noeud entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.compagnie.Noeud)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.compagnie.NoeudDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.compagnie.Noeud)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.NoeudImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.compagnie.NoeudImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    
    public java.util.Map readBackingLists()
    {
        final java.util.Map lists = new java.util.HashMap();
        final Session session = this.getSession();

        try
        {
            lists.put("noeuds", session.createQuery("select item.id from ma.nawarit.checker.compagnie.Noeud item order by item.id").list());
        }
        catch (HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
        return lists;
    }

    public java.util.Map noeudsByCode()
    {
        final java.util.Map map = new java.util.HashMap();
        final Session session = this.getSession();

        try
        {
            java.util.List<Noeud> neouds = session.createQuery("select item from ma.nawarit.checker.compagnie.Noeud item").list();
            for(Noeud noeud: neouds){
                map.put(noeud.getCode(), noeud);
            }
        }
        catch (HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
        return map;
    }



}