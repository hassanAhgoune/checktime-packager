/*
 * Created on 19 juil. 2011
 */
package ma.nawarit.checker.exception;

/**
 * @author Jumper
 */
public class JoTestException extends Exception {

	/**
	 * 
	 */
	public JoTestException() {
		super();
	}

	/**
	 * @param arg0
	 */
	public JoTestException(String msg) {
		super(msg);
	}

	/**
	 * @param arg0
	 */
	public JoTestException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param msg
	 * @param arg1
	 */
	public JoTestException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
