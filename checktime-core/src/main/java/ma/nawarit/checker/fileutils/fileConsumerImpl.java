package ma.nawarit.checker.fileutils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class fileConsumerImpl implements ma.nawarit.checker.fileutils.fileConsumer {

	public ArrayList<String> generateListFromCSVfile(String filePath) {
		Scanner s;
		ArrayList<String> list = new ArrayList<String>();
		try {
			s = new Scanner(new File(filePath));
			int cnt = 0;
			while (s.hasNext()){
				if (cnt != 0)
				    list.add(s.next());
				else
					s.next();
				cnt++;
			}
			s.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return list;
	}
}
