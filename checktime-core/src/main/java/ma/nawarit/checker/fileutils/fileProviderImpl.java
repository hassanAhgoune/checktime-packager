package ma.nawarit.checker.fileutils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class fileProviderImpl implements ma.nawarit.checker.fileutils.fileProvider {

	public fileProviderImpl() {
		
	}
	
	public void generateCSVfile(String filePath, List<String> lines) {
		Path file = Paths.get(filePath);
		try {
			Files.write(file, lines, Charset.forName("UTF-8"),StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
