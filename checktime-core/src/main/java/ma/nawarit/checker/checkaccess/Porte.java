package ma.nawarit.checker.checkaccess;

/**
 * 
 */
public abstract class Porte
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 8628735853444563938L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    private float posx;

    /**
     * 
     */
    public float getPosx()
    {
        return this.posx;
    }

    public void setPosx(float posx)
    {
        this.posx = posx;
    }

    private float posy;

    /**
     * 
     */
    public float getPosy()
    {
        return this.posy;
    }

    public void setPosy(float posy)
    {
        this.posy = posy;
    }
   
    private ma.nawarit.checker.equipement.Terminal terminal = new ma.nawarit.checker.equipement.TerminalImpl();

    /**
     * 
     */
    public ma.nawarit.checker.equipement.Terminal getTerminal()
    {
        return this.terminal;
    }

    public void setTerminal(ma.nawarit.checker.equipement.Terminal terminal)
    {
        this.terminal=terminal;
    }

    /**
     * Returns <code>true</code> if the argument is an Terminal instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof Porte))
        {
            return false;
        }
        final Porte that = (Porte)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }
    
// HibernateEntity.vsl merge-point
}