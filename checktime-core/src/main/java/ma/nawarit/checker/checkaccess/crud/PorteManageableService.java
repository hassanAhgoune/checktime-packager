package ma.nawarit.checker.checkaccess.crud;

import java.util.List;

public interface PorteManageableService
{
    public void create(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception;

    public ma.nawarit.checker.checkaccess.Porte load(int id)
        throws Exception;
    
    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception;

    public void delete(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception;

}
