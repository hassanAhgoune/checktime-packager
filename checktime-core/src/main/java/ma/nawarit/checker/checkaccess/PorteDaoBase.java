package ma.nawarit.checker.checkaccess;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ma.nawarit.checker.compagnie.User;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.equipement.Terminal</code>.
 * </p>
 *
 * @see ma.nawarit.checker.equipement.Terminal
 */
public abstract class PorteDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.checkaccess.PorteDao
{

    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.checkaccess.Porte.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.checkaccess.Porte)entity);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#load(int)
     */
    public ma.nawarit.checker.checkaccess.Porte load(int id)
    {
        return (ma.nawarit.checker.checkaccess.Porte)this.load(TRANSFORM_NONE, id);
    }




    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#create(ma.nawarit.checker.equipement.Terminal)
     */
    public ma.nawarit.checker.checkaccess.Porte create(ma.nawarit.checker.checkaccess.Porte porte)
    {
        return (ma.nawarit.checker.checkaccess.Porte)this.create(TRANSFORM_NONE, porte);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#create(int transform, ma.nawarit.checker.equipement.Terminal)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.checkaccess.Porte porte)
    {
        if (porte == null)
        {
            throw new IllegalArgumentException(
                "Terminal.create - 'terminal' can not be null");
        }
        this.getHibernateTemplate().save(porte);
        return this.transformEntity(transform, porte);
    }

    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#update(ma.nawarit.checker.equipement.Terminal)
     */
    public void update(ma.nawarit.checker.checkaccess.Porte porte)
    {
        if (porte == null)
        {
            throw new IllegalArgumentException(
                "Terminal.update - 'terminal' can not be null");
        }
        this.getHibernateTemplate().update(porte);
    }

   
    /**
     * @see ma.nawarit.checker.equipement.TerminalDao#remove(ma.nawarit.checker.equipement.Terminal)
     */
    public void remove(ma.nawarit.checker.checkaccess.Porte porte)
    {
        if (porte == null)
        {
            throw new IllegalArgumentException(
                "Terminal.remove - 'terminal' can not be null");
        }
        this.getHibernateTemplate().delete(porte);
    }

    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.equipement.TerminalDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.equipement.TerminalDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.checkaccess.Porte porte)
    {
        java.lang.Object target = null;
        if (porte != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = porte;
            }
        }
        return target;
    }

    



    public java.util.List readAll()
    {
    	
        final Session session = getSession(false);
        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.checkaccess.PorteImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }


}