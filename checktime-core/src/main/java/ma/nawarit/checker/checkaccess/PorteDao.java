package ma.nawarit.checker.checkaccess;

import java.util.List;

/**
 * @see ma.nawarit.checker.equipement.Terminal
 */
public interface PorteDao
{
    /**
     * This constant is used as a transformation flag; entities can be converted automatically into value objects
     * or other types, different methods in a class implementing this interface support this feature: look for
     * an <code>int</code> parameter called <code>transform</code>.
     * <p/>
     * This specific flag denotes no transformation will occur.
     */
    public final static int TRANSFORM_NONE = 0;

    /**
     * Loads an instance of ma.nawarit.checker.equipement.Terminal from the persistent store.
     */
    public ma.nawarit.checker.checkaccess.Porte load(int id);

    /**
     * Creates an instance of ma.nawarit.checker.equipement.Terminal and adds it to the persistent store.
     */
    public ma.nawarit.checker.checkaccess.Porte create(ma.nawarit.checker.checkaccess.Porte porte);

    
    /**
     * Updates the <code>terminal</code> instance in the persistent store.
     */
    public void update(ma.nawarit.checker.checkaccess.Porte porte);

    /**
     * Removes the instance of ma.nawarit.checker.equipement.Terminal from the persistent store.
     */
    public void remove(ma.nawarit.checker.checkaccess.Porte porte);


    
    public java.util.List readAll();
}
