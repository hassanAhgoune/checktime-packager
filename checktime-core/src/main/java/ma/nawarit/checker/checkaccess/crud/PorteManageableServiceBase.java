/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Apr 04 15:12:01 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.checkaccess.crud;

import java.util.List;

public final class PorteManageableServiceBase
    implements PorteManageableService
{
    private ma.nawarit.checker.checkaccess.PorteDao dao;

    public void setDao(ma.nawarit.checker.checkaccess.PorteDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.checkaccess.PorteDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.checkaccess.Porte load(int id)
        throws Exception
    {
        return dao.load(id);
    }
   

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.checkaccess.Porte entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.equipement.crud.TerminalManageableService.delete(ma.nawarit.checker.equipement.Terminal entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    
    }

}
