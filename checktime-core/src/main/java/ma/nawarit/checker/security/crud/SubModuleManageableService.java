/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jul 27 16:53:27 GMT 2011 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.security.crud;

public interface SubModuleManageableService
{
    public void create(ma.nawarit.checker.security.SubModule entity)
        throws Exception;

    public ma.nawarit.checker.security.SubModule load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.security.SubModule entity)
        throws Exception;

    public void delete(ma.nawarit.checker.security.SubModule entity)
        throws Exception;

}
