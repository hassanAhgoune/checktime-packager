/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Jul 28 12:06:04 GMT 2011 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.security;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.security.Authorization</code>.
 * </p>
 *
 * @see ma.nawarit.checker.security.Authorization
 */
public abstract class AuthorizationDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.security.AuthorizationDao
{

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.security.AuthorizationImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.security.Authorization)entity);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#load(int)
     */
    public ma.nawarit.checker.security.Authorization load(int id)
    {
        return (ma.nawarit.checker.security.Authorization)this.load(TRANSFORM_NONE, id);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.security.AuthorizationImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(ma.nawarit.checker.security.Authorization)
     */
    public ma.nawarit.checker.security.Authorization create(ma.nawarit.checker.security.Authorization authorization)
    {
        return (ma.nawarit.checker.security.Authorization)this.create(TRANSFORM_NONE, authorization);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(int transform, ma.nawarit.checker.security.Authorization)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.security.Authorization authorization)
    {
        if (authorization == null)
        {
            throw new IllegalArgumentException(
                "Authorization.create - 'authorization' can not be null");
        }
        this.getHibernateTemplate().save(authorization);
        return this.transformEntity(transform, authorization);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Authorization.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.security.Authorization)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)
     */
    public ma.nawarit.checker.security.Authorization create(
        boolean addEnabled,
        boolean editEnabled,
        boolean removeEnabled,
        boolean detailEnabled,
        boolean exportPdfEnabled,
        boolean exportXlsEnabled,
        boolean exportCsvEnabled,
        boolean importEnabled)
    {
        return (ma.nawarit.checker.security.Authorization)this.create(TRANSFORM_NONE, addEnabled, editEnabled, removeEnabled, detailEnabled, exportPdfEnabled, exportXlsEnabled, exportCsvEnabled, importEnabled);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(int, boolean, boolean, boolean, boolean, boolean, boolean, boolean, boolean)
     */
    public java.lang.Object create(
        final int transform,
        boolean addEnabled,
        boolean editEnabled,
        boolean removeEnabled,
        boolean detailEnabled,
        boolean exportPdfEnabled,
        boolean exportXlsEnabled,
        boolean exportCsvEnabled,
        boolean importEnabled)
    {
        ma.nawarit.checker.security.Authorization entity = new ma.nawarit.checker.security.AuthorizationImpl();
        entity.setAddEnabled(addEnabled);
        entity.setEditEnabled(editEnabled);
        entity.setRemoveEnabled(removeEnabled);
        entity.setDetailEnabled(detailEnabled);
        entity.setExportPdfEnabled(exportPdfEnabled);
        entity.setExportXlsEnabled(exportXlsEnabled);
        entity.setExportCsvEnabled(exportCsvEnabled);
        entity.setImportEnabled(importEnabled);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(boolean, boolean, boolean, boolean, boolean, boolean, boolean, ma.nawarit.checker.security.ProfilAuthorize, boolean, ma.nawarit.checker.security.SubModule)
     */
    public ma.nawarit.checker.security.Authorization create(
        boolean addEnabled,
        boolean detailEnabled,
        boolean editEnabled,
        boolean exportCsvEnabled,
        boolean exportPdfEnabled,
        boolean exportXlsEnabled,
        boolean importEnabled,
        ma.nawarit.checker.security.ProfilAuthorize profilAuthorize,
        boolean removeEnabled,
        ma.nawarit.checker.security.SubModule subModule)
    {
        return (ma.nawarit.checker.security.Authorization)this.create(TRANSFORM_NONE, addEnabled, detailEnabled, editEnabled, exportCsvEnabled, exportPdfEnabled, exportXlsEnabled, importEnabled, profilAuthorize, removeEnabled, subModule);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#create(int, boolean, boolean, boolean, boolean, boolean, boolean, boolean, ma.nawarit.checker.security.ProfilAuthorize, boolean, ma.nawarit.checker.security.SubModule)
     */
    public java.lang.Object create(
        final int transform,
        boolean addEnabled,
        boolean detailEnabled,
        boolean editEnabled,
        boolean exportCsvEnabled,
        boolean exportPdfEnabled,
        boolean exportXlsEnabled,
        boolean importEnabled,
        ma.nawarit.checker.security.ProfilAuthorize profilAuthorize,
        boolean removeEnabled,
        ma.nawarit.checker.security.SubModule subModule)
    {
        ma.nawarit.checker.security.Authorization entity = new ma.nawarit.checker.security.AuthorizationImpl();
        entity.setAddEnabled(addEnabled);
        entity.setDetailEnabled(detailEnabled);
        entity.setEditEnabled(editEnabled);
        entity.setExportCsvEnabled(exportCsvEnabled);
        entity.setExportPdfEnabled(exportPdfEnabled);
        entity.setExportXlsEnabled(exportXlsEnabled);
        entity.setImportEnabled(importEnabled);
        entity.setProfilAuthorize(profilAuthorize);
        entity.setRemoveEnabled(removeEnabled);
        entity.setSubModule(subModule);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#update(ma.nawarit.checker.security.Authorization)
     */
    public void update(ma.nawarit.checker.security.Authorization authorization)
    {
        if (authorization == null)
        {
            throw new IllegalArgumentException(
                "Authorization.update - 'authorization' can not be null");
        }
        this.getHibernateTemplate().update(authorization);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Authorization.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.security.Authorization)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#remove(ma.nawarit.checker.security.Authorization)
     */
    public void remove(ma.nawarit.checker.security.Authorization authorization)
    {
        if (authorization == null)
        {
            throw new IllegalArgumentException(
                "Authorization.remove - 'authorization' can not be null");
        }
        this.getHibernateTemplate().delete(authorization);
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.security.Authorization entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.security.AuthorizationDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Authorization.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.security.AuthorizationDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.security.AuthorizationDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.security.Authorization entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.security.Authorization)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.security.AuthorizationDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.security.Authorization)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.security.AuthorizationImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.security.AuthorizationImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }



}