/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Jul 28 12:06:04 GMT 2011 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.security.crud;

public interface ProfilAuthorizeManageableService
{
    public void create(ma.nawarit.checker.security.ProfilAuthorize entity)
        throws Exception;

    public ma.nawarit.checker.security.ProfilAuthorize load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.security.ProfilAuthorize entity)
        throws Exception;

    public void delete(ma.nawarit.checker.security.ProfilAuthorize entity)
        throws Exception;

}
