/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Jul 27 16:53:27 GMT 2011 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.security.crud;

public final class UrlPatternManageableServiceBase
    implements UrlPatternManageableService
{
    private ma.nawarit.checker.security.UrlPatternDao dao;

    public void setDao(ma.nawarit.checker.security.UrlPatternDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.security.UrlPatternDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.security.UrlPattern entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.security.UrlPattern load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.security.UrlPattern entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.security.UrlPattern entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.security.crud.UrlPatternManageableService.delete(ma.nawarit.checker.security.UrlPattern entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
