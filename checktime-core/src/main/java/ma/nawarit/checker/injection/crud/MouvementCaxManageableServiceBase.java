/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Sep 01 10:41:53 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.injection.crud;

import java.util.Collection;

import org.andromda.spring.CommonCriteria;

public final class MouvementCaxManageableServiceBase
    implements MouvementCaxManageableService
{
    private ma.nawarit.checker.injection.MouvementCaxDao dao;

    public void setDao(ma.nawarit.checker.injection.MouvementCaxDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.injection.MouvementCaxDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.injection.MouvementCax load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.injection.crud.MouvementCaxManageableService.delete(ma.nawarit.checker.injection.MouvementCax entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

    public void createAll(Collection<ma.nawarit.checker.injection.MouvementCax> entities)
    throws Exception
    {

    	dao.create(entities);
    }
    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	return dao.readByCriteria(crit);
    }

}
