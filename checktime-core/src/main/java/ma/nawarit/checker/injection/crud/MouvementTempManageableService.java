/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Jul 17 09:54:04 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.injection.crud;

public interface MouvementTempManageableService
{
    public ma.nawarit.checker.injection.MouvementTemp create(ma.nawarit.checker.injection.MouvementTemp entity)
        throws Exception;

    public ma.nawarit.checker.injection.MouvementTemp load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.injection.MouvementTemp entity)
        throws Exception;

    public void delete(ma.nawarit.checker.injection.MouvementTemp entity)
        throws Exception;

}
