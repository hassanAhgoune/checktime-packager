/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Thu Jul 17 09:54:04 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.injection.crud;

public final class MouvementTempManageableServiceBase
    implements MouvementTempManageableService
{
    private ma.nawarit.checker.injection.MouvementTempDao dao;

    public void setDao(ma.nawarit.checker.injection.MouvementTempDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.injection.MouvementTempDao getDao()
    {
        return this.dao;
    }
    
    public ma.nawarit.checker.injection.MouvementTemp create(ma.nawarit.checker.injection.MouvementTemp entity)
	    throws Exception
	{
	
	   return dao.create(entity);
	}

    
     public ma.nawarit.checker.injection.MouvementTemp load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.injection.MouvementTemp entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.injection.MouvementTemp entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.injection.crud.MouvementTempManageableService.delete(ma.nawarit.checker.injection.MouvementTemp entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
