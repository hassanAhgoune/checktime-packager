/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon May 19 17:14:31 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.injection.crud;

import java.util.Collection;
import java.util.List;

import ma.nawarit.checker.compagnie.UserDao;

import org.andromda.spring.CommonCriteria;

public final class MouvementManageableServiceBase
    implements MouvementManageableService
{
	public UserDao usDao;
    private ma.nawarit.checker.injection.MouvementDao dao;

    public void setDao(ma.nawarit.checker.injection.MouvementDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.injection.MouvementDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.injection.Mouvement entity)
        throws Exception
    {

       dao.create(entity);
    }
    public void createAll(Collection<ma.nawarit.checker.injection.Mouvement> entities)
    throws Exception
{

   dao.create(entities);
}

    
     public ma.nawarit.checker.injection.Mouvement load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }

    public java.util.List readByBadgeAndDate(String badge,java.util.Date d) throws Exception
    {
    	return dao.readByBadgeAndDate(badge, d);
    }
    public java.util.List readByMatriculeAndDate(String badge,java.util.Date d) throws Exception
    {
    	return dao.readByMatriculeAndDate(badge, d);
    }

    public void update(ma.nawarit.checker.injection.Mouvement entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.injection.Mouvement entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.injection.crud.MouvementManageableService.delete(ma.nawarit.checker.injection.Mouvement entity) - 'entity' can not be null");
        }
        dao.remove(entity);
    }

    public java.util.List readByCriteria(CommonCriteria crit)  throws Exception{
    	return dao.readByCriteria(crit);
    }
    
   
    public java.util.List readByCriteria(org.andromda.spring.MvmntCriteria crit) throws Exception {
    	return dao.readByCriteria(crit);
    }
    
    public int countWorkedDaysByCriteria(CommonCriteria crit) throws Exception {
    	return dao.countWorkedDaysByCriteria(crit);
    }
    
    public java.util.List readByZones(List<Integer> indexs) throws Exception {
    	return dao.readByZones(indexs);
    }
    public void removeByCriteria(CommonCriteria crit) throws Exception{
    	dao.removeByCriteria(crit);
    }
    
    
}
