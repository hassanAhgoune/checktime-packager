/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Sep 01 10:41:53 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.injection.crud;

import java.util.Collection;

import org.andromda.spring.CommonCriteria;

public interface MouvementCaxManageableService
{
    public void create(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception;

    public ma.nawarit.checker.injection.MouvementCax load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception;

    public void delete(ma.nawarit.checker.injection.MouvementCax entity)
        throws Exception;
    
    public void createAll(Collection<ma.nawarit.checker.injection.MouvementCax> entities)
    throws Exception;
    public java.util.List readByCriteria(CommonCriteria crit) throws Exception;
}
