/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jan 16 14:59:10 GMT 2012 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

import java.util.Date;

public interface DynPlaDetailManageableService
{
    public void create(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception;

    public ma.nawarit.checker.configuration.DynPlaDetail load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception;

    public void delete(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception;
    public void removeByCriteria(Date datePla, int planningId)throws Exception;
}
