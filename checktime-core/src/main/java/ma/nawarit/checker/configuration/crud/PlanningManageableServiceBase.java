/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jun 20 09:06:24 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

import java.util.List;

public final class PlanningManageableServiceBase
    implements PlanningManageableService
{
    private ma.nawarit.checker.configuration.PlanningDao dao;
    private ma.nawarit.checker.configuration.PlanningCyclDao daoCycl;
    
    public ma.nawarit.checker.configuration.PlanningCyclDao getDaoCycl() {
		return daoCycl;
	}

	public void setDaoCycl(ma.nawarit.checker.configuration.PlanningCyclDao daoCycl) {
		this.daoCycl = daoCycl;
	}

	public void setDao(ma.nawarit.checker.configuration.PlanningDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.configuration.PlanningDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.configuration.Planning entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.configuration.Planning load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.configuration.Planning entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.configuration.Planning entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.configuration.crud.PlanningManageableService.delete(ma.nawarit.checker.configuration.Planning entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

	public java.util.List readPlanningIdsList() {
		return dao.readPlanningIdsList();
	}

	public List readPlanningListByOffset(int offset, int rowNumber) {
		return dao.readPlanningListByOffset(offset, rowNumber);
	}

	public java.lang.Object loadCycl(int id)
    {
        return daoCycl.load(id);
    }
    

}
