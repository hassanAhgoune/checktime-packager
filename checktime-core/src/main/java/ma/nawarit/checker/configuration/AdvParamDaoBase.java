/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon May 26 11:31:49 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.configuration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.configuration.AdvParam</code>.
 * </p>
 *
 * @see ma.nawarit.checker.configuration.AdvParam
 */
public abstract class AdvParamDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.configuration.AdvParamDao
{

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.configuration.AdvParamImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.configuration.AdvParam)entity);
    }

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#load(int)
     */
    public ma.nawarit.checker.configuration.AdvParam load(int id)
    {
        return (ma.nawarit.checker.configuration.AdvParam)this.load(TRANSFORM_NONE, id);
    }

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.configuration.AdvParamImpl.class);
        this.transformEntities(transform, results);
        return results;
    }

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#update(ma.nawarit.checker.configuration.AdvParam)
     */
    public void update(ma.nawarit.checker.configuration.AdvParam jrFerie)
    {
        if (jrFerie == null)
        {
            throw new IllegalArgumentException(
                "AdvParam.update - 'jrFerie' can not be null");
        }
        this.getHibernateTemplate().update(jrFerie);
    }

    /**
     * @see ma.nawarit.checker.configuration.AdvParamDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "AdvParam.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.configuration.AdvParam)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.configuration.AdvParamDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.configuration.AdvParamDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.configuration.AdvParam entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.configuration.AdvParam)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.configuration.AdvParamDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.configuration.AdvParam)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.configuration.AdvParamImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.configuration.AdvParamImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }

    public String getValueByName(String name) {
    	Hashtable<String, String> props = new Hashtable<String, String>();
    	props.put("name", name);
    	List l = this.read(props);
    	return (l != null && !l.isEmpty())? ((AdvParam)l.get(0)).getValue() : null;
    }

}