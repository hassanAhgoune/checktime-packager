/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Nov 10 11:11:42 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

public final class TypeInterManageableServiceBase
    implements TypeInterManageableService
{
    private ma.nawarit.checker.configuration.TypeInterDao dao;

    public void setDao(ma.nawarit.checker.configuration.TypeInterDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.configuration.TypeInterDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.configuration.TypeInter entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.configuration.TypeInter load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.configuration.TypeInter entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.configuration.TypeInter entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.configuration.crud.TypeInterManageableService.delete(ma.nawarit.checker.configuration.TypeInter entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    

}
