/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Nov 10 11:11:42 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateEntity.vsl in andromda-hibernate-cartridge.
//
package ma.nawarit.checker.configuration;

/**
 * 
 */
public abstract class TypeInter
    implements java.io.Serializable
{
    /**
     * The serial version UID of this class. Needed for serialization.
     */
    private static final long serialVersionUID = 6211703536921141927L;

    private int id;

    /**
     * 
     */
    public int getId()
    {
        return this.id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
    private boolean deduir;
    
    public boolean isDeduir() {
		return deduir;
	}

	public void setDeduir(boolean deduir) {
		this.deduir = deduir;
	}
	
    private java.lang.String libelle;

    /**
     * 
     */
    public java.lang.String getLibelle()
    {
        return this.libelle;
    }

    public void setLibelle(java.lang.String libelle)
    {
        this.libelle = libelle;
    }

    private java.lang.String code;

    /**
     * 
     */
    public java.lang.String getCode()
    {
        return this.code;
    }

    public void setCode(java.lang.String code)
    {
        this.code = code;
    }   

    /**
     * Returns <code>true</code> if the argument is an TypeConge instance and all identifiers for this entity
     * equal the identifiers of the argument entity. Returns <code>false</code> otherwise.
     */
    public boolean equals(Object object)
    {
        if (this == object)
        {
            return true;
        }
        if (!(object instanceof TypeInter))
        {
            return false;
        }
        final TypeInter that = (TypeInter)object;
        if (this.id != that.getId())
        {
            return false;
        }
        return true;
    }

    /**
     * Returns a hash code based on this entity's identifiers.
     */
    public int hashCode()
    {
        int hashCode = 0;
        hashCode = 29 * hashCode + (int)id;

        return hashCode;
    }

    /**
     * Constructs new instances of {@link  ma.nawarit.checker.configuration.TypeInter}.
     */
    public static final class Factory
    {
        /**
         * Constructs a new instance of {@link  ma.nawarit.checker.configuration.TypeInter}.
         */
        public static  ma.nawarit.checker.configuration.TypeInter newInstance()
        {
            return new  ma.nawarit.checker.configuration.TypeInterImpl();
        }


        /**
         * Constructs a new instance of {@link ma.nawarit.checker.configuration.TypeInter}, taking all possible properties
         * (except the identifier(s))as arguments.
         */
        public static ma.nawarit.checker.configuration.TypeInter newInstance(java.lang.String libelle, java.lang.String code, boolean deduir)
        {
            final ma.nawarit.checker.configuration.TypeInter entity = new ma.nawarit.checker.configuration.TypeInterImpl();
            entity.setLibelle(libelle);
            entity.setCode(code);
            entity.setDeduir(deduir);
            return entity;
        }
    }

	
    
// HibernateEntity.vsl merge-point
}