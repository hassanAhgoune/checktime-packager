/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Fri Jun 20 09:25:46 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: SpringHibernateDaoBase.vsl in andromda-spring-cartridge.
//
package ma.nawarit.checker.configuration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.Expression;

/**
 * <p>
 * Base Spring DAO Class: is able to create, update, remove, load, and find
 * objects of type <code>ma.nawarit.checker.configuration.Planning</code>.
 * </p>
 *
 * @see ma.nawarit.checker.configuration.Planning
 */
public abstract class PlanningDaoBase
    extends org.springframework.orm.hibernate3.support.HibernateDaoSupport
    implements ma.nawarit.checker.configuration.PlanningDao
{

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#load(int, int)
     */
    public java.lang.Object load(final int transform, final int id)
    {
        final java.lang.Object entity = this.getHibernateTemplate().get(ma.nawarit.checker.configuration.PlanningImpl.class, new java.lang.Integer(id));
        return transformEntity(transform, (ma.nawarit.checker.configuration.Planning)entity);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#load(int)
     */
    public ma.nawarit.checker.configuration.Planning load(int id)
    {
        return (ma.nawarit.checker.configuration.Planning)this.load(TRANSFORM_NONE, id);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#loadAll()
     */
    public java.util.Collection loadAll()
    {
        return this.loadAll(TRANSFORM_NONE);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#loadAll(int)
     */
    public java.util.Collection loadAll(final int transform)
    {
        final java.util.Collection results = this.getHibernateTemplate().loadAll(ma.nawarit.checker.configuration.PlanningImpl.class);
        this.transformEntities(transform, results);
        return results;
    }


    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(ma.nawarit.checker.configuration.Planning)
     */
    public ma.nawarit.checker.configuration.Planning create(ma.nawarit.checker.configuration.Planning planning)
    {
        return (ma.nawarit.checker.configuration.Planning)this.create(TRANSFORM_NONE, planning);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(int transform, ma.nawarit.checker.configuration.Planning)
     */
    public java.lang.Object create(final int transform, final ma.nawarit.checker.configuration.Planning planning)
    {
        if (planning == null)
        {
            throw new IllegalArgumentException(
                "Planning.create - 'planning' can not be null");
        }
        this.getHibernateTemplate().save(planning);
        return this.transformEntity(transform, planning);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(java.util.Collection)
     */
    public java.util.Collection create(final java.util.Collection entities)
    {
        return create(TRANSFORM_NONE, entities);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(int, java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public java.util.Collection create(final int transform, final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Planning.create - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        create(transform, (ma.nawarit.checker.configuration.Planning)entityIterator.next());
                    }
                    return null;
                }
            });
        return entities;
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(java.lang.String, boolean, java.lang.String)
     */
    public ma.nawarit.checker.configuration.Planning create(
        java.lang.String libelle,
        java.lang.String type)
    {
        return (ma.nawarit.checker.configuration.Planning)this.create(TRANSFORM_NONE, libelle, type);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#create(int, java.lang.String, boolean, java.lang.String)
     */
    public java.lang.Object create(
        final int transform,
        java.lang.String libelle,
        java.lang.String type)
    {
        ma.nawarit.checker.configuration.Planning entity = new ma.nawarit.checker.configuration.PlanningImpl();
        entity.setLibelle(libelle);
        entity.setType(type);
        return this.create(transform, entity);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#update(ma.nawarit.checker.configuration.Planning)
     */
    public void update(ma.nawarit.checker.configuration.Planning planning)
    {
        if (planning == null)
        {
            throw new IllegalArgumentException(
                "Planning.update - 'planning' can not be null");
        }
        this.getHibernateTemplate().update(planning);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#update(java.util.Collection)
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void update(final java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Planning.update - 'entities' can not be null");
        }
        this.getHibernateTemplate().execute(
            new org.springframework.orm.hibernate3.HibernateCallback()
            {
                public java.lang.Object doInHibernate(org.hibernate.Session session)
                    throws org.hibernate.HibernateException
                {
                    for (java.util.Iterator entityIterator = entities.iterator(); entityIterator.hasNext();)
                    {
                        update((ma.nawarit.checker.configuration.Planning)entityIterator.next());
                    }
                    return null;
                }
            });
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#remove(ma.nawarit.checker.configuration.Planning)
     */
    public void remove(ma.nawarit.checker.configuration.Planning planning)
    {
        if (planning == null)
        {
            throw new IllegalArgumentException(
                "Planning.remove - 'planning' can not be null");
        }
        this.getHibernateTemplate().delete(planning);
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#remove(int)
     */
    public void remove(int id)
    {
        ma.nawarit.checker.configuration.Planning entity = this.load(id);
        if (entity != null)
        {
            this.remove(entity);
        }
    }

    /**
     * @see ma.nawarit.checker.configuration.PlanningDao#remove(java.util.Collection)
     */
    public void remove(java.util.Collection entities)
    {
        if (entities == null)
        {
            throw new IllegalArgumentException(
                "Planning.remove - 'entities' can not be null");
        }
        this.getHibernateTemplate().deleteAll(entities);
    }
    /**
     * Allows transformation of entities into value objects
     * (or something else for that matter), when the <code>transform</code>
     * flag is set to one of the constants defined in <code>ma.nawarit.checker.configuration.PlanningDao</code>, please note
     * that the {@link #TRANSFORM_NONE} constant denotes no transformation, so the entity itself
     * will be returned.
     *
     * If the integer argument value is unknown {@link #TRANSFORM_NONE} is assumed.
     *
     * @param transform one of the constants declared in {@link ma.nawarit.checker.configuration.PlanningDao}
     * @param entity an entity that was found
     * @return the transformed entity (i.e. new value object, etc)
     * @see #transformEntities(int,java.util.Collection)
     */
    protected java.lang.Object transformEntity(final int transform, final ma.nawarit.checker.configuration.Planning entity)
    {
        java.lang.Object target = null;
        if (entity != null)
        {
            switch (transform)
            {
                case TRANSFORM_NONE : // fall-through
                default:
                    target = entity;
            }
        }
        return target;
    }

    /**
     * Transforms a collection of entities using the
     * {@link #transformEntity(int,ma.nawarit.checker.configuration.Planning)}
     * method. This method does not instantiate a new collection.
     * <p/>
     * This method is to be used internally only.
     *
     * @param transform one of the constants declared in <code>ma.nawarit.checker.configuration.PlanningDao</code>
     * @param entities the collection of entities to transform
     * @see #transformEntity(int,ma.nawarit.checker.configuration.Planning)
     */
    protected void transformEntities(final int transform, final java.util.Collection entities)
    {
        switch (transform)
        {
            case TRANSFORM_NONE : // fall-through
                default:
                // do nothing;
        }
    }


    public java.util.List read(java.util.Hashtable properties)
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.configuration.PlanningImpl.class);

			Set e = properties.keySet();
			Iterator it = e.iterator();
			while(it.hasNext()){
				String tmp = (String)it.next();
				criteria.add( Expression.eq(tmp,properties.get(tmp)));	
			}		
			return criteria.list();
			
		} 
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
	}

    public java.util.List readAll()
    {
        final Session session = getSession(false);

        try
        {
            final Criteria criteria = session.createCriteria(ma.nawarit.checker.configuration.PlanningImpl.class);

            return criteria.list();
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
    }
    
    
	public java.util.List readPlanningIdsList(){
		final Session session = getSession(false);

        try
        {  		
        	String query = "SELECT distinct(u.ID) FROM PLANNING u ";
        	SQLQuery q = session.createSQLQuery(query);
			return q.list();
			
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
		
	}
	
	public java.util.List readPlanningListByOffset(int offset, int rowNumber){
		final Session session = getSession(false);
        try
        {  		
        	String query = "SELECT distinct(u.ID) FROM PLANNING u LIMIT "+rowNumber+" OFFSET "+offset;
        	SQLQuery q = session.createSQLQuery(query);
			List myList = q.list();
			List result =null;
			if(myList != null && myList.size() >= 1){
				Iterator<Integer> iter = myList.iterator();
				Integer id;
				result = new ArrayList();
				while (iter.hasNext()) {
					id = (Integer)iter.next();
					result.add(this.load(id.intValue()));
					
				}
				
			}
			return result;
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw super.convertHibernateAccessException(ex);
        }
		
	}

}