/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Mar 17 15:57:45 GMT 2010 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

public interface PenaliteManageableService
{
    public void create(ma.nawarit.checker.configuration.Penalite entity)
        throws Exception;

    public ma.nawarit.checker.configuration.Penalite load(int id)
        throws Exception;
    
    public java.util.List read(java.util.Hashtable properties)
        throws Exception;

    public java.util.List readAll()
        throws Exception;


    public void update(ma.nawarit.checker.configuration.Penalite entity)
        throws Exception;

    public void delete(ma.nawarit.checker.configuration.Penalite entity)
        throws Exception;

}
