/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jun 09 14:07:18 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

import java.util.List;

import ma.nawarit.checker.configuration.Horaire;

public final class HoraireManageableServiceBase
    implements HoraireManageableService
{
    private ma.nawarit.checker.configuration.HoraireDao dao;

    public void setDao(ma.nawarit.checker.configuration.HoraireDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.configuration.HoraireDao getDao()
    {
        return this.dao;
    }

    public ma.nawarit.checker.configuration.Horaire create(ma.nawarit.checker.configuration.Horaire entity)
        throws Exception
    {

       return dao.create(entity);
    }

    
     public ma.nawarit.checker.configuration.Horaire load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.configuration.Horaire entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.configuration.Horaire entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.configuration.crud.HoraireManageableService.delete(ma.nawarit.checker.configuration.Horaire entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }

	public List<Horaire> loadAll() {
		
		return (List<Horaire>) dao.loadAll();
	}

	public java.util.List readHoraireIdsList() {
		
		return dao.readHoraireIdsList();
	}
	
	public java.util.List readHoraireListByOffset(int offset, int rowNumber) {
		return dao.readHoraireListByOffset(offset, rowNumber);
	}


    

}
