/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Mon Jan 16 14:59:10 GMT 2012 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
package ma.nawarit.checker.configuration.crud;

import java.util.Date;

public final class DynPlaDetailManageableServiceBase
    implements DynPlaDetailManageableService
{
    private ma.nawarit.checker.configuration.DynPlaDetailDao dao;

    public void setDao(ma.nawarit.checker.configuration.DynPlaDetailDao dao)
    {
        this.dao = dao;
    }

    protected ma.nawarit.checker.configuration.DynPlaDetailDao getDao()
    {
        return this.dao;
    }

    public void create(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception
    {

       dao.create(entity);
    }

    
     public ma.nawarit.checker.configuration.DynPlaDetail load(int id)
        throws Exception
    {
        return dao.load(id);
    }
    public java.util.List read(java.util.Hashtable properties)
        throws Exception
    {
        return dao.read(properties);
    }

    public java.util.List readAll()
        throws Exception
    {
        return dao.readAll();
    }


    public void update(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception
    {

        dao.update(entity);
    }

    public void delete(ma.nawarit.checker.configuration.DynPlaDetail entity)
        throws Exception
    {
        if (entity == null)
        {
            throw new IllegalArgumentException(
                "ma.nawarit.checker.configuration.crud.DynPlaDetailManageableService.delete(ma.nawarit.checker.configuration.DynPlaDetail entity) - 'entity' can not be null");
        }

        dao.remove(entity);
    }


    public void removeByCriteria(Date datePla, int planningId)throws Exception{
    	dao.removeByCriteria(datePla, planningId);
    }

}
