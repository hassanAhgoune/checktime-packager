/**
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Crée le       :</B><DD> Wed Mar 19 17:40:52 CET 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab
 */
//
// Attention: Generated code! Do not modify by hand!
// Generated by: HibernateSearch.vsl in andromda-spring-cartridge.
//
package org.andromda.spring;

import java.io.Serializable;
import java.util.Date;

import ma.nawarit.checker.compagnie.Noeud;


public class PeriodCriteria implements Serializable{
	private Date dateDebut =null;
	private Date dateFin = null;
	private String matricule = null;
	private String statut = null;
	private String stringFormat = null;
	private boolean horNuit = false;
	
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	
	public String getStatut() {
		return statut;
	}
	public void setStatut(String statut) {
		statut = statut;
	}

	public boolean isHorNuit() {
		return horNuit;
	}
	public void setHorNuit(boolean horNuit) {
		this.horNuit = horNuit;
	}
	
	public boolean isEmpty(){
		if(this.dateDebut != null || this.dateFin != null){
			return false;
		}
		return true;
	}
	public String getStringFormat() {
		return stringFormat;
	}
	public void setStringFormat(String stringFormat) {
		this.stringFormat = stringFormat;
	}
	public String getMatricule() {
		return matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
}