package org.andromda.persistence.hibernate.usertypes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;

public class BinaryBlobType implements UserType {

	public Object assemble(Serializable arg0, Object arg1)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public int[] sqlTypes() {
		return new int[] { Types.BLOB };
	}

	public Class returnedClass() {
		return byte[].class;
	}

	public boolean equals(Object x, Object y) {
		return (x == y)
				|| (x != null && y != null && java.util.Arrays.equals(
						(byte[]) x, (byte[]) y));
	}

	public Object nullSafeGet(ResultSet rs, String[] names, Object owner)
			throws HibernateException, SQLException {
		final Object object;

        final InputStream inputStream = rs.getBinaryStream(names[0]);
        if (inputStream == null)
        {
            object = null;
        }
        else
        {
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			try
            {
			    final byte[] buffer = new byte[65536];
                int read = -1;
				
                while ((read = inputStream.read(buffer)) > -1)
                {
                    outputStream.write(buffer, 0, read);
                }
                outputStream.close();
            }
            catch (IOException exception)
            {
                throw new HibernateException("Unable to read blob " + names[0], exception);
            }
            object = outputStream.toByteArray();
        }

        return object;

	}

	public void nullSafeSet(PreparedStatement st, Object value, int index)
			throws SQLException {
		final byte[] bytes = (byte[])value;
        if (bytes == null)
        {
            try
            {
                st.setBinaryStream(index, null, 0);
            }
            catch (SQLException exception)
            {
                Blob nullBlob = null;
                st.setBlob(index, nullBlob);
            }
        }
        else
        {
            st.setBinaryStream(index, new ByteArrayInputStream(bytes), bytes.length);
        }
	}

	public Object deepCopy(Object value) {
		if (value == null)
			return null;

		byte[] bytes = (byte[]) value;
		byte[] result = new byte[bytes.length];
		System.arraycopy(bytes, 0, result, 0, bytes.length);

		return result;
	}

	public Serializable disassemble(Object arg0) throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

	public int hashCode(Object arg0) throws HibernateException {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean isMutable() {
		// TODO Auto-generated method stub
		return false;
	}

	public Object replace(Object arg0, Object arg1, Object arg2)
			throws HibernateException {
		// TODO Auto-generated method stub
		return null;
	}

}
