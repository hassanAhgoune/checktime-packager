package org.springsecurity.providers.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.GrantedAuthorityImpl;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;

import ma.nawarit.checker.compagnie.Role;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;

public class UserDetailsServiceImpl implements UserDetailsService {
	private Logger log = Logger.getLogger(UserDetailsServiceImpl.class);
	private UserDao userDao;

	
	public UserDetailsServiceImpl(){
		log.info("UserDetailsServiceImpl  ");
		
	}
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		
		UserDetails usrdetail = makeAcegiUser(loadUser(username));
		return usrdetail;
		
	}
	public User loadUser(String username)throws UsernameNotFoundException, DataAccessException  {
		
		log.info("loadUser : " + username);
		Hashtable< String, String> hash = new Hashtable<String, String>();
		hash.put("login", username);
		List<User> users = userDao.read(hash);
		User user = null;
		if(users != null && users.size()==1)
			 user = users.get(0); 
		if (user == null){
			log.info("User not found: " + username);
			throw new UsernameNotFoundException("User not found: " + username);
		}
		log.info("USER searched : " + user.getNom() +" " + user.getPrenom());
		return user;
	}

	private org.acegisecurity.userdetails.User makeAcegiUser(User user) {
		
		return new org.acegisecurity.userdetails.User(user.getLogin(),user
				.getPassword(), true, true, true, true,
				makeGrantedAuthorities(user));
	}

	private GrantedAuthority[] makeGrantedAuthorities(User user) {
		GrantedAuthority[] result = new GrantedAuthority[user.getRoles().size()];
		Iterator<Role> it = user.getRoles().iterator();
		Role role = null;
		int i = 0;
		while (it.hasNext()) {
			role = it.next();
			result[i] = new GrantedAuthorityImpl(role.getProfilApp().getCode());
			i++;
		}
		return result;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}

