package ma.nawarit.checker.batch.common;

import java.text.MessageFormat;
import java.util.ResourceBundle;



public class PropertiesReader {	

	static ResourceBundle bundle = ResourceBundle.getBundle("conf.batch-settings");
	/**
	 * 
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return bundle.getString(key);
	}

	public static String getProperty(String key, Object[] args) {
		if (args == null || args.length == 0) {
			return bundle.getString(key);
		}
		MessageFormat fmt = new MessageFormat(bundle.getString(key));
		
		return fmt.format(args);
		
	}
}
