package ma.nawarit.checker.batch.common;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;

public abstract class Encoder {
	public static final Encoder HtmlText = new HtmlTextEncoder();
	public static final Encoder HtmlPreText = new HtmlTextEncoder(true);
	public static final Encoder HtmlAttrText = new HtmlAttrEncoder();
	public static final Encoder JScriptText = new JScriptTextEncoder();
	public static final Encoder FileName = new FileNameEncoder();
	public static final Encoder ASCII = new ASCIIEncoder();
	public static final Encoder URL = new URLEncoder();
	public static final Encoder HEX = new HexEncoder();
	public static final Encoder JavaProperty = new JavaPropertyEncoder();

	public static final Encoder NULL = new NullEncoder();

	static final char[] DIGITS = {
	'0',
	'1',
	'2',
	'3',
	'4',
	'5',
	'6',
	'7',
	'8',
	'9',
	'A',
	'B',
	'C',
	'D',
	'E',
	'F',
	'G',
	'H',
	'I',
	'J',
	'K',
	'L',
	'M',
	'N',
	'O',
	'P',
	'Q',
	'R',
	'S',
	'T',
	'U',
	'V',
	'W',
	'X',
	'Y',
	'Z' };

	public abstract void encode(Writer paramWriter, Reader paramReader)
			throws IOException;

	public void decode(Writer w, Reader r) throws IOException {
		throw new RuntimeException("Method not implemented");
	}

	public void encode(PrintWriter w, String s) {
		try {
			encode(w, new StringReader(s == null ? "" : s));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void encode(Writer w, String s) throws IOException {
		encode(w, new StringReader(s == null ? "" : s));
	}

	public String encode(String s) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try {
			encode(pw, new StringReader(s == null ? "" : s));
		} catch (IOException e) {
			e.printStackTrace();
		}
		pw.close();
		return sw.toString();
	}

	public String decode(String s) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		try {
			decode(pw, new StringReader(s == null ? "" : s));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		pw.close();
		return sw.toString();
	}

	public static boolean isHexChar(int ic) {
		if ((ic >= 48) && (ic <= 57)) {
			return true;
		}
		if ((ic >= 65) && (ic <= 70)) {
			return true;
		}
		return false;
	}

	public static char getCharFromHex(char h1, char h2) throws IOException {
		int i1 =
		(h1 >= 'a') && (h1 <= 'f') ? h1 - 'f' + 10 : (h1 >= 'A')
				&& (h1 <= 'F') ?
		h1 - 'A' + 10 : (h1 >= '0') && (h1 <= '9') ? h1 - '0' :
		0;
		int i2 =
		(h2 >= 'a') && (h2 <= 'f') ? h2 - 'a' + 10 : (h2 >= 'A')
				&& (h2 <= 'F') ?
		h2 - 'A' + 10 : (h2 >= '0') && (h2 <= '9') ? h2 - '0' :
		0;
		return (char) ((i1 << 4) + i2);
	}

	public static void writeCharInHex(Writer w, int ic) throws IOException {
		w.write(DIGITS[((ic & 0xF0) >> 4)]);
		w.write(DIGITS[(ic & 0xF)]);
	}

	public static void writeCharInHex4(Writer w, int ic) throws IOException {
		w.write(DIGITS[((ic & 0xF000) >> 12)]);
		w.write(DIGITS[((ic & 0xF00) >> 8)]);
		w.write(DIGITS[((ic & 0xF0) >> 4)]);
		w.write(DIGITS[(ic & 0xF)]);
	}

	public static void writeCharInUnicode(Writer w, int ic) throws IOException {
		w.write(92);
		w.write(117);
		writeCharInHex4(w, ic);
	}

	public static StringBuffer writeCharInHex(StringBuffer w, int ic) {
		w.append(DIGITS[((ic & 0xF0) >> 4)]);
		w.append(DIGITS[(ic & 0xF)]);
		return w;
	}

	public static StringBuffer writeCharInHex4(StringBuffer w, int ic) {
		w.append(DIGITS[((ic & 0xF000) >> 12)]);
		w.append(DIGITS[((ic & 0xF00) >> 8)]);
		w.append(DIGITS[((ic & 0xF0) >> 4)]);
		w.append(DIGITS[(ic & 0xF)]);
		return w;
	}

	public static StringBuffer writeCharInUnicode(StringBuffer w, int ic) {
		w.append('\\');
		w.append('u');
		return writeCharInHex4(w, ic);
	}

	public static int readCharInHex(Reader w) throws IOException {
		int ic = w.read();
		if (ic < 0) {
			return -1;
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		int result = ic << 4;

		ic = w.read();
		if (ic < 0) {
			throw new IOException("Incomplete character encoding");
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		result += ic;
		return result;
	}

	public static int readCharInHex4(Reader w) throws IOException {
		int ic = w.read();
		if (ic < 0) {
			return -1;
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		int result = ic << 12;

		ic = w.read();
		if (ic < 0) {
			throw new IOException("Incomplete character encoding");
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		result += (ic << 8);

		ic = w.read();
		if (ic < 0) {
			throw new IOException("Incomplete character encoding");
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		result += (ic << 4);

		ic = w.read();
		if (ic < 0) {
			throw new IOException("Incomplete character encoding");
		}
		ic = getIntValueFromHexChar(ic);
		if (ic < 0) {
			throw new IOException("Invalid character HEX encoding");
		}
		result += ic;

		return result;
	}

	static final int getIntValueFromHexChar(int c) {
		if ((c >= 48) && (c <= 57)) {
			return c - 48;
		}
		if ((c >= 97) && (c <= 102)) {
			return c - 97 + 10;
		}
		if ((c >= 65) && (c <= 70)) {
			return c - 65 + 10;
		}
		return -1;
	}

	public static int getByteFromHex(int c1, int c2) throws IOException {
		int h1 = getHexCharIntValue(c1);
		int h2 = getHexCharIntValue(c2);
		if ((h1 < 0) || (h2 < 0)) {
			throw new IOException("Invalid character encoding " + h1
					+ h2);
		}
		return (h1 << 4) + h2;
	}

	public static final int getHexCharIntValue(int c) {
		if ((c >= 48) && (c <= 57)) {
			return c - 48;
		}
		if ((c >= 65) && (c <= 70)) {
			return 10 + c - 65;
		}
		if ((c >= 97) && (c <= 102)) {
			return 10 + c - 97;
		}
		return -1;
	}

	public static class ASCIIEncoder extends Encoder {
		static final char EscChar = '%';

		public void encode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				char c = (char) ic;
				if ((c != '%')
						&& (((c >= ' ') && (c <= '~')) || (c == '\n') || (c == '\t'))) {
					w.write(c);
				} else {
					w.write(37);
					writeCharInHex(w, ic);
				}
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				if (ic != 37) {
					w.write(ic);
				} else {
					int dg1 = r.read();
					int dg2 = r.read();
					if ((dg1 < 0) || (dg2 < 0)) {
						throw new IOException(
								"Incomplete character encoding");
					}
					dg1 = Character.digit((char) dg1, 16);
					dg2 = Character.digit((char) dg2, 16);
					if ((dg1 < 0) || (dg2 < 0)) {
						throw new IOException(
								"Invalid character encoding %" + (char) dg1
										+ (char) dg2);
					}
					w.write((char) ((dg1 << 4) + dg2));
				}
			}
		}
	}

	public static class FileNameEncoder extends Encoder {
		static final char EscChar = '~';
		static final String FileNameChar = "_-.!`$";

		public void encode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				char c = (char) ic;
				if (((c >= '0') && (c <= '9')) ||
				((c >= 'a') && (c <= 'z')) ||
				((c >= 'A') && (c <= 'Z')) ||
				("_-.!`$".indexOf(c) > -1)) {
					w.write(c);
				} else {
					w.write(126);
					writeCharInHex(w, ic);
				}
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				if (ic != 126) {
					w.write(ic);
				} else {
					int dg1 = r.read();
					int dg2 = r.read();
					if ((dg1 < 0) || (dg2 < 0)) {
						throw new IOException(
								"Incomplete character encoding");
					}
					dg1 = Character.digit((char) dg1, 16);
					dg2 = Character.digit((char) dg2, 16);
					if ((dg1 < 0) || (dg2 < 0)) {
						throw new IOException(
								"Invalid character encoding ~" + (char) dg1
										+ (char) dg2);
					}
					w.write((char) ((dg1 << 4) + dg2));
				}
			}
		}
	}

	public static class HexEncoder extends Encoder {
		public String encode(String s) {
			if ((s == null) || (s.length() == 0)) {
				return "";
			}
			StringBuffer sb = new StringBuffer(s.length() * 2);
			for (int i = 0; i < s.length(); i++) {
				char c = s.charAt(i);
				writeCharInHex(sb, c);
			}
			return sb.toString();
		}

		public String decode(String s) {
			if ((s == null) || (s.length() == 0)) {
				return "";
			}
			StringBuffer sb = new StringBuffer(s.length() / 2);
			for (int i = 0; i < s.length(); i += 2) {
				if (i + 1 >= s.length()) {
					throw new IllegalArgumentException(
							"Incomplete character encoding");
				}
				int c1 = s.charAt(i);
				int c2 = s.charAt(i + 1);
				int dg1 = Character.digit((char) c1, 16);
				int dg2 = Character.digit((char) c2, 16);
				if ((dg1 < 0) || (dg2 < 0)) {
					throw new IllegalArgumentException(
							"Invalid character encoding " + (char) c1
									+ (char) c2);
				}
				sb.append((char) ((dg1 << 4) + dg2));
			}
			return sb.toString();
		}

		public void encode(Writer out, Reader in) throws IOException {
			int ic;
			while ((ic = in.read()) > -1) {
				char c = (char) ic;
				writeCharInHex(out, c);
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			while (true) {
				int c1 = r.read();
				if (c1 < 0) {
					return;
				}
				int c2 = r.read();
				if (c2 < 0) {
					throw new IOException(
							"Incomplete character encoding");
				}
				int dg1 = Character.digit((char) c1, 16);
				int dg2 = Character.digit((char) c2, 16);
				if ((dg1 < 0) || (dg2 < 0)) {
					throw new IOException(
							"Invalid character encoding " + (char) c1
									+ (char) c2);
				}
				w.write((char) ((dg1 << 4) + dg2));
			}
		}
	}

	public static class HtmlAttrEncoder extends Encoder {
		public void encode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				char c = (char) ic;
				switch (c) {
				case '<':
					w.write("&lt;");
					break;
				case '>':
					w.write("&gt;");
					break;
				case '&':
					w.write("&amp;");
					break;
				case '"':
				case '\'':
					w.write("&#");
					w.write(String.valueOf(ic));
					w.write(";");
					break;
				default:
					if ((c >= ' ') && (c <= '~')) {
						w.write(c);
					} else {
						w.write("&#");
						w.write(String.valueOf(ic));
						w.write(";");
					}
					break;
				}
			}
		}
	}

	public static class HtmlTextEncoder extends Encoder {
		boolean pre = false;
		String tab = " &nbsp;&nbsp;&nbsp;";

		public HtmlTextEncoder(boolean _pre) {
			this.pre = _pre;
		}

		public HtmlTextEncoder() {
		}

		public void encode(Writer w, Reader r) throws IOException {
			int count = 0;
			char prevCh = '\000';
			int ic;
			while ((ic = r.read()) > -1) {
				char c = (char) ic;
				if (prevCh == c) {
					count++;
				} else {
					count = 0;
					prevCh = c;
				}
				switch (c) {
				case '\n':
					w.write(this.pre ? "\n" : "<BR>\n");
					break;
				case ' ':
					w.write((this.pre) || (count == 0) ? " "
							: "&nbsp;");
					break;
				case '\t':
					w.write(this.pre ? "\t" : this.tab);
					break;
				case '<':
					w.write("&lt;");
					break;
				case '>':
					w.write("&gt;");
					break;
				case '&':
					w.write("&amp;");
					break;
				case '"':
				case '\'':
					w.write("&#");
					w.write(String.valueOf(ic));
					w.write(";");
					break;
				default:
					if ((c >= ' ') && (c <= '~')) {
						w.write(c);
					} else {
						w.write("&#");
						w.write(String.valueOf(ic));
						w.write(";");
					}
					break;
				}
			}
		}
	}

	public static class JScriptTextEncoder extends Encoder {
		public void encode(Writer out, Reader in) throws IOException {
			int ic;
			while ((ic = in.read()) > -1) {
				char c = (char) ic;
				switch (c) {
				case '\n':
					out.write("\\n");
					break;
				case '\t':
					out.write("\\t");
					break;
				case '"':
					out.write("\\x22");
					break;
				case '\'':
					out.write("\\x27");
					break;
				case '\\':
					out.write("\\\\");
					break;
				case '<':
					out.write("\\x3C");
					break;
				case '>':
					out.write("\\x3E");
					break;
				default:
					if ((c >= ' ') && (c <= '~')) {
						out.write(c);
						} else if (ic <= 255) {
						out.write("\\x");
						writeCharInHex(out, ic);
					} else {
						out.write("\\u");
						writeCharInHex4(out, ic);
					}
					break;
				}
			}
		}

		public void decode(Writer out, Reader in) throws IOException {
			int ic;
			while ((ic = in.read()) > -1) {
				char c = (char) ic;
				if (c != '\\') {
					out.write(c);
				} else {
					ic = in.read();
					if (ic < 0) {
						throw new IOException(
								"Incomplete character encoding");
					}
					c = (char) ic;
					switch (c) {
					case 'n':
						out.write(10);
						break;
					case 't':
						out.write(9);
						break;
					case '"':
						out.write(34);
						break;
					case '\'':
						out.write(39);
						break;
					case '\\':
						out.write(92);
						break;
					case 'x':
						ic = in.read();
						if ((ic < 0) || (!isHexChar(ic))) {
							throw new IOException(
									"Incomplete character encoding");
						}
						char h1 = (char) ic;
						ic = in.read();
						if ((ic < 0) || (!isHexChar(ic))) {
							throw new IOException(
									"Incomplete character encoding");
						}
						char h2 = (char) ic;
						out.write(getCharFromHex(h1, h2));
						break;
					default:
						throw new IOException(
								"Incomplete character encoding");
					}
				}
			}
		}
	}

	public static class JavaPropertyEncoder extends Encoder {
		private static final String specialSaveChars = "=: \t\r\n\f#!";

		public void encode(Writer w, Reader r) throws IOException {
			int ic;
			for (int x = 0; (ic = r.read()) > -1;) {
				char c = (char) ic;
				switch (c) {
				case ' ':
					if (x == 0) {
						w.write(92);
					}
					w.write(32);
					break;
				case '\\':
					w.write(92);
					w.write(92);
					break;
				case '\t':
					w.write(92);
					w.write(116);
					break;
				case '\n':
					w.write(92);
					w.write(110);
					break;
				case '\r':
					w.write(92);
					w.write(114);
					break;
				case '\f':
					w.write(92);
					w.write(102);
					break;
				default:
					if ((c < ' ') || (c > '~')) {
						w.write(92);
						w.write(117);
						writeCharInHex4(w, c);
					} else {
						if ("=: \t\r\n\f#!".indexOf(c) != -1) {
							w.write(92);
						}
						w.write(c);
					}
					break;
				}
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			throw new NoSuchMethodError("Not yet implemented");
		}
	}

	protected static class NullEncoder extends Encoder {
		public void encode(Writer w, Reader r) throws IOException {
			char[] buffer = new char[1024];
			int i;
			while ((i = r.read(buffer)) > -1) {
				w.write(buffer, 0, i);
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			encode(w, r);
		}

		public String encode(String s) {
			return s;
		}

		public String decode(String s) {
			return s;
		}

		public void encode(PrintWriter w, String s) {
			w.print(s);
		}

		public void encode(Writer w, String s) throws IOException {
			w.write(s);
		}
	}

	public static class URLEncoder extends Encoder {
		static final char EscChar = '%';

		public void encode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				if ((ic >= 97) && (ic <= 122)) {
					w.write(ic);
				}
				else if ((ic >= 65) && (ic <= 90)) {
					w.write(ic);
				}
				else if ((ic >= 48) && (ic <= 57)) {
					w.write(ic);
				} else
					switch (ic) {
					case 32:
						w.write(43);
						break;
					case 42:
					case 45:
					case 46:
					case 95:
						w.write(ic);
						break;
					default:
						w.write(37);
						writeCharInHex(w, ic);
					}
			}
		}

		public void decode(Writer w, Reader r) throws IOException {
			int ic;
			while ((ic = r.read()) > -1) {
				char c = (char) ic;
				switch (c) {
				case '+':
					w.write(32);
					break;
				case '%':
					int c1 = getIntValueFromHexChar(r.read());
					int c2 = getIntValueFromHexChar(r.read());
					if ((c1 < 0) || (c2 < 0)) {
						throw new IOException("Invalid encoding ");
					}
					char decChar = (char) ((c1 << 4) + c2);
					w.write(decChar);
					break;
				default:
					w.write(c);
				}
			}
		}
	}
}

/*
 * Location: C:\MW\classes\ Qualified Name: zeyt.mw.Encoder JD-Core Version:
 * 0.6.2
 */