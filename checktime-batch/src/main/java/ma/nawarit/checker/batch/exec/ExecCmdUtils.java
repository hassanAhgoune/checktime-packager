package ma.nawarit.checker.batch.exec;

import java.io.IOException;


import org.apache.log4j.Logger;

/**
 * BATCH Command launcher under MS-DOS (r) Microsoft
 * @author , K.Lamhaddab
 */
public class ExecCmdUtils {
	/**
	  * Launch a command and render the output on a logger.
	  * @param s java.lang.String to execute representing the command line command.
	  * @param logger org.apache.log4j.Logger : Logger to send the output, 
	  * by default output are logged into the "info" stream and errors are logged into the "error" stream.
	  */
	public static void exec(String s, Logger logger) {
		exec(s, logger, null,new StringBuffer());
	}
	public static void exec(String s, Logger logger, StringBuffer buffer) {
		exec(s, logger, null,buffer);
	}


	/**
	 * Launch a command and render the output on a logger.
	 * @param s java.lang.String to execute representing the command line command.
	 * @param logger org.apache.log4j.Logger : Logger to send the output, 
	 * @param env Array of environnement variables.
	 * by default output are logged into the "info" stream and errors are logged into the "error" stream.
	 */
	public static void exec(String s, Logger logger, String[] env,StringBuffer buffer) {
		String s1 = System.getProperty("os.name");
		System.out.println(s1);
		String as[] = new String[3];
		if (s1.equals("Windows 2000")
			|| s1.equals("Windows Vista")
			|| s1.equals("Windows 7")
			|| s1.equals("Windows NT")
			|| s1.equals("Windows XP")
			|| s1.equals("Windows 2003")
			|| s1.equals("Windows 2008")) {
			as[0] = "cmd";
			as[1] = "/c";
			as[2] = s;
		} else if (s1.equals("Windows 95")) {
			as[0] = "command.com";
			as[1] = "/C";
			as[2] = s;
		}

		try {
			Runtime runtime = Runtime.getRuntime();
			System.out.println(as[0] + as[1] + as[2]);
			Process process = null;
			process = runtime.exec(as, env);

			

			new InputStreamHandler(
				process.getInputStream(),
				logger,
				InputStreamHandler.OUT_TYPE,
				buffer);
			new InputStreamHandler(
				process.getErrorStream(),
				logger,
				InputStreamHandler.ERR_TYPE,
				buffer);

			process.waitFor();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}
       public static void main(String[] args) {
    	   StringBuffer buffer= new StringBuffer();
    	   Logger log = Logger.getLogger(ExecCmdUtils.class);
	 	   ExecCmdUtils.exec(System.getProperty("user.dir")+"\\target\\classes\\terminal.exe users 192.168.2.120 1",
	 			   log, buffer);
	 	   String infos[]=buffer.toString().split(";"); // case 0 contenant les utilisateurs separ�s par virgule et case 1 contenant les logs separ�s par virgules
	 	   
	 	   String users[]=infos[0].split(","); // contient les utilisateurs
	 	  
	 	   String logs[] = infos[1].split(",");
//	 	  String infos[]=buffer.toString().split(";");
//	 	   String connexion = (infos[0].startsWith("Connect")?"Connecte":"Deconnect�");
//	 	   if(connexion.equals("Connecte"))
//	 	   {
//	 	   String mac = infos[1];
//	 	   String serial = infos[2];
//	 	   String version = infos[3];
//	 	   String etat_porte= infos[4];
//	 	   System.out.println(infos[0]+" "+infos[1]+" "+infos[2]+" "+infos[3]);
//	 	   }
	}    
}