package ma.nawarit.checker.batch.mwConn;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.nawarit.checker.batch.common.MWUtils;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.TerminalType;
import ma.nawarit.checker.equipement.crud.TerminalManageableService;
import ma.nawarit.checker.equipement.crud.TerminalTypeManageableService;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;

import org.andromda.spring.CommonCriteria;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.springframework.web.HttpRequestHandler;
//Command


public class MwRequestHandler implements HttpRequestHandler {
	//com mode 1(true) = BadgeId is badge num | com mode 0(false) = BadgeId is matricule
	String command = null;
	private TerminalManageableService terminalService;
	private TerminalTypeManageableService terminalTypeService;
	private UserManageableService userService;
	private MouvementManageableService mouvementService;
	
	public void handleRequest(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try {
			Map<String, String[]> requestParams = request.getParameterMap();
			String[] value;
			String ExternalId = "";
			StringBuffer sb = new StringBuffer();
	
			PrintWriter writer = response.getWriter();
			String termEnabledId = "";
			
			java.util.List<Mouvement> mvts = new ArrayList<Mouvement>();
			Mouvement mvt = new MouvementImpl();
			User user = null;
			Terminal ter = null;
			for(Map.Entry<String,String[]> entry  :  requestParams.entrySet()) {
				value = (String[])entry.getValue();
				if (entry.getKey().equals("Command")) {
					command = value[0];
					System.out.println("---> Command :" + command);
					continue;
				}
				if (entry.getKey().startsWith("TC.") 
						|| entry.getKey().equals("MyStartTime")
						|| entry.getKey().equals("MyTime")
						|| entry.getKey().equals("MessageId")
						|| entry.getKey().equals("InstallationId")
						|| entry.getKey().equals("ExternalId")
						|| entry.getKey().equals("MyTZ")
						|| entry.getKey().equals("Version")
						|| entry.getKey().equals("MyIP")) {
					System.out.println(entry.getKey() + " :" + value[0]);
					continue;
				}
				if (command != null && command.trim().equals("DATA")&& value.length > 0) {
					System.out.println(entry.getKey() + "=" + MWUtils.hexToString(value[0].toString()));
					if(entry.getKey().equals("AuthType")) {
						String type = MWUtils.hexToString(value[0].toString());
						mvt.setTypeMouvement(type);
					}
					if(entry.getKey().equals("InOutType")) {
						String inOut = MWUtils.hexToString(value[0].toString());
						mvt.setType(inOut);
					}
					if (entry.getKey().equals("TerminalId")) {
						String id = MWUtils.hexToString(value[0].toString());
						if (id != null && !id.trim().equals("")) {
							ter = terminalService.load(Integer.valueOf(id));
						}
					}else if (entry.getKey().equals("TerminalCode")) {
						String id = MWUtils.hexToString(value[0].toString());
						if (id != null && !id.trim().equals("")) {
							List l = terminalService.loadTerminalsByCode(id);
							if(l.size()>0)
								ter = (Terminal)l.get(0);
						}
					} else if (entry.getKey().equals("BadgeId")) {

						String badgeOrMatricule = MWUtils.hexToString(value[0].toString());
						mvt.setBadge(badgeOrMatricule);

						if (badgeOrMatricule != null && !badgeOrMatricule.trim().equals("")){
							CommonCriteria crit = new CommonCriteria();
							crit.setMatriculeSup(badgeOrMatricule);
							crit.setMatriculeInf(badgeOrMatricule);
							List l = userService.readByCriteria(crit);
							if (l != null && l.size() > 0)
								user = (User)l.get(0);
						}
						//if the user != null then badgeOrMatricule is a matricule
						if (user != null && user.getBadge() != null)
							mvt.setBadge(user.getBadge());

						if (ter != null)
							mvt.setTerminalCode(ter.getCode());
						
						if (ter != null && (user != null || ter.isComMode())){
							List l = StringUtils.isEmpty(user.getBadge())?
									mouvementService.readByMatriculeAndDate(mvt.getBadge(), mvt.getDate()):
									mouvementService.readByBadgeAndDate(mvt.getBadge(), mvt.getDate());
							if (l == null || l.isEmpty())
								mouvementService.create(mvt);
						}
					} if (entry.getKey().equals("PunchTime")) {
						String t = MWUtils.hexToString(value[0].toString());
						SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
						System.out.println(t);
						TimeZone tz = Calendar.getInstance().getTimeZone();
						DateTimeZone zone = DateTimeZone.forTimeZone(tz);
						DateTime dt = new DateTime(df.parse(t).getTime(), zone);
						System.out.println("#### Mouvement :" + df.parse(dt.toString("MM/dd/yyyy HH:mm:ss")));
						mvt.setDate(df.parse(dt.toString("MM/dd/yyyy HH:mm:ss")));
					}
					continue;	
				}
				if (command.trim().equals("ENABLE_DEVICE") && entry.getKey().equals("Terminal.Id")) {
					termEnabledId = MWUtils.hexToString(value[0].toString());
					Terminal termEnabled = terminalService.load(Integer.valueOf(termEnabledId));
					termEnabled.setEnabled(true);
					terminalService.update(termEnabled);
				}
				if (entry.getKey().equals("ExternalId"))
					ExternalId = value[0].toString();
				if(value.length>0) {
					for (int i = 0; i <= value.length - 1; i++) {
						System.out.println(entry.getKey() + "=" + value[i].toString());
					}
				}
			}
	
			if ( command.trim().equals("GET_NEW_INSTALLATION_ID")) {
				sb.append("InstallationId|" + ExternalId + "\n");	
				Collection<TerminalType> ttypes = terminalTypeService.readAll();
				if (ttypes != null && !ttypes.isEmpty()) {
					Iterator<TerminalType> it = ttypes.iterator();
					sb.append("TerminalTypes|");
					while (it.hasNext()) {
						TerminalType ttype = (TerminalType)it.next();
						sb.append(ttype.getId() + "~" + ttype.getLibelle() + "~" + ttype.getName() + "~" + ttype.getPackageTerm() + "|");
					}
					sb.append("\n");
				}
			} else if (command.trim().equals("GET_AVAILABLE_DEVICES")) {
				Collection<Terminal> l = terminalService.readAll();
				if (l != null && !l.isEmpty()) {
					sb.append("NumAvailableDevices|" + l.size() + "\n");
					Iterator<Terminal> it = l.iterator();
					int i = 0;
					int port = 4371;
					TimeZone tz = Calendar.getInstance().getTimeZone();
					while (it.hasNext()) {
						Terminal term = (Terminal) it.next();
						i++;
						sb.append("Terminal."+i+".Id|" + term.getId() + "\n");
						sb.append("Terminal."+i+".Type|"+ term.getType().getId() + "\n");
						sb.append("Terminal."+i+".ExternalId|1\n");
						sb.append("Terminal."+i+".UseBadge1|1\n");
						sb.append("Terminal."+i+".Name|" + term.getLibelle() + "\n");
						sb.append("Terminal."+i+".TZ|"+tz.getID()+"\n");
						sb.append("Terminal."+i+".ExtraInfo.ZK_IP|" + term.getIpAdress() + "\n");
						sb.append("Terminal."+i+".ExtraInfo.ZK_PORT_REMOTE|" + term.getIpPort() +"\n");
						sb.append("Terminal."+i+".ExtraInfo.ZK_PORT_LOCAL|" + port + "\n");
						if (term.isEnabled())
							termEnabledId += "|" + term.getId();
						port++;
					}
					sb.append("Settings|&ActiveDevices="+ termEnabledId + "\n");
				}
				
//			} else if (command.trim().equals("GET_SIMPLE_UPDATE")) {
//				sb.append("TerminalMessage.1.TermId|8\n").append("TerminalMessage.1.Type|QUERY_PUNCHES\n");
//				Collection<Terminal> l = terminalService.readAll();
//				if (l != null && !l.isEmpty()) {
//					sb.append("NumAvailableDevices|" + l.size() + "\n");
//					Iterator<Terminal> it = l.iterator();
//					int i = 0;
//					while (it.hasNext()) {
//						Terminal term = (Terminal) it.next();
//						i++;
//						if (term.isEnabled()) {
//							sb.append("TerminalMessage."+i+".TermId|" + term.getId() + "\n");
//							sb.append("TerminalMessage."+i+".Type|QUERY_PUNCHES\n");
//						}
//					}
//					sb.append("Settings|&ActiveDevices="+ termEnabledId + "\n");
//				}
//			} else if (command.trim().equals("ENABLE_DEVICE")) {
//				sb.append("Settings|&ActiveDevices="+ termEnabledId + "\n");
			} else if (command.trim().equals("DATA")) {
				System.out.println(((user != null)? user.getNom():""));
			}
			writer.write(sb.toString());
	//		writer.write("Company.1.dindy|0000\n");
			writer.write("Response|OK\n");
			this.command = null;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		
	}

	public void setTerminalService(TerminalManageableService terminalService) {
		this.terminalService = terminalService;
	}

	public void setTerminalTypeService(
			TerminalTypeManageableService terminalTypeService) {
		this.terminalTypeService = terminalTypeService;
	}
	
	public static void main(String[] args) throws ParseException {
//		String t ="3/14/2016 15:35:58";
//		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//		System.out.println(new Date(1458347232410L));
//		Calendar cal = new GregorianCalendar();
//		cal.add(Calendar.MONTH, 3);
//		System.out.println(cal.getTime());
		System.out.println(MWUtils.hexToString("303331373136313633333234"));
		Calendar cc = new GregorianCalendar();
		long l = 1462185443563L;
		cc.setTimeInMillis(l);
		System.out.println(cc.getTime());
		Enumeration names = System.getProperties().propertyNames();
		while (names.hasMoreElements()) {
			String pName = (String) names.nextElement();
			String pValue = System.getProperty(pName);
			System.out.println("MW    "+ pName + "=" + pValue);
		}
		TimeZone tz = Calendar.getInstance().getTimeZone();
//		System.out.println(tz.observesDaylightTime()); 
		System.out.println(tz.getID());
//		Locale locale = new Locale(System.getProperty("user.language"), System.getProperty("user.country"));
		System.out.println(Calendar.getInstance(TimeZone.getTimeZone("US/Alaska")).getTime());
		System.out.println(Calendar.getInstance(TimeZone.getTimeZone("Africa/Casablanca")).getTime());
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		DateTimeZone zone = DateTimeZone.forTimeZone(tz);
		DateTime dt = new DateTime(1462185443563L, zone);
		System.out.println(df.parse(dt.toString("MM/dd/yyyy HH:mm:ss")));
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}
	
}
