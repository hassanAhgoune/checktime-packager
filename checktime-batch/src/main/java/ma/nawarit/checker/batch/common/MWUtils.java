 package ma.nawarit.checker.batch.common;
 
 import java.io.File;
 import java.io.FileInputStream;
 import java.io.FileOutputStream;
 import java.io.FilenameFilter;
 import java.io.IOException;
 import java.io.InputStream;
 import java.io.InputStreamReader;
 import java.io.OutputStream;
 import java.io.Reader;
 import java.net.InetAddress;
 import java.net.NetworkInterface;
 import java.security.MessageDigest;
 import java.util.ArrayList;
 import java.util.Collection;
 import java.util.Enumeration;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Map;
 import java.util.StringTokenizer;
 import java.util.zip.ZipEntry;
 import java.util.zip.ZipOutputStream;
 
 public class MWUtils
 {
   static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
 
   public static final String[] ZERO_STRING_ARRAY = new String[0];
   public static final long MAX_IP_VALUE = 4294967294L;
   static final int INIT = 0;
   static final int HOURS = 1;
   static final int AFT_HOURS = 2;
   static final int DIV = 3;
   static final int AFT_DIV = 4;
   static final int MIN = 5;
   static final int AFT_MIN = 6;
   static final int AMPM = 7;
   static final int DONE = 8;
 
   public static String generateRandomString(int len)
   {
     StringBuffer ret = new StringBuffer();
     for (int i = 0; i < len; i++) {
       ret.append((char)((int)(Math.random() * 25.0D) + 65));
     }
     return ret.toString();
   }
 
   public static void getHexString(StringBuffer sb, byte[] b)
   {
     getHexString(sb, b, b.length);
   }
 
   public static void getHexString(StringBuffer sb, byte[] b, int len) {
     for (int i = 0; i < len; i++)
       byteToHex(sb, b[i]);
   }
 
   public static String getHexString(char[] b)
   {
     if (b == null) {
       return "";
     }
     StringBuffer ret = new StringBuffer(b.length * 2);
     getHexString(ret, b, b.length);
     return ret.toString();
   }
 
   public static void getHexString(StringBuffer sb, char[] b, int len) {
     for (int i = 0; i < len; i++)
       byteToHex(sb, (byte)b[i]);
   }
 
   public static void byteToHex(StringBuffer sb, byte b)
   {
     sb.append(hexChar[((b & 0xF0) >>> 4)]);
     sb.append(hexChar[(b & 0xF)]);
   }
 
   public static String getHexString(byte res) {
     return getHexString(new byte[] { res });
   }
 
   public static String getHexString(byte[] res) {
     return getHexString(res, res.length);
   }
 
   public static String getHexString(byte[] res, int len) {
     StringBuffer ret = new StringBuffer(len * 2);
     getHexString(ret, res, len);
     return ret.toString();
   }
 
   public static String stringToHex(String res) {
     StringBuffer ret = new StringBuffer(res.length() * 2);
     getHexString(ret, res.getBytes());
     return ret.toString();
   }
 
   public static void stringToHex(StringBuffer sb, String res) {
     getHexString(sb, res.getBytes());
   }
 
   private static int charToNibble(char c) {
     if (('0' <= c) && (c <= '9'))
       return c - '0';
     if (('a' <= c) && (c <= 'f'))
       return c - 'a' + 10;
     if (('A' <= c) && (c <= 'F')) {
       return c - 'A' + 10;
     }
     throw new IllegalArgumentException("Invalid hex character: " + c);
   }
 
   public static byte[] getHexByteArray(String s)
   {
     int stringLength = s.length();
     if ((stringLength & 0x1) != 0) {
       throw new IllegalArgumentException("fromHexString requires an even number of hex characters");
     }
     byte[] b = new byte[stringLength / 2];
 
     int i = 0; for (int j = 0; i < stringLength; j++) {
       int high = charToNibble(s.charAt(i));
       int low = charToNibble(s.charAt(i + 1));
       b[j] = ((byte)(high << 4 | low));
 
       i += 2;
     }
 
     return b;
   }
 
   public static String hexToString(String s) {
     if (s == null) {
       return "";
     }
     byte[] ret = getHexByteArray(s);
     return new String(ret);
   }
 
   public static byte[] getBytes(File f) {
     try {
       byte[] data = new byte[(int)f.length()];
       FileInputStream in = new FileInputStream(f);
       in.read(data);
       in.close();
       return data; } catch (Exception e) {
     }
     return null;
   }
 
   public static String[] splitStringWithEmpty(String s, String what)
   {
     if ((s == null) || (s.length() == 0)) {
       return ZERO_STRING_ARRAY;
     }
     ArrayList v = new ArrayList();
     splitStringWithEmpty(v, s, what);
     String[] ret = new String[v.size()];
     ret = (String[])v.toArray(ret);
     return ret;
   }
 
   public static void splitStringWithEmpty(Collection list, String s, String what) {
     if ((s == null) || (s.length() == 0)) {
       return;
     }
     boolean bLastWasResult = false;
     StringTokenizer st = new StringTokenizer(s, what, true);
     while (st.hasMoreTokens()) {
       String tmp = st.nextToken();
       if (what.indexOf(tmp) != -1) {
         if (!bLastWasResult) {
           list.add("");
         }
         bLastWasResult = false;
       }
       else {
         bLastWasResult = true;
 
         list.add(tmp);
       }
     }
   }
 
   public static String[] splitString(String s, String what) { if ((s == null) || (s.length() == 0)) {
       return ZERO_STRING_ARRAY;
     }
     ArrayList v = new ArrayList();
     splitString(v, s, what);
     String[] ret = new String[v.size()];
     ret = (String[])v.toArray(ret);
     return ret; }
 
   public static void splitString(Collection list, String s, String what)
   {
     if ((s == null) || (s.length() == 0)) {
       return;
     }
     StringTokenizer st = new StringTokenizer(s, what);
     while (st.hasMoreElements()) {
       String tmp = (String)st.nextElement();
       list.add(tmp);
     }
   }
 
   public static String[] splitString(String s, String what, boolean bRemoveDups)
   {
     if ((s == null) || (s.length() == 0)) {
       return ZERO_STRING_ARRAY;
     }
     ArrayList v = new ArrayList();
     splitString(v, s, what, bRemoveDups);
     String[] ret = new String[v.size()];
     ret = (String[])v.toArray(ret);
     return ret;
   }
 
   public static void splitString(Collection col, String s, String what, boolean bRemoveDups) {
     if (!bRemoveDups) {
       splitString(col, s, what);
       return;
     }
     if ((s == null) || (s.length() == 0)) {
       return;
     }
     StringTokenizer st = new StringTokenizer(s, what);
     while (st.hasMoreElements()) {
       String tmp = (String)st.nextElement();
       if (!col.contains(tmp))
         col.add(tmp);
     }
   }
 
   public static void toMap(Map map, String str, char pairDel, char valueDel, boolean trimValues, boolean addEmptyValues)
   {
     StringTokenizer st = new StringTokenizer(str, String.valueOf(pairDel + valueDel), true);
     String name = null;
     boolean nextName = true;
     boolean nextValue = false;
     while (st.hasMoreTokens()) {
       String s = st.nextToken();
       if (s.length() == 1) {
         if (s.charAt(0) == pairDel) {
           name = null;
           nextName = true;
           nextValue = false;
           continue;
         }
         if (s.charAt(0) == valueDel) {
           nextName = false;
           nextValue = true;
           continue;
         }
       }
       if (nextName) {
         name = s;
         nextName = false;
       }
       else if (nextValue) {
         String value = s;
         if (trimValues) {
           value = value.trim();
         }
         if ((addEmptyValues) || (value.length() != 0))
         {
           map.put(name, value);
           nextValue = false;
           name = null;
         }
       }
     }
   }
 
   public static int parseInt(Object v, int def) { if ((v instanceof Number)) {
       return ((Number)v).intValue();
     }
     if (v == null) {
       return def;
     }
     return parseInt(v.toString(), def); }
 
   public static int parseInt(String name, int def)
   {
     if ((name == null) || (name.length() == 0))
       return def;
     try
     {
       return Integer.parseInt(name); } catch (Exception e) {
     }
     return def;
   }
 
   public static long parseLong(String name, long def)
   {
     if ((name == null) || (name.length() == 0))
       return def;
     try
     {
       return Long.parseLong(name); } catch (Exception e) {
     }
     return def;
   }
 
   public static boolean parseBoolean(String str, boolean def)
   {
     if (str == null) {
       return def;
     }
     if (str.length() == 0) {
       return def;
     }
     return parseBoolean(str.charAt(0), def);
   }
 
   public static Boolean parseBoolean(String str, Boolean def) {
     if (str == null) {
       return def;
     }
     if (str.length() == 0) {
       return def;
     }
     return parseBoolean(str.charAt(0), def);
   }
 
   public static boolean parseBoolean(char c, boolean def) {
     if ((c >= '1') && (c <= '9')) {
       return true;
     }
     switch (c) {
     case '0':
       return false;
     case 'f':
       return false;
     case 'F':
       return false;
     case 'n':
       return false;
     case 'N':
       return false;
     case 't':
       return true;
     case 'T':
       return true;
     case 'y':
       return true;
     case 'Y':
       return true;
     }
     return def;
   }
 
   public static Boolean parseBoolean(char c, Boolean def) {
     if ((c >= '1') && (c <= '9')) {
       return Boolean.TRUE;
     }
     switch (c) {
     case '0':
       return Boolean.FALSE;
     case 'f':
       return Boolean.FALSE;
     case 'F':
       return Boolean.FALSE;
     case 'n':
       return Boolean.FALSE;
     case 'N':
       return Boolean.FALSE;
     case 't':
       return Boolean.TRUE;
     case 'T':
       return Boolean.TRUE;
     case 'y':
       return Boolean.TRUE;
     case 'Y':
       return Boolean.TRUE;
     }
     return def;
   }
 
   public static boolean isBlank(Object s) {
     if (s == null) {
       return true;
     }
     return isBlank(s.toString());
   }
 
   public static boolean isBlank(String s) {
     if (s == null) {
       return true;
     }
     int len = s.length();
     for (int start = 0; 
       start < len; start++) {
       char c = s.charAt(start);
       if (c >= '!')
       {
         if (c <= '~') {
           return false;
         }
         if ((!Character.isWhitespace(c)) && (!Character.isSpaceChar(c)))
         {
           return false;
         }
       }
     }
     return true;
   }
 
   public static long parseIP(String s) {
     try {
       if (isBlank(s)) {
         return -1L;
       }
       String[] elements = splitString(s, ".");
       if ((elements.length < 4) || (elements.length > 5)) {
         return -1L;
       }
       long result = 0L;
       for (int i = 0; i < elements.length; i++) {
         long ip = parseInt(elements[i], -1);
         if ((ip < 0L) || (ip > 255L)) {
           return -1L;
         }
         result += (ip << (elements.length - i - 1) * 8);
       }
       return result; } catch (Exception e) {
     }
     return -1L;
   }
 
   public static String longIPtoString(long ip)
   {
     if ((ip < 0L) || (ip > 4294967294L)) {
       return null;
     }
     StringBuffer sb = new StringBuffer();
     sb.append(ip >> 24 & 0xFF);
     sb.append(".");
     sb.append(ip >> 16 & 0xFF);
     sb.append(".");
     sb.append(ip >> 8 & 0xFF);
     sb.append(".");
     sb.append(ip & 0xFF);
     return sb.toString();
   }
 
   public static String joinString(String s1, String s2, String what) {
     if ((s1 == null) && (s2 == null)) {
       return null;
     }
     if (s1 == null) {
       return s2;
     }
     if (s2 == null) {
       return s1;
     }
     return s1 + what + s2;
   }
 
   public static String joinString(String[] s, String what) {
     if ((s == null) || (s.length == 0)) {
       return "";
     }
     StringBuffer ret = new StringBuffer();
     for (int i = 0; i < s.length; i++) {
       if (i > 0) {
         ret.append(what);
       }
       String ss = s[i];
       ret.append(ss);
     }
     return ret.toString();
   }
 
   public static String joinString(Collection c, String what) {
     if ((c == null) || (c.isEmpty())) {
       return "";
     }
     return joinString(c.iterator(), what);
   }
 
   public static String joinString(Iterator itr, String what) {
     StringBuffer ret = new StringBuffer();
     int i = 0;
     while (itr.hasNext()) {
       String s = itr.next().toString();
       if (i > 0) {
         ret.append(what);
       }
       i++;
 
       ret.append(s);
     }
     return ret.toString();
   }
 
   public static String leftPad(String s, int size, char what) {
     if (s.length() == size) {
       return s;
     }
     int needed = size - s.length();
     StringBuffer sb = new StringBuffer(size);
     for (int i = 0; i < needed; i++) {
       sb.append(what);
     }
     sb.append(s);
     return sb.toString();
   }
 
   public static String rightPad(String s, int size, char what) {
     if (s.length() == size) {
       return s;
     }
     int needed = size - s.length();
     StringBuffer sb = new StringBuffer(size);
     sb.append(s);
     for (int i = 0; i < needed; i++) {
       sb.append(what);
     }
     return sb.toString();
   }
 
   public static String digest(String s) {
     return digest(s.getBytes());
   }
 
   public static String digest(byte[] dataIn) {
     try {
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.reset();
       return getHexString(md.digest(dataIn)); } catch (Exception e) {
     }
     return "";
   }
 
   public static final boolean equals(String s1, String s2)
   {
     if ((s1 == null) && (s2 == null)) {
       return true;
     }
     if ((s1 == null) || (s2 == null)) {
       return false;
     }
     return s1.equals(s2);
   }
 
   public static final boolean equals(int s1, int s2) {
     return s1 == s2;
   }
 
   public static final boolean equals(Object s1, Object s2) {
     if ((s1 == null) && (s2 == null)) {
       return true;
     }
     if ((s1 == null) || (s2 == null)) {
       return false;
     }
     return s1.equals(s2);
   }
 
   public static final boolean equals(byte[] b1, byte[] b2) {
     if ((b1 == null) && (b2 == null)) {
       return true;
     }
     if ((b1 == null) || (b2 == null)) {
       return false;
     }
     if (b1.length != b2.length) {
       return false;
     }
     for (int i = 0; i < b1.length; i++) {
       if (b1[i] != b2[i]) {
         return false;
       }
     }
     return true;
   }
 
   public static List getNetworkIPs() {
     List ret = new ArrayList();
     try {
       for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements();) {
       
         NetworkInterface ni = (NetworkInterface)ifaces.nextElement();
				   InetAddress ia;
 		   for(Enumeration addrs = ni.getInetAddresses(); addrs.hasMoreElements();) {
					ia = (InetAddress)addrs.nextElement();
					ret.add(ia.getHostAddress());
				   }
       }
     }
     catch (Exception localException)
     {
     }
     return ret;
   }
 
   public static String removePrecedingZeros(String s) {
     if (s == null) {
       return s;
     }
     for (int i = 0; i < s.length(); i++) {
       if (s.charAt(i) != '0') {
         if (i == 0) {
           return s;
         }
         return s.substring(i);
       }
     }
     return "";
   }
 
   public static void zipFile(File inFile, ZipOutputStream zos) throws IOException {
     FileInputStream fis = new FileInputStream(inFile.getAbsolutePath());
     zipFile(fis, inFile.getName(), zos);
     fis.close();
   }
 
   public static void zipFile(InputStream fis, String fName, ZipOutputStream zos) throws IOException {
     byte[] iobuff = new byte[512];
 
     ZipEntry ze = new ZipEntry(fName);
     zos.putNextEntry(ze);
     int bytes;
     while ((bytes = fis.read(iobuff)) != -1)
     {
       zos.write(iobuff, 0, bytes);
     }
   }
 
   public static void zipFile(InputStream fis1, InputStream fis2, String fName, ZipOutputStream zos) throws IOException {
     byte[] iobuff = new byte[512];
 
     ZipEntry ze = new ZipEntry(fName);
     zos.putNextEntry(ze);
     if (fis1 != null)
     {
       int bytes;
       while ((bytes = fis1.read(iobuff)) != -1)
       {
         zos.write(iobuff, 0, bytes);
       }
     }
     if (fis2 != null)
     {
       int bytes;
       while ((bytes = fis2.read(iobuff)) != -1)
       {
         zos.write(iobuff, 0, bytes);
       }
     }
   }
 
   public static void createZip(File zipFile, File f) throws IOException {
     ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile.getAbsolutePath()));
     zipFile(f, zos);
     zos.close();
   }
 
   public static void createZip(File zipFile, InputStream fis, String fName) throws IOException {
     ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile.getAbsolutePath()));
     zipFile(fis, fName, zos);
     zos.close();
   }
 
   public static void createZip(File zipFile, InputStream fis1, InputStream fis2, String fName) throws IOException {
     ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipFile.getAbsolutePath()));
     zipFile(fis1, fis2, fName, zos);
     zos.close();
   }
 
   public static String replaceString(String aString, String oldString, String newString) {
     if ((aString == null) || (oldString == null) || (newString == null)) {
       return aString;
     }
 
     int last = 0;
     int current = 0;
     int oldLength = oldString.length();
     StringBuffer sb = new StringBuffer();
 
     while ((current = aString.indexOf(oldString, last)) != -1) {
       if (current > last) {
         sb.append(aString.substring(last, current));
       }
 
       sb.append(newString);
       last = current + oldLength;
     }
 
     if (last != aString.length()) {
       sb.append(aString.substring(last, aString.length()));
     }
 
     return sb.toString();
   }
 
   public static long setBit(long num, int bitPos, boolean b) {
     return b ? setBit(num, bitPos) : clearBit(num, bitPos);
   }
 
   public static long setBit(long num, int bitPos) {
     return num | 1 << bitPos;
   }
 
   public static long clearBit(long num, int bitPos) {
     return num & (1 << bitPos ^ 0xFFFFFFFF);
   }
 
   public static boolean isBitSet(long num, int bitPos) {
     return (num & 1 << bitPos) != 0L;
   }
 
   public static int setBit(int num, int bitPos, boolean b) {
     return b ? setBit(num, bitPos) : clearBit(num, bitPos);
   }
 
   public static int setBit(int num, int bitPos) {
     return num | 1 << bitPos;
   }
 
   public static int clearBit(int num, int bitPos) {
     return num & (1 << bitPos ^ 0xFFFFFFFF);
   }
 
   public static boolean isBitSet(int num, int bitPos) {
     return (num & 1 << bitPos) != 0;
   }
 
   public static int parseDayTimeInMS(String s, boolean canBeMinutesOnly, int dayTimeAfterMS, boolean isMilForm, long ifNull)
   {
     return (int)_parseTimeMS(s, canBeMinutesOnly, true, dayTimeAfterMS, isMilForm, false, ifNull);
   }
 
   public static int parseDayTimeInSec(String s, boolean canBeMinutesOnly, int dayTimeAfterMS, boolean isMilForm, long ifNull)
   {
     long l = _parseTimeMS(s, canBeMinutesOnly, true, dayTimeAfterMS, isMilForm, false, -9223372036854775808L);
     return l == -9223372036854775808L ? (int)ifNull : (int)(l / 1000L);
   }
 
   public static int parseDayTimeInMS(String s, boolean isMilForm) {
     return (int)_parseTimeMS(s, false, true, -1, isMilForm, false, -1L);
   }
 
   public static int parseDayTimeInSec(String s, boolean isMilForm) {
     int i = parseDayTimeInMS(s, isMilForm);
     return i == -1 ? i : i / 1000;
   }
 
   private static long _parseTimeMS(String s, boolean canBeMinutesOnly, boolean dayTime, int dayTimeAfterMS, boolean isMilForm, boolean isDefDecimal, long ifNull)
   {
     if ((s == null) || (s.length() == 0)) {
       return ifNull;
     }
 
     int h = 0; int m = 0;
     int iH = 0; int iM = 0;
     boolean decimal = isDefDecimal;
     boolean isPm = false;
     boolean isAm = false;
     boolean neg = false;
 
     int state = 0;
     int newstate = 0;
 
     int ic = 0;
     char c = '\000';
 
     while (state != 8)
     {
       boolean isNewState = state != newstate;
       if (!isNewState) {
         if (ic >= s.length()) {
           state = 8;
           break;
         }
         c = s.charAt(ic);
         ic++;
       }
       state = newstate;
 
       switch (state) {
       case 0:
         if ((c >= '0') && (c <= '9'))
           newstate = 1;
         else if ((c == ':') || (c == '.') || (c == ','))
           newstate = 3;
         else if (c == '-') {
           neg = !neg;
         }
         break;
       case 1:
         if ((c >= '0') && (c <= '9')) {
           h = h * 10 + (c - '0');
           iH++;
         } else {
           newstate = 2;
         }
         break;
       case 2:
         if ((c >= '0') && (c <= '9') && (m == 0))
           newstate = 5;
         else if ((c == 'a') || (c == 'A') || (c == 'p') || (c == 'P'))
           newstate = 7;
         else if ((c == '.') || (c == ':') || (c == ','))
           newstate = 3;
         else if (c == '-') {
           neg = !neg;
         }
         break;
       case 3:
         if ((c == '.') || (c == ','))
           decimal = true;
         else if (c == ':') {
           decimal = false;
         }
         newstate = 4;
         break;
       case 4:
         if ((c >= '0') && (c <= '9'))
           newstate = 5;
         else if ((c == 'a') || (c == 'A') || (c == 'p') || (c == 'P')) {
           newstate = 7;
         }
         break;
       case 5:
         if ((c >= '0') && (c <= '9')) {
           m = m * 10 + (c - '0');
           iM++;
         } else {
           newstate = 6;
         }
         break;
       case 6:
         if ((c == 'a') || (c == 'A') || (c == 'p') || (c == 'P')) {
           newstate = 7;
         }
         if (c == '-') {
           neg = !neg;
         }
         break;
       case 7:
         if ((c == 'p') || (c == 'P')) {
           isPm = true;
         }
         if ((c == 'a') || (c == 'A')) {
           isAm = true;
         }
         newstate = 8;
         break;
       default:
         newstate = 8;
       }
     }
     if ((iM == 0) && (iH == 0)) {
       return ifNull;
     }
     if (dayTime) {
       if (h > 0) {
         if ((canBeMinutesOnly) && (iM == 0))
         {
           if ((h < 2460) && (iH >= 3) && (iH <= 4) && (h >= 100) && (h % 100 < 60)) {
             m = h % 100;
             h /= 100;
           }
         }
         if ((h == 12) && (isAm))
           h = 0;
         else if ((h > 0) && (h < 12) && (isPm)) {
           h += 12;
         }
         if (h >= 24) {
           h %= 24;
         }
       }
     }
     else if ((h > 0) && (canBeMinutesOnly) && (iM == 0)) {
       m = h % 100;
       h /= 100;
       if ((m >= 60) && (h > 0)) {
         decimal = true;
         iM = 2;
       }
 
     }
 
     long resultMS = 0L;
     if (h > 0) {
       resultMS = h * 3600000L;
     }
     if (m > 0) {
       if (decimal)
         resultMS = (long)(resultMS + 3600000L * m / Math.pow(10.0D, iM));
       else {
         resultMS += m * 60000;
       }
     }
 
     if ((dayTime) && (dayTimeAfterMS > 0) && (resultMS > -1L) && (resultMS <= dayTimeAfterMS)) {
       if ((isAm) || (isPm)) {
         resultMS += 86400000L;
       }
       else if (isMilForm)
         resultMS += 86400000L;
       else {
         for (int i = 0; (i < 3) && (resultMS < dayTimeAfterMS); i++) {
           resultMS += 43200000L;
         }
       }
     }
 
     if ((!dayTime) && (neg)) {
       resultMS = -resultMS;
     }
     return resultMS;
   }
 
   public static boolean isInteger(String s) {
     if (s == null) {
       return false;
     }
     int sl = s.length();
     if (sl == 0) {
       return false;
     }
 
     int start = 0;
     if ((s.charAt(0) == '-') || (s.charAt(0) == '+')) {
       start = 1;
     }
     for (int i = start; i < sl; i++) {
       char c = s.charAt(i);
       if ((c < '0') || (c > '9'))
       {
         return false;
       }
     }
     return true;
   }
 
   public static String padLeft(int num, int resultLength, char padWith) {
     String s = String.valueOf(num);
     return padLeft(s, resultLength, padWith);
   }
 
   public static String padLeft(String s, int resultLength, char padWith) {
     if (s.length() == resultLength) {
       return s;
     }
     if (s.length() > resultLength) {
       return s.substring(s.length() - resultLength, resultLength);
     }
     StringBuffer sb = new StringBuffer(resultLength);
     for (int i = resultLength - s.length(); i > 0; i--) {
       sb.append(padWith);
     }
     sb.append(s);
     return sb.toString();
   }
 
   public static int round(int num, int incr, int upAfter)
   {
     int mod = num % incr;
     if (mod <= upAfter) {
       return num;
     }
     return num - mod + incr;
   }
 
   public static int move(InputStream in, OutputStream out, int bufferSize) throws IOException {
     byte[] buffer = new byte[bufferSize < 1 ? 1 : bufferSize];
     int allCount = 0;
     int count;
     while ((count = in.read(buffer)) > -1)
     {
       allCount += count;
       out.write(buffer, 0, count);
     }
     return allCount;
   }
 
   public static boolean copyFile(File from, File to) throws IOException {
     return copyFile(from, to, false);
   }
 
   public static boolean copyFile(File from, File to, boolean bDeleteOld) {
     try {
       FileInputStream fIn = new FileInputStream(from);
       FileOutputStream fOut = new FileOutputStream(to);
       move(fIn, fOut, 128);
       fIn.close();
       fOut.close();
       if (bDeleteOld) {
         from.delete();
       }
       if (to.exists())
         return true;
     }
     catch (Exception e) {
       return false;
     }
     return false;
   }
 
   public static boolean cleanupFiles(File folder, final String suffix)
   {
     File[] f = folder.listFiles(new FilenameFilter() {
       public boolean accept(File dir, String name) {
         return name.endsWith("_" + suffix);
       }
     });
     for (int i = 0; (f != null) && (i < f.length); i++) {
       String fName = f[i].getName();
       if (fName.indexOf('_') != -1)
       {
         String[] s = splitString(fName, "_");
         long timeMS = parseLong(s[0], -1L);
         if (timeMS > 0L)
         {
           if (System.currentTimeMillis() - 604800000L > timeMS)
             try {
               f[i].delete();
             } catch (Exception e) {
               return false;
             }
         }
       }
     }
     return true;
   }
 
   public static int MAKEINT(byte a1, byte b1) {
     int a = a1 & 0xFF;
     int b = b1 & 0xFF;
     return 0xFFFF & (a | b << 8);
   }
 
   public static byte[] GETINT(int i) {
     byte[] ret = new byte[2];
     ret[0] = ((byte)(i & 0xFF));
     ret[1] = ((byte)(i >> 8 & 0xFF));
     return ret;
   }
 
   public static void GETINT(byte[] ret, int offset, int i) {
     ret[offset] = ((byte)(i & 0xFF));
     ret[(offset + 1)] = ((byte)(i >> 8 & 0xFF));
   }
 
   public static long MAKELONG(int a, int b) {
     return a | b << 16;
   }
 
   public static byte[] GETLONG(long l) {
     int a = (int)(l & 0xFFFF);
     int b = (int)(l >> 16 & 0xFFFF);
     byte[] ret = new byte[4];
     byte[] i1 = GETINT(a);
     byte[] i2 = GETINT(b);
     ret[0] = i1[0];
     ret[1] = i1[1];
     ret[2] = i2[0];
     ret[3] = i2[1];
     return ret;
   }
 
   public static String MAKESTRING(byte[] b, int offset, int len) {
     for (int i = offset + len - 1; i >= offset; i--) {
       if (b[i] == 0) {
         len--;
       }
     }
     return new String(b, offset, len);
   }
 
   public static long convertToLong(byte[] mydata, int iFrom, int len) {
     long l = 0L;
     int i = iFrom; for (int j = 0; i < iFrom + len; j++) {
       long ib = 0xFF & mydata[i];
       l |= ib << j * 8;
 
       i++;
     }
 
     return l;
   }
 
   public static String removeInvalidChars(String s)
   {
     StringBuffer sb = new StringBuffer(s.length());
     boolean blank = true;
     for (char i = '\000'; i < s.length(); i = (char)(i + '\001')) {
       char c = s.charAt(i);
       if ((c >= '0') && (c <= '9')) {
         sb.append(c);
         blank = false;
       }
       else if (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z'))) {
         sb.append(c);
         blank = false;
       }
       else
       {
         /*switch (c) {
         case '':
         case '’':
           c = '`';
         }*/
 
         switch (c) {
         case '-':
         case '_':
         case '`':
           sb.append(c);
           blank = false;
           break;
         default:
           if ((Character.isSpaceChar(c)) && 
             (!blank)) {
             sb.append(' ');
             blank = true;
           }
           break;
         }
       }
     }
     return sb.toString();
   }
 
   public static String initString(String s, int loop) {
     StringBuffer sb = new StringBuffer(s.length() * loop);
     for (int i = 0; i < loop; i++) {
       sb.append(s);
     }
     return sb.toString();
   }
 
   public static void moveFile(File from, File to) throws IOException {
     FileInputStream fIn = new FileInputStream(from);
     FileOutputStream fOut = new FileOutputStream(to);
     move(fIn, fOut, 1024);
     fIn.close();
     fOut.close();
   }
 
   public static String digest(File f)
   {
     try
     {
       MessageDigest md = MessageDigest.getInstance("MD5");
       md.reset();
       FileInputStream in = new FileInputStream(f);
       byte[] data = new byte[1024];
       int len = 0;
       while ((len = in.read(data)) > 0) {
         md.update(data, 0, len);
       }
       in.close();
       return getHexString(md.digest()); } catch (Exception e) {
     }
     return "";
   }
 
   public static float parseFloat(Object str, float def)
   {
     if (str == null) {
       return def;
     }
     if ((str instanceof Number)) {
       Number n = (Number)str;
       return n.floatValue();
     }
     return parseFloat(str.toString(), def);
   }
 
   public static float parseFloat(String _s, float ifNull) {
     try {
       return Float.parseFloat(_s); } catch (Exception e) {
     }
     return ifNull;
   }
 
   public static double parseDouble(Object str, double def)
   {
     if (str == null) {
       return def;
     }
     if ((str instanceof Number)) {
       Number n = (Number)str;
       return n.doubleValue();
     }
     return parseDouble(str.toString(), def);
   }
 
   public static double parseDouble(String name, double def) {
     if (name == null)
       return def;
     try
     {
       double bNeg = 1.0D;
       if (name.startsWith("-")) {
         bNeg = -1.0D;
         name = name.substring(1);
       } else if (name.startsWith("+")) {
         name = name.substring(1);
       }
       name = replaceString(name, ",", "");
       name = replaceString(name, "$", "");
       return Double.parseDouble(name) * bNeg; } catch (Exception e) {
     }
     return def;
   }
 
   public static String replaceString(String aString, String oldString, String newString, boolean isCaseSensitive)
   {
     if ((aString == null) || (oldString == null) || (newString == null)) {
       return aString;
     }
 
     int last = 0;
     int current = 0;
     int oldLength = oldString.length();
     StringBuffer sb = new StringBuffer();
 
     if (!isCaseSensitive) {
       String aStringU = aString.toUpperCase();
       String oldStringU = oldString.toUpperCase();
       while ((current = aStringU.indexOf(oldStringU, last)) != -1) {
         if (current > last) {
           sb.append(aString.substring(last, current));
         }
 
         sb.append(newString);
         last = current + oldLength;
       }
     } else {
       while ((current = aString.indexOf(oldString, last)) != -1)
       {
         String aStringU;
         String oldStringU;
         if (current > last) {
           sb.append(aString.substring(last, current));
         }
 
         sb.append(newString);
         last = current + oldLength;
       }
     }
     if (last != aString.length()) {
       sb.append(aString.substring(last, aString.length()));
     }
 
     return sb.toString();
   }
 
   public static int[] add(int[] array, int objToAdd) {
     int[] newArray = new int[array.length + 1];
     if (array.length > 0) {
       System.arraycopy(array, 0, newArray, 0, array.length);
     }
     newArray[array.length] = objToAdd;
     return newArray;
   }
 
   public static String joinString(int[] s, String what) {
     if ((s == null) || (s.length == 0)) {
       return "";
     }
     StringBuffer ret = new StringBuffer();
     for (int i = 0; i < s.length; i++) {
       if (i > 0) {
         ret.append(what);
       }
       ret.append(s[i]);
     }
     return ret.toString();
   }
 
   public static String toString(InputStream in) throws IOException {
     InputStreamReader isr = new InputStreamReader(in);
     return toString(isr);
   }
 
   public static String toString(Reader in) throws IOException {
     StringBuffer sb = new StringBuffer();
     toStringBuffer(sb, in, 128);
     return sb.toString();
   }
 
   public static String toString(InputStream in, int bufferSize) throws IOException {
     InputStreamReader isr = new InputStreamReader(in);
     return toString(isr, bufferSize);
   }
 
   public static String toString(Reader in, int bufferSize) throws IOException {
     StringBuffer sb = new StringBuffer();
     toStringBuffer(sb, in, bufferSize);
     return sb.toString();
   }
 
   public static int toStringBuffer(StringBuffer sb, Reader in) throws IOException {
     return toStringBuffer(sb, in, 128);
   }
 
   public static int toStringBuffer(StringBuffer sb, InputStream in) throws IOException {
     InputStreamReader isr = new InputStreamReader(in);
     return toStringBuffer(sb, isr, 128);
   }
 
   public static int toStringBuffer(StringBuffer sb, InputStream in, int bufferSize) throws IOException {
     InputStreamReader isr = new InputStreamReader(in);
     return toStringBuffer(sb, isr, bufferSize);
   }
 
   public static int toStringBuffer(StringBuffer sb, Reader in, int bufferSize) throws IOException {
     char[] buffer = new char[bufferSize < 1 ? 1 : bufferSize];
     int allCount = 0;
     int count;
     while ((count = in.read(buffer)) > -1)
     {
       allCount += count;
       sb.append(buffer, 0, count);
     }
     return allCount;
   }
 
   public static double[] add(double[] array, double objToAdd) {
     double[] newArray = new double[array.length + 1];
     if (array.length > 0) {
       System.arraycopy(array, 0, newArray, 0, array.length);
     }
     newArray[array.length] = objToAdd;
     return newArray;
   }
 
   public static String formatHoursDecFromSec(int sec) {
     return formatHoursDecFromSec(sec);
   }
 
   public static String formatHoursDecFromSec(long sec) {
     boolean neg = sec < 0L;
     if (neg) {
       sec = -sec;
     }
     String s = String.valueOf(sec / 3600L);
     long dec = sec % 3600L * 100L / 3600L;
     if (dec > 9L)
       s = s + "." + dec;
     else if (dec >= 0L) {
       s = s + ".0" + dec;
     }
     if (neg) {
       s = "-" + s;
     }
     return s;
   }
 
   public static String formatHoursDecMS(long ms) {
     return formatHoursDecFromSec(ms / 1000L);
   }
 
   public static String formatHoursMinFromSec(int sec) {
     int _sec = sec < 0 ? -sec : sec;
     int h = _sec / 3600;
     String s = String.valueOf(h);
     int m = _sec % 3600 / 60;
     if (m > 9)
       s = s + ":" + m;
     else if (m >= 0) {
       s = s + ":0" + m;
     }
     if (sec < 0) {
       s = "-" + s;
     }
     return s;
   }
 
   public static String formatHoursMinFromSec(long sec) {
     boolean neg = sec < 0L;
     if (neg) {
       sec = -sec;
     }
     String s = String.valueOf(sec / 3600L);
     long m = sec % 3600L / 60L;
     if (m > 9L)
       s = s + ":" + m;
     else if (m >= 0L) {
       s = s + ":0" + m;
     }
     if (neg) {
       s = "-" + s;
     }
     return s;
   }
 
   public static String formatHoursMinMS(long ms) {
     return formatHoursMinFromSec(ms / 1000L);
   }
 
   public static String formatHoursMinSec(long sec) {
     boolean neg = sec < 0L;
     if (neg) {
       sec = -sec;
     }
     String str = String.valueOf(sec / 3600L);
     long s = sec % 60L;
     long m = sec % 3600L;
     m /= 60L;
 
     if (m > 9L)
       str = str + ":" + m;
     else if (m >= 0L) {
       str = str + ":0" + m;
     }
     if (s > 9L)
       str = str + ":" + s;
     else if (s >= 0L) {
       str = str + ":0" + s;
     }
     if (neg) {
       str = "-" + str;
     }
     return str;
   }
 
   public static String format12HoursFromSec(int sec) {
     int h = sec / 3600;
     int m = sec % 3600 / 60;
 
     boolean am = true;
 
     if (h == 24) {
       h = 12;
       am = true;
     } else if (h > 12) {
       am = false;
       h -= 12;
     } else if (h == 12) {
       am = false;
     } else if (h == 0) {
       h = 12;
     }
 
     String s = String.valueOf(h);
     s = s + (m < 10 ? ":0" : ":") + m;
     s = s + (am ? "a" : "p");
     return s;
   }
 
   public static String format12HoursMS(long ms) {
     if (ms < 0L) {
       return "";
     }
     return format12HoursFromSec((int)(ms / 1000L));
   }
 
   public static String format24HoursFromSec(int sec) {
     int h = sec / 3600;
     int m = sec % 3600 / 60;
 
     String s = (h < 10 ? "0" : "") + h;
     s = s + (m < 10 ? ":0" : ":") + m;
     return s;
   }
 
   public static String format24HoursMS(long ms) {
     if (ms < 0L) {
       return "";
     }
     return format24HoursFromSec((int)(ms / 1000L));
   }
 }

/* Location:           C:\MW\classes\
 * Qualified Name:     zeyt.mw.MWUtils
 * JD-Core Version:    0.6.2
 */