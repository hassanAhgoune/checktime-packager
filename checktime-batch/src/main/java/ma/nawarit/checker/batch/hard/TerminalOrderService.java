
package ma.nawarit.checker.batch.hard;



public interface TerminalOrderService{
	
	public String doAddUser(Object[] cmdParams );
	public String doDeleteUser(Object[] cmdParams );
	public String doLock(Object[] cmdParams );
	public String doUnlock(Object[] cmdParams );

}
