package ma.nawarit.checker.batch.hard;

import org.apache.log4j.Logger;

import ma.nawarit.checker.batch.common.PropertiesReader;
import ma.nawarit.checker.batch.exec.ExecCmdUtils;




public final class TerminalOrderServiceBase implements TerminalOrderService{
	private Logger logger = Logger.getLogger(TerminalOrderServiceBase.class);
	private String zkFwkSDKExec;
	
	public String getZkFwkSDKExec() {
		return zkFwkSDKExec;
	}
	public void setZkFwkSDKExec(String zkFwkSDKExec) {
		this.zkFwkSDKExec = zkFwkSDKExec;
	}
	/**
	 * 
	 * @param actionName
	 * @param cmdParams
	 * @return
	 */
	private String doAction(String actionName, Object[] cmdParams) {
		StringBuffer buffer = new StringBuffer();
		String cmd = "";
		cmd = zkFwkSDKExec + " ";	
		cmd += PropertiesReader.getProperty("zk_fwk_sdk_"+actionName+"_cmd",cmdParams);
		buffer.append(cmd + "\n");
		ExecCmdUtils.exec(cmd,logger,buffer);
		return buffer.toString();
	}
	/**
	 * @param cmdParams
	 * @return
	 */
	public String doAddUser(Object[] cmdParams) {
		return doAction("addUser", cmdParams);
	}
	
	/**
	 * @param cmdParams
	 * @return
	 */
	public String doDeleteUser(Object[] cmdParams) {
		return doAction("deleteUser", cmdParams);
	}

	/**
	 * @param cmdParams
	 * @return
	 */
	public String doLock(Object[] cmdParams) {
		return doAction("lock", cmdParams);
	}

	/**
	 * @param cmdParams
	 * @return
	 */
	public String doUnlock(Object[] cmdParams) {
		return doAction("unlock", cmdParams);
	}
	
	public static void main(String[] args) {
		TerminalOrderServiceBase cmdExecutor = new TerminalOrderServiceBase();
		cmdExecutor.setZkFwkSDKExec("d:/zkcommander/Copager.exe");
		Object[] cmdParams = null;
		// ENROLL {IP , MATRICULE, BADGE, NOM, PRENOM}
		cmdParams = new Object[]{"192.168.1.201","999999","5555555555","ELBAAMRANI","JAMAL"};
		// LOCK {IP , MATRICULE, BADGE}
		//	cmdParams = new Object[]{"192.168.1.201","999999","5555555555"};
		// UNLOCK {IP , MATRICULE, BADGE}
		//cmdParams = new Object[]{"192.168.1.201","999999","5555555555"};
		String response = cmdExecutor.doAddUser(cmdParams);
		System.out.println(response);
		
		
	}
    
}
