/*-------------------------------------------
Security Settings
--------------------------------------------*/

   Notice = "DHTML Slide Tree Menu, Copyright (c) - 2001, OpenCube Inc. - www.opencube.com"
   code0 = 538
   code1 = 697
   sequence = "oc94127"



/*-------------------------------------------
Required menu Settings
--------------------------------------------*/

   menu_width = 200
   menu_height = 500

   scrolldelay = 10
   scrolljump = 10

   scrolldelay_nt = 5
   scrolljump_nt = 1;

   scrolldelay_ns4 = 50;
   scrolljump_ns4 = 6;	

   urltarget = "_self"
   start_open_index = "none"
   display_urls_in_statusbar = false

   onload_statement = "none"
   codebase = "script/"



/*-------------------------------------------
Required menu indenting and margins
--------------------------------------------*/

   main_itemheight = 29
   main_extend_height = 55

   sub_itemheight = "auto"
   sub1_indentwidth = 17
   sub2_indentwidth = 15

   main_marginleft = 2
   main_margin_topbottom = 0
   sub_marginleft = 0
   sub_margin_topbottom = 0



/*-------------------------------------------
Required font Settings
--------------------------------------------*/

   main_textcolor = "#ffffff"
   main_fontfamily = "Arial"
   main_fontsize = 12
   main_textdecoration = "normal"
   main_fontweight = "normal"
   main_fontstyle = "normal"
   main_hl_textcolor = "#ffffff"
   main_hl_textdecoration = "underline"

   sub_textcolor = "#DCDCDC"
   sub_fontfamily = "Arial"
   sub_fontsize = 12
   sub_textdecoration = "normal"
   sub_fontweight = "normal"
   sub_fontstyle = "normal"
   sub_hl_textcolor = "#ffffff"
   sub_hl_textdecoration = "underline"



/*---------------------------------------------
Required and optional background Settings
-----------------------------------------------*/

  
   /*---Required Settings---------*/ 

      background_color = "#A1BBDC"      

      main_item_bgcolor = "transparent"
      main_hl_item_bgcolor = "transparent"
  
      sub_item_bgcolor = "transparent"
      sub_hl_item_bgcolor = "transparent"


   /*---Optional Settings---------*/

      main_background_color = "#A1BBDC"
      sub_background_color = "#A1BBDC"

      main_background_image = "images/mainbg.gif"
      
      /*-----non tiled background image fix for ie5&up
      -------image should match menu dimensions-------*/ 	
      ie5fix_main_background_image = "images/mainbg.gif"	      


      sub_level1_background_image = "images/subbg.gif"
      sub_level2_background_image = "images/subbg.gif"

      /*-----non tiled background image fix for ie5&up
      -------images should match menu width and be
      -------equal to or greater than the longest sub menu*/
      ie5fix_sub_level1_background_image = "images/ie5fix_subbg.gif"
      ie5fix_sub_level1_bg_image_height = 138 	
      ie5fix_sub_level2_background_image = "images/ie5fix_subbg.gif"	      	
      ie5fix_sub_level2_bg_image_height = 138


      main_hl_background_image0 = "images/mainhl_bg0.gif"
      main_hl_background_wh0 = "161,29";	
      main_hl_background_image1 = "images/mainhl_bg1.gif"
      main_hl_background_wh1 = "161,29";
      main_hl_background_image2 = "images/mainhl_bg2.gif"
      main_hl_background_wh2 = "161,29";
      

      main_clipbg_after_items = false



/*-------------------------------------------
Required border settings -- set 'main_use_border'
and 'sub_use_border' to false for no borders
--------------------------------------------*/


   /*---All browsers Border settings-------------------*/
   
      main_use_border = false
      main_border_color = "transparent"
      main_hl_border_color = "#ffffff"

      sub_use_border = false;
      sub_border_color = "#ffffff"
      sub_hl_border_color = "#ff0000"


   /*---border offset distance (netscape 4.x bug workaround) 
        adjust untill right border is visible at edge----*/
      
      main_ns4fix_rightborder_offset = 0
      sub_ns4fix_rightborder_offset = -1


   /*---IE 5-up Only additional border settings---------*/
  
      main_use_2pixel_topbottom_border = false
      main_leftright_border_width = 1
      sub_leftright_border_width = 1



/*---------------------------------------------
Optional Icon Images
-----------------------------------------------*/

   icon_image0 = "images/down_arrow.gif"
   icon_image_wh0 = "30,50"
   icon_rollover0 = "images/down_arrow_hl.gif"
   icon_rollover_wh0 = "10,22"

   icon_image1 = "images/bullet.gif"
   icon_image_wh1 = "13,8"
   icon_rollover1 = "images/bullet_hl.gif"
   icon_rollover_wh1 = "13,8"

   icon_image2 = "images/sub_down_arrow.gif"
   icon_image_wh2 = "13,11"
   icon_rollover2 = "images/sub_down_arrow_hl.gif"
   icon_rollover_wh2 = "13,11"



/*----------------------------------------------
Optionsl applet Overide Settings and additional applet
specific parameters - prefix any available parameter
name with 'applet_' to overide the setting when
running in applet mode
------------------------------------------------*/


   /*------for design purposes only---------*/

      //set the following param to true for 
      //testing the script in applet form.    
      ie5up_run_as_applet = false
    

   /*------overriden script parameters---------*/
	
      applet_icon_index0 = 3
      applet_icon_index1 = 4
      applet_icon_index2 = 5


   /*------Additional Applet Specific Parameters--*/

      //applet_main_icon_indent = 0
      //applet_sub_icon_indent = 0

      applet_load_message_color = "#ffffff"
      applet_load_message = "Loading Images..."

      applet_main_font = "Helvetica, plain, 12" 
      applet_sub_font = "Helvetica, plain, 12"

   

/********************************************
THE FOLLOWING SECTIONS DEFINE THE ACTUAL
STRUCTURE AS WELL AS TEXT, URLS, AND ICONS 
THAT MAKE THE TREE MENU. - FOR HELP WITH 
INDEXING YOUR ITEMS REFER TO THE DOCUMENTATION.
*********************************************/

/*-------------------------------------------
Main Menu Item Settings
--------------------------------------------*/

main_text0 = "<table border=0 cellpadding=0 cellspacing=0><tr><td class=sectionbg>Liste des bases affect&eacute;es <span class=txtblue>(8)</span> :&nbsp;<img src=../../../bitmap/boutons/open.gif border=0 align=absmiddle></td></tr><tr><td colspan=2 height=1 bgcolor=#517EB5></td></tr><tr><td></td></tr><tr><td height=3 class=templ-bg></td></tr></table>"
//icon_index0 = 0

main_text1 = "<table border=0 cellpadding=0 cellspacing=0><tr><td class=sectionbg><b>Statut UC : </b></td></tr><tr><td><table width=100%  border=0 align=center cellpadding=1 cellspacing=0 class=boxtour-fournisseur><tr><td width=100% height=1 bgcolor=#B8CBE2></td></tr><tr><td height=22><b> En cours de cr&eacute;ation </b><span class=txtmedium></span></td></tr></table></td></tr><tr><td height=3 class=templ-bg></td></tr></table>"
//icon_index1 = 0

main_text2 = "<table border=0 cellpadding=0 cellspacing=0><tr><td class=sectionbg>Retour saisie :</td></tr><tr><td></td></tr></table><table width=200  border=0 align=center cellpadding=0 cellspacing=0><tr><td  class=treelink><img src=../../../bitmap/treeview/puce-blanche2.gif>&nbsp;<a href=../SI/CR-signature.htm target=_parent class=linktree>Signature</a></td></tr><tr><td class=treelink><img src=../../../bitmap/treeview/puce-blanche2.gif>&nbsp;<a href=../EC/CR-type-activite-nom.htm class=linktree >Classification</a></td></tr><tr><td class=treelink><img src=../../../bitmap/treeview/puce-blanche2.gif>&nbsp;<a href=../DG/CR-description-1.htm class=linktree >Donn�es g�n�rales</a></td></tr></table>"
//icon_index2 = 0



/*---------------------------------------------
Sub Menu Item Settings
-----------------------------------------------*/
subdesc0_0 = "<iframe src=CO-LI-Bases-frame.htm frameborder=0 width=99% height=150></iframe>"
icon_index0_0 = 2
