var DateValue = ""
var DateInputID = ""
var ObjInput
var fenetre = "";

function CalendarInput(oInput) {
	if (!fenetre || fenetre.closed) {
		ObjInput = oInput
		DateValue = oInput.value
		fenetre = showModelessDialog("calendar.html",window,"status:no;dialogLeft:" + event.screenX + ";dialogTop:" + event.screenY + ";dialogWidth:181px;dialogHeight:200px;resizable:no;scroll:no;help:no");
	} else {
			if (oInput.name != ObjInput.name) {
				if (fenetre) {
					fenetre.close();
					fenetre = null;
				}
				
				ObjInput = oInput
				DateValue = oInput.value
				fenetre = showModelessDialog("calendar.html",window,"status:no;dialogLeft:" + event.screenX + ";dialogTop:" + event.screenY + ";dialogWidth:181px;dialogHeight:200px;resizable:no;scroll:no;help:no");
			} else {
				fenetre.focus();
			}
	}
}

function DateUpdate() {
	ObjInput.value = DateValue
	if (fenetre) {
	     fenetre.close();
	     fenetre = null;
	}
}