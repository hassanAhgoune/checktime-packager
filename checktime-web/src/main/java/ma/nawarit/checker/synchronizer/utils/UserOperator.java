package ma.nawarit.checker.synchronizer.utils;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.NoeudImpl;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedProperty;
import java.util.Hashtable;
import java.util.List;

public class UserOperator {
    @ManagedProperty(value = "#{NoeudManageableService}")
    private transient static NoeudManageableService noeudService;

    @ManagedProperty(value = "#{UnitManageableService}")
    private transient static UnitManageableService unitService;

    @ManagedProperty(value = "#{ProfilMetierManageableService}")
    private transient static ProfilMetierManageableService profilMetierService;

    @ManagedProperty(value = "#{UserManageableService}")
    private transient static UserManageableService userService;

    private static final Logger log = Logger.getLogger(AbsenceOperator.class);

    public static void updateUser(String line) {
        String[] attrs = line.split(";");
        final String matricule = attrs[0],
                nom = attrs[1],
                prenom = attrs[2],
                managerMat = attrs[3],
                profilMetCode = attrs[4],
                unitCode = attrs[5],
                noeudCode = attrs[6];

        try {
            List<User> users = userService.read(new Hashtable() {{
                put("matricule", matricule);
            }});
            List<User> managers = userService.read(new Hashtable() {{
                put("matricule", managerMat);
            }});
            List<Noeud> noeuds = noeudService.read(new Hashtable() {{
                put("code", noeudCode);
            }});
            List<Unit> units = unitService.read(new Hashtable() {{
                put("code", unitCode);
            }});
            List<ProfilMetier> profils = profilMetierService.read(new Hashtable() {{
                put("code", profilMetCode);
            }});

            User user;
            if (users.size() > 0) {
                user = users.get(0);
            } else {
                user = new UserImpl();
                user.setDroitPaie(true);
                user.setActive(true);
            }
            user.setNom(nom);
            user.setPrenom(prenom);
            user.setSupH(managers.size() > 0 ? managers.get(0) : null);
            user.setNoeud(noeuds.size() > 0 ? noeuds.get(0) : null);
            user.setProfilMetier(profils.size() > 0 ? profils.get(0) : null);
            user.setUnit(units.size() > 0 ? units.get(0) : null);

        } catch (Exception e) {
            log.error("Error while proccessing absence syncronisation data : [" + line + "] - "
                    + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));

        }
    }

    public static void setNoeudService(NoeudManageableService noeudService) {
        UserOperator.noeudService = noeudService;
    }

    public static void setUnitService(UnitManageableService unitService) {
        UserOperator.unitService = unitService;
    }

    public static void setProfilMetierService(ProfilMetierManageableService profilMetierService) {
        UserOperator.profilMetierService = profilMetierService;
    }

    public static void setUserService(UserManageableService userService) {
        UserOperator.userService = userService;
    }
}
