package ma.nawarit.checker.synchronizer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.synchronizer.consumer.CongeConsumer;
import ma.nawarit.checker.synchronizer.provider.AbsenceProvider;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.ImportModel;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

public class CongeOperator {
	
	


	@ManagedProperty(value = "#{UserManageableService}")
	private transient static UserManageableService userService;
	
	@ManagedProperty(value = "#{CongeManageableService}")
	private transient static CongeManageableService congeService;
	
	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private transient static TypeCongeManageableService typeCongeService;

	private static final Logger log = Logger.getLogger(CongeConsumer.class);
	/**
	 * 
	 * @return
	 */
	public static void saveConge(String line) {
		try {
			//split the data
			DateFormat dateFormat=new SimpleDateFormat(PropertiesReader.getProperty("dateformat"));
			String[] cells = line.split(";");
			Date dateDebut = dateFormat.parse(cells[0]);
			double nbrJour = Double.valueOf(cells[1].trim().replace(',', '.'));
			String Matricule = cells[2].trim();
			String TypeConge = cells[3].trim();

			// get user ID by Matricule
			java.util.List userList = userService.getUsersByMatricule(Matricule);
			if (userList == null || userList.size()==0) {
				log.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
				return;
			}
			User user = (User) userList.get(0);

			TypeConge typeConge=getTypeConge(TypeConge);
			if(typeConge==null)
				return;
			
			int result;
			result = isExist(dateDebut, nbrJour, Matricule, TypeConge);
			if(result!=-1) {
				Conge conge = congeService.load(result);
				conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				congeService.update(conge);
				log.info("Conge updated "+conge.getId());
			}
			else {
				addConge(dateDebut, user, typeConge, nbrJour);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while proccessing conge syncronisation data : ["+line+"]"+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		
	}
	
	public static int saveConge(ImportModel model) {
		int cnt=0;
		try {
			// get user ID by Matricule
			java.util.List userList = userService.getUsersByMatricule(model.getMatricule());
			if (userList == null || userList.size()==0) {
				log.error("le Collaborateur de Matricule [" + model.getMatricule() + "] n'exist pas dans la base de donnes!");
				return 0;
			}
			User user = (User) userList.get(0);

			TypeConge typeConge=getTypeConge(model.getMotif());
			if(typeConge==null)
				return 0;
			
			int result;
			result = isExist(model.getDate(), model.getDuree(), model.getMatricule(), model.getMotif());
			if(result!=-1) {
				Conge conge = congeService.load(result);
				conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				congeService.update(conge);
				log.info("Conge updated "+conge.getId());
			}
			else {
				addConge(model.getDate(), user, typeConge, model.getDuree());
				cnt=1;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while proccessing conge syncronisation data : ["+model.toString()+"]"+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return cnt;
	}
	
	public static TypeConge getTypeConge(String code) {
		// get id type conge using code type conge
		TypeConge typeConge = null;
		try {
			Hashtable<Object, Object> properties = new Hashtable<Object, Object>();
			properties.put("libelle", code);
			java.util.List listTypeConge;
			listTypeConge = typeCongeService.read(properties);
			if (listTypeConge  == null) {
				log.error("unable to find type conge spesified : " + code);
			}else
			{
				typeConge = (TypeConge) listTypeConge.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while finding type conge : " + code);
		}
		return typeConge;
	}
	
	public static void addConge(Date dateDebut,User user,TypeConge typeConge,double nbrJour) {
		try {
			Conge conge = new CongeImpl();
			conge.setIdProcess(0);
			conge.setDateDebut(dateDebut);
			conge.setDateReprise(new Date());
			conge.setCollaborateur(user);
			conge.setTypeConge(typeConge);
			conge.setNbrJour(nbrJour);
			conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			congeService.create(conge);
			log.info("Conge created "+conge.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while creating new conge "+e);
		}
	}
	
	public static int isExist(Date date,double durre,String Matricule,String Motif) throws Exception {
		int result=-1;
		List conges=congeService.readByAdvancedCriteria(date, durre, Matricule, Motif);
		if(conges!=null) {
			Conge a=(Conge) conges.get(0);
			result=a.getId();
		}
		return result;
	}
	
	/**
	 * 
	 * @return
	 */
	
	
	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		CongeOperator.congeService = congeService;
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		CongeOperator.typeCongeService = typeCongeService;
	}
	
	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public static List<String> getProvidedConges(Date dateDebut, Date dateFin) {
		// TODO Auto-generated method stub
		List<String> conges = new ArrayList<String>();
		try {
			CommonCriteria criteria = new CommonCriteria();
			criteria.setDateDebut(dateDebut);
			criteria.setDateFin(dateFin);
			List<Conge> congeList = congeService.readByCriteria(criteria);
			if (congeList != null) {
				if(congeList.size() > 0) {
					for (Conge item : congeList) {
						String DateDebut = AbsenceProvider.DATE_FORMAT.format(item.getDateDebut());
						String Durre = String.valueOf(item.getNbrJour());
						String Matricule;
						Matricule = userService.load(item.getCollaborateur().getId()).getMatricule();
						String MotifConge = typeCongeService.load(item.getTypeConge().getId()).getLibelle();
						conges.add(DateDebut + ";" + Durre + ";" + Matricule + ";" + MotifConge);
					}
				}
			}
			else {
				log.info("No conge has been found for : "+AbsenceProvider.DATE_FORMAT.format(dateDebut));
			}

		} catch (Exception e) {
			log.error("Unable to generate conge list : " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return conges;
	}
}
