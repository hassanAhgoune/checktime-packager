package ma.nawarit.checker.synchronizer.utils;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.NoeudImpl;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedProperty;
import java.util.Hashtable;
import java.util.List;

public class UnitOrgOperator {
    @ManagedProperty(value = "#{NoeudManageableService}")
    private transient static NoeudManageableService noeudService;
    private static final Logger log = Logger.getLogger(AbsenceOperator.class);

    public static void updateUnitOrg(String line) {
        String[] attrs = line.split(";");
        final String code = attrs[0];
        String label = attrs[1];
        Integer status = Integer.valueOf(attrs[2]);

        try {
            List<Noeud> noeuds = noeudService.read(new Hashtable() {{
                put("code", code);
            }});

            Noeud noeud;
            if (noeuds.size() > 0) {
                noeud = noeuds.get(0);
                noeud.setLibelle(label);
                noeudService.update(noeud);
            } else {
                noeud = new NoeudImpl();
                noeud.setCode(code);
                noeud.setLibelle(label);
                noeudService.create(noeud);
            }

        } catch (Exception e) {
            log.error("Error while proccessing absence syncronisation data : [" + line + "] - "
                    + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));

        }
    }

    public static void setNoeudService(NoeudManageableService noeudService) {
        UnitOrgOperator.noeudService = noeudService;
    }
}
