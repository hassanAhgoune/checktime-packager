package ma.nawarit.checker.synchronizer.provider;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedProperty;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import ma.nawarit.checker.fileutils.fileProvider;
import ma.nawarit.checker.fileutils.fileProviderImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.synchronizer.Provider;
import ma.nawarit.checker.synchronizer.consumer.AbsenceConsumer;
import ma.nawarit.checker.synchronizer.consumer.CongeConsumer;
import ma.nawarit.checker.synchronizer.utils.AbsenceOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

public class AbsenceProvider implements Provider {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2932975702013593540L;
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat(PropertiesReader.getProperty("dateformat"), Locale.ENGLISH);
	public static final String EXPORT_PATH=PropertiesReader.getProperty("export.path");
	private static final Logger log = Logger.getLogger(AbsenceProvider.class);

	public void provide() {
		try {
			log.info("Absence Provider");
			//generate csv file name with date and path
			Date today = new Date();
			String fileDate = AbsenceProvider.DATE_FORMAT.format(today);
			String fileName = AbsenceProvider.EXPORT_PATH+"\\absence\\absences." + fileDate + "."+CongeConsumer.FILE_EXTENTION;
			File f = new File(fileName);
			//Verify the existance of the file
			if (!f.exists()) {
				PrintWriter writer;
				writer = new PrintWriter(fileName, "UTF-8");
				writer.println("DateDebut;Duree;Matricule;Motif");
				writer.close();
			}
			//get all absence between two dates and not autorised
			List<String> listToExport = new ArrayList<String>();
			listToExport=AbsenceOperator.getProvidedAbsences(today, today);
			if(listToExport !=null) {
				if(listToExport.size()>0) {
					PrintWriter writer;
					writer = new PrintWriter(fileName, "UTF-8");
					for (String line : listToExport) {
						writer.println(line);
					}
					writer.close();
				}
			}
			//export the csv file that containes the list of absences
		} catch (Exception e) {
			log.error("unable to create file of absence : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

}
