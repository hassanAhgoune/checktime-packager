/**
 * 
 */
package ma.nawarit.checker.synchronizer.provider;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.util.Log;

import ma.nawarit.checker.synchronizer.Provider;
import ma.nawarit.checker.synchronizer.consumer.CongeConsumer;
import ma.nawarit.checker.synchronizer.utils.CongeOperator;

/**
 * @author NAWAR
 *
 */
public class CongeProvider implements Provider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see ma.nawarit.checker.synchronizer.Provider#provide()
	 */

	private static final Logger log = Logger.getLogger(CongeConsumer.class);

	public void provide() {
		// TODO Auto-generated method stub
		try {
			log.info("Conge provider");
			// generate csv file name with date and path
			Date today = new Date();
			String fileDate = AbsenceProvider.DATE_FORMAT.format(today);
			String fileName = AbsenceProvider.EXPORT_PATH + "\\conge\\conges." + fileDate + "."
					+ CongeConsumer.FILE_EXTENTION;
			File f = new File(fileName);
			// Verify the existance of the file
			if (!f.exists()) {
				PrintWriter writer;
				writer = new PrintWriter(fileName, "UTF-8");
				writer.println("DateDebut;NbrJour;Matricule;Motif");
				writer.close();
			}
			// get all absence between two dates and not autorised
			List<String> listToExport = new ArrayList<String>();
			listToExport = CongeOperator.getProvidedConges(today, today);
			if (listToExport != null) {
				if (listToExport.size() > 0) {
					PrintWriter writer;
					writer = new PrintWriter(fileName, "UTF-8");
					for (String line : listToExport) {
						writer.println(line);
					}
					writer.close();
				}
			}

			// export the csv file that containes the list of absences
		} catch (Exception e) {
			Log.error("unable to create file of conges : " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

}
