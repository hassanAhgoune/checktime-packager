package ma.nawarit.checker.synchronizer.utils;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.NoeudImpl;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.PlanningImpl;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.faces.bean.ManagedProperty;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;

public class AbsValidOperator {

    @ManagedProperty(value = "#{PlanningManageableService}")
    private static transient PlanningManageableService planningService;

    @ManagedProperty(value = "#{AbsenceManageableService}")
    private static transient AbsenceManageableService absenceService;

    @ManagedProperty(value = "#{JrFerieManageableService}")
    private static transient JrFerieManageableService jrFerieService;

    @ManagedProperty(value = "#{CongeManageableService}")
    private static transient CongeManageableService congeService;

    @ManagedProperty(value = "#{UserManageableService}")
    private static transient UserManageableService userService;

    @ManagedProperty(value = "#{TypeAbsenceManageableService}")
    private static transient TypeAbsenceManageableService typeAbsenceService;

    private static DateFormat dateFormat = new SimpleDateFormat("DD/MM/yy HH:mm");

    private static final Logger log = Logger.getLogger(AbsenceOperator.class);

    public static void updateAbsValide(String line) {
        String[] attrs = line.split(";");
        final String matricule = attrs[0];
        final String type = attrs[1];
        String dateD = attrs[2];
        String dateF = attrs[3];
        String heureD = attrs[4];
        String heureF = attrs[5];

        try {
            Date dateTimeF = dateFormat.parse(dateF + " " + heureF);
            Date dateTimeD = dateFormat.parse(dateD + " " + heureD);
            List<User> users = userService.read(new Hashtable() {{
                put("matricule", matricule);
            }});
            if(users.isEmpty()) {
                throw new Exception("Pas de collaborateur par ce matricule: "+ matricule);
            }

            Absence absence = new AbsenceImpl();

            absence.setCollaborateur(users.get(0));
            absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
            absence.setDateDebut(dateTimeD);
            absence.setDateReprise(dateTimeF);

            CommonCriteria crit = new CommonCriteria();
            crit.setDateDebut(absence.getDateDebut());
            crit.setDateFin(absence.getDateReprise());
            crit.setMatriculeInf(matricule);
            crit.setMatriculeSup(matricule);

            List<Absence> abs = absenceService.readByCriteria(crit);

            if(abs != null && !abs.isEmpty())
                return;
            absence.setDuree(compterHeure(absence, users.get(0)));
            List<TypeAbsence> types = typeAbsenceService.read(new Hashtable(){{
                put("code", type);
            }});

            if(types.isEmpty())
                throw new Exception("Pas de type d'absence par ce code: "+ type);

            absence.setTypeAbsence(types.get(0));
            absenceService.create(absence);
        } catch (Exception e) {
            log.error("Error while proccessing absence syncronisation data : [" + line + "] - "
                    + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));

        }
    }


    private static double compterHeure(Absence abs, User user)  throws Exception{
        double cont = 0;
            Day d = null;
            Calendar currentDayDate = new GregorianCalendar();
            currentDayDate.setTime(abs.getDateDebut());
            Calendar end = new GregorianCalendar();
            end.setTime(abs.getDateReprise());
            cont = 0;
            Calendar start = new GregorianCalendar();
            start.setTime(abs.getDateDebut());
            while (currentDayDate.compareTo(end) <= 0) {
                cont += 0;
                d = new Day();
                d.setDate(currentDayDate.getTime());
                PlanningImpl planning = (PlanningImpl) user.getPlanningAffected();
                if (planning.getId() > 0)
                    planning = (PlanningImpl) planningService.load(planning.getId());
                d.setPlanning(planning);

                if (!isJrRepos(d, abs, user)) {
                    cont = Utils.additionHeures(cont, d.getP_tmcontract());
                    Calendar firstPlageDebut = d.getHrFixes().get(0).getDebut();
                    Calendar lastPlageFin = d.getHrFixes().get(d.getHrFixes().size() - 1).getFin();
                    if (isSameDay(start, firstPlageDebut)) {
                        if (start.get(Calendar.HOUR_OF_DAY) > 0 &&
                                start.get(Calendar.HOUR_OF_DAY) >= firstPlageDebut.get(Calendar.HOUR_OF_DAY)) {
                            System.out.println("debut: " + start.getTime() + "\nday start: " + firstPlageDebut.getTime());
                            double plagedebutStartDiff = Utils.differenceHeures(Utils.getHourDoubleFormat(start.getTime()),
                                    Utils.getHourDoubleFormat(firstPlageDebut.getTime()));
                            cont = Utils.differenceHeures(cont, plagedebutStartDiff);
                        }
                    }
                    if (isSameDay(end, lastPlageFin)) {
                        if (end.get(Calendar.HOUR_OF_DAY) > 0 &&
                                end.get(Calendar.HOUR_OF_DAY) <= lastPlageFin.get(Calendar.HOUR_OF_DAY)) {
                            System.out.println("debut: " + end.getTime() + "\nday end: " + lastPlageFin.getTime());
                            double plagefinEndDiff = Utils.differenceHeures(Utils.getHourDoubleFormat(end.getTime()),
                                    Utils.getHourDoubleFormat(lastPlageFin.getTime()));
                            cont = Utils.differenceHeures(cont, plagefinEndDiff);
                        }
                    }
                }
                //				else {
                //					return cont++;
                //				}
                currentDayDate.add(Calendar.DAY_OF_MONTH, 1);
            }

        return cont;
    }

    private static boolean isSameDay(Calendar date1, Calendar date2) {
        return date1.get(Calendar.DAY_OF_YEAR) == date2.get(Calendar.DAY_OF_YEAR) &&
                date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR);
    }

    private static boolean isJrRepos(Day day, Absence abs, User user) throws Exception {
        if (day.isJrRepos())
            return true;
        List<JrFerie> jrFeries = jrFerieService.readAll();
        Object o = Utils.isJrFerie(day.getDate(), jrFeries);
        if (o != null) {
            day.setLibelle(((JrFerie) o).getLibelle());
            day.setNature(o);
            day.setJrRepos(true);
            return true;
        }
        Hashtable<String, Object> props = new Hashtable<String, Object>();
        props.put("statut", Globals.WORKFLOW_STATUS_ELAPSED);
        props.put("collaborateur", user);
        List<Conge> conges = congeService.read(props);
        o = Utils.isHolidays(day.getDate(), conges);
        if (o != null) {
            day.setLibelle("conge");
            day.setNature(o);
            day.setJrRepos(true);
            return true;
        }
        return false;
    }

    public void setPlanningService(PlanningManageableService planningService) {
        this.planningService = planningService;
    }

    public void setAbsenceService(AbsenceManageableService absenceService) {
        this.absenceService = absenceService;
    }

    public void setJrFerieService(JrFerieManageableService jrFerieService) {
        this.jrFerieService = jrFerieService;
    }

    public void setCongeService(CongeManageableService congeService) {
        this.congeService = congeService;
    }

    public void setUserService(UserManageableService userService) {
        this.userService = userService;
    }

    public void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
        this.typeAbsenceService = typeAbsenceService;
    }
}
