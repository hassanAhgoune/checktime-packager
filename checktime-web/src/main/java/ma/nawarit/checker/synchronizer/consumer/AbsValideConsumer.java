package ma.nawarit.checker.synchronizer.consumer;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.utils.AbsValidOperator;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedProperty;
import java.util.List;

public class AbsValideConsumer implements Consumer {
    private static final String LINE_FORMAT = PropertiesReader.getProperty("abs_validated.lineformat");
	public static final String DIRECTORY_PATH=PropertiesReader.getProperty("abs_validated.import.path");
	public static final String FILE_EXTENTION=PropertiesReader.getProperty("file.format");
    private static final Logger log = Logger.getLogger(AbsValideConsumer.class);

    public void consume() {

        log.info("Absence valide consumer");

		List<String> lines = FileOperator.loadFiles(DIRECTORY_PATH, FILE_EXTENTION, LINE_FORMAT);
        if (lines != null) {
            for (String line : lines) {

                AbsValidOperator.updateAbsValide(line);
            }
        } else {
            log.info("No Absence found to be Added");
        }
    }

}
