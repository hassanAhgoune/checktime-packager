package ma.nawarit.checker.synchronizer.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.RetardImpl;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.synchronizer.consumer.CongeConsumer;
import ma.nawarit.checker.synchronizer.provider.AbsenceProvider;
import ma.nawarit.checker.utils.Globals;

public class RetardOperator {
	
	@ManagedProperty(value = "#{UserManageableService}")
	private transient static UserManageableService userService;
	
	@ManagedProperty(value = "#{RetardManageableService}")
	private transient static RetardManageableService retardService;

	private static final Logger log = Logger.getLogger(RetardOperator.class);
	
	/**
	 * 
	 * @param dateDebut
	 * @param Matricule
	 * @param duree
	 */
	
	public static void saveRetard(Date dateDebut, String Matricule, double duree){
		try {
			Double Duree=duree;
			java.util.List userList = userService.getUsersByMatricule(Matricule);
			if (userList == null) {
				log.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
				return;
			}
			User user = (User) userList.get(0);
			
			
			// l'ajout du conge au base de données
			int result=isExist(Matricule, dateDebut, Duree.intValue());
			if(result!=-1) {
				Retard retard = retardService.load(result);
				retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				retardService.update(retard);
				log.info("Retard updated ID : "+retard.getId());
			}
			else
			{
				RetardOperator.addRetard(user, dateDebut, Duree.intValue());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.info("Error ocurred while processing a retard : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
			
	}
	
	//check the retard if exist
	public static int isExist(String Matricule, Date dateDebut, int Duree) {
		log.info("Check retard if exist ["+Matricule+"] ; ["+Duree+"]");
		int result=-1;
		List retards=RetardOperator.retardService.readByAdvancedCriteria(Matricule, dateDebut, Duree);
		if(retards!=null) {
			Retard r=(Retard) retards.get(0);
			result=r.getId();
		}
		return result;
	}
	
	public static void addRetard(User user, Date dateDebut, int duree) {

		log.info("Add Retard ["+user.getMatricule()+"] ; ["+duree+"]");
		try {
			
			Retard retard = new RetardImpl();
			retard.setUser(user);
			retard.setMatricule(user.getMatricule());
			retard.setIndexPlage(1);
			retard.setDate(dateDebut);
			retard.setHorNuit(false);
			retard.setRetardTolere(duree);
			retard.setDescription("Retard Toléré de "+duree+" min");
			retard.setRetardNonTolere(0);
			retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			retardService.create(retard);
			log.info("Retard created ID : "+retard.getId());
		} catch (Exception e) {
			log.info("Error occured while creating new retard "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	
	public static List<String> getProvidedRetards(Date dateDebut, Date dateFin) {
		// TODO Auto-generated method stub
		List<String> conges = new ArrayList<String>();
		try {
			CommonCriteria criteria = new CommonCriteria();
			criteria.setDateDebut(dateDebut);
			criteria.setDateFin(dateFin);
			List<Retard> retardList = retardService.readByCriteria(criteria);
			if (retardList != null) {
				if(retardList.size() > 0) {
					for (Retard item : retardList) {
						String DateDebut = AbsenceProvider.DATE_FORMAT.format(item.getDate());
						String Durre = String.valueOf(item.getRetardNonTolere()+item.getRetardTolere());
						String Matricule = userService.load(item.getUser().getId()).getMatricule();
						String Statut=item.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE)?"Autorisé":"Non Autorisé";
						conges.add(DateDebut + ";" + Durre + ";" + Matricule + ";" + Statut);
					}
				}
			}
			else {
				log.info("No Retard has been found for : "+AbsenceProvider.DATE_FORMAT.format(dateDebut));
			}

		} catch (Exception e) {
			log.error("Unable to generate Retard list : " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return conges;
	}
	
	/**
	 * 
	 * @return
	 */
	
	public static UserManageableService getUserService() {
		return userService;
	}

	public static void setUserService(UserManageableService userService) {
		RetardOperator.userService = userService;
	}

	public static RetardManageableService getRetardService() {
		return retardService;
	}

	public static void setRetardService(RetardManageableService retardService) {
		RetardOperator.retardService = retardService;
	}

}
