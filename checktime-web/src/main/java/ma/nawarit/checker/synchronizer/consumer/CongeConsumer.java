/**
 * 
 */
package ma.nawarit.checker.synchronizer.consumer;

import java.util.List;

import org.apache.log4j.Logger;

import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.utils.CongeOperator;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

/**
 * @author NAWAR
 *
 */
public class CongeConsumer implements Consumer {


	/**
	 * 
	 */
	private static final long serialVersionUID = 2077205123057638728L;
	public static final String DIRECTORY_PATH=PropertiesReader.getProperty("import.path");
	public static final String FILE_EXTENTION=PropertiesReader.getProperty("file.format");
	private static final String LINE_FORMAT=PropertiesReader.getProperty("conge.lineformat");
	private static final Logger log = Logger.getLogger(CongeConsumer.class);
	/* (non-Javadoc)
	 * @see ma.nawarit.checker.synchronizer.Consumer#consume()
	 */
/*
	@Override
	public void consume() {
		log.info("Conge consumer");
		List<String> lines=FileOperator.loadFiles(CongeConsumer.DIRECTORY_PATH+"\\conge",CongeConsumer.FILE_EXTENTION,CongeConsumer.LINE_FORMAT);
		if(lines!=null) {
			System.out.println("Conge I entred But lines size : "+lines.size());
			for (String line : lines) {
				CongeOperator.saveConge(line);
			}
		}
		else {
			log.info("No Conge found to be Added");
		}
	}
	*/
	public void consume() {
		// TODO Auto-generated method stub
		log.info("Conge consumer");
		List<String> lines=FileOperator.loadFiles(CongeConsumer.DIRECTORY_PATH+"\\conge",CongeConsumer.FILE_EXTENTION,CongeConsumer.LINE_FORMAT);
		if(lines!=null) {
			System.out.println("Conge I entred But lines size : "+lines.size());
			for (String line : lines) {
				CongeOperator.saveConge(line);
			}
		}
		else {
			log.info("No Conge found to be Added");
		}
	}

}
