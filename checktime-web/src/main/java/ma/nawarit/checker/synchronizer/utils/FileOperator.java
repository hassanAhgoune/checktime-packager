package ma.nawarit.checker.synchronizer.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.consumer.CongeConsumer;

public class FileOperator {
	
	private static final Logger log = Logger.getLogger(FileOperator.class);
	
	public static java.util.List loadFiles(String directory,String extention,String lineFormat){
		log.info("Load Files from : "+directory);
		List<String> lines=new ArrayList<String>();
		try {
			// TODO Auto-generated method stub
			lines=FileOperator.distinct(FileOperator.getLines(directory, lineFormat, extention));
		} catch (Exception e) {
			log.error("Error loading files "+e);
		}
		return lines;
	}
	
	public static java.util.List getLines(String directory,String lineFormat, String extention){
		List<String> lines=new ArrayList<String>();
		try {
			File file=new File(directory);
			if(file.isDirectory()) {
				List<InputStream> inputList=new ArrayList<InputStream>();
				for (File item : file.listFiles()) {
					if(FilenameUtils.getExtension(item.getName()).equals("csv")) {
						log.error("Load file : "+item.getName());
						InputStream input = new FileInputStream(item);
						inputList.add(input);
					}
				}
				SequenceInputStream is=new SequenceInputStream(Collections.enumeration(inputList));
				BufferedReader reader=new BufferedReader(new InputStreamReader(is, Consumer.CHARSET));
				String line;
			      while((line = reader.readLine()) != null && !line.isEmpty()){
			    	  if(isLineValide(line, lineFormat))
			    		  lines.add(line);
			      }
			}else {
				log.error("Configuration Error, directory path is not correct : ["+directory+"]");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("Directory not found : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("Unable to read file : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}catch(Exception e) {
			// TODO Auto-generated catch block
			log.error("Error occured while loading lines from file : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return lines;
	}
	
	public static java.util.List getLines(InputStream input, String lineFormat){
		List<String> lines=new ArrayList<String>();
		try {
			if(input!=null) {
				Reader read = new InputStreamReader(input);
				BufferedReader reader=new BufferedReader(read);
				String line;
			      while((line = reader.readLine()) != null && !line.isEmpty()){
			    	  if(isLineValide(line, lineFormat))
			    		  lines.add(line);
			      }
			}else {
				log.error("Configuration Error, directory path is not correct : ");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.error("Directory not found : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.error("Unable to read file : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}catch(Exception e) {
			// TODO Auto-generated catch block
			log.error("Error occured while loading lines from file : "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return lines;
	}
	
	public static java.util.List distinct(List<String> lines){
		List<String> Lines=new ArrayList<String>();
		//delete duplicated values
	      int []hashedLines=new int[lines.size()];
	      //hash the list of string with md5
	      for (int i = 0; i < lines.size(); i++) {
			hashedLines[i]=lines.get(i).hashCode();
      		}
	      //detect the duplicate
	      Boolean exist=false;
	      for (int i = 0; i < hashedLines.length; i++) {
	    	  exist=false;
	    	  for (int j = i+1; j < hashedLines.length;j++ ) {
					if(hashedLines[i]==hashedLines[j] && i!=j) {
						exist=true;
					}
				}
				if(!exist)
					Lines.add(lines.get(i));
			}
		return Lines;
	}
	
	public static Boolean isLineValide(String line,String pattern) {
		Boolean valide=true;
		if (!line.matches(pattern)) {
			valide = false;
			log.error("Line ["+line+"] is not correct please check the format (("+pattern+"))");
		}
		return valide;
	}
}
