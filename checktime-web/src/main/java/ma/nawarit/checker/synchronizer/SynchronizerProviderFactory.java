/**
 * 
 */
package ma.nawarit.checker.synchronizer;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

/**
 * @author NAWAR
 *
 */
public class SynchronizerProviderFactory {

	private static final Logger log = Logger.getLogger(SynchronizerProviderFactory.class);
	
	public final static String packageNAme="ma.nawarit.checker.synchronizer.provider.";
	
	public static List<Provider> getProviders() {
		List<Provider> providers=new ArrayList<Provider>();
		List<String> list=getClasses();
		if(list!=null && list.size()>0) {
			for (String item : list) {
				try {
					Class<Provider> c = (Class<Provider>) Class.forName(packageNAme+item);;
					providers.add(c.newInstance());
				} catch (ClassNotFoundException e) {
					log.error("Error while getting providers "+e.getStackTrace());
				} catch (InstantiationException e) {
					log.error("Error while getting providers "+e.getStackTrace());
				} catch (IllegalAccessException e) {
					log.error("Error while getting providers "+e.getStackTrace());
				}
			}
		}else {
			log.warn("No provider has been found");
		}
		return providers;
	}
	
	public static List<String> getClasses() {
		List<String> list=new ArrayList<String>();
		try {
			String property=PropertiesReader.getProperty("provider.classes");
			if(property.equals("") || property.equals(null))
				log.info("No Classes to syncronize exist");
			else
			{
				String[] classes=property.split(",");
				for (String classe : classes) {
					list.add(classe.trim());
				}
			}
		}
		catch(Exception e) {
			log.error("unable to load classes names "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return list;
	}
}
