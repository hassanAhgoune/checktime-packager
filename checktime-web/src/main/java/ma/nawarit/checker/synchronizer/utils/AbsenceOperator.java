package ma.nawarit.checker.synchronizer.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.faces.bean.ManagedProperty;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.synchronizer.provider.AbsenceProvider;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.ImportModel;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

public class AbsenceOperator {

	@ManagedProperty(value = "#{UserManageableService}")
	private transient static UserManageableService userService;

	@ManagedProperty(value = "#{AbsenceManageableService}")
	private transient static AbsenceManageableService absenceService;

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private transient static TypeAbsenceManageableService typeAbsenceService;

	private static final Logger log = Logger.getLogger(AbsenceOperator.class);

	/**
	 * 
	 * @return
	 */
	public static void saveAbsence(String line) {
		try {
			// split the data
			DateFormat dateFormat = new SimpleDateFormat(PropertiesReader.getProperty("dateformat"));
			String[] cells = line.split(";");
			Date dateDebut = dateFormat.parse(cells[0]);
			Double duree = Double.valueOf(cells[1].trim().replace(',', '.'));
			String Matricule = cells[2].trim();
			String typeAbsence = cells[3].trim();

			if (duree * 60 > Double.valueOf(PropertiesReader.getProperty("absence.minDuree"))) {
				// get user ID by Matricule
				java.util.List userList = userService.getUsersByMatricule(Matricule);
				if (userList == null || userList.size() == 0) {
					log.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
				} else {
					User user = (User) userList.get(0);
					TypeAbsence typeabsence = getTypeAbsence(typeAbsence);
					if (typeabsence == null)
						log.error("Type Absence [" + typeAbsence + "] Not found!");
					else {
						int result;
						result = isExist(dateDebut, duree, Matricule, typeAbsence);
						if (result != -1) {
							Absence absence = absenceService.load(result);
							absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
							absenceService.update(absence);
							log.info("Absence updated " + absence.getId());
						} else {
							addAbsence(dateDebut, user, typeabsence, duree);
						}
					}
				}
			} else {
				RetardOperator.saveRetard(dateDebut, Matricule, duree*60);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while proccessing absence syncronisation data : [" + line + "] - " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	
	public static int saveAbsence(ImportModel model) {
		int count=0;
		try {
			if (model.getDuree() * 60 > Double.valueOf(PropertiesReader.getProperty("absence.minDuree"))) {
				// get user ID by Matricule
				java.util.List userList = userService.getUsersByMatricule(model.getMatricule());
				if (userList == null || userList.size() == 0) {
					log.error("le Collaborateur de Matricule [" + model.getMatricule() + "] n'exist pas dans la base de donnes!");
				} else {
					User user = (User) userList.get(0);
					TypeAbsence typeabsence = getTypeAbsence(model.getMotif());
					if (typeabsence == null)
						log.error("Type Absence [" + model.getMotif() + "] Not found!");
					else {
						int result;
						result = isExist(model.getDate(), model.getDuree(), model.getMatricule(), model.getMotif());
						if (result != -1) {
							Absence absence = absenceService.load(result);
							absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
							absenceService.update(absence);
							log.info("Absence updated " + absence.getId());
						} else {
							addAbsence(model.getDate(), user, typeabsence, model.getDuree());
							count=1;
						}
					}
				}
			} else {
				RetardOperator.saveRetard(model.getDate(), model.getMatricule(), model.getDuree()*60);
				count=-1;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while proccessing absence syncronisation data : [" + model.toString() + "] - " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return count;
	}

	public static TypeAbsence getTypeAbsence(String code) {
		// get id type conge using code type conge
		TypeAbsence typeAbsence = null;
		try {
			Hashtable<Object, Object> properties = new Hashtable<Object, Object>();
			properties.put("libelle", code);
			java.util.List typesAbsence = typeAbsenceService.read(properties);
			if (typesAbsence == null || typesAbsence.size() == 0) {
				log.error("unable to find type absence specified : " + code);
			} else {
				typeAbsence = (TypeAbsence) typesAbsence.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while finding type absence : " + code);
		}
		return typeAbsence;
	}

	public static void addAbsence(Date dateDebut, User user, TypeAbsence typeabsence, double duree) {
		try {
			Absence absence = new AbsenceImpl();
			absence.setCollaborateur(user);
			absence.setHorNuit(false);
			absence.setHeureDebut(0);
			absence.setHeureReprise(0);
			absence.setTypeAbsence(typeabsence);
			absence.setDateDebut(dateDebut);
			absence.setIdProcess(0);
			absence.setDateReprise(dateDebut);
			absence.setDuree(duree);
			absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			absenceService.create(absence);
			log.info("Absence created  absence ID : " + absence.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error("Error while creating new absence " + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

	public static int isExist(Date dateDebut, double durre, String Matricule, String Motif) throws Exception {
		int result = -1;
		List absences = absenceService.readByAdvancedCriteria(dateDebut, durre, Matricule, Motif);
		if (absences != null) {
			Absence a = (Absence) absences.get(0);
			result = a.getId();
		}
		return result;
	}

	public static List<String> getProvidedAbsences(Date dateDebut, Date dateFin) {
		List<String> absences = new ArrayList<String>();
		try {
			CommonCriteria criteria = new CommonCriteria();
			criteria.setDateDebut(dateDebut);
			criteria.setDateFin(dateFin);
			criteria.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
			List<Absence> absenceList = absenceService.readByCriteria(criteria);
			if (absenceList != null) {
				if(absenceList.size() > 0) {
					for (Absence item : absenceList) {
						String DateDebut = AbsenceProvider.DATE_FORMAT.format(item.getDateDebut());
						String Durre = String.valueOf(item.getDuree());
						String Matricule;
						Matricule = userService.load(item.getCollaborateur().getId()).getMatricule();
						String MotifABS = typeAbsenceService.load(item.getTypeAbsence().getId()).getCode();
						absences.add(DateDebut + ";" + Durre + ";" + Matricule + ";" + MotifABS);
					}
				}
			}
			else {
				log.info("No absence has been found for : "+AbsenceProvider.DATE_FORMAT.format(dateDebut));
			}

		} catch (Exception e) {
			log.error("Unable to generate absence list : " +org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return absences;
	}

	/**
	 * 
	 * @return
	 */

	public static UserManageableService getUserService() {
		return userService;
	}

	public static void setUserService(UserManageableService userService) {
		AbsenceOperator.userService = userService;
	}

	public static AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public static void setAbsenceService(AbsenceManageableService absenceService) {
		AbsenceOperator.absenceService = absenceService;
	}

	public static TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public static void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
		AbsenceOperator.typeAbsenceService = typeAbsenceService;
	}

}
