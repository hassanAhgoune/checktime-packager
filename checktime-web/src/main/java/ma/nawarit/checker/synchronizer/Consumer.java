/**
 * 
 */
package ma.nawarit.checker.synchronizer;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.List;

import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

/**
 * @author NAWAR
 *
 */
public interface Consumer extends Serializable{

	public final static String FILE_PATH=PropertiesReader.getProperty("import.path");
	public final static String FILE_FORMAT=PropertiesReader.getProperty("file.format");
	public final static Charset CHARSET = Charset.forName("UTF-8");
	public void consume();
}
