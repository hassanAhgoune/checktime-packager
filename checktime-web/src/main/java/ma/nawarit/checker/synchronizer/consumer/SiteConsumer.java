package ma.nawarit.checker.synchronizer.consumer;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.utils.AbsenceOperator;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.synchronizer.utils.SiteOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import org.apache.log4j.Logger;

import java.util.List;

public class SiteConsumer implements Consumer {
    private static final String LINE_FORMAT = PropertiesReader.getProperty("site.lineformat");
    private static final Logger log = Logger.getLogger(AbsenceConsumer.class);

    public void consume() {

        log.info("Site(Unité) consumer");
        List<String> lines = FileOperator
                .loadFiles(CongeConsumer.DIRECTORY_PATH+"\\SITE", CongeConsumer.FILE_EXTENTION, LINE_FORMAT);
        if (lines != null) {
            for (String line : lines) {
                SiteOperator.updateSite(line);
            }
        } else {
            log.info("No Absence found to be Added");
        }
    }
}
