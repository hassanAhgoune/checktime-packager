package ma.nawarit.checker.synchronizer.consumer;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.synchronizer.utils.UnitOrgOperator;
import ma.nawarit.checker.synchronizer.utils.UserOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import org.apache.log4j.Logger;

import java.util.List;

public class UserConsumer implements Consumer {
    private static final String LINE_FORMAT = PropertiesReader.getProperty("user.lineformat");
    private static final Logger log = Logger.getLogger(AbsenceConsumer.class);

    public void consume() {

        log.info("User (Collaborateur) consumer");
        List<String> lines = FileOperator
                .loadFiles(CongeConsumer.DIRECTORY_PATH + "\\COLLABORATEUR", CongeConsumer.FILE_EXTENTION,
                        LINE_FORMAT);
        if (lines != null) {
            for (String line : lines) {
                UserOperator.updateUser(line);
            }
        } else {
            log.info("No Absence found to be Added");
        }
    }
}
