package ma.nawarit.checker.synchronizer.utils;

import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.faces.bean.ManagedProperty;
import java.util.Hashtable;
import java.util.List;

public class SiteOperator {
    @ManagedProperty(value = "#{UnitManageableService}")
    private transient static UnitManageableService unitService;
    private static final Logger log = Logger.getLogger(AbsenceOperator.class);

    public static void updateSite(String line) {
        String[] attrs = line.split(";");
        final String code = attrs[0];
        String label = attrs[1];
        Integer status = Integer.valueOf(attrs[2]);
        final String ville = attrs.length > 3 ? attrs[3] : "";

        try {
            List<Unit> units = unitService.read(new Hashtable() {{
                put("code", code);
            }});
            Unit villeUnit = null;
            if (StringUtils.isNotEmpty(ville)) {
                List<Unit> villes = unitService.read(new Hashtable() {{
                    put("libelle", ville);
                }});
                if (villes.size() > 0) {
                    villeUnit = villes.get(0);
                } else {
                    villeUnit = new UnitImpl();
                    villeUnit.setCode(ville);
                    villeUnit.setLibelle(ville);
                    villeUnit.setDescription("");
                    unitService.create(villeUnit);
                }
            }

            Unit unit;
            if (units.size() > 0) {
                unit = units.get(0);
                unit.setLibelle(label);
                unit.setUnit(villeUnit);
                unitService.update(unit);
            } else {
                unit = new UnitImpl();
                unit.setCode(code);
                unit.setLibelle(label);
                unit.setUnit(villeUnit);
                unit.setDescription("");
                unitService.create(unit);
            }

        } catch (Exception e) {
            log.error("Error while proccessing absence syncronisation data : [" + line + "] - "
                    + org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));

        }
    }

    public static void setUnitService(UnitManageableService unitService) {
        SiteOperator.unitService = unitService;
    }
}
