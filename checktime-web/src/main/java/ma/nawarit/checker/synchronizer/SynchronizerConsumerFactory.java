/**
 * 
 */
package ma.nawarit.checker.synchronizer;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import ma.nawarit.checker.synchronizer.consumer.AbsValideConsumer;
import org.apache.log4j.Logger;

import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

import javax.faces.bean.ManagedProperty;

/**
 * @author NAWAR
 *
 */
public class SynchronizerConsumerFactory {

	private static final Logger log = Logger.getLogger(SynchronizerConsumerFactory.class);
	public final static String packageNAme="ma.nawarit.checker.synchronizer.consumer.";

	@ManagedProperty(value = "#{AbsValideConsumer}")
	private transient Consumer absValideConsumer;

	@SuppressWarnings("unchecked")
	public  List<Consumer> getConsumers() {
		// TODO Auto-generated method stub
		
		List<Consumer> consumers=new ArrayList<Consumer>();
		
			for (String item : getClasses()) {
				try {
					Class<Consumer> c = (Class<Consumer>) Class.forName(packageNAme+item);
					if(c.equals(AbsValideConsumer.class))
						consumers.add(absValideConsumer);
					else
						consumers.add(c.newInstance());
				} catch (ClassNotFoundException e) {
					log.error("Error while getting consumer "+e.getStackTrace());
				} catch (InstantiationException e) {
					log.error("Error while getting consumer "+e.getStackTrace());
				} catch (IllegalAccessException e) {
					log.error("Error while getting consumer "+e.getStackTrace());
				}
				
			}
		
		return consumers;
	}
	
	public static List<String> getClasses() {
		List<String> list=new ArrayList<String>();
		try {
			String property=PropertiesReader.getProperty("consumer.classes");
			if(property.equals("") || property.equals(null))
				log.info("No Classes to syncronize exist");
			else
			{
				String[] classes=property.split(",");
				for (String classe : classes) {
					list.add(classe.trim());
				}
			}
		}
		catch(Exception e) {
			log.error("unable to load classes names "+org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		return list;
	}

	public void setAbsValideConsumer(Consumer absValideConsumer) {
		this.absValideConsumer = absValideConsumer;
	}
}
