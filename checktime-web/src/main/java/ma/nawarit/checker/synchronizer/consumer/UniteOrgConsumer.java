package ma.nawarit.checker.synchronizer.consumer;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.synchronizer.utils.SiteOperator;
import ma.nawarit.checker.synchronizer.utils.UnitOrgOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import org.apache.log4j.Logger;

import java.util.List;

public class UniteOrgConsumer implements Consumer {
    private static final String LINE_FORMAT = PropertiesReader.getProperty("unit_org.lineformat");
    private static final Logger log = Logger.getLogger(UniteOrgConsumer.class);

    public void consume() {

        log.info("Unité Org(Noeud) consumer");
        List<String> lines = FileOperator
                .loadFiles(CongeConsumer.DIRECTORY_PATH+"\\", CongeConsumer.FILE_EXTENTION, LINE_FORMAT);
        if (lines != null) {
            for (String line : lines) {
                UnitOrgOperator.updateUnitOrg(line);
            }
        } else {
            log.info("No Absence found to be Added");
        }
    }
}
