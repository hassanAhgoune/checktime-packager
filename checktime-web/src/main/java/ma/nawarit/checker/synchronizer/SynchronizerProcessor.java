/**
 * 
 */
package ma.nawarit.checker.synchronizer;

import java.util.List;

import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

import javax.faces.bean.ManagedProperty;

/**
 * @author NAWAR
 *
 */
public class SynchronizerProcessor {

	@ManagedProperty(value = "#{SynchronizerConsumerFactory}")
	private transient SynchronizerConsumerFactory synchronizerConsumerFactory;

	public void consume() {
		List<Consumer> Consumers =  synchronizerConsumerFactory.getConsumers();
		for (Consumer consumer : Consumers) {
			consumer.consume();
		}
	}
	
	public void provide() {
		List<Provider> providers=SynchronizerProviderFactory.getProviders();
		if(true) return;
		for (Provider provider : providers) {
			provider.provide();
		}
	}

	public void setSynchronizerConsumerFactory(SynchronizerConsumerFactory synchronizerConsumerFactory) {
		this.synchronizerConsumerFactory = synchronizerConsumerFactory;
	}
}
