/**
 * 
 */
package ma.nawarit.checker.synchronizer;

import java.io.Serializable;

/**
 * @author NAWAR
 *
 */
public interface Provider extends Serializable {
	public void provide();
}
