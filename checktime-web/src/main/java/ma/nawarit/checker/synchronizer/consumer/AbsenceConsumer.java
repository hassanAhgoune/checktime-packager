package ma.nawarit.checker.synchronizer.consumer;

import java.util.List;

import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.Provider;
import ma.nawarit.checker.synchronizer.utils.AbsenceOperator;
import ma.nawarit.checker.synchronizer.utils.CongeOperator;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

public class AbsenceConsumer implements Consumer {



	private static final String LINE_FORMAT=PropertiesReader.getProperty("absence.lineformat");
	private static final Logger log = Logger.getLogger(AbsenceConsumer.class);

	public void consume() {
		// TODO Auto-generated method stub
		log.info("Absence consumer");
		List<String> lines=FileOperator.loadFiles(CongeConsumer.DIRECTORY_PATH+"\\absence",CongeConsumer.FILE_EXTENTION,AbsenceConsumer.LINE_FORMAT);
		if(lines!=null) {
			for (String line : lines) {
				AbsenceOperator.saveAbsence(line);
			}
		}
		else {
			log.info("No Absence found to be Added");
		}
	}

}
