package ma.nawarit.checker.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import ma.nawarit.checker.core.common.Utils;

public class DateTimeConverter implements Converter {


	/** Default construction */
	public DateTimeConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().length() == 0) return new Character('0');
		
		return str2TimeFormat(value);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		String toReturn ="";
		if (value == null) {
			return toReturn;
		}

		if (value instanceof Double) {
			toReturn = str2TimeFormat(value.toString());
		}
		

		return toReturn;
	}
	private String str2TimeFormat(String value){
		return Utils.str2TimeFormat(value);
	}
}