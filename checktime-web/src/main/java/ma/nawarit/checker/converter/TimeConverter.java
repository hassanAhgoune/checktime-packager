package ma.nawarit.checker.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import ma.nawarit.checker.utils.Globals;

public class TimeConverter implements Converter {


	/** Default construction */
	public TimeConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().length() == 0) return new Character('0');
		
		return str2TimeFormat(value);
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		String toReturn ="";
		if (value == null) {
			return toReturn;
		}

		if (value instanceof Double) {
			toReturn = str2TimeFormat(value.toString());
		}
		

		return toReturn;
	}
	private String str2TimeFormat(String value){
		int index = value.indexOf(".");
		if(index != -1){
			String hour = value.substring(0,index);
			String min = value.substring(index+1);
			hour = ((hour.length()==1) ? "0" + hour : hour) + Globals.HOUR_STR;
			min = ((min.length()==1)? min + "0" : min) + Globals.MIN_STR;
			return hour + min;
		}
		return value;
	}
}