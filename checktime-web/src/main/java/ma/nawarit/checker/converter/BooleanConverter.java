package ma.nawarit.checker.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;

public class BooleanConverter implements Converter {

	/** Default construction */
	public BooleanConverter() {
	}

	public Object getAsObject(FacesContext context, UIComponent component, String value) throws ConverterException {
		if (value == null || value.trim().length() == 0) return new Character('N');

		Character toReturn = (value.equalsIgnoreCase("true")) ? 'Y' : 'N';
		return toReturn;
	}

	public String getAsString(FacesContext context, UIComponent component, Object value) throws ConverterException {
		String toReturn = "false";

		if (value == null) {
			return toReturn;
		}

		if (value instanceof Boolean) {
			toReturn = (((Boolean) value) == true) ? MessageFactory.getMessage(Globals.BOOLEAN_YES) : MessageFactory.getMessage(Globals.BOOLEAN_NO);
		}
		else if (value instanceof Character) {
			char toChar = ((Character) value).charValue();
			toReturn = (toChar == 'Y' || toChar == 'y') ?  MessageFactory.getMessage(Globals.BOOLEAN_YES) : MessageFactory.getMessage(Globals.BOOLEAN_NO);
		}

		return toReturn;
	}
}