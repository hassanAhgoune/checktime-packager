package ma.nawarit.checker.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.faces.application.FacesMessage;

import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.AnnotationImpl;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableServiceBase;


@FacesConverter(forClass = ma.nawarit.checker.configuration.Annotation.class ,value = "annotationConverter")
public class AnnotationConverter implements Converter {
	
	@ManagedProperty(value = "#{AnnotationManageableService}")
	private AnnotationManageableService annotationService = new AnnotationManageableServiceBase();

	private List<Annotation> annotations = new ArrayList<Annotation>();
	
	private Annotation annotation = new AnnotationImpl();

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		if (value.trim().equals("")) {
            return null;
        } else {
        	annotation.setDescription(value);
            return  annotation;
        }
	    }         
	

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		if(value == null || value.equals("")){
			return "";
		}else{
		
		return String.valueOf(((Annotation) value).getDescription());
		}
	}

	
	public List<Annotation> retourList(){

		try {
			annotations = annotationService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annotations;
	}

	public AnnotationManageableService getAnnotationService() {
		return annotationService;
	}


	public void setAnnotationService(AnnotationManageableService annotationService) {
		this.annotationService = annotationService;
	}


	public List<Annotation> getAnnotations() {
		return annotations;
	}


	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}


	public Annotation getAnnotation() {
		return annotation;
	}


	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}
	
	

}
