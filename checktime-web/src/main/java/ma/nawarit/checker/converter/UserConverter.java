package ma.nawarit.checker.converter;


import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.primefaces.component.picklist.PickList;
import org.primefaces.model.DualListModel;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;



@FacesConverter(forClass = User.class ,value = "userConverter")
public class UserConverter implements Converter {

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		PickList  p = (PickList) component;
        DualListModel dl = (DualListModel) p.getValue();
        for (Object o: dl.getSource()) {
        	User u = (User)o;
        	if (u.getId() == Integer.valueOf(value))
        		return u;
        }
        return null;
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
        return  String.valueOf(((User)value).getId());
	}
}
