package ma.nawarit.checker.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.TypeAbsenceImpl;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableServiceBase;

@FacesConverter(forClass = ma.nawarit.checker.suivi.TypeAbsence.class ,value = "typeAbsenceConverter")
public class TypeAbsenceConverter implements Converter {
	
	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsenceService = new TypeAbsenceManageableServiceBase();

	private List<TypeAbsence> typeAbsences = new ArrayList<TypeAbsence>();
	
	private TypeAbsence typeAbsence = new TypeAbsenceImpl();

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		if (value.trim().equals("")) {
            return null;
        } else {
        	typeAbsence.setId(Integer.parseInt(value));
            return  typeAbsence;
        }
	    }         
	

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		if(value == null || value.equals("")){
			return "";
		}else{
		
		return String.valueOf(((TypeAbsence) value).getId());
		}
	}
	
	public List<TypeAbsence> retourList(){

		try {
			typeAbsences = typeAbsenceService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typeAbsences;
	}


	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}


	public void setTypeAbsenceService(
			TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}


	public List<TypeAbsence> getTypeAbsences() {
		return typeAbsences;
	}


	public void setTypeAbsences(List<TypeAbsence> typeAbsences) {
		this.typeAbsences = typeAbsences;
	}


	public TypeAbsence getTypeAbsence() {
		return typeAbsence;
	}


	public void setTypeAbsence(TypeAbsence typeAbsence) {
		this.typeAbsence = typeAbsence;
	}
}