package ma.nawarit.checker.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.ProfilMetierImpl;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableServiceBase;
import ma.nawarit.checker.configuration.Annotation;

@FacesConverter("profilConverter")
public class ProfilConverter implements Converter {
 
	@ManagedProperty(value = "#{ProfilMetierManageableService}")
	private ProfilMetierManageableService annotationService = new ProfilMetierManageableServiceBase();

	private List<ProfilMetier> annotations = new ArrayList<ProfilMetier>();
	
	private ProfilMetier annotation = new ProfilMetierImpl();

	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		
		if (value.trim().equals("")) {
            return null;
        } else {
        	annotation.setId(Integer.parseInt(value));
            return  annotation;
        }
	    }         
	

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		if(value == null || value.equals("")){
			return "";
		}else{
		
		return String.valueOf(((ProfilMetier) value).getId());
		}
	}

	
	public List<ProfilMetier> retourList(){

		try {
			annotations = annotationService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annotations;
	}


	public ProfilMetierManageableService getAnnotationService() {
		return annotationService;
	}


	public void setAnnotationService(ProfilMetierManageableService annotationService) {
		this.annotationService = annotationService;
	}


	public List<ProfilMetier> getAnnotations() {
		return annotations;
	}


	public void setAnnotations(List<ProfilMetier> annotations) {
		this.annotations = annotations;
	}


	public ProfilMetier getAnnotation() {
		return annotation;
	}


	public void setAnnotation(ProfilMetier annotation) {
		this.annotation = annotation;
	}
}
