package ma.nawarit.checker.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.TypeCongeImpl;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableServiceBase;

@FacesConverter(forClass = ma.nawarit.checker.suivi.TypeConge.class ,value = "typeCongeConverter")
public class TypeCongeConverter implements Converter {

	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeService = new TypeCongeManageableServiceBase();

	private List<TypeConge> typesConge = new ArrayList<TypeConge>();
	
	private TypeConge typeConge= new TypeCongeImpl();
	
	
	public Object getAsObject(FacesContext context, UIComponent component,
			String value) {
		if (value.trim().equals("")) {
            return null;
        } else {
        	typeConge.setId(Integer.parseInt(value));
            return  typeConge;
        }
	}

	public String getAsString(FacesContext context, UIComponent component,
			Object value) {
		
		if(value == null || value.equals("")){
			return "";
		}else{
		
		return String.valueOf(((TypeConge) value).getId());
		}
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		this.typeCongeService = typeCongeService;
	}

	public List<TypeConge> getTypesConge() {
		return typesConge;
	}

	public void setTypesConge(List<TypeConge> typesConge) {
		this.typesConge = typesConge;
	}

	public TypeConge getTypeConge() {
		return typeConge;
	}

	public void setTypeConge(TypeConge typeConge) {
		this.typeConge = typeConge;
	}
}