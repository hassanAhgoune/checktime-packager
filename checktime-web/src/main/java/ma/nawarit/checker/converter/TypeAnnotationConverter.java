package ma.nawarit.checker.converter;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import ma.nawarit.checker.configuration.TypeAnnotation;
import ma.nawarit.checker.configuration.TypeAnnotationImpl;
import ma.nawarit.checker.configuration.crud.TypeAnnotationManageableService;
import ma.nawarit.checker.configuration.crud.TypeAnnotationManageableServiceBase;

@FacesConverter(forClass = ma.nawarit.checker.configuration.TypeAnnotation.class ,value = "typeAnnotationConverter")
public class TypeAnnotationConverter implements Converter {
		
		@ManagedProperty(value = "#{TypeAnnotationManageableService}")
		private TypeAnnotationManageableService typeAnnotationService = new TypeAnnotationManageableServiceBase();

		private List<TypeAnnotation> typeAnnotations = new ArrayList<TypeAnnotation>();
		
		private TypeAnnotation typeAnnotation = new TypeAnnotationImpl();

		public Object getAsObject(FacesContext context, UIComponent component,
				String value) {
			
			if (value.trim().equals("")) {
	            return null;
	        } else {
	        	typeAnnotation.setId(Integer.parseInt(value));
	            return  typeAnnotation;
	        }
		    }         
		

		public String getAsString(FacesContext context, UIComponent component,
				Object value) {
			
			if(value == null || value.equals("")){
				return "";
			}else{
			
			return String.valueOf(((TypeAnnotation) value).getId());
			}
		}

		
		public List<TypeAnnotation> retourList(){

			try {
				typeAnnotations = typeAnnotationService.readAll();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return typeAnnotations;
		}


		public TypeAnnotationManageableService getTypeAnnotationService() {
			return typeAnnotationService;
		}


		public void setTypeAnnotationService(
				TypeAnnotationManageableService typeAnnotationService) {
			this.typeAnnotationService = typeAnnotationService;
		}


		public List<TypeAnnotation> getTypeAnnotations() {
			return typeAnnotations;
		}


		public void setTypeAnnotations(List<TypeAnnotation> typeAnnotations) {
			this.typeAnnotations = typeAnnotations;
		}


		public TypeAnnotation getTypeAnnotation() {
			return typeAnnotation;
		}


		public void setTypeAnnotation(TypeAnnotation typeAnnotation) {
			this.typeAnnotation = typeAnnotation;
		}
}