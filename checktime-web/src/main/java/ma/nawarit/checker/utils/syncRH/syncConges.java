package ma.nawarit.checker.utils.syncRH;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedProperty;
import org.apache.log4j.Logger;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.fileutils.fileConsumer;
import ma.nawarit.checker.fileutils.fileConsumerImpl;
import ma.nawarit.checker.fileutils.fileProvider;
import ma.nawarit.checker.fileutils.fileProviderImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.utils.Globals;

public class syncConges {
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;

	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService;

	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeService;

	final static Logger logger = Logger.getLogger(syncConges.class);
	public final String patern = "\\d{4}-\\d{2}-\\d{2};((0|([1-9][0-9]*))((\\.|\\,)[0-9]+)?);([0-9]{1,4});([\\w]+)";
	public final String dateFormat = "yyyy-MM-dd";
	public final DateFormat Dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

	public syncConges() {

	}

	public void CongesConsumer() throws Exception {
		propertiesOperator propOP=new propertiesOperator();
		fileConsumer file = new fileConsumerImpl();
		Date today = new Date();
		String reportDate = Dateformat.format(today);
		String fileName = "conges." + reportDate + ".csv";
		File f = new File(propOP.getCongeConsumerPath() + fileName);

		// Check if today file exist or not

		if (!f.exists()) {
			logger.warn("fichier de conge de [" + reportDate + "]  n'exist pas!");
			return;
		}

		// Read all Conge from csv file
		List<String> lines = file.generateListFromCSVfile(propOP.getCongeConsumerPath() + fileName);
		if (lines.size() == 0) {
			logger.info("liste des nouvaux conges est vide");
			return;
		}

		// validation du format de fichier conges
		if (!validate(lines)) {
			logger.error("le fichier des conges est de format in correct!!");
			return;
		}
		// read all lines in list of conges and insert it in database
		Hashtable<Object, Object> properties = new Hashtable<Object, Object>();
		for (String item : lines) {
			String[] cells = item.split(";");
			Date date_Conge = Dateformat.parse(cells[0]);
			double nbrJour = Double.valueOf(cells[1].replace(',', '.'));
			String Matricule = cells[2];
			String TypeConge = cells[3];

			// get user ID by Matricule
			java.util.List userList = userService.getUsersByMatricule(Matricule);
			if (userList == null) {
				logger.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
				return;
			}
			User user = (User) userList.get(0);

			// get id type conge using code type conge
			properties.put("code", TypeConge);
			java.util.List listTypeConge = typeCongeService.read(properties);
			if (listTypeConge  == null) {
				logger.error("Code de type conge n'exist pas dans la base de données : " + TypeConge);
				return;
			}
			TypeConge tc = (TypeConge) listTypeConge.get(0);

			
			int result=isExist(date_Conge, nbrJour, Matricule, TypeConge);
			if(result!=-1) {
				Conge conge = congeService.load(result);
				conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				congeService.update(conge);
				logger.info("Conge created");
			}
			else {
				Conge conge = new CongeImpl();
				conge.setIdProcess(0);
				conge.setDateDebut(date_Conge);
				conge.setDateReprise(new Date());
				conge.setCollaborateur(user);
				conge.setTypeConge(tc);
				conge.setNbrJour(nbrJour);
				conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				congeService.create(conge);
				logger.info("Conge created");
			}
		}

	}
	

	public void CongesProvider(Date date_debut, Date date_fin) throws Exception {
		propertiesOperator propOP=new propertiesOperator();
		Date today = new Date();
		String reportDate = Dateformat.format(today);
		String fileName = "conges." + reportDate + ".csv";
		File f = new File(propOP.getCongeProviderPath() + fileName);
		if (!f.exists()) {
			PrintWriter writer = new PrintWriter(propOP.getCongeProviderPath() + fileName, "UTF-8");
			writer.println("DateDebut;NbrJours;Matricule;MotifConge");
			writer.close();
		}
		List<Conge> congeList = congeService.readByCriteria(date_debut, date_fin);
		ArrayList<String> listToExport = new ArrayList<String>();
		for (Conge conge : congeList) {
			String line = Dateformat.format(conge.getDateDebut()) + ";" + conge.getNbrJour() + ";"
					+ conge.getCollaborateur().getMatricule() + ";" + conge.getTypeConge().getCode();
			listToExport.add(line);
		}
		fileProvider fileprovider = new fileProviderImpl();
		fileprovider.generateCSVfile(propOP.getCongeProviderPath() + fileName, listToExport);
	}

	public Boolean validate(List<String> lines) {
		Boolean valide = true;
		for (String item : lines) {
			String[] cells = item.split(";");
			if (cells.length != 4) {
				valide = false;
				break;
			}
			if (!item.matches(patern)) {
				valide = false;
				logger.error("la format des donnes dans le fichier de conge est in correct!");
				break;
			}
		}
		return valide;
	}

	public int isExist(Date dateRetard,double durre,String Matricule,String Motif) throws Exception {
		int result=-1;
		List conges=congeService.readByAdvancedCriteria(dateRetard, durre, Matricule, Motif);
		if(conges!=null) {
			Conge a=(Conge) conges.get(0);
			result=a.getId();
		}
		return result;
	}
	
	
	
	/*
	 * Getter and setter
	 */

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		this.typeCongeService = typeCongeService;
	}
}
