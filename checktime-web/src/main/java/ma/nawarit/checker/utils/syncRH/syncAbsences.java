package ma.nawarit.checker.utils.syncRH;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedProperty;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.fileutils.fileConsumer;
import ma.nawarit.checker.fileutils.fileConsumerImpl;
import ma.nawarit.checker.fileutils.fileProvider;
import ma.nawarit.checker.fileutils.fileProviderImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.utils.Globals;

public class syncAbsences {
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;

	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService;

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsenceService;

	final static Logger logger = Logger.getLogger(syncAbsences.class);
	public final String patern = "\\d{4}-\\d{2}-\\d{2};((0|([1-9][0-9]*))((\\.|\\,)[0-9]+)?);([0-9]{1,4});([\\w]+)";
	public final String dateFormat = "yyyy-MM-dd";
	public final DateFormat Dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

	public syncAbsences() {

	}

	public void AbsencesConsumer() throws Exception {
		propertiesOperator propOP=new propertiesOperator();
		fileConsumer file = new fileConsumerImpl();
		Date today = new Date();
		String reportDate = Dateformat.format(today);
		String fileName = "absences." + reportDate + ".csv";
		File f = new File(propOP.getAbsenceConsumerPath() + fileName);

		// Check if today file exist or not

		if (!f.exists()) {
			logger.warn("fichier d'absences de [" + reportDate + "]  n'exist pas!");
			return;
		}

		// Read all absence from csv file
		List<String> lines = file.generateListFromCSVfile(propOP.getAbsenceConsumerPath() + fileName);
		if (lines.size() == 0) {
			logger.info("liste des nouvaux absences est vide");
			return;
		}

		// validation du format de fichier conges
		if (!validate(lines)) {
			logger.error("le fichier d'absence est de format incorrect!!");
			return;
		}
		// read all lines in list of conges and insert it in database
		Hashtable<Object, Object> properties = new Hashtable<Object, Object>();
		for (String item : lines) {
			String[] cells = item.split(";");
			Date date_absence = Dateformat.parse(cells[0]);
			double duree = Double.valueOf(cells[1].replace(',', '.'));
			String Matricule = cells[2];
			String motifAbsence = cells[3];

			// get user ID by Matricule
			java.util.List userList = userService.getUsersByMatricule(Matricule);
			if (userList == null) {
				logger.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
				return;
			}
			User user = (User) userList.get(0);

			// get id type conge using code type conge
			properties.put("code", motifAbsence);
			java.util.List listMotifAbsence = typeAbsenceService.read(properties);
			if (listMotifAbsence == null) {
				logger.error("Code de type absence n'exist pas dans la base de données : " + motifAbsence);
				return;
			}
			
			TypeAbsence motifABS = (TypeAbsence) listMotifAbsence.get(0);

			// l'ajout du conge au base de données
			int result=isExist(date_absence, duree, user.getMatricule(), motifABS.getCode());
			if(result!=-1) {
				Absence absence=absenceService.load(result);
				absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				absenceService.update(absence);
				logger.error("Absence updated absence ID : "+absence.getId());
			}else {
				Absence absence = new AbsenceImpl();
				absence.setCollaborateur(user);
				absence.setTypeAbsence(motifABS);
				absence.setDateDebut(date_absence);
				absence.setIdProcess(0);
				absence.setDateReprise(date_absence);
				absence.setDuree(duree);
				absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
				absenceService.create(absence);
				logger.info("Absence created  absence ID : "+absence.getId());
			}
		}
	}

	public void AbsencesProvider(Date date1, Date date2) {
		try {

			propertiesOperator propOP=new propertiesOperator();
			//generate csv file name with date and path
			Date today = new Date();
			String fileDate = Dateformat.format(today);
			String fileName = "absences." + fileDate + ".csv";
			File f = new File(propOP.getAbsenceProviderPath() + fileName);
			
			//Verify the existance of the file
			if (!f.exists()) {
				PrintWriter writer;
				writer = new PrintWriter(propOP.getAbsenceProviderPath() + fileName, "UTF-8");
				writer.println("DateDebut;Duree;Matricule;Motif");
				writer.close();
			}
			else
			{
				return;
			}
			
			//get all absence between two dates and not autorised
			CommonCriteria criteria=new CommonCriteria();
			criteria.setDateDebut(date1);
			criteria.setDateFin(date2);
			criteria.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
			List<Absence> absenceList=absenceService.readByCriteria(criteria);
			
			
			
			//Load absence list in list of string to write it in csv file
			ArrayList<String> listToExport = new ArrayList<String>();
			if(absenceList!=null)
			{
				for (Absence item : absenceList) {
					String DateDebut=Dateformat.format(item.getDateDebut());
					String Durre=String.valueOf(item.getDuree());
					String Matricule=userService.load(item.getCollaborateur().getId()).getMatricule();
					String MotifABS=typeAbsenceService.load(item.getTypeAbsence().getId()).getCode();
					//String line = Dateformat.format(item.getDateDebut()) + ";" + item.getDuree()+ ";"+ item.getCollaborateur().getMatricule() + ";" + item.getTypeAbsence().getCode();
					listToExport.add(DateDebut+";"+Durre+";"+Matricule+";"+MotifABS);
				}
			}
			
			
			//export the csv file that containes the list of absences
			fileProvider fileprovider = new fileProviderImpl();
			fileprovider.generateCSVfile(propOP.getAbsenceProviderPath() + fileName, listToExport);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * Validation des ligne de fichier d'absences
	 */
	public Boolean validate(List<String> lines) {
		Boolean valide = true;
		for (String item : lines) {
			String[] cells = item.split(";");
			if (cells.length != 4) {
				valide = false;
				break;
			}
			if (!item.matches(patern)) {
				valide = false;
				logger.error("la format des donnes dans le fichier de conge est in correct!");
				break;
			}
		}
		return valide;
	}
	
	public int isExist(Date dateRetard,double durre,String Matricule,String Motif) throws Exception {
		int result=-1;
		List absences=absenceService.readByAdvancedCriteria(dateRetard, durre, Matricule, Motif);
		if(absences!=null) {
			Absence a=(Absence) absences.get(0);
			result=a.getId();
		}
		return result;
	}
	
	/*
	 * Getters and setters
	 */

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

}
