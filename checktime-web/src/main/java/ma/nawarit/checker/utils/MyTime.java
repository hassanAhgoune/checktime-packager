package ma.nawarit.checker.utils;

public class MyTime {
	private String stime;
	private double dtime;
	
	public MyTime(int h,int m){
		if(m == 0) stime = h + ",00";
		stime = h + "," + m;
		dtime = Double.valueOf(h+"."+m);
	}
	public String getStime() {
		return stime;
	}
	public void setStime(String time) {
		stime = time;
	}
	public double getDtime() {
		return dtime;
	}
	public void setDtime(double time) {
		dtime = time;
	}

}
