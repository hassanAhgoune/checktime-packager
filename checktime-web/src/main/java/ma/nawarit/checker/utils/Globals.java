package ma.nawarit.checker.utils;

/**
 * Cette classe regroupe les constantes g�n�riques utilis�es dans les actions.
 *
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime</DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Cr�e le       :</B><DD> 18 Mars 08</DD>
 * </DL>
 * @since 2008
 * @version 0.1
 * @author k.Lamhaddab
 */
public class Globals {
	
	// Attributs string
	public static final String CODE_ATTRIBUTE = "code";
    public static final String LIBELLE_ATTRIBUTE = "libelle";
    public static final String NOM_ATTRIBUTE = "nom";
    public static final String PRENOM_ATTRIBUTE = "prenom";
    public static final String SITE_LIBELLE_ATTRIBUTE = "site.libelle";
    public static final String PROFILMETIER_LIBELLE_ATTRIBUTE = "profilMetier.libelle";
    public static final String ZONE_LIBELLE_ATTRIBUTE = "zone.libelle";
    public static final String LOGIN_ATTRIBUTE = "login";
    public static final String MAIL_ATTRIBUTE = "mail";
    public static final String BADGE_ATTRIBUTE = "badge";
    public static final String MATRICULE_ATTRIBUTE = "matricule";
    public static final String PHOTO_ATTRIBUTE = "photo";
    public static final String NBJOUR_ATTRIBUTE = "nbrejr";
    public static final String NB_JOUR_Panier_ATTRIBUTE = "nb_hour_panier";
    public static final String NB_JOUR_Trans_ATTRIBUTE = "nb_hour_transport";
    public static final String ID_ATTRIBUTE = "id";
    public static final String CREATE_MODE_STR = "create";
    public static final String UPDATE_MODE_STR = "update";
    public static final String NBPLHOR_ATTRIBUTE = "Nb de plages horaire";
    public static final String NBTRANCHE_ATTRIBUTE = "Nombre de tranches";
    public static final String PERIOD_ATTRIBUTE = "Period";
    public static final String TYPE_ATTRIBUTE = "type";
    public static final String DATED_ATTRIBUTE = "dated";
    public static final String DATE_ATTRIBUTE = "date";
    public static final String NAME_ATTRIBUTE = "name";
    
    public static final String POINTAGE_SHARED_LOCATION = "pointage_shared_location";
//    public static final String MAIL_SMTP_HOST = MessageFactory.getSetting("mail_smtp_host");
    public static final String REPOS = "Repos";
//    public static final String DEFAULT_USER_LOGIN = MessageFactory.getSetting("default_user_login");
//    public static final String DEFAULT_USER_PWD = MessageFactory.getSetting("default_user_pwd");
//    public static final String DEFAULT_USER_MAIL = MessageFactory.getSetting("default_user_mail");
//    public static final String DEFAULT_USER_BADGE = MessageFactory.getSetting("default_user_badge");
//    public static final String USERS_PHOTOS_DIR = MessageFactory.getSetting("users_photos_dir");
//    public static final String CONSIGNES_FILES_DIR = MessageFactory.getSetting("consignes_files_dir");
//    public static final String USERS_PHOTOS_EXT = MessageFactory.getSetting("users_photos_ext");
    public static final String COMPANY_LOGO_PATH = MessageFactory.getSetting("company_logo_path");
    public static final String CHECKTIME_RPT_LOGO_PATH = MessageFactory.getSetting("checktime_rpt_logo");
	// Shared constants
    public static final String ITEM_SEPARATOR = "item_separator" ;
    public static final String SIZE_PHOTO = "size_photo";
    public static final String DATE_PATTERN1 = "share_DatePattern1" ;
    public static final String DATE_PATTERN2 = "share_DatePattern2" ;
    public static final String DATE_PATTERN3 = "share_DatePattern3" ;
    public static final String DATE_PATTERN4 = "share_DatePattern4" ;
    public static final String MAX_ROWS_PAGE = "max_rows_page" ;
    public static final String LDAP_ACTIVE = "ldapActive" ;
    public static final String REQUIRED = "javax.faces.component.UIInput.REQUIRED";
    public static final String CHECK_EXIST = "validate.exist";
    public static final String SPL_HORAIRE = "Horaire Simple";
    public static final String CYCL_HORAIRE = "Horaire Cyclique";
    public static final String DYM_HORAIRE = "Horaire dynamique";
    public static final String HEBDO_PLANNING = "Hebdomadaire";
    public static final String CYCL_PLANNING = "Cyclique";
    public static final String PLAGE_FIXE = "Obligatoire";
    public static final String HORAIRE_LIBRE = "Libre";
    public static final String SANS_POI_COMPTEUR = "Sans pointage";
    public static final String CHECK_RESTOF_PERIOD = "validate_periodicite";
    public static final String CHECK_PERIODS_LENGTH = "validate_period_length";

    public static final String WORKFLOW_STATUS_ENCOURS = "workflow_status_init";
    public static final String WORKFLOW_STATUS_VALIDATE = "workflow_status_validated";
    public static final String WORKFLOW_STATUS_REJECTED = "workflow_status_rejected";
    public static final String WORKFLOW_STATUS_ENCOURS_COMPENSATION = "workflow_status_encours_compensation";
    public static final String BLACKLIST_FORWORD = "blacklistManip";
    public static final String VISITE_SELFSERVICE_FORWORD = "selfserviceVisiteListe";
    public static final String VISITE_FORWORD = "visiteListe";

    public static final String WORKFLOW_STATUS_ELAPSED = "workflow_status_elapsed";
    public static final String WORKFLOW_STATUS_IMPROD = "workflow_status_improd";
    public static final String WORKFLOW_STATUS_INDRT = "workflow_status_indrt";
    
    public static final String BOOLEAN_YES = "share_boolean_yes";
    public static final String BOOLEAN_NO = "share_boolean_no";
	public static final String HOUR_STR = "H : ";
	public static final String MIN_STR = " mn";
	public static final String LOGGING_ENABLED = "logging_enabled";
	public static final String USER_AUTHENTICATED = "userAuthenticated";
	public static final String USERS_PHOTOS_DIR = "upload/photos/";
	public static final String USERS_PHOTOS_EXT = ".jpeg";
	public static final String DEFAULT_USER_LOGIN = "check_";
	public static final String DEFAULT_USER_PWD = "check_";
	// Import XLS Data 
	public static final String IMPORT_XLS_ROW_START_INDEX = "import_xls_row_start_index";
	public static final String IMPORT_XLS_USER_NOM_COL_INDEX = "import_xls_user_nom_col_index";
	public static final String IMPORT_XLS_USER_PRENOM_COL_INDEX = "import_xls_user_prenom_col_index";
	public static final String IMPORT_XLS_USER_MATRICULE_COL_INDEX = "import_xls_user_matricule_col_index";
	public static final String IMPORT_XLS_USER_BADGE_COL_INDEX = "import_xls_user_badge_col_index";
	public static final String IMPORT_XLS_USER_DATE_EMBAUCHE_COL_INDEX = "import_xls_user_date_embauche_col_index";
	public static final String IMPORT_XLS_USER_CODE_PROFIL_COL_INDEX = "import_xls_user_code_profil_col_index";
	public static final String IMPORT_XLS_USER_CODE_POSITION_COL_INDEX = "import_xls_user_code_position_col_index";
	public static final String IMPORT_XLS_USER_NB_INIT_CONGE_COL_INDEX = "import_xls_user_nb_init_conge_col_index";
	public static final String IMPORT_XLS_USER_MODE_PAIEMENT_COL_INDEX = "import_xls_user_mode_paiement_col_index";
	public static final String IMPORT_XLS_USER_CODE_SITE_COL_INDEX = "import_xls_user_code_site_col_index";
	public static final String IMPORT_XLS_USER_CIN_COL_INDEX = "import_xls_user_cin_col_index";
	public static final String IMPORT_XLS_USER_CNSS_COL_INDEX = "import_xls_user_cnss_col_index";
	public static final String IMPORT_XLS_USER_DATE_NAISS_COL_INDEX = "import_xls_user_date_naiss_col_index";
	public static final String IMPORT_XLS_USER_ADRESS_COL_INDEX = "import_xls_user_adress_col_index";
	public static final String IMPORT_XLS_USER_CODE_SUPH_COL_INDEX = "import_xls_user_code_suph_col_index";
	public static final String IMPORT_XLS_USER_CODE_PROFILAPP_COL_INDEX = "import_xls_user_code_profilapp_col_index";
	public static final String IMPORT_XLS_USER_CODE_ZONE_COL_INDEX = "import_xls_user_code_zone_col_index";
	public static final String IMPORT_XLS_PROFIL_CODE_COL_INDEX = "import_xls_profil_code_col_index";
	public static final String IMPORT_XLS_PROFIL_LIBELLE_COL_INDEX = "import_xls_profil_libelle_col_index";
	public static final String IMPORT_XLS_PNTG_MAT_COL_INDEX = "import_xls_pntg_mat_col_index";
	public static final String IMPORT_XLS_PNTG_DATE_COL_INDEX = "import_xls_pntg_date_col_index";
	public static final String IMPORT_XLS_PNTG_HOUR_COL_INDEX = "import_xls_pntg_hour_col_index";
	
	
    //Applicatif messages    
    public static final String SIT_E000_ME003 = "SIT_E000_ME003";
    public static final String SIT_E001_ME001 = "SIT_E001_ME001";
    public static final String SIT_E001_ME002 = "SIT_E001_ME002";
    public static final String SIT_E002_ME001 = "SIT_E002_ME001";
    public static final String SIT_E002_ME002 = "SIT_E002_ME002";
    public static final String PRF_E001_ME001 = "PRF_E001_ME001";
    public static final String PRF_E000_ME002 = "PRF_E000_ME002";
    public static final String HRH_E000_ME002 = "HRH_E000_ME002";
    public static final String HRH_E001_ME001 = "HRH_E001_ME001";
    public static final String HRH_E001_ME003 = "HRH_E001_ME003";
    public static final String HRH_E004_ME003 = "HRH_E004_ME003";
    public static final String USR_E000_ME002 = "USR_E002_ME002";
    public static final String USR_E001_ME002 = "USR_E001_ME002";
    public static final String JRF_E001_ME001 = "JRF_E001_ME001";
    public static final String ANT_E001_ME002 = "ANT_E001_ME002";
    public static final String HOR_E001_ME002 = "HOR_E001_ME002";
    public static final String HOR_E001_ME003 = "HOR_E001_ME003";
    public static final String HOR_E001_ME004 = "HOR_E001_ME004";
    public static final String HOR_E001_ME005 = "HOR_E001_ME005";
    public static final String HOR_E001_ME006 = "HOR_E001_ME006";
    public static final String EXCP_E001_ME001 = "EXCP_E001_ME001";
    public static final String VIS_E001_ME004 =  "VIS_E001_ME002";
    public static final String EXCPPM_E001_ME001 = "EXCPPM_E001_ME001";
    public static final String EXCPPM_E001_ME003 = "EXCPPM_E001_ME003";
    public static final String EXCPPM_E001_ME004 = "EXCPPM_E001_ME004";
    public static final String EXCPPM_E001_ME005 = "EXCPPM_E001_ME005";
    public static final String EXCPPM_E001_ME006 = "EXCPPM_E001_ME006";
    public static final String EXCPPM_E001_ME007 = "EXCPPM_E001_ME007";
    public static final String EXCPPM_E001_ME008 = "EXCPPM_E001_ME008";
    public static final String PLA_E000_ME002 = "PLA_E000_ME002";
    public static final String HOR_E000_ME002 = "HOR_E000_ME002";
    public static final String ANT_E000_ME002 = "ANT_E000_ME002";
    public static final String TAB_E000_ME002 = "TAB_E000_ME002";
    public static final String TYP_E000_ME002 = "TYP_E000_ME002";
    public static final String TINTR_E000_ME002 = "TINTR_E000_ME002";
    public static final String ZON_E000_ME002 = "ZON_E000_ME002";
    
    public static final String USER_NOT_FOUND = "Login.ME001";
    public static final String USER_CONNECT = "Login.ME002";
    public static final String USR_E004_ME002 = "USR_E004_ME002";
    public static final String COM_E001_ME003 = "COM_E001_ME003";
    public static final String CNG_E001_ME005 = "CNG_E001_ME005";
    public static final String CNG_E001_ME006 = "CNG_E001_ME006";
    public static final String CNG_E001_ME007 = "CNG_E001_ME007";
    public static final String CNG_E001_ME008 = "CNG_E001_ME008";
    public static final String CNG_E001_ME009 = "CNG_E001_ME009";
    public static final String CNG_E001_ME010 = "CNG_E001_ME010";
    public static final String USR_E004_ME003 = "USR_E004_ME003";
    public static final String PRF_E003_ME003 = "PRF_E003_ME003";
    public static final String PLI_E001_ME002 = "PLI_E001_ME002";
    public static final String VIS_E001_ME002 = "VIS_E001_ME002";
    public static final String USR_E001_ME003 = "USR_E001_ME003";
    public static final String BOR_E001_ME007 = "BOR_E001_ME007";
    public static final String COM_E001_ME019 = "COM_E001_ME019";
    public static final String COM_E001_ME020 = "COM_E001_ME020";
    public static final String SHARE_LIC_VAL1 = "share.license.valide1";
    public static final String SHARE_LIC_VAL2 = "share.license.valide2";
    //Forwards string
    public static final String FAILURE_FORWARD = "failure" ;
    public static final String PROFIL_LISTE_FORWARD = "profileListe" ;
    public static final String SITE_LISTE_FORWARD = "siteListe" ;
    public static final String ZONE_LISTE_FORWARD = "zoneListe" ;
    public static final String HIERARCHIE_LISTE_FORWARD = "hierarchieListe";
    public static final String USER_LISTE_FORWARD = "userListe";
    public static final String POINTEUSE_LISTE_FORWARD = "terminalListe" ;
    public static final String POINTEUSES_CONFIG_PATH = "terminals_config_path" ;
    public static final String JRFERIE_LISTE_FORWARD = "jrFerieListe" ;
    public static final String HORAIRE_LISTE_FORWARD = "horaireListe" ;
    public static final String ANNOTATION_LISTE_FORWARD = "annotationListe" ;
    public static final String PLANNING_LISTE_FORWARD = "planningListe" ;
    public static final String EXCEPTION_PROFIL_METIER_LISTE_FORWARD = "exceptionProfilMetierListe" ;
    public static final String EXCEPTION_POSITION_LISTE_FORWARD = "exceptionPositionListe" ;
    public static final String EXCEPTION_LISTE_FORWARD = "exceptionListe" ;
    public static final String ADVPARAM_LISTE_FORWARD = "advParamsListe" ;
    
    public static final String INTER_LISTE_FORWARD = "interListe" ; 
    
    public static final String CONGE_LISTE_FORWARD = "congeListe" ; 
    public static final String CONGE_SELFSERVICE_FORWOED="selfserviceCongeListe";
    public static final String CONGESH_LISTE_FORWARD = "congeSHListe" ;
    public static final String CONGEHR_LISTE_FORWARD = "congeHRListe" ;
    public static final String CONGESH_SELFSERVICE_FORWARD = "selfserviceCongeSH" ;
    public static final String CONGEHR_SELFSERVICE_FORWARD = "selfserviceCongeHR" ;
    public static final String CONGESH_FORWARD = "congeSH" ;
    public static final String CONGEHR_FORWARD = "congeHR" ;
    public static final String CONGESH_SELFSERVICE_LISTE_FORWARD="selfserviceCongeSHListe";
    public static final String CONGEHR_SELFSERVICE_LISTE_FORWARD="selfserviceCongeHRListe";
   
    public static final String ABSENCE_SELFSERVICE_FORWOED = "selfserviceAbsenceListe";
    public static final String ABSENCE_LISTE_FORWARD = "absenceListe";
    public static final String ABSENCESH_LISTE_FORWARD = "absenceSHListe" ;
    public static final String ABSENCEHR_LISTE_FORWARD = "absenceHRListe" ;
    public static final String ABSENCESH_SELFSERVICE_FORWARD = "selfserviceAbsenceSH" ;
    public static final String ABSENCEHR_SELFSERVICE_FORWARD = "selfserviceAbsenceHR" ;
    public static final String ABSENCESH_FORWARD = "absenceSH" ;
    public static final String ABSENCEHR_FORWARD = "absenceHR" ;
    public static final String ABSENCESH_SELFSERVICE_LISTE_FORWARD="selfserviceAbsenceSHListe";
    public static final String ABSENCEHR_SELFSERVICE_LISTE_FORWARD="selfserviceAbsenceHRListe";
    
    public static final String RECUPERATION_SELFSERVICE_FORWOED = "selfserviceRecuperationListe";
    public static final String RECUPERATION_LISTE_FORWARD = "recuperationListe";
    public static final String RECUPERATIONSH_LISTE_FORWARD = "recuperationSHListe" ;
    public static final String RECUPERATIONHR_LISTE_FORWARD = "recuperationHRListe" ;
    public static final String RECUPERATIONSH_SELFSERVICE_FORWARD = "selfserviceRecuperationSH" ;
    public static final String RECUPERATIONHR_SELFSERVICE_FORWARD = "selfserviceRecuperationHR" ;
    public static final String RECUPERATIONSH_FORWARD = "recuperationSH" ;
    public static final String RECUPERATIONHR_FORWARD = "recuperationHR" ;
    public static final String RECUPERATIONSH_SELFSERVICE_LISTE_FORWARD="selfserviceRecuperationSHListe";
    public static final String RECUPERATIONHR_SELFSERVICE_LISTE_FORWARD="selfserviceRecuperationHRListe";
   
    public static final String COMPENSATION_SELFSERVICE_FORWOED = "selfserviceCompensationListe";
    public static final String COMPENSATION_LISTE_FORWARD = "compensationListe";
    public static final String COMPENSATIONSH_LISTE_FORWARD = "compensationSHListe" ;
    public static final String COMPENSATIONHR_LISTE_FORWARD = "compensationHRListe" ;
    public static final String COMPENSATIONSH_SELFSERVICE_FORWARD = "selfserviceCompensationSH" ;
    public static final String COMPENSATIONHR_SELFSERVICE_FORWARD = "selfserviceCompensationHR" ;
    public static final String COMPENSATIONSH_FORWARD = "compensationSH" ;
    public static final String COMPENSATIONHR_FORWARD = "compensationHR" ;
    public static final String COMPENSATIONSH_SELFSERVICE_LISTE_FORWARD="selfserviceCompensationSHListe";
    public static final String COMPENSATIONHR_SELFSERVICE_LISTE_FORWARD="selfserviceCompensationHRListe";
   
    public static final String HEURESUPP_SELFSERVICE_FORWOED = "selfserviceHeurSuppListe";
    public static final String HEURESUPP_LISTE_FORWARD = "heurSuppListe";
    public static final String HEURESUPPSH_LISTE_FORWARD = "heurSuppSHListe" ;
    public static final String HEURESUPPHR_LISTE_FORWARD = "heurSuppHRListe" ;
    public static final String HEURESUPPSH_SELFSERVICE_FORWARD = "selfserviceHeurSuppSH" ;
    public static final String HEURESUPPHR_SELFSERVICE_FORWARD = "selfserviceHeurSuppHR" ;
    public static final String HEURESUPPSH_FORWARD = "heurSuppSH" ;
    public static final String HEURESUPPHR_FORWARD = "heurSuppHR" ;
    public static final String HEURESUPPSH_SELFSERVICE_LISTE_FORWARD="selfserviceHeurSuppSHListe";
    public static final String HEURESUPPHR_SELFSERVICE_LISTE_FORWARD="selfserviceHeurSuppHRListe";
   
    public static final String HISTORY_LISTE_FORWARD = "historyListe";
    public static final String CONTEXT_NAME = MessageFactory.getSetting("context_name");
//    public static final String HIERARCHIE_REQ_PATH = CONTEXT_NAME + "/web/organisation/hrh/HRH-E002.jsf?hierarchieId=";
//    public static final String DYM_HORAIRE_REQ_PATH = CONTEXT_NAME + "/web/parametres/hor/HOR-E004.jsf";
//    public static final String SPL_HORAIRE_REQ_PATH = CONTEXT_NAME + "/web/parametres/hor/HOR-E002.jsf";
//    public static final String PLANNING_REQ_PATH = CONTEXT_NAME + "/web/parametres/pla/PLA-E002.jsf";
    public static final String WORKFLOW_ACTIVE = "workflow_active";
    public static final String MAIL_ACTIVE = "mail_active";
//    public static final String HEUR_SUPP_ANNUEL = MessageFactory.getSetting("heur_supp_annuel");
    public static final String ALL_PAIEMENT_MODES = "checktime_all_paiement_modes";
    public static final String ALL_INJECTION_MODES = "checktime_all_injection_modes";
    public static final String ALL_PNT_CATEGORIES = "checktime_all_pnt_categories";
     
    public static final String RPT_HNUIT = "report_horNuit";
    
}
