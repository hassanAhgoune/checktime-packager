package ma.nawarit.checker.utils;

import org.apache.log4j.Logger;

public class LoggerMailer {

	private static final Logger log = Logger.getLogger(LoggingAspect.class);
	
	public static void LogEmail(String message) {
		log.error(message);
	}
	public static void LogInfo(String message) {
		log.info(message);
	}
	
}
