package ma.nawarit.checker.utils.syncRH;


import javax.faces.bean.ManagedProperty;

import ma.nawarit.checker.synchronizer.Consumer;
import ma.nawarit.checker.synchronizer.consumer.AbsenceConsumer;
import ma.nawarit.checker.synchronizer.consumer.SiteConsumer;
import ma.nawarit.checker.synchronizer.consumer.UniteOrgConsumer;
import ma.nawarit.checker.synchronizer.consumer.UserConsumer;
import org.apache.log4j.Logger;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.synchronizer.SynchronizerProcessor;
import ma.nawarit.checker.synchronizer.utils.CongeOperator;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

public class syncRHscheduler {
	@ManagedProperty(value = "#{SynchronizerProcessor}")
	private SynchronizerProcessor synchronizerProcessor;

	@ManagedProperty(value = "#{UserManageableService}")
	private transient UserManageableService userService;

	@ManagedProperty(value = "#{AbsValideConsumer}")
	private transient Consumer absValideConsumer;

	private CongeOperator conge;

	public CongeOperator getConge() {
		return conge;
	}

	public void setConge(CongeOperator conge) {
		this.conge = conge;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	private final Logger log = Logger.getLogger(syncRHscheduler.class);

	public synchronized void execute() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("SYNCH.RH.STATUS").equals("ACTIVE")) {
				synchronizerProcessor.provide();
				synchronizerProcessor.consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	
	public synchronized void RHimport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("SYNCH.RH.STATUS").equals("ACTIVE")) {
				System.out.println("RHimport");
				//synchronizerProcessor.provide();
				synchronizerProcessor.consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	public synchronized void AbsValideImport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("absence_valide.STATUS").equals("ACTIVE")) {
				System.out.println("AbsValideImport");
				absValideConsumer.consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	public synchronized void SiteImport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("site.STATUS").equals("ACTIVE")) {
				System.out.println("AbsValideImport");
				new SiteConsumer().consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

	public synchronized void UnitOrgImport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("site.STATUS").equals("ACTIVE")) {
				System.out.println("AbsValideImport");
				new UniteOrgConsumer().consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

	public synchronized void UserImport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("site.STATUS").equals("ACTIVE")) {
				System.out.println("AbsValideImport");
				new UserConsumer().consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

	public synchronized void RHexport() {
		try {
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("SYNCH.RH.STATUS").equals("ACTIVE")) {
				System.out.println("RHexport");
				synchronizerProcessor.provide();
				//synchronizerProcessor.consume();
			}
		} catch (Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}
	
	
	public void deleteDuplicatedValues() {
		try {
			databaseOperations.deleteDuplicated("retard");
			databaseOperations.deleteDuplicated("absence");
			databaseOperations.deleteDuplicated("sooner");
		}
		catch(Exception ex) {
			ex.printStackTrace();
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(ex));
		}
	}

	public SynchronizerProcessor getSynchronizerProcessor() {
		return synchronizerProcessor;
	}

	public void setSynchronizerProcessor(SynchronizerProcessor synchronizerProcessor) {
		this.synchronizerProcessor = synchronizerProcessor;
	}

	public void setAbsValideConsumer(Consumer absValideConsumer) {
		this.absValideConsumer = absValideConsumer;
	}
}
