package ma.nawarit.checker.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class DateUtility {
	
/* Add Day/Month/Year to a Date
add() is used to add  values to a Calendar object. 
You specify which Calendar field is to be affected by the operation 
(Calendar.YEAR, Calendar.MONTH, Calendar.DATE). 
*/
	public static void addToDate(){
	//	String DATE_FORMAT = "yyyy-MM-dd";
		String DATE_FORMAT = "dd-MM-yyyy";		//Refer Java DOCS for formats
		java.text.SimpleDateFormat sdf =  new java.text.SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance();
		Date d1 = new Date();
		c1.set(1999,0 ,20); 		//(year,month,date)
		c1.add(Calendar.DATE,40);
	}
	
	
/*Substract Day/Month/Year to a Date
	roll() is used to substract values to a Calendar object. 
	You specify which Calendar field is to be affected by the operation 
	(Calendar.YEAR, Calendar.MONTH, Calendar.DATE). 
	
	Note: To substract, simply use a negative argument. 
    roll() does the same thing except you specify if you want to roll up (add 1) 
    or roll down (substract 1) to the specified Calendar field. The operation only
    affects the specified field while add() adjusts other Calendar fields. 
    See the following example, roll() makes january rolls to december in the same 
    year while add() substract the YEAR field for the correct result

*/
	
	public static void subToDate(){
		String DATE_FORMAT = "dd-MM-yyyy";
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT);
		Calendar c1 = Calendar.getInstance(); 
		c1.set(1999, 0 , 20); 
		
		// roll down, substract 1 month
		c1.roll(Calendar.MONTH, false); 

		c1.set(1999, 0 , 20); 
		c1.add(Calendar.MONTH, -1); 
		// substract 1 month
	}
	
	public static void daysBetween2Dates(){
		Calendar c1 = Calendar.getInstance(); 	//new GregorianCalendar();
		Calendar c2 = Calendar.getInstance(); 	//new GregorianCalendar();
	    c1.set(1999, 0 , 20); 
	    c2.set(1999, 0 , 22); 
	}
	
	public static void daysInMonth() {
		Calendar c1 = Calendar.getInstance(); 	//new GregorianCalendar();
	    c1.set(1999, 6 , 20); 
	    int year = c1.get(Calendar.YEAR);
	    int month = c1.get(Calendar.MONTH);
//	    int days = c1.get(Calendar.DATE);
		int [] daysInMonths = {31,28,31,30,31,30,31,31,30,31,30,31};
		daysInMonths[1] += DateUtility.isLeapYear(year) ? 1 : 0;
	}
	
	public static void getDayofTheDate() {
		Date d1 = new Date();
		String day = null;
	    DateFormat f = new SimpleDateFormat("EEEE");
	    try {
	     day = f.format(d1);
	    }
	    catch(Exception e) {
	      e.printStackTrace();
	    }
	}
	
	public static void validateAGivenDate() {
		String dt = "20011223";   
		String invalidDt = "20031315";
		String dateformat = "yyyyMMdd";   
		Date dt1=null , dt2=null;
		try {     
			SimpleDateFormat sdf = new SimpleDateFormat(dateformat);    
			sdf.setLenient(false);    
			dt1 = sdf.parse(dt);  
			dt2 = sdf.parse(invalidDt);   
		}  
		catch (ParseException e) {     
		}  
		catch (IllegalArgumentException e) {    
		}
	}
	
	public static void compare2Dates(){
		SimpleDateFormat fm = new SimpleDateFormat("dd-MM-yyyy");
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		
		c1.set(2000, 02, 15);
		c2.set(2001, 02, 15);
	}

	public static boolean isLeapYear(int year){
		 if((year%100 != 0) || (year%400 == 0)){
			 return true;
		 }
		 return false;
	}

	public static void main(String args[]){
		addToDate();
		subToDate();
		daysBetween2Dates();	//The "right" way would be to compute the julian day number of both dates and then do the substraction.
		daysInMonth();
		validateAGivenDate();
		compare2Dates();
		getDayofTheDate();
	}
	
}

