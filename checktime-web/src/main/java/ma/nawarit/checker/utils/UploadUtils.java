package ma.nawarit.checker.utils;


import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;


/**
 * Classe regroupant des m�thodes utilitaires pour l'upload de fichier.
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime</DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Cr�e le       :</B><DD> 18 Mars 08</DD>
 * </DL>
 * @since 2008
 * @version 0.1
 * @author k.Lamhaddab
 */
public class UploadUtils {
    private static final Logger log = Logger.getLogger(UploadUtils.class);

    /**
     * Alimente un fichier � partir de donn�es contenues dans un flux d'entr�e.
     * @param bufferedInputStream le flux d'entr�e i.e. les donn�es � ecrire.
     * @param nameFile le nom du fichier de sortie.
     * @throws Exception si une exception se produit pendant l'�criture.
     */
    public static void uploadFile(BufferedInputStream bufferedInputStream,
        String nameFile) throws Exception {
        byte[] buffer = new byte[1024];
        int read;

        try {
            // cr�ation du flux de sortie.
            FileOutputStream fos = new FileOutputStream(nameFile);
           
            // boucle de 'lecture flux d'entr�e'/'�criture flux de sortie'. 
            while ((read = bufferedInputStream.read(buffer, 0, buffer.length)) != -1) {
                fos.write(buffer, 0, read);
            }

			bufferedInputStream.close();
            // d�charge le flux vers la sortie.
            fos.flush();

            // ferme le flux de sortie. 
            fos.close();
           
        } catch (FileNotFoundException e) {
            throw e;
        } catch (IOException e) {
            throw e;
        }
    }
    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
    
        // Get the size of the file
        long length = file.length();
    
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
    
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];
    
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
    
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
    
        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    /**
     * Alimente un fichier � partir de donn�es contenues dans un flux d'entr�e zip�
     * puis ile d�zip et supprime le fichier zip du serveur.
     * @param bufferedInputStream le flux d'entr�e i.e. les donn�es � ecrire.
     * @param nameFile le nom du fichier de sortie.
     * @throws Exception si une exception se produit pendant l'�criture.
     */
//    public static void uploadZipFile(BufferedInputStream bufferedInputStream,
//        String nameFile, String dir) throws Exception {
//        try {
//            uploadFile(bufferedInputStream, nameFile);
//			//Utils.unzip(new org.apache.tools.zipp.ZipFile(nameFile,"Cp437"),new File(dir));
//			FileUtils.forceDelete(new File(nameFile));
//        } catch (FileNotFoundException e) {
//            throw e;
//        } catch (IOException e) {
//            throw e;
//        }
//    }
    
    
    
}
