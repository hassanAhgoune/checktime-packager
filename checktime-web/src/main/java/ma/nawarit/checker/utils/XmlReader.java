package ma.nawarit.checker.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.apache.log4j.Logger;

import ma.nawarit.checker.compagnie.UserImpl;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


/**
 * 
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime<DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Cr�e le       :</B><DD> Fri Apr 04 10:29:08 GMT 2008 </DD>
 * </DL>
 * @since 01/07/2012
 * @version 1.0
 * @author K.Lamhaddab 
 */
public class XmlReader {
	private static final Logger log = Logger.getLogger(XmlReader.class);
	private String xmlFile;
	private XStream xstream;

	
	/**
	 * Contstructeur de la classe BluAgeGenUtils.
	 * @param filePath : chemin du fichier xml.
	 * @throws IOException : si une exception d'entr�e/sortie s'est produite.
	 */
	public XmlReader(String xmlFile) throws IOException {
		this.xmlFile = xmlFile;
		initXtream();	
	}
	/**
	 * Cette m�thode initilaise les alias Xstream
	 *
	 */
	private void initXtream(){
		this.xstream = new XStream(new DomDriver());
		xstream.alias("user", UserImpl.class);
		xstream.alias("users", ArrayList.class);
		
	}
	
	/**
	 * Cette m�thode charge l'objet TerminalInfoBean depuis le fichier terminals-config.xml.
	 * @return TerminalInfoBean
	 * @throws IOException : si une exception d'entr�e/sortie s'est produite.
	 */
	public Collection<UserImpl> load(){
		try {
			return  (Collection<UserImpl>) xstream.fromXML(readXmlBuffer(xmlFile));
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	public UserImpl loadByMat(String matricule){
		try {
			Collection<UserImpl> users = this.load();
			if(users != null){
				Iterator<UserImpl> it = users.iterator();
				UserImpl currentUser;
				while(it.hasNext()){
					currentUser = it.next();
					if(matricule.equalsIgnoreCase(currentUser.getMatricule()))
						return currentUser;
				}
			}
			return null;
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		}
		
		
	}
	private String readXmlBuffer(String xmlFile) throws IOException{
		String xml = "";
		String tmp = "";
		Reader reader = new InputStreamReader(new FileInputStream(xmlFile), "utf-8");
		BufferedReader in = new BufferedReader(reader);
		//BufferedReader in =	new BufferedReader(new FileReader(xmlFile));
		while ((tmp = in.readLine()) != null) {
			xml += tmp;
		}
		in.close();
		return xml;
	}
	
	public static void main(String[] args) {
		try {
			XmlReader xmlReader = new XmlReader("d:\\arabic-names.xml");
			Collection<UserImpl> users = xmlReader.load();
			if(users != null){
				Iterator<UserImpl> it = users.iterator();
				UserImpl currentUser;
				while(it.hasNext()){
					currentUser = it.next();
				}
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}