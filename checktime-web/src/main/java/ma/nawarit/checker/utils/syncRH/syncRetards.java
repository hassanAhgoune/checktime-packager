package ma.nawarit.checker.utils.syncRH;

import java.io.File;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.bean.ManagedProperty;

import org.apache.log4j.Logger;
import org.hibernate.hql.ast.tree.RestrictableStatement;

import com.sun.faces.facelets.component.RepeatRenderer;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.fileutils.fileConsumer;
import ma.nawarit.checker.fileutils.fileConsumerImpl;
import ma.nawarit.checker.fileutils.fileProvider;
import ma.nawarit.checker.fileutils.fileProviderImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.RetardImpl;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.utils.Globals;

public class syncRetards {
 
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;
	
	@ManagedProperty(value = "#{RetardManageableService}")
	private RetardManageableService retardServise;

	

	final static Logger logger = Logger.getLogger(syncConges.class);
	public final String patern = "^([0-9]{1,4});(\\d{4}-(([0]{0,1}[1-9]{1})|([1-2][0-9]{1})|([3][0-1]{1}))-(([0]{0,1}[1-9]{1})|([1][0-2]{1})));(([0]*)[1-9]|([0]*)[1-9][0-9]|([0]*)[1-9][0-9][0-9]);([\\w]+)$";
	public final String dateFormat = "yyyy-MM-dd";
	public final DateFormat Dateformat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

	public syncRetards() {

	}

	public void retardsConsumer(){
		try {
			propertiesOperator propOP=new propertiesOperator();
			fileConsumer file = new fileConsumerImpl();
			Date today = new Date();
			String reportDate = Dateformat.format(today);
			String fileName = "retards." + reportDate + ".csv";
			File f = new File(propOP.getRetardConsumerPath() + fileName);

			// Check if today file exist or not

			if (!f.exists()) {
				logger.warn("fichier de retards de [" + reportDate + "]  n'exist pas!");
				return;
			}

			
			
			// Read all Conge from csv file
			List<String> lines = file.generateListFromCSVfile(propOP.getRetardConsumerPath() + fileName);
			if (lines.size() == 0) {
				logger.info("liste des nouveaux retards est vide");
				return;
			}

			
			
			// validation du format de fichier conges
			if (!validate(lines)) {
				logger.error("le fichier des retard est de format incorrect!!");
				return;
			}
			
			
			
			// read all lines in list of conges and insert it in database
			for (String item : lines) {
				String[] cells = item.split(";");
				String Matricule = cells[0].trim();
				Date date_retard = Dateformat.parse(cells[1].trim());
				int duree = Integer.valueOf(cells[2].trim());
				String statut = cells[3].trim();

				
				
				// get user ID by Matricule
				java.util.List userList = userService.getUsersByMatricule(Matricule);
				if (userList == null) {
					logger.error("le Collaborateur de Matricule [" + Matricule + "] n'exist pas dans la base de donnes!");
					return;
				}
				User user = (User) userList.get(0);
				
				
				// l'ajout du conge au base de données
				int result=isExist(Matricule, date_retard, duree);
				if(result!=-1) {
					Retard retard=retardServise.load(result);
					if(statut.equals("true")) {
						retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					}else {
						retard.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
					}
					retardServise.update(retard);
					logger.info("Retard updated ID : "+retard.getId());
				}
				else
				{
					Retard retard = new RetardImpl();
					retard.setUser(user);
					retard.setMatricule(Matricule);
					retard.setIndexPlage(1);
					retard.setDate(date_retard);
					retard.setHorNuit(false);
					if(statut.equals("true")) {
						retard.setRetardTolere(duree);
						retard.setRetardNonTolere(0);
						retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					}else {
						retard.setRetardTolere(0);
						retard.setRetardNonTolere(duree);
						retard.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
					}
					retardServise.create(retard);
					logger.info("Retard created ID : "+retard.getId());
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void RetardsProvider(Date date_debut, Date date_fin) throws Exception {
		propertiesOperator propOP=new propertiesOperator();
		Date today = new Date();
		String reportDate = Dateformat.format(today);
		String fileName = "retards." + reportDate + ".csv";
		File f = new File(propOP.getRetardProviderPath() + fileName);
		if (!f.exists()) {
			PrintWriter writer = new PrintWriter(propOP.getRetardProviderPath() + fileName, "UTF-8");
			writer.println("Matricule;Date;Duree;Statut");
			writer.close();
		}
		List<Retard> retardList = retardServise.readByCriteria(date_debut, date_fin);
		ArrayList<String> listToExport = new ArrayList<String>();
		for (Retard retard : retardList) {
			String statut=retard.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE)?"true":"false";
			
			String line = retard.getUser().getMatricule()+";"+Dateformat.format(retard.getDate()) + ";" 
			+ (retard.getRetardNonTolere()+retard.getRetardTolere()) + ";" + statut;
			listToExport.add(line);
		}
		fileProvider fileprovider = new fileProviderImpl();
		fileprovider.generateCSVfile(propOP.getRetardProviderPath() + fileName, listToExport);
	}

	public Boolean validate(List<String> lines) {
		Boolean valide = true;
		for (String item : lines) {
			String[] cells = item.split(";");
			if (cells.length != 4) {
				valide = false;
				break;
			}
			if (!item.matches(patern)) {
				valide = false;
				logger.error("la format des donnes dans le fichier de conge est in correct!");
				break;
			}
		}
		return valide;
	}
	
	//check retard if exist
	public int isExist(String Matricule, Date dateRetard, int Duree) {
		int result=-1;
		List retards=retardServise.readByAdvancedCriteria(Matricule, dateRetard, Duree);
		if(retards!=null) {
			Retard r=(Retard) retards.get(0);
			result=r.getId();
		}
		return result;
	}

	/*
	 * Getter and setter
	 */
	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}
	
	public RetardManageableService getRetardServise() {
		return retardServise;
	}

	public void setRetardServise(RetardManageableService retardServise) {
		this.retardServise = retardServise;
	}
}
