package ma.nawarit.checker.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.TypeAbsenceImpl;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import ma.nawarit.checker.compagnie.*;
import ma.nawarit.checker.injection.MouvementImpl;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XlsReader {
	public static List<UserImpl> updateUserData(String fileName) {
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;
			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();


			UserImpl user = null;
			List<UserImpl> users = new ArrayList<UserImpl>();
			String startRowStr = MessageFactory.getSetting(Globals.IMPORT_XLS_ROW_START_INDEX); 
			int startRowIndex = ( startRowStr!= null) ? Integer.parseInt(startRowStr) : 0;

			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					user = new UserImpl();
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_MATRICULE_COL_INDEX)));
					if (cell != null) {
						user.setMatricule(new Double(cell.getNumericCellValue()).intValue()+"");
					}

					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_SUPH_COL_INDEX)));
					if (cell != null) {
						user.setSuphCode(new Double(cell.getNumericCellValue()).intValue()+"");
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_BADGE_COL_INDEX)));
					if (cell != null) {
						user.setBadge(new Double(cell.getNumericCellValue()).intValue()+"");
					}
					
					users.add(user);
				}
				
			}
			return users;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}
	public static List<UserImpl> extractUserData(String fileName) {
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;

			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();

			UserImpl user = null;
			List<UserImpl> users = new ArrayList<UserImpl>();
			String startRowStr = MessageFactory.getSetting(Globals.IMPORT_XLS_ROW_START_INDEX); 
			int startRowIndex = 0;
			Double d;
			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					user = new UserImpl();
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_MATRICULE_COL_INDEX)));
					if (cell != null) {
						user.setMatricule(new Double(cell.getNumericCellValue()).intValue() + "");
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_NOM_COL_INDEX)));
					if (cell != null) {
//						cellValue = cell.getStringCellValue();
//						String [] results = cellValue.split(" ");
//						int i = 0;
//						String name = "";
//						for( i=0; i<results.length-2; i++)
//							name += results[i]+" ";
//						user.setNom(name+results[i]);
//						user.setPrenom(results[i+1]);
						user.setNom(cell.getStringCellValue());
						
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_PRENOM_COL_INDEX)));
					if (cell != null) {
						user.setPrenom(cell.getStringCellValue());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_DATE_EMBAUCHE_COL_INDEX)));
					if (cell != null) {
						user.setDateEmb(cell.getDateCellValue());
					}else{
						user.setDateEmb(new Date());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_BADGE_COL_INDEX)));
					if (cell != null) {
						user.setBadge((cell.getStringCellValue()!=null)?cell.getStringCellValue().toUpperCase().trim():"");
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_PROFIL_COL_INDEX)));
					if (cell != null) {
						user.setProfilMetierCode(cell.getStringCellValue().toUpperCase().trim());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_POSITION_COL_INDEX)));
					if (cell != null) {
						user.setPositionCode(new Double(cell.getNumericCellValue()).intValue() + "");
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_NB_INIT_CONGE_COL_INDEX)));
					if (cell != null) {
						d = cell.getNumericCellValue(); 
						user.setNbCongeInitial(d.intValue());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_MODE_PAIEMENT_COL_INDEX)));
					if (cell != null) {
						user.setModePaiement(cell.getStringCellValue().toUpperCase().trim());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_SITE_COL_INDEX)));
					if (cell != null) {
						user.setSiteCode(cell.getStringCellValue().toUpperCase().trim());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CNSS_COL_INDEX)));
					if (cell != null) {
						d = cell.getNumericCellValue(); 
						user.setCnssNum(""+d.intValue());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CIN_COL_INDEX)));
					if (cell != null) {
						user.setCinNum(cell.getStringCellValue().toUpperCase().trim());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_ADRESS_COL_INDEX)));
					if (cell != null) {
						user.setAdressPostale((cell.getStringCellValue().toUpperCase().trim()));
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_DATE_NAISS_COL_INDEX)));
					if (cell != null) {
						user.setDateNaiss(cell.getDateCellValue());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_PROFILAPP_COL_INDEX)));
					if (cell != null) {
						d = cell.getNumericCellValue(); 
						user.setProfilAppCode(""+d.intValue());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_USER_CODE_ZONE_COL_INDEX)));
					if (cell != null) {
						user.setZoneCode(cell.getStringCellValue().toUpperCase().trim());
					}
					users.add(user);
				}
				
			}
			return users;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}


	public static List<UserImpl> extractUserDataXLSX(String fileName) {
		try {
			return extractUserDataXLSX(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	public static List<UserImpl> extractUserDataXLSX(InputStream inputStream) {

		try {
			XSSFWorkbook wb = (XSSFWorkbook) WorkbookFactory.create(inputStream);
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;

			// No of rows
			int rows = sheet.getPhysicalNumberOfRows();

			UserImpl user = null;
			List<UserImpl> users = new ArrayList<UserImpl>();
			int startRowIndex = 1;
			Double d;
			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					user = new UserImpl();
					cell = row.getCell(0);
					if (cell != null) {
						user.setMatricule(getCellValue(cell));
					}
					cell = row.getCell(1);
					if (cell != null) {
						user.setNom(getCellValue(cell));
					}
					cell = row.getCell(2);
					if (cell != null) {
						user.setPrenom(getCellValue(cell));
					}
					cell = row.getCell(3);
					if (cell != null) {
						user.setZoneCode(getCellValue(cell));
					}
					cell = row.getCell(4);
					if (cell != null) {
						user.setPositionCode(getCellValue(cell));
					}
					cell = row.getCell(5);
					if (cell != null) {
						user.setSiteCode(getCellValue(cell));
					}
					users.add(user);
				}

			}
			return users;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}
	public static List<Noeud> extractNoeudDataXLSX(InputStream inputStream) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) WorkbookFactory.create(inputStream);
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			int rows = sheet.getPhysicalNumberOfRows();

			Noeud noeud;
			List<Noeud> noeuds = new ArrayList<Noeud>();
			int startRowIndex = 1;
			for (int r = startRowIndex; r < rows; r++) {
				try {
					row = sheet.getRow(r);
					if (row != null) {
						noeud = new NoeudImpl();
						cell = row.getCell(0);
						if (cell != null) {
							noeud.setCode(getCellValue(cell));
						}

						cell = row.getCell(1);
						if (cell != null) {
							noeud.setLibelle(getCellValue(cell));
						}
						cell = row.getCell(2);
						if (cell != null) {
							noeud.setParentCode(getCellValue(cell));
						}
						noeuds.add(noeud);
					}
				}catch (Exception e) {
					throw e;
				}
			}
			return noeuds;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}
	public static List<ProfilMetier> extractProfilDataXLSX(InputStream inputStream) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) WorkbookFactory.create(inputStream);
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			int rows = sheet.getPhysicalNumberOfRows();

			ProfilMetier profilMetier;
			List<ProfilMetier> profilMetiers = new ArrayList<ProfilMetier>();
			int startRowIndex = 1;

			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					profilMetier = new ProfilMetierImpl();
					cell = row.getCell(0);
					if (cell != null) {
						profilMetier.setCode(cell.getRawValue());
					}
					cell = row.getCell(1);
					if (cell != null) {
						profilMetier.setLibelle(cell.getStringCellValue());
					}
					profilMetiers.add(profilMetier);
				}
			}
			return profilMetiers;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}

	public static List<Unit> extractUnitDataXLSX(InputStream inputStream) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) WorkbookFactory.create(inputStream);
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			int rows = sheet.getPhysicalNumberOfRows();

			Unit unit;
			List<Unit> units = new ArrayList<Unit>();
			int startRowIndex = 1;

			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					unit = new UnitImpl();
					cell = row.getCell(0);
					if (cell != null) {
						unit.setCode(cell.getRawValue());
					}
					cell = row.getCell(1);
					if (cell != null) {
						unit.setLibelle(cell.getStringCellValue());
					}
					unit.setDescription("");
					units.add(unit);
				}
			}
			return units;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}

	public static List<TypeAbsence> extractAbsTypeDataXLSX(InputStream inputStream) {
		try {
			XSSFWorkbook wb = (XSSFWorkbook) WorkbookFactory.create(inputStream);
			XSSFSheet sheet = wb.getSheetAt(0);
			XSSFRow row;
			XSSFCell cell;
			int rows = sheet.getPhysicalNumberOfRows();

			TypeAbsence typeAbsence;
			List<TypeAbsence> typeAbsences = new ArrayList<TypeAbsence>();
			int startRowIndex = 1;

			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					typeAbsence = new TypeAbsenceImpl();
					cell = row.getCell(0);
					if (cell != null) {
						typeAbsence.setCode(cell.getRawValue());
					}
					cell = row.getCell(1);
					if (cell != null) {
						typeAbsence.setLibelle(cell.getStringCellValue());
					}
					typeAbsences.add(typeAbsence);
				}
			}
			return typeAbsences;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}


	private static String getCellValue(XSSFCell cell){
		cell.setCellType(XSSFCell.CELL_TYPE_STRING);
		String cellVal = cell.toString();
		return cellVal;
	}
	public static List<ProfilMetier> extractProfilData(String fileName) {
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell;

			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();

			ProfilMetier profil = null;
			List<ProfilMetier> profils = new ArrayList<ProfilMetier>();
			String startRowStr = MessageFactory.getSetting(Globals.IMPORT_XLS_ROW_START_INDEX); 
			int startRowIndex = ( startRowStr!= null) ? Integer.parseInt(startRowStr) : 0;
			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					profil = new ProfilMetierImpl();
					
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PROFIL_CODE_COL_INDEX)));
					if (cell != null) {
						System.err.println("cell :: " + cell.getStringCellValue().toUpperCase());
						profil.setCode(cell.getStringCellValue().toUpperCase());
					}
					cell = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PROFIL_LIBELLE_COL_INDEX)));
					if (cell != null) {
						profil.setLibelle(cell.getStringCellValue());
					}
					profils.add(profil);
				}
				
			}
			return profils;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}
	public static List<MouvementImpl> extractPntgData(String fileName) {

		List<MouvementImpl> mvmnts = new  ArrayList<MouvementImpl>();
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell1;
			HSSFCell cell2;
			HSSFCell cell3;

			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();

			String startRowStr = MessageFactory.getSetting(Globals.IMPORT_XLS_ROW_START_INDEX); 
			int startRowIndex = ( startRowStr!= null) ? Integer.parseInt(startRowStr) : 0;
			HashMap<String, Date> hash = new HashMap<String, Date>();
			MouvementImpl mvmnt;
			Calendar c;
			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					cell1 = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PNTG_MAT_COL_INDEX)));
					
					cell2 = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PNTG_DATE_COL_INDEX)));
					
					cell3 = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PNTG_HOUR_COL_INDEX)));
					if (cell1 != null && cell2 != null) {
						mvmnt = new MouvementImpl();
						mvmnt.setMatricule(new Double(cell1.getNumericCellValue()).intValue()+"");
						c = new GregorianCalendar();
						c.setTime(cell2.getDateCellValue());
						mvmnt.setDate(c.getTime());
						mvmnts.add(mvmnt);
					}
					
				}
				
			}
		} catch (Exception ioe) {
			ioe.printStackTrace();
		}
		return mvmnts;

	}
	public static HashMap<String, String> extractAffectPlaData(String fileName) {
		try {
			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheetAt(0);
			HSSFRow row;
			HSSFCell cell1;
			HSSFCell cell2;

			int rows; // No of rows
			rows = sheet.getPhysicalNumberOfRows();

			String startRowStr = MessageFactory.getSetting(Globals.IMPORT_XLS_ROW_START_INDEX); 
			int startRowIndex = ( startRowStr!= null) ? Integer.parseInt(startRowStr) : 0;
			HashMap<String, String> hash = new HashMap<String, String>();
			
			for (int r = startRowIndex; r < rows; r++) {
				row = sheet.getRow(r);
				if (row != null) {
					
					cell1 = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PROFIL_CODE_COL_INDEX)));
					
					cell2 = row.getCell(Integer.parseInt(MessageFactory.getSetting(Globals.IMPORT_XLS_PROFIL_LIBELLE_COL_INDEX)));
					if (cell1 != null && cell2 != null) {
						hash.put(cell1.getStringCellValue().trim(), cell2.getStringCellValue().trim());
					}
					
				}
				
			}
			return hash;
		} catch (Exception ioe) {
			ioe.printStackTrace();
			return null;
		}

	}
//	public static HierarchieData extractHierarchieData(String fileName, Hierarchie hrh) {
//		try {
//			POIFSFileSystem fs = new POIFSFileSystem(new FileInputStream(fileName));
//			HSSFWorkbook wb = new HSSFWorkbook(fs);
//			HSSFSheet sheet = wb.getSheetAt(0);
//			HSSFRow row;
//			HSSFCell cellCod;
//			HSSFCell cellLib;
//			String s;
//
//			int rows; // No of rows
//			rows = sheet.getPhysicalNumberOfRows();
//			System.out.println("rows :: " + rows);
//			int cols = 0; // No of columns
//			int tmp = 0;
//
//			HierarchieData hierarchie  = new HierarchieData();
//			
//			Noeud noeud = null;
//			Noeud rootNoeud;
//			Noeud lastSociete;
//			Noeud lastEtabliss;
//			Noeud lastServ;
//			List<Noeud> childs;
//			String startRowStr = "1"; 
//			int startRowIndex = ( startRowStr!= null) ? Integer.parseInt(startRowStr) : 0;
//			Double d;
//			int size=0;
//			for (int r = startRowIndex; r < rows; r++) {
//				row = sheet.getRow(r);
//				if (row != null) {
//					cellCod = row.getCell(Integer.parseInt("0"));
//					cellLib = row.getCell(Integer.parseInt("1"));
//					if(cellCod!=null && !"".equals(cellCod.getStringCellValue().trim())
//						&& cellLib!=null && !"".equals(cellLib.getStringCellValue().trim())){
//						noeud = new NoeudImpl();
//						noeud.setCode(cellCod.getStringCellValue());
//						noeud.setLibelle(cellLib.getStringCellValue());
//						noeud.setNiveau(((List<Niveau>)hrh.getNiveaus()).get(0));
//						hierarchie.setRootNode(noeud);
//						
//					}
//					cellCod = row.getCell(Integer.parseInt("2"));
//					cellLib = row.getCell(Integer.parseInt("3"));
//					if(cellCod!=null && !"".equals(cellCod.getStringCellValue().trim())
//						&& cellLib!=null && !"".equals(cellLib.getStringCellValue().trim())){
//						noeud = new NoeudImpl();
//						noeud.setCode(cellCod.getStringCellValue());
//						noeud.setLibelle(cellLib.getStringCellValue());
//						noeud.setNiveau(((List<Niveau>)hrh.getNiveaus()).get(1));
//						rootNoeud = hierarchie.getRootNode();
//						noeud.setNoeud(rootNoeud);
//						hierarchie.getSocietes().add(noeud);
//						rootNoeud.getNoeuds().add(noeud);
//					}
//					cellCod = row.getCell(Integer.parseInt("4"));
//					cellLib = row.getCell(Integer.parseInt("5"));
//					if(cellCod!=null && !"".equals(cellCod.getStringCellValue().trim())
//						&& cellLib!=null && !"".equals(cellLib.getStringCellValue().trim())){
//						noeud = new NoeudImpl();
//						noeud.setCode(cellCod.getStringCellValue());
//						noeud.setLibelle(cellLib.getStringCellValue());
//						noeud.setNiveau(((List<Niveau>)hrh.getNiveaus()).get(2));
//						size= hierarchie.getSocietes().size();
//						lastSociete = ((Noeud)hierarchie.getSocietes().get(size-1));
//						noeud.setNoeud(lastSociete);
//						hierarchie.getEtablissements().add(noeud);
//						
//						childs = lastSociete.getNoeuds();
//						childs.add(noeud);
//						lastSociete.setNoeuds(childs);
//						System.out.println("");
//						
//					}
//					cellCod = row.getCell(Integer.parseInt("6"));
//					cellLib = row.getCell(Integer.parseInt("7"));
//					if(cellCod!=null && !"".equals(cellCod.getStringCellValue().trim())
//						&& cellLib!=null && !"".equals(cellLib.getStringCellValue().trim())){
//						noeud = new NoeudImpl();
//						noeud.setCode(cellCod.getStringCellValue());
//						noeud.setLibelle(cellLib.getStringCellValue());
//						noeud.setNiveau(((List<Niveau>)hrh.getNiveaus()).get(3));
//						size= hierarchie.getEtablissements().size();
//						lastEtabliss = ((Noeud)hierarchie.getEtablissements().get(size-1));
//						noeud.setNoeud(lastEtabliss);
//						hierarchie.getServices().add(noeud);
//						lastEtabliss.getNoeuds().add(noeud);
//						
//					}
//					cellCod = row.getCell(Integer.parseInt("8"));
//					cellLib = row.getCell(Integer.parseInt("9"));
//					if(cellCod!=null && !"".equals(cellCod.getStringCellValue().trim())
//						&& cellLib!=null && !"".equals(cellLib.getStringCellValue().trim())){
//						noeud = new NoeudImpl();
//						noeud.setCode(cellCod.getStringCellValue());
//						noeud.setLibelle(cellLib.getStringCellValue());
//						noeud.setNiveau(((List<Niveau>)hrh.getNiveaus()).get(4));
//						size = hierarchie.getServices().size();
//						lastServ = ((Noeud)hierarchie.getServices().get(size-1));
//						noeud.setNoeud(lastServ);
//						hierarchie.getUnitorgs().add(noeud);
//						lastServ.getNoeuds().add(noeud);
//						
//					}
//				}
//				
//			}
//			return hierarchie;
//		} catch (Exception ioe) {
//			ioe.printStackTrace();
//			return null;
//		}
//
//	}
	public static void printNode(Noeud nd){
		if(nd.getNoeuds()!=null){
			Iterator<Noeud> it = nd.getNoeuds().iterator();
			while(it.hasNext()){
				printNode(it.next());
			}
			
		}
	}
	public static void main(String[] args) {
		//HierarchieData hierarchie = XlsReader.extractHierarchieData("d:/Hierarchie-import2.xls");
		//printNode(hierarchie.getRootNode());
		List<UserImpl> users = extractUserDataXLSX("D:\\Projets\\Checktime\\sntl\\excel data\\1-Collaborateurs.xlsx");
		System.out.println(users);
	}
}