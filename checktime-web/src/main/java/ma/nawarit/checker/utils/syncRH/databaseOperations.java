package ma.nawarit.checker.utils.syncRH;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import ma.nawarit.checker.utils.syncRH.*;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

public class databaseOperations {

	// JDBC driver name and database URL
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	public static propertiesOperator properties=new propertiesOperator();

	Connection conn = null;
	Statement stmt = null;

	public static Connection getConnection() throws ClassNotFoundException, SQLException, IOException {
		Class.forName(JDBC_DRIVER);
		return (Connection) DriverManager.getConnection(properties.getDatabaseName(), properties.getUserName(), properties.getPassword());
	}
	
	public databaseOperations() {
	}
	
	public Boolean deleteUnit(int id) throws ClassNotFoundException, SQLException, IOException {
		Class.forName("com.mysql.jdbc.Driver");
		conn = (Connection) DriverManager.getConnection(properties.getDatabaseName(), properties.getUserName(), properties.getPassword());
		stmt = (Statement) conn.createStatement();
		String sql;
		sql = "DELETE unit FROM unit where ID=" + id + "";
		Boolean result;
		result=stmt.execute(sql);
		stmt.close();
		conn.close();
		return result;
	}

	public static boolean deleteDuplicated(String type) throws ClassNotFoundException, SQLException, IOException {
		Class.forName(JDBC_DRIVER);
		Connection conn = (Connection) DriverManager.getConnection(properties.getDatabaseName(), properties.getUserName(), properties.getPassword());
		Statement stmt  = (Statement) conn.createStatement();
		String query="";
		if(type.equalsIgnoreCase("retard"))
			query="DELETE RETARD FROM RETARD INNER JOIN " + 
					"    (SELECT MIN(r.ID) AS MINID, " + 
					"	 r.DATE, r.RETARD_TOLERE, " + 
					"	 r.RETARD_NON_TOLERE, " + 
					"	 r.DESCRIPTION," + 
					"	 r.USER_FK, COUNT(DATE) as NOMBRE " + 
					"	 FROM RETARD AS r " + 
					"	 WHERE r.STATUT LIKE 'workflow_status_elapsed'" + 
					"	 GROUP BY r.DATE, " + 
					"	 r.RETARD_TOLERE, " + 
					"	 r.RETARD_NON_TOLERE," + 
					"	 r.DESCRIPTION," + 
					"	 r.USER_FK HAVING COUNT(DATE)>1) AS d" + 
					"   ON (d.DATE = RETARD.DATE" + 
					"   AND d.RETARD_TOLERE = RETARD.RETARD_TOLERE" + 
					"   AND d.RETARD_NON_TOLERE = RETARD.RETARD_NON_TOLERE" + 
					"   AND d.DESCRIPTION = RETARD.DESCRIPTION" + 
					"   AND d.USER_FK = RETARD.USER_FK" + 
					"   AND d.MINID <> RETARD.ID);";
		else if(type.equalsIgnoreCase("absence"))
			query="DELETE ABSENCE FROM ABSENCE INNER JOIN  " + 
					"    (SELECT  " + 
					"    	MIN(a.ID) AS MINID, " + 
					"		a.DATE_DEBUT,  " + 
					"		a.DATE_REPRISE,  " + 
					"		a.DUREE,  " + 
					"		a.TYPE_ABSENCE_FK,  " + 
					"		a.USER_FK,  " + 
					"		COUNT(a.DATE_DEBUT) AS COUNT  " + 
					"		FROM ABSENCE AS a WHERE a.STATUT LIKE 'workflow_status_elapsed' " + 
					"		GROUP BY a.DATE_DEBUT, a.DATE_REPRISE, a.DUREE, a.TYPE_ABSENCE_FK, a.USER_FK " + 
					"		HAVING COUNT(a.DATE_DEBUT)>1) AS d " + 
					"   ON (d.DATE_DEBUT = ABSENCE.DATE_DEBUT " + 
					"   AND d.DATE_REPRISE = ABSENCE.DATE_REPRISE " + 
					"   AND d.DUREE = ABSENCE.DUREE " + 
					"   AND d.TYPE_ABSENCE_FK = ABSENCE.TYPE_ABSENCE_FK " + 
					"   AND d.USER_FK = ABSENCE.USER_FK " + 
					"   AND d.MINID <> ABSENCE.ID);";
		else
			query="DELETE ABSENCE FROM ABSENCE WHERE DATE_DEBUT>NOW() AND DATE_REPRISE>NOW();";
		Boolean result;
		result=stmt.execute(query);
		stmt.close();
		conn.close();
		return result;
	}
}
