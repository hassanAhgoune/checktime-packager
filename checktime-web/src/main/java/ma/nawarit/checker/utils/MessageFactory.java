package ma.nawarit.checker.utils;

import java.text.MessageFormat;
import java.util.*;
import javax.faces.context.*;

public class MessageFactory {	
	static Locale locale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
	public static ResourceBundle bundle = ResourceBundle.getBundle("ma.nawar.msg.messages", locale);
	public static ResourceBundle helpBundle = ResourceBundle.getBundle("help-online", locale);
	static ResourceBundle settings = ResourceBundle.getBundle("settings");
		
	public static String getMessage(String key) {
		return bundle.getString(key);
	}
	public static String getMessage(String key, Object arg1) {
		return getMessage(key, new Object[] {arg1});
	}
	public static String getMessage(String key, Object arg1, Object arg2) {
		return getMessage(key, new Object[] {arg1, arg2});
	}
	public static String getMessage(String key, Object[] args) {
		if (args == null || args.length == 0) {
			return bundle.getString(key);
		}
		MessageFormat fmt = new MessageFormat(bundle.getString(key));
		fmt.setLocale(locale);
		return fmt.format(args);
		
	}
	
	public static String getSetting(String key, Object[] args) {
		if (args == null || args.length == 0) {
			return settings.getString(key);
		}
		MessageFormat fmt = new MessageFormat(settings.getString(key));
		
		return fmt.format(args);
		
	}

	
	public static String getSetting(String key) {
		return settings.getString(key);
	}
	public static String getHelpMessage(String key) {
		return helpBundle.getString(key);
	}
//	public static String getSetting(String key, Object[] args) {
//		if (args == null || args.length == 0) {
//			return settings.getString(key);
//		}
//		MessageFormat fmt = new MessageFormat(settings.getString(key));
//		
//		return fmt.format(args);
//		
//	}
//
//	
//	public static String getSetting(String key) {
//		return settings.getString(key);
//	}
//	public static String getHelpMessage(String key) {
//		return helpBundle.getString(key);
//	}
}
