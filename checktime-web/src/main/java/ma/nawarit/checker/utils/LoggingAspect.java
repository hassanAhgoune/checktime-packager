package ma.nawarit.checker.utils;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import ma.nawarit.checker.beans.compagnie.login.AuthenticationBean;
import ma.nawarit.checker.compagnie.User;

@Aspect
public class LoggingAspect {

	private static final Logger log = Logger.getLogger(LoggingAspect.class);

	@Before("execution(* ma.nawarit.checker..*.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		try {
			if(joinPoint.getTarget().getClass().getName().contains("crud") 
					&& !joinPoint.getSignature().getName().contains("read") 
					&& !joinPoint.getSignature().getName().contains("load")) {
				String res = "";
				res +=joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();

				Object[] signatureArgs = joinPoint.getArgs();
				if (signatureArgs != null || signatureArgs.length > 0) {
					res += ";(";
					for (Object item : signatureArgs) {
						if(item!=null)
							res += " " + item.toString() + " ";
						else
							res += " ";
					}
					res += ")";
				}
				else {
					res += ";()";
				}
				
				res+="; ";
				
				User user=AuthenticationBean.getCurrentUser();
				if (user != null)
					res += ";" + user.getMatricule() + " - "
							+ user.getNom() + " " + user.getPrenom();
				else {
					res +="; ...";
				}
				
				log.info(res);
			}
			
		}catch(Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

	@AfterThrowing(pointcut = "execution(* ma.nawarit.checker..*.*(..))", throwing = "error")
	public void logAfterThrowingException(JoinPoint joinPoint, Throwable error) {
		/*
		try {
			if(joinPoint.getTarget().getClass().toString().contains("crud")) {
				String res = "";
				res +=joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();
				
				Object[] signatureArgs = joinPoint.getArgs();
				if (signatureArgs != null || signatureArgs.length > 0) {
					res += ";(";
					for (Object item : signatureArgs) {
						res += " " + item.toString() + " ";
					}
					res += ")";
				}
				else {
					res += ";()";
				}
				
				if (error != null)
					res+=org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(error);
				if (AuthenticationBean.currentUser != null)
					res += ";" + AuthenticationBean.currentUser.getMatricule() + " - "
							+ AuthenticationBean.currentUser.getNom() + " " + AuthenticationBean.currentUser.getPrenom();
				else
					res += ";null";
				log.error(res);
			}
			
		}catch(Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
		*/
	}

	@AfterReturning(pointcut = "execution(* ma.nawarit.checker..*.*(..))", returning = "result")
	public void logAfterReturning(JoinPoint joinPoint, Object result) {
		try {
			if(joinPoint.getTarget().getClass().getName().contains("crud") 
					&& !joinPoint.getSignature().getName().contains("read") 
					&& !joinPoint.getSignature().getName().contains("load")) {
				String res = "";
				res += joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();
				
				Object[] signatureArgs = joinPoint.getArgs();
				if (signatureArgs != null || signatureArgs.length > 0) {
					res += ";(";
					for (Object item : signatureArgs) {
						res += " " + item.toString() + " ";
					}
					res += ")";
				}
				else {
					res += ";()";
				}
				
				
				if (result != null)
					res+=";"+result;
				else 
					res+="; ";
				
				User user=AuthenticationBean.getCurrentUser();
				if (user != null)
					res += ";" + user.getMatricule() + " - "
							+ user.getNom() + " " + user.getPrenom();
				else
					res += ";non authentifier";
				log.info(res);
			}
			
		}catch(Exception e) {
			log.error(org.apache.commons.lang.exception.ExceptionUtils.getStackTrace(e));
		}
	}

}
