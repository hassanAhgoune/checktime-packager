package ma.nawarit.checker.utils;

import java.util.Date;

import ma.nawarit.checker.core.common.Utils;

public class ImportModel{
	private Date date;
	private String matricule;
	private double duree;
	private String motif;
	

	public ImportModel(Date dat, String mat, double d, String m) {
		super();
		this.date = dat;
		this.matricule = mat;
		this.duree = d;
		this.motif = m;
	}
	public Date getDate() {
		return this.date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getMatricule() {
		return this.matricule;
	}
	public void setMatricule(String matricule) {
		this.matricule = matricule;
	}
	public double getDuree() {
		return duree;
	}
	public void setDuree(double duree) {
		this.duree = duree;
	}
	public String getMotif() {
		return motif;
	}
	public void setMotif(String motif) {
		this.motif = motif;
	}
	
	public String toString() {
		return matricule+";"+duree+";"+motif+Utils.convertDate2Str(date, "yyyyMMdd");
		
	}
}