package ma.nawarit.checker.utils.syncRH;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.Properties;

public class propertiesOperator {

	public final String fileName = "syncRH.properties";

	public propertiesOperator() {

	}
	
	public String getDatabaseName() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/conf/jdbc.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("hibernate.url");
	}
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getRetardConsumerPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("RetardConsumerPath");
	}
	
	public String getRetardProviderPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("RetardProviderPath");
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getAbsenceProviderPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("AbsenceConsumerPath");
	}
	public String getAbsenceConsumerPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("AbsenceProviderPath");
	}
	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public String getCongeProviderPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("CongeProviderPath");
	}
	public String getCongeConsumerPath() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("syncRH.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("CongeConsumerPath");
	}
	
	public String getUserName() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/conf/jdbc.properties");
		Properties prop = new Properties();
		prop.load(inputStream);
		inputStream.close();
		return prop.getProperty("hibernate.username");
	}
	
	public String getPassword() throws IOException {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/conf/jdbc.properties");
		Properties prop = new Properties();
		if(inputStream!=null) {
			prop.load(inputStream);
			inputStream.close();
		}
		return prop.getProperty("hibernate.password");
	}
	
	
	/*
	public Properties propertiesReader(String filename) {
		Properties prop = new Properties();
		try {
			InputStream input = null;
			input = getClass().getClassLoader().getResourceAsStream(filename);
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
			}
			prop.load(input);
			input.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
		return prop;
	}

	public void propertiesWriter(String filename, Properties pro) throws IOException {
		
			String filePath = this.getClass().getClassLoader().getResource(filename).getPath();
			InputStream input = getClass().getClassLoader().getResourceAsStream(filename);
			Properties prop = new Properties();
			if (input == null) {
				System.out.println("Sorry, unable to find " + filename);
				return;
			}
			prop.load(input);
			input.close();

			Enumeration<?> e = pro.propertyNames();
			if(e==null) {
				System.out.println("Sorry, to find any properties");
			}
			else
			{
				while (e.hasMoreElements()) {
					String key = (String) e.nextElement();
					String value = pro.getProperty(key);
					prop.setProperty(key, value);
				}
			}
			File configFile = new File(filePath);
			FileWriter writer = new FileWriter(configFile);
		    prop.store(writer, null);
		    writer.close();
	}

	public String getLastAbsenceID() {
		Properties prop = new Properties();
		prop=this.propertiesReader(fileName);
		return prop.getProperty("LastAbsenceID");
	}

	public String getLastCongeID() {
		Properties prop = new Properties();
		prop=this.propertiesReader(fileName);
		return prop.getProperty("LastCongeID");
	}

	public String getFolderPath() {
		Properties prop = new Properties();
		prop=this.propertiesReader(fileName);
		return prop.getProperty("FolderPath");
	}
	*/
}
