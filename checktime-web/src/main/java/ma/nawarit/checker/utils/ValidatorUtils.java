package ma.nawarit.checker.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;


/**
 * Utilitaire de validation.
 * 
 * <DL>
 * <DT><B>Nom du Projet :</B> <DD>CheckTime</DD>
 * <DT><B>Service       :</B><DD> NAWAR IT</DD>
 * <DT><B>Cr�e le       :</B><DD> 18 Mars 08</DD>
 * </DL>
 * @since 2008
 * @version 0.1
 * @author k.Lamhaddab
 */
public class ValidatorUtils {

	/** Logger de la classe **/
	private static final Logger log = Logger.getLogger(ValidatorUtils.class);
	
	/**
	 * Format de date court ("JJMMAA") pour optimiser la saisie.
	 * Ce format est cod� "ddMMyy" afin de d�finir un
	 * <code>java.text.SimpleDateFormat</code>.
	 */
	public static final String SHORT_DATE_FORMAT = "ddMMyy";

	/**
	 * Format de date standard ("JJ/MM/AAAA") pour saisie et affichage.
	 * Ce format est cod� "dd/MM/yyyy" afin de d�finir un
	 * <code>java.text.SimpleDateFormat</code>.
	 */
	public static final String STANDARD_DATE_FORMAT = "dd/MM/yyyy";

	/**
	 * Format de date standard sans les s�parateurs pour la saisie:
	 * Les formats sont cod�s afin de d�finir un
	 * <code>java.text.SimpleDateFormat</code>.
	 */
	public static final String STANDARD_DATE_FORMAT_WITHOUT_SEP = "ddMMyyyy";

	/**
	 * Format de date court avec les s�parateurs pour la saisie :
	 * Les formats sont cod�s afin de d�finir un
	 * <code>java.text.SimpleDateFormat</code>.
	 */
	public static final String SHORT_DATE_FORMAT_WITH_SEP = "dd/MM/yy";

	/**
	 * Format d�cimal par d�faut.
	 */
	public static final String DECIMAL_FORMAT = "###############.##";

	/**
	 * Formateur � utiliser pour analyser ou formatter une date
	 * suivant le format court <code>SHORT_DATE_FORMAT</code>.
	 */
	public static final SimpleDateFormat shortSDF =
								   new SimpleDateFormat(SHORT_DATE_FORMAT);

	/**
	 * Formateur � utiliser pour analyser ou formatter une date
	 * suivant le format standard <code>STANDARD_DATE_FORMAT</code>.
	 */
	public static final SimpleDateFormat standardSDF =
								   new SimpleDateFormat(STANDARD_DATE_FORMAT);

	/**
	 * Formateur � utiliser pour analyser ou formatter une date
	 * suivant le format standard <code>STANDARD_DATE_FORMAT_WITHOUT_SEP</code>.
	 */
	public static final SimpleDateFormat standardSDFWithoutSeparator =
								   new SimpleDateFormat(STANDARD_DATE_FORMAT_WITHOUT_SEP);

	/**
	 * Formateur � utiliser pour analyser ou formatter une date
	 * suivant le format standard <code>SHORT_DATE_FORMAT_WITH_SEP</code>.
	 */
	public static final SimpleDateFormat shortSDFWithSeparator =
								   new SimpleDateFormat(SHORT_DATE_FORMAT_WITH_SEP);

	/**
	 * Formateur � utiliser pour analyser ou formatter un nombre decimal
	 * suivant le format standard <code>DECIMAL_FORMAT</code>.
	 */
	public static final DecimalFormat df = new DecimalFormat(DECIMAL_FORMAT);
	
	/**
	 * v�rifie si le champ respecte le format d'un email ou pas 
	 * @param field
	 * @return vrai | faux
	 */
	public static boolean checkMail(String field){
		//String emailRegEx = "(\\w+\\.*)+@(\\w+\\.)(\\w+)(\\.\\w+)*";
		String emailRegEx = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)+$";
		return Pattern.matches(emailRegEx, field); 
	}

	/**
	 * v�rifie si le champ ne contient que des caract�re alphab�tiques ou pas [a-zA-Z���������]
	 * inclus aussi les caract�res accentu�s.
	 * @param field
	 * @return vrai | faux
	 */
	public static boolean checkName(String field){
		String alphaRegEx = "[a-zA-Z���������\\s-_]+"; 
		return Pattern.matches(alphaRegEx, field); 
	}


	/**
	 * v�rifie si le champ ne contient que des caract�re alphab�tiques ou pas [a-zA-Z���������]
	 * inclus aussi les caract�res accentu�s.
	 * @param field
	 * @return vrai | faux
	 */
	public static boolean checkAllName(String field){
		String alphaRegEx = "[a-zA-Z0-9\\.\\s-_]+"; 
		return Pattern.matches(alphaRegEx, field); 
	}
	/**
	 * v�rifie si le champ ne contient que des caract�re alphab�tiques ou pas [a-zA-Z���������]
	 * inclus aussi les caract�res accentu�s.
	 * @param field
	 * @return vrai | faux
	 */
	public static boolean checkAlphabetic(String field){
		String alphaRegEx = "[a-zA-Z-_]+"; 
 		return Pattern.matches(alphaRegEx, field); 
	}

	/**
	 * v�rifie si le champ ne contient que des caract�re alphanumeriques ou pas [a-zA-Z0-9���������]
	 * inclus aussi les caract�res accentu�s.
	 * @param field
	 * @return vrai | faux
	 */
	public static boolean checkAlphanumeric(String field){
		String alphaRegEx = "[a-zA-Z0-9-_]+"; 
		return Pattern.matches(alphaRegEx, field); 
	}
	/**
	 * V�rifie si une cha�ne correspond au format <b>x.x.*</b>.
	 * @return Retourne un boolean.
	 * @param value la valeur � v�rifier.
	 * @throws NumberFormatException Si la syntaxe du nombre est incorrecte.
	 */
	public static boolean checkVersion(String field){
		String versionRegEx = "(\\d+\\.)*(\\d+)"; 
		return Pattern.matches(versionRegEx, field); 
	}
		
	/**
	 * @param str La string � checker
	 * @return <code>true</code> si la string pass�e en param�tre est nulle ou
	 * vide, et <code>false</code> sinon
	 */
	public static boolean isEmpty(String str) {
		if (str == null || str.trim().equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * M�thode statique retournant une date java.sql.Date � partir d'une date
	 * String et du format correspondant.
	 * @param date La String sp�cifiant la date.
	 * @return La java.sql.Date correspondante.
	 * @since 4.0
	 */
	public static java.sql.Date checkDate(String date, String lang) {
			
		return convertToDate(displayDateByLang(date,lang));		
	}
	
	public static String displayDateByLang(String date, String lang) {
		if(date!=null && !date.trim().equals("")){
			
			String day = "";
			String month = "";
			String year = date.substring(6);
			if("en".equals(lang)){
				month = date.substring(0,2);
				day = date.substring(3,5);
			}else
			if("fr".equals(lang)){
				day = date.substring(0,2);
				month = date.substring(3,5);
			}
			return day + "/" + month + "/" + year;
		}
		return date;
	}
	/**
	 * M�thode statique retournant une date java.sql.Date � partir d'une date
	 * String et du format correspondant.
	 * @param date La String sp�cifiant la date.
	 * @return La java.sql.Date correspondante.
	 * @since 4.0
	 */
	private static java.sql.Date convertToDate(String date) {

		// La fonction "transformDateFormat()" permet de g�rer d'autres formats
		// que le format standard :
		java.util.Date utilDate = transformDateFormat(date);

		// On v�rifie que l'on obtient bien une date correcte :
		if (utilDate != null) {
			String vDate = standardSDF.format(utilDate);
			if (!date.equals(vDate)) {
			String msg = "convertToDate: Format de la date '" +
				date + "' non valide";
				log.error(msg);
				return null;
			}
		} else {
			String msg = "convertToDate: Format de la date'" +
				date + "' non valide";
			log.error(msg);
			return null;
		}

		long time = utilDate.getTime();
		java.sql.Date sqlDate = new java.sql.Date(time);
		return sqlDate;
	}


	/**
	 * M�thode statique retournant une date java.util.Date � partir d'une date
	 * String et du format correspondant.
	 * @param date La String sp�cifiant la date.
	 * @return La java.util.Date correspondante.
	 * @since 4.0
	 */
	public static java.util.Date convertToUtilDate(String date) {

		// La fonction "transformDateFormat()" permet de g�rer d'autres formats
		// que le format standard :
		java.util.Date utilDate = transformDateFormat(date);

		// On v�rifie que l'on obtient bien une date correcte :
		if (utilDate != null) {
			String vDate = standardSDF.format(utilDate);
			if (!date.equals(vDate)) {
			String msg = "convertToDate: Format de la date '" +
				date + "' non valide";
				return null;
			}
		} else {
			String msg = "convertToDate: Format de la date'" +
				date + "' non valide";
			return null;
		}
		return utilDate;
	}
	/**
	 * Transforme une date format�e selon 1 des 4 formats autoris�s, c'est �
	 * dire "JJ/MM/AAAA","JJ/MM/AA", "JJMMAAAA" ou "JJMMAA" en une date format�e
	 * au format standard (par d�faut "JJ/MM/AAAA").
	 * <ul>
	 * <li>Si la cha�ne de caract�res en entr�e ne respecte aucun des formats,
	 * ou le param�tre en entr�e est � null, alors on retourne null.</li>
	 * </ul>
	 * @param fDate La date � reformater.
	 * @return une java.util.Date format�e, si c'est une date du calendrier,
	 * ou null dans les autres cas.
	 */
	public static java.util.Date transformDateFormat(String fDate) {
		if (fDate == null) {
			return null;
		}
		// On essaye le format standard
		java.util.Date resultDate = testFormat(fDate, standardSDF);

		if (resultDate == null) {
			// On essaye le format shortSDF
			resultDate = testFormat(fDate, shortSDF);

			if (resultDate == null) {
				// On essaye le format standardSDFWithoutSeparator
				resultDate = testFormat(fDate, standardSDFWithoutSeparator);

				if (resultDate == null) {
					// On essaye le format shortSDFWithSeparator
					resultDate = testFormat(fDate, shortSDFWithSeparator);

				}
			}
		}

		return resultDate;
	}

	/**
	 * Teste si le format de la date est reconnu. si oui, on caste la date
	 * au format standard.
	 * @param fDate date � tester
	 * @param sdformat format de date � v�rifier
	 * @return outDate au format standard
	 */
	private static java.util.Date testFormat(String fDate, SimpleDateFormat sdformat) {
		try {
			java.util.Date d = sdformat.parse(fDate);

			// On v�rifie que l'on obtient bien une date correcte
			String vDate = sdformat.format(d);
			if (!fDate.equals(vDate)) {
				return null;
			}

			return standardSDF.parse(fDate);

		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * M�thode statique retournant un double r�sultat de la conversion de la
	 * cha�ne "value" en fonction du format par d�faut "###############.##".
	 * @param value La cha�ne de caract�re � transformer en double.
	 * @return double
	 * @throws IllegalArgumentException si la syntaxe du nombre est incorrecte.
	 */
	public static double convertToDouble(String value){
		Number num = df.parse(value, new ParsePosition(0));
		if (num == null) {
			String msg = "convertToDouble: Format du double '" +
				value + "' non valide";
			log.error(msg);
			throw new IllegalArgumentException();
		}

		return num.doubleValue();
	}

	/**
	 * M�thode statique retournant un double r�sultat de la conversion de la
	 * cha�ne "value" en fonction du format par d�faut "###############.##".
	 * @param value La cha�ne de caract�re � transformer en double.
	 * @param df Le DecimalFormat associ�. 
	 * @return double
	 * @throws IllegalArgumentException si la syntaxe du nombre est incorrecte.
	 */    
	public static double convertToDouble(String value, DecimalFormat df) {
		Number num = df.parse(value, new ParsePosition(0));
		if (num == null) {
			String msg = "convertToDouble: Format du double '" +
				value + "' non valide";
			log.error(msg);
			throw new IllegalArgumentException();
		}

		return num.doubleValue();
	}
    

	/**
	 * M�thode statique retournant un Double r�sultat de la conversion de la
	 * cha�ne "value" en fonction du format par d�faut "###############.##".
	 * @param value La cha�ne de caract�re � transformer en double.
	 * @return Double
	 */
	public static Double convertToDoubleClass(String value){
		double d = 0d;
		try{
			d = ValidatorUtils.convertToDouble(value);
		}catch(IllegalArgumentException iae){
			return null;
		}

		return new Double(d);
	}

	/**
	 * M�thode statique retournant un Double r�sultat de la conversion de la
	 * cha�ne "value" en fonction du format par d�faut "###############.##".
	 * @param value La cha�ne de caract�re � transformer en double.
	 * @param df Le DecimalFormat associ�. 
	 * @return Double
	 */
	public static Double convertToDoubleClass(String value, DecimalFormat df){
		
		double d = 0d;
		try{
			d = ValidatorUtils.convertToDouble(value, df);
		}catch(IllegalArgumentException iae){
			return null;	
		}
		
		return new Double(d);
	}
    

	/**
	 * Convertit une cha�ne de caract�res repr�sentant un nombre d�cimal en un
	 * java.math.BigDecimal, en fonction du format par d�faut "###############.##".
	 * @param value la valeur � convertir en un java.math.BigDecimal.
	 * @return BigDecimal
	 */
	public static BigDecimal convertToBigDecimal(String value){
		double d = 0d;
		try{
			d = ValidatorUtils.convertToDouble(value);
		}catch(IllegalArgumentException iae){
			return null;	
		}

		return new BigDecimal(d);
	}

	/**
	 * Convertit cha�ne de caract�res repr�sentant un nombre d�cimal en un
	 * <b>long</b>.
	 * @return Retourne un type long.
	 * @param value la valeur � convertir en un <b>long</b>.
	 * @throws IllegalArgumentException Si la syntaxe du nombre est incorrecte.
	 */
	public static long convertToLongType(String value)
								  throws IllegalArgumentException {
		try {
			long l = Long.parseLong(value);
			return l;
		} catch (NumberFormatException e) {
			String msg = "convertToLongType: Format du long '" +
				value + "' non valide";
			log.error(msg);
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Convertit cha�ne de caract�res repr�sentant un nombre d�cimal en un
	 * <b>Long</b>.
	 * @return Retourne une classe Long.
	 * @param value la valeur � convertir en un <b>Long</b>.
	 */
	public static Long convertToLongClass(String value){
		try {
			Long l = new Long(value);
			return l;
		} catch (NumberFormatException e) {
			String msg = "convertToLongClass: Format du long '" +
				value + "' non valide";
			log.error(msg);
			return null;
		}
	}

	/**
	 * Convertit cha�ne de caract�res repr�sentant un entier <b>int</b>.
	 * @return Retourne un int.
	 * @param value la valeur � convertir en un <b>int</b>.
	 * @throws IllegalArgumentException Si la syntaxe du nombre est incorrecte.
	 */
	public static int convertToInt(String value){
		try {
			int i = Integer.parseInt(value);
			return i;
		} catch (NumberFormatException e) {
			String msg = "convertToInt: Format du int '" +
				value + "' non valide";
			log.error(msg);
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Convertit cha�ne de caract�res repr�sentant un entier <b>Integer</b>.
	 * @return Retourne un Integer.
	 * @param value la valeur � convertir en un <b>Integer</b>.
	 */
	public static Integer convertToInteger(String value){
		try {
			Integer i = new Integer(value);
			return i;
		} catch (NumberFormatException e) {
			String msg = "convertToInteger: Format du int '" +
				value + "' non valide";
			log.error(msg);
			return null;
		}
	}

	/**
	 * retourne une date convertie en chaine de caract�res sous format dd/mm/yyyy.
	 * @param date
	 * @return
	 */
	public static String getStringDateByLang(Date date, String lang){
		
		int month = date.getMonth();
		int day = date.getDate();
		String monthStr = (month + 1 <10)?"0"+(month+1):""+ (month+1);
		String dayStr = (day + 1 <10)?"0"+day:""+ day;
		String dateStr = "";
		if("fr".equals(lang))
			dateStr = dayStr + "/" + monthStr + "/" + (date.getYear()+ 1900) ;
		else
		if("en".equals(lang))
			dateStr = monthStr + "/" + dayStr + "/" + (date.getYear()+ 1900) ;
		return dateStr;
	}
	
	/**
	 * retourne une date convertie en chaine de caract�res sous format dd/mm/yyyy.
	 * @param date
	 * @return
	 */
	public static String getStringDateWithHHMMByLang(Date date,String lang){

		int month = date.getMonth();
		int day = date.getDate();
		int hours = date.getHours();
		int minutes = date.getMinutes();
		String monthStr = (month + 1 <10)?"0"+(month+1):""+ (month+1);
		String dayStr = (day <10)?"0"+day:""+ day;
		String hourStr = (hours < 10)?"0"+hours:""+hours;
		String minStr = (minutes < 10)?"0"+minutes:""+minutes;
		String dateStr = "";
		if("fr".equals(lang))
			dateStr = dayStr + "/" + monthStr + "/" + (date.getYear()+ 1900)+ " " + hourStr+":" + minStr;
		else
		if("en".equals(lang))
			dateStr = monthStr + "/" + dayStr + "/" + (date.getYear()+ 1900)+ " " + hourStr+":" + minStr;
		return dateStr;		
	}
	
	/**
	 * r�cup�re une chaine de caract�re et renvoie une date.
	 * @param dateStr sous format dd/mm/yyyy
	 * @return Date | null en cas d'erreur
	 */
	private static Date getDateFromString(String dateStr){
		int date = Integer.parseInt(dateStr.substring(0, dateStr.indexOf("/")));
		int month = Integer.parseInt(dateStr.substring(dateStr.indexOf("/")+1, dateStr.lastIndexOf("/")));
		int year = Integer.parseInt(dateStr.substring(dateStr.lastIndexOf("/") + 1));
		return new Date(year-1900, month-1, date);
	}
	
	public static Date getDateFromStringByLang(String dateStr,String lang){
		return getDateFromString(displayDateByLang(dateStr,lang));
	}
		
	public static void main(String[] args) {
//		String str = "co po-yy_ii";
//		System.out.println("check alpha :: " + checkAlphabetic(str));
//		System.out.println("check alpha num :: " + checkAlphanumeric(str));
		String str = "a";
	
		for(int i=0; i<40; i++)
			str += "a";
		
		log.info("length :: " +str.length());
		log.info("checkmail :: " + checkMail(str));
	}
}
