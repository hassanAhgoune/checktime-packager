package ma.nawarit.checker.utils.propertiesOperator;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.MissingResourceException;
import java.util.Properties;

import org.apache.log4j.Logger;


public class PropertiesReader {

	private static final Logger log = Logger.getLogger(PropertiesReader.class);
	private static Properties props;
	
	public static Properties getProperties(String resource) throws IOException {
		Properties prop = new Properties();
		String stripped = resource.startsWith("/") ? resource.substring(1) : resource;
		InputStream stream = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader != null)
		    stream = classLoader.getResourceAsStream(stripped);
		if (stream == null)
		    stream = (PropertiesReader.class).getResourceAsStream(resource);
		if (stream == null)
		    stream = (PropertiesReader.class).getClassLoader().getResourceAsStream(stripped);
		if (stream == null)
		    stream = new FileInputStream(new File(resource));
		if (stream == null)
		    throw new IOException(resource + " not found");
		prop.load(stream);

		return prop;
	}
	
	public static void loadProperties(String resource) {
		try {
		    props = getProperties(resource);
		} catch (IOException e) {
		    log.fatal("could not load configuration file: " + resource, e);
		}
	    }
	
	public static String getProperty(String key) {
		try {
		    String value;
		    value = props.getProperty(key);
		    return value;
		} catch (NullPointerException e) {
		    log.error("La clé est nulle", e);
		    return key;
		} catch (MissingResourceException e) {
		    log.error("Valeur non trouvé pour la ressource '" + key + "'", e);
		    return key;
		} catch (ClassCastException e) {
		    log.error("La ressource '" + key + "' n'est pas de type String", e);
		    return key;
		}
	    }

}
