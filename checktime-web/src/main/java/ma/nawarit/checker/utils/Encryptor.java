package ma.nawarit.checker.utils;

import org.jasypt.spring.security.PasswordEncoder;
import org.jasypt.util.password.BasicPasswordEncryptor;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jasypt.util.text.BasicTextEncryptor;


public class Encryptor {
	
	public static String encrypt(String inputPassword) {
		BasicPasswordEncryptor passwordEncryptor = new BasicPasswordEncryptor();
		return passwordEncryptor.encryptPassword(inputPassword);
		
	}
	
	public static String decrypt(String myEncryptionPassword, String myText) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(myEncryptionPassword);
		String myEncryptedText = textEncryptor.encrypt(myText);
		return textEncryptor.decrypt(myEncryptedText);
	}
	public static String encode(String inputPassword) {
		PasswordEncoder pwEncoder =new PasswordEncoder();
		pwEncoder.setPasswordEncryptor(new StrongPasswordEncryptor());
		return pwEncoder.encodePassword(inputPassword, null);
		
	}
	public static String decode(String myEncryptionPassword) {
		BasicTextEncryptor textEncryptor = new BasicTextEncryptor();
		textEncryptor.setPassword(myEncryptionPassword);
		
		return textEncryptor.decrypt(myEncryptionPassword);
		
	}
	public static void main(String[] args) {
	}

}

