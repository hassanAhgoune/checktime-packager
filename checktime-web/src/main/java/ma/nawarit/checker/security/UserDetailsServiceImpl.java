package ma.nawarit.checker.security;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.compagnie.ProfilApp;
import ma.nawarit.checker.compagnie.Role;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserDao;
import ma.nawarit.checker.utils.Encryptor;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.GrantedAuthorityImpl;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.dao.DataAccessException;

public class UserDetailsServiceImpl implements UserDetailsService {
	private static final Logger log = Logger.getLogger(UserDetailsServiceImpl.class);
	private UserDao userDao;

	
	public UserDetailsServiceImpl(){
		log.info("UserDetailsServiceImpl  ");
		
	}
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		
		UserDetails usrdetail = makeAcegiUser(loadUser(username));
		return usrdetail;
		
	}
	public User loadUser(String username)throws UsernameNotFoundException, DataAccessException  {
		
		log.info("loadUser : " + username);
		Hashtable< String, String> hash = new Hashtable<String, String>();
		hash.put("login", username);
		List<User> users = userDao.read(hash);
		User user = null;
		if(users != null && users.size()==1)
			 user = users.get(0); 
		if (user == null){
			log.info("User not found: " + username);
			throw new UsernameNotFoundException("User not found: " + username);
		}
		log.info("USER searched : " + user.getNom() +" " + user.getPrenom());
		return user;
	}

	private org.acegisecurity.userdetails.User makeAcegiUser(User user) {
		
		return new org.acegisecurity.userdetails.User(user.getLogin(),user
				.getPassword(), true, true, true, true,
				makeGrantedAuthorities(user));
	}

	private GrantedAuthority[] makeGrantedAuthorities(User user) {
		GrantedAuthority[] result = new GrantedAuthority[user.getRoles().size()];
		int i = 0;
		Iterator<Role> it = user.getRoles().iterator();
		Role role = null;
		while (it.hasNext()) {
			role = it.next();
			ProfilApp p = role.getProfilApp();
			result[i++] = new GrantedAuthorityImpl(role.getProfilApp().getCode());
		}
		return result;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

}
