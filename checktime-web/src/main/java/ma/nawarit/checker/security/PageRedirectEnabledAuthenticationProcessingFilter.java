package ma.nawarit.checker.security;

import org.acegisecurity.Authentication;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;

public class PageRedirectEnabledAuthenticationProcessingFilter extends AuthenticationProcessingFilter {

	private PageRedirectDefinitionSource pageRedirectDefinitionSource;

	public String getDefaultTargetUrl() {

		Authentication authResult = SecurityContextHolder.getContext().getAuthentication();

		PageRedirectDefinition definition = getPageRedirectDefinitionSource()
				.getDefinitionFor(authResult.getAuthorities());

		String targetUrl = super.getDefaultTargetUrl();
		if (definition != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("PageRedirectDefinition found :" + definition);
			}

			targetUrl = "/" + definition.getPageName();
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("PageRedirectDefinition not found for authentication :" + authResult);
			}
		}

		logger.info("Default target url :" + targetUrl);

		return targetUrl;

	}

	public PageRedirectDefinitionSource getPageRedirectDefinitionSource() {
		return pageRedirectDefinitionSource;
	}

	public void setPageRedirectDefinitionSource(PageRedirectDefinitionSource pageRedirectDefinitionSource) {
		this.pageRedirectDefinitionSource = pageRedirectDefinitionSource;
	}
}
