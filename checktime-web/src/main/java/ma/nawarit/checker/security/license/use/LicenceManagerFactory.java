package ma.nawarit.checker.security.license.use;

import net.nicholaswilliams.java.licensing.FileLicenseProvider;
import net.nicholaswilliams.java.licensing.LicenseManager;
import net.nicholaswilliams.java.licensing.LicenseManagerProperties;

public class LicenceManagerFactory {
	private LicenseManager manager;
	public LicenceManagerFactory() {
		// TODO Auto-generated constructor stub
		LicenseManagerProperties
			.setPublicKeyDataProvider(new PublicKeyProvider());
		LicenseManagerProperties
			.setPublicKeyPasswordProvider(new PublicPasswordProvider());
		FileLicenseProvider licenseProvider = new FileLicenseProvider();
		licenseProvider.setBase64Encoded(true);
		LicenseManagerProperties.setLicenseProvider(licenseProvider);
		//
		// // Optional; set only if you are using a different password to
		// encrypt licenses than your public key
		// LicenseManagerProperties.setLicensePasswordProvider(new
		// LicensePasswordProvider());
		//
		// // Optional; set only if you wish to validate licenses
		LicenseManagerProperties.setLicenseValidator(new MyLicenseValidator());
		
		// Optional; defaults to 0, which translates to a 10-second (minimum)
		// cache time
		LicenseManagerProperties.setCacheTimeInMinutes(5);
		
		manager = LicenseManager.getInstance();
	}
	public LicenseManager getManager() {
		return manager;
	}
	
}
