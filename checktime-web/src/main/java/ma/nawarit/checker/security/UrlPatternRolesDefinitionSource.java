package ma.nawarit.checker.security;

import java.util.List;

import ma.nawarit.checker.security.crud.UrlPatternManageableService;

import org.acegisecurity.ConfigAttribute;
import org.acegisecurity.ConfigAttributeDefinition;
import org.acegisecurity.SecurityConfig;
import org.acegisecurity.intercept.web.PathBasedFilterInvocationDefinitionMap;
import org.apache.commons.lang.StringUtils;

public class UrlPatternRolesDefinitionSource extends
		PathBasedFilterInvocationDefinitionMap {

	private UrlPatternManageableService urlPatternService;
	private String isAuthenticatedAnonymouslyPages;
	private static final String IS_AUTHENTICATED_ANONYMOUSLY = "IS_AUTHENTICATED_ANONYMOUSLY";

	public void init() {

		try {

			List<UrlPattern> urlPatternsList = urlPatternService.readAll();
			addAuthenticatedAnonymouslyPages();
			for (UrlPattern pattern : urlPatternsList) {

				ConfigAttributeDefinition configDefinition = new ConfigAttributeDefinition();

				for (UrlPatternRole role : (List<UrlPatternRole>) pattern.getUrlPatternRoles()) {

					ConfigAttribute config = new SecurityConfig(role.getProfilApp().getCode());

					configDefinition.addConfigAttribute(config);
				}
				
				addSecureUrl(pattern.getUrlPath(), configDefinition);

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void addAuthenticatedAnonymouslyPages(){
		
		if(StringUtils.isNotEmpty(isAuthenticatedAnonymouslyPages)){
			String[] pages = isAuthenticatedAnonymouslyPages.split(",");
			ConfigAttributeDefinition configDefinition = new ConfigAttributeDefinition();
			ConfigAttribute config = new SecurityConfig(IS_AUTHENTICATED_ANONYMOUSLY);
			configDefinition.addConfigAttribute(config);
			addSecureUrl(pages[0].trim(), configDefinition);
			addSecureUrl(pages[1].trim(), configDefinition);
		}
	}
	public void setUrlPatternService(UrlPatternManageableService urlPatternService) {
		this.urlPatternService = urlPatternService;
	}

	public String getIsAuthenticatedAnonymouslyPages() {
		return isAuthenticatedAnonymouslyPages;
	}

	public void setIsAuthenticatedAnonymouslyPages(
			String isAuthenticatedAnonymouslyPages) {
		this.isAuthenticatedAnonymouslyPages = isAuthenticatedAnonymouslyPages;
	}
}