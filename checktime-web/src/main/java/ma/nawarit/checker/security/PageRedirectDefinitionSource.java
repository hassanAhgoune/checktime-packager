package ma.nawarit.checker.security;

import ma.nawarit.checker.compagnie.ProfilApp;
import ma.nawarit.checker.compagnie.crud.ProfilAppManageableService;

import org.apache.commons.logging.LogFactory;
import org.apache.commons.logging.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;

import org.acegisecurity.GrantedAuthority;


public class PageRedirectDefinitionSource {
	 
	private static final Log logger = LogFactory.getLog(PageRedirectDefinitionSource.class);
	 
	 private List definitionsList;
	 private ProfilAppManageableService profilAppService;
	 
	 public PageRedirectDefinition getDefinitionFor(GrantedAuthority[] authorities) {
	  
	  List roleList = new ArrayList();
	  
	  for (int i = 0; i < authorities.length; i++) {
	   roleList.add(authorities[i].getAuthority());
	  }
	  
	  for (Iterator iter = getDefinitionsList().iterator(); iter.hasNext();) {
	   PageRedirectDefinition definition = (PageRedirectDefinition) iter.next();
	   if(roleList.contains(definition.getRoleName())) {
	    logger.debug("First matched PageRedirectDefinition :" + definition);
	    return definition;
	   }
	  }
	  
	  return null;
	 }
	 public void init(){
		 try {
				if(this.profilAppService != null){
					 List allProfilApps = this.profilAppService.readAll();
					 if(allProfilApps != null){
						 Iterator<ProfilApp> it = allProfilApps.iterator();
						 ProfilApp profilApp;
						 PageRedirectDefinition pageRedirect;
						 definitionsList = new ArrayList<PageRedirectDefinition>();
						 while(it.hasNext()){
							 profilApp = it.next();
							 pageRedirect = new PageRedirectDefinition();
							 pageRedirect.setRoleName(profilApp.getCode());
							 pageRedirect.setPageName(profilApp.getRedirectPage());
							 definitionsList.add(pageRedirect);
						 }
					 }
				 }
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	 }
	 public List getDefinitionsList() {
		 return definitionsList;
	 }
	 public void setDefinitionsList(List definitionsList) {
	  this.definitionsList = definitionsList;
	 }

	public ProfilAppManageableService getProfilAppService() {
		return profilAppService;
	}

	public void setProfilAppService(ProfilAppManageableService profilAppService) {
		this.profilAppService = profilAppService;
	}

	
	}
