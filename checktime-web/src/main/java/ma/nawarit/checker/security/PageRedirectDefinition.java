package ma.nawarit.checker.security;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

public class PageRedirectDefinition {
	 private String roleName;
	 private String pageName;
	 
	 
	 public PageRedirectDefinition() {}
	 
	 public PageRedirectDefinition(String roleName, String pageName) {
	  setRoleName(roleName);
	  setPageName(pageName);
	 }
	 
	 public String getRoleName() {
	  return roleName;
	 }
	 public void setRoleName(String roleName) {
	  this.roleName = roleName;
	 }
	 
	
	 public String toString() {
	  return ToStringBuilder.reflectionToString(this);
	 }
	 
	 public boolean equals(Object o) {
	  return EqualsBuilder.reflectionEquals(this,o);
	 }
	 
	 public int hashCode() {
	  return HashCodeBuilder.reflectionHashCode(this);
	 }
	 
	 
	 public String getPageName() {
	  return pageName;
	 }
	 public void setPageName(String pageName) {
	  this.pageName = pageName;
	 }
	}
