package ma.nawarit.checker.security.license.use;

import ma.nawarit.checker.security.license.*;
import net.nicholaswilliams.java.licensing.*;
import net.nicholaswilliams.java.licensing.exception.*;

public class LicenseUsageService {
	public LicenseUsageService() {
		// TODO Auto-generated constructor stub
		LicenseManagerProperties
				.setPublicKeyDataProvider(new PublicKeyProvider());
		LicenseManagerProperties
				.setPublicKeyPasswordProvider(new PublicPasswordProvider());
		FileLicenseProvider licenseProvider = new FileLicenseProvider();
		licenseProvider.setBase64Encoded(true);
		LicenseManagerProperties.setLicenseProvider(licenseProvider);
		//
		// // Optional; set only if you are using a different password to
		// encrypt licenses than your public key
		// LicenseManagerProperties.setLicensePasswordProvider(new
		// LicensePasswordProvider());
		//
		// // Optional; set only if you wish to validate licenses
		 LicenseManagerProperties.setLicenseValidator(new MyLicenseValidator());

		// Optional; defaults to 0, which translates to a 10-second (minimum)
		// cache time
		LicenseManagerProperties.setCacheTimeInMinutes(5);

		LicenseManager.getInstance();
	}

	public void useLicense() {
		LicenseManager manager = LicenseManager.getInstance();
		License license = manager.getLicense("ctm.li");
		try {
			manager.validateLicense(license);
		} catch (ExpiredLicenseException e) {
			return;
		} catch (InvalidLicenseException e) {
			return;
		}

		// int seats = license.getNumberOfLicenses();

		boolean bool;
		try {
			bool = manager.hasLicenseForAllFeatures("ctm.li", "FEATURE1","FEATURE2");
		} catch (ExpiredLicenseException e) {
			bool = false;
		} catch (InvalidLicenseException e) {
			bool = false;
		}
	}

	public static void main(String[] args) {
		LicenseUsageService use = new LicenseUsageService();
		use.useLicense();
	}
}
