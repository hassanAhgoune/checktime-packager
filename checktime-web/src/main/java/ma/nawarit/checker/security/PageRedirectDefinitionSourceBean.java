package ma.nawarit.checker.security;


import java.beans.PropertyEditorSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.compagnie.ProfilApp;
import ma.nawarit.checker.compagnie.crud.ProfilAppManageableService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class PageRedirectDefinitionSourceBean extends PropertyEditorSupport {
	 private static final Log logger = LogFactory.getLog(PageRedirectDefinitionSourceBean.class);
	 private ProfilAppManageableService profilAppService;    
	 
	 
	 public void init()  {
		 try {
			 
			 PageRedirectDefinitionSource definitionSource = new PageRedirectDefinitionSource();
			 List definitionsList = null;
			 if(this.profilAppService != null){
				 List allProfilApps = this.profilAppService.readAll();
				 if(allProfilApps != null){
					 Iterator<ProfilApp> it = allProfilApps.iterator();
					 ProfilApp profilApp;
					 PageRedirectDefinition pageRedirect;
					 definitionsList = new ArrayList<PageRedirectDefinition>();
					 while(it.hasNext()){
						 profilApp = it.next();
						 pageRedirect = new PageRedirectDefinition();
						 pageRedirect.setRoleName(profilApp.getCode());
						 pageRedirect.setPageName(profilApp.getRedirectPage());
						 definitionsList.add(pageRedirect);
					 }
				 }
			 }
			definitionSource.setDefinitionsList(definitionsList);
			setValue(definitionSource);
		 } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	 }
	 public void setProfilAppService(ProfilAppManageableService profilAppService) {
			this.profilAppService = profilAppService;
		}
	public ProfilAppManageableService getProfilAppService() {
		return profilAppService;
	}
	}
