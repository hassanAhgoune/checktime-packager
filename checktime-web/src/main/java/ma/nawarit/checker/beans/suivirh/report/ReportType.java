package ma.nawarit.checker.beans.suivirh.report;

import java.util.Date;

public class ReportType {
	
	private String name;
	
	private Date createdDate;
	
	private Date dateDebut;
	
	private Date dateFin;
	
	private String type;

	public ReportType() {
		// TODO Auto-generated constructor stub
	}
	public ReportType(ReportType r) {
		this.createdDate = r.createdDate;
		this.dateDebut = r.dateDebut;
		this.dateFin = r.dateFin;
		this.name = r.name;
		this.type = r.type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
