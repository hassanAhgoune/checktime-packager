package ma.nawarit.checker.beans.journal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.SequenceInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.commons.io.FilenameUtils;

import ma.nawarit.checker.journal.Action;
import ma.nawarit.checker.synchronizer.Consumer;

@ManagedBean(name = "journalBean")
@ViewScoped
public class JournalBean {

	private List<Action> actions;
	private List<Action> filtredActions;
	
	public JournalBean() {
		actions=new ArrayList<Action>();
		filtredActions=new ArrayList<Action>();
	}
	
	public void loadActions() throws IOException {
		this.getActions(getLines());
		for (Action item : actions) {

			System.out.println(item.getDate()+" - "+item.getName()+" - "+item.getParameters()+" - "+item.getResult()+" - "+item.getUser());
		}
	}
	
	public java.util.List<String> getLines() throws IOException {
		List<String> lines=new ArrayList<String>();
		File file=new File(System.getProperty("catalina.base") + "/logs");
		if(file.isDirectory()) {
			List<InputStream> inputList=new ArrayList<InputStream>();
			for (File item : file.listFiles()) {
				if(FilenameUtils.getExtension(item.getName()).equals("csv")) {
					InputStream input = new FileInputStream(item);;
					inputList.add(input);
				}
			}
			
			SequenceInputStream is=new SequenceInputStream(Collections.enumeration(inputList));
			BufferedReader reader=new BufferedReader(new InputStreamReader(is, Consumer.CHARSET));
			String line;
		      while((line = reader.readLine()) != null && !line.isEmpty()){
		    	  lines.add(line);
		      }
		}
		return lines;
	}
	
	public void getActions(List<String> lines) {
		actions=new ArrayList<Action>();
		if(lines!=null) {
			for (String line : lines) {
				String[] cells=line.split(";");
				Action act=new Action();
				act.setDate(cells[0]);
				act.setName(cells[1]);
				act.setParameters(cells[2]);
				act.setResult(cells[3]);
				act.setUser(cells[4]);
				actions.add(act);
			}
		}
	}
	
	public List<Action> getActions() {
		return actions;
	}

	public void setActions(List<Action> actions) {
		this.actions = actions;
	}

	public List<Action> getFiltredActions() {
		return filtredActions;
	}

	public void setFiltredActions(List<Action> filtredActions) {
		this.filtredActions = filtredActions;
	}
}
