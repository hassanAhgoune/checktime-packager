package ma.nawarit.checker.beans.jbpm;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.http.HttpServletRequest;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.jbpm.DecisionHandler;
import ma.nawarit.checker.jbpm.JbpmStatable;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.TreeNode;

@ManagedBean(name = "jbpmBean")
@RequestScoped
public class JbpmBean {
	private static final long serialVersionUID = 1506754237590967483L;

	private static final Logger logger = Logger.getLogger(JbpmBean.class);

	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService;
	
	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsenceService;
	
	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeService;
	
	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService;
	
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;
	
	@ManagedProperty(value = "#{decisionHandler}")
	private DecisionHandler decisionHandler;
	
	@ManagedProperty(value = "#{AdvParamManageableService}")
	private AdvParamManageableService advParamService;

	private JbpmStatable jbpmEntitie;
	private List<JbpmStatable> jbpmEntities;
	private boolean mailActive;
	private boolean wkfAccept;
	private String comment;

	private AbsenceImpl absence;
	private JbpmDataModel hrDataModel;
	private DataTable absencesTable = new DataTable();

	private CongeImpl conge;
	private JbpmDataModel hrCongesDataModel;
	private DataTable congesTable = new DataTable();


	private User user;
	private FacesContext context = FacesContext.getCurrentInstance();
	private String workflowUrl;
	
	private CommonCriteria criteria;
	private TreeNode selectedNoeudNode;
	private TreeNode selectedUnitNode;

	private boolean isAdmin = false;

	public JbpmBean() {
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		user = (User) obj;
		HttpServletRequest origRequest = (HttpServletRequest)context.getExternalContext().getRequest();
		workflowUrl = "http://"+origRequest.getServerName()+":"+origRequest.getServerPort()+"/checktime-web";
		criteria=new CommonCriteria();
		criteria.setProcessActive(true);
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		UserDetails currentUser = (UserDetails) principal;
		GrantedAuthority authority = currentUser.getAuthorities()[0];
		isAdmin = "ROLE_ADMIN".equals(authority.getAuthority());
	}
	
	public void createJbpmObjectActionListner(ActionEvent ev){
		try {

			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			absence.setCollaborateur(user);
			absence.setDateDebut(new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2014 08:00:00"));
			absence.setDateReprise(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse("01/01/2014 12:00:00"));
			absence.setTypeAbsence(typeAbsenceService.load(1));
			
			absence.setStatut("workflow_status_init");
			
			HashMap<String, String> hash = new HashMap<String, String>();
			hash.put("sender", user.getMail());
			hash.put("receiver", user.getSupH().getMail());
			hash.put("subject", "Nouvelle déclaration Absence");
			hash.put("manager", user.getSupH().getNom());
			hash.put("userName", user.getNom() + " " + user.getPrenom());
			hash.put("profil", "profil");
			hash.put("workflowName", "Absence");
			hash.put("workflowUrl", "http://localhost:9080/checktime-web/pages/jbpm/jbpm.wit");
			hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
			
			int processId = decisionHandler.deployProcessDefinition(hash, "process-workflow", mailActive);
			absence.setIdProcess(processId);
			absenceService.create(absence);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void managerTakeDecisionAction(){
		try {
			absence = (AbsenceImpl)jbpmEntitie;
			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			if(decisionHandler.isManagerDecisionReached(jbpmEntitie)){
				// To get from screen form
				comment = "Manager : "+comment;
				boolean accept = true;
				
				// Merge mail Template
				User u = absence.getCollaborateur();
				HashMap<String, String> hash = new HashMap<String, String>();
				hash.put("sender", u.getSupH().getMail());
				if(accept){
					hash.put("receiver", u.getSupH().getSupH().getMail());
					hash.put("rhManager", u.getSupH().getSupH().getNom());
				}
				else
					hash.put("receiver", u.getMail());
				hash.put("subject", "Approbation déclaration Absence - (" + u.getNom() + " " + u.getPrenom()+")");
				
				hash.put("manager", u.getSupH().getNom());
				hash.put("managerMail", u.getSupH().getMail());
				hash.put("managerComment", comment);
				hash.put("userName", u.getNom() + " " + u.getPrenom());
				hash.put("userMail", u.getMail());
				hash.put("profil", "profil");
				hash.put("workflowName", "Absence");
				hash.put("workflowUrl", "http://localhost:9080/checktime-web/pages/jbpm/jbpm.wit");
				hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

				// Take a decision
				decisionHandler.managerTakeDecision(absence.getIdProcess(), accept, comment, hash, mailActive);
				if(!accept){
					absence.setStatut("workflow_status_rejected");
					absenceService.update(absence);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}					
	}
	
	public void hrTakeDecisionActionListner(boolean wkfAccept){
		try {
			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			boolean hrDecision = decisionHandler.isHrDecisionReached((JbpmStatable)absence);
			if (!hrDecision)
				managerTakeDecisionAction();
			hrDecision = decisionHandler.isHrDecisionReached((JbpmStatable)absence);
			if(hrDecision){
				// To get from screen form
				comment = "HR : "+comment;
				
				// Merge mail Template
				User u = absence.getCollaborateur();
				HashMap<String, String> hash = new HashMap<String, String>();
//				hash.put("sender", u.getSupH().getMail());
//				if(wkfAccept)
//					hash.put("receiver", u.getMail());
//				else
//					hash.put("receiver", u.getMail() + ";"+ u.getSupH().getMail());
				
				hash.put("subject", "Approbation déclaration Absence - (" + u.getNom() + " " + u.getPrenom()+")");
				
//				hash.put("manager", u.getSupH().getNom());
//				hash.put("managerMail", u.getSupH().getMail());
//				hash.put("rhManager", u.getSupH().getSupH().getNom());
				hash.put("rhComment", comment);
//				hash.put("rhMail", u.getSupH().getSupH().getMail());
				hash.put("userName", u.getNom() + " " + u.getPrenom());
//				hash.put("userMail", u.getMail());
				hash.put("dateDebut", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(absence.getDateDebut()));
				hash.put("dateFin", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(absence.getDateReprise()));
				hash.put("workflowName", "Absence");
				hash.put("workflowUrl", "http://localhost:9080/checktime-web/pages/jbpm/jbpm.wit");
				hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

				// Take a decision
				decisionHandler.hrTakeDecision(absence.getIdProcess(), wkfAccept, comment, hash, mailActive);
				if(!wkfAccept){
					absence.setStatut("workflow_status_rejected");
					absenceService.update(absence);
				}else{
					absence.setStatut("workflow_status_validated");
					absenceService.update(absence);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}					
	}

	public void deleteAbsence(){
		try {
			if ((isAdmin && !Globals.WORKFLOW_STATUS_ELAPSED.equals(absence.getStatut()))
					|| Globals.WORKFLOW_STATUS_ENCOURS.equals(absence.getStatut()) || Globals.WORKFLOW_STATUS_VALIDATE.equals(absence.getStatut())) {
				absenceService.delete(absence);
				reloadPage();
			}
		}catch (Exception e){
			logger.error("Erreur de suppression d'absence",e);
		}
	}

	public void deleteConge(){
		try {
			if ((isAdmin && !Globals.WORKFLOW_STATUS_ELAPSED.equals(conge.getStatut()))
					|| Globals.WORKFLOW_STATUS_ENCOURS.equals(conge.getStatut()) || Globals.WORKFLOW_STATUS_VALIDATE.equals(conge.getStatut())) {
				congeService.delete(conge);
				reloadPage();
			}
		}catch (Exception e){
			logger.error("Erreur de suppression de cong\u00E9",e);
		}
	}

	public void reloadPage(){
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		try {
			ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void hrTakeCongeDecisionListner(boolean wkfAccept){
		try {
			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			boolean hrDecision = decisionHandler.isHrDecisionReached((JbpmStatable)conge);
			if (!hrDecision)
				managerTakeDecisionAction();
			hrDecision = decisionHandler.isHrDecisionReached((JbpmStatable)conge);
			if(hrDecision){
				// To get from screen form
				comment = "HR : "+comment;
				
				// Merge mail Template
				User u = conge.getCollaborateur();
				HashMap<String, String> hash = new HashMap<String, String>();
//				hash.put("sender", u.getSupH().getMail());
//				if(wkfAccept)
//					hash.put("receiver", u.getMail());
//				else
//					hash.put("receiver", u.getMail() + ";"+ u.getSupH().getMail());
				
				hash.put("subject", "Approbation déclaration Congé - (" + u.getNom() + " " + u.getPrenom()+")");
				
//				hash.put("manager", u.getSupH().getNom());
//				hash.put("managerMail", u.getSupH().getMail());
//				hash.put("rhManager", u.getSupH().getSupH().getNom());
				hash.put("rhComment", comment);
//				hash.put("rhMail", u.getSupH().getSupH().getMail());
				hash.put("userName", u.getNom() + " " + u.getPrenom());
				hash.put("userMail", u.getMail());
				hash.put("dateDebut", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(conge.getDateDebut()));
				hash.put("dateFin", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(conge.getDateReprise()));
				hash.put("workflowName", "Conge");
				hash.put("workflowUrl", workflowUrl+"/pages/jbpm/jbpm.wit");
				hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

				// Take a decision
				decisionHandler.hrTakeDecision(conge.getIdProcess(), wkfAccept, comment, hash, mailActive);
				if(!wkfAccept){
					conge.setStatut(Globals.WORKFLOW_STATUS_REJECTED);
					congeService.update(conge);
				}else{
					conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					congeService.update(conge);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}					
	}
	
	public void refreshData(String type) {
		try {
			criteria.setProcessActive(true);
			if (selectedNoeudNode != null)
				criteria.setNoeudId(String.valueOf(((Noeud) selectedNoeudNode.getData()).getId()));
			if (selectedUnitNode != null)
				criteria.setSiteId(String.valueOf(((Unit) selectedUnitNode.getData()).getId()));
			if(type.equalsIgnoreCase("conge"))
				hrCongesDataModel = new JbpmDataModel(decisionHandler.fetchObjects4Manager(congeService.readByCriteria(criteria)));
			if(type.equalsIgnoreCase("absence"))
				hrDataModel = new JbpmDataModel(decisionHandler.fetchObjectsWithVariables(absenceService.readByCriteria(criteria)));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	public JbpmDataModel getHrDataModel() {
		try {
			//hrDataModel = new JbpmDataModel(decisionHandler.fetchObjects4Hr(absenceService.readByCriteria(criteria)));
			if (hrDataModel == null || hrDataModel.isEmpty())
				hrDataModel = new JbpmDataModel(absenceService.readByCriteria(criteria));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hrDataModel;
	}

	public JbpmDataModel getHrCongesDataModel() {
		try {
			//put here some criteria
			//Hashtable<String, String> hash = new Hashtable<String, String>();
			//hash.put("statut", Globals.WORKFLOW_STATUS_ENCOURS);
			
			hrCongesDataModel = new JbpmDataModel(decisionHandler.fetchObjects4Manager(congeService.readByCriteria(criteria)));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return hrCongesDataModel;
	}
	
/*	

	public void setHrCongesDataModel(JbpmDataModel hrCongesDataModel) {
		this.hrCongesDataModel = hrCongesDataModel;
	
	public JbpmDataModel getAcceptedDataModel() {
		try {
			//put here some criteria
			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put("statut", "workflow_status_validated");
			if (acceptedDataModel == null || acceptedDataModel.isEmpty())
				acceptedDataModel = new JbpmDataModel(decisionHandler.fetchObjectsWithVariables(absenceService.read(hash)));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return acceptedDataModel;
	}
*/
	public DecisionHandler getDecisionHandler() {
		return decisionHandler;
	}

	public DataTable getAbsencesTable() {
		return absencesTable;
	}

	public void setAbsencesTable(DataTable absencesTable) {
		this.absencesTable = absencesTable;
	}

	public JbpmStatable getJbpmEntitie() {
		return jbpmEntitie;
	}

	public void setJbpmEntitie(JbpmStatable jbpmEntitie) {
		this.jbpmEntitie = jbpmEntitie;
	}

	public boolean isMailActive() {
		return mailActive;
	}

	public void setMailActive(boolean mailActive) {
		this.mailActive = mailActive;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public boolean isWkfAccept() {
		return wkfAccept;
	}

	public void setWkfAccept(boolean wkfAccept) {
		this.wkfAccept = wkfAccept;
	}

	public List<JbpmStatable> getJbpmEntities() {
		return jbpmEntities;
	}

	public void setJbpmEntities(List<JbpmStatable> jbpmEntities) {
		this.jbpmEntities = jbpmEntities;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	public void setHrDataModel(JbpmDataModel hrDataModel) {
		this.hrDataModel = hrDataModel;
	}

	public AbsenceImpl getAbsence() {
		return absence;
	}

	public void setAbsence(AbsenceImpl absence) {
		this.absence = absence;
	}

	public void setDecisionHandler(DecisionHandler decisionHandler) {
		this.decisionHandler = decisionHandler;
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}
	
	public void setTypeAbsenceService(
			TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}
	

	public DataTable getCongesTable() {
		return congesTable;
	}

	public void setCongesTable(DataTable congesTable) {
		this.congesTable = congesTable;
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		this.typeCongeService = typeCongeService;
	}

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public CongeImpl getConge() {
		return conge;
	}

	public void setConge(CongeImpl conge) {
		this.conge = conge;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public TreeNode getSelectedNoeudNode() {
		return selectedNoeudNode;
	}

	public void setSelectedNoeudNode(TreeNode selectedNoeudNode) {
		this.selectedNoeudNode = selectedNoeudNode;
	}

	public TreeNode getSelectedUnitNode() {
		return selectedUnitNode;
	}

	public void setSelectedUnitNode(TreeNode selectedUnitNode) {
		this.selectedUnitNode = selectedUnitNode;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean admin) {
		isAdmin = admin;
	}
}
