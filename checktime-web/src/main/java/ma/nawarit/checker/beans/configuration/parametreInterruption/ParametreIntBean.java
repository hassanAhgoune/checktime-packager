package ma.nawarit.checker.beans.configuration.parametreInterruption;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ma.nawarit.checker.configuration.TypeInter;
import ma.nawarit.checker.configuration.crud.TypeInterManageableService;
import ma.nawarit.checker.configuration.crud.TypeInterManageableServiceBase;

import org.primefaces.event.RowEditEvent;

@ManagedBean(name = "parametreIntBean")
@SessionScoped
public class ParametreIntBean {
	
	@ManagedProperty(value = "#{TypeInterManageableService}")
	private TypeInterManageableService typeInterServise = new TypeInterManageableServiceBase();
		
	private List<TypeInter> typesInter = new ArrayList<TypeInter>();
	private TypeInter typeInter;
	FacesContext context;
	
	public ParametreIntBean() {

		context = FacesContext.getCurrentInstance();
	}

	public void saveTypeInter() throws Exception {
		if (typeInter != null && typeInter.getLibelle() != null) {

			typeInterServise.create(typeInter);
		}
		typesInter.add(typeInter);
	}	
	
	public void cleanTypeInter() {
		typeInter = new ma.nawarit.checker.configuration.TypeInterImpl();
	}	
	
	public void onRowEditType(RowEditEvent event) throws Exception {
		FacesMessage msg = new FacesMessage("Enregistrement de ",
				((TypeInter) event.getObject()).getLibelle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
		
		for (TypeInter type : typesInter) {			
			typeInterServise.update(type);
		}
	}	
	
	public void onRowCancelType(RowEditEvent event) throws Exception {
		FacesMessage msg = new FacesMessage("Edit Cancelled",
				((TypeInter) event.getObject()).getLibelle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
		for (TypeInter type : typesInter) {			
			typeInterServise.update(type);
		}
	}

	public TypeInterManageableService getTypeInterServise() {
		return typeInterServise;
	}

	public void setTypeInterServise(TypeInterManageableService typeInterServise) {
		this.typeInterServise = typeInterServise;
	}

	public List<TypeInter> getTypesInter() {
		try {
			if (typesInter == null || typesInter.isEmpty()) {
				typesInter = typeInterServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesInter;
	}

	public void setTypesInter(List<TypeInter> typesInter) {
		this.typesInter = typesInter;
	}

	public TypeInter getTypeInter() {
		return typeInter;
	}

	public void setTypeInter(TypeInter typeInter) {
		this.typeInter = typeInter;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}
}