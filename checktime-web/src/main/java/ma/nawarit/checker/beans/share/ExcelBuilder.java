package ma.nawarit.checker.beans.share;

import ma.nawarit.checker.engine.UserReportData;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ExcelBuilder extends AbstractExcelView {



    protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook wb, HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse) throws Exception {

        String grandTitle = (String) model.get("grandTitle");
        List<String> titles = (List<String>) model.get("titles");
        List<UserReportData> data = (List<UserReportData>) model.get("dataSource");
        XlsModel.generateXLSCumulReport(data, grandTitle, titles, wb);
    }

        // Write the output
}
