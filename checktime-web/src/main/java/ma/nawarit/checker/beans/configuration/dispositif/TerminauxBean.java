/*
Nom du projet : Checktime
Version : 1.0
Author : M.Morabit
Date : Mai 2014
*/
package ma.nawarit.checker.beans.configuration.dispositif;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.tempuri.SyncTerminalSoap;
import org.tempuri.SyncTerminalSoapProxy;

import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.TerminalImpl;
import ma.nawarit.checker.equipement.TerminalParam;
import ma.nawarit.checker.equipement.TerminalType;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.crud.TerminalManageableService;
import ma.nawarit.checker.equipement.crud.TerminalTypeManageableService;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

@ManagedBean(name = "terminauxBean")
@ViewScoped
public class TerminauxBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{TerminalManageableService}")
	private TerminalManageableService terminalService;
	@ManagedProperty(value = "#{TerminalTypeManageableService}")
	private TerminalTypeManageableService terminalTypeService;
	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;

	private Terminal terminal = new TerminalImpl();;
	private List<Terminal> listeTerminals;
	private String unit;
	private List<Unit> units;
	private String fichier;
	private TerminalParam parametre;
	
	public SyncTerminalSoap terminalWebService = new SyncTerminalSoapProxy();

	public TerminalParam getParametre() {
		return parametre;
	}

	public void setParametre(TerminalParam parametre) {
		this.parametre = parametre;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

	public List<Unit> getUnits() {
		try {
			List<Unit> u = unitService.readAll();
			return u;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<TerminalType> getTypes() {
		try {
			return terminalTypeService.readAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public UnitManageableService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public TerminalManageableService getTerminalService() {
		return terminalService;
	}

	public void setTerminalService(TerminalManageableService terminalService) {
		this.terminalService = terminalService;
	}

	public void setTerminalTypeService(TerminalTypeManageableService terminalTypeService) {
		this.terminalTypeService = terminalTypeService;
	}

	public Terminal getTerminal() {
		return terminal;
	}

	public void setTerminal(Terminal terminal) {
		this.terminal = terminal;
	}

	public String getFichier() {
		return fichier;
	}

	public void setFichier(String fichier) {
		this.fichier = fichier;
	}

	public List<Terminal> getListeTerminals() {
		try {
			return terminalService.readAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void setListeTerminals(List<Terminal> listeTerminals) {
		this.listeTerminals = listeTerminals;
	}

	public void saveTerminalListener() {
		try {
			// terminal.setPointageLocation(fichier);
			terminal.setDatePattern("yyyy/MM/dd");
			terminal.setHourPattern("HH:mm:ss");
			terminal.setCategorie("pntg");
			terminal.setType(terminalTypeService.load(terminal.getType().getId()));
			terminalService.create(terminal);

			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
			terminalWebService.add(Integer.valueOf(terminal.getCode()), terminal.getLibelle(), terminal.getLibelle(),
					unitService.load(terminal.getUnit().getId()).getCode());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void editTerminal() {
		try {
			terminal.setUnit(unitService.load(Integer.valueOf(unit)));
			terminalService.update(terminal);
			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
				terminalWebService.update(Integer.valueOf(terminal.getCode()), terminal.getLibelle(), terminal.getLibelle(),
						unitService.load(terminal.getUnit().getId()).getCode());
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteTerminal() {
		try {
			int code = Integer.valueOf(terminal.getCode());
			terminalService.delete(terminal);

			PropertiesReader.loadProperties("syncRH.properties");
			if(PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE"))
				terminalWebService.delete(code);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void activeTerminalAction(Terminal ter) {
		if (ter == null)
			return;
		if (ter.isEnabled())
			ter.setEnabled(false);
		else
			ter.setEnabled(true);
		try {
			terminalService.update(ter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void copyFile(String fileName, InputStream in) {
		try {

			fichier = "C:\\Users\\moi\\workspace\\checktime-web\\upload\\conf_terminal_"
					+ new SimpleDateFormat("yyyyMMddHHmmSS").format(new java.util.Date()) + ".txt";
			OutputStream out = new FileOutputStream(new File(fichier));

			int read = 0;
			byte[] bytes = new byte[1024];

			while ((read = in.read(bytes)) > 0) {
				out.write(bytes, 0, read);
			}

			in.close();
			out.flush();
			out.close();

			System.out.println("Fichier : " + fichier + " enregistré!");
			FacesMessage msg = new FacesMessage("Fichier : ", fichier + " est enregistré.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String format(java.util.Date d) {
		return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(d);
	}

	public void loadDelete() {
		try {
			this.terminal = terminalService.load(Integer.valueOf(
					FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id")));

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public void loadUpdate() {
		try {

			this.unit = this.terminal.getUnit().getId() + "";
			this.terminal = terminalService.load(Integer.valueOf(
					FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id")));
			if (this.terminal.getTerminalParams().isEmpty()) {
				TerminalParam p[] = new TerminalParam[4];
				String libelles[] = { "Nom", "Adresse", "Date", "Heure" };
				for (int i = 0; i < p.length; i++) {
					p[i] = TerminalParam.Factory.newInstance();
					p[i].setName(libelles[i]);
					p[i].setDescription("");
					p[i].setPosition("");
					p[i].setLength("");
					this.terminal.getTerminalParams().add(p[i]);
				}
			}

		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void loadAdd() {
		this.terminal.getTerminalParams().clear();
		TerminalParam p[] = new TerminalParam[4];
		String libelles[] = { "Nom", "Adresse", "Date", "Heure" };
		for (int i = 0; i < p.length; i++) {
			p[i] = TerminalParam.Factory.newInstance();
			p[i].setName(libelles[i]);
			p[i].setDescription("");
			p[i].setPosition("");
			p[i].setLength("");
			this.terminal.getTerminalParams().add(p[i]);

		}
	}

	public void addParameter() {
		TerminalParam p = TerminalParam.Factory.newInstance();
		p.setName("");
		p.setDescription("");
		p.setPosition("");
		p.setLength("");
		this.terminal.getTerminalParams().add(p);

	}

	public void deleteParam() {
		Integer id = Integer
				.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id"));
		// ArrayList<TerminalParam> l = (ArrayList<TerminalParam>)
		// this.terminal.getTerminalParams();
		Iterator<TerminalParam> it = this.terminal.getTerminalParams().iterator();
		while (it.hasNext()) {
			parametre = it.next();
			if (parametre.getId() == id) {
				it.remove();
				break;
			}
		}
		// for(TerminalParam p : l)
		// {
		// if(p.getId()==id)
		// this.terminal.getTerminalParams().remove(p);
		// }
	}

}
