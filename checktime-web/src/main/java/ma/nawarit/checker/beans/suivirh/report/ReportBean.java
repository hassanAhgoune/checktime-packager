package ma.nawarit.checker.beans.suivirh.report;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import com.google.gson.Gson;

import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.PieChartModel;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.dom4j.util.UserDataAttribute;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.schedule.Schedule;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.PageEvent;

import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.compagnie.Group;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.GroupManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.engine.LiveProcessTask;
import ma.nawarit.checker.engine.LiveProcessingService;
import ma.nawarit.checker.engine.UserReportData;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.suivi.Absence;

@ManagedBean(name = "reportBean")
@ViewScoped
public class ReportBean {
	
	private static final Logger logger = Logger.getLogger(ReportBean.class);
	
	@ManagedProperty(value = "#{LiveProcessingService}")
	private LiveProcessingService processorService;

	@ManagedProperty(value = "#{GroupManageableService}")
	private GroupManageableService groupService;
		
	private CommonCriteria criteria = new CommonCriteria();
	
	private List<UserReportData> userDatas;
	
	private DayData total = new DayData();
	
	private int weekCount = 1;
	
	private UserReportData userData;
	
	private List<Date> columns = new ArrayList<Date>();
	
	private Schedule schedule;
	
	private FacesContext context = FacesContext.getCurrentInstance();
	
	private User usrAuthenticated;
	
	private boolean isSupH = false;
	
	private PieChartModel pieModel = new PieChartModel();
	
	
	private TreeNode selectedNode;
	
	public ReportBean() {
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		usrAuthenticated = (User)obj;
		isSupH=context.getExternalContext().isUserInRole("ROLE_SUPH");
		this.criteria.setTypeTreatment("RPT");
	}
	
	public void processAction() {
		criteria.setStatut(null);
		if (usrAuthenticated != null && usrAuthenticated.getId() > 1 && isSupH)
			criteria.setSuppHrhId(usrAuthenticated.getId() + "");
		if (this.selectedNode != null) {
			criteria.setNoeudId(null);
			criteria.setNoeuds(null);
			criteria.setNoeuds(Utils.getValueChildren((Noeud)this.selectedNode.getData()));
		}

		userDatas = processorService.process(criteria, null);
		Calendar c = new GregorianCalendar();
		c.setTime(criteria.getDateDebut());
		columns = new ArrayList<Date>();
		if (userDatas != null && (!userDatas.isEmpty())
				&& (userDatas.get(0).getDayDatas() != null && (!userDatas.get(0).getDayDatas().isEmpty())))
			while(criteria.getDateFin().compareTo(c.getTime()) >= 0) {
				columns.add(c.getTime());
				c.add(Calendar.DAY_OF_MONTH, 1);
			}
		loadReportingParams();
		criteria.getIndexs().clear();
	}
	
	public void onSelfTabChangeListener(TabChangeEvent event) {
		System.out.println(event.getTab().getId());
		if (schedule != null && event.getTab().getId().equals("AttSheetId")) {
			System.out.println(schedule.getValue().getEvents().get(0).getStartDate());
			criteria = new CommonCriteria();
//			criteria.setDateDebut(schedule.getModel().getSelectedDate());
		}
	}
	
	public void loadReportingParams(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("DATE_DEBUT",
				criteria.getDateDebut());
		context.getExternalContext().getSessionMap().put("DATE_FIN",
				criteria.getDateFin());
		context.getExternalContext().getSessionMap().put("DATA_RESULT",
				this.userDatas);
		Integer[] tauxList = {125,150,200};
		context.getExternalContext().getSessionMap().put("TAUX_LIST",
				tauxList);
	}
	
	
	public void onRowToggleListener(UserReportData userData) {
		this.userData = userData;
		createPieModel(this.userData);
		total = new DayData();
		total.setP_tmcontract(0.0);
		total.setCol1(0.0);
		if (userData == null || userData.getDayDatas() == null || userData.getDayDatas().isEmpty())
			return;
		int d = (weekCount - 1) * 7;
		int f = weekCount * 7 - 1;
		for (int i = d; (i <= f && i <= userData.getDayDatas().size() - 1); i++) 
			addweeksTotal(userData.getDayDatas().get(i));
	}
	
	public void addweeksTotal(DayData d) {
		try {					
				total.setCol1(Utils.additionHeures(total.getCol1(), d.getCol1()));
				total.setCol2(Utils.additionHeures(total.getCol2(), d.getCol2()));
				total.setCol2cop(Utils.additionHeures(total.getCol2cop(), d.getCol2cop()));
				total.setCol2ht(Utils.additionHeures(total.getCol2ht(), d.getCol2ht()));
				total.setCol2bis(total.getCol2bis()+ d.getCol2bis());
				total.getCol2prm()[0] += d.getCol2prm()[0];
				total.getCol2prm()[1] += d.getCol2prm()[1];
				total.getCol2prm()[2] += d.getCol2prm()[2];
				total.setCol3(Utils.additionHeures(total.getCol3(), d.getCol3()));
				total.setCol4(Utils.additionHeures(total.getCol4(), d.getCol4()));
				total.setCol5(Utils.additionHeures(total.getCol5(), d.getCol5()));
				total.setCol6(Utils.additionHeures(total.getCol6(), d.getCol6()));
				total.setTcol7(total.getTcol7()+ d.getTcol7());
				total.setTcol8(Utils.additionHeures(total.getTcol8(), d.getTcol8()));
				total.setTcol9(Utils.additionHeures(total.getTcol9(), d.getTcol9()));
				total.setCol10(Utils.additionHeures(total.getCol10(), d.getCol10()));
				total.setCol11(Utils.additionHeures(total.getCol11(), d.getCol11()));
				total.setCol12(Utils.additionHeures(total.getCol12(), d.getCol12()));
				total.setCol13(Utils.additionHeures(total.getCol13(), d.getCol13()));
				total.setCol14(Utils.additionHeures(total.getCol14(), d.getCol14()));	
				total.setCol15(Utils.additionHeures(total.getCol15(), d.getCol15()));	
				total.setCol16(Utils.additionHeures(total.getCol16(), d.getCol16()));
				total.setCol17(Utils.additionHeures(total.getCol17(), d.getCol17()));
				total.setCol18(Utils.additionHeures(total.getCol18(), d.getCol18()));
				total.setCol19(Utils.additionHeures(total.getCol19(), d.getCol19()));
				total.setCol20(Utils.additionHeures(total.getCol20(), d.getCol20()));
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void groupValueChangeListener(ValueChangeEvent e) throws Exception {
    	Group group = this.groupService.load(Integer.parseInt((String)e.getNewValue()));
    	criteria.setIndexs(new ArrayList<Integer>());
    	for(User u: group.getUsers())
    		criteria.getIndexs().add(u.getId());
    	this.criteria.setChanged(true);
    	
    }
	
	public void processWeekListener(PageEvent e) {
		weekCount = e.getPage() + 1;
		if (userData == null || userData.getDayDatas() == null || userData.getDayDatas().isEmpty())
			return;
		total = new DayData();
		int d = (weekCount - 1) * 7;
		int f = weekCount * 7 - 1;
		for (int i = d; (i <= f && i <= userData.getDayDatas().size() - 1); i++) {
			addweeksTotal(userData.getDayDatas().get(i));
		}
	}

	public void processWeekListener(ActionEvent e) {
		weekCount++;
	}
	public String getPieChartData(UserReportData user) {
		List<String> labels=new ArrayList<String>();
		List<Double> values=new ArrayList<Double>();
		if(user.getTcol2ht()>=1) {
			labels.add("Heures travaill\u00E9es");
			values.add(user.getTcol2ht());
		}
		if(user.getTcol3()>=1) {
			labels.add("Retard tol\u00E9r\u00E9");
			values.add(user.getTcol3());
		}
		if(user.getTcol4()>=1) {
			labels.add("Retard non tol\u00E9r\u00E9");
			values.add(user.getTcol4());
		}
//		if((user.getTcol8()+user.getTcol9())>=1) {
//			labels.add("Absence");
//			values.add((user.getTcol8()+user.getTcol9()));
//		}
		
		if(user.getTcol8()>=1) {
			labels.add("Absence justifi\u00E9");
			values.add(user.getTcol8());
		}
		if(user.getTcol9()>=1) {
			labels.add("Absence non justifi\u00E9");
			values.add(user.getTcol9());
		}
		if(user.getTcol7()>=1) {
			labels.add("Cong\u00E9");
			values.add(user.getTcol7());
		}
		if(user.getTcol5()>=1) {
			labels.add("Interruption");
			values.add(user.getTcol5());
		}
		String json = new Gson().toJson(new ChartDataModel(labels,values));
		return json;
	}
	
	public PieChartModel getPieModel() {
		return pieModel;
	}
	
	public void createPieModel(UserReportData userdata) {
		pieModel = new PieChartModel();
		Map<String, Number> data = new Hashtable<String, Number>();
		if(userdata.getTcol2ht()>=1) {
			data.put("Heures travaill\u00E9es", Utils.getHeurePrCent(userdata.getTcol2ht(), userData.getTcol1()));
		}
		if(userdata.getTcol3()>=1) {
			data.put("Retard tol\u00E9r\u00E9", Utils.getHeurePrCent(userdata.getTcol3(), userData.getTcol1()));
		}
		if(userdata.getTcol4()>=1) {
			data.put("Retard non tol\u00E9r\u00E9", Utils.getHeurePrCent(userdata.getTcol4(), userData.getTcol1()));
		}
//		if((user.getTcol8()+user.getTcol9())>=1) {
//			labels.add("Absence");
//			values.add((user.getTcol8()+user.getTcol9()));
//		}
		
		if(userdata.getTcol8()>=1) {
			data.put("Absence justifi\u00E9", Utils.getHeurePrCent(userdata.getTcol8(), userData.getTcol1()));
		}
		if(userdata.getTcol9()>=1) {
			data.put("Absence non justifi\u00E9", Utils.getHeurePrCent(userdata.getTcol9(), userData.getTcol1()));
		}
		if(userdata.getTcol7()>=1) {
			data.put("Cong\u00E9", Utils.getHeurePrCent(userdata.getTcol7(), userData.getTcol1()));
		}
		if(userdata.getTcol5()>=1)
			data.put("Interruption", Utils.getHeurePrCent(userdata.getTcol5(), userData.getTcol1()));
		pieModel.setData(data);
	}
	
	public String loadAbsencejustif(Collection<Absence> absences) {
		String res= Utils.getAbsenceCodes(absences);
		return res;
	}
	
	
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public void setProcessorService(LiveProcessingService processorService) {
		this.processorService = processorService;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public void setGroupService(GroupManageableService groupService) {
		this.groupService = groupService;
	}

	public List<UserReportData> getUserDatas() {
		return userDatas;
	}

	public int getWeekCount() {
		return weekCount;
	}

	public DayData getTotal() {
		return total;
	}

	public void setTotal(DayData total) {
		this.total = total;
	}

	public void setWeekCount(int weekCount) {
		this.weekCount = weekCount;
	}

	public List<Date> getColumns() {
		return columns;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}
	
}

 class ChartDataModel{
		public List<String> labels;
		List<Double> series;

	 public ChartDataModel(List<String> labels, List<Double> values) {
		 this.labels = labels;
		 this.series = values;
	 }
 }