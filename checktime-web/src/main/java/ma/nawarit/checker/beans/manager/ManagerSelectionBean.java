package ma.nawarit.checker.beans.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ma.nawarit.checker.beans.compagnie.collabo.UserDataModel;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;

import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.TreeNode;

@ManagedBean(name = "managerSelectionBean")
@ViewScoped
public class ManagerSelectionBean {

	private TreeNode selectedNode;
	private DataTable usersTable = new DataTable();
	
	public void handleNodeSelection() {
		if (selectedNode != null && selectedNode.getData() != null) {
			Noeud n = (Noeud)selectedNode.getData();
			Map<String, Object> filters = new HashMap<String, Object>();
			String value = getValueChildren(n);
			filters.put("noeud.libelle", value);
			usersTable.setFilters(filters);
			
		}
	}
	
	public String getValueChildren(Noeud n) {
		String value = n.getLibelle();
		if (n.getNoeuds() != null && !n.getNoeuds().isEmpty()) 
			for(Noeud nd: n.getNoeuds()) 
				value += "/" + getValueChildren(nd);
		return value;
	}
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}
	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}
	public DataTable getUsersTable() {
		Object o = usersTable.getValue();
		if (o instanceof UserDataModel) {
			UserDataModel dataModel = (UserDataModel)o;
			if (dataModel.getData().isEmpty())
				usersTable.loadLazyData();
		}
		return usersTable;
	}
	public void setUsersTable(DataTable usersTable) {
		this.usersTable = usersTable;
	}
	
	
}
