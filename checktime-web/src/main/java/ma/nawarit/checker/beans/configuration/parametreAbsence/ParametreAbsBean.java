package ma.nawarit.checker.beans.configuration.parametreAbsence;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;

import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.TypeAbsenceImpl;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableServiceBase;

@ManagedBean(name = "parametreAbsBean")
@SessionScoped
public class ParametreAbsBean {

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsServise = new TypeAbsenceManageableServiceBase();

	private List<TypeAbsence> typesAbsence = new ArrayList<TypeAbsence>();
	private TypeAbsence typeAbsence = new TypeAbsenceImpl();
	private TypeAbsence selectedAbsence = new TypeAbsenceImpl();
	private String deduire = "Non";
	FacesContext context;

	private String code;

	public ParametreAbsBean() {

		context = FacesContext.getCurrentInstance();
		typeAbsence = new TypeAbsenceImpl();
		selectedAbsence = new TypeAbsenceImpl();
	}

	public void saveTypeAbsence() throws Exception {

		if (typeAbsence != null && typeAbsence.getLibelle() != null) {

			typeAbsServise.create(typeAbsence);
		}
		typesAbsence.add(typeAbsence);
		typeAbsence = new TypeAbsenceImpl();
	}

	public void selectedTypeAbsence(RowEditEvent event) throws Exception {

		selectedAbsence = ((TypeAbsence) event.getObject());
	}

	public void deleteTypeAbsence() throws Exception {

		if (selectedAbsence != null) {
			typeAbsServise.delete(selectedAbsence);
			typesAbsence.remove(selectedAbsence);
		}
	}

	public void cleanTypeAbsence(RowEditEvent event) {
		typeAbsence = ((TypeAbsence) event.getObject());
		typeAbsence = new TypeAbsenceImpl();
	}

	public void updateTypeAbsence(RowEditEvent event) throws Exception {

		typeAbsence = ((TypeAbsence) event.getObject());
		typeAbsServise.update(typeAbsence);
	}

	public TypeAbsenceManageableService getTypeAbsServise() {
		return typeAbsServise;
	}

	public void setTypeAbsServise(TypeAbsenceManageableService typeAbsServise) {
		this.typeAbsServise = typeAbsServise;
	}

	public List<TypeAbsence> getTypesAbsence() {
		try {
			if (typesAbsence == null || typesAbsence.isEmpty()) {
				typesAbsence = typeAbsServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesAbsence;
	}

	public void setTypesAbsence(List<TypeAbsence> typesAbsence) {
		this.typesAbsence = typesAbsence;
	}

	public TypeAbsence getTypeAbsence() {
		return typeAbsence;
	}

	public void setTypeAbsence(TypeAbsence typeAbsence) {
		this.typeAbsence = typeAbsence;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public TypeAbsence getSelectedAbsence() {
		return selectedAbsence;
	}

	public void setSelectedAbsence(TypeAbsence selectedAbsence) {
		this.selectedAbsence = selectedAbsence;
	}

	public String getDeduire() {
		if (selectedAbsence != null) {
			if (selectedAbsence.isDeduir()) {
				deduire = "Oui";
			} else
				deduire = "Non";
		}
		return deduire;
	}

	public void setDeduire(String deduire) {
		this.deduire = deduire;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
