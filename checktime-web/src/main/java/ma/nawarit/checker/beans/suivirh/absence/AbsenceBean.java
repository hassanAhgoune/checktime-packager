package ma.nawarit.checker.beans.suivirh.absence;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.http.HttpServletRequest;

import ma.nawarit.checker.beans.compagnie.collabo.UserDataModel;
import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableServiceBase;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningCycl;
import ma.nawarit.checker.configuration.PlanningImpl;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableServiceBase;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.configuration.crud.PlanningManageableServiceBase;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.jbpm.DecisionHandler;
import ma.nawarit.checker.jbpm.JbpmManageable;
import ma.nawarit.checker.jbpm.JbpmManageableImpl;
import ma.nawarit.checker.jbpm.JbpmStatable;
import ma.nawarit.checker.mail.MailDto;
import ma.nawarit.checker.mail.MailManager;
import ma.nawarit.checker.mail.MailManagerImpl;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.TypeAbsenceImpl;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableServiceBase;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableServiceBase;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableServiceBase;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableServiceBase;
import ma.nawarit.checker.utils.Globals;

import org.andromda.spring.CommonCriteria;
import org.jbpm.JbpmContext;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.Token;
import org.jbpm.util.ClassLoaderUtil;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

@ManagedBean(name = "absenceBean")
@ViewScoped
public class AbsenceBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7437021414337002754L;

	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService = new AbsenceManageableServiceBase();

	@ManagedProperty(value = "#{RetardManageableService}")
	private RetardManageableService retardService = new RetardManageableServiceBase();

	@ManagedProperty(value = "#{JrFerieManageableService}")
	private JrFerieManageableService jrFerieService = new JrFerieManageableServiceBase();

	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService = new CongeManageableServiceBase();

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsenceService = new TypeAbsenceManageableServiceBase();

	@ManagedProperty(value = "#{ProfilMetierManageableService}")
	private ProfilMetierManageableService fonctionService;

	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService = new UserManageableServiceBase();

	@ManagedProperty(value = "#{PlanningManageableService}")
	private PlanningManageableService planningService = new PlanningManageableServiceBase();
	
	@ManagedProperty(value = "#{jbpmManageable}")
	private JbpmManageable jbpmService = new JbpmManageableImpl();
	
	@ManagedProperty(value = "#{decisionHandler}")
	private DecisionHandler decisionHandler;
	
	@ManagedProperty(value = "#{mailManager}")
	private MailManager mailService = new MailManagerImpl();

	@ManagedProperty(value = "#{AdvParamManageableService}")
	private AdvParamManageableService advParamService;

	private List<Absence> absences = new ArrayList<Absence>();
	private List<TypeAbsence> typesAbsence = new ArrayList<TypeAbsence>();
	private List<Absence> filtredAbsences = new ArrayList<Absence>();
	private Absence absence;
	private TypeAbsence typeAbsence;
	private TypeAbsence selectedTypeAbsence = new TypeAbsenceImpl();
	private Absence selectedAbsence;
	private boolean workflowActive;
	private boolean mailActive;
	private CommonCriteria criteria;
	private StreamedContent _photo;
	private UploadedFile photo;
	private File uploadedFile;
	private ProfilMetier fonction;
	private List<ProfilMetier> fonctions;
	private List<User> users;
	private List<User> filteredUsers;
	private User user = new UserImpl();
	private User authUser;
	private boolean isSupH;
	private boolean isRP;
	private User selectedUser = new UserImpl();
	private String nom;
	private String unitTemps = "hr";
	private double total;
	private FacesContext context;

	// demande absence
	private List<Absence> userAbsences;
	private String workflowUrl;
//	private Absence absenceDemande;

	public AbsenceBean() throws Exception {
		criteria = new CommonCriteria();
		criteria.setProcessActive(true);
		criteria.setChanged(true);
		users = new ArrayList<User>();
		context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		authUser = (User)obj;
		isSupH=context.getExternalContext().isUserInRole("ROLE_SUPH");
		isRP=context.getExternalContext().isUserInRole("ROLE_RP");
		userAbsences= new ArrayList<Absence>();
		absence=new AbsenceImpl();

		HttpServletRequest origRequest = (HttpServletRequest)context.getExternalContext().getRequest();
		workflowUrl = "http://"+origRequest.getServerName()+":"+origRequest.getServerPort()+"/checktime-web";
//		absenceDemande=new AbsenceImpl();
	}

	public void saveAbsence() throws Exception {
		if (isRP) {
			Hashtable props = new Hashtable();
			props.put("code", "MIS");
			List l = typeAbsenceService.read(props);
			if (l != null && !l.isEmpty())
				selectedTypeAbsence = (TypeAbsence)l.get(0); 
		} else if (typeAbsence != null) 
			selectedTypeAbsence = typeAbsenceService.load(typeAbsence.getId());
		else
			return;
		absence.setTypeAbsence(selectedTypeAbsence);
		
		if(this.user.getId()>0) {
			user = userService.load(user.getId());
			absence.setCollaborateur(user);
		}else {
			absence.setCollaborateur(authUser);
		}

		workflowActive = advParamService.getValueByName("workflow_active").equalsIgnoreCase("yes")? true : false;
		absence.setDuree(compterHeure(absence));
		
		// voir s'il existe une absence identique
		CommonCriteria crit = new CommonCriteria();
		crit.setProfilId(4+"");
		crit = new CommonCriteria();
		crit.setDateDebut(absence.getDateDebut());
		crit.setDateFin(absence.getDateReprise());
		crit.setMatriculeInf(user.getMatricule());
		crit.setMatriculeSup(user.getMatricule());
		List<Absence> abs = absenceService.readByCriteria(crit);
		if (abs != null && !abs.isEmpty())
			for (Absence a : abs) {
				if (a.getStatut().equals(Globals.WORKFLOW_STATUS_ENCOURS)
						|| a.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE))
					return;
			}
		
		if (workflowActive) {
			int Idprocess = decisionHandler.deployProcessDefinition(null, "process-workflow", false);
			absence.setIdProcess(Idprocess);
			absence.setStatut(Globals.WORKFLOW_STATUS_ENCOURS);
		}else {			
			absence.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			absence.setIdProcess(0);
		}
		absence.setHorNuit(false);
		absenceService.create(absence);
		managerTakeDecision((AbsenceImpl)absence);
		absence.getCollaborateur().getMatricule();
		absences.add(absence);
		
		// si l'absence effectuer est deja enregistrer on lui affecte le motif
		crit = new CommonCriteria();
		crit.setDateDebut(absence.getDateDebut());
		crit.setDateFin(absence.getDateReprise());
		crit.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
		crit.setMatriculeInf(user.getMatricule());
		crit.setMatriculeSup(user.getMatricule());
		abs = absenceService.readByCriteria(crit);
		if (abs != null && !abs.isEmpty())
			for (Absence a : abs) {
				a.setTypeAbsence(selectedTypeAbsence);
				absenceService.update(a);
			}
	}
	
	public void managerTakeDecision(AbsenceImpl abs){
		try {
			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			if(decisionHandler.isManagerDecisionReached((JbpmStatable) abs)){
				// To get from screen form
				String comment = "Manager : Ok let's go !";
				
				// Merge mail Template
				User u = abs.getCollaborateur();
				HashMap<String, String> hash = new HashMap<String, String>();
//				hash.put("sender", (u.getSupH().getMail() == null)? "":u.getSupH().getMail());
//				if(workflowActive){
//					User sup = u.getSupH();
//					hash.put("receiver", (sup.getSupH().getMail() == null)? "":sup.getSupH().getMail());
//					hash.put("rhManager", sup.getSupH().getNom());
//				}
//				else
//					hash.put("receiver", (u.getMail() == null)? "":u.getMail());
				hash.put("subject", "Approbation déclaration Absence - (" + u.getNom() + " " + u.getPrenom()+")");
				
//				hash.put("manager", u.getSupH().getNom());
//				hash.put("managerMail", u.getSupH().getMail());
				hash.put("managerComment", comment);
				hash.put("userName", u.getNom() + " " + u.getPrenom());
				hash.put("userMail", u.getMail());
				hash.put("profil", "profil");
				hash.put("workflowName", "Absence");
				hash.put("workflowUrl", workflowUrl+"/pages/jbpm/jbpm.wit");
				hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

				// Take a decision
				decisionHandler.managerTakeDecision(abs.getIdProcess(), workflowActive, comment, hash, mailActive);
				if(!workflowActive){
					abs.setStatut("workflow_status_rejected");
					absenceService.update(abs);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}					
	}

	public void updateAbsence() throws Exception {

		if (!typeAbsence.equals(absence.getTypeAbsence())) {
			selectedTypeAbsence = typeAbsenceService.load(typeAbsence.getId());
			absence.setTypeAbsence(selectedTypeAbsence);
		}
		absenceService.update(absence);

		int i = absences.indexOf(absence);
		absences.remove(absence);
		absences.add(i, absence);
		if(absence.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE)) {
			validateRetards();
		}
	}

	public void updateAbsence(String statut) throws Exception {
		absence.setStatut(statut);
		absenceService.update(absence);
		int i = absences.indexOf(absence);
		absences.remove(absence);
		absences.add(i, absence);
		if(absence.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE)) {
			validateRetards();
		}
	}
	
	//updates retards if absence valide
	public void validateRetards() throws Exception {
		//update retard
		List<Retard> retards=new ArrayList<Retard>();
		CommonCriteria crit=new CommonCriteria();
		crit.setDateDebut(absence.getDateDebut());
		crit.setDateFin(absence.getDateReprise());
		crit.setMatriculeInf(user.getMatricule());
		crit.setMatriculeSup(user.getMatricule());
		crit.setRetardTolEnabled(true);
		retards=retardService.readByCriteria(crit);
		
		if(retards!=null) {
			if(retards.size()>0) {
				for (Retard retard : retards) {
					retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					retardService.update(retard);
				}
			}
		}
	}
	
	public void deleteAbsence() throws Exception {
		absenceService.delete(absence);
		absences.remove(absence);
	}

	/**
	 * 
	 * @throws Exception
	 */

//	@SuppressWarnings("unchecked")
//	public void onDemandeAbsence() throws Exception {
//		FacesContext fc = FacesContext.getCurrentInstance();
//		Object obj = fc.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
//		if (obj != null && (obj instanceof User)) {
//			User u = (User) obj;
//			user=u;
//			criteria.setMatriculeInf(u.getMatricule());
//			criteria.setMatriculeSup(u.getMatricule());
//			userAbsences = absenceService.readByCriteria(criteria);
//			absenceDemande=new AbsenceImpl();
//		}
//	}
//
//	public void saveDemandeAbsence() throws Exception {
//		if (absenceDemande.getTypeAbsence() == null) {
//			return;
//		}
//		absenceDemande.setCollaborateur(user);
//		absenceDemande.setStatut(Globals.WORKFLOW_STATUS_ENCOURS);
//		absenceDemande.setIdProcess(0);
//		absenceDemande.setHorNuit(false);
//		absenceService.create(absenceDemande);
//		System.out.println("Absence "+absenceDemande.getId());
//		absenceDemande=absenceService.load(absenceDemande.getId());
//		userAbsences.add(absenceDemande);
//		absenceDemande=new AbsenceImpl();
//	}
//	
//	//when date debut absence change
//	public void onDateDebutSelect(SelectEvent event) throws Exception {
//		absenceDemande.setDateDebut((Date)event.getObject());
//        calculeAbsenceDuration();
//    }
//	
//	//when date Fin absence change
//	public void onDateRepriseSelect(SelectEvent event) throws Exception {
//		absenceDemande.setDateReprise((Date)event.getObject());
//        calculeAbsenceDuration();
//    }
//	
//	public void calculeAbsenceDuration() throws Exception {
//		if(absenceDemande.getDateDebut()!=null && absenceDemande.getDateReprise()!=null) {
//			absenceDemande.setDuree(compterHeure(absenceDemande));
//		}else {
//			if(absenceDemande.getDateDebut()!=null) {
//				System.out.println("Date debut is Null");
//			}
//			if(absenceDemande.getDateReprise()!=null) {
//				System.out.println("Date Fin is Null");
//			}
//		}
//	}
//	//set type Absence
//	public void typeAbsenceValueChangeListener(ValueChangeEvent e) throws Exception {
//		String id=((String)e.getNewValue());
//		absenceDemande.setTypeAbsence(typeAbsenceService.load(Integer.valueOf(id)));
//	}

	public void cleanAbsence(ActionEvent e) {
		this.absence = new AbsenceImpl();
	}

	public void refreshAbsListAction(String stat, User user) {
		if (stat.equals("dem"))
			this.criteria.setStatut("workflow_status_encours");
		else if (stat.equals("val"))
			this.criteria.setStatut("workflow_status_validated");
		else if (stat.equals("ref"))
			this.criteria.setStatut("workflow_status_rejected");
		else if (stat.equals("eff"))
			this.criteria.setStatut("workflow_status_elapsed");
		if (user != null) {
			this.criteria.setIndexs(new ArrayList<Integer>());
			this.criteria.getIndexs().add(user.getId());
		}
		this.criteria.setChanged(true);
	}

	private void resetSearchActionListener(ActionEvent e) {
		if (!this.criteria.getIndexs().isEmpty()) {
			this.criteria = new CommonCriteria();
			this.criteria.setChanged(true);
		}
	}

	public void chooseUser() {
		selectedUser = new UserImpl();
	}

	public String getRowClasses() {
		StringBuffer rowClasses = new StringBuffer();
		if (absences != null && !absences.isEmpty())
			for (Absence u : absences) {
				rowClasses.append(u.getDateReprise() != null ? "leftRowColor," : ",");
			}
		return rowClasses.toString();
	}

	public void onCountryChange() {

		if (fonction != null && !fonction.equals("")) {
			for (User u : users) {
				if (u.getProfilMetier() != fonction) {
					users.remove(u);
				}
			}
		}
	}

	public void onDemandeAbsence() {
		context = FacesContext.getCurrentInstance();
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		user = (User) obj;
		criteria.setNumBadge(user.getBadge());
	}
	public void onTabChange(TabChangeEvent event) {
		if (nom != null && nom != "") {
			selectedUser = readByName(nom);
		}
		FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onTabClose(TabCloseEvent event) {
		if (nom != null && nom != "") {
			selectedUser = readByName(nom);
		}
		FacesMessage msg = new FacesMessage("Tab Closed", "Closed tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowToggle(ToggleEvent event) {
		Collection<Absence> l = ((User) event.getData()).getAbsences();
	}

	public Collection<Absence> contentOf(User user) {
		return user.getAbsences();
	}

	public int sizeof(User user) {
		return user.getAbsences().size();
	}

	public void uploadFileListener(ActionEvent e) throws FileNotFoundException {
		_photo = new DefaultStreamedContent(new FileInputStream(uploadedFile), "image/jpeg");
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public List<Absence> getAbsences() {
		try {
			
			if (!criteria.isEmpty() && criteria.isChanged())
				absences = absenceService.readByCriteria(criteria);
			else if (criteria.isEmpty() && !criteria.isChanged() && (absences == null || absences.isEmpty()))
				absences = absenceService.readByCriteria(new CommonCriteria());
			criteria.setChanged(false);
			total = 0;
			if (absences != null && !absences.isEmpty())
				for (Absence a : absences) {
					if (a.getStatut().equals(Globals.WORKFLOW_STATUS_ELAPSED))
						total = Utils.additionHeures(a.getDuree(), total);
					a.setCollaborateur(Utils.deproxy(a.getCollaborateur(), User.class));
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return absences;
	}

	public void setAbsences(List<Absence> absences) {
		this.absences = absences;
	}

	public Absence getAbsence() {
		return absence;
	}

	public void setAbsence(Absence absence) {
		if (absence != null)
			this.typeAbsence = absence.getTypeAbsence();
		this.absence = absence;
	}

	public List<Absence> getFiltredAbsences() {
		return filtredAbsences;
	}

	public void setFiltredAbsences(List<Absence> filtredAbsences) {

		this.filtredAbsences = filtredAbsences;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public StreamedContent get_photo() {
		return _photo;
	}

	public void set_photo(StreamedContent _photo) {
		this._photo = _photo;
	}

	public UploadedFile getPhoto() {
		return photo;
	}

	public void setPhoto(UploadedFile photo) {
		this.photo = photo;
	}

	public File getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(File uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public ProfilMetierManageableService getFonctionService() {
		return fonctionService;
	}

	public void setFonctionService(ProfilMetierManageableService fonctionService) {
		this.fonctionService = fonctionService;
	}

	public ProfilMetier getFonction() {
		return fonction;
	}

	public void setFonction(ProfilMetier fonction) {
		this.fonction = fonction;
	}

	public List<ProfilMetier> getFonctions() throws Exception {
		if (this.fonctions == null || this.fonctions.isEmpty())
			this.fonctions = fonctionService.readAll();
		return fonctions;
	}

	public void setFonctions(List<ProfilMetier> fonctions) {
		this.fonctions = fonctions;
	}

	public List<User> getUsers() throws Exception {
		try {
			CommonCriteria userCriteria = new CommonCriteria();
			if(isSupH)
				userCriteria.setSuppHrhId(String.valueOf(authUser.getId()));
			
			if (users == null || users.isEmpty())
				users = userService.readByCriteria(userCriteria);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public User getUser() {

		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public List<User> getFilteredUsers() {
		return filteredUsers;
	}

	public void setFilteredUsers(List<User> filteredUsers) {
		this.filteredUsers = filteredUsers;
	}

	public void onUserSelected(SelectEvent event) throws Exception {
		this.user = (User) event.getObject();
		if (this.user == null)
			return;
		this.criteria = new CommonCriteria();
		this.criteria.getIndexs().add(this.user.getId());
		this.criteria.setChanged(true);
	}

	public void onRowEdit(RowEditEvent event) {
		absence = (Absence) event.getObject();
		try {
			if (absence != null) {
				absence.setDuree(compterHeure(absence));
				absenceService.update(absence);
				absence.setTypeAbsence(typeAbsenceService.load(absence.getTypeAbsence().getId()));
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void onRowCancel(RowEditEvent event) {
		absence = (Absence) event.getObject();
		try {
			if (absence != null)
				absenceService.delete(absence);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String onFlowProcess(FlowEvent event) {
		if (nom != null && nom != "") {
			selectedUser = readByName(nom);
		}
		return event.getNewStep();
	}

	public void onSelectUser() {
		if (nom != null && nom != "") {
			selectedUser = readByName(nom);
		}
	}

	public User readByName(String nom) {

		List<User> list = new ArrayList<User>();
		try {
			if (list == null || list.isEmpty()) {
				Hashtable<String, String> props = new Hashtable<String, String>();
				props.put("nom", nom);
				list = userService.read(props);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (list.isEmpty()) {
			return null;
		} else
			return list.get(0);
	}

	public TypeAbsence readByLibelle(String libelle) {

		List<TypeAbsence> list = new ArrayList<TypeAbsence>();
		try {
			if (list == null || list.isEmpty()) {
				Hashtable<String, String> props = new Hashtable<String, String>();
				props.put("libelle", libelle);
				list = typeAbsenceService.read(props);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list.get(0);
	}

	public List<TypeAbsence> getTypesAbsence() {
		try {
			if (typesAbsence == null || typesAbsence.isEmpty()) {
				typesAbsence = typeAbsenceService.readAll();
				typesAbsence.remove(0);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesAbsence;
	}

	public void typeValueChangeListener(ValueChangeEvent e) {
		criteria.setMotif((String) e.getNewValue());
		this.criteria.setChanged(true);
	}
	
	public void stutatValueChangeListener(ValueChangeEvent e) {
		criteria.setStatut((String) e.getNewValue());
		this.criteria.setChanged(true);
	}
	
	public void searchUsersAction() {
		this.criteria.setChanged(true);
	}

	public double compterHeure(Absence abs) {
		double cont = 0;
		try {
			Day d = null;
			Calendar currentDayDate = new GregorianCalendar();
			currentDayDate.setTime(abs.getDateDebut());
			Calendar end = new GregorianCalendar();
			end.setTime(abs.getDateReprise());
			cont = 0;
			Calendar start = new GregorianCalendar();
			start.setTime(abs.getDateDebut());
			while (currentDayDate.compareTo(end) <= 0) {
				cont +=0;
				d = new Day();
				d.setDate(currentDayDate.getTime());
				PlanningImpl planning = (PlanningImpl) user.getPlanningAffected() ;
				if (planning.getId()>0)
					planning = (PlanningImpl) planningService.load(planning.getId());
					d.setPlanning(planning);

				if (!isJrRepos(d,abs)) {
					cont = Utils.additionHeures(cont, d.getP_tmcontract());
					Calendar firstPlageDebut = d.getHrFixes().get(0).getDebut();
					Calendar lastPlageFin = d.getHrFixes().get(d.getHrFixes().size()-1).getFin();
					if(isSameDay(start,firstPlageDebut)) {
						if (start.get(Calendar.HOUR_OF_DAY) > 0 &&
								start.get(Calendar.HOUR_OF_DAY) >= firstPlageDebut.get(Calendar.HOUR_OF_DAY)) {
							//System.out.println("debut: " + start.getTime() + "\nday start: " + firstPlageDebut.getTime());
							double plagedebutStartDiff = Utils.differenceHeures(Utils.getHourDoubleFormat(start.getTime()), Utils.getHourDoubleFormat(firstPlageDebut.getTime()));
							cont = Utils.differenceHeures(cont,plagedebutStartDiff);
						}
					}
					if(isSameDay(end,lastPlageFin)){
						if (end.get(Calendar.HOUR_OF_DAY) > 0 &&
								end.get(Calendar.HOUR_OF_DAY)<= lastPlageFin.get(Calendar.HOUR_OF_DAY)) {
							System.out.println("debut: " +end.getTime()+"\nday end: "+lastPlageFin.getTime());
							double plagefinEndDiff =  Utils.differenceHeures(Utils.getHourDoubleFormat(end.getTime()),Utils.getHourDoubleFormat(lastPlageFin.getTime()));
							cont = Utils.differenceHeures(cont, plagefinEndDiff);
						}
					}
				}
//				else {
//					return cont++;
//				}
				currentDayDate.add(Calendar.DAY_OF_MONTH, 1);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return cont;
	}
	public boolean isSameDay(Calendar date1,Calendar date2){
		return date1.get(Calendar.DAY_OF_YEAR) == date2.get(Calendar.DAY_OF_YEAR) &&
				date1.get(Calendar.YEAR) == date2.get(Calendar.YEAR);
	}

	@SuppressWarnings("unchecked")
	public boolean isJrRepos(Day day,Absence abs) throws Exception {
		if (day.isJrRepos())
			return true;
		List<JrFerie> jrFeries = jrFerieService.readAll();
		Object o = Utils.isJrFerie(day.getDate(), jrFeries);
		if (o != null) {
			day.setLibelle(((JrFerie) o).getLibelle());
			day.setNature(o);
			day.setJrRepos(true);
			return true;
		}
		Hashtable<String, Object> props = new Hashtable<String, Object>();
		props.put("statut", Globals.WORKFLOW_STATUS_ELAPSED);
		props.put("collaborateur", this.user);
		List<Conge> conges = congeService.read(props);
		o = Utils.isHolidays(day.getDate(), conges);
		if (o != null) {
			day.setLibelle("conge");
			day.setNature(o);
			day.setJrRepos(true);
			return true;
		}
		return false;
	}

	public void setTypesAbsence(List<TypeAbsence> typesAbsence) {
		this.typesAbsence = typesAbsence;
	}

	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public TypeAbsence getTypeAbsence() {
		return typeAbsence;
	}

	public void setTypeAbsence(TypeAbsence typeAbsence) {
		this.typeAbsence = typeAbsence;
	}

	public TypeAbsence getSelectedTypeAbsence() {
		return selectedTypeAbsence;
	}

	public void setSelectedTypeAbsence(TypeAbsence selectedTypeAbsence) {
		this.selectedTypeAbsence = selectedTypeAbsence;
	}

	public Absence getSelectedAbsence() {
		return selectedAbsence;
	}

	public void setSelectedAbsence(Absence selectedAbsence) {
		this.selectedAbsence = selectedAbsence;
	}

	public boolean isWorkflowActive() {
		return workflowActive;
	}

	public void setWorkflowActive(boolean workflowActive) {
		this.workflowActive = workflowActive;
	}

	public void setJrFerieService(JrFerieManageableService jrFerieService) {
		this.jrFerieService = jrFerieService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public String getUnitTemps() {
		return unitTemps;
	}

	public void setUnitTemps(String unitTemps) {
		this.unitTemps = unitTemps;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}

	public JbpmManageable getJbpmService() {
		return jbpmService;
	}

	public void setJbpmService(JbpmManageable jbpmService) {
		this.jbpmService = jbpmService;
	}

	public MailManager getMailService() {
		return mailService;
	}

	public void setMailService(MailManager mailService) {
		this.mailService = mailService;
	}

	public boolean isMailActive() {
		return mailActive;
	}

	public void setMailActive(boolean mailActive) {
		this.mailActive = mailActive;
	}
	
	public RetardManageableService getRetardService() {
		return retardService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public void setDecisionHandler(DecisionHandler decisionHandler) {
		this.decisionHandler = decisionHandler;
	}

//	public Absence getAbsenceDemande() {
//		return absenceDemande;
//	}
//
//	public void setAbsenceDemande(Absence absenceDemande) {
//		this.absenceDemande = absenceDemande;
//	}

	

}