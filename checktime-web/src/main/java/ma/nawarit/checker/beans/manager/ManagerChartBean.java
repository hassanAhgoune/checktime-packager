package ma.nawarit.checker.beans.manager;
import java.io.Serializable;  

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
  
@ManagedBean(name = "managerChartBean")
@ViewScoped
public class ManagerChartBean implements Serializable {  
  
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BarChartModel lineModel;  
    
//	@PostConstruct
//    public void init() {  
//    	createLineModels();  
//    }  
  
    private void createLineModels() {  
    	lineModel = initLinearModel();
        lineModel.setTitle("Linear Chart");
        lineModel.setLegendPosition("e");
        lineModel.setShowPointLabels(true);
        Axis xAxis = lineModel.getAxis(AxisType.X);
        xAxis.setMin(4);
        xAxis.setMax(10);  
        xAxis.setTickFormat("%d");
        xAxis.setLabel("Jours");
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(24);   
        yAxis.setLabel("Heurs travaillées");
        lineModel.setSeriesColors("E61111,4986E7");
    }

    private BarChartModel initLinearModel() {
    	BarChartModel model = new BarChartModel();
 
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("TC");
 
        for (Integer i = 4; i < 11; i++) {
        	series1.set(i, 7);
		}
 
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("HTM");
        series2.setShowLine(false);
        series2.setDisableStack(true);
 
        series2.set(4, 5);
        series2.set(5, 6);
        series2.set(6, 7);
        series2.set(7, 8);
        series2.set(8, 10);
        series2.set(9, 5);
        series2.set(10, 0);
 
        model.addSeries(series1);
        model.addSeries(series2);
         
        return model;
    }
    
	public BarChartModel getLineModel() {
		if (lineModel == null)
		createLineModels(); 
		return lineModel;
	}  
    
}
