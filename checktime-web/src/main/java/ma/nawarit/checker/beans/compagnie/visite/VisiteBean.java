/*
Nom du projet : Checktime
Version : 1.0
Author : M.Morabit
Date : Mai 2014
*/
package ma.nawarit.checker.beans.compagnie.visite;
 
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.Vehicule;
import ma.nawarit.checker.compagnie.Visite;
import ma.nawarit.checker.compagnie.VisiteImpl;
import ma.nawarit.checker.compagnie.Visiteur;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.VisiteManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.TerminalParam;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.jbpm.DecisionHandler;
import ma.nawarit.checker.utils.Globals;


@ManagedBean(name = "visiteBean")
@ViewScoped
public class VisiteBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty(value = "#{VisiteManageableService}")
	private VisiteManageableService visiteService;
	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;
	@ManagedProperty(value = "#{UserManageableService}")
	private transient UserManageableService userService;
	
//	@ManagedProperty(value = "#{decisionHandler}")
//	private DecisionHandler decisionHandler;
	
	FacesContext context = FacesContext.getCurrentInstance();
	private User userAuthenticated;
	
	
	private Visite visite = new VisiteImpl();;
	private List<Visite> listeVisites;
	private String unit;
	private List<Unit> units;
	private String fichier;
	
	public VisiteBean() {
		// TODO Auto-generated constructor stub
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		userAuthenticated = (User)obj;
	}

	public void setUnits(List<Unit> units) {
		this.units = units;
	}

	public List<Unit> getUnits()
	{
		try {
			List<Unit> u = unitService.readAll();
			return u;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public UnitManageableService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public VisiteManageableService getVisiteService() {
		return visiteService;
	}

	public void setVisiteService(VisiteManageableService visiteService) {
		this.visiteService = visiteService;
	}

	public Visite getVisite() {
		return visite;
	}

	public void setVisite(Visite visite) {
		this.visite = visite;
	}

	

	public String getFichier() {
		return fichier;
	}

	public void setFichier(String fichier) {
		this.fichier = fichier;
	}

	public List<Visite> getListeVisites() {
		try {
			return visiteService.readAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void setListeVisites(List<Visite> listeVisites) {
		this.listeVisites = listeVisites;
	}
	
	
	public void saveVisiteListener(ActionEvent ev)
	{
		try {						
			if (visite.getId() == 0) {
				visite.setCode("");
				visite.setStatut("encours");
				if (visite.getDateRdv() != null) {
					visite.setDateDebut(visite.getDateRdv());
					visite.setDateFin(visite.getDateRdv());
				}
				if (userAuthenticated != null)
					visite.setUser(userAuthenticated);
//				HashMap<String, String> hash = new HashMap<String, String>();
//				System.out.println(userAuthenticated.getSupH().getId());
//				User supHr = userService.load(userAuthenticated.getSupH().getId());
//				if (supHr == null)
//					return;
//				hash.put("sender", userAuthenticated.getMail());
//				hash.put("receiver", supHr.getMail());
//				hash.put("subject", "Demande de visite");
//				hash.put("workflowDate", Utils.toStrDate(new Date(), "dd/MM/yyyy hh:mm"));
//				hash.put("manager", supHr.getNom());
//				hash.put("workflowName", "visite");
//				hash.put("workflowUrl", "localhost:8080/checktime-web/pages/compagnie/visite/VIS-ACC.wit");
//				hash.put("userName", userAuthenticated.getNom());
//				hash.put("userMail", userAuthenticated.getMail());
//				decisionHandler.deployProcessDefinition(hash, "process-workflow", true);
				visiteService.create(visite);
				visite.setCode("VIS-"+visite.getId());
			}
			visiteService.update(visite);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void refreshVisAction(String stat, Visite vis) {
		if (stat.equals("dem"))
			vis.setStatut("encours");
		else if (stat.equals("val"))
			vis.setStatut("validated");
		else if (stat.equals("ref"))
			vis.setStatut("rejected");
		else if (stat.equals("eff"))
			vis.setStatut("elapsed");
		try {
			visiteService.update(vis);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void editVisite()
	{
		try {
			visiteService.update(visite);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadUpdate(int id) {
		try {
			this.visite = visiteService.load(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteVisite()
	{
		try {
			visiteService.delete(visite);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addVisiteur()
	{
		Visiteur v = Visiteur.Factory.newInstance();
		v.setNom("");
		v.setPrenom("");
		v.setNumIdent("");
		v.setAdress("");
		v.setCity("");
		v.setPays("");
		v.setSociete("");
		v.setFonction("");
		v.setDateNss(new Date());
		v.setMail("");
		v.setSexe("");
		v.setVisite(visite);
		this.visite.getVisiteurs().add(v);

	}
	
	public void deleteVisiteur(int index) {
		try {
			visite.getVisiteurs().remove(index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addVehicule()
	{
		Vehicule v = Vehicule.Factory.newInstance();
		v.setMarque("");
		v.setModele("");
		v.setMatricule("");
		v.setCouleur("");
		v.setVisite(visite);
		this.visite.getVehicules().add(v);

	}
	
	public void deleteVehicule(int index) {
		try {
			visite.getVehicules().remove(index);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

//	public void setDecisionHandler(DecisionHandler decisionHandler) {
//		this.decisionHandler = decisionHandler;
//	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}
	
	
}
