/* ====================================================================
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
==================================================================== */

package ma.nawarit.checker.beans.share;

import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.core.common.CommonReportData;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.engine.Field;
import ma.nawarit.checker.engine.UserReportData;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AnnomalieImpl;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;

import org.apache.poi.xssf.usermodel.*;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import java.util.HashMap;
import java.util.Calendar;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * XlsModel
 *
 * @author j.elbaamrani
 */
public class XlsModel {

	private static final int TAUX1 = 125;
	private static final int TAUX2 = 150;

	public static ByteArrayOutputStream generateXLSUsersReport(List<User> data, String grandTitle, List<String> titles,
			List<Field> fields) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = createSheet("Etat Collaborateurs", wb, styles, grandTitle, titles);

		Row row;
		int rownum = 3;
		int k = 0;
		String styleName = "cell_b";
		for (int i = 0; i < data.size(); i++, rownum++) {
			row = sheet.createRow(rownum);
			if (data.get(i) == null)
				continue;
			k = 0;
			for (Field f : fields) {
				if (f.isCheck()) {
					Cell cell = row.createCell(k);
					if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z10"))) {
						cell.setCellValue(data.get(i).getMatricule());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z1"))) {
						cell.setCellValue(data.get(i).getNom());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z2"))) {
						cell.setCellValue(data.get(i).getPrenom());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z20"))) {
						cell.setCellValue(data.get(i).getCinNum());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z15"))) {
						cell.setCellValue(data.get(i).getBadge());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z22"))
							&& data.get(i).getDateNaiss() != null) {
						cell.setCellValue(Utils.convertDate2Str(data.get(i).getDateNaiss(), "dd-MM-yyyy"));
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z23"))) {
						cell.setCellValue(data.get(i).getNaissLoc());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z21"))) {
						cell.setCellValue(data.get(i).getCnssNum());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z3"))
							&& data.get(i).getProfilMetier() != null) {
						cell.setCellValue(data.get(i).getProfilMetier().getLibelle());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z5"))
							&& data.get(i).getDateEmb() != null) {
						cell.setCellValue(Utils.convertDate2Str(data.get(i).getDateEmb(), "dd-MM-yyyy"));
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z4"))
							&& data.get(i).getUnit() != null) {
						cell.setCellValue(data.get(i).getUnit().getLibelle());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z12"))
							&& data.get(i).getNoeud() != null) {
						cell.setCellValue(data.get(i).getNoeud().getLibelle());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z9"))
							&& data.get(i).getProfilApp() != null) {
						cell.setCellValue(data.get(i).getProfilApp().getLibelle());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z19"))) {
						cell.setCellValue(data.get(i).getAdressPostale());
						cell.setCellStyle(styles.get(styleName));
					} else if (f.getLabel().equals(MessageFactory.getMessage("USR_E001_Z26"))
							&& data.get(i).getPlanningAffected() != null) {
						cell.setCellValue(data.get(i).getPlanningAffected().getLibelle());
						cell.setCellStyle(styles.get(styleName));
					}

					k++;
				}
			}
		}

		sheet.setZoom(3, 4);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSMvtsReport(List<CommonReportData> data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = createSheet("Etat Pointage", wb, styles, grandTitle, titles);

		Row row;
		int rownum = 3;
		for (int i = 0; i < data.size(); i++, rownum++) {
			row = sheet.createRow(rownum);
			if (data.get(i) == null)
				continue;

			Cell cell = row.createCell(0);
			String styleName = "cell_b";
			cell.setCellValue(data.get(i).getUser().getMatricule());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(1);
			cell.setCellValue(data.get(i).getUser().getNom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(2);
			cell.setCellValue(data.get(i).getUser().getPrenom());
			cell.setCellStyle(styles.get(styleName));

			styleName = "cell_normal_date";
			List<Mouvement> mvts;
			String lesMvts = "";
			for (int j = 0; j < data.get(i).getResults().size(); j++) {
				cell = row.createCell(j + 3);
				CommonReportData res = (CommonReportData) data.get(i).getResults().get(j);

				mvts = res.getResults();
				if (mvts != null)
					for (Mouvement m : mvts)
						lesMvts += Utils.convertDate2Str(m.getDate(), "HH:mm:ss") + "\n";
				cell.setCellValue(lesMvts);
				lesMvts = "";
				cell.setCellStyle(styles.get(styleName));
			}
		}

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		sheet.setColumnWidth(0, 256 * 10);
		sheet.setColumnWidth(1, 256 * 18);
		sheet.setColumnWidth(2, 256 * 15);
		sheet.setZoom(3, 4);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);

		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXlsAnomalieReport(List<AnnomalieImpl> data, String grandTitle,
			List<String> titles) throws Exception {
		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = createSheet("Etat d'Anomalies", wb, styles, grandTitle, titles);

		Row row;
		int rownum = 3;
		AnnomalieImpl anno = null;
		User usr = null;
		for (int i = 0; i < data.size(); i++, rownum++) {
			row = sheet.createRow(rownum);
			anno = data.get(i);
			if (anno == null)
				continue;
			usr = anno.getUser();
			Cell cell = row.createCell(0);
			String styleName = "cell_b";
			if (usr != null)
				cell.setCellValue(usr.getMatricule());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(1);
			if (usr != null)
				cell.setCellValue(usr.getNom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(2);
			if (usr != null)
				cell.setCellValue(usr.getPrenom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(3);
			cell.setCellValue(Utils.convertDate2Str(anno.getDateAnomalie(), "dd/MM/yyyy"));
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(4);
			cell.setCellValue(anno.getDescription());
			cell.setCellStyle(styles.get(styleName));

		}

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		sheet.setColumnWidth(0, 256 * 10);
		sheet.setColumnWidth(1, 256 * 18);
		sheet.setColumnWidth(2, 256 * 15);
		sheet.setZoom(3, 4);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSComalam1MvtsReport(List<UserReportData> data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = wb.createSheet("Etat de Pointage");

		// turn off gridlines
		sheet.setDisplayGridlines(false);
		sheet.setPrintGridlines(false);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);

		// title row
		Row titleRow = sheet.createRow(0);
		titleRow.setHeightInPoints(45);
		Cell titleCell = titleRow.createCell(0);
		titleCell.setCellValue(grandTitle);
		titleCell.setCellStyle(styles.get("title"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));

		// the header row: centered text in 48pt font
		Row interRow = sheet.createRow(2);
		interRow.setHeightInPoints(12.75f);
		Cell cellinter = interRow.createCell(5);
		cellinter.setCellValue("Interruptions");
		cellinter.setCellStyle(styles.get("header"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("$F$3:$G$3"));

		Row headerRow = sheet.createRow(3);
		headerRow.setHeightInPoints(12.75f);
		int j = 0;
		for (String t : titles) {
			if (t == null)
				continue;
			Cell cell = headerRow.createCell(j);
			cell.setCellValue(t);
			cell.setCellStyle(styles.get("header"));
			sheet.setColumnWidth(j, 256 * (t.length() + 6));
			j++;
		}

		// freeze the first row
		sheet.createFreezePane(0, 1);
		sheet.createFreezePane(0, 4);

		Row row;
		int rownum = 4;
		for (int i = 0; i < data.size(); i++, rownum++) {
			row = sheet.createRow(rownum);
			if (data.get(i) == null)
				continue;

			Cell cell = row.createCell(0);
			String styleName = "cell_b";
			cell.setCellValue(data.get(i).getUser().getMatricule());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(1);
			cell.setCellValue(data.get(i).getUser().getNom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(2);
			cell.setCellValue(data.get(i).getUser().getPrenom());
			cell.setCellStyle(styles.get(styleName));

			styleName = "cell_normal_date";
			List<Mouvement> mvts;
			DayData res = (DayData) data.get(i).getDayDatas().get(0);
			if (res.getInOuts() != null && !res.getInOuts().isEmpty()) {
				cell = row.createCell(3);
				cell.setCellValue(res.getInOuts().get(0));
				cell.setCellStyle(styles.get(styleName));
				mvts = res.getMouvements();
				if (mvts != null && !mvts.isEmpty()) {
					cell = row.createCell(4);
					cell.setCellValue(Utils.convertDate2Str(mvts.get(0).getDate(), "HH:mm"));
					cell.setCellStyle(styles.get(styleName));
					int k = 1;
					String s1 = "", s2 = "";
					if (res.getMouvements().size() > 2)
						while (k < res.getMouvements().size() - 1) {
							if (k % 2 != 0)
								s1 += Utils.convertDate2Str(mvts.get(k).getDate(), "HH:mm");
							else
								s2 += Utils.convertDate2Str(mvts.get(k).getDate(), "HH:mm");
							k++;
						}

					cell = row.createCell(5);
					cell.setCellValue(s1);
					cell.setCellStyle(styles.get(styleName));
					cell = row.createCell(6);
					cell.setCellValue(s2);
					cell.setCellStyle(styles.get(styleName));

					if (res.getMouvements().size() >= 2) {
						cell = row.createCell(7);
						cell.setCellValue(
								Utils.convertDate2Str(mvts.get(res.getMouvements().size() - 1).getDate(), "HH:mm"));
						cell.setCellStyle(styles.get(styleName));
					}
				}
				cell = row.createCell(8);
				cell.setCellValue(res.getInOuts().get(1));
				cell.setCellStyle(styles.get(styleName));
			}
			cell = row.createCell(9);
			cell.setCellValue(Utils.hourFormat(res.getCol2ht()));
			cell.setCellStyle(styles.get("cell_b"));
		}

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		sheet.setColumnWidth(0, 256 * 10);
		sheet.setColumnWidth(1, 256 * 18);
		sheet.setColumnWidth(2, 256 * 15);
		sheet.setZoom(3, 4);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSComalam2MvtsReport(List<UserReportData> data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = createSheet("Etat Pointage", wb, styles, grandTitle, titles);

		Row row;
		int rownum = 3;
		for (int i = 0; i < data.size(); i++, rownum++) {
			row = sheet.createRow(rownum);
			if (data.get(i) == null)
				continue;

			Cell cell = row.createCell(0);
			String styleName = "cell_b";
			cell.setCellValue(data.get(i).getUser().getMatricule());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(1);
			cell.setCellValue(data.get(i).getUser().getNom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(2);
			cell.setCellValue(data.get(i).getUser().getPrenom());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(3);
			int nb = 0;
			for (DayData dd : data.get(i).getDayDatas())
				if (dd.getMouvements() != null && !dd.getMouvements().isEmpty())
					nb++;
			cell.setCellValue(nb);
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(4);
			nb = 0;
			for (DayData dd : data.get(i).getDayDatas())
				if (dd.getTcol8() > 0)
					nb++;
			cell.setCellValue(nb);
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(5);
			nb = 0;
			for (DayData dd : data.get(i).getDayDatas())
				if (dd.getTcol9() > 0)
					nb++;
			cell.setCellValue(nb);
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(6);
			cell.setCellValue(data.get(i).getTcol2());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(7);
			cell.setCellValue(data.get(i).getTcol2ht());
			cell.setCellStyle(styles.get(styleName));

			cell = row.createCell(8);
			cell.setCellValue(data.get(i).getTcol1() - data.get(i).getTcol2ht());
			cell.setCellStyle(styles.get(styleName));
		}

		// set column widths, the width is measured in units of 1/256th of a character
		// width
		sheet.setColumnWidth(0, 256 * 10);
		sheet.setColumnWidth(1, 256 * 18);
		sheet.setColumnWidth(2, 256 * 15);
		sheet.setZoom(3, 4);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSCumulReportStream(List<UserReportData> data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		generateXLSCumulReport(data, grandTitle, titles, wb);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static void generateXLSCumulReport(List<UserReportData> data, String grandTitle, List<String> titles, Workbook wb) {
		Map<String, CellStyle> styles = createStyles(wb);

		Sheet sheet = createSheet("Etat Cumuls", wb, styles, grandTitle, titles);

		Row row;
		List<Integer> rowsgrouped = new ArrayList<Integer>();
		int rownum = 3;

		String styleName = "cell_b_t";
		String styleName2 = "cell_b_centered";
		String styleName_t = "cell_b_t";
		String styleName2_t = "cell_b_centered";
//		List<Field> fields = data.get(0).getFields();
		short ind;
		UserReportData urData;
		for (int i = 0; i < data.size(); i++, rownum++) {
			urData = data.get(i);
			row = sheet.createRow(rownum++);
			row.getRowNum();
			Cell cell = row.createCell(0);
			String userrow = urData.getUser().getMatricule() + " " + urData.getUser().getNom() + " "
					+ urData.getUser().getPrenom();
			cell.setCellValue(userrow);
			sheet.addMergedRegion(CellRangeAddress.valueOf("$A$" + rownum + ":$O$" + rownum));
			cell.setCellStyle(styles.get("cell_bb"));

			ind = 0;
			row = sheet.createRow(rownum++);
			rowsgrouped.add(row.getRowNum());
			cell = row.createCell(ind++);
			cell.setCellValue("");
			sheet.addMergedRegion(CellRangeAddress.valueOf("$A$" + rownum + ":$C$" + rownum));
			cell.setCellStyle(styles.get(styleName_t));
			cell = row.createCell(ind++);
			cell = row.createCell(ind++);
			cell = row.createCell(ind++);
			cell.setCellValue(urData.getTcol1());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(ind++);
			cell.setCellValue(urData.getTcol2());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(ind++);
			cell.setCellValue(urData.getTcol2ht());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(ind++);
			cell.setCellValue(urData.getTcol2bis());
			cell.setCellStyle(styles.get(styleName2_t));

			// cell = row.createCell(ind++);
			// String p = urData.getTcol2prm()[0] + "p " + urData.getTcol2prm()[1] + "t " +
			// urData.getTcol2prm()[2] + "r";
			// cell.setCellValue(p);
			// cell.setCellStyle(styles.get(styleName_t));
//			if (fields.get(0).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol3());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(1).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol4());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(2).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol5());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(3).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol6());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(4).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol7());
//				cell.setCellStyle(styles.get(styleName2_t));
//				cell = row.createCell(ind++);
//			}
//
//			if (fields.get(5).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol8());
//				cell.setCellStyle(styles.get(styleName2_t));
//				cell = row.createCell(ind++);
//			}
//
//			if (fields.get(6).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol9());
//				cell.setCellStyle(styles.get(styleName2_t));
//				cell = row.createCell(ind++);
//			}
//
//			if (fields.get(7).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol10());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(8).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol11());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(9).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol12());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(10).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol13());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(11).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol14());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(12).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol15());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(13).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol16());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(14).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol17());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(15).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol18());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(16).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol19());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}
//
//			if (fields.get(17).isCheck()) {
//				cell = row.createCell(ind++);
//				cell.setCellValue(urData.getTcol20());
//				cell.setCellStyle(styles.get(styleName2_t));
//			}

			for (DayData dd : urData.getDayDatas()) {
				row = sheet.createRow(rownum++);
				ind = 0;
				cell = row.createCell(ind++);
				if (dd.getType().equalsIgnoreCase("total")) {
					cell.setCellValue(dd.getLibelle());
					sheet.addMergedRegion(CellRangeAddress.valueOf("$A$" + rownum + ":$C$" + rownum));
				} else
					cell.setCellValue(Utils.convertDate2Str(dd.getDate(), "EEE dd/MM/yyyy"));
				cell.setCellStyle(styles.get(styleName));

				cell = row.createCell(ind++);
				String mvtStr = "";
				if (dd.getMouvements() != null && !dd.getMouvements().isEmpty()) {
					for (Mouvement mvt : dd.getMouvements())
						mvtStr += Utils.convertDate2Str(mvt.getDate(), "HH:mm:ss") + "\n";
					cell.setCellValue(new HSSFRichTextString(mvtStr));
					cell.setCellStyle(styles.get("cell_b_hour"));
				}

				cell = row.createCell(ind++);
				Horaire hor = dd.getHoraire();
				String horLib = (hor != null && hor.getLibelle() != null) ? hor.getLibelle() : "";
				cell.setCellValue(horLib);
				cell.setCellStyle(styles.get(styleName));

				cell = row.createCell(ind++);
				cell.setCellValue(dd.getCol1());
				cell.setCellStyle(styles.get(styleName2));

				cell = row.createCell(ind++);
				cell.setCellValue(dd.getCol2());
				cell.setCellStyle(styles.get(styleName2));

				cell = row.createCell(ind++);
				cell.setCellValue(dd.getCol2ht());
				cell.setCellStyle(styles.get(styleName2));

				cell = row.createCell(ind++);
				cell.setCellValue(dd.getCol2bis());
				cell.setCellStyle(styles.get(styleName2));

				// cell = row.createCell(ind++);
				// p = dd.getCol2prm()[0] + "p " + dd.getCol2prm()[1] + "t " +
				// dd.getCol2prm()[2] + "r";
				// cell.setCellValue(p);
				// cell.setCellStyle(styles.get(styleName));

//				if (fields.get(0).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol3());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(1).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol4());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(2).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol5());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(3).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol6());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(4).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getTcol7());
//					cell.setCellStyle(styles.get(styleName2));
//					cell = row.createCell(ind++);
//					String motifCg = "";
//					if (dd.getCol7() != null && !dd.getCol7().isEmpty()) {
//						CongeImpl cng = ((CongeImpl) dd.getCol7().get(0));
//						motifCg = (cng != null) ? cng.getMotif4Report() : "";
//					}
//					cell.setCellValue(motifCg);
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(5).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getTcol8());
//					cell.setCellStyle(styles.get(styleName2));
//
//					cell = row.createCell(ind++);
//					String motifAb = "";
//					if (dd.getCol8() != null && !dd.getCol8().isEmpty())
//						for (Absence ab : dd.getCol8()) {
//							if (ab != null)
//								motifAb += ((AbsenceImpl) ab).getMotif4Report();
//						}
//					cell.setCellValue(motifAb);
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(6).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getTcol9());
//					cell.setCellStyle(styles.get(styleName2));
//
//					cell = row.createCell(ind++);
//					String motifAb = "";
//					if (dd.getCol9() != null && !dd.getCol9().isEmpty())
//						for (Absence ab : dd.getCol9()) {
//							if (ab != null)
//								motifAb += ((AbsenceImpl) ab).getMotif4Report();
//						}
//					cell.setCellValue(motifAb);
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(7).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol10());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(8).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol11());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(9).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol12());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(10).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol13());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(11).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol14());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(12).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol15());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(13).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol16());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(14).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol17());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(15).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol18());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(16).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol19());
//					cell.setCellStyle(styles.get(styleName2));
//				}
//
//				if (fields.get(17).isCheck()) {
//					cell = row.createCell(ind++);
//					cell.setCellValue(dd.getCol20());
//					cell.setCellStyle(styles.get(styleName2));
//				}
			}
			rowsgrouped.add(rownum - 1);
		}

		sheet.setZoom(3, 4);

		// group rows for each phase, row numbers are 0-based

		int j, rowI, rowJ;

		for (int i = 0; i < rowsgrouped.size(); i++) {
			j = i + 1;
			rowI = rowsgrouped.get(i);
			rowJ = rowsgrouped.get(j);
			sheet.groupRow(rowI, rowJ);
			sheet.setRowGroupCollapsed(rowI, true);
			i++;
		}

		sheet.autoSizeColumn(1);
	}


	public static ByteArrayOutputStream generateXLSPaie4CopagReport(List<UserReportData> data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("USR_E000_label13"));
		// Nom
		titles.add(MessageFactory.getMessage("USR_E000_label1"));
		// Prenom
		titles.add(MessageFactory.getMessage("USR_E000_label2"));
		// HNT
		titles.add(MessageFactory.getMessage("BOR_E001_L5"));
		// HS25
		titles.add(MessageFactory.getMessage("BOR_E001_L11") + TAUX1);
		// HS50
		titles.add(MessageFactory.getMessage("BOR_E001_L12") + TAUX2);

		Sheet sheet = createSheet("Etat Paie", wb, styles, grandTitle, titles);

		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		UserReportData urData;
		Cell cell;

		for (int i = 0; i < data.size(); i++, rownum++) {
			urData = data.get(i);
			row = sheet.createRow(rownum);

			cell = row.createCell(0);
			cell.setCellValue(urData.getUser().getMatricule());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(1);
			cell.setCellValue(urData.getUser().getNom());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(2);
			cell.setCellValue(urData.getUser().getPrenom());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(3);
			cell.setCellValue(urData.getTcol2cop());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(4);
			cell.setCellValue(urData.getTcol10());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(5);
			cell.setCellValue(urData.getTcol11());
			cell.setCellStyle(styles.get(styleName2_t));

		}
		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSAbsenceReport(HashMap data, String grandTitle, List<String> titles)
			throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("USR_E000_label13"));
		// Nom
		titles.add(MessageFactory.getMessage("USR_E000_label1"));
		// Prenom
		titles.add(MessageFactory.getMessage("USR_E000_label2"));
		// Motif
		titles.add(MessageFactory.getMessage("ABS_E000_Z2"));
		// Début
		titles.add(MessageFactory.getMessage("ABS_E000_Z3"));
		// Fin
		titles.add(MessageFactory.getMessage("ABS_E000_Z4"));
		// Durée
		titles.add(MessageFactory.getMessage("ABS_E000_Z5"));
		// Payant ?
		titles.add(MessageFactory.getMessage("ABS_E000_Z6"));

		Sheet sheet = createSheet("Etat Absence", wb, styles, grandTitle, titles);

		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		short ind;
		List<CommonReportData> datas = (List<CommonReportData>) data.get("dataSource");
		CommonReportData cmReportData;
		Absence absence;
		List<Absence> subDatas;
		Cell cell;

		if (datas != null && !data.isEmpty()) {
			Iterator<CommonReportData> it = datas.iterator();
			Iterator<Absence> subIt;
			while (it.hasNext()) {
				cmReportData = it.next();
				row = sheet.createRow(rownum);
				cell = row.createCell(0);
				cell.setCellValue(cmReportData.getUser().getMatricule());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(1);
				cell.setCellValue(cmReportData.getUser().getNom());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(2);
				cell.setCellValue(cmReportData.getUser().getPrenom());
				cell.setCellStyle(styles.get(styleName2_t));
				subDatas = cmReportData.getResults();
				if (subDatas != null && !subDatas.isEmpty()) {
					subIt = subDatas.iterator();
					while (subIt.hasNext()) {
						absence = subIt.next();
						rownum++;
						row = sheet.createRow(rownum);

						cell = row.createCell(3);
						cell.setCellValue(absence.getTypeAbsence().getLibelle());
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(4);
						cell.setCellValue(Utils.convertDate2Str(absence.getDateDebut(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(5);
						cell.setCellValue(Utils.convertDate2Str(absence.getDateReprise(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(6);
						cell.setCellValue(absence.getDuree());
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(7);
						cell.setCellValue((absence.getTypeAbsence().isDeduir()) ? "oui" : "non");
						cell.setCellStyle(styles.get(styleName2_t));

					}
					rownum++;
					row = sheet.createRow(rownum);

					cell = row.createCell(5);
					cell.setCellValue("TOTAL :");
					cell.setCellStyle(styles.get(styleName2_t));
					cell = row.createCell(6);
					cell.setCellValue(cmReportData.getCumulAbsenceAuto());
					cell.setCellStyle(styles.get(styleName2_t));
					rownum++;

				}

			}
		}

		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSAbsenceJournalier4CopagReport(HashMap data, String grandTitle,
			List<String> titles,String context) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();

		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("Collabo.L6"));
		// Nom
		titles.add(MessageFactory.getMessage("USR_E000_label1"));
		// Prenom
		titles.add(MessageFactory.getMessage("USR_E000_label2"));
		// Motif
		titles.add(MessageFactory.getMessage("ABS_E000_Z2"));
		// Début
		titles.add(MessageFactory.getMessage("ABS_E000_Z3"));
		// Fin
		titles.add(MessageFactory.getMessage("ABS_E000_Z4"));
		// Durée
		titles.add(MessageFactory.getMessage("ABS_E000_Z5"));
		// Payant ?
		titles.add(MessageFactory.getMessage("ABS_E000_Z6"));

		Sheet sheet = createSheet("Etat Absence", wb, styles, grandTitle, titles);
		/**
		 * 
		 */
		if (context != null) {
			String CheckTimeLogo = context+Globals.CHECKTIME_RPT_LOGO_PATH;
			String CompanyLogo = context+Globals.COMPANY_LOGO_PATH;
			InputStream checktime_logo = new FileInputStream(CheckTimeLogo);
			InputStream campany_logo = new FileInputStream(CompanyLogo);

			byte[] check_log = IOUtils.toByteArray(checktime_logo);
			byte[] comp_log = IOUtils.toByteArray(campany_logo);

			int CheckIdx = wb.addPicture(check_log, Workbook.PICTURE_TYPE_PNG);
			int CompIdx = wb.addPicture(comp_log, Workbook.PICTURE_TYPE_PNG);

			checktime_logo.close();
			campany_logo.close();

			CreationHelper Check_helper = wb.getCreationHelper();
			Drawing Check_drawing = sheet.createDrawingPatriarch();
			ClientAnchor Checkanchor = Check_helper.createClientAnchor();
			Checkanchor.setCol1(0);
			Checkanchor.setRow1(0);
			Checkanchor.setCol2(2);
			Checkanchor.setRow2(1);
			Picture Checkpict = Check_drawing.createPicture(Checkanchor, CheckIdx);
			Cell cel = sheet.createRow(1).createCell(2);
			
			CreationHelper Compa_helper = wb.getCreationHelper();
			Drawing Compa_drawing = sheet.createDrawingPatriarch();
			ClientAnchor Comp_anchor = Compa_helper.createClientAnchor();
			Comp_anchor.setCol1(7); // Column B
			Comp_anchor.setRow1(0); // Row 3
			Comp_anchor.setCol2(8); // Column C
			Comp_anchor.setRow2(2);
			Picture Compapict = Compa_drawing.createPicture(Comp_anchor, CompIdx);

			cel = sheet.createRow(2).createCell(8);
		}
		/**
		 * Finnish
		 */
		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		short ind;
		List<CommonReportData> datas = (List<CommonReportData>) data.get("dataSource");
		CommonReportData cmReportData;
		Absence absence;
		List<Absence> subDatas;
		Cell cell;

		if (datas != null && !data.isEmpty()) {
			Iterator<CommonReportData> it = datas.iterator();
			Iterator<Absence> subIt;
			while (it.hasNext()) {
				cmReportData = it.next();
				row = sheet.createRow(rownum);
				cell = row.createCell(0);
				cell.setCellValue(cmReportData.getUser().getMatricule());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(1);
				cell.setCellValue(cmReportData.getUser().getNom());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(2);
				cell.setCellValue(cmReportData.getUser().getPrenom());
				cell.setCellStyle(styles.get(styleName2_t));
				subDatas = cmReportData.getResults();
				if (subDatas != null && !subDatas.isEmpty()) {
					subIt = subDatas.iterator();
					while (subIt.hasNext()) {
						absence = subIt.next();

						cell = row.createCell(3);
						cell.setCellValue(absence.getTypeAbsence().getLibelle());
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(4);
						cell.setCellValue(Utils.convertDate2Str(absence.getDateDebut(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(5);
						cell.setCellValue(Utils.convertDate2Str(absence.getDateReprise(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(6);
						cell.setCellValue(absence.getDuree());
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(7);
						cell.setCellValue((absence.getTypeAbsence().isDeduir()) ? "oui" : "non");
						cell.setCellStyle(styles.get(styleName2_t));

					}
					rownum++;

				}

			}
		}

		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSAbsencePeriodique4CopagReport(HashMap data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("Collabo.L6"));
		// Nom
		titles.add(MessageFactory.getMessage("Collabo.add.L1"));
		// Prenom
		titles.add(MessageFactory.getMessage("Collabo.add.L2"));
		// Durée
		titles.add(MessageFactory.getMessage("Absence.L5"));

		Sheet sheet = createSheet("Etat Absence", wb, styles, grandTitle, titles);

		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		short ind;
		List<CommonReportData> datas = (List<CommonReportData>) data.get("dataSource");
		CommonReportData cmReportData;
		Absence absence;
		List<Absence> subDatas;
		Cell cell;
		int nbrjAbs = 0;
		if (datas != null && !datas.isEmpty()) {
			Iterator<CommonReportData> it = datas.iterator();
			Iterator<Absence> subIt;
			while (it.hasNext()) {
				cmReportData = it.next();
				row = sheet.createRow(rownum);
				cell = row.createCell(0);
				cell.setCellValue(cmReportData.getUser().getMatricule());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(1);
				cell.setCellValue(cmReportData.getUser().getNom());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(2);
				cell.setCellValue(cmReportData.getUser().getPrenom());
				cell.setCellStyle(styles.get(styleName2_t));
				subDatas = cmReportData.getResults();
				nbrjAbs = 0;
				if (subDatas != null && !subDatas.isEmpty()) {
					subIt = subDatas.iterator();
					while (subIt.hasNext()) {
						absence = subIt.next();
						if (absence.getDuree() >= 8)
							nbrjAbs++;
					}
					cell = row.createCell(3);
					cell.setCellValue(nbrjAbs);
					cell.setCellStyle(styles.get(styleName2_t));
					rownum++;
				}

			}
		}

		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSRetardReport(HashMap data, String grandTitle, List<String> titles)
			throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("USR_E000_label13"));
		// Nom
		titles.add(MessageFactory.getMessage("USR_E000_label1"));
		// Prenom
		titles.add(MessageFactory.getMessage("USR_E000_label2"));
		// Date
		titles.add(MessageFactory.getMessage("RTD_E000_label3"));
		// Retard toléré
		titles.add(MessageFactory.getMessage("RTD_E000_label9"));
		// Retard non toléré
		titles.add(MessageFactory.getMessage("RTD_E000_label10"));
		// Description
		titles.add(MessageFactory.getMessage("RTD_E000_label5"));

		Sheet sheet = createSheet("Etat Retard", wb, styles, grandTitle, titles);

		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		short ind;
		List<CommonReportData> datas = (List<CommonReportData>) data.get("dataSource");
		CommonReportData cmReportData;
		Retard retard;
		List<Retard> subDatas;
		Cell cell;

		if (datas != null && !data.isEmpty()) {
			Iterator<CommonReportData> it = datas.iterator();
			Iterator<Retard> subIt;
			while (it.hasNext()) {
				cmReportData = it.next();
				row = sheet.createRow(rownum);
				cell = row.createCell(0);
				cell.setCellValue(cmReportData.getUser().getMatricule());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(1);
				cell.setCellValue(cmReportData.getUser().getNom());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(2);
				cell.setCellValue(cmReportData.getUser().getPrenom());
				cell.setCellStyle(styles.get(styleName2_t));
				subDatas = cmReportData.getResults();
				if (subDatas != null && !subDatas.isEmpty()) {
					subIt = subDatas.iterator();
					while (subIt.hasNext()) {
						retard = subIt.next();
						rownum++;
						row = sheet.createRow(rownum);

						cell = row.createCell(3);
						cell.setCellValue(Utils.convertDate2Str(retard.getDate(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(4);
						cell.setCellValue(Utils.convertMin2Hour(Double.parseDouble("" + retard.getRetardTolere())));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(5);
						cell.setCellValue(Utils.convertMin2Hour(Double.parseDouble("" + retard.getRetardNonTolere())));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(6);
						cell.setCellValue(retard.getDescription());
						cell.setCellStyle(styles.get(styleName2_t));

					}
					rownum++;
					row = sheet.createRow(rownum);

					cell = row.createCell(3);
					cell.setCellValue("TOTAL :");
					cell.setCellStyle(styles.get(styleName2_t));
					cell = row.createCell(4);
					cell.setCellValue(Utils.convertMin2Hour(Double.parseDouble(cmReportData.getCumulRetardTolere())));
					cell.setCellStyle(styles.get(styleName2_t));
					cell = row.createCell(5);
					cell.setCellValue(
							Utils.convertMin2Hour(Double.parseDouble(cmReportData.getCumulRetardNonTolere())));
					cell.setCellStyle(styles.get(styleName2_t));

					rownum++;

				}

			}
		}

		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSRetard4CopagReport(HashMap data, String grandTitle,
			List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Matricule
		titles.add(MessageFactory.getMessage("USR_E000_label13"));
		// Nom
		titles.add(MessageFactory.getMessage("USR_E000_label1"));
		// Prenom
		titles.add(MessageFactory.getMessage("USR_E000_label2"));
		// Date
		titles.add(MessageFactory.getMessage("RTD_E000_label3"));
		// Retard non toléré
		titles.add(MessageFactory.getMessage("RTD_E000_label10"));
		// Description
		titles.add(MessageFactory.getMessage("RTD_E000_label5"));

		Sheet sheet = createSheet("Etat Retard", wb, styles, grandTitle, titles);

		Row row;

		int rownum = 3;

		String styleName2_t = "cell_b_centered";

		short ind;
		List<CommonReportData> datas = (List<CommonReportData>) data.get("dataSource");
		CommonReportData cmReportData;
		Retard retard;
		List<Retard> subDatas;
		Cell cell;

		if (datas != null && !data.isEmpty()) {
			Iterator<CommonReportData> it = datas.iterator();
			Iterator<Retard> subIt;
			while (it.hasNext()) {
				cmReportData = it.next();
				row = sheet.createRow(rownum);
				cell = row.createCell(0);
				cell.setCellValue(cmReportData.getUser().getMatricule());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(1);
				cell.setCellValue(cmReportData.getUser().getNom());
				cell.setCellStyle(styles.get(styleName2_t));

				cell = row.createCell(2);
				cell.setCellValue(cmReportData.getUser().getPrenom());
				cell.setCellStyle(styles.get(styleName2_t));
				subDatas = cmReportData.getResults();
				if (subDatas != null && !subDatas.isEmpty()) {
					subIt = subDatas.iterator();
					while (subIt.hasNext()) {
						retard = subIt.next();
						rownum++;
						row = sheet.createRow(rownum);

						cell = row.createCell(3);
						cell.setCellValue(Utils.convertDate2Str(retard.getDate(), "dd/MM/yyyy"));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(4);
						cell.setCellValue(Utils.convertMin2Hour(Double.parseDouble("" + retard.getRetardNonTolere())));
						cell.setCellStyle(styles.get(styleName2_t));

						cell = row.createCell(5);
						cell.setCellValue(retard.getDescription());
						cell.setCellStyle(styles.get(styleName2_t));

					}
					rownum++;
					row = sheet.createRow(rownum);

					cell = row.createCell(3);
					cell.setCellValue("TOTAL :");
					cell.setCellStyle(styles.get(styleName2_t));
					cell = row.createCell(4);
					cell.setCellValue(
							Utils.convertMin2Hour(Double.parseDouble(cmReportData.getCumulRetardNonTolere())));
					cell.setCellStyle(styles.get(styleName2_t));

					rownum++;

				}

			}
		}

		sheet.setZoom(3, 4);

		sheet.autoSizeColumn(1);

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	public static ByteArrayOutputStream generateXLSCartePointage4CopagReport(List<UserReportData> data,
			String grandTitle, List<String> titles) throws Exception {

		Workbook wb = new XSSFWorkbook();
		Map<String, CellStyle> styles = createStyles(wb);
		titles = new ArrayList<String>();
		// Jour
		titles.add(MessageFactory.getMessage("BOR_E001_L1"));
		// HNT
		titles.add(MessageFactory.getMessage("BOR_E001_L5"));
		// HS25
		titles.add(MessageFactory.getMessage("BOR_E001_L11") + TAUX1);
		// HS50
		titles.add(MessageFactory.getMessage("BOR_E001_L12") + TAUX2);
		Sheet sheet;
		String userTitle;
		for (UserReportData userData : data) {
			userTitle = MessageFactory.getMessage("USR_E000_label13") + " :" + userData.getUser().getMatricule() + " "
					+ MessageFactory.getMessage("USR_E000_label1") + " :" + userData.getUser().getNom() + " "
					+ MessageFactory.getMessage("USR_E000_label2") + " :" + userData.getUser().getPrenom() + "      "
					+ grandTitle;
			sheet = createSheet4Carte("Carte N�" + userData.getUser().getMatricule(), wb, styles, userTitle, titles);

			Row row;

			int rownum = 3;
			int rownum2 = 3;

			String styleName2_t = "cell_b_centered";

			Cell cell;
			DayData qz1 = new DayData();
			DayData qz2 = new DayData();
			int thisday = userData.getDayDatas().get(0).getDateCal().get(Calendar.DAY_OF_MONTH);
			int thismonth = userData.getDayDatas().get(0).getDateCal().get(Calendar.MONTH);
			int dy = 0;
			int mth;
			int j = 0;
			for (DayData dyData : userData.getDayDatas()) {
				if (dyData.getType().equalsIgnoreCase("TOTAL"))
					continue;
				dy = dyData.getDateCal().get(Calendar.DAY_OF_MONTH);
				mth = dyData.getDateCal().get(Calendar.MONTH);
				if ((thisday == 1 && dy <= 15) || (thisday == 16 && thismonth == mth)) {
					row = sheet.createRow(rownum);
					cell = row.createCell(0);
					cell.setCellValue(Utils.convertDate2Str(dyData.getDate(), "dd"));
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(1);
					cell.setCellValue(dyData.getCol2cop());
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(2);
					cell.setCellValue(dyData.getCol10());
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(3);
					cell.setCellValue(dyData.getCol11());
					cell.setCellStyle(styles.get(styleName2_t));

					ma.nawarit.checker.common.Utils.addToSum(qz1, dyData);
					rownum++;
				} else if ((thisday == 1 && dy > 15) || (thisday == 16 && thismonth != mth)) {
					row = sheet.getRow(rownum2);
					if (row == null)
						row = sheet.createRow(rownum2);
					cell = row.createCell(5);
					cell.setCellValue(Utils.convertDate2Str(dyData.getDate(), "dd"));
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(6);
					cell.setCellValue(dyData.getCol2cop());
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(7);
					cell.setCellValue(dyData.getCol10());
					cell.setCellStyle(styles.get(styleName2_t));

					cell = row.createCell(8);
					cell.setCellValue(dyData.getCol11());
					cell.setCellStyle(styles.get(styleName2_t));

					ma.nawarit.checker.common.Utils.addToSum(qz2, dyData);
					rownum2++;
				}
			}

			row = sheet.getRow(rownum);
			if (row == null)
				row = sheet.createRow(rownum);
			cell = row.createCell(1);
			cell.setCellValue(qz1.getCol2cop());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(2);
			cell.setCellValue(qz1.getCol10());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(3);
			cell.setCellValue(qz1.getCol11());
			cell.setCellStyle(styles.get(styleName2_t));

			row = sheet.getRow(rownum2);
			if (row == null)
				row = sheet.createRow(rownum2);
			cell = row.createCell(6);
			cell.setCellValue(qz2.getCol2cop());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(7);
			cell.setCellValue(qz2.getCol10());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(8);
			cell.setCellValue(qz2.getCol11());
			cell.setCellStyle(styles.get(styleName2_t));

			rownum++;
			rownum++;
			row = sheet.createRow(rownum);
			cell = row.createCell(3);
			cell.setCellValue(userData.getTcol2cop());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(4);
			cell.setCellValue(userData.getTcol10());
			cell.setCellStyle(styles.get(styleName2_t));

			cell = row.createCell(5);
			cell.setCellValue(userData.getTcol11());
			cell.setCellStyle(styles.get(styleName2_t));

			sheet.setZoom(3, 4);

			sheet.autoSizeColumn(1);
		}

		// Write the output
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		BufferedOutputStream out = new BufferedOutputStream(result);
		wb.write(out);
		out.close();
		return result;
	}

	private static Sheet createSheet4Carte(String sheetName, Workbook wb, Map<String, CellStyle> styles,
			String grandTitle, List<String> titles) {

		Sheet sheet = wb.createSheet(sheetName);

		// turn off gridlines
		sheet.setDisplayGridlines(false);
		sheet.setPrintGridlines(false);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);

		// title row
		Row titleRow = sheet.createRow(0);
		titleRow.setHeightInPoints(45);
		Cell titleCell = titleRow.createCell(0);
		titleCell.setCellValue(grandTitle);
		titleCell.setCellStyle(styles.get("title"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));

		// the header row: centered text in 48pt font
		Row headerRow = sheet.createRow(2);
		headerRow.setHeightInPoints(12.75f);
		int i = 0;
		for (String t : titles) {
			if (t == null)
				continue;
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(t);
			cell.setCellStyle(styles.get("header"));
			sheet.setColumnWidth(i, 256 * (t.length() + 6));
			i++;
		}

		i++;
		for (String t : titles) {
			if (t == null)
				continue;
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(t);
			cell.setCellStyle(styles.get("header"));
			sheet.setColumnWidth(i, 256 * (t.length() + 6));
			i++;
		}

		// freeze the first row
		sheet.createFreezePane(0, 1);
		sheet.createFreezePane(0, 3);
		return sheet;
	}

	private static Sheet createSheet(String sheetName, Workbook wb, Map<String, CellStyle> styles, String grandTitle,
			List<String> titles) {

		Sheet sheet = wb.createSheet(sheetName);

		// turn off gridlines
		sheet.setDisplayGridlines(false);
		sheet.setPrintGridlines(false);
		sheet.setFitToPage(true);
		sheet.setHorizontallyCenter(true);
		PrintSetup printSetup = sheet.getPrintSetup();
		printSetup.setLandscape(true);

		// title row
		Row titleRow = sheet.createRow(0);
		titleRow.setHeightInPoints(45);
		Cell titleCell = titleRow.createCell(0);
		titleCell.setCellValue(grandTitle);
		titleCell.setCellStyle(styles.get("title"));
		sheet.addMergedRegion(CellRangeAddress.valueOf("$A$1:$L$1"));

		// the header row: centered text in 48pt font
		Row headerRow = sheet.createRow(2);
		headerRow.setHeightInPoints(12.75f);
		int i = 0;
		titles = new ArrayList<String>();
		titles.add("");
		titles.add("");
		titles.add("");
		titles.add("HN");
		titles.add("HNT");
		titles.add("HT");
		titles.add("JT");
		if(titles != null)
		for (String t : titles) {
			if (t == null)
				continue;
			Cell cell = headerRow.createCell(i);
			cell.setCellValue(t);
			cell.setCellStyle(styles.get("header"));
			sheet.setColumnWidth(i, 256 * (t.length() + 6));
			i++;
		}

		// freeze the first row
		sheet.createFreezePane(0, 1);
		sheet.createFreezePane(0, 3);
		return sheet;
	}

	/**
	 * create a library of cell styles
	 */
	public static Map<String, CellStyle> createStyles(Workbook wb) {
		Map<String, CellStyle> styles = new HashMap<String, CellStyle>();
		DataFormat df = wb.createDataFormat();

		CellStyle style;
		Font titleFont = wb.createFont();
		titleFont.setFontHeightInPoints((short) 18);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = wb.createCellStyle();
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFont(titleFont);
		styles.put("title", style);

		Font headerFont = wb.createFont();
		headerFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		styles.put("header", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setFont(headerFont);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("header_date", style);

		Font font1 = wb.createFont();
		font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font1);
		styles.put("cell_b", style);

		Font fontTotal = wb.createFont();
		font1.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		style.setFont(fontTotal);
		styles.put("cell_b_t", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(font1);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
		styles.put("cell_b_centered", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setFont(font1);
		style.setDataFormat(HSSFDataFormat.getBuiltinFormat("0.00"));
		style.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		styles.put("cell_b_centered_t", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setFont(font1);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_b_date", style);

		style = createBorderedStyle(wb);
		style.setWrapText(true);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		style.setDataFormat(df.getFormat("hh:mm"));
		styles.put("cell_b_hour", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setFont(font1);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_g", style);

		Font font2 = wb.createFont();
		font2.setColor(IndexedColors.BLUE.getIndex());
		font2.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font2);
		styles.put("cell_bb", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setFont(font1);
		style.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_bg", style);

		Font font3 = wb.createFont();
		font3.setFontHeightInPoints((short) 14);
		font3.setColor(IndexedColors.DARK_BLUE.getIndex());
		font3.setBoldweight(Font.BOLDWEIGHT_BOLD);
		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setFont(font3);
		style.setWrapText(true);
		styles.put("cell_h", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setWrapText(true);
		styles.put("cell_normal", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		style.setWrapText(true);
		styles.put("cell_normal_centered", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_RIGHT);
		style.setVerticalAlignment(CellStyle.VERTICAL_TOP);
		style.setWrapText(true);
		style.setDataFormat(df.getFormat("d-mmm"));
		styles.put("cell_normal_date", style);

		style = createBorderedStyle(wb);
		style.setAlignment(CellStyle.ALIGN_LEFT);
		style.setIndention((short) 1);
		style.setWrapText(true);
		styles.put("cell_indented", style);

		style = createBorderedStyle(wb);
		style.setFillForegroundColor(IndexedColors.BLUE.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		styles.put("cell_blue", style);

		return styles;
	}

	private static CellStyle createBorderedStyle(Workbook wb) {
		CellStyle style = wb.createCellStyle();
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setRightBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setTopBorderColor(IndexedColors.BLACK.getIndex());
		return style;
	}
}