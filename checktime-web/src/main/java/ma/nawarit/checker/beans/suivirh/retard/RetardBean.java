package ma.nawarit.checker.beans.suivirh.retard;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import ma.nawarit.checker.beans.compagnie.collabo.UserBean;
import ma.nawarit.checker.beans.compagnie.collabo.UserDataModel;
import ma.nawarit.checker.compagnie.Group;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.GroupManageableService;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableServiceBase;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.engine.LiveProcessingService;
import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.equipement.crud.TerminalManageableService;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.RetardImpl;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.syncRH.databaseOperations;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import org.andromda.spring.CommonCriteria;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.TreeNode;


@ManagedBean(name = "retardBean")
@ViewScoped
public class RetardBean  implements   Serializable {

	@ManagedProperty(value = "#{NoeudManageableService}")
	private transient NoeudManageableService noeudService;

	@ManagedProperty(value = "#{RetardManageableService}")
	private RetardManageableService retardServise;

	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService = new UserManageableServiceBase();

	@ManagedProperty(value = "#{GroupManageableService}")
	private transient GroupManageableService groupService;

	@ManagedProperty(value = "#{MouvementManageableService}")
	private MouvementManageableService mouvementService;

	@ManagedProperty(value = "#{UnitManageableService}")
	private transient UnitManageableService uniteService;

	@ManagedProperty(value = "#{TerminalManageableService}")
	private TerminalManageableService terminalService;
	
	@ManagedProperty(value = "#{LiveProcessingService}")
	private LiveProcessingService processorService;

	

	private List<Retard> retards = new ArrayList<Retard>();

	public UnitManageableService getUniteService() {
		return uniteService;
	}

	public void setUniteService(UnitManageableService uniteService) {
		this.uniteService = uniteService;
	}

	public TerminalManageableService getTerminalService() {
		return terminalService;
	}

	public void setTerminalService(TerminalManageableService terminalService) {
		this.terminalService = terminalService;
	}

	private Retard retard;
	private CommonCriteria criteria;
	private String nom;
	private User user = new UserImpl();
	private User selectedUser ;
	private List<User> users;
	private List<User> selectedUsers;
	private Retard current;
	private Retard detailedRetard;
	
	

	private UserDataModel dataModel;
	private TabView tabView;

	public RetardBean() throws Exception {
		criteria = new CommonCriteria();
		users = new ArrayList<User>();
		retard = new RetardImpl();
		pointage = new ArrayList<Mouvement>();
		retardsList = new ArrayList<Retard>();
		allRetards = true;
		duree = "15";
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		usrAuthenticated = (User)obj;
		reCalcul=false;
		selectedUsers = new ArrayList<User>();
		selectedRetards  =  new   ArrayList<Retard>();
	}

	/***
	 * Code of the Retard module
	 * 
	 */
	private TreeNode selectedNode;
	private TreeNode selectedUnit;
	private Group group;
	private Noeud noeud;
	private Date dateDebut;
	private Date dateFin;
	private Unit unit;
	private String statut;
	private String duree;
	private List<Mouvement> pointage;
	private List<Retard> retardsList;
	private List<Retard> filtredRetards;
	public DateFormat Dateformat = new SimpleDateFormat("yyyy-MM-dd");

	private   List<Group>  groups;
	private FacesContext context = FacesContext.getCurrentInstance();
	private User usrAuthenticated;
	private Boolean allRetards;
	private Boolean reCalcul;

	private UserImpl manager;

	private ProfilMetier profilMetier;

	private int roleId;

	private boolean inTheGroup;

	private List<Retard> selectedRetards;

	public List<Retard> getSelectedRetards() {
		return selectedRetards;
	}

	public void setSelectedRetards(List<Retard> selectedRetards) {
		this.selectedRetards = selectedRetards;
	}

	//handle when organigramme changed
	public void handleNodeSelection() {
		if (selectedNode != null && selectedNode.getData() != null) {
			noeud = (Noeud) selectedNode.getData();
		}
	}

	//unit unit selection
	public void handleUnitNodeSelection() {
		if (selectedNode != null && selectedNode.getData() != null) {
			unit = (Unit) selectedNode.getData();
		}
	}

	//when group value changed
	public void groupValueChangeListener(ValueChangeEvent e) throws Exception {
		  if (e.getNewValue() == e.getOldValue()) { ((SelectOneListbox)
		  e.getComponent()).setValue(null);
		  RequestContext.getCurrentInstance().update("@form");
		  this.criteria = new CommonCriteria(); this.criteria.setChanged(true); return;
		  }
		this.group = this.groupService.load(Integer.parseInt((String) e.getNewValue()));
		if (this.group != null && this.selectedRetards != null) {
		    retardsList  =   retardServise.readByCriteria(this.criteria);
		    List<User>   selectedUsers   =  new   ArrayList<User>();
		  for (Retard rtr : retardsList) {
			  selectedUsers.add(rtr.getUser());
		}
			boolean inTheGroup = false;
			for (User u : selectedUsers)
				if (this.group.getUsers().contains(u)) {
					inTheGroup = true;
					break;
				}
			if (inTheGroup)
				this.inTheGroup = true;
			else
				this.inTheGroup = false;
		}
		criteria.setIndexs(new ArrayList<Integer>());
		for (User u : group.getUsers())
			criteria.getIndexs().add(u.getId());
		this.criteria.setChanged(true);

	}

		
		
	

	// when a retard selected
	@SuppressWarnings("unchecked")
	public void onRetardSelected(SelectEvent event) throws Exception {
		retard = (Retard) event.getObject();
		if (retard != null) {
			CommonCriteria cri = new CommonCriteria();
			cri.setDateDebut(retard.getDate());
			cri.setDateFin(retard.getDate());
			cri.setMatriculeInf(retard.getUser().getMatricule());
			cri.setMatriculeSup(retard.getUser().getMatricule());
			List<Mouvement> mouvements = mouvementService.readByCriteria(cri);

			if (mouvements != null) {
				for (int i = 0; i < mouvements.size(); i++) {
					List<Terminal> terminals = terminalService.loadTerminalsByCode(mouvements.get(i).getTerminalCode());
					Mouvement mvn = new MouvementImpl();
					mvn.setDate(mouvements.get(i).getDate());
					mvn.setBadge(mouvements.get(i).getBadge());
					mvn.setId(mouvements.get(i).getId());
					mvn.setTypeMouvement(mouvements.get(i).getTypeMouvement());
					if (terminals != null) {
						Terminal ter = (Terminal) terminals.get(0);
						mvn.setTerminalCode(uniteService.load(ter.getUnit().getId()).getLibelle());
					}
					pointage.add(mvn);
				}
			}
		}
	}

	// apply filter
	@SuppressWarnings("unchecked")
	public void applyFilter() throws Exception {
		Date d=new Date();
		retardsList=new ArrayList<Retard>();
		
		if (noeud != null)
			criteria.setNoeudId(String.valueOf(noeud.getId()));

		if (group != null)
			criteria.setGroup(String.valueOf(group.getId()));

		if (unit != null)
			criteria.setSiteId(String.valueOf(unit.getId()));
			
		criteria.setDurre(duree);
		if (dateFin != null && dateDebut != null) {
			if (dateFin.compareTo(dateDebut) >= 0) {
				Calendar c = Calendar.getInstance();
				c.setTime(dateDebut);
				c.add(Calendar.DAY_OF_MONTH, 1);
				criteria.setDateDebut(c.getTime());
				c.setTime(dateFin);
				c.add(Calendar.DAY_OF_MONTH, 1);
				criteria.setDateFin(c.getTime());
				
				//perform calcule
				if(reCalcul==true)
					processAction(d);
				
			} else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Erreur!", "Date début est supérieur à date fin");
				FacesContext.getCurrentInstance().addMessage(null, message);
				retardsList=new ArrayList<Retard>();
				filtredRetards=new ArrayList<Retard>();
				return;
			}
		}else {
			if(reCalcul==true)
				processAction(d);
		}
		criteria.setStatut(statut);
		retardsList = retardServise.readByAdvancedCriteria(criteria);
		allRetards = false;
	}

	//generate retard report
	public void generateRetardReport(String type) throws Exception {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		// logo ressources
		String WEBDIR = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/");
		// add parameters
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("WEBDIR", WEBDIR);
		parameters.put("DATE_DEBUT", Utils.convertDate2Str(dateDebut, "yyyy-MM-dd"));
		parameters.put("DATE_FIN", Utils.convertDate2Str(dateFin, "yyyy-MM-dd"));
		if (noeud != null)
			parameters.put("NOEUD_ID", String.valueOf(noeud.getId()));

		if (group != null)
			parameters.put("GROUP_ID", String.valueOf(noeud.getId()));

		if (unit != null)
			parameters.put("UNIT_ID", String.valueOf(unit.getId()));
		
		if(duree !=null)
			parameters.put("DUREE", String.valueOf(duree));
		
		String jrxmlFile = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/jasperreports/retards.jasper");
		File file = new File(jrxmlFile);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
				databaseOperations.getConnection());
		if(type.equalsIgnoreCase("xls")) {
			JRXlsExporter exporterXLS = new JRXlsExporter();
			 exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
			 exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(ec.getResponseOutputStream()));
			 SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration(); 
			 configuration.setOnePagePerSheet(false);
			 configuration.setDetectCellType(false);
			 configuration.setCollapseRowSpan(false);
			 exporterXLS.setConfiguration(configuration);
			 exporterXLS.exportReport();
			 ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Retards "+Utils.convertDate2Str(Calendar.getInstance().getTime(), "dd-MM-yyyy hhmmss")+".xls")));
			 ec.setResponseHeader("Content-Disposition", "attachment; filename=Retards"+Utils.convertDate2Str(Calendar.getInstance().getTime(), "dd-MM-yyyy hhmmss")+".xls"); 
			 fc.responseComplete();
		}else {
			ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Retards"+Utils.convertDate2Str(Calendar.getInstance().getTime(), "dd-MM-yyyy hhmmss")+".pdf")));
			ec.setResponseHeader("Content-Disposition", "attachment; filename=Retards"+Utils.convertDate2Str(Calendar.getInstance().getTime(), "dd-MM-yyyy hhmmss")+".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, ec.getResponseOutputStream());
			fc.responseComplete();
		}
	}
	
	
	//////://////////////////////////////////////////////////////////////////////////////////////////
	public void retardValidated() throws Exception {
		
		if(this.selectedRetards!=null  ) {
			if(this.selectedRetards.size()>0) {
				for (Retard retard : this.selectedRetards) {
					retard.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					retardServise.update(retard);
					
				}
				
			}
		}
	}
	
	
	
	
	

	////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//get statut
	public String convertStatut(String statut) {
		String stt="";
		if(statut.equals(Globals.WORKFLOW_STATUS_VALIDATE))
			stt="Autoris\u00e9";
		else
			stt="Non autoris\u00e9";
		return stt;
	}
	
	//method that update a retard
	public void updateRetardListner(RowEditEvent event) throws Exception {
		Retard entity = ((Retard) event.getObject());
		
		if (entity.getId() > 0) {
			if(entity.getStatut().equals(Globals.WORKFLOW_STATUS_ELAPSED))
				entity.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			retardServise.update(entity);
		}
	}
	
	
	//lunch the calcule for today
	public void processAction(Date date) {
		CommonCriteria crit=new CommonCriteria();
		crit.setDateDebut(date);
		crit.setDateFin(date);
		crit.setTypeReport("RECL");
		if (usrAuthenticated != null && usrAuthenticated.getId() > 1)
			crit.setSuppHrhId(usrAuthenticated.getId() + "");
		processorService.process(crit, null);
	}
	
	/**
	 * Getters and setters
	 */
	public NoeudManageableService getNoeudService() {
		return noeudService;
	}

	public void setNoeudService(NoeudManageableService noeudService) {
		this.noeudService = noeudService;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getSelectedUnit() {
		return selectedUnit;
	}

	public void setSelectedUnit(TreeNode selectedUnit) {
		this.selectedUnit = selectedUnit;
	}

	/***
	 * Finnish of the code
	 */

	public void intializRetardListener(ActionEvent e) {
		this.retard = new RetardImpl();
	}

	public void chooseUser() {
		selectedUser = new UserImpl();
	}

	public void saveRetard() throws Exception {

		retardServise.create(retard);
	}

	public void onSelectUser() {
		if (nom != null && nom != "") {
			selectedUser = readByName(nom);
		}
	}

	public void onRowToggleListener(User user) {
		if (user.getRetards() != null && !user.getRetards().isEmpty())
			while (user.getRetards().iterator().hasNext())
				System.out.println(((Retard) user.getRetards().iterator().next()).getDescription());
	}

	@SuppressWarnings("unchecked")
	public User readByName(String nom) {

		List<User> list = new ArrayList<User>();
		try {
			if (list == null || list.isEmpty()) {
				Hashtable<String, String> props = new Hashtable<String, String>();
				props.put("nom", nom);
				list = userService.read(props);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (list.isEmpty()) {
			return null;
		} else
			return list.get(0);
	}

	@SuppressWarnings("unchecked")
	public List<Retard> getRetards() {
		try {
			if (retards == null || retards.isEmpty())
				retards = retardServise.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retards;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		try {
			if (users == null || users.isEmpty())
				users = userService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public RetardManageableService getRetardServise() {
		return retardServise;
	}

	public void setRetardServise(RetardManageableService retardServise) {
		this.retardServise = retardServise;
	}

	public void setRetards(List<Retard> retards) {
		this.retards = retards;
	}

	public Retard getRetard() {
		return retard;
	}

	public void setRetard(Retard retard) {
		this.retard = retard;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}


	public User getUser() {
		if (this.users != null && this.users.size() == 1 && this.user == null)
			this.user = this.users.get(0);
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public Retard getCurrent() {
		return current;
	}

	public void setCurrent(Retard current) {
		this.current = current;
	}

	public Retard getDetailedRetard() {
		return detailedRetard;
	}

	public void setDetailedRetard(Retard detailedRetard) {
		this.detailedRetard = detailedRetard;
	}

	public GroupManageableService getGroupService() {
		return groupService;
	}

	public void setGroupService(GroupManageableService groupService) {
		this.groupService = groupService;
	}

	public List<Group> getGroups() throws Exception {
		if (groups == null || groups.isEmpty()) {
			groups = this.groupService.readAll();
		}
		return groups;
	}
	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public Noeud getNoeud() {
		return noeud;
	}

	public void setNoeud(Noeud noeud) {
		this.noeud = noeud;
	}

	public Date getDateDebut() {
		if(dateDebut==null)
			this.dateDebut=Calendar.getInstance().getTime();
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		if(dateFin==null)
			this.dateFin=Calendar.getInstance().getTime();
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public String getDuree() {
		return duree;
	}

	public void setDuree(String duree) {
		this.duree = duree;
	}

	public MouvementManageableService getMouvementService() {
		return mouvementService;
	}

	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}

	public List<Mouvement> getPointage() {
		return pointage;
	}

	public void setPointage(List<Mouvement> pointage) {
		this.pointage = pointage;
	}

	public Boolean getAllRetards() {
		return allRetards;
	}

	public void setAllRetards(Boolean allRetards) {
		this.allRetards = allRetards;
	}

	public List<Retard> getFiltredRetards() {
		return filtredRetards;
	}

	public void setFiltredRetards(List<Retard> filtredRetards) {
		this.filtredRetards = filtredRetards;
	}

	public List<Retard> getRetardsList() throws Exception {
		
		return retardsList;
	}

	public void setRetardsList(List<Retard> retardsList) {
		this.retardsList = retardsList;
	}

	public void setProcessorService(LiveProcessingService processorService) {
		this.processorService = processorService;
	}

	public Boolean getReCalcul() {
		return reCalcul;
	}

	public void setReCalcul(Boolean reCalcul) {

		this.reCalcul = reCalcul;
	}


	
}