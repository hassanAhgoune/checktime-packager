package ma.nawarit.checker.beans.configuration.profilMetier;

import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.ProfilMetierImpl;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableServiceBase;
import org.primefaces.event.RowEditEvent;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "profilMetierBean")
@SessionScoped
public class ProfilMetierBean {
    @ManagedProperty(value = "#{ProfilMetierManageableService}")
    private ProfilMetierManageableService profilMetierService = new ProfilMetierManageableServiceBase();

    private List<ProfilMetier> profilMetiers = new ArrayList<ProfilMetier>();
    private ProfilMetier profilMetier = new ProfilMetierImpl();
    private ProfilMetier selectedProfilMetier;
    FacesContext context;

    public ProfilMetierBean() {

        context = FacesContext.getCurrentInstance();
    }

    public void saveProfilMetier() throws Exception {
        if (profilMetier != null && profilMetier.getLibelle() != null && profilMetier.getCode() != null) {
            profilMetierService.create(profilMetier);
            profilMetiers.clear();
            profilMetier = new ProfilMetierImpl();
        }
    }

    public void cleanProfilMetier() {
        profilMetier = new ProfilMetierImpl();
    }

    public void updateProfilMetier(RowEditEvent event) throws Exception{
        profilMetier = ((ProfilMetier)event.getObject());
        System.out.println(profilMetier.getLibelle());
        profilMetierService.update(profilMetier);
    }

    public void selectedDeleteProfilMetier(RowEditEvent event) throws Exception{

        selectedProfilMetier= ((ProfilMetier) event.getObject());
    }

    public void deleteProfilMetier() throws Exception{

        if(selectedProfilMetier!= null){
            profilMetierService.delete(selectedProfilMetier);
            profilMetiers.clear();
            selectedProfilMetier = new ProfilMetierImpl();
        }
    }

    public ProfilMetierManageableService getProfilMetierService() {
        return profilMetierService;
    }

    public void setProfilMetierService(ProfilMetierManageableService profilMetierService) {
        this.profilMetierService = profilMetierService;
    }

    public List<ProfilMetier> getProfilMetiers() {

        try {
            if (profilMetiers == null || profilMetiers.isEmpty()) {
                profilMetiers = profilMetierService.readAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return profilMetiers;
    }

    public void setProfilMetiers(List<ProfilMetier> profilMetiers) {
        this.profilMetiers = profilMetiers;
    }

    public FacesContext getContext() {
        return context;
    }

    public void setContext(FacesContext context) {
        this.context = context;
    }

    public ProfilMetier getProfilMetier() {
        return profilMetier;
    }

    public void setProfilMetier(ProfilMetier profilMetier) {
        this.profilMetier = profilMetier;
    }

    public ProfilMetier getSelectedProfilMetier() {
        return selectedProfilMetier;
    }

    public void setSelectedProfilMetier(ProfilMetier selectedProfilMetier) {
        this.selectedProfilMetier = selectedProfilMetier;
    }
}
