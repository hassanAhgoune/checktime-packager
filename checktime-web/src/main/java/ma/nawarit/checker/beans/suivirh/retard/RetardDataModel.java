package ma.nawarit.checker.beans.suivirh.retard;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.model.DataModelListener;

import org.primefaces.component.chart.Chart;
import org.primefaces.context.RequestContext;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortMeta;
import org.primefaces.model.SortOrder;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.BarChartModel;
import org.primefaces.model.chart.LineChartSeries;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.suivi.Retard;

/**
 * Dummy implementation of LazyDataModel that uses a list to mimic a real
 * datasource like a database.
 */
public class RetardDataModel extends LazyDataModel<Retard> {

	private List<Retard> datasource;
	private List<Retard> data;
    public RetardDataModel(List<Retard> datasource) {
        this.datasource = datasource;
    }

    @Override
    public Retard getRowData(String rowKey) {
        for (Retard retard : datasource) {
            if (retard.getId()== Integer.parseInt(rowKey)) {
                return retard;
            }
        }

        return null;
    }

    @Override
    public Object getRowKey(Retard Retard) {
        return Retard.getId();
    }

    @Override
    public List<Retard> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String,Object> filters) {
        data = new ArrayList<Retard>();
        if (datasource != null)
	        for(Retard user : datasource) {
	            boolean match = true;
	            if (filters != null) {
	                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
	                    try {
	                        String filterProperty = it.next();
	                        Object filterValue = filters.get(filterProperty);
	                        String fieldValue = "";
	                        Field field;
	                        if (filterProperty.contains(".")) {
	                        	user = Utils.deproxy(user, Retard.class);
	                        	field = user.getClass().getSuperclass().getDeclaredField(filterProperty.substring(0, filterProperty.indexOf('.')));
	                            field.setAccessible(true);
	                            Object o = field.get(user);
	                            if (o != null) {
		                            if (o instanceof Unit) {
		                            	Unit u = (Unit)o;
		                            	field = u.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(u));
		                            } else if (o instanceof ProfilMetier) {
		                            	ProfilMetier p = (ProfilMetier)o;
		                            	p = Utils.deproxy(p, ProfilMetier.class);
		                            	field = p.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(p));
		                            } else if (o instanceof Planning) {
		                            	Planning p = (Planning)o;
		                            	p = Utils.deproxy(p, Planning.class);
		                            	field = p.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(p));
		                            } else if (o instanceof Noeud) {
		                            	Noeud n = (Noeud)o;
		                            	n = Utils.deproxy(n, Noeud.class);
		                            	field = n.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(n));
		                            }
	                            }
	                        } else {
		                        field = user.getClass().getSuperclass().getDeclaredField(filterProperty);
		                        field.setAccessible(true);
		                        fieldValue = String.valueOf(field.get(user));
	                        }
	                        if (filterValue != null && filterValue.toString().contains("/")) {
	                        	String[] filterValues = ((String)filterValue).split("/");
	                        	match = false;
	                        	for (int i = 0; i < filterValues.length; i++) {
	                        		System.out.println(filterValues[i].toString());
	                        		if(fieldValue.toLowerCase().contains(filterValues[i].toLowerCase())) {
			                            match = true;
			                            break;
	                        		} 
	                        	}
	                        } else if(filterValue == null || fieldValue.toLowerCase().contains(filterValue.toString().toLowerCase())) {
		                            match = true;
		                    } else {
	                            match = false;
	                            break;
	                        }
	                    } catch(Exception e) {
	                    	System.out.println(user.getMatricule());
	                    	e.printStackTrace();
	                        match = false;
	                    }
	                }
	            }
	 
	            if(match) {
	                data.add(user);
	            }
	        }

        //sort
       

        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);

        //paginate
        if (dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch (IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
	
}
