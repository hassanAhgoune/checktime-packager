package ma.nawarit.checker.beans.share;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;

import ma.nawarit.checker.beans.compagnie.collabo.UserDataModel;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilApp;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.ProfilMetierImpl;
import ma.nawarit.checker.compagnie.Role;
import ma.nawarit.checker.compagnie.RoleImpl;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilAppManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.synchronizer.utils.AbsenceOperator;
import ma.nawarit.checker.synchronizer.utils.CongeOperator;
import ma.nawarit.checker.synchronizer.utils.FileOperator;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;
import ma.nawarit.checker.utils.UploadUtils;
import ma.nawarit.checker.utils.XlsReader;
import ma.nawarit.checker.utils.XmlReader;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;

import org.jasypt.spring.security.PasswordEncoder;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import ma.nawarit.checker.utils.ImportModel;



@ManagedBean
@ViewScoped
public class ImportBean {
	
	private UploadedFile uploadFile;
	private Collection<UserImpl> users;
	private Collection<ProfilMetier> profils;
	private Collection<MouvementImpl> mvmnts;
	private UserDataModel dataModel;
	private List<User> selectedUsers;
	private int i = 1;
	
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;
	
	@ManagedProperty(value = "#{ProfilMetierManageableService}")
	private ProfilMetierManageableService profilMetierService;
	
	@ManagedProperty(value = "#{ProfilAppManageableService}")
	private ProfilAppManageableService profilAppService;
	
	@ManagedProperty(value = "#{NoeudManageableService}")
	private NoeudManageableService noeudService;
	
	@ManagedProperty(value = "#{PlanningManageableService}")
	private PlanningManageableService planningService;
	
	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;
	
	@ManagedProperty(value = "#{MouvementManageableService}")
	private MouvementManageableService mouvementService;
	
	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService;
	
	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService;
	
	protected FacesContext context = FacesContext.getCurrentInstance();
	
	@ManagedProperty(value = "#{passwordEncoder}")
	private PasswordEncoder passwordEncoder;
	
	private UploadedFile uplodedFile;
	
	private List<ImportModel> importModels;
	
	private String importType;
	
	public String getImportType() {
		return importType;
	}

	public void setImportType(String importType) {
		this.importType = importType;
	}

	public ImportBean() {
		PropertiesReader.loadProperties("syncRH.properties");
		importType="ABS";
	}
	
	public List<ImportModel> getImportModels() {
		return importModels;
	}

	public void setImportModels(List<ImportModel> importModels) {
		this.importModels = importModels;
	}

	//upload method
	public void handleFileUpload(FileUploadEvent event) throws IOException, ParseException {
		importModels=new ArrayList<ImportModel>();
		uploadFile=event.getFile();
		String type=importType.equals("ABS")?"absence":"conge";
		List<String> lines =FileOperator.getLines(uploadFile.getInputstream(), PropertiesReader.getProperty(type+".lineformat"));
		for (String line : lines) {
			importModels.add(convertLineToObject(line));
		}
    }
	
	public void importData() {
		int nbrAbs=0;
		int nbrCng=0;
		int nbrRtr=0;
		if(importType.equals("ABS")) {
			for (ImportModel model : importModels) {
				int c=AbsenceOperator.saveAbsence(model);
				nbrAbs+=(c==1?1:0);
				nbrRtr+=(c==-1?1:0);
			}
		}else if(importType.equals("CNG")) {
			for (ImportModel model : importModels) {
				nbrCng+=CongeOperator.saveConge(model);
			}
		}
		System.out.println("Nombre Conge est : "+nbrCng);
		System.out.println("Nombre Retard est : "+nbrRtr);
		System.out.println("Nombre Absence est : "+nbrAbs);
	}
	
	private ImportModel convertLineToObject(String line) throws ParseException {
		DateFormat dateFormat = new SimpleDateFormat(PropertiesReader.getProperty("dateformat"));
		String[] cells = line.split(";");
		Date date = dateFormat.parse(cells[0]);
		Double duree = Double.valueOf(cells[1].trim().replace(',', '.'));
		String matricule = cells[2].trim();
		String motif = cells[3].trim();
		return new ImportModel(date,matricule,duree,motif);
	}

	public void uploadUserFileListener(org.primefaces.event.FileUploadEvent fileUploadEvent){
		try {
			this.users = XlsReader.extractUserDataXLSX(fileUploadEvent.getFile().getInputstream());
			Map metiers = profilMetierService.profilsByCode();
			Map noeuds = noeudService.noeudsByCode();
			Map units = unitService.unitsByCode();
			if(this.users != null && this.users.size() > 0)
				for (User user: users) {
					user.setDroitPaie( true);
					user.setActive( true);
					if (metiers.containsKey(((UserImpl)user).getPositionCode())) {
						user.setProfilMetier((ProfilMetier) metiers.get(((UserImpl)user).getPositionCode()));
					}else
						user.setProfilMetier(null);
					if (units.containsKey(((UserImpl)user).getSiteCode())) {
						user.setUnit((Unit) units.get(((UserImpl)user).getSiteCode()));
					}else
						user.setUnit(null);
					if (noeuds.containsKey(((UserImpl)user).getZoneCode())) {
						user.setNoeud((Noeud) noeuds.get(((UserImpl)user).getZoneCode()));
					}else
						user.setNoeud(null);
					try {
						this.userService.create(user);
					}catch (Exception e){
						throw e;
					}

				}
			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("collabotabsId:impuserform");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public void uploadUnitFileListener(org.primefaces.event.FileUploadEvent fileUploadEvent){
		try {
			List<Unit> units = XlsReader.extractUnitDataXLSX(fileUploadEvent.getFile().getInputstream());
			for (Unit unit : units) {
				try {
					unitService.create(unit);
				} catch (Exception e) {
					throw e;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void uploadNoeudFileListener(org.primefaces.event.FileUploadEvent fileUploadEvent){
		try {
			List<Noeud> noeuds = XlsReader.extractNoeudDataXLSX(fileUploadEvent.getFile().getInputstream());

			for (Noeud noeud : noeuds) {
				try {
					noeudService.create(noeud);
				} catch (Exception e) {
					throw e;
				}
			}
			Map map = noeudService.noeudsByCode();
			for (Noeud noeud : noeuds) {
				if ("".equals(noeud.getParentCode()) || noeud.getParentCode() == null)
					continue;
				if (map.containsKey(noeud.getParentCode())) {
					noeud.setNoeud((Noeud) map.get(noeud.getParentCode()));
					noeudService.update(noeud);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void uploadProfilMetierFileListener(org.primefaces.event.FileUploadEvent fileUploadEvent){
		try {
			List<ProfilMetier> profilMetiers = XlsReader.extractProfilDataXLSX(fileUploadEvent.getFile().getInputstream());
			for (ProfilMetier profilMetier : profilMetiers) {
				try {
					profilMetierService.create(profilMetier);
				} catch (Exception e) {
					throw e;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private User findUser(String matricule){
		Iterator<UserImpl> it = this.users.iterator();
		User currentUser;
		while (it.hasNext()) {
			currentUser = it.next();
			if(matricule.equals(currentUser.getMatricule()))
				return currentUser;
		}
		return null;
	}
	
	public  String affectUser2SuphAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload/user-uploadedFile";
			
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
							
				if("application/vnd.ms-excel".equals(contentType) 
						|| "application/xls".equals(contentType) 
						|| ":application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)){
					file += ".xls";
					UploadUtils.uploadFile(bis, file);
					List<UserImpl> usersXls = XlsReader.updateUserData(file);
					Iterator<UserImpl> it = usersXls.iterator();
					UserImpl currentUser;
					UserImpl tmpUser;
					System.out.println("users size :: " + usersXls.size());
					this.users = new ArrayList<UserImpl>();
					while (it.hasNext()) {
						tmpUser = it.next();
						currentUser = findUserByMat(tmpUser.getMatricule());
						currentUser.setSupH(findSupHByCode(tmpUser.getSuphCode()));
						System.out.println("["+currentUser.getMatricule() + ","+currentUser.getNom()+"] >>>> ["
								        +currentUser.getSupH().getMatricule()+","+currentUser.getSupH().getNom());
						this.users.add(currentUser);
						//userService.update(currentUser);
					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
					
	}
	
	public  String affectBadge2UserAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload/managers";
			
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
							
				if("application/vnd.ms-excel".equals(contentType) 
						|| "application/xls".equals(contentType) 
						|| ":application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)
						|| "application/octet-stream".equals(contentType)){
					file += ".xls";
					UploadUtils.uploadFile(bis, file);
					List<UserImpl> usersXls = XlsReader.updateUserData(file);
					Iterator<UserImpl> it = usersXls.iterator();
					UserImpl currentUser;
					UserImpl tmpUser;
					User  suph;
					System.out.println("users size :: " + usersXls.size());
					ProfilApp profil = this.profilAppService.load(3);
					java.util.Set<Role> roles =  new HashSet<Role>();
					this.users = new ArrayList<UserImpl>();
					while (it.hasNext()) {
						tmpUser = it.next();
						currentUser = findUserByMat(tmpUser.getMatricule());
						if(currentUser!=null){
							if(currentUser.getBadge() == null || !tmpUser.getBadge().equalsIgnoreCase(currentUser.getBadge())){
								currentUser.setBadge(tmpUser.getBadge());
								System.out.println("["+currentUser.getMatricule() + ","+currentUser.getNom()+"] >>>> ["
								        +currentUser.getBadge()+"]");
								suph = findUserByMat(tmpUser.getSuphCode());
								currentUser.setSupH(suph);
								Role role = new RoleImpl();
								role.setProfilApp(profil);
								roles.add(role);
								currentUser.setRoles(roles);
								this.users.add(currentUser);
								userService.update(currentUser);
							}
							
						}else{
							System.out.println("User with matricule [" + tmpUser.getMatricule()+"] not found !");
						}
						
						
					}
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
					
	}
	
	public String uploadUserFileAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload/effectif";
			
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
							
				if("application/vnd.ms-excel".equals(contentType) 
						|| "application/xls".equals(contentType) 
						|| ":application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)){
					file += ".xls";
					UploadUtils.uploadFile(bis, file);
					System.out.println(file);
					List<UserImpl> usersXls = XlsReader.extractUserData(file);
					
					this.users = this.updateUsersByDefaultAttributes(usersXls);
					for (User user: users) 
						this.userService.create(user);
					return null;
				}
				else
					if("text/xml".equals(contentType)){
						file +=".xml";
						UploadUtils.uploadFile(bis, file);
						XmlReader xmlReader = new XmlReader(file);
						this.users = xmlReader.load();
						return null;
					}
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private List<UserImpl> updateUsersByDefaultAttributes(List users){
		try {
			Iterator<UserImpl> it = users.iterator();
			UserImpl currentUser;
			String defaultMat = "000";
			int i=2;
			while (it.hasNext()) {
				currentUser = it.next();
				if(currentUser.getMatricule()== null || "".equals(currentUser.getMatricule().trim())){
					if(i<10) defaultMat = "00000";
					else if(10<= i && i<100) defaultMat = "0000";
					else if(100<= i && i<1000) defaultMat = "000";
					else defaultMat="000";
					defaultMat += i;
					i++;
					currentUser.setMatricule(defaultMat);
				}
				//if(imageFolder !=null && !"".equals(this.imageFolder) )
					//currentUser.setPhoto(UploadUtils.getBytesFromFile(new File(this.getImageFolder()+currentUser.getMatricule()+".jpg")));
				currentUser.setProfilMetier(findProfilByLibelle(currentUser.getProfilMetierCode()));
				currentUser.setNoeud(findNoeudByCode(currentUser.getPositionCode()));
				currentUser.setUnit(findUnitByCode(currentUser.getSiteCode()));
				currentUser.setLogin(Globals.DEFAULT_USER_LOGIN + currentUser.getMatricule());
				currentUser.setPassword(passwordEncoder.encodePassword(Globals.DEFAULT_USER_PWD + currentUser.getMatricule(),null));
				if(currentUser.getBadge()== null || "".equals(currentUser.getBadge().trim()))
					currentUser.setBadge(currentUser.getMatricule());
				if(currentUser.getProfilAppCode()!= null && !"".equals(currentUser.getProfilAppCode().trim())){
					ProfilApp profil = this.profilAppService.load(Integer.parseInt(currentUser.getProfilAppCode()));
					if(profil!=null){
						List roles = (List)currentUser.getRoles();
						Role role = new RoleImpl();
						role.setProfilApp(profil);
						roles.add(role);
					}
				}
				currentUser.setDroitPaie(true);
			}
			return users;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return users;
		}
	}
	
	private ProfilMetier findProfilByCode(String code){
		try {
			if (code == null)
				return null;
			ProfilMetier profil = null;
			List profils;
			Hashtable  hash = new Hashtable();
			hash.put("code", code);
			profils = this.profilMetierService.read(hash);
			if(profils != null && profils.size() > 0) 
				profil = (ProfilMetier)profils.get(0);
			return profil;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private ProfilMetier findProfilByLibelle(String lib){
		try {
			if (lib == null)
				return null;
			ProfilMetier profil = null;
			List profils;
			Hashtable  hash = new Hashtable();
			hash.put("libelle", lib);
			profils = this.profilMetierService.read(hash);
			if(profils != null && profils.size() > 0) 
				profil = (ProfilMetier)profils.get(0);
			else {
				String defaultMat;
				if(i<10) defaultMat = "00";
				else if(10<= i && i<100) defaultMat = "0";
				else defaultMat = "";
				defaultMat += i;
				i++;
				profil = new ProfilMetierImpl();
				profil.setCode(defaultMat);
				profil.setLibelle(lib);
				this.profilMetierService.create(profil);
			}
			return profil;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private Noeud findNoeudByCode(String code){
		try {
			if (code == null)
				return null;
			Noeud node = null;
			List nodes;
			Hashtable  hash = new Hashtable();
			hash.put("code", code);
			nodes = this.noeudService.read(hash);
			if(nodes != null && nodes.size() > 0) 
				node = (Noeud)nodes.get(0);
			return node;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private User findSupHByCode(String code){
		try {
			User user = null;
			List<User> users;
			Hashtable<String, String>  hash = new Hashtable<String, String>();
			hash.put("matricule", code);
			users = this.userService.read(hash);
			if(users != null && users.size() > 0) 
				user = (User)users.get(0);
			return user;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private Unit findUnitByCode(String code){
		try {
			if (code == null)
				return null;
			Unit unit = null;
			List<Unit> units;
			
			Hashtable<String, String>  hash = new Hashtable<String, String>();
			hash.put("code", code);
			units = this.unitService.read(hash);
			if(units != null && units.size() > 0) 
				unit = (Unit)units.get(0);
			return unit;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String uploadProfilFileAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload/profil-uploadedFile";
			
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
				System.err.println("contentType ::" + contentType);			
				if("application/vnd.ms-excel".equals(contentType) 
						|| "application/xls".equals(contentType) 
						|| "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)){
					file += ".xls";
					UploadUtils.uploadFile(bis, file);
					this.profils = XlsReader.extractProfilData(file);
								
					return null;
				}
				
				
			}
				context.addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MessageFactory.getMessage(
										Globals.USR_E004_ME002, ""), null));
			
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return null;
	}
	
	public String uploadAffectPlaFileAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload/profil-uploadedFile";
			
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
							
				if("application/vnd.ms-excel".equals(contentType)){
					file += ".xls";
					UploadUtils.uploadFile(bis, file);
					HashMap<String, String> hash = XlsReader.extractAffectPlaData(file);
					this.users = (Collection<UserImpl>)affectPla2Users(hash);
								
					return null;
				}
				
				
			}
				context.addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MessageFactory.getMessage(
										Globals.USR_E004_ME002, ""), null));
			
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return null;
	}
	
	public String uploadPntgFileAction(){
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String file = rootpath + "upload\\generateur de pointage";
			System.out.println("FILE UPLOADED : " + file);
			if(getUploadFile() != null){
				String contentType = getUploadFile().getContentType();
				BufferedInputStream bis = new BufferedInputStream(getUploadFile().getInputstream());
				System.out.println("FILE CONTENT TYPE : " + contentType);			
				if("application/vnd.ms-excel".equals(contentType) 
						|| "application/xls".equals(contentType) 
						|| ":application/vnd.openxmlformats-officedocument.spreadsheetml.sheet".equals(contentType)
						|| "application/octet-stream".equals(contentType)){
					file += ".xls";
					
					UploadUtils.uploadFile(bis, file);
					System.out.println("FILE UPLOADED WITH SUCCESS !");
					List<MouvementImpl> mvmntsXls= XlsReader.extractPntgData(file);
					if(mvmntsXls != null && !mvmntsXls.isEmpty()){
						Iterator<MouvementImpl> iter = mvmntsXls.iterator();
						MouvementImpl mvmnt = null;
						String badge;
						mvmnts = new ArrayList<MouvementImpl>();
						while (iter.hasNext()) {
							mvmnt = iter.next();
							badge = findUserBadgeByMatricule(mvmnt.getMatricule());
							if(badge!=null && !"".equals(badge.trim())){
								mvmnt.setBadge(badge);
								mouvementService.create(mvmnt);
								mvmnt.setExist(isMvmntExist(mvmnt));
								mvmnts.add(mvmnt);
							}else{
								context.addMessage("",
										new FacesMessage(FacesMessage.SEVERITY_ERROR,
												"Aucun Badge n'est attribué pour le matricule : " + mvmnt.getMatricule()+". Veuillez vérifier si le matricule renseigné est correct!", null));
							
							}
						}
						
					}
					bis.close();			
					return null;
				}
				
				
			}
			System.out.println("FILE IS NOT UPLOADED !!");	
			context.addMessage("",
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MessageFactory.getMessage(
										Globals.USR_E004_ME002, ""), null));
			
			
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
		return null;
	}
	
	private String findUserBadgeByMatricule(String matricule){
		User user = findUserByMat(matricule);
		if(user!=null)
			return user.getBadge();
		System.out.println("----> no matricule :" + matricule);
		return null;
	}
	
	private List<UserImpl> affectPla2Users(HashMap<String, String> hash){
		List<UserImpl> userResults = null;
		if(!hash.isEmpty()){
			Iterator<String> iter = hash.keySet().iterator();
			String key;
			UserImpl user = null;
			Planning pla = null;
			userResults = new ArrayList<UserImpl>();
			while (iter.hasNext()) {
				key = iter.next();
				user = findUserByMat(key);
				pla = findPlaByLib(hash.get(key));
				user.setPlanning(pla);
				userResults.add(user);
				
			}
		}
		return userResults;
	}

	private UserImpl findUserByMat(String matricule){
		try {
			List<User> list;
			Hashtable<String, String>  hash = new Hashtable<String, String>();
			hash.put("matricule", matricule);
			list = this.userService.read(hash);
			if(list!=null && !list.isEmpty()) 
				return (UserImpl)list.get(0);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private Planning findPlaByLib(String libelle){
		try {
			List<Planning> list;
			Hashtable<String, String>  hash = new Hashtable<String, String>();
			hash.put("libelle", libelle);
			list = this.planningService.read(hash);
			if(list!=null && !list.isEmpty()) 
				return (Planning)list.get(0);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	/**
	 * Getters and setters
	 * @return
	 */
	public Collection<UserImpl> getUsers() {
		return users;
	}

	public void setUsers(Collection<UserImpl> users) {
		this.users = users;
	}
	
	public void reset(ActionEvent ev){
		this.users = null;
		this.profils = null;
		this.mvmnts = null;
		
	}

	public UploadedFile getUploadFile() {
		return uploadFile;
	}

	public void setUploadFile(UploadedFile uploadFile) {
		this.uploadFile = uploadFile;
	}

	public Collection<ProfilMetier> getProfils() {
		return profils;
	}

	public void setProfils(Collection<ProfilMetier> profils) {
		this.profils = profils;
	}
	
	public String saveImportedMvmntsAction(){
		try {
			if(this.mvmnts != null && this.mvmnts.size() > 0){
				Iterator<MouvementImpl> it = this.mvmnts.iterator();
				MouvementImpl mvmnt;	
				while (it.hasNext()) {
					mvmnt = it.next();
					if(!mvmnt.isExist())
						this.mouvementService.create(mvmnt);
						
				}
				return "pointageListe";	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.USR_E004_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private boolean isMvmntExist(Mouvement mvmnt){
		try {
			Hashtable<String, Object> props = new Hashtable<String, Object>();
			props.put("badge", mvmnt.getBadge());
			props.put("date", mvmnt.getDate());
			List results=this.mouvementService.read(props);
			if(results ==null || results.size() ==0)
				return false;
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
	
	public void saveImportedUsersAction(){
		try {
			Map metiers = profilMetierService.profilsByCode();
			Map neouds = noeudService.noeudsByCode();
			Map units = unitService.unitsByCode();
			if(this.users != null && this.users.size() > 0)
				for (User user: users) {
					user.setDroitPaie( true);
					user.setActive( true);
					this.userService.create(user);
				}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String saveAffectUser2SuphAction(){
		try {
			if(this.users != null && this.users.size() > 0){
				Iterator<UserImpl> it = this.users.iterator();
				User currentUser;	
				while (it.hasNext()) {
					currentUser = it.next();
					System.out.println("["+currentUser.getMatricule() + ","+currentUser.getNom()+"] >>>> ["
					        +currentUser.getSupH().getMatricule()+","+currentUser.getSupH().getNom());
					this.userService.update(currentUser);
						
				}
				return Globals.USER_LISTE_FORWARD;	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.USR_E004_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public String saveAffectBadge2UserAction(){
		try {
			if(this.users != null && this.users.size() > 0){
				Iterator<UserImpl> it = this.users.iterator();
				User currentUser;	
				while (it.hasNext()) {
					currentUser = it.next();
					System.out.println("["+currentUser.getMatricule() + ","+currentUser.getNom()+"] >>>> ["
					        +currentUser.getBadge()+"]");
					this.userService.update(currentUser);
						
				}
				return Globals.USER_LISTE_FORWARD;	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.USR_E004_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String saveAffectPlaAction(){
		try {
			if(this.users != null && this.users.size() > 0){
				Iterator<UserImpl> it = this.users.iterator();
				User user;	
				while (it.hasNext()) {
					user = it.next();
					this.userService.update(user);
						
				}
				return Globals.USER_LISTE_FORWARD;	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.USR_E004_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public String saveImportedProfilsAction(){
		try {
			if(this.profils != null && this.profils.size() > 0){
				Iterator<ProfilMetier> it = this.profils.iterator();
				ProfilMetier profil;	
				while (it.hasNext()) {
					profil = it.next();
					this.profilMetierService.create(profil);
						
				}
				return Globals.PROFIL_LISTE_FORWARD;	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.PRF_E003_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	private void saveNode(Noeud nd){
		try {
			System.out.println("["+nd.getCode()+","+nd.getLibelle()+"] ");
			noeudService.create(nd);
			if(nd.getNoeuds()!=null){
				Iterator<Noeud> it = nd.getNoeuds().iterator();
				while(it.hasNext()){
					saveNode(it.next());
				}
				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String saveImportedHierarchieAction(){
		try {
			if(this.profils != null && this.profils.size() > 0){
				Iterator<ProfilMetier> it = this.profils.iterator();
				ProfilMetier profil;	
				while (it.hasNext()) {
					profil = it.next();
					this.profilMetierService.create(profil);
						
				}
				return Globals.PROFIL_LISTE_FORWARD;	
			}
			context.addMessage("", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.PRF_E003_ME003), null));
			return null;	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void setProfilMetierService(
			ProfilMetierManageableService profilMetierService) {
		this.profilMetierService = profilMetierService;
	}

//	public String getImageFolder() {
//		if(imageFolder !=null && !"".equals(this.imageFolder) ){
//			int index = imageFolder.lastIndexOf("\\");
//			if(imageFolder.length()-1 != index){
//				imageFolder += "\\";
//			}
//		}
//		return imageFolder;
//	}
//
//	public void setImageFolder(String imageFolder) {
//		this.imageFolder = imageFolder;
//	}

	public void setProfilAppService(ProfilAppManageableService profilAppService) {
		this.profilAppService = profilAppService;
	}

	public void setNoeudService(NoeudManageableService noeudService) {
		this.noeudService = noeudService;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}
	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}


	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}


	public Collection<MouvementImpl> getMvmnts() {
		return mvmnts;
	}


	public void setMvmnts(Collection<MouvementImpl> mvmnts) {
		this.mvmnts = mvmnts;
	}


	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}


	public List<User> getSelectedUsers() {
		return selectedUsers;
	}


	public void setSelectedUsers(List<User> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}
	
	public UploadedFile getUplodedFile() {
		return uplodedFile;
	}

	public void setUplodedFile(UploadedFile uplodedFile) {
		this.uplodedFile = uplodedFile;
	}
}
