package ma.nawarit.checker.beans.configuration.parametreAvance;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import ma.nawarit.checker.configuration.AdvParam;
import ma.nawarit.checker.configuration.AdvParamImpl;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

@ManagedBean(name = "advParamBean")
@ViewScoped
public class AdvParamBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{AdvParamManageableService}")
	private AdvParamManageableService advParamServise;
	
	private AdvParam advParam = new AdvParamImpl();
	private AdvParam current = new AdvParamImpl();
	private AdvParam detailedadvParam = new AdvParamImpl();
	private List<AdvParam> advParams = new ArrayList<AdvParam>();
	
    public void onRowEdit(RowEditEvent event) throws Exception {
        
    	current = (AdvParam) event.getObject();
    	advParamServise.update(current);

		int i = advParams.indexOf(current);
		advParams.remove(current);
		advParams.add(i, current);	
    }
    
    public void onCellEdit(CellEditEvent event) throws Exception {
        Object oldValue = event.getOldValue();
        Object newValue = event.getNewValue();
         
        if(newValue != null && !newValue.equals(oldValue)) {
    		advParamServise.update(advParam);
        }
    }
	
	public void intializadvParamListener(){
		advParam = new AdvParamImpl();
	}
	
	public void updateAdvParam() throws Exception{
		advParamServise.update(current);

		int i = advParams.indexOf(current);
		advParams.remove(current);
		advParams.add(i, current);		
	}

	public void setAdvParamServise(AdvParamManageableService advParamServise) {
		this.advParamServise = advParamServise;
	}

	public AdvParam getAdvParam() {
		return advParam;
	}

	public void setAdvParam(AdvParam advParam) {
		this.advParam = advParam;
	}

	public List<AdvParam> getAdvParams() {
		try {
			if (advParams == null || advParams.isEmpty())
				advParams = advParamServise.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return advParams;
	}

	public void setAdvParams(List<AdvParam> advParams) {
		this.advParams = advParams;
	}

	public AdvParam getCurrent() {
		return current;
	}

	public void setCurrent(AdvParam current) {
		this.current = current;
	}

	public AdvParam getDetailedadvParam() {
		return detailedadvParam;
	}

	public void setDetailedadvParam(AdvParam detailedadvParam) {
		this.detailedadvParam = detailedadvParam;
	}
}
