package ma.nawarit.checker.beans;

import java.io.InputStream;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.andromda.spring.CommonCriteria;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@RequestScoped
public class ReportDatasBean {
	private StreamedContent file;  
	private CommonCriteria criteria;
    
    public ReportDatasBean() {          
        InputStream stream = ((ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext()).getResourceAsStream("/resources/files/ETAT_PER_M.ATTABE.xlsx");  
        file = new DefaultStreamedContent(stream, "application/vnd.ms-excel", "ETAT_REPORT_122013.xlsx");  
    }  
  
    public StreamedContent getFile() {  
        return file;  
    }
}
