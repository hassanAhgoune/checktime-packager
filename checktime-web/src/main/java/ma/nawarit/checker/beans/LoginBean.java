package ma.nawarit.checker.beans;



import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.acegisecurity.BadCredentialsException;
import org.acegisecurity.ui.AbstractProcessingFilter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;



@ManagedBean(name="loginBean")
@ViewScoped
public class LoginBean
{

    private String username = "";
    private String password = "";
    private boolean rememberMe = false;
    private boolean loggedIn = false;
    private boolean erreur = false;
    
    
    
	public String doLogin() throws Exception
    {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();

        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest())
                .getRequestDispatcher("/j_spring_security_check");
  

        dispatcher.forward((ServletRequest) context.getRequest(), (ServletResponse) context.getResponse());
        
//        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        String name = auth.getName(); //get logged in username
//        System.out.println(name);
        
        FacesContext.getCurrentInstance().responseComplete();
        
        
//        List<Lien> list=  ls.readAll();
//	    System.out.println(list.get(0).getLibelle());
        return null;

    }

    @PostConstruct
    @SuppressWarnings({ "unused", "deprecation", "restriction" })
    private boolean handleErrorMessage()
    {
    	
        Exception e = (Exception) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(
        		AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY);

        if (e instanceof BadCredentialsException)
        {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Le login ou le mot de passe est invalid.", null));
            erreur =true;
  
            
        }
		return erreur;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public boolean isRememberMe()
    {
        return this.rememberMe;
    }

    public void setRememberMe(final boolean rememberMe)
    {
        this.rememberMe = rememberMe;
    }

    public boolean isLoggedIn()
    {
        return this.loggedIn;
    }

    public void setLoggedIn(final boolean loggedIn)
    {
        this.loggedIn = loggedIn;
    }
    
    public boolean isErreur() {
		return erreur;
	}

	public void setErreur(boolean erreur) {
		this.erreur = erreur;
	}
	
	
}
