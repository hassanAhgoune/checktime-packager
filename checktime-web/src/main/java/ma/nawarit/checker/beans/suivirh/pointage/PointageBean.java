package ma.nawarit.checker.beans.suivirh.pointage;

import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;
import org.primefaces.model.TreeNode;
import ma.nawarit.checker.beans.suivirh.report.ReportBean;
import ma.nawarit.checker.compagnie.Group;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.GroupManageableService;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.core.common.LineData;
import ma.nawarit.checker.core.common.LineDay;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.crud.TerminalManageableService;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.HeurSuppImpl;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableServiceBase;
import ma.nawarit.checker.suivi.crud.HeurSuppManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.syncRH.databaseOperations;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

@ManagedBean(name = "pointageBean")
@ViewScoped
public class PointageBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(ReportBean.class);

	private List<String> mois;
	private String moisSelected;
	private String moisCours;
	private String anneeSelected;
	private Date dateSelected;
	private int nbre_jours;
	private List<List<Pointage>> pointages;
	private List<CustomPointageItem> customPointages;
	private List<CustomPointageItem> customPointagesModel;
	private Pointage pointage;
	private List<Date> colonnes;
	private List<Mouvement> mouvements;
	private boolean omp = true;
	private boolean tjr = true;
	private boolean nrt = true;
	private boolean nip = true;
	private boolean rendered = false;
	private Mouvement mouvement;
	private DataTable dataTable;

	private List<Mouvement> mouvementList;
	private List<Absence> absencesList;
	private List<Retard> retardsList;
	private User user = new UserImpl();
	private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HHmmss");
	private Boolean isPersonalFicheLoaded = false;
	private String mouvementImage;
	private Date dateDebut;
	private Date dateFin;
	private String matriculeInf;
	private String matriculeSup;
	private TreeNode selectedNoeudNode;
	private TreeNode selectedUnitNode;
	private String selectedGroupId;
	private boolean isRetardVisible;
	private boolean isAbsenceVisible;
	private boolean isCongeVisible;
	
	private String selectedGroup;
	private TreeNode selectedNoeud;
	private TreeNode selectedUnit;

	@ManagedProperty(value = "#{AnnomalieManageableService}")
	private AnnomalieManageableService anomalieService;

	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;

	@ManagedProperty(value = "#{MouvementManageableService}")
	private MouvementManageableService mouvementService;

	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService;

	@ManagedProperty(value = "#{HeurSuppManageableService}")
	private HeurSuppManageableService hrSuppService;

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsService;

	@ManagedProperty(value = "#{AnnotationManageableService}")
	private AnnotationManageableService annotationService;

	@ManagedProperty(value = "#{GroupManageableService}")
	private GroupManageableService groupService;

	@ManagedProperty(value = "#{RetardManageableService}")
	private RetardManageableService retardService;

	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService = new CongeManageableServiceBase();

	@ManagedProperty(value = "#{TerminalManageableService}")
	private TerminalManageableService terminalService;

	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;

	@ManagedProperty(value = "#{TypeAbsenceManageableService}")
	private TypeAbsenceManageableService typeAbsenceService;

	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeService;

	@ManagedProperty(value = "#{PlanningManageableService}")
	private PlanningManageableService planningService;
	
	@ManagedProperty(value = "#{NoeudManageableService}")
	private NoeudManageableService noeudService;

	private ScheduleModel eventModel;

	private CommonCriteria criteria = new CommonCriteria();

	private FacesContext context = FacesContext.getCurrentInstance();

	private User usrAuthenticated;
	
	private boolean isSupH=false;

	public PointageBean() {
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		usrAuthenticated = (User) obj;
		isSupH=context.getExternalContext().isUserInRole("ROLE_SUPH");
		setAnneeSelected(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
		setMoisSelected(String.valueOf(Calendar.getInstance().get(Calendar.MONTH)));
		eventModel = new DefaultScheduleModel();
		isCongeVisible = false;
		isAbsenceVisible = true;
		isRetardVisible = true;
		this.mouvement = new MouvementImpl();
	}
	
	public void saveVirtPointage() {
		this.mouvement.setDate(new Date());
		if (usrAuthenticated != null)
			this.mouvement.setBadge(this.usrAuthenticated.getBadge());
		this.mouvement.setTypeMouvement("VIRTUEL");
		try {
			this.mouvementService.create(mouvement);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void afficherDetails() {
		String mat = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("ligne");
		int col = Integer.parseInt(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("colonne"));
		for (int i = 0; i < pointages.size(); i++) {
			if (pointages.get(i).get(col).getMatricule().equals(mat)) {
				this.pointage = pointages.get(i).get(col);
				break;
			}
		}
		// this.pointage = pointages.get(ligne).get(col);
		if (this.pointage.getMouvements() == null)
			this.pointage.setMouvements(new ArrayList<Mouvement>());
		if (this.pointage.getMouvements().size() == 0
				|| this.pointage.getMouvements().get(this.pointage.getMouvements().size() - 1).getId() > 0)
			this.pointage.getMouvements().add(new MouvementImpl());
	}

	public void groupValueChangeListener(ValueChangeEvent e) throws Exception {
		Group group = this.groupService.load(Integer.parseInt((String) e.getNewValue()));
		criteria.setIndexs(new ArrayList<Integer>());
		for (User u : group.getUsers())
			criteria.getIndexs().add(u.getId());
		this.criteria.setChanged(true);
	}

	public void desactiver_activer_omp() {
		if (this.omp)
			this.omp = false;
		else
			this.omp = true;
		RequestContext.getCurrentInstance().update("pointageTable");
	}

	public void desactiver_activer_tjr() {
		if (this.tjr)
			this.tjr = false;
		else
			this.tjr = true;
		RequestContext.getCurrentInstance().update("pointageTable");
	}

	public void desactiver_activer_nrt() {
		if (this.nrt)
			this.nrt = false;
		else
			this.nrt = true;
		RequestContext.getCurrentInstance().update("pointageTable");
	}

	public void desactiver_activer_nip() {
		if (this.nip)
			this.nip = false;
		else
			this.nip = true;
		RequestContext.getCurrentInstance().update("pointageTable");
	}

	public void chargerPointages() {
		this.rendered = true;
		this.omp = true;
		this.tjr = true;
		this.nrt = true;
		this.nip = true;
		this.charger_donnees();
	}

	public void charger_donnees() {
		int anneeSelectedInt = Integer.parseInt(anneeSelected);
		pointages = new ArrayList<List<Pointage>>();
		List<Pointage> ligne;
		Calendar d1 = Calendar.getInstance();
		d1.set(Calendar.MONTH, mois.indexOf(this.moisSelected));
		d1.set(Calendar.YEAR, anneeSelectedInt);
		d1.set(Calendar.DAY_OF_MONTH, 1);
		d1.set(Calendar.HOUR_OF_DAY, 0);
		d1.set(Calendar.MINUTE, 0);
		d1.set(Calendar.SECOND, 0);
		d1.set(Calendar.MILLISECOND, 0);
		dateSelected = d1.getTime();
		this.nbre_jours = d1.getActualMaximum(Calendar.DAY_OF_MONTH);
		this.moisCours = this.moisSelected;
		CommonCriteria crit = new CommonCriteria();
		crit.setDateDebut(d1.getTime());
		d1.set(Calendar.DAY_OF_MONTH, this.nbre_jours);
		crit.setDateFin(d1.getTime());
		try {
			criteria = new CommonCriteria();
			if (selectedNoeud != null)
				criteria.setNoeuds(Utils.getValueChildren((Noeud) selectedNoeud.getData()));
			if (selectedUnit != null)
				criteria.setSiteId(String.valueOf(((Unit) selectedUnit.getData()).getId()));
			if (this.selectedGroup != null && !this.selectedGroup.equals(""))
				criteria.setGroup(this.selectedGroup);
			if(isSupH)
				criteria.setSuppHrhId(String.valueOf(usrAuthenticated.getId()));
			
			List<User> users = userService.readByCriteria(criteria);
			if (usrAuthenticated != null && "ROLE_SUPH".equals(usrAuthenticated.getProfilApp().getCode())) {
				users = Utils.getUsersByManagerId(users, usrAuthenticated.getId());
			}
			Pointage pointage = null;
			List<Annomalie> anos = null;
			List<Mouvement> mvts = null;
			List<Annomalie> newAnos = null;
			List<Mouvement> newMvts = null;
			Calendar d2 = Calendar.getInstance();
			int i1 = 0;
			int i2 = 0;
			for (User user : users) {
				ligne = new ArrayList<Pointage>();
				// crit = new CommonCriteria();
				crit.setMatriculeInf(user.getMatricule());
				crit.setMatriculeSup(user.getMatricule());
				anos = anomalieService.readByCriteria(crit);
				if (anos != null && !anos.isEmpty())
					Utils.sortAnomaliesByTime(anos);
				mvts = mouvementService.readByCriteria(crit);
				if (mvts != null && !mvts.isEmpty())
					Utils.sortMvtsByTime(mvts);
				d1.set(Calendar.YEAR,anneeSelectedInt);
				d1.set(Calendar.DAY_OF_MONTH, 1);
				d1.set(Calendar.MONTH, mois.indexOf(this.moisSelected));
				for (int j = 1; j <= this.nbre_jours; j++) {
					newAnos = new ArrayList<Annomalie>();
					i1 = 0;
					if (anos != null && !anos.isEmpty())
						for (Annomalie a : anos) {
							if (a.getDateAnomalie().compareTo(d1.getTime()) != 0)
								break;
							newAnos.add(a);
							i1++;
						}
					newMvts = new ArrayList<Mouvement>();
					i2 = 0;
					if (mvts != null && !mvts.isEmpty())
						for (Mouvement m : mvts) {
							d2.setTime(m.getDate());
							d2.set(Calendar.HOUR_OF_DAY, 0);
							d2.set(Calendar.MINUTE, 0);
							d2.set(Calendar.SECOND, 0);
							d2.set(Calendar.MILLISECOND, 0);
							if (d2.compareTo(d1) != 0)
								break;
							newMvts.add(m);
							i2++;
						}
					pointage = new Pointage(user, d1.getTime(), newAnos, newMvts);
					if (i1 > 0)
						anos = anos.subList(i1, anos.size());
					if (i2 > 0)
						mvts = mvts.subList(i2, mvts.size());
					ligne.add(pointage);
					d1.add(Calendar.DAY_OF_MONTH, 1);
				}
				pointages.add(ligne);
			}
		} catch (Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public ScheduleModel getMyMouvementModel() {
		if (!isPersonalFicheLoaded) {
			if (eventModel.getEvents().size() == 0) {
				FacesContext fc = FacesContext.getCurrentInstance();
				try {
					Object obj = fc.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
					if (obj != null && (obj instanceof User)) {
						User u = (User) obj;
						user = userService.load(u.getId());
						CommonCriteria crit = new CommonCriteria();
						crit.setMatriculeInf(user.getMatricule());
						crit.setMatriculeSup(user.getMatricule());

						// ajout des mouvements
						List<Mouvement> mouvementList = mouvementService.readByCriteria(crit);
						if (mouvementList != null) {
							if (mouvementList.size() > 0) {
								for (Mouvement mvn : mouvementList) {
									Calendar endDate = Calendar.getInstance();
									endDate.setTime(mvn.getDate());
									endDate.add(Calendar.MINUTE, 30);
									DefaultScheduleEvent ev = new DefaultScheduleEvent();
									ev.setData(mvn.getId());
									ev.setStartDate(mvn.getDate());
									ev.setEndDate(endDate.getTime());
									ev.setTitle(mvn.getTypeMouvement());
									ev.setStyleClass("mouvement");
									eventModel.addEvent(ev);
								}
							}
						}

						// add les absences
						List<Absence> absencesList = absenceService.readByCriteria(crit);
						if (absencesList != null) {
							if (absencesList.size() > 0) {
								for (Absence abs : absencesList) {
									Calendar startDate = Calendar.getInstance();
									Calendar endDate = Calendar.getInstance();

									// set start date
									int hour = (int) abs.getHeureDebut();
									int minut = (int) ((abs.getHeureDebut() - hour) * 100);
									int dureeHour = (int) abs.getDuree();
									int dureeMinut = (int) ((abs.getDuree() - dureeHour) * 100);

									startDate.setTime(abs.getDateDebut());
									startDate.add(Calendar.HOUR_OF_DAY, hour);
									startDate.add(Calendar.MINUTE, minut);

									// set end date
									endDate.setTime(startDate.getTime());
									endDate.add(Calendar.HOUR_OF_DAY, dureeHour);
									endDate.add(Calendar.MINUTE, dureeMinut);

									DefaultScheduleEvent ev = new DefaultScheduleEvent();
									ev.setData(abs.getId());
									ev.setStartDate(startDate.getTime());
									ev.setEndDate(endDate.getTime());
									ev.setTitle(getAbsenceStatut(abs.getStatut()));
									ev.setStyleClass("absence");
									eventModel.addEvent(ev);
								}
							}
						}

						// add les retard
						crit.setRetardTolEnabled(true);
						List<Retard> retardList = retardService.readByCriteria(crit);
						if (retardList != null) {
							if (retardList.size() > 0) {
								for (Retard rtr : retardList) {
									Calendar startDate = Calendar.getInstance();
									Calendar endDate = Calendar.getInstance();

									startDate.setTime(rtr.getDate());
									Calendar startTime = getRetardStartTime(rtr.getDescription());
									if (startTime != null) {
										startDate.add(Calendar.HOUR_OF_DAY, startTime.get(Calendar.HOUR_OF_DAY));
									}
									// add end date
									int totalretardMinut = rtr.getRetardNonTolere() + rtr.getRetardTolere();
									endDate.setTime(startDate.getTime());
									endDate.add(Calendar.MINUTE, totalretardMinut);

									// add event
									DefaultScheduleEvent ev = new DefaultScheduleEvent();
									ev.setData(rtr.getId());
									ev.setStartDate(startDate.getTime());
									ev.setEndDate(endDate.getTime());
									ev.setTitle(getAbsenceStatut(rtr.getStatut()));
									ev.setStyleClass("retard");
									eventModel.addEvent(ev);
								}
							}
						}

						// list des conges
						crit.setRetardTolEnabled(true);
						List<Conge> congeList = congeService.readByCriteria(crit);
						if (congeList != null) {
							if (congeList.size() > 0) {
								for (Conge cng : congeList) {
									// add event
									DefaultScheduleEvent ev = new DefaultScheduleEvent();
									ev.setData(cng.getId());
									ev.setStartDate(cng.getDateDebut());
									ev.setEndDate(cng.getDateReprise());
									ev.setTitle(getAbsenceStatut(cng.getStatut()));
									ev.setStyleClass("conge");
									eventModel.addEvent(ev);
								}
							}
						}

					}
				} catch (Exception e) {
					e.printStackTrace();
					// TODO: handle exception
				}
			}
			isPersonalFicheLoaded = true;
		}
		return eventModel;
	}

	public Calendar getRetardStartTime(String description) throws ParseException {
		Calendar time = Calendar.getInstance();
		String hor = "";
		if (description.length() > 0) {
			String[] parts = description.trim().split(" ");
			int l = parts.length - 1;
			hor = parts[parts.length - 1].trim();
			if (hor.length() > 0) {
				if (hor.matches("^([0-9]{2}):([0-9]{2}):([0-9]{2})$")) {
					DateFormat format = new SimpleDateFormat("HH:mm:ss");
					time.setTime(format.parse(hor));
				} else {
					time = null;
				}
			} else {
				time = null;
			}
		} else {
			time = null;
		}
		return time;
	}

	public String getAbsenceStatut(String statut) {
		String stt = "";
		if (statut.equals(Globals.WORKFLOW_STATUS_ELAPSED))
			stt = "Effectu\u00e9";
		if (statut.equals(Globals.WORKFLOW_STATUS_VALIDATE))
			stt = "Valid\u00e9";
		if (statut.equals(Globals.WORKFLOW_STATUS_ENCOURS))
			stt = "Demand\u00e9";
		if (statut.equals(Globals.WORKFLOW_STATUS_REJECTED))
			stt = "Rejout\u00e9";
		return stt;
	}

	public List<String> getMois() {
		mois = new ArrayList<String>();
		mois.add("Janvier");
		mois.add("F\u00E9vrier");
		mois.add("Mars");
		mois.add("Avril");
		mois.add("Mai");
		mois.add("Juin");
		mois.add("Juillet");
		mois.add("Ao\u00FBt");
		mois.add("Septembre");
		mois.add("Octobre");
		mois.add("Novembre");
		mois.add("D\u00E9cembre");

		return mois;
	}

	public boolean isColorAccepted(String color) {
		return (color.equalsIgnoreCase("rgb(237,28,36)") && this.omp)
				|| (color.equalsIgnoreCase("rgb(163,73,164)") && this.tjr)
				|| (color.equalsIgnoreCase("rgb(195,195,195)") && this.nrt)
				|| (color.equalsIgnoreCase("rgb(250,250,175)") && this.nip);
	}

	public void updatePointageListener(RowEditEvent event) {
		try {
			Mouvement mouvement = ((Mouvement) event.getObject());
			if (mouvement != null && mouvement.getId() > 0) {
				String newStrDate = this.pointage.getDatePointage() + " "
						+ Utils.toStrDate(mouvement.getDate(), "HH:mm:ss");
				mouvement.setDate(new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(newStrDate));
				mouvementService.update(mouvement);
			} else if (mouvement != null && mouvement.getDate() != null) {
				String newStrDate = this.pointage.getDatePointage() + " "
						+ Utils.toStrDate(mouvement.getDate(), "HH:mm:ss");
				mouvement.setDate(new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(newStrDate));
				mouvement.setBadge(this.pointage.getBadge());
				mouvementService.create(mouvement);
				RequestContext.getCurrentInstance().update("pointageTable");
				this.pointage.getMouvements().add(new MouvementImpl());
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deletePointageListener(RowEditEvent event) {
		try {
			Mouvement mouvement = ((Mouvement) event.getObject());
			if (mouvement != null && mouvement.getId() > 0) {
				pointage.getMouvements().remove(mouvement);
				RequestContext.getCurrentInstance().update("pointageTable");
				mouvementService.delete(mouvement);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void correctAnomalieListener(Annomalie ano) {
		Mouvement newMvmnt = null;
		try {
			if (ano.getType().equals("NRT")) {
				Absence abs;
				String[] res = ano.getDescription().split("'");
				abs = new AbsenceImpl();
				abs.setDateDebut(ano.getDateAnomalie());
				abs.setDateReprise(ano.getDateAnomalie());
				abs.setDuree(new Double(res[1]));
				abs.setCollaborateur(ano.getUser());
				abs.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
				Hashtable<String, String> typeAbsProps = new Hashtable<String, String>();
				typeAbsProps.put("code", "NJ");
				abs.setTypeAbsence((TypeAbsence) typeAbsService.read(typeAbsProps).get(0));
				absenceService.create(abs);
				this.anomalieService.delete(ano);
			} else if (ano.getType().equals("OPJO")) {
				String hrPoi = null;
				newMvmnt = new MouvementImpl();
				newMvmnt.setBadge(ano.getUser().getBadge());
				java.util.StringTokenizer tokenizer = null;
				if (ano.getHeurePointage() != null && !"".equals(ano.getHeurePointage().trim())) {
					if (hrPoi == null || (hrPoi != null && !ano.getHeurePointage().equals(hrPoi)))
						hrPoi = ano.getHeurePointage();
				}
				if (hrPoi != null && !"".equals(hrPoi.trim()))
					tokenizer = new java.util.StringTokenizer(hrPoi, ":");
				else {
					tokenizer = new java.util.StringTokenizer(ano.getDescription(), ":");
					tokenizer.nextToken();
				}
				Calendar cal = new GregorianCalendar();
				cal.setTime(ano.getDateAnomalie());
				cal.set(Calendar.HOUR_OF_DAY, new Integer(tokenizer.nextToken()));
				cal.set(Calendar.MINUTE, new Integer(tokenizer.nextToken()));
				newMvmnt.setDate(cal.getTime());
				this.mouvementService.create(newMvmnt);
				this.anomalieService.delete(ano);
			} else if (ano.getType().equals("NAHS") || ano.getType().equals("TJR")) {
				Annotation annotation = null;
				if (ano.getAnnotationId() > 0) {
					HeurSupp hrSupp = new HeurSuppImpl();
					hrSupp.setCollaborateur(ano.getUser());
					annotation = annotationService.load(ano.getAnnotationId());
					hrSupp.setAnnotation(annotation);
					hrSupp.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
					Calendar date = new GregorianCalendar();
					date.setTime(ano.getDateAnomalie());
					String[] res = ano.getMatricule().split("-");
					if ("JF".equals(res[0]) || "JR".equals(res[0])) {
						hrSupp.setDateDebut(date.getTime());
						date.set(Calendar.HOUR_OF_DAY, 23);
						date.set(Calendar.MINUTE, 59);
						this.hrSuppService.create(hrSupp);
						// create validate hrSupp
						hrSupp = new HeurSuppImpl();
						hrSupp.setCollaborateur(ano.getUser());
						annotation = this.annotationService.load(ano.getAnnotationId());
						hrSupp.setAnnotation(annotation);
						date.set(Calendar.HOUR_OF_DAY, 0);
						date.set(Calendar.MINUTE, 0);
						hrSupp.setDateDebut(date.getTime());
						date.set(Calendar.HOUR_OF_DAY, 23);
						date.set(Calendar.MINUTE, 59);
						hrSupp.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					} else if ("JC".equals(res[0])) {
						// Hashtable<String, String> props = new Hashtable<String, String>();
						// props.put("statut",
						// MessageFactory.getMessage(Globals.WORKFLOW_STATUS_VALIDATE));
						// congeService.read(props);
					} else {
						java.util.StringTokenizer tokenizer = new java.util.StringTokenizer(ano.getDescription(), "'");
						tokenizer.nextToken();
						Double hSupp1 = new Double(tokenizer.nextToken().replace(':', '.'));
						tokenizer.nextToken();
						Double hSupp2 = new Double(tokenizer.nextToken().replace(':', '.'));
						hrSupp.setDateDebut(date.getTime());
						hrSupp.setHeureDebut(hSupp1);
						hrSupp.setHeureFin(hSupp2);
						hrSupp.setDuree(Utils.differenceHeures(hSupp1, hSupp2));
						hrSuppService.create(hrSupp);
						// create validate hrSupp
						hrSupp = new HeurSuppImpl();
						annotation = this.annotationService.load(ano.getAnnotationId());
						hrSupp.setAnnotation(annotation);
						hrSupp.setCollaborateur(ano.getUser());
						hrSupp.setDateDebut(date.getTime());
						hrSupp.setHeureDebut(hSupp1);
						hrSupp.setHeureFin(hSupp2);
						hrSupp.setDuree(Utils.differenceHeures(hSupp1, hSupp2));
						hrSupp.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
					}
					this.hrSuppService.create(hrSupp);
					pointage.getAnomalies().remove(ano);
					this.anomalieService.delete(ano);
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void deleteAnomalieListener(Annomalie ano) {
		try {
			if (ano != null) {

				pointage.getAnomalies().remove(ano);
				anomalieService.delete(ano);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void onEventSelect(SelectEvent even) throws Exception {
		ScheduleEvent event = new DefaultScheduleEvent();
		event = (ScheduleEvent) even.getObject();
		if (event != null) {
			if (event.getStartDate() != null) {
				loadUserDetails(dateFormat.parse(dateFormat.format(event.getStartDate())));
			}
		}
	}

	// affichage l'image de pointage
	public void showMouvementImage(Mouvement mvn) {
		User usr = userService.readUserByBadge(mvn.getBadge());
		if (usr == null)
			return;
		String imgName = "";
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String p = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
		String realPath = ctx.getRealPath("/");
		String matricule = usr.getMatricule();
		File userPhoto = new File(realPath + "upload" + File.separator + "mouvements" + File.separator + Integer.valueOf(matricule)
				+ File.separator + dateTimeFormat.format(mvn.getDate()) + ".jpg");
		if (!userPhoto.exists()) {
			imgName = ".." + File.separator + ".." + File.separator + ".." + File.separator + "upload" + File.separator
					+ "mouvements" + File.separator + "no_mouvement.jpg";
		} else {
			imgName = ".." + File.separator + ".." + File.separator + ".." + File.separator + "upload" + File.separator
					+ "mouvements" + File.separator + Integer.valueOf(matricule) + File.separator + dateTimeFormat.format(mvn.getDate())
					+ ".jpg";
		}
		mouvementImage = imgName;
	}

	public void onDateSelect(SelectEvent even) throws Exception {
		loadUserDetails((Date) even.getObject());
	}

	public String readMouvementLocation(String code) throws Exception {
		String location = "VIRTUEL";
		if (code == null || code.equals(""))
			return location;
		Hashtable<Object, Object> properties = new Hashtable<Object, Object>();
		properties.put("code", code);
		List<Terminal> term = terminalService.read(properties);
		if (term != null && term.size() > 0) {
			properties = new Hashtable<Object, Object>();
			properties.put("code", code);
			location = unitService.load(term.get(0).getUnit().getId()).getLibelle();
		}
		return location;
	}

	/**
	 * Custom Pointage loading...
	 * 
	 * @throws Exception
	 */

	public void loadCustomPointageModel() throws Exception {
		customPointages = new ArrayList<CustomPointageItem>();
		CommonCriteria criteria = new CommonCriteria();

		if (dateDebut == null || dateFin == null || dateFin.compareTo(dateDebut) < 0) {
			showMessage("error", "Date Début est après date fin !");
			return;
		}

		criteria.setMatriculeInf(matriculeInf);
		criteria.setMatriculeSup(matriculeSup);

		if (selectedNoeudNode != null)
			criteria.setNoeudId(String.valueOf(((Noeud) selectedNoeudNode.getData()).getId()));
		if (selectedUnitNode != null)
			criteria.setSiteId(String.valueOf(((Unit) selectedUnitNode.getData()).getId()));
		if (this.selectedGroupId != null && !this.selectedGroupId.equals(""))
			criteria.setGroup(this.selectedGroupId);
		if(isSupH)
			criteria.setSuppHrhId(String.valueOf(usrAuthenticated.getId()));

		List<User> users = userService.readByCriteria(criteria);
		if (users == null || users.size() == 0)
			return;

		criteria = new CommonCriteria();
		criteria.setDateDebut(dateDebut);
		criteria.setDateFin(dateFin);

		List<Mouvement> mouvements;
		List<Retard> retards = null;
		List<Absence> absences = null;
		List<Conge> conges = null;
		for (User user : users) {
			criteria.setMatriculeInf(user.getMatricule());
			criteria.setMatriculeSup(user.getMatricule());
			mouvements = mouvementService.readByCriteria(criteria);
			if (isRetardVisible)
				retards = retardService.readByCriteria(criteria);
			if (isAbsenceVisible)
				absences = absenceService.readByCriteria(criteria);
			if (isCongeVisible)
				conges = congeService.readByCriteria(criteria);
			customPointages.add(new CustomPointageItem(user, mouvements, retards, absences, conges));
		}
	}

	private List<LineData> loadReportModel() throws Exception {
		List<LineData> datas=new ArrayList<LineData>();
		
		LineData linedata;
		List<LineDay> linedays;
		
		Calendar date=Calendar.getInstance();
		
		if(customPointages==null || customPointages.size()==0)
			loadCustomPointageModel();
		
		for(CustomPointageItem item:customPointages) {
			linedata=new LineData();
			linedata.setUser(item.user);
			
			linedays=new ArrayList<LineDay>();
			date.setTime(dateDebut);
			while (date.getTime().compareTo(dateFin)<=0) {
				List<String> mouvements=new ArrayList<String>();
				List<String> absences=new ArrayList<String>();
				List<String> conges=new ArrayList<String>();
				List<String> retards=new ArrayList<String>();
				List<String> justifications=new ArrayList<String>();
				
				for (Mouvement mvn : item.getMouvements()) {
					if(Utils.compareDateOnly(mvn.getDate(), date.getTime())==0){
						mouvements.add(Utils.convertDate2Str(mvn.getDate(), "HH:mm:ss"));
					}
				}
				
				for (Absence abs : item.getAbsences()) {
					if((Utils.compareDateOnly(abs.getDateDebut(), date.getTime())==0) 
							|| (Utils.compareDateOnly(abs.getDateReprise(), date.getTime())==0) 
							|| (Utils.compareDateOnly(abs.getDateReprise(), date.getTime())>0 && Utils.compareDateOnly(abs.getDateDebut(), date.getTime())<0) ) {
						absences.add(abs.getDuree()+"");
						justifications.add(" - "+typeAbsenceService.load(abs.getTypeAbsence().getId()).getLibelle());
					}
				}
				
				for (Retard rtr : item.getRetards()) {
					if(rtr.getDate().compareTo(date.getTime())==0) {
						retards.add(rtr.getRetardNonTolere()+rtr.getRetardTolere()+"");
						justifications.add(" - "+rtr.getDescription());
					}
				}
				
				for (Conge cng : item.getConges()) {
					if((Utils.compareDateOnly(cng.getDateDebut(), date.getTime())==0) 
							|| (Utils.compareDateOnly(cng.getDateReprise(), date.getTime())==0) 
							|| (Utils.compareDateOnly(cng.getDateReprise(), date.getTime())>0 && Utils.compareDateOnly(cng.getDateDebut(), date.getTime())<0) ) {
						conges.add(cng.getNbrJour()+"");
						justifications.add(" - "+typeCongeService.load(cng.getTypeConge().getId()).getLibelle());
					}
				}
				
				linedays.add(new LineDay(date.getTime(),mouvements,retards,absences,conges,justifications));
				
				date.add(Calendar.DAY_OF_MONTH, 1);
			}
			linedata.setLineDays(linedays);
			datas.add(linedata);
		}
		return datas;
	}
	
	public String converMinToTime(double d) {
		String result = "";
		int hours = ((Double) (d / 60)).intValue();
		int minuts = ((Double) (d % 60)).intValue();
		if (hours < 10)
			result += "0" + hours + ":";
		else
			result += "" + hours + ":";
		if (minuts < 10)
			result += "0" + minuts + ":00";
		else
			result += "" + minuts + ":00";
		return result;
	}

	public void printCustomPointage(String type) throws Exception {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		String jrxmlFile = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/jasperreports/pointage.jasper");
		Map<String, Object> parameters = new HashMap<String, Object>();

		if (dateDebut != null && dateFin != null) {
			if (dateFin.compareTo(dateDebut) >= 0) {
				parameters.put("dateDebut", dateFormat.format(dateDebut));
				parameters.put("dateFin", dateFormat.format(dateFin));
			} else {
				showMessage("error", "Date Début est après date fin !");
				return;
			}
		} else {
			return;
		}

		if (selectedNoeudNode != null)
			parameters.put("organigramme", ((Noeud) selectedNoeudNode.getData()).getId());
		if (selectedUnitNode != null)
			parameters.put("unit_fk", ((Unit) selectedUnitNode.getData()).getId());

		if(isSupH)
			parameters.put("MANAGER",usrAuthenticated.getId());

		parameters.put("WEBDIR", fc.getExternalContext().getRealPath("/"));

		if (matriculeInf != null && !matriculeInf.equals(""))
			parameters.put("matriculeInf", Integer.valueOf(matriculeInf));
		if (matriculeSup != null && !matriculeSup.equals(""))
			parameters.put("matriculeSup", Integer.valueOf(matriculeSup));
		if (selectedGroupId != null && !selectedGroupId.equals(""))
			parameters.put("group", Integer.valueOf(selectedGroupId));
		parameters.put("isAbsence", this.isAbsenceVisible);
		parameters.put("isConge", this.isCongeVisible);
		parameters.put("isRetard", this.isRetardVisible);

		File file = new File(jrxmlFile);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
				databaseOperations.getConnection());
		if (type.equalsIgnoreCase("xls")) {
			 JRXlsExporter exporterXLS = new JRXlsExporter();
			 exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
			 exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(ec.getResponseOutputStream()));
			 SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration(); 
			 configuration.setOnePagePerSheet(false);
			 configuration.setDetectCellType(false);
			 configuration.setCollapseRowSpan(false);
			 exporterXLS.setConfiguration(configuration);
			 exporterXLS.exportReport();
			 ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.xls")));
			 ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".xls"); 
			 fc.responseComplete();
		} else {
			ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.pdf")));
			ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, ec.getResponseOutputStream());
			fc.responseComplete();
		}
	}
	
	public void printGlobalPointageReport(String type) throws Exception {

		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<LineData> datas=loadReportModel();
		
		
		parameters.put("dataSource", datas);
		String jrxmlFile = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/jasperreports/pointages-report.jasper");
		
		if (dateDebut != null && dateFin != null) {
			if (dateFin.compareTo(dateDebut) >= 0) {
				parameters.put("dateDebut", dateFormat.format(dateDebut));
				parameters.put("dateFin", dateFormat.format(dateFin));
			} else {
				showMessage("error", "Date Début est après date fin !");
				return;
			}
		} else {
			return;
		}
		
		parameters.put("WEBDIR", fc.getExternalContext().getRealPath("/"));
		
		File file = new File(jrxmlFile);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,new JRBeanArrayDataSource(datas.toArray()));
		
		if (type.equalsIgnoreCase("xls")) {
			 JRXlsExporter exporterXLS = new JRXlsExporter();
			 exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
			 exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(ec.getResponseOutputStream()));
			 SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration(); 
			 configuration.setOnePagePerSheet(false);
			 configuration.setDetectCellType(false);
			 configuration.setCollapseRowSpan(false);
			 exporterXLS.setConfiguration(configuration);
			 exporterXLS.exportReport();
			 ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.xls")));
			 ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages-report2."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy HHmmss")+".xls"); 
			 fc.responseComplete();
		} else {
			ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.pdf")));
			ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages-report2."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy HHmmss")+".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, ec.getResponseOutputStream());
			fc.responseComplete();
		}
		/*
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		String jrxmlFile = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/jasperreports/pointages-report.jasper");
		Map<String, Object> parameters = new HashMap<String, Object>();

		if (dateDebut != null && dateFin != null) {
			if (dateFin.compareTo(dateDebut) >= 0) {
				parameters.put("dateDebut", dateFormat.format(dateDebut));
				parameters.put("dateFin", dateFormat.format(dateFin));
			} else {
				showMessage("error", "Date Début est après date fin !");
				return;
			}
		} else {
			return;
		}

		if (selectedNoeudNode != null)
			parameters.put("noeud", ((Noeud) selectedNoeudNode.getData()).getId());
		if (selectedUnitNode != null)
			parameters.put("unite", ((Unit) selectedUnitNode.getData()).getId());

		parameters.put("WEBDIR", fc.getExternalContext().getRealPath("/"));

		if (matriculeInf != null && !matriculeInf.equals(""))
			parameters.put("matriculeInf", Integer.valueOf(matriculeInf));
		if (matriculeSup != null && !matriculeSup.equals(""))
			parameters.put("matriculeSup", Integer.valueOf(matriculeSup));
		if (selectedGroupId != null && !selectedGroupId.equals(""))
			parameters.put("group", Integer.valueOf(selectedGroupId));
		
		if(isSupH)
			parameters.put("MANAGER",usrAuthenticated.getId());

		File file = new File(jrxmlFile);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
				databaseOperations.getConnection());
		if (type.equalsIgnoreCase("xls")) {
			 JRXlsExporter exporterXLS = new JRXlsExporter();
			 exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
			 exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(ec.getResponseOutputStream()));
			 SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration(); 
			 configuration.setOnePagePerSheet(false);
			 configuration.setDetectCellType(false);
			 configuration.setCollapseRowSpan(false);
			 exporterXLS.setConfiguration(configuration);
			 exporterXLS.exportReport();
			 ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.xls")));
			 ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages-report."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".xls"); 
			 fc.responseComplete();
		} else {
			ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.pdf")));
			ec.setResponseHeader("Content-Disposition", "attachment; filename=Pointages-report."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, ec.getResponseOutputStream());
			fc.responseComplete();
		}
		*/
	}

	public void showMessage(String type, String message) {
		if (type.equals("error")) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erreur!", message);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else if (type.equals("info")) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Information", message);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} else if (type.equals("warn")) {
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Attention ", message);
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	@SuppressWarnings("unchecked")
	public void loadUserDetails(Date currentDate) throws Exception {
		FacesContext fc = FacesContext.getCurrentInstance();
		Object obj = fc.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		if (obj != null && (obj instanceof User)) {
			User user = (User) obj;
			user = userService.load(user.getId());
			CommonCriteria crit = new CommonCriteria();
			crit.setMatriculeInf(user.getMatricule());
			crit.setMatriculeSup(user.getMatricule());
			crit.setDateDebut(currentDate);
			crit.setDateFin(currentDate);

			mouvementList = mouvementService.readByCriteria(crit);
			crit.setRetardTolEnabled(true);
			absencesList = absenceService.readByCriteria(crit);
			retardsList = retardService.readByCriteria(crit);
		}
	}

	public void resetCustomPointageFilter() {
		this.selectedGroupId = "";
		this.selectedNoeudNode = null;
		this.selectedUnitNode = null;
		this.matriculeInf = "";
		this.matriculeSup = "";
		this.dateDebut = new Date();
		this.dateFin = new Date();
		this.customPointagesModel = new ArrayList<CustomPointageItem>();
		this.customPointages = new ArrayList<CustomPointageItem>();
	}

	public String convertDurationFromDouble(double duree) {
		return ((int) duree) + " H : " + (int) ((duree - ((int) duree)) * 100) + " MIN";
	}

	public void setMois(List<String> mois) {
		this.mois = mois;
	}

	public String getMoisSelected() {
		moisSelected = mois.get(Calendar.getInstance().get(Calendar.MONTH));
		return moisSelected;
	}

	public void setMoisSelected(String moisSelected) {
		this.moisSelected = moisSelected;
	}

	public String getAnneeSelected() {
		return anneeSelected;
	}

	public void setAnneeSelected(String anneeSelected) {
		this.anneeSelected = anneeSelected;
	}

	public List<List<Pointage>> getPointages() {
		return pointages;
	}

	public void setPointages(List<List<Pointage>> pointages) {
		this.pointages = pointages;
	}

	public List<Date> getColonnes() {
		colonnes = new ArrayList<Date>();
		if (this.moisSelected == null)
			return colonnes;
		Calendar cal = new GregorianCalendar();
		if (dateSelected != null)
			cal.setTime(dateSelected);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		while (cal.get(Calendar.MONTH) == this.getMois().indexOf(this.moisSelected)) {
			colonnes.add(cal.getTime());
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return colonnes;
	}

	public void setColonnes(List<Date> colonnes) {
		this.colonnes = colonnes;
	}

	public List<Mouvement> getMouvements() {
		return mouvements;
	}

	public void setMouvements(List<Mouvement> mouvements) {
		this.mouvements = mouvements;
	}

	public AnnomalieManageableService getAnomalieService() {
		return anomalieService;
	}

	public void setAnomalieService(AnnomalieManageableService anomalieService) {
		this.anomalieService = anomalieService;
	}

	public boolean isRendered() {
		return rendered;
	}

	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	public int getNbre_jours() {
		return nbre_jours;
	}

	public void setNbre_jours(int nbre_jours) {
		this.nbre_jours = nbre_jours;
	}

	public boolean isOmp() {
		return omp;
	}

	public void setOmp(boolean omp) {
		this.omp = omp;
	}

	public boolean isTjr() {
		return tjr;
	}

	public void setTjr(boolean tjr) {
		this.tjr = tjr;
	}

	public boolean isNrt() {
		return nrt;
	}

	public void setNrt(boolean nrt) {
		this.nrt = nrt;
	}

	public boolean isNip() {
		return nip;
	}

	public void setNip(boolean nip) {
		this.nip = nip;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public MouvementManageableService getMouvementService() {
		return mouvementService;
	}

	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}

	public TerminalManageableService getTerminalService() {
		return terminalService;
	}

	public void setTerminalService(TerminalManageableService terminalService) {
		this.terminalService = terminalService;
	}

	public UnitManageableService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}

	public PlanningManageableService getPlanningService() {
		return planningService;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}

	public String getMoisCours() {
		return moisCours;
	}

	public void setMoisCours(String moisCours) {
		this.moisCours = moisCours;
	}

	public String format(java.util.Date d) {
		return new SimpleDateFormat("HH:mm:ss").format(d);
	}

	public Date getDateSelected() {
		return dateSelected;
	}

	public void setDateSelected(Date dateSelected) {
		this.dateSelected = dateSelected;
	}

	public Pointage getPointage() {
		return pointage;
	}

	public void setPointage(Pointage pointage) {
		this.pointage = pointage;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public void setHrSuppService(HeurSuppManageableService hrSuppService) {
		this.hrSuppService = hrSuppService;
	}

	public void setTypeAbsService(TypeAbsenceManageableService typeAbsService) {
		this.typeAbsService = typeAbsService;
	}

	public void setAnnotationService(AnnotationManageableService annotationService) {
		this.annotationService = annotationService;
	}

	public void setMouvement(Mouvement mouvement) {
		this.mouvement = mouvement;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public void setGroupService(GroupManageableService groupService) {
		this.groupService = groupService;
	}

	public DataTable getDataTable() {
		return dataTable;
	}

	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}

	public RetardManageableService getRetardService() {
		return retardService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		this.typeCongeService = typeCongeService;
	}
	
	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public NoeudManageableService getNoeudService() {
		return noeudService;
	}

	public void setNoeudService(NoeudManageableService noeudService) {
		this.noeudService = noeudService;
	}

	public List<Mouvement> getMouvementList() {
		return mouvementList;
	}

	public void setMouvementList(List<Mouvement> mouvementList) {
		this.mouvementList = mouvementList;
	}

	public List<Absence> getAbsencesList() {
		return absencesList;
	}

	public void setAbsencesList(List<Absence> absencesList) {
		this.absencesList = absencesList;
	}

	public List<Retard> getRetardsList() {
		return retardsList;
	}

	public void setRetardsList(List<Retard> retardsList) {
		this.retardsList = retardsList;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getMouvementImage() {
		return mouvementImage;
	}

	public void setMouvementImage(String mouvementImage) {
		this.mouvementImage = mouvementImage;
	}

	public Date getDateDebut() {
		if (dateDebut == null)
			dateDebut = Calendar.getInstance().getTime();
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		if (dateFin == null)
			dateFin = Calendar.getInstance().getTime();
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getMatriculeInf() {
		return matriculeInf;
	}

	public void setMatriculeInf(String matriculeInf) {
		this.matriculeInf = matriculeInf;
	}

	public String getMatriculeSup() {
		return matriculeSup;
	}

	public void setMatriculeSup(String matriculeSup) {
		this.matriculeSup = matriculeSup;
	}

	public TreeNode getSelectedNoeudNode() {
		return selectedNoeudNode;
	}

	public void setSelectedNoeudNode(TreeNode selectedNoeudNode) {
		this.selectedNoeudNode = selectedNoeudNode;
	}

	public TreeNode getSelectedUnitNode() {
		return selectedUnitNode;
	}

	public void setSelectedUnitNode(TreeNode selectedUnitNode) {
		this.selectedUnitNode = selectedUnitNode;
	}

	public String getSelectedGroupId() {
		return selectedGroupId;
	}

	public void setSelectedGroupId(String selectedGroupId) {
		this.selectedGroupId = selectedGroupId;
	}

	public List<CustomPointageItem> getCustomPointages() {
		return customPointages;
	}

	public boolean getIsRetardVisible() {
		return isRetardVisible;
	}

	public void setIsRetardVisible(boolean isRetardVisible) {
		this.isRetardVisible = isRetardVisible;
	}

	public boolean getIsAbsenceVisible() {
		return isAbsenceVisible;
	}

	public void setIsAbsenceVisible(boolean isAbsenceVisible) {
		this.isAbsenceVisible = isAbsenceVisible;
	}

	public boolean getIsCongeVisible() {
		return isCongeVisible;
	}

	public void setIsCongeVisible(boolean isCongeVisible) {
		this.isCongeVisible = isCongeVisible;
	}

	public void setCustomPointages(List<CustomPointageItem> customPointages) {
		this.customPointages = customPointages;
	}

	public List<CustomPointageItem> getCustomPointagesModel() {
		return customPointagesModel;
	}

	public void setCustomPointagesModel(List<CustomPointageItem> customPointagesModel) {
		this.customPointagesModel = customPointagesModel;
	}

	public TreeNode getSelectedNoeud() {
		return selectedNoeud;
	}

	public void setSelectedNoeud(TreeNode selectedNoeud) {
		this.selectedNoeud = selectedNoeud;
	}

	public TreeNode getSelectedUnit() {
		return selectedUnit;
	}

	public void setSelectedUnit(TreeNode selectedUnit) {
		this.selectedUnit = selectedUnit;
	}

	public String getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(String selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	/**
	 * Custom Classes
	 */
	public class CustomPointageItem {
		private User user;
		private List<Mouvement> mouvements;
		private List<Retard> retards;
		private List<Absence> absences;
		private List<Conge> conges;

		public CustomPointageItem() {

		}

		public CustomPointageItem(User user, List<Mouvement> mouvements, List<Retard> retards, List<Absence> absences,
				List<Conge> conges) {
			super();
			this.user = user;

			if (mouvements != null)
				this.mouvements = mouvements;
			else
				this.mouvements = new ArrayList<Mouvement>();

			if (retards != null)
				this.retards = retards;
			else
				this.retards = new ArrayList<Retard>();
			if (absences != null)
				this.absences = absences;
			else
				this.absences = new ArrayList<Absence>();
			if (conges != null)
				this.conges = conges;
			else
				this.conges = new ArrayList<Conge>();
		}

		public User getUser() {
			return user;
		}

		public void setUser(User user) {
			this.user = user;
		}

		public List<Mouvement> getMouvements() {
			return mouvements;
		}

		public void setMouvements(List<Mouvement> mouvements) {
			this.mouvements = mouvements;
		}

		public List<Retard> getRetards() {
			return retards;
		}

		public void setRetards(List<Retard> retards) {
			this.retards = retards;
		}

		public List<Absence> getAbsences() {
			return absences;
		}

		public void setAbsences(List<Absence> absences) {
			this.absences = absences;
		}

		public List<Conge> getConges() {
			return conges;
		}

		public void setConges(List<Conge> conges) {
			this.conges = conges;
		}
	}

	public static class Pointage {

		private String matricule;
		private String nom;
		private String prenom;
		private String badge;
		private String datePointage;
		private List<Annomalie> anomalies;
		private List<Mouvement> mouvements;
		private String anomalie;
		private List<String> colors = new ArrayList<String>();

		public Pointage() {
			super();
		}

		public Pointage(User user, Date datePointage, List<Annomalie> anomalies, List<Mouvement> mouvements) {
			super();
			this.matricule = user.getMatricule();
			this.nom = user.getNom();
			this.prenom = user.getPrenom();
			this.badge = user.getBadge();
			this.datePointage = Utils.convertDate2Str(datePointage, "dd MMM yyyy");
			this.anomalies = anomalies;
			this.mouvements = mouvements;
			if (anomalies != null && !anomalies.isEmpty())
				for (Annomalie a : anomalies)
					if (!colors.contains(a.getColor()))
						colors.add(a.getColor());
		}

		public String getDatePointage() {
			return datePointage;
		}

		public void setDatePointage(String datePointage) {
			this.datePointage = datePointage;
		}

		public String getAnomalie() {
			return anomalie;
		}

		public void setAnomalie(String anomalie) {
			this.anomalie = anomalie;
		}

		public String getMatricule() {
			return matricule;
		}

		public void setMatricule(String matricule) {
			this.matricule = matricule;
		}

		public List<String> getColors() {
			return colors;
		}

		public void setColors(List<String> colors) {
			this.colors = colors;
		}

		public List<Annomalie> getAnomalies() {
			return anomalies;
		}

		public void setAnomalies(List<Annomalie> anomalies) {
			this.anomalies = anomalies;
		}

		public List<Mouvement> getMouvements() {
			return mouvements;
		}

		public void setMouvements(List<Mouvement> mouvements) {
			this.mouvements = mouvements;
		}

		public String getNom() {
			return nom;
		}

		public void setNom(String nom) {
			this.nom = nom;
		}

		public String getPrenom() {
			return prenom;
		}

		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}

		public String getBadge() {
			return badge;
		}

		public void setBadge(String badge) {
			this.badge = badge;
		}
	}

	public Mouvement getMouvement() {
		return mouvement;
	}

}


