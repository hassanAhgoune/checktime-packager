package ma.nawarit.checker.beans;
import java.io.Serializable;  

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.primefaces.model.chart.PieChartModel;  
  
@ManagedBean(name = "chartBean")
@ViewScoped
public class ChartBean implements Serializable {  
  
    /**
	 * 
	 */
	private static final long serialVersionUID = -2372149228766680164L;
	
	private PieChartModel pieModel;  
    private CartesianChartModel categoryModel;
    
    public ChartBean() {  
        createPieModel();  
        createCategoryModel();  
    }  
  
    public CartesianChartModel getCategoryModel() {  
        return categoryModel;  
    }  
  
    public PieChartModel getPieModel() {  
        return pieModel;  
    }  
  
    private void createPieModel() {  
        pieModel = new PieChartModel();  
  
        pieModel.set("Respectants l'horaire", 300);  
        pieModel.set("En retrad toléré", 150);
        pieModel.set("En retard non toléré entre 20mn et 45mn", 35);
        pieModel.set("Dépassant le retard toléré de 01H:20Mn", 15);   
    }  
    
    private void createCategoryModel() {  
        categoryModel = new CartesianChartModel();  
  
        ChartSeries an1 = new ChartSeries();  
        an1.setLabel("Année 2012");  
  
        an1.set("JAN", 1340);  
        an1.set("FEV", 1000);  
        an1.set("MAR", 1440);  
        an1.set("AVR", 1500);  
        an1.set("MAI", 1600);  
        an1.set("JUN", 1540);  
        
        categoryModel.addSeries(an1);  
        
        ChartSeries an2 = new ChartSeries();  
        an2.setLabel("Année 2013");  
  
        an2.set("JAN", 1200);  
        an2.set("FEV", 1200);  
        an2.set("MAR", 1340);  
        an2.set("AVR", 1000);  
        an2.set("MAI", 1525);  
        an2.set("JUN", 1600);  
        
        categoryModel.addSeries(an2);
    }  
}
