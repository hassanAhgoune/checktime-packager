package ma.nawarit.checker.beans.jbpm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ma.nawarit.checker.beans.compagnie.collabo.LazySorter;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.jbpm.JbpmStatable;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 * Dummy implementation of LazyDataModel that uses a list to mimic a real
 * datasource like a database.
 */
public class JbpmDataModel extends LazyDataModel<JbpmStatable> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7114225919578628621L;
	private List<JbpmStatable> datasource;
	private List<JbpmStatable> data;

	public JbpmDataModel(List<JbpmStatable> jbpmEntities) {
		this.datasource = jbpmEntities;
		this.setWrappedData(this.datasource);
		
	}
	public int getRowCount() {
		return datasource.size();
	}

	@Override
	public JbpmStatable getRowData(String rowKey) {
		 for(JbpmStatable jbpm : datasource) {
			 if(jbpm.getId() == Integer.parseInt(rowKey))
				 return jbpm;
		 }
		 return null;
	}
	
	@Override
	public Object getRowKey(JbpmStatable jbpm) {
		return jbpm.getId();
	}

	public List<JbpmStatable> getItemsByRange(int firstRow, int endRow) {
		return this.datasource.subList(firstRow, endRow);
	}
	
	
	@Override
	public List<JbpmStatable> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		data = new ArrayList<JbpmStatable>();
        if (datasource != null)
	        for(JbpmStatable user : datasource) {
	            boolean match = true;
	            if (filters != null) {
	                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
	                    try {
	                        String filterProperty = it.next();
	                        Object filterValue = filters.get(filterProperty);
	                        String fieldValue = "";
	                        Field field;
	                        if (filterProperty.contains(".")) {
	                        	field = user.getClass().getSuperclass().getDeclaredField(filterProperty.substring(0, filterProperty.indexOf('.')));
	                            field.setAccessible(true);
	                            Object o = field.get(user);
	                            if (o != null) {
		                            if (o instanceof Unit) {
		                            	Unit u = (Unit)o;
		                            	field = u.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(u));
		                            } else if (o instanceof ProfilMetier) {
		                            	ProfilMetier p = (ProfilMetier)o;
		                            	p = Utils.deproxy(p, ProfilMetier.class);
		                            	field = p.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(p));
		                            } else if (o instanceof Planning) {
		                            	Planning p = (Planning)o;
		                            	p = Utils.deproxy(p, Planning.class);
		                            	field = p.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(p));
		                            } else if (o instanceof Noeud) {
		                            	Noeud n = (Noeud)o;
		                            	n = Utils.deproxy(n, Noeud.class);
		                            	field = n.getClass().getSuperclass().getDeclaredField(filterProperty.substring(filterProperty.indexOf('.') + 1));
		                                field.setAccessible(true);
		                            	fieldValue = String.valueOf(field.get(n));
		                            }
	                            }
	                        } else {
		                        field = user.getClass().getSuperclass().getDeclaredField(filterProperty);
		                        field.setAccessible(true);
		                        fieldValue = String.valueOf(field.get(user));
	                        }
	                        if (filterValue != null && filterValue.toString().contains("/")) {
	                        	String[] filterValues = ((String)filterValue).split("/");
	                        	match = false;
	                        	for (int i = 0; i < filterValues.length; i++) {
	                        		System.out.println(filterValues[i].toString());
	                        		if(fieldValue.toLowerCase().contains(filterValues[i].toLowerCase())) {
			                            match = true;
			                            break;
	                        		} 
	                        	}
	                        } else if(filterValue == null || fieldValue.toLowerCase().contains(filterValue.toString().toLowerCase())) {
		                            match = true;
		                    } else {
	                            match = false;
	                            break;
	                        }
	                    } catch(Exception e) {
	                    	e.printStackTrace();
	                        match = false;
	                    }
	                }
	            }
	 
	            if(match) {
	                data.add(user);
	            }
	        }
 
        //sort
        if(sortField != null) {
            Collections.sort(data, new JbpmLazySorter(sortField, sortOrder));
        }
        
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
        
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
	}
	
	
	
	public boolean isEmpty() {
		return (this.datasource == null || this.datasource.isEmpty());
	}
	
}
