package ma.nawarit.checker.beans.checkaccess;
 
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import ma.nawarit.checker.batch.exec.ExecCmdUtils;
import ma.nawarit.checker.batch.hard.TerminalOrderService;
import ma.nawarit.checker.checkaccess.Porte;
import ma.nawarit.checker.checkaccess.PorteImpl;
import ma.nawarit.checker.checkaccess.crud.PorteManageableService;
import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.equipement.crud.TerminalManageableService;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.utils.UploadUtils;

import org.apache.log4j.Logger;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;

/**
 * Nom du projet : Checktime ( Module CheckAccess)
 * Version : 1.0
 * @author : M.Morabit
 * Date : Juin 2014
 */
@ManagedBean(name = "checkaccessBean")
@ViewScoped
public class CheckaccessBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<CheckaccessBean.dispo> dispositifs = new ArrayList<CheckaccessBean.dispo>();
	private TreeNode treeNode;  
	private TreeNode selectedNode;  
	private String has_plan;
	private int plan_width;
	private int plan_height;
	private UploadedFile plan;
	List<Terminal> dispobyunit;
	private String id_dispo;
	private String selectedUnit;
	private List<Porte> portes = new ArrayList<Porte>();
//	public CheckaccessBean.details terminal_details = new details();
	
	
	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;

	@ManagedProperty(value = "#{TerminalManageableService}")
	private TerminalManageableService terminalService;
	
	@ManagedProperty(value = "#{TerminalOrderService}")
	private TerminalOrderService terminalOrderService;

	@ManagedProperty(value = "#{PorteManageableService}")
	private PorteManageableService porteService;
	
	
	
	public void charger_dispo()
	{
		RequestContext.getCurrentInstance().execute("PF('dispositif').show()");	
	}
	public void fermer_dispo()
	{
		RequestContext.getCurrentInstance().execute("PF('dispositif').hide()");		
	}
	public void init_dispo()
	{
		dispositifs.clear();
	}
	public void charger_portes()
	{
		Unit u;
		try {
			u = unitService.load(Integer.valueOf(this.getSelectedNode().toString()));
		dispobyunit= new ArrayList<Terminal>();
		List<Terminal> tmp = terminalService.readAll();
		List<Porte> prt = porteService.readAll();
		
		portes.clear();
		for(Terminal p : tmp)
		{
			if(p.getUnit().getId()==u.getId())
				dispobyunit.add(p);
		}
		for(Terminal p : dispobyunit)
		{
			for(Porte pr : prt)
			{
				if(pr.getTerminal().getId()==p.getId())
					portes.add(pr);
			}
		}
		
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void clean_portes()
	{
			charger_portes();
			for(Porte pr : portes)
			{
					try {
						porteService.delete(pr);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
		
	}
	public void ajouter_dispo()
	{
		try {
			Porte p = new PorteImpl();
			p.setPosx(Float.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posx").split("px")[0]));
			p.setPosy(Float.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posy").split("px")[0]));
			p.setTerminal(terminalService.load(Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id").substring(5).split("_")[0])));
			porteService.create(p);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//afficher_contenu();
	}
	
//	public void details_dispo()
//	{
//		int num =Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id_dispo"));
//		try {
//				terminal_details = new details(terminalService.load(num));
//				RequestContext.getCurrentInstance().update("form:details_corp");
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	public void ouvrir_porte()
	{
//		terminalOrderService. 
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage( "Notification ","Porte ouverte pour 5 secondes !") );
        RequestContext.getCurrentInstance().update("form:growl");
	}
	public void verouiller_porte()
	{
//		Object[] cmdParams = cmdParams = new Object[]{this.terminal_details.getTerminal().getIpAdress(),u.getMatricule(),u.getBadge()};
//		terminalOrderService.doLock(cmdParams);
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Notification","Terminal verouill�") );
        RequestContext.getCurrentInstance().update("form:growl");
	}
	public void deverouiller_porte()
	{
//		action_terminal("deverouiller");
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Notification","Terminal d�verouill�") );
        RequestContext.getCurrentInstance().update("form:growl");
	}
	public void eteindre_terminal()
	{
//		action_terminal("eteindre");
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Notification","Terminal �teint") );
        RequestContext.getCurrentInstance().update("form:growl");
	}
	public void clear_logs()
	{
		System.out.println("clear des logs");
//		action_terminal("clear");
		FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage("Notification","Liste des mouvements supprim�s") );
        RequestContext.getCurrentInstance().update("form:growl");
	}
	

	
	public void modifier_dispo()
	{
		
		CheckaccessBean.dispo dispo = new CheckaccessBean.dispo();
		dispo.setId(Integer.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id").substring(5)));
		dispo.setX(Float.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posx")));
		dispo.setY(Float.valueOf(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("posy")));
		Iterator<CheckaccessBean.dispo> i =dispositifs.iterator();
		while(i.hasNext())
		{
			CheckaccessBean.dispo d = i.next();
			if(d.getId()==dispo.getId())
			{
				d.setX(dispo.getX());
				d.setY(dispo.getY());
			}
		}
		afficher_contenu();
	}
	
	public void afficher_contenu()
	{
		System.out.println("======== Liste des dispositifs et leurs positions =========");
		for(CheckaccessBean.dispo d:dispositifs)
		{
			System.out.println(d);
		}
		System.out.println("===========================================================");
	}
	
	
	
	public TreeNode getTreeNode() {
		Unit parent;
		try {
			this.treeNode = new DefaultTreeNode("ROOT", null);
			parent = unitService.load(new java.lang.Integer(27));
			Unit nda = new UnitImpl();
			BeanUtils.copyProperties(parent, nda);
			this.buildTree(new DefaultTreeNode(nda, this.treeNode));
			this.treeNode.setExpanded(true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return this.treeNode;
	}
	
	public void buildTree(TreeNode node) {
		Unit n = (Unit) node.getData();
		if (n.getUnits() != null) {
			TreeNode nodeImpl = null;
			Unit nda;
			for (Unit nd : n.getUnits()) {
				nda = new UnitImpl();
				BeanUtils.copyProperties(nd, nda);
				nodeImpl = new DefaultTreeNode(nda, node);
				this.buildTree(nodeImpl);
				nodeImpl.setExpanded(true);
			}
		}
	}

	public void onNodeSelect(NodeSelectEvent event)
	{
		
		try {
			Unit u = unitService.load(Integer.valueOf(this.getSelectedNode().toString()));
			has_plan = u.getPlan();
			selectedUnit = u.getLibelle();
			
			charger_portes();
			for(Terminal p : dispobyunit)
				System.out.println("Dispositif : "+p.getCode());
			
			FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String lien = rootpath + "upload/" +has_plan;
			
			BufferedImage image = ImageIO.read(new File(lien));
			System.out.println("Image :"+has_plan+", Width : "+image.getWidth()+", Height : "+image.getHeight());
			this.plan_width = image.getWidth();
			this.plan_height = image.getHeight();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(javax.imageio.IIOException e){
			System.out.println("Erreur : Image de l'unit� selectionn�e introuvable !!!");
			
		}catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void upload(FileUploadEvent event) {         
		System.out.println("methode upload "+event.getFile().getFileName()+" pour l unite "+this.getSelectedNode().toString());
    	System.out.println("extension : "+event.getFile().getFileName().substring(event.getFile().getFileName().length()-3));
    	try {
            
        	FacesContext context = FacesContext.getCurrentInstance();
			ServletContext scontext = (ServletContext)context.getExternalContext().getContext();
			String rootpath = scontext.getRealPath("/");
			String fileType = event.getFile().getContentType();
			String file = rootpath + "upload/unit_"+this.getSelectedNode().toString()+"."+fileType.substring(fileType.indexOf("/") + 1);
			if(event.getFile() != null){
				BufferedInputStream bis = new BufferedInputStream(event.getFile().getInputstream());
				UploadUtils.uploadFile(bis, file);
			}
			
             try {
				Unit u = unitService.load(Integer.valueOf(this.getSelectedNode().toString()));
				u.setPlan("unit_"+this.getSelectedNode().toString()+"."+fileType.substring(fileType.indexOf("/") + 1));
				unitService.update(u);
				has_plan = u.getPlan();
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
             
 			BufferedImage image = ImageIO.read(new File(file));
 			System.out.println("Image :"+has_plan+", Width : "+image.getWidth()+", Height : "+image.getHeight());
 			this.plan_width = image.getWidth();
 			this.plan_height = image.getHeight();
 			
     		
         } catch (IOException e) {
             e.printStackTrace();
         } catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
    }
	
	
	
	
//	
//	public CheckaccessBean.details getTerminal_details() {
//		return terminal_details;
//	}
//	public void setTerminal_details(CheckaccessBean.details terminal_details) {
//		this.terminal_details = terminal_details;
//	}
	public List<Porte> getPortes() {
		return portes;
	}
	public void setPortes(List<Porte> portes) {
		this.portes = portes;
	}
	public PorteManageableService getPorteService() {
		return porteService;
	}
	public void setPorteService(PorteManageableService porteService) {
		this.porteService = porteService;
	}
	public String getSelectedUnit() {
		return selectedUnit;
	}

	public void setSelectedUnit(String selectedUnit) {
		this.selectedUnit = selectedUnit;
	}

	public String getId_dispo() {
		return id_dispo;
	}

	public void setId_dispo(String id_dispo) {
		this.id_dispo = id_dispo;
	}

	public UploadedFile getPlan() {
		return plan;
	}

	public void setPlan(UploadedFile plan) {
		this.plan = plan;
	}

	public List<Terminal> getDispobyunit() {
		return dispobyunit;
	}

	public void setDispobyunit(List<Terminal> dispobyunit) {
		this.dispobyunit = dispobyunit;
	}

	public TerminalManageableService getTerminalService() {
		return terminalService;
	}

	public void setTerminalService(TerminalManageableService terminalService) {
		this.terminalService = terminalService;
	}

	public int getPlan_width() {
		return plan_width;
	}

	public void setPlan_width(int plan_width) {
		this.plan_width = plan_width;
	}

	public int getPlan_height() {
		return plan_height;
	}

	public void setPlan_height(int plan_height) {
		this.plan_height = plan_height;
	}

	public String getHas_plan() {
		return has_plan;
	}


	public void setHas_plan(String has_plan) {
		this.has_plan = has_plan;
	}
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public UnitManageableService getUnitService() {
		return unitService;
	}

	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}

	public void setTreeNode(TreeNode treeNode) {
		this.treeNode = treeNode;
	}

	public List<CheckaccessBean.dispo> getDispositifs() {
		return dispositifs;
	}
	public void setDispositifs(List<CheckaccessBean.dispo> dispositifs) {
		this.dispositifs = dispositifs;
	}
	
	
	public TerminalOrderService getTerminalOrderService() {
		return terminalOrderService;
	}
	public void setTerminalOrderService(TerminalOrderService terminalOrderService) {
		this.terminalOrderService = terminalOrderService;
	}

	public static class dispo
	{
		private int id;
		private float x;
		private float y;
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public float getX() {
			return x;
		}
		public void setX(float x) {
			this.x = x;
		}
		public float getY() {
			return y;
		}
		public void setY(float y) {
			this.y = y;
		}
		public dispo(int id, float x, float y) {
			super();
			this.id = id;
			this.x = x;
			this.y = y;
		}
		public dispo() {
			super();
		}
		@Override
		public String toString() {
			return "dispo [id=" + id + ", x=" + x + ", y=" + y + "]";
		}
		
		
	}
	
	public static class details
	{
		public Terminal terminal;
		public String connexion;
		public String mac;
		public String serial;
		public String version;
		public String etat_porte;
		public List<Utilisateur>utilisateurs=new ArrayList<Utilisateur>();
		
		public details(Terminal p) {
			super();
			this.terminal = p;
				Logger log = Logger.getLogger(ExecCmdUtils.class);
		 	   StringBuffer buffer= new StringBuffer();
		 	   //System.out.println(System.getProperty("user.dir"));
		 	   ExecCmdUtils.exec(System.getProperty("user.dir")+"\\terminal.exe details "+this.terminal.getIpAdress()+" "+this.terminal.getCode(),
		 			   log, buffer);
		 	   String infos[]=buffer.toString().split(";");
		 	   this.connexion = (infos[0].startsWith("Connect")?"Connecte":"Deconnecté");
		 	   if(this.connexion.equals("Connecte"))
		 	   {
		 	   this.mac = infos[1];
		 	   this.serial = infos[2];
		 	   this.version = infos[3];
		 	   this.etat_porte= infos[4];
		 	   System.out.println(infos[0]+" "+infos[1]+" "+infos[2]+" "+infos[3]);
		 	   }
		}

		public void users_log()
		{
			utilisateurs.clear();
			Logger log = Logger.getLogger(ExecCmdUtils.class);
		 	   StringBuffer buffer= new StringBuffer();
		 	   System.out.println(System.getProperty("user.dir"));
		 	   ExecCmdUtils.exec(System.getProperty("user.dir")+"\\terminal.exe users "+this.terminal.getIpAdress()+" "+this.terminal.getCode(),
		 			   log, buffer);
		 	   String infos[]=buffer.toString().split(";"); // case 0 contenant les utilisateurs separ�s par virgule et case 1 contenant les logs separ�s par virgules
		 	   
		 	   String users[]=infos[0].split(","); // contient les utilisateurs
		 	  for(int i=0;i<users.length;i++)
		 	   {
		 		   String utilisateur[] = users[i].split(" ");
		 		   utilisateurs.add(new Utilisateur(Integer.parseInt(utilisateur[0]),utilisateur[1],((Integer.parseInt(utilisateur[2])==0)?"Utilisateur":"Administrateur"),utilisateur[3].equalsIgnoreCase("True")?"Autoris�":"Non autoris�"));
		 	   }
		 	  
		 	   try
		 	   {
		 	   String logs[] = infos[1].split(","); // contient les logs de pointage
		 	  int num;
		 	  for(int i=0;i<utilisateurs.size();i++)
		 	   {
		 	   		for(int j=0;j<logs.length;j++)
		 	   		{
		 	   			num =Integer.parseInt(logs[j].split(" ")[0]);
		 	   			if(num==utilisateurs.get(i).getNum())
		 	   			{
		 	   				try {
		 	   					
								utilisateurs.get(i).getMouvements().add(new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").parse(logs[j].split(" ")[1]+" "+logs[j].split(" ")[2]));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
		 	   			}
		 	   		}
		 	   } 
		 	  }catch(ArrayIndexOutOfBoundsException er){}
		 	 for(int i=0;i<utilisateurs.size();i++)
		 	   { 
		 	   System.out.println(utilisateurs.get(i));
		 	   }
		 	
		 	RequestContext.getCurrentInstance().update("form:userDialog");
		 	
		 	
		}
		
		public details() {
			super();
		}

		

		public List<Utilisateur> getUtilisateurs() {
			return utilisateurs;
		}



		public void setUtilisateurs(List<Utilisateur> utilisateurs) {
			this.utilisateurs = utilisateurs;
		}



		public String getEtat_porte() {
			return etat_porte;
		}



		public void setEtat_porte(String etat_porte) {
			this.etat_porte = etat_porte;
		}



		public Terminal getTerminal() {
			return terminal;
		}

		public void setTerminal(Terminal terminal) {
			this.terminal = terminal;
		}

		public String getConnexion() {
			return connexion;
		}

		public void setConnexion(String connexion) {
			this.connexion = connexion;
		}
		public String getMac() {
			return mac;
		}
		public void setMac(String mac) {
			this.mac = mac;
		}
		public String getSerial() {
			return serial;
		}
		public void setSerial(String serial) {
			this.serial = serial;
		}
		public String getVersion() {
			return version;
		}
		public void setVersion(String version) {
			this.version = version;
		}
		
		
		
		
	}
	
	
	public TimeZone getTimeZone() {
	    return TimeZone.getDefault();
	}
	
}



