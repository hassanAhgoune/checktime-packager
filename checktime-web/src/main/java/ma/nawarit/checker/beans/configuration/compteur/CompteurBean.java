package ma.nawarit.checker.beans.configuration.compteur;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.AnnotationImpl;
import ma.nawarit.checker.configuration.TypeAnnotation;
import ma.nawarit.checker.configuration.TypeAnnotationImpl;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableServiceBase;
import ma.nawarit.checker.configuration.crud.TypeAnnotationManageableService;
import ma.nawarit.checker.configuration.crud.TypeAnnotationManageableServiceBase;
import ma.nawarit.checker.suivi.TypeAbsence;
import org.primefaces.event.RowEditEvent;

@ManagedBean(name = "compteurBean")
@SessionScoped
public class CompteurBean {
	
	@ManagedProperty(value = "#{AnnotationManageableService}")
	private AnnotationManageableService annotationServise = new AnnotationManageableServiceBase();
	
	@ManagedProperty(value = "#{TypeAnnotationManageableService}")
	private TypeAnnotationManageableService typeAnnotationServise = new TypeAnnotationManageableServiceBase();	
		
	private List<Annotation> annotations = new ArrayList<Annotation>();
	private List<TypeAnnotation> typesAnnotation = new ArrayList<TypeAnnotation>();
	private TypeAnnotation typeAnnotation;
	private Annotation annotation = new ma.nawarit.checker.configuration.AnnotationImpl();
	private Annotation selectedCompteur;
	FacesContext context;
	private String editeCompteur;
	
	public CompteurBean() {

		context = FacesContext.getCurrentInstance();
		typeAnnotation = new TypeAnnotationImpl();
	}

	public void saveCompteur() throws Exception {
		
		if (typeAnnotation != null) {
			annotation.setTypeAnnotation(typeAnnotationServise.load(typeAnnotation.getId()));
		}
		annotationServise.create(annotation);
		annotations.add(annotation);
		annotation = new AnnotationImpl();
	}	
	
	public void cleanCompteur() {
		annotation = new ma.nawarit.checker.configuration.AnnotationImpl();
	}	
	
	public void updateCompteur(RowEditEvent event) throws Exception {

		annotation = ((Annotation) event.getObject());
		if (typeAnnotation != null) {
			annotation.setTypeAnnotation(typeAnnotationServise.load(typeAnnotation.getId()));
		}
		annotationServise.update(annotation);
	}    
	
	public void selectedDeleteCompteur(RowEditEvent event) throws Exception{
		
		selectedCompteur= ((Annotation) event.getObject());
	}
	public void deleteCompteur() throws Exception{
		if(selectedCompteur!= null){
			annotationServise.delete(selectedCompteur);
			annotations.remove(selectedCompteur);
			selectedCompteur = new AnnotationImpl();
		}
	}

	
	public TypeAnnotation getTypeByLibelle(String libelle) {
		
		TypeAnnotation type = null;
		try {
			Hashtable<String, String> props = new Hashtable<String, String>();
			props.put("libelle", libelle);
			List<TypeAnnotation> types = typeAnnotationServise.read(props);
			type = types.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return type;
	}	

	public AnnotationManageableService getAnnotationServise() {
		return annotationServise;
	}

	public void setAnnotationServise(AnnotationManageableService annotationServise) {
		this.annotationServise = annotationServise;
	}

	public List<Annotation> getAnnotations() {
		try {
			if (annotations == null || annotations.isEmpty()) {
				annotations = annotationServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annotations;
	}

	public void setAnnotations(List<Annotation> annotations) {
		this.annotations = annotations;
	}

	public Annotation getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public List<TypeAnnotation> getTypesAnnotation() {
		try {
			if (typesAnnotation == null || typesAnnotation.isEmpty()) {
				typesAnnotation = typeAnnotationServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesAnnotation;
	}

	public void setTypesAnnotation(List<TypeAnnotation> typesAnnotation) {
		this.typesAnnotation = typesAnnotation;
	}

	public TypeAnnotationManageableService getTypeAnnotationServise() {
		return typeAnnotationServise;
	}

	public void setTypeAnnotationServise(
			TypeAnnotationManageableService typeAnnotationServise) {
		this.typeAnnotationServise = typeAnnotationServise;
	}

	public TypeAnnotation getTypeAnnotation() {

		if(selectedCompteur!= null && selectedCompteur.getTypeAnnotation() != null){
			typeAnnotation = selectedCompteur.getTypeAnnotation();
		}
		return typeAnnotation;
	}

	public void setTypeAnnotation(TypeAnnotation typeAnnotation) {
		this.typeAnnotation = typeAnnotation;
	}

	public Annotation getSelectedCompteur() {
		return selectedCompteur;
	}

	public void setSelectedCompteur(Annotation selectedCompteur) {
		this.selectedCompteur = selectedCompteur;
	}

	public String getEditeCompteur() {
		if(selectedCompteur!= null && selectedCompteur.getTypeAnnotation()!=null){
			editeCompteur = selectedCompteur.getTypeAnnotation().getLibelle();
		}
		return editeCompteur;
	}

	public void setEditeCompteur(String editeCompteur) {
		this.editeCompteur = editeCompteur;
	}
}