package ma.nawarit.checker.beans.checkaccess;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Utilisateur
{
	public int num;
	public String nom;
	public String fonction;
	public String autorisation;
	public List<Date>mouvements= new ArrayList<Date>();
	
	
	
	public Utilisateur() {
		super();
	}
	public Utilisateur(int num, String nom, String fonction, String autorisation) {
		super();
		this.num = num;
		this.nom = nom;
		this.fonction = fonction;
		this.autorisation = autorisation;
	}
	
	
	
	
	@Override
	public String toString() {
		return "Utilisateur [num=" + num + ", nom=" + nom + ", fonction="
				+ fonction + ", autorisation=" + autorisation + ", mouvements="
				+ mouvements + "]";
	}
	public List<Date> getMouvements() {
		return mouvements;
	}
	public void setMouvements(List<Date> mouvements) {
		this.mouvements = mouvements;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getFonction() {
		return fonction;
	}
	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	public String getAutorisation() {
		return autorisation;
	}
	public void setAutorisation(String autorisation) {
		this.autorisation = autorisation;
	}
	
	
	
}


