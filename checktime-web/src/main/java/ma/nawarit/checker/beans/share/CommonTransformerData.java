package ma.nawarit.checker.beans.share;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.core.common.CommonReportData;
import ma.nawarit.checker.core.common.CrossData;
import ma.nawarit.checker.core.common.NoeudReportData;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.Retard;


public class CommonTransformerData {
	private static double cumul =0;
	private static int cumulRtdTol =0;
	private static int cumulRtdNonTol =0;
	public static List<CommonReportData> getAnoGroupedList(List<Annomalie> inList){
		if(inList != null){
			Iterator<Annomalie> it = inList.iterator();
			Annomalie anno = null;
			User user = null;
			List<CommonReportData> results = new ArrayList<CommonReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			CommonReportData data;

			while(it.hasNext()){
				anno = it.next();
				user = anno.getUser();
				if(user != null && user.getId() != 0 && (!hash.containsKey(""+user.getId()))){
					hash.put(""+user.getId(), ""+user.getId());
					data = new CommonReportData();
					data.setUser(user);
					data.setResults(searchAnomalieListByUser(user.getId(),inList));
					results.add(data);
				}
			
			}
			return results;
		}
		return null;
	}
	
	public static List<CommonReportData> getAbsGroupedList(List<Absence> inList){
		if(inList != null){
			Iterator<Absence> it = inList.iterator();
			Absence abs = null;
			User user = null;
			List<CommonReportData> results = new ArrayList<CommonReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			CommonReportData data;
			
			while(it.hasNext()){
				abs = it.next();
				user = abs.getCollaborateur();
				if(user == null || (!hash.containsKey(""+user.getId()))){
					hash.put(""+user.getId(), ""+user.getId());
					
					data = new CommonReportData();
					data.setUser(user);
					data.setResults(searchAbsenceListByUser(user.getId(),inList));
					data.setCumulAbsenceAuto(""+cumul+"H");
					results.add(data);
				}
			
			}
			
			return results;
		}
		return null;
	}
	public static List<CommonReportData> getCngGroupedList(List<Conge> inList){
		if(inList != null){
			Iterator<Conge> it = inList.iterator();
			Conge cng = null;
			User user = null;
			List<CommonReportData> results = new ArrayList<CommonReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			CommonReportData data;
			
			while(it.hasNext()){
				cng = it.next();
				user = cng.getCollaborateur();
				if(user == null || (!hash.containsKey(""+user.getId()))){
					hash.put(""+user.getId(), ""+user.getId());
					
					data = new CommonReportData();
					data.setUser(user);
					data.setResults(searchCongeListByUser(user.getId(),inList));
					data.setCumulCongePaye(Utils.convertTime2DayHour(cumul));
					results.add(data);
				}
			
			}
			
			return results;
		}
		return null;
	}
	public static List<CommonReportData> getIntrpGroupedList(List<Interruption> inList){
		if(inList != null){
			Iterator<Interruption> it = inList.iterator();
			Interruption intr = null;
			User user = null;
			List<CommonReportData> results = new ArrayList<CommonReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			CommonReportData data;
			
			while(it.hasNext()){
				intr = it.next();
				user = intr.getUser();
				if(user == null || (!hash.containsKey(""+user.getId()))){
					hash.put(""+user.getId(), ""+user.getId());
					data = new CommonReportData();
					data.setUser(user);
					data.setResults(searchIntrListByUser(user.getId(),inList));
					data.setCumulInterruption(""+cumul);
					results.add(data);
				}
			
			}
			
			return results;
		}
		return null;
	}
	public static List<CommonReportData> getRetardGroupedList(List<Retard> inList){
		if(inList != null){
			Iterator<Retard> it = inList.iterator();
			Retard rtd = null;
			User user = null;
			List<CommonReportData> results = new ArrayList<CommonReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			CommonReportData data;
			
			while(it.hasNext()){
				rtd = it.next();
				user = rtd.getUser();
				if(user == null || (!hash.containsKey(""+user.getId()))){
					hash.put(""+user.getId(), ""+user.getId());
					data = new CommonReportData();
					data.setUser(user);
					data.setResults(searchRtdListByUser(user.getId(),inList));
					data.setCumulRetardTolere(""+cumulRtdTol );
					data.setCumulRetardNonTolere(""+cumulRtdNonTol );
					results.add(data);
				}
			
			}
			
			return results;
		}
		return null;
	}
	
	public static List<NoeudReportData> getNodeGroupedList(List<User> inList){
		if(inList != null){
			Iterator<User> it = inList.iterator();
			User user = null;
			Noeud node = null;
			List<NoeudReportData> results = new ArrayList<NoeudReportData>();
			HashMap<String,String> hash = new HashMap<String, String>();
			NoeudReportData data;
			
			while(it.hasNext()){
				user = it.next();
				node = user.getNoeud();
				if(node != null && (!hash.containsKey(""+node.getId()))){
					hash.put(""+node.getId(), ""+node.getId());
					
					data = new NoeudReportData();
					data.setNode(node);
					data.setUsers(searchUserListByNode(node.getId(),inList));
					results.add(data);
				}
			
			}
			
			return results;
		}
		return null;
	}
	private static List searchUserListByNode(int nodeId, List inList){
		if(inList != null){
			Iterator<User> it = inList.iterator();
			User user = null;
			List<User> results = new ArrayList<User>();
			Noeud node;
			while(it.hasNext()){
				user = it.next();
				node = user.getNoeud(); 
				if(node!=null){
					if(nodeId == user.getNoeud().getId()){
						results.add(user);
					}
				}
			}
			return results;
		}
		return null;
	}
	private static List searchAnomalieListByUser(int userId, List inList){
		
		if(inList != null){
			Iterator<Annomalie> it = inList.iterator();
			Annomalie anno = null;
			List<Annomalie> results = new ArrayList<Annomalie>();
			while(it.hasNext()){
				anno = it.next();
				if(anno.getUser()!=null && (userId == anno.getUser().getId())){
					results.add(anno);
				}
			}
			return results;
		}
		return null;
	}
	
	private static List searchAbsenceListByUser(int userId, List inList){
		
		if(inList != null){
			Iterator<Absence> it = inList.iterator();
			Absence abs = null;
			List<Absence> results = new ArrayList<Absence>();
			cumul = 0;
			while(it.hasNext()){
				abs = it.next();
				if(userId == abs.getCollaborateur().getId()){
					results.add(abs);
					cumul +=abs.getDuree();
				}
			}
			return results;
		}
		return null;
	}
	private static List searchCongeListByUser(int userId, List inList){
		
		if(inList != null){
			Iterator<Conge> it = inList.iterator();
			Conge cng = null;
			List<Conge> results = new ArrayList<Conge>();
			cumul = 0;
			while(it.hasNext()){
				cng = it.next();
				if(userId == cng.getCollaborateur().getId()){
					results.add(cng);
					cumul +=cng.getNbrJour();
					
				}
			}
			return results;
		}
		return null;
	}
	private static List searchIntrListByUser(int userId, List inList){
		
		if(inList != null){
			Iterator<Interruption> it = inList.iterator();
			Interruption intr = null;
			List<Interruption> results = new ArrayList<Interruption>();
			cumul = 0;
			while(it.hasNext()){
				intr = it.next();
				if(userId == intr.getUser().getId()){
					results.add(intr);
					cumul +=intr.getDuree();
				}
			}
			return results;
		}
		return null;
	}
	private static List searchRtdListByUser(int userId, List inList){
		
		if(inList != null){
			Iterator<Retard> it = inList.iterator();
			Retard rtd = null;
			List<Retard> results = new ArrayList<Retard>();
			cumulRtdTol = 0;
			cumulRtdNonTol = 0;
			while(it.hasNext()){
				rtd = it.next();
				if(userId == rtd.getUser().getId()){
					results.add(rtd);
					cumulRtdTol +=rtd.getRetardTolere();
					cumulRtdNonTol +=rtd.getRetardNonTolere();
				}
			}
			return results;
		}
		return null;
	}
	
	public static List sortByDate(List inList, final boolean ascending) {
		Comparator comparator = new Comparator() {
			public int compare(Object o1, Object o2) {
				if (o1 instanceof Mouvement && o2 instanceof Mouvement) {
					Mouvement m1 = (Mouvement) o1;
					Mouvement m2 = (Mouvement) o2;
					return ascending ? m1.getDate().compareTo(m2.getDate())
							: m2.getDate().compareTo(m1.getDate());
					
				} else if (o1 instanceof CrossData && o2 instanceof CrossData) {
					CrossData m1 = (CrossData) o1;
					CrossData m2 = (CrossData) o2;
					return ascending ? m1.getDate().compareTo(m2.getDate())
							: m2.getDate().compareTo(m1.getDate());
					
				} else
					return 0;
				 
				
			}
		};
		Collections.sort(inList, comparator);
		return inList;
	}
}