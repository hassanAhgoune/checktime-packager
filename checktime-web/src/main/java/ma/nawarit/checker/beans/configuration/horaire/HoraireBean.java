package ma.nawarit.checker.beans.configuration.horaire;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import ma.nawarit.checker.common.Utils;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.AnnotationImpl;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.configuration.PlageHoraire;
import ma.nawarit.checker.configuration.PlageHoraireImpl;
import ma.nawarit.checker.configuration.SmplHoraire;
import ma.nawarit.checker.configuration.SmplHoraireImpl;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableServiceBase;
import ma.nawarit.checker.configuration.crud.HoraireManageableService;
import ma.nawarit.checker.configuration.crud.HoraireManageableServiceBase;
import ma.nawarit.checker.configuration.crud.PlageHoraireManageableService;
import ma.nawarit.checker.configuration.crud.PlageHoraireManageableServiceBase;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;

@ManagedBean(name = "horaireBean")
@ViewScoped
public class HoraireBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{HoraireManageableService}")
	private HoraireManageableService horaireServise = new HoraireManageableServiceBase();

	@ManagedProperty(value = "#{AnnotationManageableService}")
	private AnnotationManageableService annotationService = new AnnotationManageableServiceBase();

	@ManagedProperty(value = "#{PlageHoraireManageableService}")
	private PlageHoraireManageableService plageHoraireService = new PlageHoraireManageableServiceBase();
	
	FacesContext context = FacesContext.getCurrentInstance();

	private List<Horaire> horaires = new ArrayList<Horaire>();
	private Horaire horaire;
	private PlageHoraire plageHoraire;
	private String horaireId;

	private List<Annotation> annotations = new ArrayList<Annotation>();

	private ScheduleModel eventModel;

	private boolean editMode = true;
	
	private boolean editHor;

	/**
	 * Constructeur
	 * 
	 * @throws Exception
	 */
	public HoraireBean() throws Exception {

		horaires = new ArrayList<Horaire>();
	}

	/**
     * Verifie l'unicite de l'horaire
     * @param libelle
     * @return
     */
    private boolean checkExist(String libelle) {
		try {			
			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put(Globals.LIBELLE_ATTRIBUTE, libelle);			
			List l = this.horaireServise.read(hash);
			return ((l != null) && (l.size() > 0)) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public void savehoraire() throws Exception {
		if (horaire == null)
			return;
		if (horaire.getId() == 0) {
			if(!checkExist(horaire.getLibelle())) {
				horaire.setType(Globals.SPL_HORAIRE);
				horaireServise.create(horaire);
				horaires.add(horaire);
			} else
				FacesContext.getCurrentInstance().addMessage(Globals.LIBELLE_ATTRIBUTE,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage(Globals.CHECK_EXIST, this.horaire
									.getLibelle()), null));
		} else 
			horaireServise.update(horaire);
		horaire = new SmplHoraireImpl();
	}

	public void deletehoraire(){
		try {
			horaireServise.delete(horaire);
			horaires.remove(horaire);
			eventModel.clear();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context = FacesContext.getCurrentInstance();
			context.addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, 
							MessageFactory.getMessage("Horaire.M1", 
									horaire.getLibelle()), null));

		} 
	}

	public void clear() {

		horaire = new SmplHoraireImpl();

	}

	public Horaire getHoraire() {
		return horaire;
	}

	public void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}

	public List<Horaire> getHoraires() {
		try {
			if (horaires == null || horaires.isEmpty()) {
				horaires = horaireServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return horaires;
	}

	public PlageHoraire getPlageHoraire() {
		return plageHoraire;
	}

	public void setPlageHoraire(PlageHoraire plageHoraire) {
		this.plageHoraire = plageHoraire;
	}

	public void setHoraireServise(HoraireManageableService horaireServise) {
		this.horaireServise = horaireServise;
	}

	public void setAnnotationService(
			AnnotationManageableService annotationService) {
		this.annotationService = annotationService;
	}


	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public String getHoraireId() {
		return horaireId;
	}

	public void setHoraireId(String horaireId) {
		this.horaireId = horaireId;
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public void viewHoraireAction(SelectEvent e) {
		this.horaire = (SmplHoraire)horaire;  
		if (horaire != null && horaire.getLibelle() != null) {
			SmplHoraire smpHoraire = (SmplHoraire) horaire;
			eventModel = new DefaultScheduleModel();
			if (smpHoraire.getPlageHoraires() != null
					&& !smpHoraire.getPlageHoraires().isEmpty()) {
				for (PlageHoraire plage : smpHoraire.getPlageHoraires()) {
					eventModel.addEvent(new DefaultScheduleEvent(plage.getAnnotation().getDescription() + " " + plage.getAnnotation().getTaux()+"%",
							today1Pm(plage.getDebut()), today1Pm(plage.getFin()), plage.getId()));
				}
			} 
		} 
		editMode = false;
	}

	private Calendar today() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar;
	}


	private Date today1Pm(Double d) {
		Calendar t = (Calendar) today().clone();
		t.set(Calendar.AM_PM, Calendar.AM);
		t.set(Calendar.HOUR, d.intValue());
		t.set(Calendar.MINUTE, Utils.decimal(d));
		return t.getTime();
	}

	private Date nextToDay(Date date,Double d, int nb){
		Calendar t  = Calendar.getInstance();
        t.setTime(date);
        t.add(Calendar.DAY_OF_MONTH, nb);
		t.set(Calendar.AM_PM, Calendar.AM);
		t.set(Calendar.HOUR, d.intValue());
		t.set(Calendar.MINUTE, Utils.decimal(d));
		return t.getTime();
	}

//	public boolean validateBornes() {
//		MessageFactory msg = new MessageFactory();
//		if (plageHoraire.getFin() < plageHoraire.getDebut())
//			FacesContext.getCurrentInstance().addMessage(null, FacesMessage.SEVERITY_ERROR, msg
//					.getMessage(Globals.HOR_E001_ME003, i+1), null);
//	}
	public void addEvent(ActionEvent actionEvent) {
		try {
			SmplHoraire smplHoraire = (SmplHoraire)horaire;
			if (plageHoraire != null) {
//				pService.create(plageHoraire);
				if (plageHoraire.getAnnotation().getId() > 0)
					plageHoraire.setAnnotation(annotationService.load(plageHoraire.getAnnotation().getId()));
				if (plageHoraire.getId() == 0)
					smplHoraire.getPlageHoraires().add(plageHoraire);
				if (plageHoraire.getId() > 0) {
					PlageHoraire pr = null;
					for(PlageHoraire p: smplHoraire.getPlageHoraires())
						if (p.getId() == plageHoraire.getId()) {
							pr = p;
							break;
						}
					smplHoraire.getPlageHoraires().remove(pr);
					smplHoraire.getPlageHoraires().add(plageHoraire);
				}
			} 
			
			horaireServise.update(horaire);
			viewHoraireAction(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void deleteEvent(ActionEvent actionEvent) {
		try {
			SmplHoraire smplHoraire = (SmplHoraire)horaire;
			smplHoraire.getPlageHoraires().remove(plageHoraire);
			horaireServise.update(horaire);
			viewHoraireAction(null);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onEventSelect(SelectEvent selectEvent) {
		try {
			ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();
			int id = (Integer)event.getData();
			if (id > 0)
				plageHoraire = plageHoraireService.load(id);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void onDateSelect(SelectEvent selectEvent) {
		plageHoraire = new PlageHoraireImpl();
		plageHoraire.setHoraire((SmplHoraire)horaire);
		plageHoraire.setAnnotation(new AnnotationImpl());
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event moved", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());

		addMessage(message);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event resized", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());
		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Annotation> getAnnotations() {
		try {
			if (annotations == null || annotations.isEmpty())
				annotations = annotationService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return annotations;
	}

	public boolean isEditHor() {
		return editHor;
	}

	public void setEditHor(boolean editHor) {
		this.editHor = editHor;
	}

	public void setPlageHoraireService(
			PlageHoraireManageableService plageHoraireService) {
		this.plageHoraireService = plageHoraireService;
	}

}
