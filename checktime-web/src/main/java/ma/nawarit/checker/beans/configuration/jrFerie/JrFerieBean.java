package ma.nawarit.checker.beans.configuration.jrFerie;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;

import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.JrFerieImpl;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableServiceBase;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.utils.Globals;

@ManagedBean(name = "jrFerieBean")
@SessionScoped
public class JrFerieBean {

	@ManagedProperty(value = "#{JrFerieManageableService}")
	private JrFerieManageableService jrFerieServise = new JrFerieManageableServiceBase();

	private List<JrFerie> jrFeries = new ArrayList<JrFerie>();
	private JrFerie jrferie = new JrFerieImpl();
	private JrFerie selectedJour;
	FacesContext context;

	public JrFerieBean() {

		context = FacesContext.getCurrentInstance();
	}

	public void saveJrferie() throws Exception {
		if (jrferie != null && jrferie.getLibelle() != null) {
			jrFerieServise.create(jrferie);
		}
		jrFeries.add(jrferie);
	}

	public void cleanJrferie() {
		jrferie = new JrFerieImpl();
	}
	
	public void updateFerie(RowEditEvent event) throws Exception{
		jrferie = ((JrFerie)event.getObject());
		System.out.println(jrferie.getLibelle());
		jrFerieServise.update(jrferie);
	}
	
	public void selectedDeleteJrFerie(RowEditEvent event) throws Exception{
		
		selectedJour= ((JrFerie) event.getObject());	
	}
	
	public void deleteferie() throws Exception{
		
		if(selectedJour!= null){
			jrFerieServise.delete(selectedJour);
			jrFeries.remove(selectedJour);
			selectedJour = new JrFerieImpl();
		}		
	}

	public JrFerieManageableService getJrFerieServise() {
		return jrFerieServise;
	}

	public void setJrFerieServise(JrFerieManageableService jrFerieServise) {
		this.jrFerieServise = jrFerieServise;
	}

	public List<JrFerie> getJrFeries() {

		try {
			if (jrFeries == null || jrFeries.isEmpty()) {
				jrFeries = jrFerieServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jrFeries;
	}

	public void setJrFeries(List<JrFerie> jrFeries) {
		this.jrFeries = jrFeries;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public JrFerie getJrferie() {
		return jrferie;
	}

	public void setJrferie(JrFerie jrferie) {
		this.jrferie = jrferie;
	}

	public JrFerie getSelectedJour() {
		return selectedJour;
	}

	public void setSelectedJour(JrFerie selectedJour) {
		this.selectedJour = selectedJour;
	}
}
