package ma.nawarit.checker.beans.configuration.planning;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableServiceBase;
import ma.nawarit.checker.configuration.Horaire;
import ma.nawarit.checker.configuration.HoraireImpl;
import ma.nawarit.checker.configuration.Jour;
import ma.nawarit.checker.configuration.Period;
import ma.nawarit.checker.configuration.PeriodImpl;
import ma.nawarit.checker.configuration.PlageHoraire;
import ma.nawarit.checker.configuration.PlageHoraireImpl;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningCycl;
import ma.nawarit.checker.configuration.PlanningCyclImpl;
import ma.nawarit.checker.configuration.SmplHoraire;
import ma.nawarit.checker.configuration.SmplHoraireImpl;
import ma.nawarit.checker.configuration.crud.HoraireManageableService;
import ma.nawarit.checker.configuration.crud.HoraireManageableServiceBase;
import ma.nawarit.checker.configuration.crud.PlageHoraireManageableService;
import ma.nawarit.checker.configuration.crud.PlageHoraireManageableServiceBase;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.configuration.crud.PlanningManageableServiceBase;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;

import org.apache.myfaces.custom.schedule.UISchedule;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.ScheduleEntryMoveEvent;
import org.primefaces.event.ScheduleEntryResizeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultScheduleEvent;
import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.LazyScheduleModel;
import org.primefaces.model.ScheduleEvent;
import org.primefaces.model.ScheduleModel;


@ManagedBean(name = "planningBean")
@ViewScoped
public class PlanningBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7210848349238551615L;
	
	@ManagedProperty(value = "#{PlanningManageableService}")
	private PlanningManageableService planningService = new PlanningManageableServiceBase();
	
	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService = new UserManageableServiceBase();

	@ManagedProperty(value = "#{HoraireManageableService}")
	private HoraireManageableService horaireServise = new HoraireManageableServiceBase();
	

	@ManagedProperty(value = "#{PlageHoraireManageableService}")
	private PlageHoraireManageableService plageHoraireService = new PlageHoraireManageableServiceBase();
	
	FacesContext context = FacesContext.getCurrentInstance();
	
	private String planningId;
	private List<Planning> plannings = null;
	private int period;
	private List<Jour> jours;
	private ScheduleModel model;
	private Date currentDay;
	private Period periode = new PeriodImpl();
	private boolean editMode;
	private Horaire horaire = new HoraireImpl();
	private int horaireId;
	private ScheduleModel eventModel;
	private PlageHoraire plageHoraire = new PlageHoraireImpl();
	private List<Period> periods = new ArrayList<Period>();
	private Date intialDate = new Date();
	private String scheduleView = "month";
	
	private Planning planning;


	public PlanningBean() throws Exception {

		plannings = new ArrayList<Planning>();
	}

	public void onEdit(RowEditEvent event) {

		FacesMessage msg = new FacesMessage("Plage Edited");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onCancel(RowEditEvent event) {

		FacesMessage msg = new FacesMessage("Plage Cancelled");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	/**
     * Verifie l'unicite d'planning
     * @param libelle
     * @return
     */
    private boolean checkExist(String libelle) {
		try {			
			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put(Globals.LIBELLE_ATTRIBUTE, libelle);			
			List planningListe = this.planningService.read(hash);
			return ((planningListe != null) && (planningListe.size() > 0)) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public void savePlanning() throws Exception {
		if (planning == null)
			return;
			if (planning.getId() == 0) {
				if (!checkExist(planning.getLibelle())) {
					planning.setType(Globals.CYCL_PLANNING);
					planning.setCategHrsp(1);
					planning.setMetier(true);
					planningService.create(planning);
					plannings.add(planning);
					planning = new PlanningCyclImpl();
				}
				else {
					FacesContext.getCurrentInstance().addMessage(Globals.LIBELLE_ATTRIBUTE,
							new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFactory
									.getMessage(Globals.CHECK_EXIST, this.planning
											.getLibelle()), null));
				}
			} else
				planningService.update(planning);
	}

	public void updateplanning() throws Exception {

		planningService.update(planning);
	}

	public void deleteplanning(){
		try {
			planningService.delete(planning);
			plannings.remove(planning);
			planning = new PlanningCyclImpl();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			context = FacesContext.getCurrentInstance();
			context.addMessage(null, 
					new FacesMessage(FacesMessage.SEVERITY_ERROR, 
							MessageFactory.getMessage("Planning.M2", 
									planning.getLibelle()), null));

		}
	}

	public void clear() {

		planning = new PlanningCyclImpl();
	}

	// Getters and Setters
	
	public boolean isEditMode() {
		return editMode;
	}

	public void setEditMode(boolean editMode) {
		this.editMode = editMode;
	}

	public PlanningManageableService getPlanningService() {
		return planningService;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}

	public String getPlanningId() {
		return planningId;
	}

	public void setPlanningId(String planningId) {
		this.planningId = planningId;
	}


	public List<Planning> getPlannings() {

		try {
			if (plannings == null || plannings.isEmpty()) {
				Hashtable<String, String> props = new Hashtable<String, String>();
				props.put("type", Globals.CYCL_PLANNING);
				plannings = planningService.read(props);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return plannings;
	}

	public void setPlannings(List<Planning> plannings) {
		this.plannings = plannings;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}

	public List<Jour> getJours() {
		return jours;
	}

	public void setJours(List<Jour> jours) {
		this.jours = jours;
	}

	public ScheduleModel getModel() {
		return model;
	}

	public void setModel(ScheduleModel model) {
		this.model = model;
	}

	public Date getCurrentDay() {
		return currentDay;
	}

	public void setCurrentDay(Date currentDay) {
		this.currentDay = currentDay;
	}

	private List<Planning> planningList;

	public List<Planning> getPlanningList() {
		return planningList;
	}

	public void setPlanningList(List<Planning> planningList) {
		this.planningList = planningList;
	}

	public Horaire getHoraire() {
		return horaire;
	}

	public void setHoraire(Horaire horaire) {
		this.horaire = horaire;
	}

	public PlageHoraire getPlageHoraire() {
		return plageHoraire;
	}

	public void setPlageHoraire(PlageHoraire plageHoraire) {
		this.plageHoraire = plageHoraire;
	}

	public Date getInitialDate() {
		Calendar calendar = Calendar.getInstance();
		calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY,
				calendar.get(Calendar.DATE), 0, 0, 0);

		return calendar.getTime();
	}

	private Date nextToDay(Date date,Double d, int nb){
		Calendar t  = Calendar.getInstance();
        t.setTime(date);
        t.add(Calendar.DAY_OF_MONTH, nb);
		t.set(Calendar.AM_PM, Calendar.AM);
		t.set(Calendar.HOUR, d.intValue());
		t.set(Calendar.MINUTE, Utils.decimal(d));
		return t.getTime();
	}


	public void onEventSelect(SelectEvent selectEvent) {
			
		ScheduleEvent event = (ScheduleEvent) selectEvent.getObject();
		try {
			int id = (Integer)event.getData();
			if(id > 0)
				plageHoraire =  plageHoraireService.load(id);
			else 
				plageHoraire = new PlageHoraireImpl();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}

	public void onDateSelect(SelectEvent selectEvent) {
		currentDay = (Date) selectEvent.getObject();
	}

	public void onEventMove(ScheduleEntryMoveEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event moved", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());

		addMessage(message);
	}

	public void onEventResize(ScheduleEntryResizeEvent event) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				"Event resized", "Day delta:" + event.getDayDelta()
						+ ", Minute delta:" + event.getMinuteDelta());
		addMessage(message);
	}

	private void addMessage(FacesMessage message) {
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public ScheduleModel getEventModel() {
		return eventModel;
	}
	
	public ScheduleModel getMyEventModel() {
		FacesContext fc = FacesContext.getCurrentInstance();
		try {
			Object obj = fc.getExternalContext().getSessionMap()
					.get(Globals.USER_AUTHENTICATED);
			if (obj != null && (obj instanceof User)) {
				User user = (User)obj;
				user = userService.load(user.getId());
				this.planning = user.getPlanningAffected();
				loadLazyModel();
			}
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
		return eventModel;
	}
	
	public void loadLazyModel() {
		eventModel = new LazyScheduleModel() {
            
            /**
			 * 
			 */
			private static final long serialVersionUID = -4144837507990049280L;

			@Override
            public void loadEvents(Date start, Date end) {
        		if (planning != null && planning.getLibelle() != null) {
        			SmplHoraire smplHoraire = new SmplHoraireImpl();
        			PlanningCycl planningCycl = Utils.deproxy(planning, PlanningCycl.class);
        			Date dateDebut = planningCycl.getDateDebut();
        			Calendar first = new GregorianCalendar();
        			first.setTime(start);
        			Calendar last = new GregorianCalendar();
        			last.setTime(end);
        			DefaultScheduleEvent ev;
        			Calendar date1 = new GregorianCalendar();
            		date1.setTime(dateDebut);
            		date1.set(Calendar.HOUR_OF_DAY, 0);
            		date1.set(Calendar.MINUTE, 0);
            		date1.set(Calendar.SECOND, 0);
            		date1.set(Calendar.MILLISECOND, 0);
            		Calendar date2 = new GregorianCalendar();
            		date2.setTime(dateDebut);
            		date2.set(Calendar.HOUR_OF_DAY, 0);
            		date2.set(Calendar.MINUTE, 0);
            		date2.set(Calendar.SECOND, 0);
            		date2.set(Calendar.MILLISECOND, 0);
            		
            		date2.add(Calendar.DAY_OF_MONTH, planningCycl.getPeriodicite());
            		//set Last Date
            		Calendar PlanningStartDate = new GregorianCalendar();
            		PlanningStartDate.setTime(planningCycl.getDateDebut());
            		
            		
            		
            		while (first.compareTo(date2) > 0) {
        				date1.add(Calendar.DAY_OF_MONTH, planningCycl.getPeriodicite());
            			date2.add(Calendar.DAY_OF_MONTH, planningCycl.getPeriodicite());
        			}
            		int nbreJr = 0;
            		date2.add(Calendar.DAY_OF_MONTH, -planningCycl.getPeriodicite());
            		
            		//si la date debut du planing superieur a date debut du calendrier
            		if(first.compareTo(PlanningStartDate) <= 0)
            		{
            			first.setTime(planningCycl.getDateDebut());
            			PlanningStartDate.add(Calendar.DAY_OF_MONTH, 7);
            			last.setTime(PlanningStartDate.getTime());
            		}
            		while (first.compareTo(last) <= 0){
            			if (planningCycl.getPeriods() != null
        					&& !planningCycl.getPeriods().isEmpty()) {
            				
        					for (Period p : planningCycl.getPeriods()) {

        						horaire = p.getHoraire();
        						smplHoraire = (SmplHoraire) horaire;
        						date2.add(Calendar.DAY_OF_MONTH, p.getNbreJr());
        						date1.add(Calendar.DAY_OF_MONTH, nbreJr);
        						
        							while ((first.compareTo(date1) >= 0) && (first.compareTo(date2) < 0)) {
        								if ("Repos".equals(p.getHoraire().getType())) {
        							        first.add(Calendar.DAY_OF_YEAR, 1);
        							        continue;
        							     }
        								for (PlageHoraire plage : smplHoraire
        									.getPlageHoraires()) {
        									ev = new DefaultScheduleEvent();
        									ev.setDescription(horaire.getLibelle());
        									ev.setData(plage.getId());
        									ev.setTitle("  " + plage.getAnnotation().getCode() + "-" + ((Double)plage.getAnnotation().getTaux()).intValue() + "%");
        									ev.setStartDate(nextToDay(first.getTime(),plage.getDebut(),0));
        									ev.setEndDate(nextToDay(first.getTime(),plage.getFin(),((plage.getDebut() > plage.getFin())?1:0)));
        									eventModel.addEvent(ev);
        								}
        								first.add(Calendar.DAY_OF_YEAR, 1);
        								if (first.compareTo(last) > 0)
        									break;
        							}
        						if (first.compareTo(last) > 0)
        							break;
        						nbreJr = p.getNbreJr();
        					}
        				} else
        					break;
            		}
        		}
            }   
        };
	}
	

	public void setEventModel(ScheduleModel eventModel) {
		this.eventModel = eventModel;
	}

	public Period getPeriode() {
		return periode;
	}

	public void setPeriode(Period periode) {
		this.periode = periode;
	}

	public HoraireManageableService getHoraireServise() {
		return horaireServise;
	}

	public void setHoraireServise(HoraireManageableService horaireServise) {
		this.horaireServise = horaireServise;
	}

	public Period ajouterPeriod() {

		Period pd = new PeriodImpl();
		pd.setNbreJr(1);
		Horaire hr = new HoraireImpl();
		try {
			hr = horaireServise.load(24);
		} catch (Exception e) {
			e.printStackTrace();
		}
		pd.setHoraire(hr);
		return pd;
	}
	
	public void initialPeriodsListener() {
		((PlanningCycl)planning).getPeriods().add(new PeriodImpl());
	}
	
	public void setHoraireIdListener(Period p) {
		try {
			if (horaireId > 0)
				p.setHoraire(horaireServise.load(horaireId));
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public void removePeriodListener(int index) {
		((PlanningCycl)planning).getPeriods().remove(index);
		((PlanningCycl)planning).getPeriods().add(index, new PeriodImpl());
	}
	
	public void addPeriodListener() {
		PlanningCycl pl = ((PlanningCycl)planning);
		int pcite = pl.getPeriodicite();
		int n = 0;
    	for(Period p : pl.getPeriods()){
    		if (p.getNbreJr() == 0)
    			return;
    		n = n + p.getNbreJr();
    		if (n > pcite)
    			p.setNbreJr(pcite - (n - p.getNbreJr())); 
    	}
    	if (n < pcite)
    		pl.getPeriods().add(new PeriodImpl());
	}
	
	public static Date ajouterJour(Date date, int nbJour) { 
		  Calendar cal = Calendar.getInstance(); 
		  cal.setTime(date);
		  cal.add(Calendar.DAY_OF_MONTH, nbJour);
		  return cal.getTime();
	}	

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public List<Period> getPeriods() {
		return periods;
	}

	public void setPeriods(List<Period> periods) {
		this.periods = periods;
	}
	public Date getIntialDate() {
		return intialDate;
	}
	public void setIntialDate(Date intialDate) {
		this.intialDate = intialDate;
	}
	
	public int getHoraireId() {
		return horaireId;
	}
	
	public void setHoraireId(int horaireId) {
		this.horaireId = horaireId;
	}
	public void setPlageHoraireService(
			PlageHoraireManageableService plageHoraireService) {
		this.plageHoraireService = plageHoraireService;
	}

	public String getScheduleView() {
		if (planning != null) {
			PlanningCycl planningCycl = (PlanningCycl)planning;
			if (planningCycl.getPeriodicite() == 1)
				scheduleView = "basicDay";
			else if (planningCycl.getPeriodicite() <= 7)
				scheduleView = "agendaWeek";
			else
				scheduleView = "month";
		}
		return scheduleView;
	}

	public void setScheduleView(String scheduleView) {
		this.scheduleView = scheduleView;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}
	
	
}
