package ma.nawarit.checker.beans.manager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import ma.nawarit.checker.beans.compagnie.collabo.UserDataModel;
import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.compagnie.Group;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.NoeudImpl;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.GroupManageableService;
import ma.nawarit.checker.compagnie.crud.NoeudManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.engine.LiveProcessingService;
import ma.nawarit.checker.engine.UserReportData;
import ma.nawarit.checker.utils.Globals;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.primefaces.component.schedule.Schedule;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.data.PageEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.Axis;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.springframework.beans.BeanUtils;

@ManagedBean(name = "managerBean")
@ViewScoped
public class ManagerBean {
	
	private static final Logger logger = Logger.getLogger(ManagerBean.class);
	
	@ManagedProperty(value = "#{LiveProcessingService}")
	private LiveProcessingService processorService;

	@ManagedProperty(value = "#{GroupManageableService}")
	private GroupManageableService groupService;
	
	@ManagedProperty(value = "#{NoeudManageableService}")
	private transient NoeudManageableService noeudService;
		
	private CommonCriteria criteria = new CommonCriteria();
	
	private List<UserReportData> userDatas;
	
	private DayData total = new DayData();
	
	private int weekCount = 1;
	
	private UserReportData userData;
	
	private List<Date> columns = new ArrayList<Date>();
	
	private Schedule schedule;
	
	private FacesContext context = FacesContext.getCurrentInstance();
	
	private User usrAuthenticated;
	
	private TreeNode treeNode;
	
	private UserDataModel dataModel;
	
	private LineChartModel lineModel;
	
	@PostConstruct
	public void init() {
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		usrAuthenticated = (User)obj;
		criteria.setDateDebut(Utils.getMondayOfCurrentWeek().getTime());
		criteria.setDateFin(Utils.getSundayOfCurrentWeek().getTime());
	}
	
	public void initlDataModel() {
		this.dataModel = null;
		criteria.setIndexs(new ArrayList<Integer>());
	}
	public UserDataModel getDataModel() {
		if (dataModel == null && usrAuthenticated != null && usrAuthenticated.getId() > 1) {
			criteria.setSuppHrhId(usrAuthenticated.getId() + "");
			criteria.setTypeTreatment("MGR");
			userDatas = processorService.process(criteria, null);
			List<User> users = new ArrayList<User>();
			for (UserReportData urd : userDatas) {
				System.out.println(urd.getUser().getNoeud().getCode());
				users.add(urd.getUser());
			}
			dataModel = new UserDataModel(users);
		}
		return dataModel;
	}
	
	public TreeNode getTreeNode() {
		Noeud node;
		try {
			if (treeNode == null) {
				this.treeNode = new DefaultTreeNode("ROOT", null);
				if (usrAuthenticated != null && usrAuthenticated.getId() > 1
						&& usrAuthenticated.getNoeud() != null) {
					node = noeudService.load(usrAuthenticated.getNoeud().getId());
					this.buildTree(new DefaultTreeNode(node, this.treeNode));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return this.treeNode;
	}
	
	public void buildTree(TreeNode node) {
		Noeud n = (Noeud) node.getData();
		if (n.getNoeuds() != null) {
			Noeud nda;
			for (Noeud nd : n.getNoeuds()) {
				nda = new NoeudImpl();
				BeanUtils.copyProperties(nd, nda);
				this.buildTree(new DefaultTreeNode(nda, node));
			}
		}
	}
	
	public void onSelfTabChangeListener(TabChangeEvent event) {
		System.out.println(event.getTab().getId());
		if (schedule != null && event.getTab().getId().equals("AttSheetId")) {
			System.out.println(schedule.getValue().getEvents().get(0).getStartDate());
			criteria = new CommonCriteria();
//			criteria.setDateDebut(schedule.getModel().getSelectedDate());
		}
	}
	
	public void loadReportingParams(){
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("DATE_DEBUT",
				criteria.getDateDebut());
		context.getExternalContext().getSessionMap().put("DATE_FIN",
				criteria.getDateFin());
		context.getExternalContext().getSessionMap().put("DATA_RESULT",
				this.userDatas);
		Integer[] tauxList = {125,150,200};
		context.getExternalContext().getSessionMap().put("TAUX_LIST",
				tauxList);
	}
	
	public void onRowToggleListener(UserReportData userData) {
		this.userData = userData;
		total = new DayData();
		total.setP_tmcontract(0.0);
		total.setCol1(0.0);
		if (userData == null || userData.getDayDatas() == null || userData.getDayDatas().isEmpty())
			return;
		int d = (weekCount - 1) * 7;
		int f = weekCount * 7 - 1;
		for (int i = d; (i <= f && i <= userData.getDayDatas().size() - 1); i++) 
			addweeksTotal(userData.getDayDatas().get(i));
	}
	
	public void addweeksTotal(DayData d) {
		try {					
				total.setCol1(Utils.additionHeures(total.getCol1(), d.getCol1()));
				total.setCol2(Utils.additionHeures(total.getCol2(), d.getCol2()));
				total.setCol2cop(Utils.additionHeures(total.getCol2cop(), d.getCol2cop()));
				total.setCol2ht(Utils.additionHeures(total.getCol2ht(), d.getCol2ht()));
				total.setCol2bis(total.getCol2bis()+ d.getCol2bis());
				total.getCol2prm()[0] += d.getCol2prm()[0];
				total.getCol2prm()[1] += d.getCol2prm()[1];
				total.getCol2prm()[2] += d.getCol2prm()[2];
				total.setCol3(Utils.additionHeures(total.getCol3(), d.getCol3()));
				total.setCol4(Utils.additionHeures(total.getCol4(), d.getCol4()));
				total.setCol5(Utils.additionHeures(total.getCol5(), d.getCol5()));
				total.setCol6(Utils.additionHeures(total.getCol6(), d.getCol6()));
				total.setTcol7(total.getTcol7()+ d.getTcol7());
				total.setTcol8(Utils.additionHeures(total.getTcol8(), d.getTcol8()));
				total.setTcol9(Utils.additionHeures(total.getTcol9(), d.getTcol9()));
				total.setCol10(Utils.additionHeures(total.getCol10(), d.getCol10()));
				total.setCol11(Utils.additionHeures(total.getCol11(), d.getCol11()));
				total.setCol12(Utils.additionHeures(total.getCol12(), d.getCol12()));
				total.setCol13(Utils.additionHeures(total.getCol13(), d.getCol13()));
				total.setCol14(Utils.additionHeures(total.getCol14(), d.getCol14()));	
				total.setCol15(Utils.additionHeures(total.getCol15(), d.getCol15()));	
				total.setCol16(Utils.additionHeures(total.getCol16(), d.getCol16()));
				total.setCol17(Utils.additionHeures(total.getCol17(), d.getCol17()));
				total.setCol18(Utils.additionHeures(total.getCol18(), d.getCol18()));
				total.setCol19(Utils.additionHeures(total.getCol19(), d.getCol19()));
				total.setCol20(Utils.additionHeures(total.getCol20(), d.getCol20()));
				
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void groupValueChangeListener(ValueChangeEvent e) throws Exception {
		logger.error("-------> group start");
    	Group group = this.groupService.load(Integer.parseInt((String)e.getNewValue()));
    	criteria.setIndexs(new ArrayList<Integer>());
    	for(User u: group.getUsers())
    		criteria.getIndexs().add(u.getId());
    	this.criteria.setChanged(true);
    	logger.error("-------> group end");
    	
    }
	
	public void processWeekListener(PageEvent e) {
		weekCount = e.getPage() + 1;
		if (userData == null || userData.getDayDatas() == null || userData.getDayDatas().isEmpty())
			return;
		total = new DayData();
		int d = (weekCount - 1) * 7;
		int f = weekCount * 7 - 1;
		for (int i = d; (i <= f && i <= userData.getDayDatas().size() - 1); i++) {
			addweeksTotal(userData.getDayDatas().get(i));
		}
	}

	public void processWeekListener(ActionEvent e) {
		weekCount++;
	}
	public void setProcessorService(LiveProcessingService processorService) {
		this.processorService = processorService;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public void setGroupService(GroupManageableService groupService) {
		this.groupService = groupService;
	}

	public List<UserReportData> getUserDatas() {
		return userDatas;
	}

	public int getWeekCount() {
		return weekCount;
	}

	public DayData getTotal() {
		return total;
	}

	public void setTotal(DayData total) {
		this.total = total;
	}

	public void setWeekCount(int weekCount) {
		this.weekCount = weekCount;
	}

	public List<Date> getColumns() {
		return columns;
	}

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public void setNoeudService(NoeudManageableService noeudService) {
		this.noeudService = noeudService;
	}
	
	public LineChartModel getLineModel() {
		//intialchart
		lineModel = initLinearModel();
        lineModel.setLegendPosition("e");
        lineModel.setShowPointLabels(true);
        Axis xAxis = new DateAxis();
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        xAxis.setMin(sdf.format(criteria.getDateDebut()));
        xAxis.setMax(sdf.format(criteria.getDateFin()));
        xAxis.setTickFormat("%d-%m");
        xAxis.setTickAngle(-50);
        xAxis.setTickCount(Utils.getDaysCount(criteria.getDateDebut(), criteria.getDateFin()));
        xAxis.setLabel("Jours");
        lineModel.getAxes().put(AxisType.X, xAxis);
        Axis yAxis = lineModel.getAxis(AxisType.Y);
        yAxis.setMin(0);
        yAxis.setMax(12);   
        yAxis.setLabel("Heurs travaillées");
        lineModel.setSeriesColors("E61111,4986E7");
		if (lineModel == null)
			lineModel = new LineChartModel();
		return lineModel;
	}
	
	private LineChartModel initLinearModel() {
    	LineChartModel model = new LineChartModel();
 
        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("TC");
        Calendar c = new GregorianCalendar();
        c.setTime(criteria.getDateDebut());
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        while (!c.getTime().after(criteria.getDateFin())) {
        	series1.set(sdf.format(c.getTime()), 7);
        	c.add(Calendar.DAY_OF_WEEK, 1);
		}
        
        Map<Object, Integer> hts = new HashMap<Object, Integer>(); 
        int size = 1;
        if (this.dataModel != null && this.dataModel.getData() != null && !this.dataModel.getData().isEmpty()) {
        	size = this.dataModel.getData().size();
	        for (User u: this.dataModel.getData()) {
	        	UserImpl ui = (UserImpl)u;
	        	if (ui.getDays() != null && !ui.getDays().isEmpty()) {
	        		for (int i = 0; i < ui.getDays().size(); i++) {
						Day d = (Day) ui.getDays().get(i);
//						if (d.isJrRepos()) 
//							continue;
						if (hts.containsKey(sdf.format(d.getDate()))) {
							Integer n = hts.get(sdf.format(d.getDate())) + d.getMinworked();
							hts.remove(sdf.format(d.getDate()));
							hts.put(sdf.format(d.getDate()), n);
						} else
							hts.put(sdf.format(d.getDate()), d.getMinworked());
					}
	        	}
	        }
        }
        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("HTM");
        series2.setShowLine(false);
        series2.setDisableStack(true);
        Map<Object, Number> hts2 = new HashMap<Object, Number>();
        for(Object o: hts.keySet()) {
        	int t = hts.get(o)/size;
        	int min = (int) (t % 60);
    		t /= 60;
    		int hour = (int) (t % 24);
        	String s = hour + "." + min;
        	hts2.put(o, Double.valueOf(s));
        }
        series2.setData(hts2);
        model.addSeries(series1);
        model.addSeries(series2);
         
        return model;
    }
}
