package ma.nawarit.checker.beans.share;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.SmplHoraire;
import ma.nawarit.checker.configuration.crud.HoraireManageableService;
import ma.nawarit.checker.core.common.CommonReportData;
import ma.nawarit.checker.core.common.CrossData;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.engine.Field;
import ma.nawarit.checker.engine.UserReportData;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.InterruptionManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;
import ma.nawarit.checker.utils.syncRH.databaseOperations;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import org.andromda.spring.CommonCriteria;
import org.andromda.spring.PeriodCriteria;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

public class CommonReportController extends MultiActionController {
	private final Logger log = Logger.getLogger(CommonReportController.class);
	private UserManageableService userService = null;
	private AnnomalieManageableService anomalieService = null;
	private RetardManageableService retardService = null;
	private InterruptionManageableService interruptionService = null;
	private CongeManageableService congeService = null;
	private AbsenceManageableService absenceService = null;
	private TypeAbsenceManageableService typeAbsenceService = null;
	private MouvementManageableService mouvementService = null;
	private HoraireManageableService horaireService = null;
	private CommonCriteria criteria = null;
	private List<UserReportData> dataResult;
	private Date DateDebut = null, DateFin = null;
	private Integer[] tauxList;
	private String companyName = "Radeel";
	private List<String> titles;
	protected List<Field> fields;

	// For user Report Module
	public ModelAndView custumReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'userReport' method...");

		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("rtf")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		model.put("format", format);
		return new ModelAndView("custumMultiFormatReport", model);
	}

	// For user Report Module
	public ModelAndView userReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		}
		// else {
		// format = "pdf";
		// }
		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("IMAGEDIR", Globals.USERS_PHOTOS_DIR);
		model.put("IMAGEEXT", Globals.USERS_PHOTOS_EXT);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		List<User> users;
		if (this.criteria != null && !this.criteria.isEmpty()) {
			users = this.userService.readByCriteria(this.criteria);
		} else
			users = this.userService.readAll();

		// if (format.equalsIgnoreCase("xls")) {
		// initHeaderTitles(request);
		// String grandTitle = "Liste des collaborateurs";
		// ByteArrayOutputStream result = XlsModel.generateXLSUsersReport(users,
		// grandTitle, titles, fields);
		// response.reset();
		// response.setHeader("Content-type", "application/xls");
		// response.setHeader("Content-Disposition",
		// "inline; filename=users-report.xls");
		// ServletOutputStream servletOutputStream = response
		// .getOutputStream();
		// servletOutputStream.write(result.toByteArray());
		// servletOutputStream.flush();
		// servletOutputStream.close();
		// return null;
		// } else {
		model.put("dataSource", users);
		// }
		ModelAndView modelAndView = new ModelAndView("userMultiFormatReport", model);
		return modelAndView;
	}

	// For Anomalie Report Module
	public ModelAndView anomalieReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'anomalieReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		if (this.criteria != null && !this.criteria.isEmpty()) {
			if (this.criteria.getDateDebut() != null && this.criteria.getDateFin() != null) {
				model.put("DATE_DEBUT", this.criteria.getDateDebut());
				model.put("DATE_FIN", this.criteria.getDateFin());

			}
			if (format.equalsIgnoreCase("xls")) {
				annoXlsReport(response);
				return null;
			}
			model.put("dataSource",
					CommonTransformerData.getAnoGroupedList(this.anomalieService.readByCriteria(this.criteria)));
			this.criteria = null;

		} else {
			if (format.equalsIgnoreCase("xls")) {
				annoXlsReport(response);
				return null;
			}
			model.put("dataSource", CommonTransformerData.getAnoGroupedList(this.anomalieService.readAll()));
		}

		if ("xml".equalsIgnoreCase(format)) {
			return new ModelAndView("anomalieListXML", model);
		} else
			return new ModelAndView("anomalieMultiFormatReport", model);
	}

	private void annoXlsReport(HttpServletResponse response) throws Exception {
		String grandTitle = "Etat des anomalies";
		List<String> titles = new ArrayList<String>();
		titles.add(MessageFactory.getMessage("USR_E001_Z10"));
		titles.add(MessageFactory.getMessage("USR_E001_Z1"));
		titles.add(MessageFactory.getMessage("USR_E001_Z2"));
		titles.add(MessageFactory.getMessage("ANO_E000_label3"));
		titles.add(MessageFactory.getMessage("ANO_E000_label4"));
		ByteArrayOutputStream result;
		if (this.criteria != null && !this.criteria.isEmpty()) {
			if (this.criteria.getDateDebut() != null && this.criteria.getDateFin() != null)
				grandTitle += "entre " + Utils.convertDate2Str(this.criteria.getDateDebut(), "dd/MM/yyyy") + " et "
						+ Utils.convertDate2Str(this.criteria.getDateFin(), "dd/MM/yyyy");
			result = XlsModel.generateXlsAnomalieReport(this.anomalieService.readByCriteria(this.criteria), grandTitle,
					titles);
		} else
			result = XlsModel.generateXlsAnomalieReport(this.anomalieService.readAll(), grandTitle, titles);

		response.reset();
		response.setHeader("Content-type", "application/xls");
		response.setHeader("Content-Disposition", "inline; filename=anomalies-report.xls");
		ServletOutputStream servletOutputStream = response.getOutputStream();
		servletOutputStream.write(result.toByteArray());
		servletOutputStream.flush();
		servletOutputStream.close();

	}

	// For Retard Report Module
	public ModelAndView retardReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'retardReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		String grandTitle = "Etat des retardataires";
		if (this.criteria != null && !this.criteria.isEmpty()) {
			if (this.criteria.getDateDebut() != null && this.criteria.getDateFin() != null)
				grandTitle += ", Période entre " + Utils.convertDate2Str(this.criteria.getDateDebut(), "dd/MM/yyyy")
						+ " et " + Utils.convertDate2Str(this.criteria.getDateFin(), "dd/MM/yyyy");

			model.put("dataSource",
					CommonTransformerData.getRetardGroupedList(this.retardService.readByCriteria(this.criteria)));
			this.criteria = null;
		} else
			model.put("dataSource", CommonTransformerData.getRetardGroupedList(this.retardService.readAll()));
		if ("xls".equalsIgnoreCase(format)) {

			ByteArrayOutputStream result = XlsModel.generateXLSRetard4CopagReport((HashMap) model, grandTitle, titles);
			response.reset();
			response.setHeader("Content-type", "application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "inline; filename=retard-report.xls");
			ServletOutputStream servletOutputStream = response.getOutputStream();
			servletOutputStream.write(result.toByteArray());
			servletOutputStream.flush();
			servletOutputStream.close();
			return null;

		} else
			return new ModelAndView("retardMultiFormatReport", model);
	}

	// For Interruption Report Module
	public ModelAndView interruptionReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'interruptionReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		if (this.criteria != null && !this.criteria.isEmpty()) {
			model.put("dataSource",
					CommonTransformerData.getIntrpGroupedList(this.interruptionService.readByCriteria(this.criteria)));
			this.criteria = null;
		} else
			model.put("dataSource", CommonTransformerData.getIntrpGroupedList(this.interruptionService.readAll()));
		if ("xml".equalsIgnoreCase(format)) {
			return new ModelAndView("interruptionListXML", model);
		} else
			return new ModelAndView("interruptionMultiFormatReport", model);
	}

	// For Interruption Report Module
	public ModelAndView congeReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'congeReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		if (this.criteria != null && !this.criteria.isEmpty()) {
			model.put("dataSource",
					CommonTransformerData.getCngGroupedList(this.congeService.readByCriteria(this.criteria)));
			this.criteria = null;
		} else
			model.put("dataSource", CommonTransformerData.getCngGroupedList(this.congeService.readAll()));
		if ("xml".equalsIgnoreCase(format)) {
			return new ModelAndView("congeListXML", model);
		} else
			return new ModelAndView("congeMultiFormatReport", model);
	}

	public ModelAndView CRRSReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'congepvReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");
		String congeId = request.getParameter("conge");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		Hashtable hash = new Hashtable();
		hash.put("id", Integer.parseInt(congeId));
		model.put("dataSource", this.congeService.read(hash));
		if ("xml".equalsIgnoreCase(format)) {
			return new ModelAndView("congeListXML", model);
		} else
			return new ModelAndView("CRRSMultiFormatReport", model);
	}

	// // For Interruption Report Module
	// public ModelAndView absenceReport(HttpServletRequest request,
	// HttpServletResponse response) throws Exception {
	// log.debug("entering 'absenceReport' method...");
	// Map model = new HashMap();
	// //It is the path of our webapp - is there a better way to do this?
	// String reportResourcePath =
	// getServletContext().getRealPath("/").replace("%20", " ");
	//
	// String format = request.getParameter("format");
	//
	// //Default format to pdf
	//
	// if (StringUtils.hasText(format)) {
	//
	// if (!(format.equalsIgnoreCase("pdf")
	// || format.equalsIgnoreCase("html")
	// || format.equalsIgnoreCase("csv")
	// || format.equalsIgnoreCase("xls") || format
	// .equalsIgnoreCase("xml"))) {
	// format = "pdf";
	// }
	// } else {
	// format = "pdf";
	// }
	//
	// model.put("format", format);
	// model.put("WEBDIR", reportResourcePath);
	// initCriteria4Search(request);
	// if(this.criteria != null && !this.criteria.isEmpty()){
	// model.put("dataSource",
	// CommonTransformerData.getAbsGroupedList(this.absenceService.readByCriteria(this.criteria)));
	// this.criteria = null;
	// }
	// else{
	// Hashtable<String, String> hash = new Hashtable<String, String>();
	// hash.put("statut", "workflow_status_validated");
	// model.put("dataSource",
	// CommonTransformerData.getAbsGroupedList(this.absenceService.read(hash)));
	// }
	// if ("xml".equalsIgnoreCase(format)) {
	// return new ModelAndView("absenceListXML", model);
	// } else
	// return new ModelAndView("absenceMultiFormatReport", model);
	// }
	public ModelAndView absenceReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");
		if (format.equals("pdf")) {
			initCriteria4Reporting(request);
			absencePDFReport(request, response);
			return null;
		} else {

			// Default format to pdf

			if (StringUtils.hasText(format)) {

				if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html")
						|| format.equalsIgnoreCase("csv") || format.equalsIgnoreCase("xls")
						|| format.equalsIgnoreCase("xls1") || format.equalsIgnoreCase("xml"))) {
					format = "pdf";
				}
			} else {
				format = "pdf";
			}

			model.put("format", format);
			model.put("WEBDIR", reportResourcePath);
			model.put("COMPANY_NAME", this.companyName);
			model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);

			initCriteria4Reporting(request);
			String grandTitle = "";
			if (this.criteria != null && !this.criteria.isEmpty()) {
				if (this.criteria.getDateDebut() != null && this.criteria.getDateFin() != null) {
					model.put("DATE_DEBUT", this.criteria.getDateDebut());
					model.put("DATE_FIN", this.criteria.getDateFin());
					this.criteria.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
					grandTitle = "Etat d'absenteisme, Période entre "
							+ Utils.convertDate2Str(this.criteria.getDateDebut(), "dd/MM/yyyy") + " et "
							+ Utils.convertDate2Str(this.criteria.getDateFin(), "dd/MM/yyyy");
				}
				model.put("dataSource",
						CommonTransformerData.getAbsGroupedList(this.absenceService.readByCriteria(this.criteria)));
				this.criteria = null;
			} else
				model.put("dataSource", CommonTransformerData.getAbsGroupedList(this.absenceService.readAll()));
			if ("xls1".equalsIgnoreCase(format)) {

				ByteArrayOutputStream result = XlsModel.generateXLSAbsencePeriodique4CopagReport((HashMap) model,
						grandTitle, titles);
				response.reset();
				response.setHeader("Content-type", "application/xls");
				response.setHeader("Content-Disposition", "inline; filename=absence-report.xls");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(result.toByteArray());
				servletOutputStream.flush();
				servletOutputStream.close();
				return null;

			} else if ("xls".equalsIgnoreCase(format)) {
				String resourcePath = getServletContext().getRealPath("/").replace("%20", " ");
				System.out.println("Path : " + resourcePath);
				ByteArrayOutputStream result = XlsModel.generateXLSAbsenceJournalier4CopagReport((HashMap) model,
						grandTitle, titles, resourcePath);
				response.reset();
				response.setHeader("Content-type", "application/xls");
				response.setHeader("Content-Disposition", "inline; filename=absence-report.xls");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(result.toByteArray());
				servletOutputStream.flush();
				servletOutputStream.close();
				return null;

			} else {
				return null;
			}
		}
	}

	// getAbsence PDF report
	public void absencePDFReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> parameters = new HashMap<String, Object>();
		/*
		if (this.criteria != null && !this.criteria.isEmpty()) {
			if (this.criteria.getDateDebut() != null && this.criteria.getDateFin() != null) {
				parameters.put("DATE_DEBUT", this.criteria.getDateDebut());
				parameters.put("DATE_FIN", this.criteria.getDateDebut());
				this.criteria.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
			}
			this.criteria = null;
		}*/
		
		parameters.put("WEBDIR", getServletContext().getRealPath("/"));
		String jrxmlFile = getServletContext().getRealPath("/jasperreports/absences.jasper");
		File file = new File(jrxmlFile);
		InputStream inputStream = new FileInputStream(file);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, databaseOperations.getConnection());
		response.setContentType(getServletContext().getRealPath("/Absences"+Utils.convertDate2Str(Calendar.getInstance().getTime(), "yyyy-MM-dd")+".pdf"));
		response.setHeader("Content-Disposition", "attachment; filename=\"Absences"+Utils.convertDate2Str(Calendar.getInstance().getTime(), "yyyy-MM-dd")+".pdf");
		JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
	}

	private String convertStatut(String statut) {
		String stt = "";
		if (statut.equals(Globals.WORKFLOW_STATUS_VALIDATE))
			stt = "Valid\u00e9e";
		else if (statut.equals(Globals.WORKFLOW_STATUS_ENCOURS))
			stt = "Demand\u00e9e";
		else if (statut.equals(Globals.WORKFLOW_STATUS_REJECTED))
			stt = "Refus\u00e9e";
		else
			stt = "Effectu\u00e9e";
		return stt;
	}


	// For Mouvement Report Module
	public ModelAndView mvtReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'mvtReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		System.out.println("format " + format);
		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		initCriteria4Reporting(request);
		if (this.criteria != null && !this.criteria.isEmpty()) {
			// model.put("dataSource", this.loadMvtReportData(this.criteria));
			try {
				Calendar c1 = new GregorianCalendar();
				c1.setTime(criteria.getDateDebut());
				Calendar c2 = new GregorianCalendar();
				c2.setTime(criteria.getDateFin());
				if (format.equalsIgnoreCase("xls")) {
					String grandTitle = "Etat journalier des entrées-sorties, Période entre "
							+ Utils.convertDate2Str(c1.getTime(), "dd/MM/yyyy") + " et "
							+ Utils.convertDate2Str(c2.getTime(), "dd/MM/yyyy");

					List<String> titles = new ArrayList<String>();
					titles.add(MessageFactory.getMessage("USR_E001_Z10"));
					titles.add(MessageFactory.getMessage("USR_E001_Z1"));
					titles.add(MessageFactory.getMessage("USR_E001_Z2"));

					while (c1.compareTo(c2) <= 0) {
						titles.add(Utils.convertDate2Str(c1.getTime(), "EEE dd-MM-yyyy "));
						c1.add(Calendar.DAY_OF_MONTH, 1);
					}
					ByteArrayOutputStream result = XlsModel.generateXLSMvtsReport(this.loadMvtReportData(criteria),
							grandTitle, titles);
					response.reset();
					response.setHeader("Content-type", "application/xls");
					response.setHeader("Content-Disposition", "inline; filename=pointages-report.xls");
					ServletOutputStream servletOutputStream = response.getOutputStream();
					servletOutputStream.write(result.toByteArray());
					servletOutputStream.flush();
					servletOutputStream.close();
					return null;
				} else
					model.put("dataSource", getMvtGroupedList(this.criteria));
			} catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
			this.criteria = null;
		}
		return new ModelAndView("mvtMultiFormatReport", model);
	}

	// For Mouvement Report Module
	public ModelAndView cumulReport(HttpServletRequest request, HttpServletResponse response) {

		try {
			log.debug("entering 'cumulReport' method...");
			Map model = new HashMap();
			// It is the path of our webapp - is there a better way to do this?
			String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

			String format = request.getParameter("format");

			// Default format to pdf

			if (StringUtils.hasText(format)) {

				if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html")
						|| format.equalsIgnoreCase("csv") || format.equalsIgnoreCase("xls")
						|| format.equalsIgnoreCase("xml"))) {
					format = "pdf";
				}
			} else {
				format = "pdf";
			}

			getDataResult(request);
			String grandTitle = "";
			if (format.equalsIgnoreCase("xls")) {
				if (this.dataResult != null && !this.dataResult.isEmpty()) {
					grandTitle = "Rapport de Synthèse, Période entre " +
							Utils.convertDate2Str(this.DateDebut, "dd/MM/yyyy") + " et " +
							Utils.convertDate2Str(this.DateFin, "dd/MM/yyyy");
//					ByteArrayOutputStream result = XlsModel.generateXLSCumulReport(this.dataResult, grandTitle, titles);
//					response.reset();
//					response.setHeader("Content-type", "application/xls");
//					response.setHeader("Content-Disposition",
//							"inline; filename=cumul-report.xls");
//					ServletOutputStream servletOutputStream = response
//							.getOutputStream();
//					servletOutputStream.write(result.toByteArray());
//					servletOutputStream.flush();
//					servletOutputStream.close();
				}
				//return null;
			}

			model.put("format", format);
			model.put("WEBDIR", reportResourcePath);
			model.put("COMPANY_NAME", this.companyName);
			model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
			model.put("dataSource", this.dataResult);
			model.put("DATE_DEBUT", this.DateDebut);
			model.put("DATE_FIN", this.DateFin);
			model.put("TAUX_LIST", this.tauxList);
			model.put("titles", titles);
			model.put("grandTitle", grandTitle);
			if("xls".equals(format))
				return new ModelAndView("excelView", model);
			return new ModelAndView("cumulMultiFormatReport", model);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public ModelAndView horaireReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		Hashtable hash = new Hashtable();
		hash.put("type", "Horaire Simple");
		model.put("dataSource", (List<SmplHoraire>) this.horaireService.read(hash));
		return new ModelAndView("horaireMultiFormatReport", model);
	}

	public ModelAndView nodeReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		model.put("dataSource", CommonTransformerData.getNodeGroupedList(this.userService.readAll()));
		return new ModelAndView("nodeMultiFormatReport", model);
	}

	public ModelAndView cantineReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		log.debug("entering 'cumulReport' method...");
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		PeriodCriteria periodCriteria = new PeriodCriteria();
		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);
		getDataResult(request);
		model.put("dataSource", this.dataResult);
		model.put("DATE_DEBUT", this.DateDebut);
		model.put("DATE_FIN", this.DateFin);
		return new ModelAndView("cantineMultiFormatReport", model);
	}

	public ModelAndView paieReport(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map model = new HashMap();
		// It is the path of our webapp - is there a better way to do this?
		String reportResourcePath = getServletContext().getRealPath("/").replace("%20", " ");

		String format = request.getParameter("format");

		// Default format to pdf

		if (StringUtils.hasText(format)) {

			if (!(format.equalsIgnoreCase("pdf") || format.equalsIgnoreCase("html") || format.equalsIgnoreCase("csv")
					|| format.equalsIgnoreCase("xls") || format.equalsIgnoreCase("xls2")
					|| format.equalsIgnoreCase("xml"))) {
				format = "pdf";
			}
		} else {
			format = "pdf";
		}
		getDataResult(request);
		if (format.equalsIgnoreCase("xls")) {
			if (this.dataResult != null && !this.dataResult.isEmpty()) {
				String grandTitle = "Etat Périodique de Pointages - Période entre "
						+ Utils.convertDate2Str(this.DateDebut, "dd/MM/yyyy") + " et "
						+ Utils.convertDate2Str(this.DateFin, "dd/MM/yyyy");
				ByteArrayOutputStream result = XlsModel.generateXLSPaie4CopagReport(this.dataResult, grandTitle,
						titles);
				response.reset();
				response.setHeader("Content-type", "application/xls");
				response.setHeader("Content-Disposition", "inline; filename=cumul-report.xls");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(result.toByteArray());
				servletOutputStream.flush();
				servletOutputStream.close();
			}
			return null;
		} else if (format.equalsIgnoreCase("xls2")) {
			if (this.dataResult != null && !this.dataResult.isEmpty()
					&& this.dataResult.get(0).getDayDatas().size() >= 17) {
				String grandTitle = Utils.convertDate2Str(this.DateDebut, "MMM yyyy");
				ByteArrayOutputStream result = XlsModel.generateXLSCartePointage4CopagReport(this.dataResult,
						grandTitle, titles);
				response.reset();
				response.setHeader("Content-type", "application/xls");
				response.setHeader("Content-Disposition", "inline; filename=cumul-report.xls");
				ServletOutputStream servletOutputStream = response.getOutputStream();
				servletOutputStream.write(result.toByteArray());
				servletOutputStream.flush();
				servletOutputStream.close();
			}
			return null;
		}

		model.put("format", format);
		model.put("WEBDIR", reportResourcePath);
		model.put("COMPANY_NAME", this.companyName);
		model.put("COMPANY_LOGO", Globals.COMPANY_LOGO_PATH);

		model.put("dataSource", this.dataResult);
		model.put("DATE_DEBUT", this.DateDebut);
		model.put("DATE_FIN", this.DateFin);
		return new ModelAndView("paieMultiFormatReport" + format, model);
	}

	public void setUserService(UserManageableService mgr) {
		this.userService = mgr;
	}

	public void setAnomalieService(AnnomalieManageableService anomalieService) {
		this.anomalieService = anomalieService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public void setInterruptionService(InterruptionManageableService interruptionService) {
		this.interruptionService = interruptionService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	// private void initCriteria4Search(HttpServletRequest request){
	// Object obj = request.getSession().getAttribute("CRITERIA_4SEARCH");
	// if(obj!=null){
	// this.criteria = (CommonCriteria)obj;
	// //request.getSession().removeAttribute("CRITERIA_4SEARCH");
	// }
	// }
	private void initCriteria4Reporting(HttpServletRequest request) {
		Object obj = request.getSession().getAttribute("CRITERIA_4REPORTING");
		if (obj != null) {
			this.criteria = (CommonCriteria) obj;
			this.criteria.setNumOfRows(0);
			// request.getSession().removeAttribute("CRITERIA_4SEARCH");
		}
	}

	private void initHeaderTitles(HttpServletRequest request) {
		Object obj = request.getSession().getAttribute("headerTitles");
		if (obj != null) {
			fields = (List) obj;

			this.titles = new ArrayList<String>();
			for (Field f : fields) {
				if (f.isCheck())
					this.titles.add(f.getLabel().substring(0, f.getLabel().length() - 2));
			}

		}
	}

	private void getDataResult(HttpServletRequest request) {
		Object obj = request.getSession().getAttribute("DATA_RESULT");
		if (obj != null) {
			this.dataResult = (List) obj;
			request.getSession().removeAttribute("DATA_RESULT");
		}
		obj = request.getSession().getAttribute("headerTitles");
		if (obj != null) {
			titles = (List) obj;
		}
		Object objDateDebut = request.getSession().getAttribute("DATE_DEBUT");
		if (objDateDebut != null) {
			this.DateDebut = (Date) objDateDebut;
			request.getSession().removeAttribute("DATE_DEBUT");
		}
		Object objDateFin = request.getSession().getAttribute("DATE_FIN");
		if (objDateFin != null) {
			this.DateFin = (Date) objDateFin;
			request.getSession().removeAttribute("DATE_FIN");
		}
		Object objTauxList = request.getSession().getAttribute("TAUX_LIST");
		if (objDateFin != null) {
			this.tauxList = (Integer[]) objTauxList;
			request.getSession().removeAttribute("TAUX_LIST");
		}

	}

	public MouvementManageableService getMouvementService() {
		return mouvementService;
	}

	public void setMouvementService(MouvementManageableService mouvementService) {
		this.mouvementService = mouvementService;
	}

	private List<CrossData> loadMvtCrossData(CommonCriteria criteria) throws Exception {
		CommonCriteria vcriteria = (CommonCriteria) BeanUtils.cloneBean(criteria);
		List<CrossData> results1 = new ArrayList<CrossData>();
		Calendar calDebut = new GregorianCalendar();
		Calendar calFin = new GregorianCalendar();
		CrossData data1;
		List<Integer> usersId = vcriteria.getIndexs();
		List<Mouvement> mvts;
		int j;
		for (int i = 0; i < usersId.size(); i++) {
			User user = this.userService.load(usersId.get(i));
			vcriteria.setIndexs(new ArrayList<Integer>());
			vcriteria.getIndexs().add(usersId.get(i));
			calDebut.setTime(criteria.getDateDebut());
			calFin.setTime(criteria.getDateFin());
			while (calDebut.compareTo(calFin) <= 0) {
				vcriteria.setDateDebut(calDebut.getTime());
				vcriteria.setDateFin(calDebut.getTime());
				mvts = CommonTransformerData.sortByDate(mouvementService.readByCriteria(vcriteria), true);
				data1 = new CrossData();
				data1.setUser(user);
				data1.setDate(calDebut.getTime());
				if (mvts != null)
					for (Mouvement m : mvts)
						data1.setMvts(data1.getMvts() + " " + Utils.convertDate2Str(m.getDate(), "HH:mm"));

				results1.add(data1);
				calDebut.add(Calendar.DAY_OF_MONTH, 1);
			}
		}
		return results1;
	}

	private List<CommonReportData> loadMvtReportData(CommonCriteria criteria) throws Exception {
		CommonCriteria vcriteria = (CommonCriteria) BeanUtils.cloneBean(criteria);
		List<CommonReportData> results = new ArrayList<CommonReportData>();
		List<Integer> usersId = vcriteria.getIndexs();
		List<Integer> newListIndex;
		Date dateDebut = criteria.getDateDebut();
		Date dateFin = criteria.getDateFin();
		for (int i = 0; i < usersId.size(); i++) {
			newListIndex = new ArrayList<Integer>();
			newListIndex.add(usersId.get(i));
			vcriteria.setIndexs(newListIndex);
			vcriteria.setDateDebut(dateDebut);
			vcriteria.setDateFin(dateFin);
			results.add(loadMvtReportDataByUser(vcriteria));

		}
		return results;
	}

	private CommonReportData loadMvtReportDataByUser(CommonCriteria vcriteria) throws Exception {
		User user = this.userService.load(vcriteria.getIndexs().get(0));
		CommonReportData data = new CommonReportData();
		data.setUser(user);
		data.setResults(groupUserMvmntsByDate(vcriteria));
		return data;
	}

	private List<CommonReportData> groupUserMvmntsByDate(CommonCriteria vcriteria) throws Exception {
		Calendar calDebut = new GregorianCalendar();
		Calendar calFin = new GregorianCalendar();
		calDebut.setTime(criteria.getDateDebut());
		calFin.setTime(criteria.getDateFin());
		CommonReportData data;
		List<CommonReportData> results = new ArrayList<CommonReportData>();
		while (calDebut.compareTo(calFin) <= 0) {
			vcriteria.setDateDebut(calDebut.getTime());
			vcriteria.setDateFin(calDebut.getTime());
			data = new CommonReportData();
			data.setDatemMvt(calDebut.getTime());
			data.setResults(CommonTransformerData.sortByDate(mouvementService.readByCriteria(vcriteria), true));
			results.add(data);
			calDebut.add(Calendar.DAY_OF_MONTH, 1);

		}
		return results;

	}

	private List<CommonReportData> agroupUserMvmntsByDate(List<Mouvement> inList) {
		try {
			if (inList != null) {
				Iterator<Mouvement> it = inList.iterator();
				Mouvement mvt = null;
				Date date = null;
				List<CommonReportData> results = new ArrayList<CommonReportData>();
				HashMap<Date, Date> hash = new HashMap<Date, Date>();
				CommonReportData data;

				while (it.hasNext()) {
					mvt = it.next();
					date = mvt.getDate();
					date = new Date(date.getYear(), date.getMonth(), date.getDate());
					if (!hash.containsKey(date)) {
						hash.put(date, date);
						data = new CommonReportData();
						data.setDatemMvt(date);
						data.setResults(searchMvtListByDate(date, inList));
						results.add(data);
					}

				}

				return results;
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<CommonReportData> getMvtGroupedList(CommonCriteria crit) {
		try {
			List<CommonReportData> results = null;
			// Search users
			CommonCriteria usrCrit = new CommonCriteria();
			usrCrit.setMatriculeInf(crit.getMatriculeInf());
			usrCrit.setMatriculeSup(crit.getMatriculeSup());
			usrCrit.setIndexs(crit.getIndexs());
			List<User> usersSearched = userService.readByCriteria(usrCrit);
			if (usersSearched != null && !usersSearched.isEmpty()) {
				Iterator<User> it = usersSearched.iterator();
				CommonReportData data;
				User usr;
				results = new ArrayList<CommonReportData>();
				while (it.hasNext()) {
					usr = it.next();
					data = new CommonReportData();
					data.setUser(usr);
					data.setResults(getMvtGroupedListByDate(crit, usr));
					results.add(data);
				}
			}
			return results;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public List<CommonReportData> getMvtGroupedListByDate(CommonCriteria crit, User usr) {
		try {
			List<CommonReportData> results = null;
			Calendar calStart = new GregorianCalendar();
			calStart.setTime(crit.getDateDebut());
			Calendar calEnd = new GregorianCalendar();
			calEnd.setTime(crit.getDateFin());
			CommonReportData data;
			results = new ArrayList<CommonReportData>();
			CommonCriteria mvtCrit = new CommonCriteria();
			mvtCrit.setMatriculeInf(usr.getMatricule());
			mvtCrit.setMatriculeSup(usr.getMatricule());

			while (calStart.compareTo(calEnd) <= 0) {
				data = new CommonReportData();
				data.setDatemMvt(calStart.getTime());
				mvtCrit.setDateDebut(calStart.getTime());
				mvtCrit.setDateFin(calStart.getTime());
				data.setResults(mouvementService.readByCriteria(mvtCrit));
				results.add(data);
				calStart.add(Calendar.DAY_OF_MONTH, 1);
			}
			return results;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	private List searchMvtListByDate(Date date, List inList) {

		if (inList != null) {
			Iterator<Mouvement> it = inList.iterator();
			Mouvement mvt = null;
			List<Mouvement> results = new ArrayList<Mouvement>();
			while (it.hasNext()) {
				mvt = it.next();
				if (date.equals(new Date(mvt.getDate().getYear(), mvt.getDate().getMonth(), mvt.getDate().getDate()))) {
					results.add(mvt);
				}
			}
			return CommonTransformerData.sortByDate(results, true);
		}
		return null;
	}

	// public List<CommonReportData> getCumulGroupedList(List<Mouvement> inList){
	// try {
	// if(inList != null){
	// Iterator<Mouvement> it = inList.iterator();
	// Mouvement mvt = null;
	// String badge = null;
	// List<CommonReportData> results = new ArrayList<CommonReportData>();
	// HashMap<String,String> hash = new HashMap<String, String>();
	// CommonReportData data;
	// Hashtable<String, String> hasht ;
	// User user;
	// while(it.hasNext()){
	// mvt = it.next();
	// badge = mvt.getBadge();
	// if(badge == null || (!hash.containsKey(badge))){
	// hash.put(badge, badge);
	//
	// data = new CommonReportData();
	// hasht = new Hashtable<String, String>();
	// hasht.put("badge", badge);
	// user = (User)this.userService.read(hasht).get(0);
	// data.setUser(user);
	//
	// data.setResults(getMvtGroupedListByDate(searchMvtListByUser(badge,inList),user));
	//
	// results.add(data);
	// }
	//
	// }
	//
	// return results;
	// }
	// return null;
	// } catch (Exception e) {
	// e.printStackTrace();
	// return null;
	// }
	//
	// }
	private List getRetardByDate(Date date, User user) {
		try {
			Hashtable<String, Object> hash = new Hashtable<String, Object>();
			hash.put("date", date);
			hash.put("user", user);
			return this.retardService.read(hash);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private double getInterruptionByDate(Date date, User user) {
		double somme = 0;
		try {

			Hashtable<String, Object> hash = new Hashtable<String, Object>();
			hash.put("date", date);
			hash.put("user", user);
			List results = this.interruptionService.read(hash);
			if (results != null && results.size() > 0) {
				Iterator<Interruption> it = results.iterator();
				while (it.hasNext()) {
					somme += it.next().getDuree();
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return somme;
	}

	public void setHoraireService(HoraireManageableService horaireService) {
		this.horaireService = horaireService;
	}

	public void setTypeAbsenceService(TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

}