package ma.nawarit.checker.beans.share;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.utils.Globals;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

@ManagedBean
@SessionScoped
public class ReportingBean implements Controller {
	private Logger log = Logger.getLogger(ReportingBean.class);
	private User user;
	private UserManageableService userService;
	private UnitManageableService siteService;
	private CongeManageableService congeService;
	// private BasicDataSource dataSource;
	// private String reportType;
	// private String actionName;
	private String congeId;

	public ReportingBean() {
		this.user = new UserImpl();
	}

	public void generateCustumReportAction(String actionName, String reportType) {
		try {
			log.info("generateCustumReportAction : " + actionName + ":" + reportType);

			FacesContext fc = FacesContext.getCurrentInstance();
			Object obj = fc.getExternalContext().getSessionMap().get("CRITERIA_4SEARCH");
			if (obj != null) {
				fc.getExternalContext().getSessionMap().remove("CRITERIA_4SEARCH");
				fc.getExternalContext().getSessionMap().put("CRITERIA_4REPORTING", obj);
			}
			String cname = Globals.CONTEXT_NAME;
			fc.getExternalContext().redirect(
					cname + "/jasperSpring/" + actionName + "?action=" + actionName + "&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateHoraireReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/horaireReport?action=horaireReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateNodeReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext()
					.redirect(Globals.CONTEXT_NAME + "/jasperSpring/nodeReport?action=nodeReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateUserReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext()
					.redirect(Globals.CONTEXT_NAME + "/jasperSpring/userReport?action=userReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateAnomalieReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/anomalieReport?action=anomalieReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateRetardReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/retardReport?action=retardReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateInterruptionReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(Globals.CONTEXT_NAME
					+ "/jasperSpring/interruptionReport?action=interruptionReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateCongeReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/congeReport?action=congeReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateCRRSReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(Globals.CONTEXT_NAME + "/jasperSpring/CRRSReport?action=CRRSReport&format="
					+ reportType + "&conge=" + congeId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateAbsenceReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/absenceReport?action=absenceReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateMouvementReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext()
					.redirect(Globals.CONTEXT_NAME + "/jasperSpring/mvtReport?action=mvtReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateCumulReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext().redirect(
					Globals.CONTEXT_NAME + "/jasperSpring/cumulReport?action=cumulReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generateCantineReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext()
					.redirect(Globals.CONTEXT_NAME + "/jasperSpring/cantineReport?action=cantineReport&format=pdf");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void generatePaieReportAction(String reportType) {
		try {
			FacesContext fc = FacesContext.getCurrentInstance();
			fc.getExternalContext()
					.redirect(Globals.CONTEXT_NAME + "/jasperSpring/paieReport?action=paieReport&format=" + reportType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// public void generateAllUserReportAction(String reportType) {
	// try {
	// FacesContext fc = FacesContext.getCurrentInstance();
	// ExternalContext ec= fc.getExternalContext();
	// if(dataSource!= null){
	// if(dataSource.getConnection()!=null){
	// ReportGenerator ge = new
	// ReportGenerator(reportType,dataSource.getConnection()) ;
	// HttpServletResponse response=(HttpServletResponse)ec.getResponse();
	// ServletContext scontext = (ServletContext)ec.getContext();
	// String rootpath = scontext.getRealPath("/");
	// File fichier=new File(rootpath +
	// "\\WEB-INF\\reports\\jrxml\\custum-report.jrxml") ;
	// ge.showInBrowser(fichier,response);
	// fc.responseComplete();
	// }
	// }
	// } catch (SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (log.isDebugEnabled()) {
			log.debug("entering 'handleRequest' method...");
		}

		String viewName = "userList";
		if (request != null && request.getParameter("report") != null) {
			viewName += request.getParameter("report");
		}

		Map model = new HashMap();
		return new ModelAndView("report1", "users", getModel2(request.getSession().getAttribute("reportingBean")));
	}

	// public ModelAndView handleRequest(HttpServletRequest request,
	// HttpServletResponse response) throws ServletException, IOException,
	// ClassNotFoundException, SQLException {
	// //return new ModelAndView("report1",
	// getModel(request.getSession().getAttribute("reportingBean")));
	// return new ModelAndView("report1", "users",
	// getModel(request.getSession().getAttribute("reportingBean")));
	// }
	//
	public List getModel2(Object obj) {
		try {
			ReportingBean bean = (ReportingBean) obj;
			return bean.getUserService().readAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public Map getModel(Object obj) {
		HashMap model = new HashMap();

		try {
			ReportingBean bean = (ReportingBean) obj;
			java.sql.ResultSet resultSet = bean.getUserService().executeQuery(bean.getUser());
			JRResultSetDataSource resultSetDataSource = new JRResultSetDataSource(resultSet);
			// Collection<User> users = bean.getUserService().find(bean.getUser());
			// JRBeanCollectionDataSource resultSetDataSource = new
			// JRBeanCollectionDataSource(users);
			model.put("datasource", resultSetDataSource);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return model;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	// public void generateReport(ActionEvent actionEvent)
	// throws ClassNotFoundException, SQLException, IOException,JRException{
	// FacesContext fc = FacesContext.getCurrentInstance();
	// HttpServletResponse response =
	// (HttpServletResponse)fc.getExternalContext().getResponse();
	// ExternalContext ec= fc.getExternalContext();
	// ServletContext scontext = (ServletContext)ec.getContext();
	// String filePath = scontext.getRealPath("/") +
	// "\\WEB-INF\\reports\\all-users.jrxml";
	// InputStream reportStream =
	// fc.getExternalContext().getResourceAsStream("\\reports\\all-users.jrxml");
	// ServletOutputStream servletOutputStream = response.getOutputStream();
	// byte[] bytes = JasperRunManager.runReportToPdf(filePath, new HashMap(),
	// this.dataSource.getConnection());
	// servletOutputStream.write(bytes);
	// this.dataSource.getConnection().close();
	// response.setHeader("Expires", "0");
	// response.setHeader("Cache-Control", "must-revalidate, post-check=0,
	// pre-check=0");
	// response.setHeader("Pragma", "public");
	// response.setHeader("Content-disposition", "attachment;
	// filename=constanciaCreacion.pdf");
	//
	// response.setContentType("application/pdf");
	// servletOutputStream.flush();
	// servletOutputStream.close();
	// }

	public void generateReport(ActionEvent actionEvent)
			throws ClassNotFoundException, SQLException, IOException, JRException {
		try {
			Connection connection;
			FacesContext context = FacesContext.getCurrentInstance();
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			// InputStream reportStream =
			// context.getExternalContext().getResourceAsStream("/reports/all-users.jrxml");
			ServletOutputStream servletOutputStream = response.getOutputStream();

			Class.forName("com.mysql.jdbc.Driver");
			connection = DriverManager
					.getConnection("jdbc:mysql://localhost:3306/checktime_1_1?user=root&password=root");
			// byte[] bytes = JasperRunManager.runReportToPdf("c:/reports/all-users.jasper",
			// new HashMap(), connection);
			// //JasperRunManager.runReportToPdfStream(reportStream, servletOutputStream,
			// new HashMap(), connection);
			// servletOutputStream.write(bytes);
			// connection.close();
			// response.setContentType("application/pdf");
			// servletOutputStream.flush();
			// servletOutputStream.close();

			JasperDesign jasperDesign = JRXmlLoader.load("c:/reports/all-users.jrxml");
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), connection);
			connection.close();

			response.setContentType("application/pdf");
			response.setHeader("Content-disposition", "filename=etat.pdf");
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
			exporter.exportReport();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void showReport() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();

			List results = this.userService.readAll();
			Map parameters = new HashMap();
			parameters.put("MYDATE", "01/07/2012");
			parameters.put("format", "pdf");
			parameters.put("WEBDIR", ((ServletContext) context.getExternalContext().getContext()).getRealPath("/"));
			InputStream is = context.getExternalContext().getResourceAsStream("/WEB-INF/reports/userList.jrxml");
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(results);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.setContentType("application/pdf");
			response.addHeader("Content-Disposition", "attachment; filename=userList.pdf");
			JasperDesign jasperDesign = JRXmlLoader.load(is);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			context.responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	public void showSiteReport() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();

			List results = this.siteService.readAll();
			Map parameters = new HashMap();
			parameters.put("MYDATE", "01/07/2012");
			parameters.put("format", "pdf");
			parameters.put("WEBDIR", ((ServletContext) context.getExternalContext().getContext()).getRealPath("/"));
			InputStream is = context.getExternalContext().getResourceAsStream("/WEB-INF/reports/siteList.jrxml");
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(results);
			HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
			response.setContentType("application/pdf");
			response.setHeader("Content-Disposition", "attachment; filename=siteList.pdf");
			JasperDesign jasperDesign = JRXmlLoader.load(is);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
			JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			// context.responseComplete();

		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
	}

	public String showMyReport() {
		try {
			List results = this.userService.readAll();
			FacesContext context = FacesContext.getCurrentInstance();
			Map parameters = new HashMap();
			ServletContext srvCtxt = (ServletContext) context.getExternalContext().getContext();
			String ctxPath = srvCtxt.getRealPath("/");
			parameters.put("MYDATE", "01/07/2012");
			parameters.put("format", "pdf");
			parameters.put("WEBDIR", ctxPath);

			// prepare report and data
			InputStream is = srvCtxt.getResourceAsStream("/WEB-INF/reports/userList.jrxml");
			JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(results);

			// generate pdf file
			JasperDesign jasperDesign = JRXmlLoader.load(is);
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, ds);
			JasperExportManager.exportReportToPdfFile(jasperPrint, "c:/resources/userList.pdf");
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		}
		return null;
	}

	public void setSiteService(UnitManageableService siteService) {
		this.siteService = siteService;
	}

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public String getCogeId() {
		return congeId;
	}

	public void setCongeId(String congeId) {
		this.congeId = congeId;
	}

}
