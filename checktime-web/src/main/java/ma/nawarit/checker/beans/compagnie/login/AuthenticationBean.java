package ma.nawarit.checker.beans.compagnie.login;

import java.io.Serializable;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import ma.nawarit.checker.security.license.use.LicenseUsageService;
import ma.nawarit.checker.security.license.use.MyLicenseValidator;
import ma.nawarit.checker.security.license.use.PublicKeyProvider;
import ma.nawarit.checker.security.license.use.PublicPasswordProvider;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;


import org.acegisecurity.context.SecurityContext;
import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.providers.encoding.PasswordEncoder;
import org.acegisecurity.ui.AbstractProcessingFilter;
import org.acegisecurity.userdetails.UserDetails;
import org.apache.axis.security.AuthenticatedUser;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import net.nicholaswilliams.java.licensing.FileLicenseProvider;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.LicenseManager;
import net.nicholaswilliams.java.licensing.LicenseManagerProperties;
import net.nicholaswilliams.java.licensing.exception.ExpiredLicenseException;
import net.nicholaswilliams.java.licensing.exception.InvalidLicenseException;

/**
 * Bean associ� au module gestion des profiles m�tier.
 * <DL>
 * <DT><B>Nom du Projet :</B>
 * <DD>CheckTime
 * <DD>
 * <DT><B>Service :</B>
 * <DD>NAWAR IT</DD>
 * <DT><B>Cr�e le :</B>
 * <DD>Tue Aug 05 09:00:08 GMT 2008</DD>
 * </DL>
 * 
 * @since 05/08/2008
 * @version 1.0
 * @author k.lamhaddab
 */

@ManagedBean(name="authenticationBean")
@ViewScoped
public class AuthenticationBean implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	
	private static final Logger log = Logger.getLogger(AuthenticationBean.class);
	private String login;
	private String password;
	private String newPassword;
	

	private User userAuthenticated;
	private Boolean resetPassword;
	

	@ManagedProperty(value="#{UserManageableService}")
	private UserManageableService userService;
	private PasswordEncoder passwordEncoder;

	/**
	 * default empty constructor
	 */
	public AuthenticationBean() {
		FacesContext fc = FacesContext.getCurrentInstance();
		
		Exception ex = (Exception) fc
				.getExternalContext()
				.getSessionMap()
				.get(AbstractProcessingFilter.ACEGI_SECURITY_LAST_EXCEPTION_KEY);
		Object obj = fc.getExternalContext().getSessionMap()
				.get(Globals.USER_AUTHENTICATED);
		if (obj == null && ex != null) {
			if (ex.getMessage().startsWith("Maximum sessions of"))
				FacesContext
						.getCurrentInstance()
						.addMessage(
								null,
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										MessageFactory
												.getMessage(Globals.USER_CONNECT),
										ex.getMessage()));
			else
				FacesContext.getCurrentInstance().addMessage(
						null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MessageFactory
										.getMessage(Globals.USER_NOT_FOUND), ex
										.getMessage()));
		}

	}
	
	public String getLogin() {
		try {
			this.login=userAuthenticated.getLogin();
		} catch (Exception e) {
			// TODO: handle exception
		}
		return login;
	}

	public void setLogin(String _login) {
		this.login = _login;
	}
	
	public String getRole() {
		getUserAuthenticated();
		return userAuthenticated.getProfilApp().getCode();
	}
	
	public User getUserAuthenticated() {
		try {
			User user = null;
			FacesContext fc = FacesContext.getCurrentInstance();
			Object obj = fc.getExternalContext().getSessionMap()
					.get(Globals.USER_AUTHENTICATED);
			SecurityContext secCtx = SecurityContextHolder.getContext();

			if (secCtx != null && secCtx.getAuthentication() != null
					&& secCtx.getAuthentication().getPrincipal() != null && (!(secCtx.getAuthentication().getPrincipal()  instanceof String))) {
				String userName = ((UserDetails) secCtx
						.getAuthentication().getPrincipal()).getUsername();
				Hashtable<String, String> hash = new Hashtable<String, String>();
				hash.put("login", userName);
				List<User> users = this.userService.read(hash);
				if (users != null && users.size() == 1)
					user = users.get(0);
				fc.getExternalContext().getSessionMap()
						.put(Globals.USER_AUTHENTICATED, user);
			} else {
				user = (User) obj;
			}

			this.userAuthenticated = user;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		} finally {
			return userAuthenticated;
		}

	}

	public void setUserAuthenticated(User userAuthenticated) {
		this.userAuthenticated = userAuthenticated;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void resetPasswordListener(String rest) {
		if(rest.equals("true")) {
			this.resetPassword=true;
		}else {
			this.resetPassword=false;
		}
	}
	
	public void changeUserPwdAction() {
		try {
			if(this.resetPassword) {
				Hashtable<String, String> hash = new Hashtable<String, String>();
				hash.put("login", getLogin());

				List<User> result = userService.read(hash);
				if (result != null && !result.isEmpty()) {
					User user = result.get(0);
					System.out.println("user searched :: " + user.getNom());
					//boolean isPwdValid = passwordEncoder.isPasswordValid(
					//		user.getPassword(), getPassword(), null);
					//System.out.println("isPwdValid :: " + isPwdValid);
					if (validatePassword()) {
						user.setLogin(login);
						user.setPassword(passwordEncoder.encodePassword(
								newPassword, null));
						userService.update(user);
						System.out.println("User password updated ! ");
						FacesContext fc = FacesContext.getCurrentInstance();
						fc.addMessage(
								"LOGIN",
								new FacesMessage(
										FacesMessage.SEVERITY_ERROR,
										"Changement de mot de passe a été effectué avec succès !",
										null));

					} else {
						FacesContext fc = FacesContext.getCurrentInstance();
						fc.addMessage("LOGIN", new FacesMessage(
								FacesMessage.SEVERITY_ERROR,
								"Le Mot de passe saisi est incorrect!", null));
					}

				} else {
					FacesContext fc = FacesContext.getCurrentInstance();
					fc.addMessage("LOGIN", new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"Veuilez renseigner un Login correct!", null));
				}
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Boolean validatePassword() {
		Boolean isValid=false;
		if(!this.login.equals("") && this.password.equals(this.newPassword)) {
			isValid=true;
		}
		return isValid;
	}


	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}
	public Boolean getResetPassword() {
		return resetPassword;
	}

	public void setResetPassword(Boolean resetPassword) {
		this.resetPassword = resetPassword;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	public static User getCurrentUser() {
		FacesContext fc = FacesContext.getCurrentInstance();
		Object obj = fc.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		return (User) obj;
	}
}
