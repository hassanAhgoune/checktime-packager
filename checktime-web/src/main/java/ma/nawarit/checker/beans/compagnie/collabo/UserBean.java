package ma.nawarit.checker.beans.compagnie.collabo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.servlet.ServletContext;

import ma.nawarit.checker.batch.hard.TerminalOrderService;
import ma.nawarit.checker.beans.compagnie.login.AuthenticationBean;
import ma.nawarit.checker.compagnie.crud.*;
import ma.nawarit.checker.compagnie.*;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.equipement.Terminal;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.suivi.Except;
import ma.nawarit.checker.suivi.ExceptImpl;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import ma.nawarit.checker.utils.syncRH.databaseOperations;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;

import org.acegisecurity.providers.encoding.PasswordEncoder;
import org.andromda.spring.CommonCriteria;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.collection.PersistentSet;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.selectonelistbox.SelectOneListbox;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TransferEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.BeanUtils;
import org.springframework.transaction.annotation.Transactional;
import org.tempuri.SyncUsersSoap;
import org.tempuri.SyncUsersSoapProxy;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.FileUploadEvent;

@ManagedBean(name = "userBean")
@ViewScoped
public class UserBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1506754237590967483L;

	private static final Logger logger = Logger.getLogger(UserBean.class);

	@ManagedProperty(value = "#{UserManageableService}")
	private transient UserManageableService userService;

	@ManagedProperty(value = "#{NoeudManageableService}")
	private transient NoeudManageableService noeudService;

	@ManagedProperty(value = "#{GroupManageableService}")
	private transient GroupManageableService groupService;

	@ManagedProperty(value = "#{ProfilMetierManageableService}")
	private transient ProfilMetierManageableService fonctionService;

	@ManagedProperty(value = "#{UnitManageableService}")
	private transient UnitManageableService uniteService;

	@ManagedProperty(value = "#{ProfilAppManageableService}")
	private transient ProfilAppManageableService roleService;

	@ManagedProperty(value = "#{PlanningManageableService}")
	private transient PlanningManageableService planningService;

	@ManagedProperty(value = "#{passwordEncoder}")
	private PasswordEncoder passwordEncoder;

	@ManagedProperty(value = "#{terminalOrderService}")
	private TerminalOrderService terminalOrderService;

	@ManagedProperty("#{BadgeManageableService}")
	private transient BadgeManageableService badgeService;


	private Date date = new Date();

	private int roleId;
	private List<ProfilApp> roles;

	private Unit unite;
	private List<Unit> unites;

	private List<Noeud> noeuds;

	private ProfilMetier fonction;
	private List<ProfilMetier> fonctions;

	private Group group;
	private boolean addToGrdoup;
	private List<Group> groups;

	private CommonCriteria criteria;

	private TreeNode unitTreeNode;

	private List<User> users;
	private List<User> selectedUsers;
	private UserDataModel dataModel;
	private DataTable usersTable = new DataTable();
	private User user = new UserImpl();
	private List<User> filtredUsers;
	private List<User> managers;
	private User manager = new UserImpl();
	private int selectedManagerId;
	private List<Planning> plannings;
	private Planning planning;

	private boolean view_dlg = false;
	private boolean detail_dlg = false;
	private boolean add_dlg = false;
	private boolean edit_dlg = false;

	private boolean droitPaie = false;
	private TreeNode treeNode;
	private TreeNode treeNode2;
	private TreeNode selectedNode;
	private Noeud  noeud = new ma.nawarit.checker.compagnie.NoeudImpl();

	private boolean resetPassword;

	private Date dateDebut;
	private Date dateFin;

	private ProfilMetier profilMetier = new ProfilMetierImpl();

	private TabView tabView;
	private User usrAuthenticated;

	private boolean inTheGroup;

	// badge properties
	private String selectedBadge;
	private String badgeNumber;
	private List<Badge> badges;
	private Boolean operation;
	private Boolean isROLE_RH = false;
	private boolean isRoleSuph = false;

	// change password
	private String password;
	private String rePassword;
	private String login;
	private boolean showLoginPass = false;
	// User Web Service
	public SyncUsersSoap userWebService = new SyncUsersSoapProxy();

	/**
	 * THIS CODE IS RESPONSIBLE ON ADDING PHOTO TO A USER
	 */
	private String UrlImage = ".." + File.separator + ".." + File.separator + ".." + File.separator + "upload"
			+ File.separator + "photos" + File.separator + "UserImage";

	private UploadedFile file;

	public UploadedFile getFile() {
		return file;
	}

	public String getUrlImage() {
		return this.UrlImage;
	}

	public void setUrlImage(String url) {
		this.UrlImage = url;
	}

	public void LoadImage(String Matricule) {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String realPath = ctx.getRealPath("/");
		File userPhoto = new File(
				realPath + "upload" + File.separator + "photos" + File.separator + Matricule + ".png");
		if (!userPhoto.exists()) {
			setUrlImage(".." + File.separator + ".." + File.separator + ".." + File.separator + "upload"
					+ File.separator + "photos" + File.separator + "UserImage");
		} else {
			setUrlImage(".." + File.separator + ".." + File.separator + ".." + File.separator + "upload"
					+ File.separator + "photos" + File.separator + Matricule);
		}
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public void upload(FileUploadEvent event) {
		file = event.getFile();
		tempImageDeleter();
		setTempImagename();
		tempImageSaver();
		tempImageLoad();
	}

	public void ImageDeleter(String FileName) {
		try {
			ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String realPath = ctx.getRealPath("/");

			File fileImage = new File(
					realPath + "upload" + File.separator + "photos" + File.separator + FileName + ".png");
			fileImage.delete();
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage("Erreur de suppression de l'image d'utilisateur");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public void ImageSaver(String FileName) {
		if (file != null) {
			try {
				ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
						.getContext();
				String realPath = ctx.getRealPath("/");

				File fileImage = new File(
						realPath + "upload" + File.separator + "photos" + File.separator + FileName + ".png");
				InputStream inputStream = file.getInputstream();
				OutputStream outputStream = new FileOutputStream(fileImage);
				IOUtils.copy(inputStream, outputStream);
			} catch (IOException e) {
				e.printStackTrace();
				FacesMessage message = new FacesMessage(
						"There was a probleme your file was not uploaded." + e.getMessage(), e.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
	}

	/**
	 * This Code Responsible On Displaying the Photo After It Loaded
	 */

	public String tempImageName;

	public void tempImageLoad() {
		ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		String realPath = ctx.getRealPath("/");
		File userPhoto = new File(realPath + "upload" + File.separator + "photos" + File.separator + "temp"
				+ File.separator + tempImageName + ".png");
		if (userPhoto.exists()) {
			setUrlImage(".." + File.separator + ".." + File.separator + ".." + File.separator + "upload"
					+ File.separator + "photos" + File.separator + "temp" + File.separator + tempImageName);
		}
	}

	public void setTempImagename() {
		tempImageName = String.valueOf(Math.random());
	}

	public void tempImageSaver() {
		if (file != null) {
			try {
				ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext()
						.getContext();
				String realPath = ctx.getRealPath("/");

				// File fileImage = new File(realPath + "upload" + File.separator + "photos" +
				// File.separator + "temp"
				// + File.separator + tempImageName + ".png");
				File fileImage = new File(".." + File.separator + ".." + File.separator + ".." + File.separator
						+ "upload" + File.separator + "photos" + File.separator + "temp" + File.separator
						+ tempImageName + ".png");
				InputStream inputStream = file.getInputstream();
				OutputStream outputStream = new FileOutputStream(fileImage);
				IOUtils.copy(inputStream, outputStream);
			} catch (IOException e) {
				e.printStackTrace();
				FacesMessage message = new FacesMessage(
						"There was a probleme your file was not uploaded." + e.getMessage(), e.getMessage());
				FacesContext.getCurrentInstance().addMessage(null, message);
			}
		}
	}

	public void tempImageDeleter() {
		try {
			ServletContext ctx = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
			String realPath = ctx.getRealPath("/");
			File fileImage = new File(realPath + "upload" + File.separator + "photos" + File.separator + "temp"
					+ File.separator + tempImageName + ".png");
			fileImage.delete();
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage message = new FacesMessage("Erreur de suppression de l'image d'utilisateur");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	// Method that add new Badge
	public void addBadge() {
		try {
			Boolean exist = false;
			for (Badge item : badges) {
				if (item.getNumber().equals(badgeNumber)) {
					exist = true;
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Attention! - Le numéro de badge existe déjà", "Attention!");
					FacesContext.getCurrentInstance().addMessage(null, message);
					break;
				}
			}
			if (badgeService.ReadByBadgeNumber(badgeNumber).size() > 0) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"Erreur! - Le numéro de badge est déjà utilisé", "Erreur!");
				FacesContext.getCurrentInstance().addMessage(null, message);
				exist = true;
			}
			if (!exist.booleanValue()) {
				Badge entity = new BadgeImpl();
				entity.setNumber(badgeNumber);
				entity.setCollaborateur(user);
				if (operation) {
					badgeService.create(entity);
				}
				badges.add(entity);
				badgeNumber = "";
			}
		} catch (Exception ex) {

		}
	}

	// After Validating Badge
	public void validateBadge() {
		user.setBadge(null);
		user.setBadge(selectedBadge);
	}

	// On badge Selectedd
	public void onSelectBadge() {
		for (Badge item : badges) {
			if (item.getNumber().equals(selectedBadge)) {
				badgeNumber = item.getNumber();
			}
		}
	}

	// Delete The Badge Selected
	public void deleteBadge() throws Exception {
		if (operation) {
			for (Badge item : badges) {
				if (item.getNumber().equals(selectedBadge)) {
					badgeService.delete(item);
					badges.remove(item);
					badgeNumber = "";
				}
			}
		} else {
			for (Badge item : badges) {
				if (item.getNumber().equals(selectedBadge)) {
					badges.remove(item);
					badgeNumber = "";
				}
			}
		}
	}

	/**
	 * 
	 */
	public UserBean() {
		// TODO Auto-generated constructor stub
		FacesContext context = FacesContext.getCurrentInstance();
		users = new ArrayList<User>();
		badges = new ArrayList();
		group = new GroupImpl();
		noeud.setNoeud(new ma.nawarit.checker.compagnie.NoeudImpl());
		criteria = new CommonCriteria();
		selectedUsers = new ArrayList<User>();
		Object obj = context.getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
		usrAuthenticated = (User) obj;
		operation = Boolean.valueOf(false);
		isROLE_RH = context.getExternalContext().isUserInRole("ROLE_RH");
		isRoleSuph= context.getExternalContext().isUserInRole("ROLE_SUPH");
		if (isROLE_RH) {
			roleId = 2;
		}
	}

	public void setPasswordForAllUsers() throws Exception {
		List<User> usrList = userService.readAll();
		for (User u : usrList) {
			if (u.getLogin() == null || u.getPassword() == null) {
				String log = "check_" + u.getMatricule();
				u.setPassword(passwordEncoder.encodePassword(log, null));
				u.setLogin(log);
				userService.update(u);
			} else {
				if (u.getLogin().length() == 0 || u.getPassword().length() == 0) {
					String log = "check_" + u.getMatricule();
					u.setPassword(passwordEncoder.encodePassword(log, null));
					u.setLogin(log);
					userService.update(u);
				}
			}
		}
	}

	public void saveUser() throws Exception {
		// user.setEnableConn(true);
		if(user.getDateQuit()!=null) {
			user.setActive(false);
		}
		if (profilMetier != null && profilMetier.getId() > 0)
			user.setProfilMetier(profilMetier);
		if (manager != null && manager.getId() > 0)
			user.setSupH(manager);
		if (selectedNode != null) {
			user.setNoeud((Noeud) selectedNode.getData());
		}
		if(this.noeud!=null) {
			user.setNoeud(this.noeud);
		}
		if(user.getMatricule() != null && !"".equals(user.getMatricule())) {
			//removes non digits
			user.setMatricule(user.getMatricule().replaceAll("\\D+",""));
			String login = "check_" + String.format("%04d", Integer.valueOf(this.user.getMatricule()));
			if((user.getLogin() == null || "".equals(user.getLogin()))) {
				this.user.setPassword(passwordEncoder.encodePassword(login, null));
				this.user.setLogin(login);
			}
		}
		if (roleId > 0 && !isROLE_RH) {
			ProfilApp profil = this.roleService.load(this.roleId);
			Set<Role> roles = new HashSet<Role>();
			Role role = new RoleImpl();
			role.setProfilApp(profil);
			roles.add(role);
			this.user.setRoles(roles);
		}
		if (isRoleSuph)
			this.user.setActive(false);
		if (isROLE_RH) {
			ProfilApp profil = this.roleService.load(2);
			Set<Role> roles = new HashSet<Role>();
			Role role = new RoleImpl();
			role.setProfilApp(profil);
			roles.add(role);
			this.user.setRoles(roles);
			// default password
			this.user.setPassword(passwordEncoder.encodePassword("check_" + this.user.getMatricule(), null));
			this.user.setLogin("check_" + this.user.getMatricule());
		}
		if (resetPassword) {
			this.user.setPassword(passwordEncoder.encodePassword(this.user.getPassword(), null));
		}

		if (user.getId() > 0) {
			userService.update(user);
			// update user in access manager
			Calendar cal = Calendar.getInstance();
			cal.setTime(user.getDateEmb());

			PropertiesReader.loadProperties("syncRH.properties");
			if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
				String unit_code = uniteService.load(user.getUnit().getId()).getCode();
				userWebService.update(user.getMatricule(), user.getNom() + " " + user.getPrenom(), 2, 0, "DEFAULT",
						"Empty", cal, user.getBadge(), unit_code);
			}
		} else {
			user.setDroitPaie(true);
			userService.create(user);
			Calendar cal = Calendar.getInstance();
			cal.setTime(user.getDateEmb());

			PropertiesReader.loadProperties("syncRH.properties");
			if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
				String unitcode = uniteService.load(user.getUnit().getId()).getCode();
				String s = userWebService.add(user.getMatricule(), user.getNom() + " " + user.getPrenom(), 2, 0,
						"DEFAULT", "Empty", cal, user.getBadge(), unitcode);
			}
		}
		criteria.setChanged(true);
		dataModel = null;
		setUrlImage(".." + File.separator + ".." + File.separator + ".." + File.separator + "upload" + File.separator
				+ "photos" + File.separator + "UserImage");
		ImageSaver(user.getMatricule());
		tempImageDeleter();

		// Adding Badges of a new User
		if (!operation.booleanValue()) {
			for (Badge item : badges) {
				item.setCollaborateur(user);
				badgeService.create(item);
			}
		}
		this.manager = new UserImpl();
		this.manager.setId(0);
		badges.clear();
		selectedBadge = "";
		badgeNumber = "";
		
		user = new UserImpl();
		if (isROLE_RH) {
			roleId = 2;
		}
		
		if (tabView != null) {
			tabView.setActiveIndex(0);
			RequestContext.getCurrentInstance().update("collabotabsId");
		}

	}
	public void newUser() {
		user=new UserImpl();
		badges.clear();
		selectedBadge = "";
		badgeNumber = "";
		this.manager = new UserImpl();
		this.manager.setId(0);
	}

	@SuppressWarnings("unused")
	public void savePassword() throws Exception {
		if (resetPassword) {
			if (isLoginValid()) {
				if (isPasswordValid()) {
					Object obj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
					User currentUser = (User) obj;
					currentUser = userService.load(currentUser.getId());
					currentUser.setPassword(passwordEncoder.encodePassword(this.password, null));
					currentUser.setLogin(login);
					userService.update(currentUser);
					addMessage("info", "Operation à été effectuer");
				} else {
					addMessage("error", "Erreur Mot de passe n'est pas valide !");
				}
			} else {
				addMessage("error", "Erreur : login est déja utilisé !");
			}
		} else {

		}
	}

	@SuppressWarnings("unchecked")
	public Boolean isLoginValid() throws Exception {
		Boolean isvalide = false;
		Hashtable<String, String> hash = new Hashtable<String, String>();
		hash.put("login", getLogin());
		List<User> result = userService.read(hash);
		if (result == null)
			isvalide = true;
		else {
			if (result.size() == 0) {
				isvalide = true;
			} else {
				if (result.get(0).getLogin().equals(login)) {
					isvalide = true;
				}
			}
		}
		return isvalide;
	}

	public Boolean isPasswordValid() {
		Boolean isvalide = false;
		if (password != null && rePassword != null) {
			if (password.equals(rePassword)) {
				isvalide = true;
			}
		}
		return isvalide;
	}

	public void addMessage(String type, String Message) {
		FacesMessage message = new FacesMessage(Message);
		FacesContext.getCurrentInstance().addMessage(type, message);
	}

	public void updateDesactivatedUser() {
		try {
			if (this.selectedUsers != null && !this.selectedUsers.isEmpty() && this.dateFin != null) {
				for (User u : this.selectedUsers) {
					u.setDateQuit(dateFin);
					if(u.getDateQuit()!=null) {
						u.setActive(false);
					}
					userService.update(u);
					selectedUsers = new ArrayList<User>();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteUser() {
		try {
			if (selectedUsers != null && !selectedUsers.isEmpty()) {
				// delete user in access manager
				for (User user : selectedUsers) {
					userService.delete(user.getId());

					PropertiesReader.loadProperties("syncRH.properties");
					if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE"))
						userWebService.delete(user.getMatricule());
				}

				this.criteria.setChanged(true);
				selectedUsers = new ArrayList<User>();
				ImageDeleter(user.getMatricule());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void update(int id) throws Exception {
		user = userService.load(id);
		this.criteria.setChanged(true);
	}

	public void onTransfer(TransferEvent event) {
		StringBuilder builder = new StringBuilder();
		for (Object item : event.getItems()) {
			builder.append(((User) item).getNom()).append("<br />");
		}
	}

	/**
	 * L'action qui permet la suppression de noeud
	 * 
	 * @return String
	 */
	public void deleteNoeud() {
		try {

			// Noeud noeud = (Noeud)this.selectedNode.getData();
			if (this.noeud.getId() > 0) {
				// this.noeud.setUsers(null);
				this.noeud = this.noeudService.load(noeud.getId());
				this.noeudService.delete(noeud);
				this.treeNode = null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, MessageFactory.getMessage("Collabo.M1"), null));
		}
	}

	public TreeNode getTreeNode() {
		Noeud parent;
		try {
			if (treeNode == null) {
				this.treeNode = new DefaultTreeNode("ROOT", null);
				if (usrAuthenticated.getNoeud() == null || isROLE_RH) {
					List<Noeud> l = noeudService.readAll();
					if (l != null && !l.isEmpty())
						for (Noeud p : l) {
							if (p != null && p.getNoeud() == null)
								this.buildTree(new DefaultTreeNode(p, this.treeNode));
						}
				} else if (usrAuthenticated != null && usrAuthenticated.getId() > 1) {
					parent = noeudService.load(usrAuthenticated.getNoeud().getId());
					this.buildTree(new DefaultTreeNode(parent, this.treeNode));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.treeNode;
	}

	public TreeNode getTreeNode2() {
		Noeud parent;
		try {
			if (treeNode2 == null) {
				this.treeNode2 = new DefaultTreeNode("ROOT", null);
				parent = noeudService.load(1);
				if (parent != null)
					this.buildTree(new DefaultTreeNode(parent, this.treeNode2));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.treeNode2;
	}

	public void buildTree(TreeNode node) {
		Noeud n = (Noeud) node.getData();
		if (this.user != null && this.user.getNoeud() != null && this.user.getNoeud().getId() == n.getId()) {
			node.setSelected(true);
			expandAllNodes(node);
		}
		if (n.getNoeuds() != null) {
			Noeud nda;
			for (Noeud nd : n.getNoeuds()) {
				nda = new NoeudImpl();
				BeanUtils.copyProperties(nd, nda);
				this.buildTree(new DefaultTreeNode(nda, node));
			}
		}
	}

	public void expandAllNodes(TreeNode n) {
		if (n == null)
			return;
		n.setExpanded(true);
		expandAllNodes(n.getParent());
	}

	public TreeNode getUnitTreeNode() {
		Unit parent;
		try {
			this.unitTreeNode = new DefaultTreeNode("ROOT", null);
			parent = uniteService.load(1);
			Unit unit = new UnitImpl();
			BeanUtils.copyProperties(parent, unit);
			this.buildUnitTree(new DefaultTreeNode(unit, this.unitTreeNode));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.treeNode;
	}

	public void buildUnitTree(TreeNode node) {
		Unit u = (Unit) node.getData();
		if (u.getUnits() != null) {
			Unit nda;
			for (Unit nd : u.getUnits()) {
				nda = new UnitImpl();
				BeanUtils.copyProperties(nd, nda);
				this.buildUnitTree(new DefaultTreeNode(nda, node));
			}
		}
	}

	private Terminal searchPointeuseByZone(Unit unit) {
		try {
			Hashtable<String, Object> hash = new Hashtable();
			hash.put("unit", unit);
			List<Terminal> result = uniteService.read(hash);
			if (result != null && !result.isEmpty())
				return result.get(0);
			return null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public void affectUserUniteAction(TreeNode node) {
		try {
			if (this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
				Unit unit = (Unit) node.getData();
				for (User u : this.selectedUsers) {
					u.setUnit(unit);
					userService.update(u);

					PropertiesReader.loadProperties("syncRH.properties");
					if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
						String UnitCode = uniteService.load(unit.getId()).getCode();
						String result = userWebService.updateUnit(u.getMatricule(), UnitCode);
					}
				}
				selectedUsers = new ArrayList<User>();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void affectUserNodeAction() {
		try {
			if (this.selectedNode != null && this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
				for (User u : this.selectedUsers) {
					u.setNoeud((Noeud) this.selectedNode.getData());
					userService.update(u);
				}
				selectedUsers = new ArrayList<User>();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addUsersToGroupAction() throws Exception {
		if (this.group != null && this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
			group.getUsers().addAll(this.selectedUsers);
			groupService.update(group);
			for (User u : this.selectedUsers)
				this.criteria.getIndexs().add(u.getId());
			this.criteria.setChanged(true);
			selectedUsers = new ArrayList<User>();
		}
	}

	public void deleteUsersToGroupAction() {
		try {
			if (group != null && this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
				group.getUsers().removeAll(this.selectedUsers);
				groupService.update(group);
				criteria.setIndexs(new ArrayList<Integer>());
				for (User u : group.getUsers())
					criteria.getIndexs().add(u.getId());
				this.criteria.setChanged(true);
				selectedUsers = new ArrayList<User>();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	@Transactional
	public void affectUserHorAction(String planningId) {
		try {
			if (planningId != null)
				planning = planningService.load(Integer.parseInt(planningId));
			else
				return;
			Except e = null;
			if (this.dateDebut != null && this.dateFin != null) {
				e = new ExceptImpl();
				e.setDateDebut(dateDebut);
				e.setDateFin(dateFin);
				e.setPlanning(planning);
				e.setCode("");
			}
			if (this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
				for (User u : this.selectedUsers) {
					if (e != null)
						u.getExceptions().add(e);
					else
						u.setPlanning(planning);
					userService.update(u);
				}
			}
			selectedUsers = new ArrayList<User>();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String onFlowProcess(FlowEvent event) {
		return event.getNewStep();
	}

	public void onNodeSelect(NodeSelectEvent event) {
		this.noeud = (Noeud) event.getTreeNode().getData();
	}

	public void onSelectedUser(SelectEvent event) throws Exception {
		this.user = (User) event.getObject();
		if (this.user == null)
			return;
		LoadImage(user.getMatricule());
		this.user = userService.load(this.user.getId());
		RequestContext.getCurrentInstance().update("userInfoForm");
	}
	
	public void onUserSelected(SelectEvent event) throws Exception {
		this.user = (User) event.getObject();
		if (this.user == null)
			return;

		LoadImage(user.getMatricule());
		this.user = userService.load(this.user.getId());

		RequestContext.getCurrentInstance().update("adduserform");
		if (tabView != null) {
			//tabView.setActiveIndex(1);
			//RequestContext.getCurrentInstance().update("collabotabsId");
			RequestContext.getCurrentInstance().update("badgesform");
			
		}
		if (this.user.getSupH() != null && this.user.getSupH().getId() > 0) {
			this.manager = Utils.deproxy(userService.load(this.user.getSupH().getId()), UserImpl.class);
		} else
			this.manager.setId(0);
		if (this.user.getProfilMetier() != null)
			this.profilMetier = Utils.deproxy(this.user.getProfilMetier(), ProfilMetier.class);
		if (this.user.getProfilApp() != null)
			this.roleId = this.user.getProfilApp().getId();
		if (this.user.getUnit() == null)
			this.user.setUnit(new UnitImpl());
		
		if(this.user.getNoeud()!=null)
			this.noeud=noeudService.load(this.user.getNoeud().getId());
			
		treeNode2 = null;
		badges = badgeService.ReadByColaborateurID(user.getId());
		setOperation(Boolean.valueOf(true));
		badgeNumber = user.getBadge();
		selectedBadge = user.getBadge();
	}
	
	public void editUser(int id) throws Exception {
		showLoginPass = false;
		this.user = userService.load(id);
		if (this.user == null)
			return;
		LoadImage(user.getMatricule());
		
		RequestContext.getCurrentInstance().update("adduserform");
		if (tabView != null) {
			//tabView.setActiveIndex(1);
			//RequestContext.getCurrentInstance().update("collabotabsId");
			RequestContext.getCurrentInstance().update("badgesform");
			
		}
		if (this.user.getSupH() != null && this.user.getSupH().getId() > 0) {
			this.manager = Utils.deproxy(userService.load(this.user.getSupH().getId()), UserImpl.class);
		} else
			this.manager.setId(0);
		if (this.user.getProfilMetier() != null)
			this.profilMetier = Utils.deproxy(this.user.getProfilMetier(), ProfilMetier.class);
		if (this.user.getProfilApp() != null)
			this.roleId = this.user.getProfilApp().getId();
		if (this.user.getUnit() == null)
			this.user.setUnit(new UnitImpl());
		
		if(this.user.getNoeud()!=null)
			this.noeud=noeudService.load(this.user.getNoeud().getId());
			
		treeNode = null;
		badges = badgeService.ReadByColaborateurID(user.getId());
		setOperation(Boolean.valueOf(true));
		badgeNumber = user.getBadge();
		selectedBadge = user.getBadge();
	}
	
	public void intializUserListener(ActionEvent e) {
		this.user = new UserImpl();
	}

	public void selectAllUsersAction() {
		if (this.dataModel != null) {
			List<User> users = dataModel.getData();
			this.selectedUsers = new ArrayList<User>();
			for (User user : users) {
				if (dataModel.isAllSelected()) {
					user.setFlag(user.isleft() ? false : true);
					this.selectedUsers.add(user);
				} else {
					user.setFlag(false);
					this.selectedUsers.remove(user);
				}
			}
		}
	}

	public void selectOneUsersAction(User user) {
		if (user.isFlag())
			this.selectedUsers.add(user);
		else if (this.selectedUsers.contains(user))
			this.selectedUsers.remove(user);
		if (this.group != null && this.selectedUsers != null) {
			boolean inTheGroup = false;
			for (User u : this.selectedUsers)
				if (this.group.getUsers().contains(u)) {
					inTheGroup = true;
					break;
				}
			if (inTheGroup)
				this.inTheGroup = true;
			else
				this.inTheGroup = false;
		}
	}

	public void displaySelectedSingle() {
		if (selectedNode != null)
			noeud = (Noeud) selectedNode.getData();
	}

	public void saveNode() {
		try {
			if (this.noeud.getId() == 0) {
				if (this.selectedNode != null)
					this.noeud.setNoeud((Noeud) this.selectedNode.getData());
				this.noeudService.create(this.noeud);
			} else {
				Noeud n = this.noeudService.load(noeud.getId());
				n.setLibelle(noeud.getLibelle());
				this.noeudService.update(n);
			}
			treeNode = null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void saveGroup() {
		try {
			if (selectedUsers != null && !selectedUsers.isEmpty()) {

				group.setUsers(selectedUsers);
				groupService.create(group);
				group = new GroupImpl();
				this.groups = null;
				selectedUsers = new ArrayList<User>();
				this.dataModel = null;
				this.criteria = new CommonCriteria();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteGroup() {
		try {
			if (group != null && group.getId() != 0) {
				groupService.delete(group);
				this.groups = null;
				this.criteria = new CommonCriteria();
				this.dataModel = null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void groupValueChangeListener(ValueChangeEvent e) throws Exception {
		if (e.getNewValue() == e.getOldValue()) {
			((SelectOneListbox) e.getComponent()).setValue(null);
			RequestContext.getCurrentInstance().update("collabotabsId:groupForm");
			this.criteria = new CommonCriteria();
			this.criteria.setChanged(true);
			return;
		}
		
		this.group = this.groupService.load(Integer.parseInt((String) e.getNewValue()));
		if (this.group != null && this.selectedUsers != null) {
			boolean inTheGroup = false;
			for (User u : this.selectedUsers)
				if (this.group.getUsers().contains(u)) {
					inTheGroup = true;
					break;
				}
			if (inTheGroup)
				this.inTheGroup = true;
			else
				this.inTheGroup = false;
		}
		criteria.setIndexs(new ArrayList<Integer>());
		for (User u : group.getUsers())
			criteria.getIndexs().add(u.getId());
		this.criteria.setChanged(true);
	}

	public void managerValueChangeListener(ValueChangeEvent e) throws Exception {
		selectedManagerId = Integer.valueOf((String) e.getNewValue());

		if(selectedUsers.size()>0 ){
			//User manager = userService.load(manId);
//			for(User user : selectedUsers ){
//				user.setSupH(man);
//				userService.update(user);
//			}
			return;
		}
		if (e.getNewValue() == e.getOldValue()) {
			((SelectOneListbox) e.getComponent()).setValue(null);
			RequestContext.getCurrentInstance().update("collabotabsId:groupForm");
			this.criteria = new CommonCriteria();
			this.criteria.setChanged(true);
			return;
		}
		criteria.setSuppHrhId((String) e.getNewValue());
		this.criteria.setChanged(true);
	}

	public void affectManagerListener(){
		List<User> usersToUpdate = new ArrayList<User>();
		User man = new UserImpl();
		man.setId(selectedManagerId);
		if (selectedUsers.size() > 0 && selectedManagerId>0) {
			try {
				for (User user : selectedUsers) {
					if(manager.getId() != user.getId()) {
						user.setSupH(man);
						usersToUpdate.add(user);
					}
				}
				userService.update(usersToUpdate);
			} catch (Exception e) {
				logger.error("Could no update user", e);
			}
		}
	}
	public void unitValueChangeListener(ValueChangeEvent e) throws Exception {
		System.out.println(((Planning) e.getNewValue()).getLibelle());
	}

	public void setSelectedNode(TreeNode selectedNode) {
		if (selectedNode == null)
			return;
		if (selectedNode.equals(this.selectedNode)) {
			this.selectedNode = null;
			selectedNode.setSelected(false);
			RequestContext.getCurrentInstance().update("collabotabsId:orgmForm:orgmId");
		} else {
			this.selectedNode = selectedNode;
			this.noeud = (Noeud) selectedNode.getData();
		}
	}

	public void resetNodePanel() {
		this.noeud = new NoeudImpl();
		if (this.selectedNode != null)
			this.noeud.setNoeud((Noeud)this.selectedNode.getData());
		else
			this.noeud.setNoeud(new ma.nawarit.checker.compagnie.NoeudImpl());
	}

	//print user report
	public void userReport(String type) throws JRException, ClassNotFoundException, SQLException, IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		String jrxmlFile = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/jasperreports/user-report.jasper");
		Map<String, Object> parameters = new HashMap<String, Object>();


		if (criteria.getNoeudId() != null)
			parameters.put("NOEUD", Integer.valueOf(criteria.getNoeudId()));
		if (criteria.getSuppHrhId() != null)
			parameters.put("MANAGER", Integer.valueOf(criteria.getSuppHrhId()) );
		
		parameters.put("WEBDIR", fc.getExternalContext().getRealPath("/"));

		if (criteria.getGroup() != null && !criteria.getGroup().equals(""))
			parameters.put("GROUP", Integer.valueOf(criteria.getGroup()));

		File file = new File(jrxmlFile);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(file);
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
				databaseOperations.getConnection());
		if (type.equalsIgnoreCase("xls")) {
			 JRXlsExporter exporterXLS = new JRXlsExporter();
			 exporterXLS.setExporterInput( new SimpleExporterInput(jasperPrint));
			 exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(ec.getResponseOutputStream()));
			 SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration(); 
			 configuration.setOnePagePerSheet(false);
			 configuration.setDetectCellType(false);
			 configuration.setCollapseRowSpan(false);
			 exporterXLS.setConfiguration(configuration);
			 exporterXLS.exportReport();
			 ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/user-report.xls")));
			 ec.setResponseHeader("Content-Disposition", "attachment; filename=user-report."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".xls"); 
			 fc.responseComplete();
		} else {
			ec.setResponseContentType(ec.getMimeType(ec.getRealPath("/Pointages.pdf")));
			ec.setResponseHeader("Content-Disposition", "attachment; filename=user-report."+Utils.convertDate2Str(new Date(), "dd-MM-yyyy hhmmss")+".pdf");
			JasperExportManager.exportReportToPdfStream(jasperPrint, ec.getResponseOutputStream());
			fc.responseComplete();
		}
	}
	
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public List<User> getUsers() {
		if (user == null) {
			try {
				this.users = this.userService.readAll();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return users;
	}

	public List<User> getManagers() throws Exception {
		if (managers == null) {
			managers = new ArrayList<User>();
			List<User> l = this.userService.readAll();
			String profilCode;
			for (User manager : l) {
				profilCode = (manager.getProfilApp() != null) ? manager.getProfilApp().getCode() : "";
				if ("ROLE_SUPH".equals(profilCode))
					managers.add(manager);
			}
		}
		return managers;
	}

	public void handleNodeSelection() {
		if (selectedNode != null && selectedNode.getData() != null) {
			Noeud n = (Noeud) selectedNode.getData();
			Map<String, Object> filters = new HashMap<String, Object>();
			filters.put("noeud.libelle", n.getLibelle());
			usersTable.setFilters(filters);
			criteria.setNoeudId(String.valueOf(n.getId()));
		}
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public List<User> getSelectedUsers() {
		return selectedUsers;
	}

	public void setSelectedUsers(List<User> selectedUsers) {
		this.selectedUsers = selectedUsers;
	}

	public boolean isView_dlg() {
		return view_dlg;
	}

	public void setView_dlg(boolean view_dlg) {
		this.view_dlg = view_dlg;
	}

	public boolean isAdd_dlg() {
		return add_dlg;
	}

	public void setAdd_dlg(boolean add_dlg) {
		this.add_dlg = add_dlg;
	}

	public boolean isEdit_dlg() {
		return edit_dlg;
	}

	public void setEdit_dlg(boolean edit_dlg) {
		this.edit_dlg = edit_dlg;
	}

	public User getUser() {
		if (this.users != null && this.users.size() == 1 && this.user == null)
			this.user = this.users.get(0);
		return user;
	}

	public void listenerSelected() {
		// TODO Auto-generated method stub
		System.out.println("go");
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isDroitPaie() {
		return droitPaie;
	}

	public void setDroitPaie(boolean droitPaie) {
		user.setDroitPaie(droitPaie);
		this.droitPaie = droitPaie;
	}

	public boolean isDetail_dlg() {
		return detail_dlg;
	}

	public void setDetail_dlg(boolean detail_dlg) {
		this.detail_dlg = detail_dlg;
	}

	public void changeView(String view) {

		view_dlg = false;
		detail_dlg = false;
		add_dlg = false;
		edit_dlg = false;

		if (view.equals("view"))
			view_dlg = true;
		else if (view.equals("detail"))
			detail_dlg = true;
		else if (view.equals("add"))
			add_dlg = true;
		else if (view.equals("edit"))
			edit_dlg = true;

	}

	public void clear() {

		user = new UserImpl();

	}

	public void resetSearchAction() {
		this.group = null;
		this.selectedNode = null;
		if (this.selectedUsers != null && !this.selectedUsers.isEmpty()) {
			for (User u : this.selectedUsers)
				u.setFlag(false);
			this.selectedUsers.clear();
		}
		Map<String, Object> filters = new HashMap<String, Object>();
		usersTable.setFilters(filters);
		usersTable.clearInitialState();
		usersTable.clearLazyCache();
	}

	public UserDataModel getDataModel() {
		try {
			if (!criteria.isEmpty() && criteria.isChanged()) {
				criteria.setActive(true);
				if (usrAuthenticated != null && "ROLE_SUPH".equals(usrAuthenticated.getProfilApp().getCode())) 
					criteria.setSuppHrhId(String.valueOf(usrAuthenticated.getId()));
				List users = userService.readByCriteria(criteria);
				if(users != null)
					for(Object u : users)
						((UserImpl)u).setProfilMetier(Utils.deproxy(((UserImpl)u).getProfilMetier(), ProfilMetier.class));
				dataModel = new UserDataModel(users);

				if (group != null && !this.selectedUsers.isEmpty())
					for (User u : this.selectedUsers)
						if (dataModel.getData() != null && dataModel.getData().contains(u))
							dataModel.getData().get(dataModel.getData().indexOf(u)).setFlag(true);
			} else if (criteria.isEmpty() && !criteria.isChanged() && dataModel == null) {
				CommonCriteria crit=new CommonCriteria();
				crit.setActive(true);
				if (usrAuthenticated != null && "ROLE_SUPH".equals(usrAuthenticated.getProfilApp().getCode())) 
					crit.setSuppHrhId(String.valueOf(usrAuthenticated.getId()));
				List users = userService.readByCriteria(crit);
				for(Object u : users)
					((UserImpl)u).setProfilMetier(Utils.deproxy(((UserImpl)u).getProfilMetier(), ProfilMetier.class));
				dataModel = new UserDataModel(users);
			}
			criteria.setChanged(false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dataModel;
	}

	public void setDataModel(UserDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public void setNoeudService(NoeudManageableService noeudService) {
		this.noeudService = noeudService;
	}

	public Noeud getNoeud() {
		if (noeud == null)
			noeud = new NoeudImpl();
		return noeud;
	}

	public void setNoeud(Noeud noeud) {
		this.noeud = noeud;
	}

	public Group getGroup() {
		// if (group == null)
		// group = new GroupImpl();
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public List<Group> getGroups() throws Exception {
		if (groups == null || groups.isEmpty()) {
			groups = this.groupService.readAll();
		}
		return groups;
	}

	public void setGroupService(GroupManageableService groupService) {
		this.groupService = groupService;
	}

	public ProfilMetier getFonction() {
		return fonction;
	}

	public void setFonction(ProfilMetier fonction) {
		this.fonction = fonction;
	}

	public List<ProfilMetier> getFonctions() throws Exception {
		if (this.fonctions == null || this.fonctions.isEmpty())
			this.fonctions = fonctionService.readAll();
		return fonctions;
	}

	public void setFonctionService(ProfilMetierManageableService fonctionService) {
		this.fonctionService = fonctionService;
	}

	public Unit getUnite() {
		return unite;
	}

	public void setUnite(Unit unite) {
		this.unite = unite;
	}

	public List<Unit> getUnites() throws Exception {
		if (this.unites == null || this.unites.isEmpty())
			this.unites = uniteService.readAll();
		return unites;
	}
	
	public List<Noeud> getNoeuds() throws Exception {
		if (this.noeuds == null || this.noeuds.isEmpty())
			this.noeuds = noeudService.readAll();
		return noeuds;
	}

	public void setUniteService(UnitManageableService uniteService) {
		this.uniteService = uniteService;
	}

	public List<ProfilApp> getRoles() throws Exception {
		if (this.roles == null || this.roles.isEmpty())
			this.roles = roleService.readAll();
		return roles;
	}

	public void setRoleService(ProfilAppManageableService roleService) {
		this.roleService = roleService;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getRoleId() {
		return roleId;
	}

	public String getRole(String id) throws Exception {
		return roleService.load(Integer.parseInt(id)).getLibelle();
	}

	public String getUnite(String id) throws Exception {
		return uniteService.load(Integer.parseInt(id)).getLibelle();
	}

	public String getFonction(String id) throws Exception {
		return fonctionService.load(Integer.parseInt(id)).getLibelle();
	}

	public String getRowClasses() {
		StringBuffer rowClasses = new StringBuffer();
		if (users != null && !users.isEmpty())
			for (User u : users) {
				rowClasses.append(u.getDateQuit() != null ? "leftRowColor," : ",");
			}
		return rowClasses.toString();
	}

	public List<User> getFiltredUsers() {
		return filtredUsers;
	}

	public void setFiltredUsers(List<User> filtredUsers) {
		this.filtredUsers = filtredUsers;
	}

	public List<Planning> getPlannings() throws Exception {
		if (plannings == null)
			plannings = planningService.readAll();
		return plannings;
	}

	public void setPlannings(List<Planning> plannings) {
		this.plannings = plannings;
	}

	public Planning getPlanning() {
		return planning;
	}

	public void setPlanning(Planning planning) {
		this.planning = planning;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public boolean isResetPassword() {
		return resetPassword;
	}

	public void resetPasswordListener(String resetPassword) {
		if (resetPassword.equals("true"))
			this.resetPassword = true;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public ProfilMetier getProfilMetier() {
		return profilMetier;
	}

	public void setProfilMetier(ProfilMetier profilMetier) {
		this.profilMetier = profilMetier;
	}

	public TabView getTabView() {
		return tabView;
	}

	public void setTabView(TabView tabView) {
		this.tabView = tabView;
	}

	public void setTerminalOrderService(TerminalOrderService terminalOrderService) {
		this.terminalOrderService = terminalOrderService;
	}

	public Date getDate() {
		return date;
	}

	public User getManager() {
		return manager;
	}

	public void setManager(User manager) {
		this.manager = manager;
	}

	public DataTable getUsersTable() {
		return usersTable;
	}

	public void setUsersTable(DataTable usersTable) {
		this.usersTable = usersTable;
	}

	public boolean isInTheGroup() {
		return inTheGroup;
	}

	public BadgeManageableService getBadgeService() {
		return badgeService;
	}

	public void setBadgeService(BadgeManageableService badgeService) {
		this.badgeService = badgeService;
	}

	public String getSelectedBadge() {
		return selectedBadge;
	}

	public void setSelectedBadge(String selectedBadge) {
		this.selectedBadge = selectedBadge;
	}

	public String getBadgeNumber() {
		return badgeNumber;
	}

	public void setBadgeNumber(String badgeNumber) {
		this.badgeNumber = badgeNumber;
	}

	public List<Badge> getBadges() {
		return badges;
	}

	public void setBadges(List<Badge> badges) {
		this.badges = badges;
	}

	public Boolean getOperation() {
		return operation;
	}

	public void setOperation(Boolean operation) {
		this.operation = operation;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public Boolean getIsROLE_RH() {
		return isROLE_RH;
	}

	public void setIsROLE_RH(Boolean isROLE_RH) {
		this.isROLE_RH = isROLE_RH;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRePassword() {
		return rePassword;
	}

	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}

	public String getLogin() {
		if (login == null || login.equals("")) {
			Object obj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(Globals.USER_AUTHENTICATED);
			User currentUser = (User) obj;
			login = currentUser.getLogin();
		}
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public int getSelectedManagerId() {
		return selectedManagerId;
	}

	public void setSelectedManager(int selectedManagerId) {
		this.selectedManagerId = selectedManagerId;
	}

	public Boolean getShowLoginPass() {
		return showLoginPass;
	}

	public void setShowLoginPass(Boolean showLoginPass) {
		this.showLoginPass = showLoginPass;
	}
}
