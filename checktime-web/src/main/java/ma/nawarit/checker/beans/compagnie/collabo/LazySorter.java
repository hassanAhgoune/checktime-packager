package ma.nawarit.checker.beans.compagnie.collabo;


import java.util.Comparator;

import ma.nawarit.checker.compagnie.User;

import org.primefaces.model.SortOrder;

public class LazySorter implements Comparator<User> {

    private String sortField;
   
    private SortOrder sortOrder;
   
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(User User1, User User2) {
        try {
            Object value1 = User.class.getField(this.sortField).get(User1);
            Object value2 = User.class.getField(this.sortField).get(User2);

            int value = ((Comparable)value1).compareTo(value2);
           
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}

