package ma.nawarit.checker.beans.share;

import java.util.Hashtable;
import java.util.List;
import javax.faces.bean.ViewScoped;
import ma.nawarit.checker.compagnie.Lien;
import ma.nawarit.checker.compagnie.ProfilApp;
import ma.nawarit.checker.compagnie.crud.LienManageableServiceBase;

import org.springframework.stereotype.Component;


@Component("roleBean")
@ViewScoped
public class RoleBean {
	
	private LienManageableServiceBase lienService;
	
	
	public String find(String code) throws Exception{
		Hashtable<String , String > properties = new Hashtable<String, String>();
		properties.put("code", code);
		List<Lien> liens =  lienService.read(properties);
		Lien lien = liens.get(0);
		String roles="";
		for (ProfilApp profil : lien.getProfilApps()) {
			
			roles= profil.getCode() + "," + roles;
		}
		return roles;
	}
	
	

}
