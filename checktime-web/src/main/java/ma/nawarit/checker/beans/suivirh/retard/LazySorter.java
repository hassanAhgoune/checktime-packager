package ma.nawarit.checker.beans.suivirh.retard;


import java.util.Comparator;


import org.primefaces.model.SortOrder;

import ma.nawarit.checker.suivi.Retard;

public class LazySorter implements Comparator<Retard> {

    private String sortField;
   
    private SortOrder sortOrder;
   
    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(Retard Retard1, Retard Retard2) {
        try {
            Object value1 = Retard.class.getField(this.sortField).get(Retard1);
            Object value2 = Retard.class.getField(this.sortField).get(Retard2);

            int value = ((Comparable)value1).compareTo(value2);
           
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}

