package ma.nawarit.checker.beans.configuration.parametreConge;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableService;
import ma.nawarit.checker.compagnie.crud.ProfilMetierManageableServiceBase;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.suivi.ParametreConge;
import ma.nawarit.checker.suivi.ParametreCongeImpl;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.TypeCongeImpl;
import ma.nawarit.checker.suivi.crud.ParametreCongeManageableService;
import ma.nawarit.checker.suivi.crud.ParametreCongeManageableServiceBase;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableServiceBase;

import org.primefaces.event.RowEditEvent;

@ManagedBean(name = "parametreCongeBean")
@SessionScoped
public class ParametreCongeBean {

	@ManagedProperty(value = "#{ParametreCongeManageableService}")
	private ParametreCongeManageableService parametresCongeServise = new ParametreCongeManageableServiceBase();

	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeServise = new TypeCongeManageableServiceBase();

	@ManagedProperty(value = "#{ProfilMetierManageableService}")
	private ProfilMetierManageableService profilMetierServise = new ProfilMetierManageableServiceBase();

	private List<ParametreConge> parametresConge = new ArrayList<ParametreConge>();
	private List<TypeConge> typesConge = new ArrayList<TypeConge>();
	private TypeConge typeConge;
	private TypeConge selectedTypeConge;
	private ParametreConge parametreConge;
	private ParametreConge selectedConge;
	private ProfilMetier profilMetier;
	private List<ProfilMetier> profilsMetier = new ArrayList<ProfilMetier>();
	FacesContext context;
	private String payer;
	private String editeProfil;

	public ParametreCongeBean() {

		context = FacesContext.getCurrentInstance();
		selectedConge = new ParametreCongeImpl();
		parametreConge = new ParametreCongeImpl();
		typeConge = new TypeCongeImpl();
		selectedTypeConge = new TypeCongeImpl();
	}

	public void saveTypeConge() throws Exception {
		if (typeConge != null && typeConge.getLibelle() != null) {

			typeCongeServise.create(typeConge);
		}
		typesConge.add(typeConge);
	}

	public void savePrConge() throws Exception {

		if (profilMetier != null) {

			parametreConge.setProfilMetier(profilMetierServise.load(profilMetier.getId()));
		}
		parametresCongeServise.create(parametreConge);
		parametresConge.add(parametreConge);
		parametreConge = new ParametreCongeImpl();
	}

	public ProfilMetier getProfilByLibelle(String libelle) {

		ProfilMetier pm = null;
		try {
			Hashtable<String, String> props = new Hashtable<String, String>();
			props.put("libelle", libelle);
			List<ProfilMetier> prMetier = profilMetierServise.read(props);
			pm = prMetier.get(0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pm;
	}

	public void updateParametreConge(RowEditEvent event) throws Exception {

		parametreConge = ((ParametreConge) event.getObject());
		if (profilMetier != null) {

			parametreConge.setProfilMetier(profilMetierServise.load(profilMetier.getId()));
		}
		parametresCongeServise.update(parametreConge);
	}

	public void updateTypeConge(RowEditEvent event) throws Exception {
		typeConge = ((TypeConge) event.getObject());
		typeCongeServise.update(typeConge);
	}

	public void cleanPrConge() {
		parametreConge = new ParametreCongeImpl();
	}

	public void selectedParametreConge(RowEditEvent event) throws Exception {

		selectedConge = ((ParametreConge) event.getObject());
	}

	public void deleteParametrageConge() throws Exception {
		if (selectedConge != null) {
			parametresCongeServise.delete(selectedConge);
			parametresConge.remove(selectedConge);
			selectedConge = new ParametreCongeImpl();
		}
	}

	public void selectedTypeConge(RowEditEvent event) throws Exception {

		selectedTypeConge = ((TypeConge) event.getObject());
	}

	public void deleteTypeConge() throws Exception {
		if (selectedTypeConge != null) {
			typeCongeServise.delete(selectedTypeConge);
			typesConge.remove(selectedTypeConge);
			selectedTypeConge = new TypeCongeImpl();
		}
	}

	public void cleanTypeConge(RowEditEvent event) {
		typeConge = ((TypeConge) event.getObject());
		typeConge = new TypeCongeImpl();
	}

	public ParametreCongeManageableService getParametresCongeServise() {
		return parametresCongeServise;
	}

	public void setParametresCongeServise(
			ParametreCongeManageableService parametresCongeServise) {
		this.parametresCongeServise = parametresCongeServise;
	}

	public List<ParametreConge> getParametresConge() {
		try {
			if (parametresConge == null || parametresConge.isEmpty()) {
				parametresConge = parametresCongeServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return parametresConge;
	}

	public void setParametresConge(List<ParametreConge> parametresConge) {
		this.parametresConge = parametresConge;
	}

	public ParametreConge getParametreConge() {
		return parametreConge;
	}

	public void setParametreConge(ParametreConge parametreConge) {
		this.parametreConge = parametreConge;
	}

	public FacesContext getContext() {
		return context;
	}

	public void setContext(FacesContext context) {
		this.context = context;
	}

	public ProfilMetier getProfilMetier() {
		return profilMetier;
	}

	public void setProfilMetier(ProfilMetier profilMetier) {
		this.profilMetier = profilMetier;
	}

	public List<ProfilMetier> getProfilsMetier() {
		try {
			if (profilsMetier == null || profilsMetier.isEmpty()) {
				profilsMetier = profilMetierServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return profilsMetier;
	}

	public void setProfilsMetier(List<ProfilMetier> profilsMetier) {
		this.profilsMetier = profilsMetier;
	}

	public ProfilMetierManageableService getProfilMetierServise() {
		return profilMetierServise;
	}

	public void setProfilMetierServise(
			ProfilMetierManageableService profilMetierServise) {
		this.profilMetierServise = profilMetierServise;
	}

	public TypeCongeManageableService getTypeCongeServise() {
		return typeCongeServise;
	}

	public void setTypeCongeServise(TypeCongeManageableService typeCongeServise) {
		this.typeCongeServise = typeCongeServise;
	}

	public List<TypeConge> getTypesConge() {
		try {
			if (typesConge == null || typesConge.isEmpty()) {
				typesConge = typeCongeServise.readAll();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesConge;
	}

	public void setTypesConge(List<TypeConge> typesConge) {
		this.typesConge = typesConge;
	}

	public TypeConge getTypeConge() {
		return typeConge;
	}

	public void setTypeConge(TypeConge typeConge) {
		this.typeConge = typeConge;
	}

	public ParametreConge getSelectedConge() {
		return selectedConge;
	}

	public void setSelectedConge(ParametreConge selectedConge) {
		this.selectedConge = selectedConge;
	}

	public TypeConge getSelectedTypeConge() {
		return selectedTypeConge;
	}

	public void setSelectedTypeConge(TypeConge selectedTypeConge) {
		this.selectedTypeConge = selectedTypeConge;
	}

	public String getPayer() {
		if (selectedTypeConge != null) {

			if (selectedTypeConge.getPaye() == true) {
				payer = "oui";
			} else
				payer = "Non";

		}
		return payer;
	}

	public void setPayer(String payer) {
		this.payer = payer;
	}

	public String getEditeProfil() {
		if (selectedConge != null && selectedConge.getProfilMetier() != null) {
			editeProfil = selectedConge.getProfilMetier().getLibelle();
		}
		return editeProfil;
	}

	public void setEditeProfil(String editeProfil) {
		this.editeProfil = editeProfil;
	}

}