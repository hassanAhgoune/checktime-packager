package ma.nawarit.checker.beans.suivirh.conge;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.naming.Context;
import javax.persistence.PostLoad;
import javax.servlet.http.HttpServletRequest;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.compagnie.crud.UserManageableServiceBase;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningCycl;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.configuration.crud.AdvParamManageableServiceBase;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableServiceBase;
import ma.nawarit.checker.configuration.crud.PlanningManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.jbpm.DecisionHandler;
import ma.nawarit.checker.jbpm.JbpmStatable;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.TypeConge;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableServiceBase;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableServiceBase;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableService;
import ma.nawarit.checker.suivi.crud.TypeCongeManageableServiceBase;
import ma.nawarit.checker.utils.Globals;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;

@ManagedBean(name = "congeBean")
@ViewScoped
public class CongeBean {
	private Logger log = Logger.getLogger(CongeBean.class);

	@ManagedProperty(value = "#{TypeCongeManageableService}")
	private TypeCongeManageableService typeCongeService;

	@ManagedProperty(value = "#{CongeManageableService}")
	private CongeManageableService congeService;
								
	@ManagedProperty(value = "#{AdvParamManageableService}")
	private AdvParamManageableService advParamService;
	
	@ManagedProperty(value = "#{AbsenceManageableService}")
	private AbsenceManageableService absenceService;
	
	@ManagedProperty(value = "#{JrFerieManageableService}")
	private JrFerieManageableService jrFerieService;

	@ManagedProperty(value = "#{UserManageableService}")
	private UserManageableService userService;
	
	@ManagedProperty(value = "#{PlanningManageableService}")
	private PlanningManageableService planningService;

	@ManagedProperty(value = "#{decisionHandler}")
	private DecisionHandler decisionHandler;

	private List<Conge> conges = new ArrayList<Conge>();
	private Conge conge;
	private Conge selectedConge = new CongeImpl();
	private boolean workflowActive;
	private CommonCriteria criteria;
	private String nom;
	private User user = new UserImpl();
	private User selectedUser = new UserImpl();
	private List<User> users;
	private List<User> filtredUsers;
	private TypeConge typeConge;
	private List<TypeConge> typesConge;
	private String unitTemps = "hr";
	private int total;
	private Conge current = new CongeImpl();
	private Conge detailedConge = new CongeImpl();	
	private int seuilConge;
	private List<JrFerie> jrFeries;
	private boolean mailActive;
	
	//demande conge
	private Conge congeDemande;
	private List<Conge> userConges;
	private String workflowUrl;

	public CongeBean() {
		criteria = new CommonCriteria();
		criteria.setProcessActive(true);
		criteria.setChanged(true);
		users = new ArrayList<User>();
		conge = new CongeImpl();
		congeDemande = new CongeImpl();
		userConges =new ArrayList<Conge>();
	}
	
	@PostConstruct
	private void initAllParams() {
    	try {
    		HttpServletRequest origRequest = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
    		workflowUrl="http://"+origRequest.getServerName()+":"+origRequest.getServerPort()+"/checktime-web";
        	this.jrFeries = jrFerieService.readAll();
    		this.seuilConge = new Integer(advParamService.getValueByName("seuil_min_droit_conge"));
    		String result = advParamService.getValueByName("workflow_active");
    		if(result != null && !result.trim().equals(""))
    			this.workflowActive = ("no".equals(result)? false: true);
				
		} catch (Exception e) {
			log.error(e.getMessage());
	    	e.printStackTrace();
		}
	}
	
	public void saveConge() throws Exception {
		if (!isAfterSeuilConge(conge))
			FacesContext.getCurrentInstance().addMessage("user", new FacesMessage(
					FacesMessage.SEVERITY_ERROR, MessageFactory
							.getMessage("Conge.M1", new String[]{user.getMatricule() + "-" + user.getNom(), seuilConge + ""}), null));
		if (typeConge != null) {
			conge.setTypeConge(typeCongeService.load(typeConge.getId()));
		}
		// voir s'il existe une absence identique
		CommonCriteria crit = new CommonCriteria();
		crit.setProfilId(4+"");
		crit = new CommonCriteria();
		crit.setDateDebut(conge.getDateDebut());
		crit.setDateFin(conge.getDateReprise());
		crit.setMatriculeInf(user.getMatricule());
		crit.setMatriculeSup(user.getMatricule());
		List<Conge> cgs = congeService.readByCriteria(crit);
		if (cgs != null && !cgs.isEmpty())
			for (Conge a : cgs) {
				if (a.getStatut().equals(Globals.WORKFLOW_STATUS_ENCOURS)
						|| a.getStatut().equals(Globals.WORKFLOW_STATUS_VALIDATE))
					return;
			}
		conge.setCollaborateur(this.user);
		conge.setNbrJour(getDuree(conge));
		if (workflowActive) {
			int Idprocess = decisionHandler.deployProcessDefinition(null, "process-workflow", false);
			conge.setIdProcess(Idprocess);
			conge.setStatut(Globals.WORKFLOW_STATUS_ENCOURS);
		}
		else {
			conge.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
			conge.setIdProcess(0);
			crit = new CommonCriteria();
			crit.setDateDebut(conge.getDateDebut());
			crit.setDateFin(conge.getDateReprise());
			crit.setMatriculeInf(user.getMatricule());
			crit.setMatriculeSup(user.getMatricule());
			crit.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
			List<Absence> abs = absenceService.readByCriteria(crit);
			Calendar cal = new GregorianCalendar();
			if (abs != null && !abs.isEmpty()) {
				Conge c;
				int j;
				for (int i = 0; i < abs.size(); i++) {
					cal.setTime(abs.get(i).getDateReprise());
					cal.add(Calendar.DAY_OF_MONTH, 1);
					j = i + 1;
					while (j < abs.size() && abs.get(j).getDateDebut().equals(cal.getTime())) {
						cal.setTime(abs.get(j).getDateReprise());
						cal.add(Calendar.DAY_OF_MONTH, 1);
						j++;
					}
					c = new CongeImpl();
					c.setDateDebut(abs.get(i).getDateDebut());
					c.setDateReprise(abs.get(i).getDateReprise());
					c.setTypeConge(typeConge);
					c.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
					c.setCollaborateur(user);
					c.setNbrJour(getDuree(c));
					congeService.create(c);
					for (int k = i; k < j; k++) {
						absenceService.delete(abs.get(k));
					}
				}
			}
		}
		congeService.create(conge);
		managerTakeDecision((CongeImpl)conge);
		crit = new CommonCriteria();
		crit.setMatriculeInf(user.getMatricule());
		crit.setMatriculeSup(user.getMatricule());
		conges = congeService.readByCriteria(crit);
	}
	
	public void managerTakeDecision(CongeImpl cng){
		try {
			mailActive = advParamService.getValueByName("mail_active").equals("yes")? true : false;
			if(decisionHandler.isManagerDecisionReached((JbpmStatable) cng)){
				// To get from screen form
				String comment = "Manager : Ok let's go !";
				
				// Merge mail Template
				User u = cng.getCollaborateur();
				HashMap<String, String> hash = new HashMap<String, String>();
//				hash.put("sender", (u.getSupH().getMail() == null)? "":u.getSupH().getMail());
//				if(workflowActive){
//					User sup = u.getSupH();
//					hash.put("receiver", (sup.getSupH().getMail() == null)? "":sup.getSupH().getMail());
//					hash.put("rhManager", sup.getSupH().getNom());
//				}
//				else
//					hash.put("receiver", (u.getMail() == null)? "":u.getMail());
				hash.put("subject", "Approbation déclaration Absence - (" + u.getNom() + " " + u.getPrenom()+")");
				
//				hash.put("manager", u.getSupH().getNom());
//				hash.put("managerMail", u.getSupH().getMail());
				hash.put("managerComment", comment);
				hash.put("userName", u.getNom() + " " + u.getPrenom());
//				hash.put("userMail", u.getMail());
				hash.put("profil", "profil");
				hash.put("workflowName", "Absence");
				hash.put("workflowUrl", workflowUrl+"/pages/jbpm/jbpm.wit");
				hash.put("workflowDate", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));

				// Take a decision
				decisionHandler.managerTakeDecision(cng.getIdProcess(), workflowActive, comment, hash, mailActive);
				if(!workflowActive){
					cng.setStatut(Globals.WORKFLOW_STATUS_REJECTED);
					congeService.update(cng);
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}					
	}
	
	public void updateCongeStatut(String statut) throws Exception {
		conge.setStatut(statut);
		congeService.update(conge);
	}
	
	private boolean isAfterSeuilConge(Conge c) {
		Calendar cal = new GregorianCalendar();
		if (user.getDateEmb() != null)
			cal.setTime(user.getDateEmb());
		cal.add(Calendar.MONTH, seuilConge);
		return c.getDateDebut().after(cal.getTime());
	}
	
	public void updateConge() throws Exception{
		if(!typeConge.equals(current.getTypeConge())){
			current.setTypeConge(typeCongeService.load(typeConge.getId()));
		}
		congeService.update(current);

		int i = conges.indexOf(current);
		conges.remove(current);
		conges.add(i, current);
	}
	
	public void deleteConge() throws Exception {
		congeService.delete(detailedConge);
		conges.remove(detailedConge);
	}	

	public void chooseUser() {
		selectedUser = new UserImpl();
	}
	
	public void refreshCngListAction(String stat, User user) {
		if (stat.equals("dem"))
			this.criteria.setStatut(Globals.WORKFLOW_STATUS_ENCOURS);
		else if (stat.equals("val"))
			this.criteria.setStatut(Globals.WORKFLOW_STATUS_VALIDATE);
		else if (stat.equals("ref"))
			this.criteria.setStatut(Globals.WORKFLOW_STATUS_REJECTED);
		else if (stat.equals("eff"))
			this.criteria.setStatut(Globals.WORKFLOW_STATUS_ELAPSED);
		if (user !=null) {
			this.criteria.setIndexs(new ArrayList<Integer>());
			this.criteria.getIndexs().add(user.getId());
		}
		this.criteria.setChanged(true);
	}
	
	private void resetSearchActionListener(ActionEvent e) {
		if (!this.criteria.getIndexs().isEmpty()) {
			int userId = this.criteria.getIndexs().get(0);
			this.criteria = new CommonCriteria();
			this.criteria.getIndexs().add(userId);
			this.criteria.setChanged(true);
		}
	}
	
	public void cleanConge(ActionEvent e) {
		this.conge = new CongeImpl();
	}
	
	public void onUserSelected(SelectEvent event) throws Exception {
		this.user = (User)event.getObject();
		if (this.user == null)
			return;
		this.criteria = new CommonCriteria();
		this.criteria.getIndexs().add(this.user.getId());
		this.criteria.setChanged(true);
		//conges=congeService.readByCriteria(criteria);
	}

	public void onRowEdit(RowEditEvent event) {
			conge = (Conge)event.getObject();
			try {
				if (conge != null) {
					conge.setNbrJour(getDuree(conge));
					congeService.update(conge);
					conge.setTypeConge(typeCongeService.load(conge.getTypeConge().getId()));
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}
	
	public double getDuree(Conge conge){
		double cont=0;
		try {
			Day d = null;
			Calendar date1 = new GregorianCalendar ();
			date1.setTime(conge.getDateDebut());
			Calendar date2 = new GregorianCalendar ();
			date2.setTime(conge.getDateReprise());
			cont = 0;
			while(date1.compareTo(date2) <= 0){ 	
				d = new Day();
				d.setDate(date1.getTime());
				PlanningCycl planningCycl;
				Planning plan=planningService.load(Utils.findAffectPlanning(date1.getTime(), userService.load(conge.getCollaborateur().getId())).getId());
				if(plan.getType().equalsIgnoreCase("Cyclique")) {
					planningCycl=(PlanningCycl) planningService.loadCycl(plan.getId());
					d.setPlanning(planningCycl);
				}else {
					d.setPlanning(plan);
				}
			
				if(!isJrRepos(d)) {
					cont++;
				}else {
					System.out.println("is repos");
				}
				date1.add(Calendar.DAY_OF_MONTH, 1);
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return cont;
	}
	
	public boolean isJrRepos(Day day) throws Exception {	
    	if (day.isJrRepos())
			return true;
    	List<JrFerie> jrFeries = jrFerieService.readAll();
    	Object o = Utils.isJrFerie(day.getDate(), jrFeries);
    	if (o != null) {
    		day.setLibelle(((JrFerie)o).getLibelle());
    		day.setNature(o);
    		day.setJrRepos(true);
    		return true;
    	}
		return false;
    }
     
    public void onRowCancel(RowEditEvent event) {
    	conge = (Conge)event.getObject();
    	try {
    		if (conge != null)
    			congeService.delete(conge);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    }

	@SuppressWarnings("unchecked")
	public User readByName(String nom) {

		List<User> list = new ArrayList<User>();
		try {
			if (list == null || list.isEmpty()) {
				Hashtable<String, String> props = new Hashtable<String, String>();
				props.put("nom", nom);
				list = userService.read(props);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (list.isEmpty()) {
			return null;
		} else
			return list.get(0);
	}
	
	//demande conge
	@SuppressWarnings("unchecked")
	public void onDemandeConge() throws Exception {
		FacesContext fc = FacesContext.getCurrentInstance();
		Object obj = fc.getExternalContext().getSessionMap()
				.get(Globals.USER_AUTHENTICATED);
		if (obj != null && (obj instanceof User)) {
			User u = (User)obj;
			user=u;
			CommonCriteria crit=new CommonCriteria();
			crit.setMatriculeInf(u.getMatricule());
			crit.setMatriculeSup(u.getMatricule());
			userConges=congeService.readByCriteria(crit);
		}
	}
	
	//Save demande de conge
	public void saveDemandeConge() throws Exception {
		if (congeDemande.getTypeConge() == null) {
			FacesContext.getCurrentInstance().addMessage("user", new FacesMessage(
					FacesMessage.SEVERITY_ERROR,"Erreur!","Veulliez choisir le motif de congé"));
		}
		if (congeDemande.getNbrJour()<=0) {
			FacesContext.getCurrentInstance().addMessage("user", new FacesMessage(
					FacesMessage.SEVERITY_ERROR,"Erreur!","Veulliez choisir date début et date fin correctement"));
		}
		congeDemande.setCollaborateur(user);
		congeDemande.setStatut(Globals.WORKFLOW_STATUS_ENCOURS);
		congeService.create(congeDemande);
		congeDemande=congeService.load(congeDemande.getId());
		userConges.add(congeDemande);
		congeDemande = new CongeImpl();
	}
	
	public void onDateSelect(SelectEvent event) {
        calculeCongeDuration();
    }
	
	//calcule nbr de jour de conge
	public void calculeCongeDuration() {
		if(congeDemande.getDateDebut()!=null && congeDemande.getDateReprise()!=null) {
			/*Calendar date_debut=Calendar.getInstance();
			Calendar date_fin=Calendar.getInstance();
			
			date_debut.setTime(congeDemande.getDateDebut());
			date_fin.setTime(congeDemande.getDateReprise());
			*/
			//double def=(double)( (congeDemande.getDateReprise().getTime() - congeDemande.getDateDebut().getTime()) / (1000 * 60 * 60 * 24) );
			congeDemande.setNbrJour(getDuree(congeDemande));
		}
	}
	
	//set type conge
	public void typeCongeValueChangeListener(ValueChangeEvent e) throws Exception {
		String id=((String)e.getNewValue());
		congeDemande.setTypeConge(typeCongeService.load(Integer.valueOf(id)));
	}
	
	
	
	@SuppressWarnings("unchecked")
	public List<Conge> getConges() {
		try {
			if (!criteria.isEmpty() && criteria.isChanged())
				conges = congeService.readByCriteria(criteria);
			else if (criteria.isEmpty() && !criteria.isChanged() && conges == null )
				conges = congeService.readByCriteria(new CommonCriteria());
			criteria.setChanged(false);
			total = 0;
			if (conges != null && !conges.isEmpty()) 
				for (Conge c: conges) {
					total += c.getNbrJour();
					c.setCollaborateur(Utils.deproxy(c.getCollaborateur(), User.class));
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conges;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsers() {
		try {
			if (users == null || users.isEmpty())
				users = userService.readByCriteria(new CommonCriteria());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	public List<User> getFiltredUsers() {
		return filtredUsers;
	}

	public void setFiltredUsers(List<User> filtredUsers) {
		this.filtredUsers = filtredUsers;
	}

	@SuppressWarnings("unchecked")
	public List<TypeConge> getTypesConge() {
		try {
			if (typesConge == null || typesConge.isEmpty())
				typesConge = typeCongeService.readAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typesConge;
	}
	
	public void typeValueChangeListener(ValueChangeEvent e) {
		criteria.setMotif((String)e.getNewValue());
    	this.criteria.setChanged(true);
	}
	
	public void statutChangeListener(ValueChangeEvent e) {
		criteria.setStatut((String) e.getNewValue());
    	this.criteria.setChanged(true);
	}

	public TypeCongeManageableService getTypeCongeService() {
		return typeCongeService;
	}

	public void setTypeCongeService(TypeCongeManageableService typeCongeService) {
		this.typeCongeService = typeCongeService;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public TypeConge getTypeConge() {
		return typeConge;
	}

	public void setTypeConge(TypeConge typeConge) {
		this.typeConge = typeConge;
	}

	public void setTypesConge(List<TypeConge> typesConge) {
		this.typesConge = typesConge;
	}

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public void setConges(List<Conge> conges) {
		this.conges = conges;
	}

	public Conge getConge() {
		return conge;
	}

	public void setConge(Conge conge) {
		this.conge = conge;
	}

	public CommonCriteria getCriteria() {
		return criteria;
	}

	public void setCriteria(CommonCriteria criteria) {
		this.criteria = criteria;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public User getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(User selectedUser) {
		this.selectedUser = selectedUser;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public PlanningManageableService getPlanningService() {
		return planningService;
	}

	public void setPlanningService(PlanningManageableService planningService) {
		this.planningService = planningService;
	}

	public Conge getSelectedConge() {
		return selectedConge;
	}

	public void setSelectedConge(Conge selectedConge) {
		this.selectedConge = selectedConge;
	}

	public Conge getCurrent() {
		return current;
	}

	public void setCurrent(Conge current) {
		this.current = current;
	}

	public Conge getDetailedConge() {
		return detailedConge;
	}

	public void setDetailedConge(Conge detailedConge) {
		this.detailedConge = detailedConge;
	}

	public void setJrFerieService(JrFerieManageableService jrFerieService) {
		this.jrFerieService = jrFerieService;
	}

	public String getUnitTemps() {
		return unitTemps;
	}

	public void setUnitTemps(String unitTemps) {
		this.unitTemps = unitTemps;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<Conge> getUserConges() {
		return userConges;
	}

	public void setUserConges(List<Conge> userConges) {
		this.userConges = userConges;
	}

	public Conge getCongeDemande() {
		return congeDemande;
	}

	public void setCongeDemande(Conge congeDemande) {
		this.congeDemande = congeDemande;
	}
	public boolean isMailActive() {
		return mailActive;
	}

	public void setMailActive(boolean mailActive) {
		this.mailActive = mailActive;
	}

	public DecisionHandler getDecisionHandler() {
		return decisionHandler;
	}

	public void setDecisionHandler(DecisionHandler decisionHandler) {
		this.decisionHandler = decisionHandler;
	}

	public AdvParamManageableService getAdvParamService() {
		return advParamService;
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public JrFerieManageableService getJrFerieService() {
		return jrFerieService;
	}
}