package ma.nawarit.checker.beans.suivirh.report;

public class ReportFactory {
	public ReportType createReport(ReportType rptT) {
		if (rptT.getType().equalsIgnoreCase("RPT_GNL")) {	
			return new GeneralReport(rptT);
		} else
			return (GeneralReport)rptT;
	}
}
