/*
Nom du projet : Checktime
Version : 1.0
Author : M.Morabit
Date : Mai 2014
*/
package ma.nawarit.checker.beans.configuration.locaux;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.equipement.Unit;
import ma.nawarit.checker.equipement.UnitImpl;
import ma.nawarit.checker.equipement.crud.UnitManageableService;
import ma.nawarit.checker.utils.Globals;
import ma.nawarit.checker.utils.MessageFactory;
import ma.nawarit.checker.utils.propertiesOperator.PropertiesReader;
import ma.nawarit.checker.utils.syncRH.databaseOperations;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import org.springframework.beans.BeanUtils;
import org.tempuri.SyncAccessGroupSoap;
import org.tempuri.SyncAccessGroupSoapProxy;

@ManagedBean(name = "unitBean")
@ViewScoped
public class UnitBean implements Serializable {

	@ManagedProperty(value = "#{UnitManageableService}")
	private UnitManageableService unitService;

	private FacesContext context = FacesContext.getCurrentInstance();

	private TreeNode treeNode;

	private TreeNode selectedNode;

	private Unit unit = new UnitImpl();

	public String oldUnitName;

	public SyncAccessGroupSoap unitWebService = new SyncAccessGroupSoapProxy();

	public UnitBean() {

	}

	public TreeNode getTreeNode() {
		Unit parent;
		try {
			this.treeNode = new DefaultTreeNode("ROOT", null);
			Hashtable<String, String> hash = new Hashtable<String, String>();
		
		  	parent = (Unit) unitService.read(hash).get(0);
			Unit nda = new UnitImpl();
			BeanUtils.copyProperties(parent, nda);
			this.buildTree(new DefaultTreeNode(nda, this.treeNode));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this.treeNode;
	}

	public void buildTree(TreeNode node) {
		Unit n = (Unit) node.getData();
		if (n.getUnits() != null) {
			TreeNode nodeImpl = null;
			Unit nda;
			for (Unit nd : n.getUnits()) {
				nda = new UnitImpl();
				BeanUtils.copyProperties(nd, nda);
				nodeImpl = new DefaultTreeNode(nda, node);
				this.buildTree(nodeImpl);
			}
		}
	}

	public void updateUnit() {
		if (unit == null)
			return;
		try {
			unitService.update(this.unit);

			PropertiesReader.loadProperties("syncRH.properties");
			if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE"))
				unitWebService.update(oldUnitName, this.unit.getCode());
			unit = new UnitImpl();
		} catch (Exception e) {
			System.err.println("Erreur : paramètre incorrect !");
			e.printStackTrace();
		}

	}

	/**
	 * Verifie l'unicite de l'horaire
	 * 
	 * @param libelle
	 * @return
	 */

	private boolean checkExist(String libelle) {
		try {
			Hashtable<String, String> hash = new Hashtable<String, String>();
			hash.put(Globals.LIBELLE_ATTRIBUTE, libelle);
			List l = this.unitService.read(hash);
			return ((l != null) && (l.size() > 0)) ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void saveUnit() {
		if (unit == null)
			return;
		try {
			if (!checkExist(unit.getLibelle())) {
				Unit newUnit = new UnitImpl();
				newUnit.setCode(unit.getCode());
				newUnit.setLibelle(unit.getLibelle());
				newUnit.setDescription(unit.getDescription());
				newUnit.setUnit(unitService.load(this.unit.getId()));
				newUnit.setId(0);

				this.unit.setUnit(unitService.load(this.unit.getId()));
				this.unit.setId(0);
				unitService.create(newUnit);

				PropertiesReader.loadProperties("syncRH.properties");
				if (PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE")) {
					String code = unitService.load(this.unit.getId()).getCode();
					unitWebService.add(code);
				}
			} else {
				FacesContext.getCurrentInstance().addMessage(Globals.LIBELLE_ATTRIBUTE,
						new FacesMessage(FacesMessage.SEVERITY_ERROR,
								MessageFactory.getMessage(Globals.CHECK_EXIST, this.unit.getLibelle()), null));
			}
			unit = new UnitImpl();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void deleteUnit() {
		try {
			if (this.unit.getId() > 0) {
				Unit u = this.unitService.load(this.unit.getId());
				databaseOperations op = new databaseOperations();
				op.deleteUnit(u.getId());
				unitService.delete(u);
				PropertiesReader.loadProperties("syncRH.properties");
				if(PropertiesReader.getProperty("ACM.WEBSERVICE").equals("ACTIVE"))
					unitWebService.delete(u.getCode());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			context = FacesContext.getCurrentInstance();
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					MessageFactory.getMessage("Unit.M2", unit.getLibelle()), null));
		}
	}

	public TreeNode getRoot() {
		return this.treeNode;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		if (selectedNode != null)
			this.unit = (Unit) selectedNode.getData();
		else
			this.unit = null;
		this.selectedNode = selectedNode;
	}

	public void deleteNode() {
		selectedNode.getChildren().clear();
		selectedNode.getParent().getChildren().remove(selectedNode);
		selectedNode.setParent(null);

		selectedNode = null;
	}

	public void update() {
		oldUnitName = this.unit.getCode();
		System.out.println(oldUnitName);
	}

	public void setUnitService(UnitManageableService unitService) {
		this.unitService = unitService;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}
}
