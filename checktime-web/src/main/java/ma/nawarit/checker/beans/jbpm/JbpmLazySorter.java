package ma.nawarit.checker.beans.jbpm;


import java.util.Comparator;

import ma.nawarit.checker.jbpm.JbpmStatable;

import org.primefaces.model.SortOrder;

public class JbpmLazySorter implements Comparator<JbpmStatable> {

    private String sortField;
   
    private SortOrder sortOrder;
   
    public JbpmLazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    public int compare(JbpmStatable jbpmA, JbpmStatable jbpmB) {
        try {
            Object value1 = JbpmStatable.class.getField(this.sortField).get(jbpmA);
            Object value2 = JbpmStatable.class.getField(this.sortField).get(jbpmA);

            int value = ((Comparable)value1).compareTo(value2);
           
            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        }
        catch(Exception e) {
            throw new RuntimeException();
        }
    }
}

