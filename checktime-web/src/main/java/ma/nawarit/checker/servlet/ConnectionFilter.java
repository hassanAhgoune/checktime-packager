package ma.nawarit.checker.servlet;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.transaction.UserTransaction;

public class ConnectionFilter implements Filter {

	 
	
	 @Resource
	 private UserTransaction utx;
	
	 
	 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	  try {
	   utx.begin();
	   chain.doFilter(request, response);
	   utx.commit();
	  } catch (Exception e) {
	   e.printStackTrace();
	  }
	
	 }
	
	 
	 public void init(FilterConfig arg0) throws ServletException {
	
	 }

	public void destroy() {
		// TODO Auto-generated method stub
		
	}
}
