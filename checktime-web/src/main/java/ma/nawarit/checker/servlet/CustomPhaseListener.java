package ma.nawarit.checker.servlet;

import java.io.IOException;

import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

public class CustomPhaseListener implements PhaseListener{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4456314518992792207L;
	public static final String HOME_PAGE="/web/login.jsf";
	public void afterPhase(PhaseEvent event) {
		FacesContext facesContext = event.getFacesContext();
	    UIViewRoot uiview = facesContext.getViewRoot();
	    if(uiview!=null)
	    	System.out.println("uiview!=null ::" + uiview.getId());
	    else{   
	       try{   
	    	   facesContext.getExternalContext().redirect(HOME_PAGE);   
	           facesContext.responseComplete();   
	       } catch (IOException e){   
	           e.printStackTrace();   
	       }   
	     }
	}
	public void beforePhase(PhaseEvent event) {
		// TODO Auto-generated method stub
		
	}
	public PhaseId getPhaseId() {
		// TODO Auto-generated method stub
		return PhaseId.RESTORE_VIEW;
	}




}
