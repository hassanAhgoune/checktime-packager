package ma.nawarit.checker.servlet;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ma.nawarit.checker.security.license.use.LicenceManagerFactory;
import net.nicholaswilliams.java.licensing.License;
import net.nicholaswilliams.java.licensing.exception.ExpiredLicenseException;
import net.nicholaswilliams.java.licensing.exception.InvalidLicenseException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


/**
 * 
 * When the session destroyed, MySessionListener will do necessary logout operations.
 * Later, at the first request of client, this filter will be fired and redirect
 * the user to the appropriate timeout page if the session is not valid.
 * 
 * @author hturksoy
 * 
 */
public class SessionTimeoutFilter implements Filter {

 private static final Logger logger = Logger.getLogger(SessionTimeoutFilter.class);

 private String timeoutPage = "/login.wit";
 private String path;

 public void init(FilterConfig filterConfig) throws ServletException {
	 path = filterConfig.getServletContext().getRealPath("/");
 }

 public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException,
   ServletException {
	 
  if ((request instanceof HttpServletRequest) && (response instanceof HttpServletResponse)) {
   HttpServletRequest httpServletRequest = (HttpServletRequest) request;
   HttpServletResponse httpServletResponse = (HttpServletResponse) response;
   
   // is session expire control required for this request?
   if (isSessionControlRequiredForThisResource(httpServletRequest)) {
    
    // is session invalid?
    if (isSessionInvalid(httpServletRequest)) {
    	
    	String timeoutUrl = httpServletRequest.getContextPath() + "/" + getTimeoutPage();
    	
    	logger.error("session is invalid! redirecting to timeoutpage : " + timeoutUrl);
     
     httpServletResponse.sendRedirect(timeoutUrl);
     return;
    }
    LicenceManagerFactory factory = new LicenceManagerFactory();
    License license = factory.getManager().getLicense(path + "upload/" + "ctm.li");
	try {
		factory.getManager().validateLicense(license);
	} catch (ExpiredLicenseException e) {
		httpServletResponse.sendRedirect("/checktime-web/denied/access-denied.wit");
		return;
	} catch (InvalidLicenseException e) {
		httpServletResponse.sendRedirect("/checktime-web/denied/access-denied.wit");
		return;
	}
   }
  }
  filterChain.doFilter(request, response);
 }

 /**
  * 
  * session shouldn't be checked for some pages. For example: for timeout page..
  * Since we're redirecting to timeout page from this filter,
  * if we don't disable session control for it, filter will again redirect to it
  * and this will be result with an infinite loop... 
  */
 private boolean isSessionControlRequiredForThisResource(HttpServletRequest httpServletRequest) {
  String requestPath = httpServletRequest.getRequestURI();
  
  boolean controlRequired = !StringUtils.contains(requestPath, getTimeoutPage());
  
  return controlRequired;
 }

 private boolean isSessionInvalid(HttpServletRequest httpServletRequest) {
  boolean sessionInValid = (httpServletRequest.getRequestedSessionId() != null)
    && !httpServletRequest.isRequestedSessionIdValid();
  return sessionInValid;
 }

 public void destroy() {
 }

 public String getTimeoutPage() {
  return timeoutPage;
 }

 public void setTimeoutPage(String timeoutPage) {
  this.timeoutPage = timeoutPage;
 }
 
}