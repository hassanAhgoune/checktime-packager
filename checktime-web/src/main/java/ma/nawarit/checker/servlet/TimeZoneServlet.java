package ma.nawarit.checker.servlet;

import java.util.SimpleTimeZone;
import java.util.TimeZone;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class TimeZoneServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		String defaultTimeZone = config.getInitParameter("timeZone");
        SimpleTimeZone l_simpleTimeZone = new SimpleTimeZone(0,
        													defaultTimeZone,
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0,
                                                             0);
        TimeZone.setDefault(l_simpleTimeZone);

	}


}