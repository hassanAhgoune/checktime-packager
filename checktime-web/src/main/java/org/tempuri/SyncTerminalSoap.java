/**
 * SyncTerminalSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface SyncTerminalSoap extends java.rmi.Remote {
    public java.lang.String add(int id, java.lang.String name, java.lang.String description, java.lang.String unit) throws java.rmi.RemoteException;
    public java.lang.String update(int id, java.lang.String name, java.lang.String description, java.lang.String unit) throws java.rmi.RemoteException;
    public java.lang.String delete(int id) throws java.rmi.RemoteException;
}
