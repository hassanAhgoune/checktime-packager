package org.tempuri;

public class SyncAccessGroupSoapProxy implements org.tempuri.SyncAccessGroupSoap {
  private String _endpoint = null;
  private org.tempuri.SyncAccessGroupSoap syncAccessGroupSoap = null;
  
  public SyncAccessGroupSoapProxy() {
    _initSyncAccessGroupSoapProxy();
  }
  
  public SyncAccessGroupSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSyncAccessGroupSoapProxy();
  }
  
  private void _initSyncAccessGroupSoapProxy() {
    try {
      syncAccessGroupSoap = (new org.tempuri.SyncAccessGroupLocator()).getsyncAccessGroupSoap();
      if (syncAccessGroupSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)syncAccessGroupSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)syncAccessGroupSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (syncAccessGroupSoap != null)
      ((javax.xml.rpc.Stub)syncAccessGroupSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.SyncAccessGroupSoap getSyncAccessGroupSoap() {
    if (syncAccessGroupSoap == null)
      _initSyncAccessGroupSoapProxy();
    return syncAccessGroupSoap;
  }
  
  public java.lang.String add(java.lang.String name) throws java.rmi.RemoteException{
    if (syncAccessGroupSoap == null)
      _initSyncAccessGroupSoapProxy();
    return syncAccessGroupSoap.add(name);
  }
  
  public java.lang.String update(java.lang.String oldName, java.lang.String newName) throws java.rmi.RemoteException{
    if (syncAccessGroupSoap == null)
      _initSyncAccessGroupSoapProxy();
    return syncAccessGroupSoap.update(oldName, newName);
  }
  
  public java.lang.String delete(java.lang.String name) throws java.rmi.RemoteException{
    if (syncAccessGroupSoap == null)
      _initSyncAccessGroupSoapProxy();
    return syncAccessGroupSoap.delete(name);
  }
  
  
}