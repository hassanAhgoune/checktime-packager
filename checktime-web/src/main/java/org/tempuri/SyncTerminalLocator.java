/**
 * SyncTerminalLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public class SyncTerminalLocator extends org.apache.axis.client.Service implements org.tempuri.SyncTerminal {

    public SyncTerminalLocator() {
    }


    public SyncTerminalLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public SyncTerminalLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for syncTerminalSoap
    private java.lang.String syncTerminalSoap_address = "http://localhost/CheckTime-ws/syncTerminal.asmx";

    public java.lang.String getsyncTerminalSoapAddress() {
        return syncTerminalSoap_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String syncTerminalSoapWSDDServiceName = "syncTerminalSoap";

    public java.lang.String getsyncTerminalSoapWSDDServiceName() {
        return syncTerminalSoapWSDDServiceName;
    }

    public void setsyncTerminalSoapWSDDServiceName(java.lang.String name) {
        syncTerminalSoapWSDDServiceName = name;
    }

    public org.tempuri.SyncTerminalSoap getsyncTerminalSoap() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(syncTerminalSoap_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getsyncTerminalSoap(endpoint);
    }

    public org.tempuri.SyncTerminalSoap getsyncTerminalSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            org.tempuri.SyncTerminalSoapStub _stub = new org.tempuri.SyncTerminalSoapStub(portAddress, this);
            _stub.setPortName(getsyncTerminalSoapWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setsyncTerminalSoapEndpointAddress(java.lang.String address) {
        syncTerminalSoap_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (org.tempuri.SyncTerminalSoap.class.isAssignableFrom(serviceEndpointInterface)) {
                org.tempuri.SyncTerminalSoapStub _stub = new org.tempuri.SyncTerminalSoapStub(new java.net.URL(syncTerminalSoap_address), this);
                _stub.setPortName(getsyncTerminalSoapWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("syncTerminalSoap".equals(inputPortName)) {
            return getsyncTerminalSoap();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://tempuri.org/", "syncTerminal");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "syncTerminalSoap"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("syncTerminalSoap".equals(portName)) {
            setsyncTerminalSoapEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
