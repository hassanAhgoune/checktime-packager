package org.tempuri;

public class SyncTerminalSoapProxy implements org.tempuri.SyncTerminalSoap {
  private String _endpoint = null;
  private org.tempuri.SyncTerminalSoap syncTerminalSoap = null;
  
  public SyncTerminalSoapProxy() {
    _initSyncTerminalSoapProxy();
  }
  
  public SyncTerminalSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSyncTerminalSoapProxy();
  }
  
  private void _initSyncTerminalSoapProxy() {
    try {
      syncTerminalSoap = (new org.tempuri.SyncTerminalLocator()).getsyncTerminalSoap();
      if (syncTerminalSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)syncTerminalSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)syncTerminalSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (syncTerminalSoap != null)
      ((javax.xml.rpc.Stub)syncTerminalSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.SyncTerminalSoap getSyncTerminalSoap() {
    if (syncTerminalSoap == null)
      _initSyncTerminalSoapProxy();
    return syncTerminalSoap;
  }
  
  public java.lang.String add(int id, java.lang.String name, java.lang.String description, java.lang.String unit) throws java.rmi.RemoteException{
    if (syncTerminalSoap == null)
      _initSyncTerminalSoapProxy();
    return syncTerminalSoap.add(id, name, description, unit);
  }
  
  public java.lang.String update(int id, java.lang.String name, java.lang.String description, java.lang.String unit) throws java.rmi.RemoteException{
    if (syncTerminalSoap == null)
      _initSyncTerminalSoapProxy();
    return syncTerminalSoap.update(id, name, description, unit);
  }
  
  public java.lang.String delete(int id) throws java.rmi.RemoteException{
    if (syncTerminalSoap == null)
      _initSyncTerminalSoapProxy();
    return syncTerminalSoap.delete(id);
  }
  
  
}