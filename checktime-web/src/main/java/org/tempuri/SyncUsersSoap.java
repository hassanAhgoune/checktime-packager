/**
 * SyncUsersSoap.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface SyncUsersSoap extends java.rmi.Remote {
    public java.lang.String add(java.lang.String matricule, java.lang.String username, int previlege, int tzCode, java.lang.String dep, java.lang.String description, java.util.Calendar dtReg, java.lang.String RFCArd, java.lang.String unit) throws java.rmi.RemoteException;
    public java.lang.String update(java.lang.String matricule, java.lang.String username, int previlege, int tzCode, java.lang.String dep, java.lang.String description, java.util.Calendar dtReg, java.lang.String RFCArd, java.lang.String unit) throws java.rmi.RemoteException;
    public java.lang.String updateUnit(java.lang.String matricule, java.lang.String unit) throws java.rmi.RemoteException;
    public java.lang.String delete(java.lang.String matricule) throws java.rmi.RemoteException;
}
