/**
 * SyncAccessGroup.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package org.tempuri;

public interface SyncAccessGroup extends javax.xml.rpc.Service {
    public java.lang.String getsyncAccessGroupSoapAddress();

    public org.tempuri.SyncAccessGroupSoap getsyncAccessGroupSoap() throws javax.xml.rpc.ServiceException;

    public org.tempuri.SyncAccessGroupSoap getsyncAccessGroupSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
