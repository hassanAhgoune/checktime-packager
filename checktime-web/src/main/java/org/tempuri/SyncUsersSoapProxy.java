package org.tempuri;

public class SyncUsersSoapProxy implements org.tempuri.SyncUsersSoap {
  private String _endpoint = null;
  private org.tempuri.SyncUsersSoap syncUsersSoap = null;
  
  public SyncUsersSoapProxy() {
    _initSyncUsersSoapProxy();
  }
  
  public SyncUsersSoapProxy(String endpoint) {
    _endpoint = endpoint;
    _initSyncUsersSoapProxy();
  }
  
  private void _initSyncUsersSoapProxy() {
    try {
      syncUsersSoap = (new org.tempuri.SyncUsersLocator()).getsyncUsersSoap();
      if (syncUsersSoap != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)syncUsersSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)syncUsersSoap)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (syncUsersSoap != null)
      ((javax.xml.rpc.Stub)syncUsersSoap)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public org.tempuri.SyncUsersSoap getSyncUsersSoap() {
    if (syncUsersSoap == null)
      _initSyncUsersSoapProxy();
    return syncUsersSoap;
  }
  
  public java.lang.String add(java.lang.String matricule, java.lang.String username, int previlege, int tzCode, java.lang.String dep, java.lang.String description, java.util.Calendar dtReg, java.lang.String RFCArd, java.lang.String unit) throws java.rmi.RemoteException{
    if (syncUsersSoap == null)
      _initSyncUsersSoapProxy();
    return syncUsersSoap.add(matricule, username, previlege, tzCode, dep, description, dtReg, RFCArd, unit);
  }
  
  public java.lang.String update(java.lang.String matricule, java.lang.String username, int previlege, int tzCode, java.lang.String dep, java.lang.String description, java.util.Calendar dtReg, java.lang.String RFCArd, java.lang.String unit) throws java.rmi.RemoteException{
    if (syncUsersSoap == null)
      _initSyncUsersSoapProxy();
    return syncUsersSoap.update(matricule, username, previlege, tzCode, dep, description, dtReg, RFCArd, unit);
  }
  
  public java.lang.String updateUnit(java.lang.String matricule, java.lang.String unit) throws java.rmi.RemoteException{
    if (syncUsersSoap == null)
      _initSyncUsersSoapProxy();
    return syncUsersSoap.updateUnit(matricule, unit);
  }
  
  public java.lang.String delete(java.lang.String matricule) throws java.rmi.RemoteException{
    if (syncUsersSoap == null)
      _initSyncUsersSoapProxy();
    return syncUsersSoap.delete(matricule);
  }
  
  
}