package ma.nawarit.checker.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.DynPlaDetail;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.DynPlaDetailManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceEff;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceEffManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;
import ma.nawarit.checker.suivi.crud.CompensationManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.HeurSuppManageableService;
import ma.nawarit.checker.suivi.crud.InterruptionManageableService;
import ma.nawarit.checker.suivi.crud.RecuperationManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;



public class LiveProcessingServiceBaseFM6 implements LiveProcessingService{
	
	private static Logger logger = Logger.getLogger(LiveProcessingServiceBaseFM6.class);
	// Services
	private UserManageableService userService;
	private AnnomalieManageableService annoService;
	private AdvParamManageableService advParamService;
	private TypeAbsenceManageableService typeAbsenceService;
	private MouvementManageableService mvmntService;
	private JrFerieManageableService jrFerieService;
	private DynPlaDetailManageableService dynPlaDetailService;
	
	// Services From ProcessTask
	private AnnotationManageableService annotService;
	private InterruptionManageableService interService;
	private CongeManageableService congeService;
	private AbsenceManageableService absenceService;
	private AbsenceEffManageableService absenceEffService;
	private CompensationManageableService compensationService;
	private RecuperationManageableService recuperationService;
	private RetardManageableService retardService;
	private HeurSuppManageableService heurSuppService;
	
	// Task Executor
	private ThreadPoolTaskExecutor taskExecutor;

	
	// Global Attributes
	private List<Day> days = new ArrayList<Day>();
	private TypeAbsence noJustifyAbsence;
	private List<User> users = null;
	private Collection<JrFerie> jrFeries = null;
	private Collection<Mouvement> mvmnts = null;
	private CommonCriteria userCriteria;
	private Map<String, Annotation> annotMap;

	private final String VALIDATE_STATUT = "validate";
	private final String EFFECTUE_STATUT = "effectue";
	private final String NOJUSTIFY= "noJustify";
	
	private long DT_mvt_prm;
	private int DT_msFocus_prm; 
	private int DT_hsp_prm;
	private boolean hspEnabled_prm;
	private int DT_out_prm;
	private int DT_HNuit_prm;
	private double DT_pause_prm;
	private String Rpt_hnuit_prm;
	private String Ano_TJR;
	private String Ano_TJF;
	private Double Sl_hsup_cond_prm;
	private boolean Calc_Par_Tous_Mvts;
	private boolean processing = false;
	

	List<UserReportData> datas;


	/**
     * Gets the shared instance of this Class
     *
     * @return the shared AnnoTrucker instance.
     */
    public  LiveProcessingServiceBaseFM6 (){	
    	
    }
    
    /**
     * methode initialisation des parametres
     */
    private void initAllParams() {
    	try {
	    	 	
	    	this.jrFeries = jrFerieService.readAll();
	    	Hashtable<String, String> typeAbsProps = new Hashtable<String, String>();
			typeAbsProps.put("code", MessageFactory.getMessage(NOJUSTIFY));
	    	List<TypeAbsence> noJustifyAbsences = typeAbsenceService.read(typeAbsProps);
	    	if (noJustifyAbsences != null && !noJustifyAbsences.isEmpty())
	    		this.noJustifyAbsence = (TypeAbsence)noJustifyAbsences.get(0);
    		this.Rpt_hnuit_prm = advParamService.getValueByName("report_horNuit");
			this.DT_pause_prm = new Double(advParamService.getValueByName("DeltaT_pause"));
			this.Sl_hsup_cond_prm = new Double(advParamService.getValueByName("seuil_hrSupp_cond"));
			this.DT_out_prm = new Integer(advParamService.getValueByName("DeltaT_Out"));
			this.DT_HNuit_prm = new Integer(advParamService.getValueByName("DeltaT_HorNuit"));
			this.DT_hsp_prm = new Integer(advParamService.getValueByName("DeltaT_Hsp"));
			this.hspEnabled_prm = new Boolean(advParamService.getValueByName("hrsupp_enable"));
			this.DT_mvt_prm = new Integer(advParamService.getValueByName("DeltaT_Mvt"))  * 60000;
			this.DT_msFocus_prm = new Integer(advParamService.getValueByName("DeltaT_MasseFocus"))  * 60000;
			this.Ano_TJR = advParamService.getValueByName("anoTrJrRepos");
			this.Ano_TJF = advParamService.getValueByName("anoTrJrFerie");
			this.Calc_Par_Tous_Mvts = "yes".equals(advParamService.getValueByName("Calc_Par_Tous_Mvts"));
			
			this.absencesEffectues.clear();
			this.retardsEffectues.clear();
			this.anomaliesEffectues.clear();
			this.congesEffectues.clear();
		} catch (Exception e) {
			logger.error(e.getMessage());
	    	logger.error(e.getMessage());
	    	e.printStackTrace();
		}
	}
    

    
  
    
    /**
     * on construit la liste des jours à traiter
     */
    private void buildDaysList() {
    	try {
    		Calendar d1 = new GregorianCalendar();
    		Calendar d2 = new GregorianCalendar();
    		d1.setTime(userCriteria.getDateDebut());
	    	d1.set(Calendar.HOUR_OF_DAY, 0);
	    	d1.set(Calendar.MINUTE, 0);
	    	d1.set(Calendar.SECOND, 0);
	    	d1.set(Calendar.MILLISECOND, 0);
	    	
	    	d2.setTime(userCriteria.getDateFin());
	    	d2.set(Calendar.HOUR_OF_DAY, 0);
	    	d2.set(Calendar.MINUTE, 0);
	    	d2.set(Calendar.SECOND, 0);
	    	d2.set(Calendar.MILLISECOND, 0); 
	    	
    		days = new ArrayList<Day>();
    		Day day;
    		while (d1.compareTo(d2) <= 0) {
    			day = new Day(d1.getTime(), Rpt_hnuit_prm, null);
    			days.add(day);
    			d1.add(Calendar.DAY_OF_MONTH, 1);
    		}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getMessage());
		}
    }
    
 
	private boolean checkUserCriteria(){
		try {
			if (this.userCriteria.getDateDebut() == null 
					|| this.userCriteria.getDateFin() == null) // dateDebut et dateFin obligatoires
				return false;
//			String userAuthId = this.userCriteria.getSuppHrhId();
//			this.userCriteria.setSuppHrhId(null);
			this.users = this.userService.readByCriteria(this.userCriteria);
			if ((this.users == null || this.users.isEmpty()))				
				return false;
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			return false;
		}
		
	}
	
	
	private void cleanOldData(){
		try {	
			userCriteria.setStatut(MessageFactory.getMessage(this.EFFECTUE_STATUT));
			annoService.removeCriteria(this.userCriteria);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
	
	private void detectSmartHoraire(){
		try {
			CommonCriteria crit = new CommonCriteria();
			Map<Planning, List<Integer>> indexs;
			Planning pl = null;
			List<Mouvement> mvts;
			List<Day> ds = new ArrayList<Day>(days);
			int n = 0;
			DynPlaDetail dynPlaDetail;
			for (Day d: ds) {
				crit.setDateDebut(d.getDate());
				crit.setDateFin(d.getDate());
				indexs = new HashMap<Planning, List<Integer>>();
				for (User user: users) {
					pl = Utils.findAffectPlanning(d.getDate(), user);
					pl = Utils.deproxy(pl, Planning.class);
					if (pl == null || !Utils.inCyclicPlg(d.getDate(), pl))
						continue;
					d.setPlanning(pl);
					if (d.isEnMasse()) {
						if (indexs.containsKey(pl))
							indexs.get(pl).add(user.getId());
						else {
							indexs.put(pl, new ArrayList(user.getId()));
							indexs.get(pl).add(user.getId());
						}
					}
					d.cleanDay();
				}
				Set<Planning> keys = indexs.keySet();
				for (Planning pla: keys) {
					mvmnts = new ArrayList<Mouvement>();
					List<Integer> inds =indexs.get(pla);
					int eff = 0;
					for (int i: inds) {	
						crit.getIndexs().clear();	
						crit.getIndexs().add(i);
						mvts = mvmntService.readByCriteria(crit);
						if (mvts != null && !mvts.isEmpty())
							Utils.filterMvtsByDelta(mvts, DT_mvt_prm);
						else
							continue;
						mvmnts.addAll(mvts);
						eff++;
					}		
					if (!mvmnts.isEmpty()) {
						List<Date> inOut = Utils.getInOuts((List)mvmnts, DT_msFocus_prm, eff);
						if (!inOut.isEmpty()){ 
							days.get(n).getSmartHor().put(pla, inOut);
							//########### ?????? TEST AJOUTE PAR KHALID
							//d.setPlanning(pla);
							// clean Dyn_Pla_Detail
						}
					}
				}
				n++;
			}
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
	
	private void loadValideTimeFactors(LiveProcessTaskFM6 processTask) {
		try {
			userCriteria.setStatut(MessageFactory
					.getMessage(this.VALIDATE_STATUT));
			if (this.hspEnabled_prm)
				processTask.setHrSuppsValidates(heurSuppService.readByCriteria(userCriteria));
			processTask.setCongesValidates(congeService.readByCriteria(userCriteria));
			processTask.setAbsencesValidates(absenceService.readByCriteria(userCriteria));
			processTask.setRecupsValidates(recuperationService.readByCriteria(userCriteria));
			processTask.setCompValidates(compensationService.readByCriteria(userCriteria));
			processTask.setAbsRecs(recuperationService.readIfAbsIn(userCriteria));
		} catch (Exception e) {
			logger.error("> EXCEPTION - GenericException : " + e.getMessage());
		}
	}
	
	private void loadEffectTimeFactors(LiveProcessTaskFM6 processTask) {
		try {
			userCriteria.setStatut(MessageFactory
					.getMessage(this.EFFECTUE_STATUT));
			processTask.setType("reporting");
			processTask.setAbsencesEffectues(absenceService.readByCriteria(userCriteria));
			processTask.setCongesEffectues(congeService.readByCriteria(userCriteria));
			processTask.setRetardsEffectues(retardService.readByCriteria(userCriteria));
			if (this.hspEnabled_prm)
				processTask.setHrSuppsEffectues(heurSuppService.readByCriteria(userCriteria));
			processTask.setInterruptionsEffectues(interService.readByCriteria(userCriteria));
		} catch (Exception e) {
			logger.error("> EXCEPTION - GenericException : " + e.getMessage());
		}
	}
	
	private void loadAllAnnots() {
		try {
			List<Annotation> annos = annotService.readAll();
			annotMap = new HashMap<String, Annotation>();
			for(Annotation a: annos) 
				annotMap.put(a.getCode(), a);
		} catch (Exception e) {
			logger.error("> EXCEPTION - GenericException : " + e.getMessage());
		}
	}
	
	private Collection<Absence> absencesEffectues = new ArrayList<Absence>();
	private Collection<Retard> retardsEffectues = new ArrayList<Retard>();
	private Collection<Interruption> interruptionsEffectues = new ArrayList<Interruption>();
	private Collection<HeurSupp> hrSuppsEffectues = new ArrayList<HeurSupp>();
	private Collection<AbsenceEff> absenceEffsEffectues = new ArrayList<AbsenceEff>();
	private Collection<Conge> congesEffectues = new ArrayList<Conge>();
	private Collection<Annomalie> anomaliesEffectues = new ArrayList<Annomalie>();

	private void saveAllReseltsOfTask() {
		try {
			if (!absencesEffectues.isEmpty())
				absenceService.create(absencesEffectues);
			if (!absenceEffsEffectues.isEmpty())
				absenceEffService.create(absenceEffsEffectues);
			if (!retardsEffectues.isEmpty())
				retardService.create(retardsEffectues);
			if (!congesEffectues.isEmpty())
				congeService.create(congesEffectues);
			if (!interruptionsEffectues.isEmpty())
				interService.create(interruptionsEffectues);
			if (!anomaliesEffectues.isEmpty())
				annoService.create(anomaliesEffectues);
			// if (!hrSuppsEffectues.isEmpty())
			// 	heurSuppService.create(hrSuppsEffectues);
//			recuperationService.create(processTask.getre)
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	private void rassembleAllReseltsOfTask(LiveProcessTaskFM6 processTask) {
		try {
			if (!processTask.getAbsencesEffectues().isEmpty())
				absencesEffectues.addAll(processTask.getAbsencesEffectues());
			if (!processTask.getAbsenceEffsEffectues().isEmpty())
				absenceEffsEffectues.addAll(processTask.getAbsenceEffsEffectues());
			if (!processTask.getRetardsEffectues().isEmpty())
				retardsEffectues.addAll(processTask.getRetardsEffectues());
			if (!processTask.getCongesEffectues().isEmpty())
				congesEffectues.addAll(processTask.getCongesEffectues());
			if (!processTask.getInterruptionsEffectues().isEmpty())
				interruptionsEffectues.addAll(processTask.getInterruptionsEffectues());
			if (!processTask.getAnomaliesEffectues().isEmpty())
				anomaliesEffectues.addAll(processTask.getAnomaliesEffectues());
			// if (!processTask.getHrSuppsEffectues().isEmpty())
			// 	hrSuppsEffectues.addAll(processTask.getHrSuppsEffectues());
//			recuperationService.create(processTask.getre)
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	private void processAllUsers(){
		try {
			LiveProcessTaskFM6 processTaskLocal;
			List<Day> daysLocal;
			User userLocal;
			int c = 0;
			int i = 0;
			Noeud noeud = null;
			List<LiveProcessTaskFM6> tasks = new ArrayList<LiveProcessTaskFM6>();
			
			for (User user: users) {
				userLocal = new UserImpl();
				processTaskLocal = new LiveProcessTaskFM6();
				tasks.add(processTaskLocal);
				daysLocal = new ArrayList<Day>(this.days);
				//getting parent noeud before last
//				noeud = user.getNoeud();
//				i=0;
//				while(noeud.getNoeud() != null) {
//					//15 because we don't want reccursive .getNeoud or an infinite loop
//					if(noeud.getNoeud().getNoeud() != null && i<15) {
//						noeud = noeud.getNoeud();
//						
//					}else break;
//					i++;
//				}
//				user.setNoeud(noeud);
				
				userLocal = user;
				this.userCriteria.getIndexs().clear();
				this.userCriteria.getIndexs().add(user.getId());
				processTaskLocal.setUser(userLocal);
				processTaskLocal.setDays(daysLocal);
				
				if (processing) {
					processTaskLocal.setType("processing");
					loadValideTimeFactors(processTaskLocal);
				} else if (userCriteria.getTypeTreatment().equals("RPT")){
					processTaskLocal.setType("reporting");
					loadEffectTimeFactors(processTaskLocal);
				}
				if (userCriteria.getTypeTreatment().equals("MGR")){
					processTaskLocal.setType("mgrReporting");
				} else {
					processTaskLocal.setAnnotations(annotMap);
					
					processTaskLocal.setJrFeries(this.jrFeries);
					processTaskLocal.setNoJustifyAbsence(this.noJustifyAbsence);
					
					processTaskLocal.setAno_TJF(this.Ano_TJF);
					processTaskLocal.setAno_TJR(this.Ano_TJR);
					processTaskLocal.setDT_HNuit_prm(this.DT_HNuit_prm);
					processTaskLocal.setDT_hsp_prm(this.DT_hsp_prm);
					processTaskLocal.setHspEnabled_prm(this.hspEnabled_prm);
					processTaskLocal.setDT_pause_prm(this.DT_pause_prm);
					processTaskLocal.setRpt_hnuit_prm(this.Rpt_hnuit_prm);
					processTaskLocal.setSl_hsup_cond_prm(this.Sl_hsup_cond_prm);
					
					processTaskLocal.setAnnoService(annoService);
				}
				processTaskLocal.setDT_mvt_prm(this.DT_mvt_prm);
				processTaskLocal.setCalc_Par_Tous_Mvts(this.Calc_Par_Tous_Mvts);
				processTaskLocal.setDT_out_prm(this.DT_out_prm);
				processTaskLocal.setMvmntService(mvmntService);
				taskExecutor.setBeanName("taskExecutor("+c+") -> " + userLocal.getNom() + " " + userLocal.getPrenom());
				taskExecutor.execute(processTaskLocal);	 
				c++;
			}
			
			int count = taskExecutor.getActiveCount();
			logger.error("Start Active Threads : " + count);
			for (;;) {
				count = taskExecutor.getActiveCount();
				if (count == 0) {     
					logger.error("No Active threads !");
					Thread.sleep(1000);
					break;
				}
			}
			Thread.sleep(5000);
//			taskExecutor.shutdown();
			logger.error("Save all results of treatment !");
			UserReportData data;
			datas = new ArrayList<UserReportData>(users.size());
			for (LiveProcessTaskFM6 task: tasks) {
				if (processing) {
					rassembleAllReseltsOfTask(task);
					saveAllReseltsOfTask();
					
				}
//				calculTotals(task);
				data = (UserReportData)task;
				task = null;
				datas.add(data);
			}
			tasks = null;
			logger.error("Processiong finished");
		
		}catch (InterruptedException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
        } catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	public void calculTotals(LiveProcessTaskFM6 task) {
		if (task.getDayDatas() == null || task.getDayDatas().isEmpty())
			return;
		try {
			task.resetTotals();
			for(DayData d: task.getDayDatas()) {			
				task.setTcol2ht(Utils.additionHeures(task.getTcol2ht(), d.getCol2ht()));
				if (userCriteria.getTypeTreatment().equals("MGR"))
					continue;
				task.setTcol1(Utils.additionHeures(task.getTcol1(), d.getCol1()));
				task.setTcol2(Utils.additionHeures(task.getTcol2(), d.getCol2()));
				task.setTcol2cop(Utils.additionHeures(task.getTcol2cop(), d.getCol2cop()));				
				task.setTcol2bis(task.getTcol2bis() + d.getCol2bis());
				task.getTcol2prm()[0] += d.getCol2prm()[0];
				task.getTcol2prm()[1] += d.getCol2prm()[1];
				task.getTcol2prm()[2] += d.getCol2prm()[2];
				task.setTcol3(Utils.additionHeures(task.getTcol3(), d.getCol3()));
				task.setTcol4(Utils.additionHeures(task.getTcol4(), d.getCol4()));
				task.setTcol5(Utils.additionHeures(task.getTcol5(), d.getCol5()));
				task.setTcol6(Utils.additionHeures(task.getTcol6(), d.getCol6()));
				task.setTcol7(Utils.additionHeures(task.getTcol7(), d.getTcol7()));
				task.setTcol8(Utils.additionHeures(task.getTcol8(), d.getTcol8()));
				task.setTcol9(Utils.additionHeures(task.getTcol9(), d.getTcol9()));
				task.setTcol10(Utils.additionHeures(task.getTcol10(), d.getCol10()));
//				task.setTcol10bis(Utils.additionHeures(task.getTcol10bis(), d.getCol10bis()));
				task.setTcol11(Utils.additionHeures(task.getTcol11(), d.getCol11()));
				task.setTcol12(Utils.additionHeures(task.getTcol12(), d.getCol12()));
				task.setTcol13(Utils.additionHeures(task.getTcol13(), d.getCol13()));	
				task.setTcol14(Utils.additionHeures(task.getTcol14(), d.getCol14()));
				task.setTcol15(Utils.additionHeures(task.getTcol15(), d.getCol15()));
				task.setTcol16(Utils.additionHeures(task.getTcol16(), d.getCol16()));
				task.setTcol17(Utils.additionHeures(task.getTcol17(), d.getCol17()));
				task.setTcol18(Utils.additionHeures(task.getTcol18(), d.getCol18()));
				task.setTcol19(Utils.additionHeures(task.getTcol19(), d.getCol19()));
				task.setTcol20(Utils.additionHeures(task.getTcol20(), d.getCol20()));
				
			}
//			task.setTcol7bis(tHrConge);
//			task.setTcol21(tJrChome);
//			task.setTcol21bis(tHrChome);
//			if (paramVtHt.equals("tp") && task.getTcol2() < task.getTcol1()) {
//				double[] hSups = {task.getTcol10(), task.getTcol11(), task.getTcol12(), task.getTcol18(), task.getTcol19(), task.getTcol20(),task.getTcol14(), task.getTcol15(), task.getTcol16()};
//				int i = 0;
//				int k = 0;
//				while (task.getTcol2() < task.getTcol1() && i < hSups.length) {
//					if (i+1 - k == 1)
//						task.setTcol2(Utils.additionHeures(task.getTcol2(), Utils.convertMin2Hour(Utils.convertHour2Minutes(hSups[i]) * TAUX1/100)));
//					else if (i+1 - k == 2)
//						task.setTcol2(Utils.additionHeures(task.getTcol2(), Utils.convertMin2Hour(Utils.convertHour2Minutes(hSups[i]) * TAUX2/100)));
//					else if (i+1 - k == 3)
//						task.setTcol2(Utils.additionHeures(task.getTcol2(), Utils.convertMin2Hour(Utils.convertHour2Minutes(hSups[i]) * TAUX3/100)));
//					if (i+1%3 == 0)
//						k = i+1;
//					i++;
//				}
//				if (task.getTcol2() > task.getTcol1()) {
//					hSups[i-1] = Utils.soustraireDeuxHrs(task.getTcol2(), task.getTcol1());
//					task.setTcol2(task.getTcol1());
//				}
//			}
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	
	private void setGlobalVars(CommonCriteria criteria){
		this.userCriteria = new CommonCriteria();
		//BeanUtils.copyProperties(criteria, this.userCriteria,CommonCriteria.class);
		this.userCriteria = criteria;
		if (criteria.getTypeTreatment().equals("RECL_RPT") || criteria.getTypeTreatment().equals("RECL"))
			this.processing = true;
		else
			this.processing = false;
		logger.error("Period to analyse : ");
		logger.error("from : ["+Utils.toStrDate(userCriteria.getDateDebut(), "dd/MM/yyyy")+"]");
		logger.error("to : ["+Utils.toStrDate(userCriteria.getDateFin(), "dd/MM/yyyy")+"]");
	}
    
	public List<UserReportData> process(CommonCriteria criteria, Planning pl){
    	try {
    		
    		// Set global vars
    		logger.error("0 - Set globals variables ...");
    		setGlobalVars(criteria);
    		
			// Check UserCriteria
			logger.error("1 - Check UserCriteria ...");
			if(!checkUserCriteria()) 
				return datas;
			
			
			// Init all params
			logger.error("2 - Init All params ...");
			if (!userCriteria.getTypeTreatment().equals("MGR"))
				initAllParams();
			
			// Load all annotations
			logger.error("2 - Load all annotations ...");
			if (!userCriteria.getTypeTreatment().equals("MGR"))
				loadAllAnnots();
			
			// Clean Old Anomalies
			if (!userCriteria.getTypeTreatment().equals("MGR") && processing) {
				logger.error("3 - Clean Old Data ...");
				cleanOldData();
			} 
			
			 // Build day list
			logger.error("4 - Build day List ...");
			buildDaysList();			

			// Process all user
			logger.error("6 - Process All users ...");
			processAllUsers();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getMessage());
			
		}
    	return datas;
    }

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void setAnnoService(AnnomalieManageableService annoService) {
		this.annoService = annoService;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public void setTypeAbsenceService(
			TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public void setMvmntService(MouvementManageableService mvmntService) {
		this.mvmntService = mvmntService;
	}

	public void setJrFerieService(JrFerieManageableService jrFerieService) {
		this.jrFerieService = jrFerieService;
	}

	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}


	public void setDynPlaDetailService(
			DynPlaDetailManageableService dynPlaDetailService) {
		this.dynPlaDetailService = dynPlaDetailService;
	}

	public void setAnnotService(AnnotationManageableService annotService) {
		this.annotService = annotService;
	}

	public void setInterService(InterruptionManageableService interService) {
		this.interService = interService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public void setAbsenceEffService(AbsenceEffManageableService absenceEffService) {
		this.absenceEffService = absenceEffService;
	}

	public void setCompensationService(
			CompensationManageableService compensationService) {
		this.compensationService = compensationService;
	}

	public void setRecuperationService(
			RecuperationManageableService recuperationService) {
		this.recuperationService = recuperationService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public void setHeurSuppService(HeurSuppManageableService heurSuppService) {
		this.heurSuppService = heurSuppService;
	}
}
