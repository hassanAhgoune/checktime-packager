package ma.nawarit.checker.engine;

import java.io.Serializable;
import java.util.List;

import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.compagnie.User;

public class UserReportData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 0L;
	private boolean displayTable = false;
	private List<DayData> dayDatas;
	private List<Field>	fields;
	private User user;
	private double tcol1; // HN
	private double tcol2; // HNT
	private double tcol2cop; // HN copag
	private double tcol2ht; // HT (entre mvts)
	private double tcol2bis; // JT
	private Integer[] tcol2prm = {0,0,0}; // prime
	private double tcol3; // R Tol
	private double tcol4; // R NTol
	private double tcol5; // Inter
	private double tcol6; // Sortie avant
	
	private double tcol7; // nbre jours Conge
	private double tcol7bis; // nbre heures Conge
	private double tcol8; // Abs P
	private double tcol9; // Abs NP
	private double tcol10; // HS 125
	private double tcol10bis; // HS 100
	private double tcol11; // HS 150
	private double tcol12; // HS 200
	private double tcol13;
	private double tcol14;
	private double tcol15;
	private double tcol16;
	private double tcol17;
	private double tcol18;
	private double tcol19;
	private double tcol20;
	private double tcol21; // nbre jrs chomes payes
	private double tcol21bis; // nbre hrs chomes payes
//	public String getRowClasses(){
//		StringBuffer rowClasses = new StringBuffer();
//		if (datas != null && !datas.isEmpty())
//			for(DayData d: datas) {
//				rowClasses.append(d.getType().equalsIgnoreCase("total")? "totalRowColor,":(
//						(d.getHor() != null && d.getHor().getType().equalsIgnoreCase("Repos"))? "reposRowColor,":(
//								(d.getHor() != null && d.getHor().getType().equalsIgnoreCase("ferie"))? "ferieRowColor,":(
//										(d.getMvts() == null || d.getMvts().isEmpty())?"emptyRowColor,":","))));
//			}
//		return rowClasses.toString();
//	}
	
	public void resetTotals(){
		tcol1 = 0; // HN
		tcol2 = 0; // HNT
		tcol2cop = 0; // HN copag
		tcol2ht = 0; // HT (entre mvts)
		tcol2bis = 0; // JT
		tcol3 = 0; // R Tol
		tcol4 = 0; // R NTol
		tcol5 = 0; // Inter
		tcol6 = 0; // Sortie avant
		
		tcol7 = 0; // nbre jours Conge
		tcol7bis = 0; // nbre heures Conge
		tcol8 = 0; // Abs P
		tcol9 = 0; // Abs NP
		tcol10 = 0; // HS 125
		tcol10bis = 0; // HS 100
		tcol11 = 0; // HS 150
		tcol12 = 0; // HS 200
		tcol13 = 0;
		tcol14 = 0;
		tcol15 = 0;
		tcol16 = 0;
		tcol17 = 0;
		tcol18 = 0;
		tcol19 = 0;
		tcol20 = 0;
		tcol21 = 0; // nbre jrs chomes payes
		tcol21bis = 0; // nbre hrs chomes payes
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<DayData> getDayDatas() {
		return dayDatas;
	}
	public void setDayDatas(List<DayData> dayDatas) {
		this.dayDatas = dayDatas;
	}
	public double getTcol1() {
		return tcol1;
	}
	public void setTcol1(double tcol1) {
		this.tcol1 = tcol1;
	}
	public double getTcol2ht() {
		return tcol2ht;
	}
	public void setTcol2ht(double tcol2ht) {
		this.tcol2ht = tcol2ht;
	}
	public double getTcol2cop() {
		return tcol2cop;
	}
	public void setTcol2cop(double tcol2cop) {
		this.tcol2cop = tcol2cop;
	}
	public double getTcol2() {
		return tcol2;
	}
	public void setTcol2(double tcol2) {
		this.tcol2 = tcol2;
	}
	public double getTcol3() {
		return tcol3;
	}
	public void setTcol3(double tcol3) {
		this.tcol3 = tcol3;
	}
	public double getTcol4() {
		return tcol4;
	}
	public void setTcol4(double tcol4) {
		this.tcol4 = tcol4;
	}
	public double getTcol5() {
		return tcol5;
	}
	public void setTcol5(double tcol5) {
		this.tcol5 = tcol5;
	}
	public double getTcol6() {
		return tcol6;
	}
	public void setTcol6(double tcol6) {
		this.tcol6 = tcol6;
	}
	public double getTcol7() {
		return tcol7;
	}
	public void setTcol7(double tcol7) {
		this.tcol7 = tcol7;
	}
	public double getTcol8() {
		return tcol8;
	}
	public void setTcol8(double tcol8) {
		this.tcol8 = tcol8;
	}
	public double getTcol9() {
		return tcol9;
	}
	public void setTcol9(double tcol9) {
		this.tcol9 = tcol9;
	}
	public double getTcol10() {
		return tcol10;
	}
	public void setTcol10(double tcol10) {
		this.tcol10 = tcol10;
	}
	
	public double getTcol10bis() {
		return tcol10bis;
	}

	public void setTcol10bis(double tcol10bis) {
		this.tcol10bis = tcol10bis;
	}

	public double getTcol11() {
		return tcol11;
	}
	public void setTcol11(double tcol11) {
		this.tcol11 = tcol11;
	}
	public double getTcol12() {
		return tcol12;
	}
	public void setTcol12(double tcol12) {
		this.tcol12 = tcol12;
	}
	public double getTcol13() {
		return tcol13;
	}
	public void setTcol13(double tcol13) {
		this.tcol13 = tcol13;
	}

	public double getTcol14() {
		return tcol14;
	}

	public void setTcol14(double tcol14) {
		this.tcol14 = tcol14;
	}

	public double getTcol15() {
		return tcol15;
	}

	public void setTcol15(double tcol15) {
		this.tcol15 = tcol15;
	}

	public double getTcol16() {
		return tcol16;
	}

	public void setTcol16(double tcol16) {
		this.tcol16 = tcol16;
	}

	public double getTcol17() {
		return tcol17;
	}

	public void setTcol17(double tcol17) {
		this.tcol17 = tcol17;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public boolean isDisplayTable() {
		return displayTable;
	}

	public void setDisplayTable(boolean displayTable) {
		this.displayTable = displayTable;
	}

	public double getTcol2bis() {
		return tcol2bis;
	}

	public void setTcol2bis(double tcol2bis) {
		this.tcol2bis = tcol2bis;
	}

	public Integer[] getTcol2prm() {
		return tcol2prm;
	}

	public void setTcol2prm(Integer[] tcol2prm) {
		this.tcol2prm = tcol2prm;
	}

	public double getTcol18() {
		return tcol18;
	}

	public void setTcol18(double tcol18) {
		this.tcol18 = tcol18;
	}

	public double getTcol19() {
		return tcol19;
	}

	public void setTcol19(double tcol19) {
		this.tcol19 = tcol19;
	}

	public double getTcol20() {
		return tcol20;
	}

	public void setTcol20(double tcol20) {
		this.tcol20 = tcol20;
	}
	
	public double getTcol7bis() {
		return tcol7bis;
	}

	public void setTcol7bis(double tcol7bis) {
		this.tcol7bis = tcol7bis;
	}

	public double getTcol21() {
		return tcol21;
	}

	public void setTcol21(double tcol21) {
		this.tcol21 = tcol21;
	}

	public double getTcol21bis() {
		return tcol21bis;
	}

	public void setTcol21bis(double tcol21bis) {
		this.tcol21bis = tcol21bis;
	}
}
