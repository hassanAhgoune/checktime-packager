package ma.nawarit.checker.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.DayData;
import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.core.common.Plage;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceEff;
import ma.nawarit.checker.suivi.AbsenceEffImpl;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.AnnomalieImpl;
import ma.nawarit.checker.suivi.Compensation;
import ma.nawarit.checker.suivi.CompensationImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.HeurSuppImpl;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.InterruptionImpl;
import ma.nawarit.checker.suivi.Recuperation;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.RetardImpl;
import ma.nawarit.checker.suivi.Timing;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;

import org.andromda.spring.CommonCriteria;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;

public class LiveProcessTask extends UserReportData implements Runnable {

	private static final Logger logger = Logger.getLogger(LiveProcessTask.class);
	
	private String type;
	// Init global
	private Collection<JrFerie> jrFeries = null;
	private List<Day> days = new ArrayList<Day>();
	private TypeAbsence noJustifyAbsence;
	private long DT_mvt_prm;
	private int DT_hsp_prm;
	private int DT_HNuit_prm;
	private double DT_pause_prm;
	private String Rpt_hnuit_prm;
	private String Ano_TJR;
	private String Ano_TJF;
	private Double Sl_hsup_cond_prm;

	// Attributs per each User
	private Collection<Conge> congesValidates = null;
	private Collection<Absence> absencesValidates = null;
	private Collection<Recuperation> recupsValidates = null;
	private Collection<Compensation> compValidates = null;
	private List<Recuperation> absRecs = null;
	private Collection<HeurSupp> hrSuppsValidates = null;
	
	private Collection<Absence> absencesEffectues = new ArrayList<Absence>();
	private Collection<Retard> retardsEffectues = new ArrayList<Retard>();
	private Collection<Interruption> interruptionsEffectues = new ArrayList<Interruption>();
	private Collection<HeurSupp> hrSuppsEffectues = new ArrayList<HeurSupp>();
	private Collection<AbsenceEff> absenceEffsEffectues = new ArrayList<AbsenceEff>();
	private Collection<Conge> congesEffectues = new ArrayList<Conge>();
	private Collection<Compensation> compsEffectues = new ArrayList<Compensation>();
	private Collection<Annomalie> anomaliesEffectues = new ArrayList<Annomalie>();

	private Map<String, Annotation> annotations = new Hashtable<String, Annotation>();
	
	
	// Services
	private AnnomalieManageableService annoService;
	private MouvementManageableService mvmntService;
	
	// Global constants
	private final String HORAIRE_LIBRE = "horaireLibre";
	private final String SANS_POI_COMPTEUR = "sansPointage";
	private final String OUT_SANS_POI_COMPTEUR = "sortieSansPointage";
	private final String EFFECTUE_STATUT = "effectue";
	

	// Task attributes
	private Planning plAtribute;
	
	

	/**
	 * Gets the shared instance of this Class
	 * 
	 * @return the shared AnnoTrucker instance.
	 */
	public LiveProcessTask() {

	}
	
	public void clearDatas() {
		jrFeries = null;
		congesValidates = null;
		absencesValidates = null;
		recupsValidates = null;
		compValidates = null;
		absRecs = null;
		hrSuppsValidates = null;
		absencesEffectues = null;
		retardsEffectues = null;
		interruptionsEffectues = null;
		hrSuppsEffectues = null;
		absenceEffsEffectues = null;
		congesEffectues = null;
		compsEffectues = null;
		anomaliesEffectues = null;
		annotations = null;
		annoService = null;
		mvmntService = null;
	}


	/**
	 * Calculer le cumul des heures supplimentaires consomm�es par le
	 * collaborateur
	 * 
	 * @param d1
	 * @param d2
	 * @param hrSupps
	 * @return
	 */
	private void compareProgHrSupp(Day d, Calendar d1, Calendar d2, User user)
			throws Exception {
		Calendar cal = new GregorianCalendar();
		for (Plage plage : d.getHrSupps()) {

			HeurSupp hrSuppEf = new HeurSuppImpl();
			long diff;
			if ((d1.compareTo(plage.getFin()) >= 0)
					|| (d2.compareTo(plage.getDebut()) <= 0)) // pas
																// d'intersection
																// entre
																// d1<-->d2 et
																// debut<-->fin
				continue;
			else if ((d1.compareTo(plage.getDebut()) >= 0)
					&& (d2.compareTo(plage.getFin()) <= 0)) { // cumul : d1 <-->
																// d2
				diff = Utils.differenceDates(d1.getTime(), d2.getTime());
				if (diff <= DT_hsp_prm * 60000)
					continue;
				cal.setTimeInMillis(diff);
				try {
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1
							.getTime()));
					hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
					hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					if (plage.getPlageHor() == null)
						hrSuppEf.setAnnotation(annotations.get("OBL"));
					else
						hrSuppEf.setAnnotation(plage.getPlageHor()
								.getAnnotation());
					if (d.isHoraireNuit())
						hrSuppEf.setHorNuit(true);
					hrSuppsEffectues.add(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
				d1.set(Calendar.HOUR_OF_DAY, d2.get(Calendar.HOUR_OF_DAY));
				d1.set(Calendar.MINUTE, d2.get(Calendar.MINUTE));
				break;
			} else if ((d1.compareTo(plage.getDebut()) >= 0)
					&& (d2.compareTo(plage.getFin()) >= 0)) { // cumul : d1 <-->
																// fin
				diff = Utils.differenceDates(d1.getTime(), plage.getFin()
						.getTime());
				if (diff <= DT_hsp_prm * 60000)
					continue;
				cal.setTimeInMillis(diff);
				try {
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1
							.getTime()));
					hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(plage
							.getFin().getTime()));
					hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					if (plage.getPlageHor() == null)
						hrSuppEf.setAnnotation(annotations.get("HSUP100"));
					else
						hrSuppEf.setAnnotation(plage.getPlageHor()
								.getAnnotation());
					if (d.isHoraireNuit())
						hrSuppEf.setHorNuit(true);
					d.getHrSuppsEffectues().add(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
				d1.set(Calendar.HOUR_OF_DAY,
						plage.getFin().get(Calendar.HOUR_OF_DAY));
				d1.set(Calendar.MINUTE, plage.getFin().get(Calendar.MINUTE));
			} else if ((d1.compareTo(plage.getDebut()) <= 0)
					&& (d2.compareTo(plage.getFin()) >= 0)) { // cumul : debut
																// <--> fin
				diff = Utils.differenceDates(plage.getDebut().getTime(), plage
						.getFin().getTime());
				if (diff <= DT_hsp_prm * 60000)
					continue;
				cal.setTimeInMillis(diff);
				try {
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(plage
							.getDebut().getTime()));
					hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(plage
							.getFin().getTime()));
					hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					if (plage.getPlageHor() == null)
						hrSuppEf.setAnnotation(annotations.get("HSUP100"));
					else
						hrSuppEf.setAnnotation(plage.getPlageHor()
								.getAnnotation());
					if (d.isHoraireNuit())
						hrSuppEf.setHorNuit(true);
					d.getHrSuppsEffectues().add(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
				Calendar c = new GregorianCalendar();
				c.setTime(d1.getTime());
				compareProgHrSupp(d, c, plage.getDebut(), user);
				c.setTime(d2.getTime());
				compareProgHrSupp(d, plage.getFin(), c, user);
			} else if ((d1.compareTo(plage.getDebut()) <= 0)
					&& (d2.compareTo(plage.getFin()) <= 0)) { // cumul : debut
																// <--> d2
				diff = Utils.differenceDates(plage.getDebut().getTime(),
						d2.getTime());
				if (diff <= DT_hsp_prm * 60000)
					continue;
				cal.setTimeInMillis(diff);
				try {
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(plage
							.getDebut().getTime()));
					hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
					hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					if (plage.getPlageHor() == null)
						hrSuppEf.setAnnotation(annotations.get("HSUP100"));
					else
						hrSuppEf.setAnnotation(plage.getPlageHor()
								.getAnnotation());
					if (d.isHoraireNuit())
						hrSuppEf.setHorNuit(true);
					d.getHrSuppsEffectues().add(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error(e.getMessage());
				}
				d2.set(Calendar.HOUR_OF_DAY,
						plage.getDebut().get(Calendar.HOUR_OF_DAY));
				d2.set(Calendar.MINUTE, plage.getDebut().get(Calendar.MINUTE));
			}
		}
	}

	private void projection(Plage plage, Day d, User user, Calendar input)
			throws Exception {

		long entree = 0;
		long sortie = 0;

		// indicateur de la suppression du mvnt de traitement finit
		boolean deleted = false;

		// parametres de definiton d'interruption
		Date debutInter = null;

		Calendar laDate;

		Interruption interruption = new InterruptionImpl();

		long tpsEff = 0;
		Calendar c = Calendar.getInstance();

		plage.getZoneOut().add(Calendar.MINUTE, -DT_hsp_prm);
		Utils.sortMvtsByTime(d.getMouvements());
		List<Mouvement> list = new ArrayList<Mouvement>(d.getMouvements());
		Utils.sortMvtsByTime(list);
		for (Mouvement mvmnt : list) {

			// on construit laDate par la date, l'heure et minute de pointage
			laDate = new GregorianCalendar();
			laDate.setTime(mvmnt.getDate());
			if (mvmnt.isTraited())
				continue;

			deleted = false;
			Retard retard = new RetardImpl();

			if (laDate.compareTo(plage.getDebut()) < 0) {
				if (!plage.isEntree()) {
					if (plage.getLastPlage(d.getHrFixes()) == null
							|| (plage.getLastPlage(d.getHrFixes()) != null && plage
									.getLastPlage(d.getHrFixes()).isSortie())) {
						plage.setEntree(true);
						mvmnt.setTraited(true);
						entree = -1;
						input.setTime(laDate.getTime());
					} else if (plage.getLastPlage(d.getHrFixes()) != null
							&& !plage.getLastPlage(d.getHrFixes()).isSortie()) {
						plage.getLastPlage(d.getHrFixes()).setSortie(true);

						tpsEff = tpsEff
								+ Utils.differenceDates(input.getTime(),
										laDate.getTime());
						c.setTimeInMillis(tpsEff);
						if (d.isFlexible()) {
							d.setTmworked(Utils.additionHeures(d.getTmworked(),
									Utils.getHourDoubleFormat(c.getTime())));
							tpsEff = 0;
						}
						mvmnt.setTraited(true);

						MouvementImpl fin = new MouvementImpl();
						fin.setDate(plage.getLastPlage(d.getHrFixes()).getFin()
								.getTime());
						fin.setBadge(user.getBadge());
						fin.setTraited(true);
						if (!d.getMouvements().contains(fin))
							d.getMouvements().add(fin);
					}
					continue;
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff
							+ Utils.differenceDates(input.getTime(),
									laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else if (laDate.compareTo(plage.getDebut()) == 0) {
				if (!plage.isEntree()) {
					plage.setEntree(true);
					entree = 1;
					deleted = true;
					input.setTime(laDate.getTime());
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff
							+ Utils.differenceDates(input.getTime(),
									laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else if ((laDate.compareTo(plage.getDebut()) > 0)
					&& (laDate.compareTo(plage.getDebutPlus()) <= 0)) {
				if (!plage.isEntree()) {
					int Rt = (laDate.get(Calendar.HOUR_OF_DAY) * 60 + laDate
							.get(Calendar.MINUTE))
							- (plage.getDebut().get(Calendar.HOUR_OF_DAY) * 60 + plage
									.getDebut().get(Calendar.MINUTE));
					if ((plage.getDebutFlex() == null)
							|| (plage.getDebutFlex() != null && plage
									.getDebutFlex().compareTo(laDate) < 0)) {
						retard.setDate(d.getDate());
						retard.setIndexPlage(plage.getIndex());
						retard.setUser((UserImpl) user);
						retard.setMatricule(user.getMatricule() + "-ER");
						retard.setRetardTolere(Rt);
						retard.setDescription("Retard tol�r� de :"
								+ Rt
								+ " mn � "
								+ Utils.getHourDateFormat(plage.getDebut()
										.getTime()));
						if (d.isHoraireNuit())
							retard.setHorNuit(true);
						else
							retard.setHorNuit(false);
						d.getRetardsEffectues().add(retard);
					}
					plage.setEntree(true);
					input.setTime(laDate.getTime());
					entree = 1;
					deleted = true;
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff
							+ Utils.differenceDates(input.getTime(),
									laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else if ((laDate.compareTo(plage.getDebutPlus()) > 0)
					&& (laDate.compareTo(plage.getFinMoins()) < 0)) {
				if (!plage.isEntree()) {
					if (laDate.compareTo(plage.getZoneOut()) <= 0) {
						int Rt = (laDate.get(Calendar.HOUR_OF_DAY) * 60 + laDate
								.get(Calendar.MINUTE))
								- (plage.getDebut().get(Calendar.HOUR_OF_DAY) * 60 + plage
										.getDebut().get(Calendar.MINUTE));
						if ((plage.getDebutFlex() == null)
								|| (plage.getDebutFlex() != null && plage
										.getDebutFlex().compareTo(laDate) < 0)) {
							retard.setDate(d.getDate());
							retard.setIndexPlage(plage.getIndex());
							retard.setUser((UserImpl) user);
							retard.setMatricule(user.getMatricule() + "-ER");
							retard.setRetardNonTolere(Rt);
							retard.setDescription("Retard non tol�r� de :"
									+ Rt
									+ " mn � "
									+ Utils.getHourDateFormat(plage.getDebut()
											.getTime()));
							if (d.isHoraireNuit())
								retard.setHorNuit(true);
							else
								retard.setHorNuit(false);
							d.getRetardsEffectues().add(retard);
						}
						plage.setEntree(true);
						input.setTime(laDate.getTime());
						entree = 1;
						deleted = true;
					} else {
						plage.setSortie(true);
						sortie = 1;
						deleted = true;
					}
				} else {
					if (debutInter == null) {
						debutInter = new Date(laDate.getTimeInMillis());
						tpsEff = tpsEff
								+ Utils.differenceDates(input.getTime(),
										laDate.getTime());

						// Traitement EXCP PR&PO
						if (d.getExceptP() != null
								&& (d.getExceptP() instanceof ProfilMetier)) {
							ProfilMetier pr = (ProfilMetier) d.getExceptP();
							Annomalie annomalie;
							if (plage.isDebutTmg()
									&& plage.getDebut().compareTo(input) > 0) {
								// ano de type EPM-HS
								annomalie = new AnnomalieImpl();
								annomalie.setDateAnomalie(d.getDate());
								annomalie.setDescription("travail en tant que "
										+ pr.getCode()
										+ "-"
										+ pr.getLibelle()
										+ " entre '"
										+ Utils.getHourDoubleFormat(input
												.getTime())
										+ "' et '"
										+ Utils.getHourDoubleFormat(plage
												.getDebut().getTime()) + "'");
								annomalie.setUser(user);
								annomalie.setType("EPM-HS");
								annomalie.setMatricule("E-"
										+ user.getMatricule());
								if (d.isHoraireNuit())
									annomalie.setHorNuit(true);
								anomaliesEffectues.add(annomalie);
							}
							List<Timing> l;
							if (plage.getDebut().compareTo(input) > 0)
								l = plage.getEffTmgs(d.getDate(),
										plage.getDebut(), laDate);
							else
								l = plage
										.getEffTmgs(d.getDate(), input, laDate);

							// anos de type EPM
							for (Timing t : l) {
								annomalie = new AnnomalieImpl();
								annomalie.setDateAnomalie(d.getDate());
								annomalie
										.setDescription("travail en tant que '"
												+ pr.getCode() + "'-'"
												+ pr.getLibelle() + "' entre '"
												+ t.getHreDebut() + "' et '"
												+ t.getHreFin() + "'");
								annomalie.setUser(user);
								annomalie.setType("EPM");
								annomalie.setMatricule("E-"
										+ user.getMatricule());
								if (d.isHoraireNuit())
									annomalie.setHorNuit(true);
								anomaliesEffectues.add(annomalie);
							}
						} else if (d.getExceptP() != null
								&& (d.getExceptP() instanceof Noeud)) {
							Noeud po = (Noeud) d.getExceptP();
							Annomalie annomalie;
							if (plage.isDebutTmg()
									&& plage.getDebut().compareTo(input) > 0) {
								// ano de type EPO-HS
								annomalie = new AnnomalieImpl();
								annomalie.setDateAnomalie(d.getDate());
								annomalie
										.setDescription("travail dans le position "
												+ po.getCode()
												+ "-"
												+ po.getLibelle()
												+ " entre '"
												+ Utils.getHourDoubleFormat(input
														.getTime())
												+ "' et '"
												+ Utils.getHourDoubleFormat(plage
														.getDebut().getTime())
												+ "'");
								annomalie.setUser(user);
								annomalie.setType("EPO-HS");
								annomalie.setMatricule("E-"
										+ user.getMatricule());
								if (d.isHoraireNuit())
									annomalie.setHorNuit(true);
								anomaliesEffectues.add(annomalie);
							}
							List<Timing> l;
							if (plage.getDebut().compareTo(input) > 0)
								l = plage.getEffTmgs(d.getDate(),
										plage.getDebut(), laDate);
							else
								l = plage
										.getEffTmgs(d.getDate(), input, laDate);

							// anos de type EPO
							for (Timing t : l) {
								annomalie = new AnnomalieImpl();
								annomalie.setDateAnomalie(d.getDate());
								annomalie
										.setDescription("travail dans le position '"
												+ po.getCode()
												+ "'-'"
												+ po.getLibelle()
												+ "' entre '"
												+ t.getHreDebut()
												+ "' et '"
												+ t.getHreFin() + "'");
								annomalie.setUser(user);
								annomalie.setType("EPO");
								annomalie.setMatricule("E-"
										+ user.getMatricule());
								if (d.isHoraireNuit())
									annomalie.setHorNuit(true);
								anomaliesEffectues.add(annomalie);
							}
						}
					} else {
						Date[] outs = plage.getValideInterp(debutInter,
								laDate.getTime(), DT_pause_prm);
						Date d1;
						Date d2;
						if (outs != null) {
							d1 = outs[0];
							d2 = outs[1];
							double diff = Utils.differenceHeures(
									Utils.getHourDoubleFormat(d1),
									Utils.getHourDoubleFormat(d2));
							int i = stateAbsComp(d, diff);
							Annomalie annomalie = new AnnomalieImpl();
							if (i == 1) {
								annomalie.setDateAnomalie(d.getDate());
								annomalie
										.setDescription("J.trv :La duree absent�e est sup�rieure � celle programm�e dans la demande compensation.");
								annomalie.setUser(user);
								annomalie.setType("NRT");
								annomalie.setMatricule("E-"
										+ user.getMatricule());
								if (d.isHoraireNuit())
									annomalie.setHorNuit(true);
								anomaliesEffectues.add(annomalie);
							} else if (i == -1) {
								AbsenceImpl absence = new AbsenceImpl();
								Calendar cal = new GregorianCalendar();
								cal.setTime(d1);
								cal.set(Calendar.HOUR_OF_DAY, 0);
								cal.set(Calendar.MINUTE, 0);
								cal.set(Calendar.SECOND, 0);
								cal.set(Calendar.MILLISECOND, 0);
								absence.setDateDebut(cal.getTime());
								absence.setHeureDebut(Utils
										.getHourDoubleFormat(d1));

								Calendar cal2 = new GregorianCalendar();
								cal2.setTime(d2);
								cal2.set(Calendar.HOUR_OF_DAY, 0);
								cal2.set(Calendar.MINUTE, 0);
								cal2.set(Calendar.SECOND, 0);
								cal2.set(Calendar.MILLISECOND, 0);
								absence.setDateReprise(cal2.getTime());
								absence.setHeureReprise(Utils
										.getHourDoubleFormat(d2));

								absence.setCollaborateur(user);
								absence.setStatut(MessageFactory
										.getMessage(EFFECTUE_STATUT));
								if (d.isHoraireNuit())
									absence.setHorNuit(true);
								else
									absence.setHorNuit(false);
								List<Absence> l = getAbsencesIfValidated(absence);
								if (l == null || l.isEmpty()) {
									String start = Utils.getHourDateFormat(d1);
									String end = Utils.getHourDateFormat(d2);
									interruption.setDate(d.getDate());
									interruption.setDuree(diff);
									interruption.setHeureDebut(Double
											.parseDouble(start.substring(0, 2)
													+ "."
													+ start.substring(3, 5)));
									interruption
											.setHeureReprise(Double
													.parseDouble(end.substring(
															0, 2)
															+ "."
															+ end.substring(3,
																	5)));
									interruption
											.setDescription("interruption entre '"
													+ start
													+ "' et '"
													+ end
													+ "'");
									interruption.setUser(user);
									interruption.setMatricule("ITRS-"
											+ user.getMatricule());
									if (d.isHoraireNuit())
										interruption.setHorNuit(true);
									else
										interruption.setHorNuit(false);
									d.getInterruptionsEffectues().add(interruption);
								} else {
									d.getLegalAbs().addAll(l);
									for (Absence ab : l) {
										if (ab == null)
											continue;
										ab.setStatut(MessageFactory
												.getMessage(EFFECTUE_STATUT));
										if (ab.getTypeAbsence().getLibelle() == null)
											continue;
										d.getAbsencesEffectues().add(ab);
									}
								}
							}
						}
						debutInter = null;
					}
					input.setTime(laDate.getTime());
					deleted = true;
				}
			} else if ((laDate.compareTo(plage.getFinMoins()) >= 0)
					&& (laDate.compareTo(plage.getFin()) <= 0)
					&& !plage.isSortie()) {
				plage.setSortie(true);
				tpsEff = tpsEff
						+ Utils.differenceDates(input.getTime(),
								laDate.getTime());
				sortie = 1;
				deleted = true;

				// Traitement EXCPPM
				if (d.getExceptP() != null
						&& (d.getExceptP() instanceof ProfilMetier)) {
					ProfilMetier pr = (ProfilMetier) d.getExceptP();
					Annomalie annomalie;
					if (plage.isDebutTmg()
							&& plage.getDebut().compareTo(input) > 0) {
						// ano de type EPM-HS
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que "
								+ pr.getCode()
								+ "-"
								+ pr.getLibelle()
								+ " entre '"
								+ Utils.getHourDoubleFormat(input.getTime())
								+ "' et '"
								+ Utils.getHourDoubleFormat(plage.getDebut()
										.getTime()) + "'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
					List<Timing> l;
					if (plage.getDebut().compareTo(input) > 0)
						l = plage.getEffTmgs(d.getDate(), plage.getDebut(),
								laDate);
					else
						l = plage.getEffTmgs(d.getDate(), input, laDate);

					// anos de type EPM
					for (Timing t : l) {
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que '"
								+ pr.getCode() + "'-'" + pr.getLibelle()
								+ "' entre '" + t.getHreDebut() + "' et '"
								+ t.getHreFin() + "'");
						annomalie.setUser(user);
						annomalie.setType("EPM");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
				} else if (d.getExceptP() != null
						&& (d.getExceptP() instanceof Noeud)) {
					Noeud po = (Noeud) d.getExceptP();
					Annomalie annomalie;
					if (plage.isDebutTmg()
							&& plage.getDebut().compareTo(input) > 0) {
						// ano de type EPO-HS
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail dans le position "
								+ po.getCode()
								+ "-"
								+ po.getLibelle()
								+ " entre '"
								+ Utils.getHourDoubleFormat(input.getTime())
								+ "' et '"
								+ Utils.getHourDoubleFormat(plage.getDebut()
										.getTime()) + "'");
						annomalie.setUser(user);
						annomalie.setType("EPO-HS");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
					List<Timing> l;
					if (plage.getDebut().compareTo(input) > 0)
						l = plage.getEffTmgs(d.getDate(), plage.getDebut(),
								laDate);
					else
						l = plage.getEffTmgs(d.getDate(), input, laDate);

					// anos de type EPO
					for (Timing t : l) {
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail dans le position '"
								+ po.getCode() + "'-'" + po.getLibelle()
								+ "' entre '" + t.getHreDebut() + "' et '"
								+ t.getHreFin() + "'");
						annomalie.setUser(user);
						annomalie.setType("EPO");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
				}
			} else if (!plage.isSortie() || debutInter != null) {

				if (plage.getFolowPlage(d.getHrFixes()) == null) {
					MouvementImpl fin = new MouvementImpl();
					fin.setDate(plage.getFin().getTime());
					fin.setBadge(user.getBadge());
					fin.setTraited(true);
					if (!d.getMouvements().contains(fin))
						d.getMouvements().add(fin);
					sortie = 2;
				}
				// else if (plage.getLastPlage(d.getHrFixes()) != null
				// && plage.getLastPlage(d.getHrFixes()).isEntree()
				// && plage.getLastPlage(d.getHrFixes()).isSortie()) {
				// Mouvement debut = new MouvementImpl();
				// debut.setDate(plage.getDebut().getTime());
				// debut.setBadge(user.getBadge());
				// debut.setTraited(true);
				// if (!d.getMouvements().contains(debut))
				// d.getMouvements().add(debut);
				// Mouvement fin = new MouvementImpl();
				// fin.setDate(plage.getFin().getTime());
				// fin.setBadge(user.getBadge());
				// fin.setTraited(true);
				// if (!d.getMouvements().contains(fin))
				// d.getMouvements().add(fin);
				// }

				Calendar cal = new GregorianCalendar();
				cal.setTime(laDate.getTime());
				Day nextDay = d.getNextDay(days);
				if (nextDay != null && nextDay.isHoraireNuit()
						&& nextDay.getHrFixes() != null
						&& !nextDay.getHrFixes().isEmpty()) {
					cal.setTime(nextDay.getHrFixes().get(0).getDebut()
							.getTime());
					cal.add(Calendar.HOUR_OF_DAY, -1);
				}
				if (plage.getFolowPlage(d.getHrFixes()) == null
						|| laDate.compareTo(cal) < 0) {
					plage.setSortie(true);
					mvmnt.setTraited(true);
					tpsEff = tpsEff
							+ Utils.differenceDates(input.getTime(),
									laDate.getTime());
				}

				// Traitement EXCPPM
				if (d.getExceptP() != null
						&& (d.getExceptP() instanceof ProfilMetier)) {
					ProfilMetier pr = (ProfilMetier) d.getExceptP();
					Annomalie annomalie;
					if (plage.isDebutTmg()
							&& plage.getDebut().compareTo(input) > 0) {
						// ano de type EPM-HS
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que "
								+ pr.getCode()
								+ "-"
								+ pr.getLibelle()
								+ " entre '"
								+ Utils.getHourDoubleFormat(input.getTime())
								+ "' et '"
								+ Utils.getHourDoubleFormat(plage.getDebut()
										.getTime()) + "'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
					if (plage.isFinTmg()) {
						// ano de type EPM-HS
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que "
								+ pr.getCode()
								+ "-"
								+ pr.getLibelle()
								+ " entre '"
								+ Utils.getHourDoubleFormat(plage.getFin()
										.getTime()) + "' et '"
								+ Utils.getHourDoubleFormat(laDate.getTime())
								+ "'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
					List<Timing> l;
					if (plage.getDebut().compareTo(input) > 0)
						l = plage.getEffTmgs(d.getDate(), plage.getDebut(),
								plage.getFin());
					else
						l = plage
								.getEffTmgs(d.getDate(), input, plage.getFin());

					// anos de type EPM
					for (Timing t : l) {
						annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que '"
								+ pr.getCode() + "'-'" + pr.getLibelle()
								+ "' entre '" + t.getHreDebut() + "' et '"
								+ t.getHreFin() + "'");
						annomalie.setUser(user);
						annomalie.setType("EPM");
						annomalie.setMatricule("E-" + user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
				}
			} else if (d.getExceptP() != null
					&& (d.getExceptP() instanceof Noeud)) {
				Noeud po = (Noeud) d.getExceptP();
				Annomalie annomalie;
				if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0) {
					// ano de type EPO-HS
					annomalie = new AnnomalieImpl();
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail dans le position "
							+ po.getCode()
							+ "-"
							+ po.getLibelle()
							+ " entre '"
							+ Utils.getHourDoubleFormat(input.getTime())
							+ "' et '"
							+ Utils.getHourDoubleFormat(plage.getDebut()
									.getTime()) + "'");
					annomalie.setUser(user);
					annomalie.setType("EPO-HS");
					annomalie.setMatricule("E-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				}
				if (plage.isFinTmg()) {
					// ano de type EPO-HS
					annomalie = new AnnomalieImpl();
					annomalie.setDateAnomalie(d.getDate());
					annomalie
							.setDescription("travail dans le position "
									+ po.getCode()
									+ "-"
									+ po.getLibelle()
									+ " entre '"
									+ Utils.getHourDoubleFormat(plage.getFin()
											.getTime())
									+ "' et '"
									+ Utils.getHourDoubleFormat(laDate
											.getTime()) + "'");
					annomalie.setUser(user);
					annomalie.setType("EPO-HS");
					annomalie.setMatricule("E-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				}
				List<Timing> l;
				if (plage.getDebut().compareTo(input) > 0)
					l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
				else
					l = plage.getEffTmgs(d.getDate(), input, laDate);

				// anos de type EPO
				for (Timing t : l) {
					annomalie = new AnnomalieImpl();
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail dans le position '"
							+ po.getCode() + "'-'" + po.getLibelle()
							+ "' entre '" + t.getHreDebut() + "' et '"
							+ t.getHreFin() + "'");
					annomalie.setUser(user);
					annomalie.setType("EPO");
					annomalie.setMatricule("E-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				}
			}
			if (deleted)
				d.getMouvements().remove(mvmnt);

			if (mvmnt.isTraited()) {
				d.getMouvements().remove(mvmnt);
				d.getMouvements().add(mvmnt);
			}
			if (plage.isSortie())
				break;
		}

		c.setTimeInMillis(tpsEff);
		if (d.isFlexible()) {
			d.setTmworked(Utils.additionHeures(d.getTmworked(),
					Utils.getHourDoubleFormat(c.getTime())));
			tpsEff = 0;
		}
		// if (plage.getLastPlage(d.getHrFixes()) != null
		// && plage.getLastPlage(d.getHrFixes()).isEntree()
		// && !plage.getLastPlage(d.getHrFixes()).isSortie()
		// && entree == 0)
		// entree = -1;

		if (entree == -1) {
			MouvementImpl debut = new MouvementImpl();
			debut.setDate(plage.getDebut().getTime());
			debut.setBadge(user.getBadge());
			debut.setTraited(true);
			d.getMouvements().add(debut);
		}

		if (debutInter != null) {
			plage.setSortie(true);

			double diff = Utils.differenceHeures(
					Utils.getHourDoubleFormat(debutInter),
					Utils.getHourDoubleFormat(plage.getFin().getTime()));

			AbsenceImpl absence = new AbsenceImpl();
			Calendar cal = new GregorianCalendar();
			cal.setTime(debutInter);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			absence.setDateDebut(cal.getTime());
			absence.setHeureDebut(Utils.getHourDoubleFormat(debutInter));

			Calendar cal2 = new GregorianCalendar();
			cal2.setTime(plage.getFin().getTime());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			absence.setDateReprise(cal2.getTime());
			absence.setHeureReprise(Utils.getHourDoubleFormat(plage.getFin()
					.getTime()));

			absence.setDuree(0);
			absence.setCollaborateur(user);
			absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			if (d.isHoraireNuit())
				absence.setHorNuit(true);
			else
				absence.setHorNuit(false);
			List<Absence> l = getAbsencesIfValidated(absence);
			if ((l == null || l.isEmpty())
					&& (plage.getPlageHor().getFlexFin() <= diff)) {
				String start = Utils.getHourDateFormat(debutInter);
				String end = Utils.getHourDateFormat(plage.getFin().getTime());
				interruption.setDate(d.getDate());
				interruption.setDuree(diff);
				interruption.setHeureDebut(Double.parseDouble(start.substring(
						0, 2) + "." + start.substring(3, 5)));
				interruption.setHeureReprise(Double.parseDouble(end.substring(
						0, 2) + "." + end.substring(3, 5)));
				interruption.setDescription("interruption sans retoure entre '"
						+ start + "' et '" + end + "'");
				interruption.setUser(user);
				interruption.setMatricule("ITRS-" + user.getMatricule());
				if (d.isHoraireNuit())
					interruption.setHorNuit(true);
				else
					interruption.setHorNuit(false);
				d.getInterruptionsEffectues().add(interruption);
			} else if (l != null && !l.isEmpty()) {
				d.getLegalAbs().addAll(l);
				for (Absence ab : l) {
					if (ab == null)
						continue;
					ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					if (ab.getTypeAbsence().getLibelle() == null)
						continue;
					d.getAbsencesEffectues().add(ab);
				}
			}
		}

	}

	/**
	 * Lancement de traitement apres l'extraction de plages horaires de type
	 * obligatoire et heure suplimentaire
	 * 
	 * @param horaire
	 * @param laDate
	 * @param user
	 * @throws Exception
	 */
	private void traiterParHoraire(Day d, User user) throws Exception {
		Annomalie annomalie = new AnnomalieImpl();
		d.getMvtsAllDay().clear();
		d.getMvtsAllDay().addAll(d.getMouvements());
		if (d.isJrRepos()) {
			if (!d.isHrSuppDay() && !d.isCompensedDay() && !d.isRecuperedDay()) {
				if ((d.getNature() != null && d.getNature() instanceof JrFerie)
						&& Ano_TJF.equals("yes")) { // planning condi ttes hsups
													// sont valid�es
					JrFerie jf = (JrFerie) d.getNature();
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail jour ferie :"
							+ jf.getLibelle());
					annomalie.setUser(user);
					annomalie.setType("TJR");
					annomalie.setMatricule("JF-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				} else if (d.getNature() != null
						&& d.getNature() instanceof Conge) {
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail jour de cong�");
					annomalie.setUser(user);
					annomalie.setType("TJR");
					annomalie.setMatricule("JC-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				}
				if (Ano_TJR.equals("yes")) {
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail jour de Repos");
					annomalie.setUser(user);
					annomalie.setType("TJR");
					annomalie.setMatricule("JR-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					anomaliesEffectues.add(annomalie);
				}
				return;
			}
		} else if (d.isAbsCompensedDay()) {
			annomalie.setDateAnomalie(d.getDate());
			annomalie.setDescription("Travail jour d'un repos compensateur");
			annomalie.setUser(user);
			annomalie.setType("TJC");
			if (d.isHoraireNuit())
				annomalie.setHorNuit(true);
			annomalie.setMatricule("JC-" + user.getMatricule());
			anomaliesEffectues.add(annomalie);
		}

		if (d.getMouvements().size() % 2 != 0) {
			annomalie = new AnnomalieImpl();
			annomalie.setDateAnomalie(d.getDate());
			annomalie.setDescription("Warning : Nombre impaire de pointage ("
					+ d.getMouvements().size() + " mouvements)");
			annomalie.setUser(user);
			annomalie.setType("NIP");
			annomalie.setMatricule("NIP-" + user.getMatricule());
			anomaliesEffectues.add(annomalie);
		}
		if (d.getPlanning() == null)
			return;
		Calendar input = new GregorianCalendar();
		if (d.isFlexible())
			d.setTmworked(0.0);
		int index = 0;
		for (Plage plage : d.getHrFixes()) {
			index++;
			plage.setIndex(index);
			String anot = plage.getPlageHor().getAnnotation()
					.getTypeAnnotation().getLibelle();
			if (anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE)))
				d.setLibelle(MessageFactory.getMessage(this.HORAIRE_LIBRE));
			Calendar cal = Calendar.getInstance();
			if (plage.getDebut().after(cal))
				break;
			else if (!anot.equals(MessageFactory
					.getMessage(this.HORAIRE_LIBRE))
						&& !anot.equals(MessageFactory
							.getMessage(this.SANS_POI_COMPTEUR)))
					projection(plage, d, user, input); // projeter la liste des mvts
													// sur la plage horaire fixe
		}

		Utils.sortMvtsByTime(d.getMouvements());
		Calendar cal = new GregorianCalendar();
		cal.setTime(d.getDate());
		cal.add(Calendar.DAY_OF_MONTH, -1);
		boolean isAbs = false;
		index = 0;
		for (Plage plage : d.getHrFixes()) {
			index++;
			String anot = plage.getPlageHor().getAnnotation()
					.getTypeAnnotation().getLibelle();
			if (anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE)))
				continue;
			if (!plage.isEntree() || !plage.isSortie())
				isAbs = true;
			if (plage.isEntree() && !plage.isSortie()) {				
				annomalie.setDateAnomalie(d.getDate());
				annomalie.setDescription("Omission de pointage à la sortie :"+plage.getEndHour());
				annomalie.setUser(user);
				annomalie.setType("OPJO");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("S-"+user.getMatricule());
				anomaliesEffectues.add(annomalie);
				
				AbsenceEffImpl absence = new AbsenceEffImpl();
				absence.setDateDebut(d.getDate());	
				absence.setDateReprise(d.getDate());
				absence.setDuree(0);
				absence.setHeureDebut(Double.parseDouble(plage.getStartHour().replace(':', '.')));
				absence.setHeureReprise(Double.parseDouble(plage.getEndHour().replace(':', '.')));
				absence.setCollaborateur(user);
				absence.setIndexPlage(index);
				absenceEffsEffectues.add(absence);
			} else if (!plage.isEntree() && plage.isSortie()) {
				annomalie.setDateAnomalie(d.getDate());
				annomalie.setDescription("Omission de pointage à l'entrée :"+plage.getStartHour());
				annomalie.setUser(user);
				annomalie.setType("OPJO");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("E-"+user.getMatricule());
				anomaliesEffectues.add(annomalie);
				AbsenceEffImpl absence = new AbsenceEffImpl();
				absence.setDateDebut(d.getDate());
				absence.setDateReprise(d.getDate());
				absence.setDuree(0);
				absence.setHeureDebut(Double.parseDouble(plage.getStartHour().replace(':', '.')));
				absence.setHeureReprise(Double.parseDouble(plage.getEndHour().replace(':', '.')));
				absence.setCollaborateur(user);
				absence.setIndexPlage(index);
				absenceEffsEffectues.add(absence);
			} else if (!plage.isEntree() && !plage.isSortie()) {
				AbsenceImpl absence = new AbsenceImpl();
				if (plage.isPlageNuit()) {
					absence.setDateDebut(cal.getTime());
					absence.setHorNuit(true);
				} else {
					absence.setDateDebut(d.getDate());
					absence.setHorNuit(false);
				}
				absence.setDateReprise(d.getDate());
				absence.setDuree(0);
				absence.setHeureDebut(Double.parseDouble(plage.getStartHour()
						.replace(':', '.')));
				absence.setHeureReprise(Double.parseDouble(plage.getEndHour()
						.replace(':', '.')));
				absence.setCollaborateur(user);
				absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
				List<Absence> l = getAbsencesIfValidated(absence);
				if (l == null || l.isEmpty()) {
					absence.setTypeAbsence(noJustifyAbsence);
					d.getAbsencesEffectues().add(absence);
				} else {
					d.getLegalAbs().addAll(l);
					for (Absence ab : l) {
						if (ab == null)
							continue;
						ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
						if (ab.getTypeAbsence().getLibelle() == null)
							continue;
						d.getAbsencesEffectues().add(ab);
					}
				}
			}
		}

		if (!d.getLegalAbs().isEmpty()) {
			AbsenceImpl absence = new AbsenceImpl();
			absence.setDuree(d.getP_tmcontract());
			Double duree = absence.minus(d.getLegalAbs());
			if (duree > 0) {
				absence.setDateDebut(d.getDate());
				absence.setDateReprise(d.getDate());
				if (d.isHoraireNuit())
					absence.setHorNuit(true);
				else
					absence.setHorNuit(false);
				absence.setCollaborateur(user);
				absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
				absence.setTypeAbsence(noJustifyAbsence);
				absence.setDuree(duree);
				d.getAbsencesEffectues().add(absence);
			}
		}

		if (d.isFlexible() && !isAbs) {
			if (d.getTmworked() < d.getP_tmcontract()) {
				double diff = Utils.differenceHeures(d.getP_tmcontract(),
						d.getTmworked());
				annomalie.setDateAnomalie(d.getDate());
				annomalie
						.setDescription("Non respect de temps contractuel avec '"
								+ diff
								+ "' de difference le cas d'horaire flexibe !!");
				annomalie.setUser(user);
				annomalie.setType("NRT");
				annomalie.setMatricule("JT-" + user.getMatricule());
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				anomaliesEffectues.add(annomalie);
			}
		}

	}

	private void traiterHSupp(Day d, User user) throws Exception {
		Annomalie annomalie = new AnnomalieImpl();
		if (d.getLibelle()
				.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE))) {
			if (d.getMouvements().size() % 2 != 0) {
				annomalie.setDateAnomalie(d.getDate());
				annomalie
						.setDescription("Omission de pointage cas d'horaire Libre.");
				annomalie.setUser(user);
				annomalie.setType("OPHL");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("E-" + user.getMatricule());
				anomaliesEffectues.add(annomalie);
			} else if (d.getTmworked() < d.getP_tmcontract()) {
				double diff = Utils.differenceHeures(d.getP_tmcontract(),
						d.getTmworked());
				annomalie.setDateAnomalie(d.getDate());
				annomalie
						.setDescription("Non respect de temps contractuel avec '"
								+ diff
								+ "' de difference le cas d'horaire libre !!");
				annomalie.setUser(user);
				annomalie.setType("NRT");
				annomalie.setMatricule("JT-" + user.getMatricule());
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				anomaliesEffectues.add(annomalie);
			}
			if (!d.isJrRepos()
					|| !(d.getNature() != null && d.getNature() instanceof JrFerie))
				return;
		}

		if (d.isHrSuppDay()) {
			Plage plage;
			if (this.hrSuppsValidates != null
					&& !this.hrSuppsValidates.isEmpty())
				for (HeurSupp hrSuppVal : hrSuppsValidates) {
					if (d.getDate().compareTo(hrSuppVal.getDateDebut()) == 0) {
						plage = new Plage(d.getDate(), hrSuppVal);
						d.getHrSupps().add(plage);
					}
				}
			d.sortPlages(d.getHrSupps());
		}

//		if (d.isEnMasse()) {
//			getMvtsByDate(d);
//			Utils.sortMvtsByTime(d.getMouvements());
//			Utils.filterMvtsByDelta(d.getMouvements(), DT_mvt_prm);
//		}

		List<Mouvement> list = new ArrayList<Mouvement>(d.getMouvements());
		Calendar d1 = new GregorianCalendar();
		Calendar d2 = new GregorianCalendar();

		if (list.size() % 2 != 0)
			list.remove(list.size() - 1);

		long somme = 0;
		boolean passSeuil = false;
		for (int i = 0; i < list.size(); i++) {

			Mouvement mvmnt1 = list.get(i);
			if (mvmnt1.isTraited())
				d.getMouvements().remove(mvmnt1);

			i++;

			Mouvement mvmnt2 = list.get(i);
			if (mvmnt2.isTraited())
				d.getMouvements().remove(mvmnt2);

			d1.setTime(mvmnt1.getDate());
			d2.setTime(mvmnt2.getDate());
			long diff = Utils.differenceDates(d1.getTime(), d2.getTime());
			if (d.isEnMasse()) {
				if (d.isJrRepos() && !(d.getNature() instanceof JrFerie)) {
					Hashtable<String, String> props = new Hashtable<String, String>();
					HeurSupp hrSuppEf = new HeurSuppImpl();
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1
							.getTime()));
					hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
					hrSuppEf.setDuree(Utils.differenceHeures(
							Utils.getHourDoubleFormat(d1.getTime()),
							Utils.getHourDoubleFormat(d2.getTime())));
					if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
						hrSuppEf.setCategory("C");
					} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
						hrSuppEf.setCategory("P");
					}
					props.clear();
					hrSuppEf.setAnnotation(annotations.get("HSUP50"));
					d.getHrSuppsEffectues().add(hrSuppEf);
					continue;
				}

				if (d.getNature() != null && d.getNature() instanceof JrFerie
						&& !d.isJrFerieHrSuppDay())
					return;

				if (d.getTmworked() > d.getP_tmcontract()) {
					somme += diff;
					Calendar c = new GregorianCalendar();
					c.setTime(d.getDate());
					c.set(Calendar.HOUR_OF_DAY, Sl_hsup_cond_prm.intValue());
					c.set(Calendar.MINUTE, Utils.decimal(Sl_hsup_cond_prm));
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					long tmcontract = Utils.double2Mills(d.getP_tmcontract());
					HeurSupp hrSuppEf = new HeurSuppImpl();
					hrSuppEf.setCollaborateur(user);
					hrSuppEf.setDateDebut(d.getDate());
					hrSuppEf.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					if (somme > tmcontract) {
						if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
							hrSuppEf.setCategory("C");
						} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
							hrSuppEf.setCategory("P");
						}
						if (c.compareTo(d1) >= 0 && c.compareTo(d2) <= 0) {
							d2.add(Calendar.MILLISECOND,
									-(int) (somme - tmcontract));
							hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d2
									.getTime()));
							hrSuppEf.setHeureFin(Sl_hsup_cond_prm);
							hrSuppEf.setDuree(Utils.differenceHeures(
									Utils.getHourDoubleFormat(d2.getTime()),
									Sl_hsup_cond_prm));
							hrSuppEf.setAnnotation(annotations.get("HSUP25"));
							d.getHrSuppsEffectues().add(hrSuppEf);
							hrSuppEf = new HeurSuppImpl();
							d2.add(Calendar.MILLISECOND,
									(int) (somme - tmcontract));
							hrSuppEf.setCollaborateur(user);
							hrSuppEf.setDateDebut(d.getDate());
							hrSuppEf.setStatut(MessageFactory
									.getMessage(EFFECTUE_STATUT));
							if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
								hrSuppEf.setCategory("C");
							} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
								hrSuppEf.setCategory("P");
							}
							hrSuppEf.setHeureDebut(Sl_hsup_cond_prm);
							hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2
									.getTime()));
							hrSuppEf.setDuree(Utils.differenceHeures(
									Sl_hsup_cond_prm,
									Utils.getHourDoubleFormat(d2.getTime())));
							hrSuppEf.setAnnotation(annotations.get("HSUP50"));
							d.getHrSuppsEffectues().add(hrSuppEf);
							passSeuil = true;
						} else if (passSeuil) {
							hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1
									.getTime()));
							hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2
									.getTime()));
							hrSuppEf.setDuree(Utils.differenceHeures(
									Utils.getHourDoubleFormat(d1.getTime()),
									Utils.getHourDoubleFormat(d2.getTime())));
							hrSuppEf.setAnnotation(annotations.get("HSUP50"));
							d.getHrSuppsEffectues().add(hrSuppEf);
						} else {
							d1.add(Calendar.MILLISECOND, (int) (tmcontract
									- somme + diff));
							hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1
									.getTime()));
							hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2
									.getTime()));
							hrSuppEf.setDuree(Utils.differenceHeures(
									Utils.getHourDoubleFormat(d1.getTime()),
									Utils.getHourDoubleFormat(d2.getTime())));
							hrSuppEf.setAnnotation(annotations.get("HSUP25"));
							d.getHrSuppsEffectues().add(hrSuppEf);
						}
					}
				}
				continue;
			}
			if (diff <= DT_hsp_prm * 60000)
				continue;
			// KLA : Ajout du test d.getPlanning()!=null
			if (d.getPlanning()!=null && (d.getPlanning().getCategHrsp() == 1
					|| d.getPlanning().getCategHrsp() == 0)) { // Planning-adm-agr-faraj
				HeurSupp hrSuppEf = new HeurSuppImpl();
				hrSuppEf.setCollaborateur(user);
				hrSuppEf.setDateDebut(d.getDate());
				hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
				hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
				hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
				hrSuppEf.setDuree(Utils.differenceHeures(
						Utils.getHourDoubleFormat(d1.getTime()),
						Utils.getHourDoubleFormat(d2.getTime())));
				if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
					hrSuppEf.setCategory("C");
					hrSuppEf.setAnnotation(annotations.get("HSUP50"));
				} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
					hrSuppEf.setCategory("P");
					hrSuppEf.setAnnotation(annotations.get("HSUP50"));
				}
				d.getHrSuppsEffectues().add(hrSuppEf);
				continue;
			}
			compareProgHrSupp(d, d1, d2, user);
			annomalie = new AnnomalieImpl();
			diff = Utils.differenceDates(d1.getTime(), d2.getTime());
			if (diff <= DT_hsp_prm * 60000)
				continue;
			if (!d1.equals(d2)) {
				HeurSupp h = getHrSuppInValidated(d1.getTime(), d2.getTime());
				if (h == null) {
					String heureSupp1 = Utils.toString(d1
							.get(Calendar.HOUR_OF_DAY))
							+ ":"
							+ Utils.toString(d1.get(Calendar.MINUTE)); // si
																		// heure
																		// ou
																		// minute
																		// < 10
																		// on
																		// ajout
																		// un 0
																		// en
																		// amont
																		// exple
																		// : h =
																		// 7 =>
																		// stringH
																		// = 07
					String heureSupp2 = Utils.toString(d2
							.get(Calendar.HOUR_OF_DAY))
							+ ":"
							+ Utils.toString(d2.get(Calendar.MINUTE));
					annomalie
							.setDescription("Non autoris� aux heures supplimentaire de '"
									+ heureSupp1 + "' � '" + heureSupp2 + "'");
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setUser(user);
					annomalie.setType("NAHS");
					annomalie.setMatricule("NATR-" + user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					try {
						anomaliesEffectues.add(annomalie);
					} catch (Exception e) {
						logger.error("> EXCEPTION - GenericException : " + e.getMessage());
					}
				}
			}
		}
	}

	private void saveHolidays(User user) throws Exception {
		Conge c1 = null;
		Conge c2 = null;
		int duree;
		int j;
		for (int i = 0; i < days.size(); i++) {
			if (!days.get(i).getLibelle().equals("conge")
					|| !days.get(i).getMvtsAllDay().isEmpty())
				continue;

			c1 = (Conge) days.get(i).getNature();
			duree = 1;
			j = i + 1;
			while (j < days.size()) {
				if (days.get(j).getLibelle().equals("conge")
						&& days.get(i).getMvtsAllDay().isEmpty()) {
					c2 = (Conge) days.get(j).getNature();
					if (c2.getTypeConge().getLibelle()
							.equalsIgnoreCase(c1.getTypeConge().getLibelle())) {
						duree++;
						j++;
					} else
						break;
				} else
					break;

			}

			Conge cEff = new CongeImpl();
			cEff.setCollaborateur(user);
			cEff.setTypeConge(c1.getTypeConge());
			cEff.setDateDebut(days.get(i).getDate());
			cEff.setDateReprise(days.get(j - 1).getDate());
			cEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			cEff.setNbrJour(duree);
			congesEffectues.add(cEff);
			i = j;
			// BeanUtils.copyProperties(c, cEff);
			// cEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			// if (c.getDateDebut().before(days.get(0).getDate())) {
			// int diff = getNbrJour(days.get(0).getDate(), c.getDateDebut(),
			// user);
			// cEff.setNbrJour(cEff.getNbrJour() - diff);
			// cEff.setDateDebut(days.get(0).getDate());
			// }
			// if (!c.getDateReprise().after(days.get(days.size() -
			// 1).getDate())) {
			// if (!jrRepos.isEmpty() && contains(cEff, jrRepos) > 0) {
			// cEff.setNbrJour(cEff.getNbrJour() - contains(cEff, jrRepos));
			// }
			// congeService.create(cEff);
			// } else {
			// int diff = getNbrJour(c.getDateReprise(), days.get(days.size() -
			// 1).getDate(), user);
			// cEff.setNbrJour(cEff.getNbrJour() - diff);
			// cEff.setDateReprise(days.get(days.size() - 1).getDate());
			// if (!jrRepos.isEmpty() && contains(cEff, jrRepos) > 0) {
			// cEff.setNbrJour(cEff.getNbrJour() - contains(cEff, jrRepos));
			// }
			// cEff.setId(0);
			// congeService.create(cEff);
			// break;
			// }
			// while((i < days.size()) &&
			// !c.getDateReprise().equals(days.get(i).getDate()))
			// i++;
			//

		}
	}

	private boolean hasBeenAbsent(Day day) {
		try {
			Annomalie annomalie = new AnnomalieImpl();
			if (day.getTmworked() > 0) {
				isCompDay(day);
				isRecpDay(day);
			} else {
				isAbsCompDay(day);
				isAbsRecupDay(day);
			}
			if (day.isJrAbsence()) {
				if (!day.isJrRepos()) {
					if (!day.isAbsCompensedDay()
							&& !day.isAbsRecuperedDay()) {
						AbsenceImpl absence = new AbsenceImpl();
						absence.setDateDebut(day.getDate());
						absence.setDuree(day.getP_tmcontract());
						absence.setDateReprise(day.getDate());
						absence.setCollaborateur(this.getUser());
						absence.setHeureReprise(new Double("23.59"));
						if (day.isHoraireNuit())
							absence.setHorNuit(true);
						else
							absence.setHorNuit(false);
						absence.setStatut(MessageFactory
								.getMessage(EFFECTUE_STATUT));
						List<Absence> l = getAbsencesIfValidated(absence);
						if (l == null || l.isEmpty()) {
							absence.setTypeAbsence(noJustifyAbsence);
							absence.setHeureReprise(0);
							if (!isInRecuperation(absence))
								day.getAbsencesEffectues().add(absence);
						} else {
							for (Absence ab : l) {
								if (ab == null)
									continue;
								ab.setStatut(MessageFactory
										.getMessage(EFFECTUE_STATUT));
								if (ab.getTypeAbsence().getLibelle() == null)
									continue;
								day.getAbsencesEffectues().add(ab);
							}
							absence.setTypeAbsence(noJustifyAbsence);
							Double duree = absence.minus(l);
							if (duree > 0) {
								absence.setDuree(duree);
								absence.setHeureReprise(0);
								day.getAbsencesEffectues().add(absence);
							}
						}
					}
				} else {
					if (day.isHrSuppDay()) {
						annomalie.setDateAnomalie(day.getDate());
						annomalie
								.setDescription("Absence le jour de "
										+ day.getLibelle()
										+ " avec une demande d'heures supplimentaires valid�es");
						annomalie.setUser(this.getUser());
						annomalie.setType("AJRHV");
						annomalie.setMatricule("JF-" + this.getUser().getMatricule());
						if (day.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					} else if (day.isCompensedDay()) {
						annomalie.setDateAnomalie(day.getDate());
						annomalie
								.setDescription("Absence le jour de "
										+ day.getLibelle()
										+ " avec une demande de compensation valid�e");
						annomalie.setUser(this.getUser());
						annomalie.setType("AJRCV");
						annomalie.setMatricule("JF-" + this.getUser().getMatricule());
						if (day.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					} else if (day.isRecuperedDay()) {
						annomalie.setDateAnomalie(day.getDate());
						annomalie
								.setDescription("Absence le jour de "
										+ day.getLibelle()
										+ " avec une demande de recuperation valid�e");
						annomalie.setUser(this.getUser());
						annomalie.setType("AJRRV");
						annomalie.setMatricule("JF-" + this.getUser().getMatricule());
						if (day.isHoraireNuit())
							annomalie.setHorNuit(true);
						anomaliesEffectues.add(annomalie);
					}
				}
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean isJrRepos(Day day) throws Exception {
		if (day.isJrRepos())
			return true;
		Object o = Utils.isJrFerie(day.getDate(), (List) this.jrFeries);
		if (o != null) {
			day.setLibelle(((JrFerie) o).getLibelle());
			day.setNature(o);
			day.setJrRepos(true);
			return true;
		}
		o = Utils.isHolidays(day.getDate(), (List) this.congesValidates);
		if (o != null) {
			day.setLibelle("conge");
			day.setNature(o);
			day.setJrRepos(true);
			return true;
		}
		return false;
	}

	private void isAbsCompDay(Day day) throws Exception {
		if (this.compValidates == null || this.compValidates.isEmpty())
			return;
		Calendar cal1 = new GregorianCalendar();
		Calendar cal = new GregorianCalendar();
		cal.setTime(day.getDate());
		for (Compensation comp : compValidates) {
			cal1.setTime(comp.getAbsence().getDateDebut());
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal) == 0) {
				if (comp.getAbsence().getHeureDebut() == comp.getAbsence()
						.getHeureReprise()) {
					if (day.isJrAbsence()) {
						Absence abs = new AbsenceImpl();
						BeanUtils.copyProperties(comp.getAbsence(), abs);
						abs.setStatut(MessageFactory
								.getMessage(EFFECTUE_STATUT));
						if (day.isHoraireNuit())
							abs.setHorNuit(true);
						abs.setId(0);
						day.getAbsencesEffectues().add(abs);

						Compensation compEff = new CompensationImpl();
						BeanUtils.copyProperties(comp, compEff);
						compEff.setId(0);
						compEff.setStatut(MessageFactory
								.getMessage(EFFECTUE_STATUT));
						compEff.setAbsence(abs);
						compsEffectues.add(compEff);
					}
					day.setAbsCompensedDay(true);
				}
				return;
			}
		}
		day.setAbsCompensedDay(false);
	}

	private void isAbsRecupDay(Day day) throws Exception {
		if (this.recupsValidates == null || this.recupsValidates.isEmpty())
			return;
		Calendar cal1 = new GregorianCalendar();
		Calendar cal = new GregorianCalendar();
		cal.setTime(day.getDate());
		for (Recuperation recup : recupsValidates) {
			cal1.setTime(recup.getAbsence().getDateDebut());
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal) == 0) {
				if (recup.getAbsence().getHeureDebut() == recup.getAbsence()
						.getHeureReprise())
					day.setAbsRecuperedDay(true);
				return;
			}
		}
		day.setAbsRecuperedDay(false);
	}

	private void isCompDay(Day day) throws Exception {
		if (this.compValidates == null || this.compValidates.isEmpty())
			return;
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		cal1.setTime(day.getDate());
		Double diff = 0.0;
		for (Compensation comp : compValidates) {
			cal2.setTime(comp.getDateCom());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal2) == 0) {
				Annomalie annomalie = new AnnomalieImpl();
				annomalie.setDateAnomalie(comp.getAbsence().getDateDebut());
				annomalie.setUser(this.getUser());
				annomalie.setType("NRT");
				diff = Utils.differenceHeures(comp.getTmContract(),
						day.getTmworked());
				if (day.getTmworked() < comp.getTmContract()) {
					annomalie
							.setDescription("annomalie non respect de temps contractuel avec '"
									+ diff
									+ "' de moins lors de 'compensation'!!");
					annomalie.setMatricule("JCM-" + this.getUser().getMatricule());
					anomaliesEffectues.add(annomalie);
				}
				if (day.getTmworked() > comp.getTmContract()) {
					HeurSupp hrSuppEff = new HeurSuppImpl();
					hrSuppEff.setCollaborateur(this.getUser());
					hrSuppEff.setDateDebut(day.getDate());
					hrSuppEff.setDuree(diff);
					hrSuppEff.setAnnotation(annotations.get("HSUP100"));
					hrSuppEff.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					hrSuppEff.setMotif("Compensation");
					day.getHrSuppsEffectues().add(hrSuppEff);
				}
				day.setCompensedDay(true);
				return;
			}
		}
	}

	private void isRecpDay(Day day) throws Exception {
		if (this.recupsValidates == null || this.recupsValidates.isEmpty())
			return;

		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		cal1.setTime(day.getDate());
		Double diff = 0.0;
		for (Recuperation recup : recupsValidates) {
			cal2.setTime(recup.getDateRec());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal2) == 0) {
				diff = Utils.differenceHeures(recup.getAbsence().getDuree(),
						day.getTmworked());
				if (day.getTmworked() < recup.getAbsence().getDuree()) {
					Annomalie annomalie = new AnnomalieImpl();
					annomalie
							.setDateAnomalie(recup.getAbsence().getDateDebut());
					annomalie
							.setDescription("annomalie non respect de temps contractuel avec '"
									+ diff
									+ "' de moins lors de 'recuperation'!!");
					annomalie.setUser(this.getUser());
					annomalie.setType("NRT");
					if (day.isHoraireNuit())
						annomalie.setHorNuit(true);
					annomalie.setMatricule("JRC-" + this.getUser().getMatricule());
					anomaliesEffectues.add(annomalie);
				}
				if (day.getTmworked() > recup.getAbsence().getDuree()) {
					HeurSupp hrSuppEff = new HeurSuppImpl();
					hrSuppEff.setCollaborateur(this.getUser());
					hrSuppEff.setDateDebut(day.getDate());
					hrSuppEff.setDuree(diff);
					hrSuppEff.setAnnotation(annotations.get("HSUP100"));
					hrSuppEff.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					hrSuppEff.setMotif("Compensation");
					day.getHrSuppsEffectues().add(hrSuppEff);
				}
				return;
			}
		}
	}

	private boolean isInRecuperation(Absence abs) {
		if (this.absRecs == null || this.absRecs.isEmpty())
			return false;

		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		cal1.setTime(abs.getDateDebut());
		Recuperation recbis = null;
		for (Recuperation rec : absRecs) {
			cal2.setTime(rec.getAbsence().getDateDebut());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal2) == 0) {
				recbis = rec;
				break;
			}
		}
		if (recbis != null) {
			absRecs.remove(recbis);
			return true;
		}
		return false;
	}

	private int stateAbsComp(Day day, Double duree) throws Exception {
		if (this.compValidates == null || this.compValidates.isEmpty())
			return -1;
		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();
		Hashtable<String, Object> props = new Hashtable<String, Object>();

		for (Compensation comp : compValidates) {
			cal1.setTime(comp.getAbsence().getDateDebut());
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			cal2.setTime(day.getDate());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal2) == 0) {
				props.put("type", "NRT");
				props.put("dateAnomalie", comp.getDateCom());
				props.put("user", this.getUser());
				List list = null;
				synchronized(annoService) {
					list = annoService.read(props);
				}
				Double d = null;
				if (list != null && !list.isEmpty()) {
					Annomalie anomalie = (Annomalie) list.get(0);
					StringTokenizer tokenizer = new java.util.StringTokenizer(
							anomalie.getDescription(), "'");
					tokenizer.nextToken();
					String diff = tokenizer.nextToken();
					if (anomalie.getMatricule().substring(0, 3).equals("JCP"))
						d = Utils.additionHeures(comp.getTmContract(),
								new Double(diff));
					else if (anomalie.getMatricule().substring(0, 3)
							.equals("JCM"))
						d = Utils.differenceHeures(comp.getTmContract(),
								new Double(diff));
				} else
					d = comp.getTmContract();
				if (d == 0) {
					return -1;
				} else if (duree > d) {
					// comp.getAbsence().setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					// absenceService.update(comp.getAbsence());
					// comp.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					// compensationService.update(comp);
					return 1;
				} else {
					Absence abs = new AbsenceImpl();
					BeanUtils.copyProperties(comp.getAbsence(), abs);
					abs.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					if (day.isHoraireNuit())
						abs.setHorNuit(true);
					abs.setId(0);
					day.getAbsencesEffectues().add(abs);
					Compensation compEff = new CompensationImpl();
					BeanUtils.copyProperties(comp, compEff);
					compEff.setId(0);
					compEff.setStatut(MessageFactory
							.getMessage(EFFECTUE_STATUT));
					compEff.setAbsence(abs);
					compsEffectues.add(compEff);
					return 2;
				}
			}
		}
		return -1;
	}

	private HeurSupp getHrSuppInValidated(Date d1, Date d2) {
		if (this.hrSuppsValidates == null || this.hrSuppsValidates.isEmpty())
			return null;
		for (HeurSupp h : hrSuppsValidates) {
			if (!d1.before(Utils.getCompleteDate(h.getDateDebut(),
					h.getHeureDebut()))
					&& !d1.after(Utils.getCompleteDate(h.getDateDebut(),
							h.getHeureFin()))
					&& !d2.before(Utils.getCompleteDate(h.getDateDebut(),
							h.getHeureDebut()))
					&& !d2.after(Utils.getCompleteDate(h.getDateDebut(),
							h.getHeureFin())))
				return h;
			else 
				return null;
		}
		return null;
	}

	private List<Absence> getAbsencesIfValidated(Absence abEff) {
		if (this.absencesValidates == null || this.absencesValidates.isEmpty())
			return null;

		List<Absence> l = new ArrayList<Absence>();
		for (Absence av : absencesValidates) {
			List<Absence> res = ((AbsenceImpl) av).intersect(abEff);
			if (res != null)
				l.addAll(res);
		}
		return l;
	}

	private boolean isJrFerieHsuppEnabled(User u, Date d) throws Exception {
		Calendar cal = new GregorianCalendar();
		cal.setTime(d);
		cal.add(Calendar.DAY_OF_MONTH, -1);

		CommonCriteria crit = new CommonCriteria();
		crit.setMatriculeInf(u.getMatricule());
		crit.setMatriculeSup(u.getMatricule());
		crit.setDateDebut(cal.getTime());
		crit.setDateFin(cal.getTime());

		int count = 0;
		synchronized (mvmntService) {
			count = mvmntService.countWorkedDaysByCriteria(crit);
		}
		if (count > 0)
			return true;
		crit.setDateFin(cal.getTime());
		cal.setTime(u.getDateEmb());
		crit.setDateDebut(cal.getTime());
		synchronized (mvmntService) {
			count = mvmntService.countWorkedDaysByCriteria(crit);
		}
		if (count >= 13)
			return true;
		return false;
	}
	
	private void addReasultsLists(Day d) {
		this.absencesEffectues.addAll(d.getAbsencesEffectues());
		this.retardsEffectues.addAll(d.getRetardsEffectues());
		this.interruptionsEffectues.addAll(d.getInterruptionsEffectues());
		this.hrSuppsEffectues.addAll(d.getHrSuppsEffectues());
	}
	
	private void setResultsLists(Day d) {
		if (this.absencesEffectues != null && !this.absencesEffectues.isEmpty())
			for (Absence a: this.absencesEffectues) 
				if (a.getDateDebut().compareTo(d.getDate()) <= 0 && a.getDateReprise().compareTo(d.getDate()) >= 0)
					d.getAbsencesEffectues().add(a);
		
		if (this.retardsEffectues != null && !this.retardsEffectues.isEmpty())
			for (Retard r: this.retardsEffectues)
				if (r.getDate().compareTo(d.getDate()) == 0)
					d.getRetardsEffectues().add(r);
		if (this.interruptionsEffectues != null && !this.interruptionsEffectues.isEmpty())
			for (Interruption i: this.interruptionsEffectues)
				if (i.getDate().compareTo(d.getDate()) == 0)
					d.getInterruptionsEffectues().add(i);
		if (this.interruptionsEffectues != null && !this.interruptionsEffectues.isEmpty())
			for (HeurSupp hs: this.hrSuppsEffectues)
				if (hs.getDateDebut().compareTo(d.getDate()) == 0)
					d.getHrSuppsEffectues().add(hs);
	}
	
	public void run() {
		try {
			/**
			 * Localiser le planning pour chaque journée affecté au
			 * collaborateur
			 */
			Planning pl = null;
			List<Day> copyDays = new ArrayList<Day>(days.size());
			Day d = null;
			for (Day day : days) {
				d = new DayData(day.getDate(), day.getRptHNuit(), day.getSmartHor());
				copyDays.add(d);
				d.setCumulTrait(false);
				d.setJrFerieHrSuppDay(isJrFerieHsuppEnabled(this.getUser(), d.getDate()));
				if (this.getUser().getDateEmb() != null
						&& d.getDate().before(this.getUser().getDateEmb())
						|| (this.getUser().getDateQuit() != null && d.getDate().after(
								this.getUser().getDateQuit()))) {
					continue;
				}
				synchronized (mvmntService) {
					d.setMouvements(StringUtils.isEmpty(this.getUser().getBadge())?
							mvmntService.readByMatriculeAndDate(this.getUser().getMatricule(), d.getDate()):
							mvmntService.readByBadgeAndDate(this.getUser().getBadge(), d.getDate()));
				}
				Utils.sortMvtsByTime(d.getMouvements());
				Utils.filterMvtsByDelta(d.getMouvements(), DT_mvt_prm);
				pl = Utils.findAffectPlanning(d.getDate(), this.getUser());
				pl = Utils.deproxy(pl, Planning.class);
				d.setPlanning(pl);
			}
			days = copyDays;
			if (pl == null)
				return;
			
			for (Day day : days) {
				if (day.isHoraireNuit())
					if (Rpt_hnuit_prm.equals("no")) {
						Day next = day.getNextDay(days);
						if (next != null && next.isHoraireNuit()
								&& !day.getMouvements().isEmpty()
								&& !next.getHrFixes().isEmpty()) {
							Calendar cal = new GregorianCalendar();
							cal.setTime(next.getHrFixes().get(0).getDebut()
									.getTime());
							cal.add(Calendar.MINUTE, -DT_HNuit_prm);
							Calendar laDate = new GregorianCalendar();
	
							List<Mouvement> l = new ArrayList<Mouvement>();
							for (Mouvement m : day.getMouvements()) {
								laDate.setTime(m.getDate());
								if (laDate.compareTo(cal) > 0) {
									l.add(m);
									m.setTraited(false);
									next.getMouvements().add(m);
								}
							}
							day.getMouvements().removeAll(l);
						}
					} else if (Rpt_hnuit_prm.equals("yes")) {
						Day next = day.getNextDay(days);
						if (next != null && day.isHoraireNuit()
								&& !next.getMouvements().isEmpty()
								&& !day.getHrFixes().isEmpty()) {
							Calendar cal = new GregorianCalendar();
							cal.setTime(day.getHrFixes()
									.get(day.getHrFixes().size() - 1).getFin()
									.getTime());
							cal.add(Calendar.MINUTE, DT_HNuit_prm);
							Calendar laDate = new GregorianCalendar();
	
							List<Mouvement> l = new ArrayList<Mouvement>();
							for (Mouvement m : next.getMouvements()) {
								laDate.setTime(m.getDate());
								if (laDate.compareTo(cal) < 0) {
									l.add(m);
									m.setTraited(false);
									day.getMouvements().add(m);
								}
							}
							next.getMouvements().removeAll(l);
						}
					}
				}
			CommonCriteria crit = new CommonCriteria();
			crit.setMatriculeInf(this.getUser().getMatricule());
			crit.setMatriculeSup(this.getUser().getMatricule());
			Day sd = days.get(0);
			Day fd = days.get(days.size() - 1);
			List mvnts = null;
			if (Rpt_hnuit_prm.equals("yes") && fd.isHoraireNuit()
					&& !fd.getHrFixes().isEmpty()) {
				Plage theLast = fd.getHrFixes().get(fd.getHrFixes().size() - 1);
				Calendar d1 = new GregorianCalendar();
				d1.setTime(fd.getDate());
				d1.add(Calendar.DAY_OF_MONTH, 1);
				d1.set(Calendar.HOUR_OF_DAY, 0);
				d1.set(Calendar.MINUTE, 0);
				d1.set(Calendar.SECOND, 0);
				d1.set(Calendar.MILLISECOND, 0);
				crit.setDateDebut(d1.getTime());
				d1.setTime(theLast.getFin().getTime());
				d1.add(Calendar.MINUTE, DT_HNuit_prm);
				crit.setDateFin(d1.getTime());
				synchronized (mvmntService) {
					mvnts = mvmntService.readByCriteria(crit);
				}
				if (mvnts != null && !mvnts.isEmpty())
					fd.getMouvements().addAll(mvnts);
				synchronized(annoService) {
					annoService.removeCriteria(crit);
				}
			} else if (Rpt_hnuit_prm.equals("no") && sd.isHoraireNuit()
					&& !sd.getHrFixes().isEmpty()) {
				Plage thefirst = sd.getHrFixes().get(0);
				Calendar d1 = new GregorianCalendar();
				d1.setTime(thefirst.getDebut().getTime());
				d1.add(Calendar.MINUTE, -DT_HNuit_prm);
				crit.setDateDebut(d1.getTime());
				d1.setTime(sd.getDate());
				d1.set(Calendar.HOUR_OF_DAY, 0);
				d1.set(Calendar.MINUTE, 0);
				d1.set(Calendar.SECOND, 0);
				d1.set(Calendar.MILLISECOND, 0);
				crit.setDateFin(d1.getTime());
				synchronized (mvmntService) {
					mvnts = mvmntService.readByCriteria(crit);
				}
				if (mvnts != null && !mvnts.isEmpty())
					sd.getMouvements().addAll(mvnts);
				synchronized(annoService) {
					annoService.removeCriteria(crit);
				}
			}

			/**
			 * traiter les mouvements jour par jour suivant l'horaire
			 */
			DayData dayData;
			this.setDayDatas(new ArrayList<DayData>(days.size()));
			for (Day day : days) {
				if (plAtribute != null && !plAtribute.equals(day.getPlanning()))
					continue;
				
				if (this.getUser().getDateEmb() != null
						&& day.getDate().before(this.getUser().getDateEmb())
						|| (this.getUser().getDateQuit() != null && day.getDate().after(
								this.getUser().getDateQuit()))) {
					continue;
				}
				
				Utils.sortMvtsByTime(day.getMouvements());
				Utils.filterMvtsByDelta(day.getMouvements(), DT_mvt_prm);
				
				if (day.getPlanning() != null) {
					if (!Utils.inCyclicPlg(day.getDate(), day.getPlanning())) {
						Annomalie annomalie = new AnnomalieImpl();
						annomalie.setDateAnomalie(day.getDate());
						annomalie.setDescription("Planning non affecte");
						annomalie.setUser(this.getUser());
						annomalie.setType("PNA");
						annomalie.setMatricule("PNA-" + this.getUser().getMatricule());
						anomaliesEffectues.add(annomalie);
						continue;
					} else if (day.getLibelle().equals("dynamique")
								&& day.getHoraire() == null
								&& (days.indexOf(day) < days.size() - 1)) {
							Annomalie annomalie = new AnnomalieImpl();
							annomalie.setDateAnomalie(day.getDate());
							annomalie
									.setDescription("Pointages insuffisants pour fixer l'horaire dynamique");
							annomalie.setUser(this.getUser());
							annomalie.setType("PTIN");
							annomalie.setMatricule("PTIN-"
									+ this.getUser().getMatricule());
							anomaliesEffectues.add(annomalie);
							continue;
						}
				} else {
					Annomalie annomalie = new AnnomalieImpl();
					annomalie.setDateAnomalie(day.getDate());
					annomalie.setDescription("Planning non affecte");
					annomalie.setUser(this.getUser());
					annomalie.setType("PNA");
					annomalie.setMatricule("PNA-" + this.getUser().getMatricule());
					anomaliesEffectues.add(annomalie);
					continue;
				}
				
				if (this.hrSuppsValidates != null
						&& !this.hrSuppsValidates.isEmpty()
						&& Utils.isHrSuppDay(day.getDate(),
								(List) this.hrSuppsValidates) != null) {
					day.setHrSuppDay(true);
				}
				isJrRepos(day);
				day.calculTpConract();
				if (type.equals("processing")) {
					if (!hasBeenAbsent(day)) {
						traiterParHoraire(day, this.getUser());
						traiterHSupp(day, this.getUser());
						day.getMouvements().clear();
						day.getMouvements().addAll(day.getMvtsAllDay());
					}
					addReasultsLists(day);
				} else if (type.equals("reporting")) 
					setResultsLists(day);
				
				
				dayData = (DayData)day;
				dayData.rollupsEngine();
				this.getDayDatas().add(dayData);
			}

			/**
			 * enregistre les jours de congé effectués
			 */
			saveHolidays(this.getUser());

		} catch (BeansException e) {
			// TODO Auto-generated catch block
			logger.error("> EXCEPTION - BeansException : " + e.getMessage());
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			logger.error("> EXCEPTION - NumberFormatException : " + e.getMessage());
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			logger.error("> EXCEPTION - ClassCastException : " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error("> EXCEPTION - GenericException" + e.getMessage());
			e.printStackTrace();
		}
	}

	public void setJrFeries(Collection<JrFerie> jrFeries) {
		this.jrFeries = jrFeries;
	}

	public void setDays(List<Day> days) {
		this.days = days;
	}

	public void setNoJustifyAbsence(TypeAbsence noJustifyAbsence) {
		this.noJustifyAbsence = noJustifyAbsence;
	}

	public void setDT_mvt_prm(long dT_mvt_prm) {
		DT_mvt_prm = dT_mvt_prm;
	}

	public void setDT_hsp_prm(int dT_hsp_prm) {
		DT_hsp_prm = dT_hsp_prm;
	}

	public void setDT_HNuit_prm(int dT_HNuit_prm) {
		DT_HNuit_prm = dT_HNuit_prm;
	}

	public void setDT_pause_prm(double dT_pause_prm) {
		DT_pause_prm = dT_pause_prm;
	}

	public void setRpt_hnuit_prm(String rpt_hnuit_prm) {
		Rpt_hnuit_prm = rpt_hnuit_prm;
	}

	public void setAno_TJR(String ano_TJR) {
		Ano_TJR = ano_TJR;
	}

	public void setAno_TJF(String ano_TJF) {
		Ano_TJF = ano_TJF;
	}

	public void setSl_hsup_cond_prm(Double sl_hsup_cond_prm) {
		Sl_hsup_cond_prm = sl_hsup_cond_prm;
	}

	public void setAnnotations(Map<String, Annotation> annotations) {
		this.annotations = Collections.synchronizedMap(annotations);
	}

	public void setPlAtribute(Planning plAtribute) {
		this.plAtribute = plAtribute;
	}


	public Collection<Absence> getAbsencesEffectues() {
		return absencesEffectues;
	}


	public void setAbsencesValidates(Collection<Absence> absencesValidates) {
		this.absencesValidates = absencesValidates;
	}


	public Collection<Compensation> getCompsEffectues() {
		return compsEffectues;
	}

	public Collection<HeurSupp> getHrSuppsEffectues() {
		return hrSuppsEffectues;
	}


	public void setCongesValidates(Collection<Conge> congesValidates) {
		this.congesValidates = congesValidates;
	}


	public void setRecupsValidates(Collection<Recuperation> recupsValidates) {
		this.recupsValidates = recupsValidates;
	}


	public void setCompValidates(Collection<Compensation> compValidates) {
		this.compValidates = compValidates;
	}



	public void setHrSuppsValidates(Collection<HeurSupp> hrSuppsValidates) {
		this.hrSuppsValidates = hrSuppsValidates;
	}


	public Collection<AbsenceEff> getAbsenceEffsEffectues() {
		return absenceEffsEffectues;
	}


	public Collection<Conge> getCongesEffectues() {
		return congesEffectues;
	}


	public Collection<Retard> getRetardsEffectues() {
		return retardsEffectues;
	}


	public Collection<Interruption> getInterruptionsEffectues() {
		return interruptionsEffectues;
	}


	public Collection<Annomalie> getAnomaliesEffectues() {
		return anomaliesEffectues;
	}


	public void setAbsRecs(List<Recuperation> absRecs) {
		this.absRecs = absRecs;
	}


	public void setAnnoService(AnnomalieManageableService annoService) {
		this.annoService = annoService;
	}


	public void setMvmntService(MouvementManageableService mvmntService) {
		this.mvmntService = mvmntService;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setAbsencesEffectues(Collection<Absence> absencesEffectues) {
		this.absencesEffectues = absencesEffectues;
	}

	public void setInterruptionsEffectues(
			Collection<Interruption> interruptionsEffectues) {
		this.interruptionsEffectues = interruptionsEffectues;
	}

	public void setHrSuppsEffectues(Collection<HeurSupp> hrSuppsEffectues) {
		this.hrSuppsEffectues = hrSuppsEffectues;
	}

	public void setCongesEffectues(Collection<Conge> congesEffectues) {
		this.congesEffectues = congesEffectues;
	}

	public void setAnomaliesEffectues(Collection<Annomalie> anomaliesEffectues) {
		this.anomaliesEffectues = anomaliesEffectues;
	}

	public void setRetardsEffectues(Collection<Retard> retardsEffectues) {
		this.retardsEffectues = retardsEffectues;
	}
	

}
