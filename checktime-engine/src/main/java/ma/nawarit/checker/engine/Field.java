package ma.nawarit.checker.engine;

import java.io.Serializable;

public class Field implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean check;
	private String label;
	private String operator;
	private String value;
	
	
	public Field(boolean check, String label, String operator, String value) {
		super();
		this.check = check;
		this.label = label;
		this.operator = operator;
		this.value = value;
	}
	
	public boolean isCheck() {
		return check;
	}
	public Boolean isChecked() {
		return check;
	}
	public void setCheck(boolean check) {
		this.check = check;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
