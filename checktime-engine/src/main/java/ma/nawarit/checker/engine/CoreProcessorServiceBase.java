package ma.nawarit.checker.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.DynPlaDetail;
import ma.nawarit.checker.configuration.DynPlaDetailImpl;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.PlanningImpl;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.DynPlaDetailManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceEffManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;
import ma.nawarit.checker.suivi.crud.CompensationManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.HeurSuppManageableService;
import ma.nawarit.checker.suivi.crud.InterruptionManageableService;
import ma.nawarit.checker.suivi.crud.RecuperationManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


public class CoreProcessorServiceBase implements CoreProcessorService{
	
	private static final Logger logger = Logger.getLogger(CoreProcessorServiceBase.class);
	// Services
	private UserManageableService userService;
	private AnnomalieManageableService annoService;
	private AdvParamManageableService advParamService;
	private TypeAbsenceManageableService typeAbsenceService;
	private MouvementManageableService mvmntService;
	private JrFerieManageableService jrFerieService;
	private DynPlaDetailManageableService dynPlaDetailService;
	
	// Services From ProcessTask
	

	private AnnotationManageableService annotService;
	private InterruptionManageableService interService;
	private CongeManageableService congeService;
	private AbsenceManageableService absenceService;
	private AbsenceEffManageableService absenceEffService;
	private CompensationManageableService compensationService;
	private RecuperationManageableService recuperationService;
	private RetardManageableService retardService;
	private HeurSuppManageableService heurSuppService;
	
	// Task Executor
	private ThreadPoolTaskExecutor taskExecutor;

	
	// Global Attributes
	private List<Day> days = new ArrayList<Day>();
	private TypeAbsence noJustifyAbsence;
	private boolean liveState = false;
	private Collection<User> users = null;
	private Collection<JrFerie> jrFeries = null;
	private Collection<Mouvement> mvmnts = null;
	private CommonCriteria userCriteria;
	private Planning plAtribute = null;
	private Map<String, Annotation> annotMap;

	private final String VALIDATE_STATUT = "validate";
	private final String EFFECTUE_STATUT = "effectue";
	private final String NOJUSTIFY= "noJustify";
	
	private long DT_mvt_prm;
	private int DT_msFocus_prm; 
	private int DT_hsp_prm;
	private int DT_out_prm;
	private int DT_HNuit_prm;
	private double DT_pause_prm;
	private String Rpt_hnuit_prm;
	private String Ano_TJR;
	private String Ano_TJF;
	private Double Sl_hsup_cond_prm;
	private boolean reporting;
	private boolean processing;
	
	
    /**
     * Gets the shared instance of this Class
     *
     * @return the shared AnnoTrucker instance.
     */
    public  CoreProcessorServiceBase (){	
    	
    }
    
    /**
     * methode initialisation des parametres 
     * @param mvmnts
     * @param users
     */
    private void initAllParams() {
    	try {
	    	 	
	    	this.jrFeries = jrFerieService.readAll();
	    	Hashtable<String, String> typeAbsProps = new Hashtable<String, String>();
			typeAbsProps.put("code", MessageFactory.getMessage(NOJUSTIFY));
	    	List<TypeAbsence> noJustifyAbsences = typeAbsenceService.read(typeAbsProps);
	    	if (noJustifyAbsences != null && !noJustifyAbsences.isEmpty())
	    		this.noJustifyAbsence = (TypeAbsence)noJustifyAbsences.get(0);
    		this.Rpt_hnuit_prm = advParamService.getValueByName("report_horNuit");
			this.DT_pause_prm = new Double(advParamService.getValueByName("DeltaT_pause"));
			this.Sl_hsup_cond_prm = new Double(advParamService.getValueByName("seuil_hrSupp_cond"));
			this.DT_out_prm = new Integer(advParamService.getValueByName("DeltaT_Out"));
			this.DT_HNuit_prm = new Integer(advParamService.getValueByName("DeltaT_HorNuit"));
			this.DT_hsp_prm = new Integer(advParamService.getValueByName("DeltaT_Hsp"));
			this.DT_mvt_prm = new Integer(advParamService.getValueByName("DeltaT_Mvt"))  * 60000;
			this.DT_msFocus_prm = new Integer(advParamService.getValueByName("DeltaT_MasseFocus"))  * 60000;
			this.Ano_TJR = advParamService.getValueByName("anoTrJrRepos");
			this.Ano_TJF = advParamService.getValueByName("anoTrJrFerie");
				
		} catch (Exception e) {
			logger.error(e.getMessage());
	    	logger.error(e.getMessage());
	    	e.printStackTrace();
		}
	}
    

    
  
    
    /**
     * on construit la liste des jours à traiter
     * @param debut
     * @param fin
     */
    private void buildDaysList() {
    	try {
    		Calendar d1 = new GregorianCalendar();
    		Calendar d2 = new GregorianCalendar();
    		d1.setTime(userCriteria.getDateDebut());
	    	d1.set(Calendar.HOUR_OF_DAY, 0);
	    	d1.set(Calendar.MINUTE, 0);
	    	d1.set(Calendar.SECOND, 0);
	    	d1.set(Calendar.MILLISECOND, 0);
	    	
	    	d2.setTime(userCriteria.getDateFin());
	    	d2.set(Calendar.HOUR_OF_DAY, 0);
	    	d2.set(Calendar.MINUTE, 0);
	    	d2.set(Calendar.SECOND, 0);
	    	d2.set(Calendar.MILLISECOND, 0); 
	    	
    		days = new ArrayList<Day>();
    		Day day;
    		while (d1.compareTo(d2) <= 0) {
    			day = new Day(d1.getTime(), Rpt_hnuit_prm, null);
    			days.add(day);
    			d1.add(Calendar.DAY_OF_MONTH, 1);
    		}
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getMessage());
		}
    }
    
 
	public void processOnline(CommonCriteria criteria, Planning plAtribute) {
		this.liveState =true;
		process(criteria, plAtribute);
		
	}
	private boolean checkUserCriteria(){
		try {
			if (this.userCriteria.getDateDebut() == null 
					|| this.userCriteria.getDateFin() == null) // dateDebut et dateFin obligatoires
				return false;
			this.users = this.userService.readByCriteria(this.userCriteria);   
			if ((this.users == null || this.users.isEmpty()))				
				return false;
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			return false;
		}
		
	}
	private void cleanOldData(){
		try {	
			userCriteria.setStatut(MessageFactory.getMessage(this.EFFECTUE_STATUT));
			annoService.removeCriteria(this.userCriteria);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
	private void detectSmartHoraire(){
		try {
			CommonCriteria crit = new CommonCriteria();
			Map<Planning, List<Integer>> indexs;
			Planning pl = null;
			List<Mouvement> mvts;
			List<Day> ds = new ArrayList<Day>(days);
			int n = 0;
			DynPlaDetail dynPlaDetail;
			for (Day d: ds) {
				crit.setDateDebut(d.getDate());
				crit.setDateFin(d.getDate());
				indexs = new HashMap<Planning, List<Integer>>();
				for (User user: users) {
					pl = Utils.findAffectPlanning(d.getDate(), user);
					pl = Utils.deproxy(pl, Planning.class);
					if (pl == null || !Utils.inCyclicPlg(d.getDate(), pl))
						continue;
					d.setPlanning(pl);
					if (d.isEnMasse()) {
						if (indexs.containsKey(pl))
							indexs.get(pl).add(user.getId());
						else {
							indexs.put(pl, new ArrayList(user.getId()));
							indexs.get(pl).add(user.getId());
						}
					}
					d.cleanDay();
				}
				Set<Planning> keys = indexs.keySet();
				for (Planning pla: keys) {
					mvmnts = new ArrayList<Mouvement>();
					List<Integer> inds =indexs.get(pla);
					int eff = 0;
					for (int i: inds) {	
						crit.getIndexs().clear();	
						crit.getIndexs().add(i);
						mvts = mvmntService.readByCriteria(crit);
						if (mvts != null && !mvts.isEmpty())
							Utils.filterMvtsByDelta(mvts, DT_mvt_prm);
						else
							continue;
						mvmnts.addAll(mvts);
						eff++;
					}		
					if (!mvmnts.isEmpty()) {
						List<Date> inOut = Utils.getInOuts((List)mvmnts, DT_msFocus_prm, eff);
						if (!inOut.isEmpty()){ 
							days.get(n).getSmartHor().put(pla, inOut);
							//########### ?????? TEST AJOUTE PAR KHALID
							//d.setPlanning(pla);
							// clean Dyn_Pla_Detail
							if (!liveState) {
								logger.error("DETECT DYN PLANNING ...");
								dynPlaDetailService.removeByCriteria(d.getDate(), pla.getId());
								for (int i = 0; i < inOut.size(); i++) {
									if(i%2 == 1) {
										dynPlaDetail = new DynPlaDetailImpl();
										dynPlaDetail.setDatePla(d.getDate());
										dynPlaDetail.setPlanning(pla);
										dynPlaDetail.setDebut(Utils.getHourDoubleFormat(inOut.get(i - 1)));
										dynPlaDetail.setFin(Utils.getHourDoubleFormat(inOut.get(i)));
										dynPlaDetailService.create(dynPlaDetail);
									}
								}
							}
						}
					}
				}
				n++;
			}
		} catch (ClassCastException e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
		}
	}
	
	private void loadTimeFactors(ProcessTask processTask) {
		try {
			userCriteria.setStatut(MessageFactory
					.getMessage(this.VALIDATE_STATUT));
			processTask.setHrSuppsValidates(heurSuppService.readByCriteria(userCriteria));
			processTask.setCongesValidates(congeService.readByCriteria(userCriteria));
			processTask.setAbsencesValidates(absenceService.readByCriteria(userCriteria));
			processTask.setRecupsValidates(recuperationService.readByCriteria(userCriteria));
			processTask.setCompValidates(compensationService.readByCriteria(userCriteria));
			processTask.setAbsRecs(recuperationService.readIfAbsIn(userCriteria));
		} catch (Exception e) {
			logger.error("> EXCEPTION - GenericException : " + e.getMessage());
		}
	}
	
	private void loadAllAnnots() {
		try {
			List<Annotation> annos = annotService.readAll();
			annotMap = new HashMap<String, Annotation>();
			for(Annotation a: annos) 
				annotMap.put(a.getCode(), a);
		} catch (Exception e) {
			logger.error("> EXCEPTION - GenericException : " + e.getMessage());
		}
	}
	
	private void saveAllReseltsOfTask(ProcessTask processTask) {
		try {
			absenceService.create(processTask.getAbsencesEffectues());
			absenceEffService.create(processTask.getAbsenceEffsEffectues());
			retardService.create(processTask.getRetardsEffectues());
			congeService.create(processTask.getCongesEffectues());
			interService.create(processTask.getInterruptionsEffectues());
			annoService.create(processTask.getAnomaliesEffectues());
			heurSuppService.create(processTask.getHrSuppsEffectues());
//			recuperationService.create(processTask.getre)
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

	private void processAllUsers(){
		try {
			ProcessTask processTaskLocal;
			List<Day> daysLocal;
			CommonCriteria critLocal;
			User userLocal;
			Planning plaLocal;
			int c = 0;
			List<ProcessTask> tasks = new ArrayList<ProcessTask>();
			for (User user: users) {
				userLocal = new UserImpl();
				processTaskLocal = new ProcessTask();
				tasks.add(processTaskLocal);
				daysLocal = new ArrayList<Day>(this.days);
				plaLocal = new PlanningImpl();
				
				userLocal = user;
				plaLocal = this.plAtribute;
				
				loadTimeFactors(processTaskLocal);
				processTaskLocal.setAnnotations(annotMap);
				processTaskLocal.setUser(userLocal);
				processTaskLocal.setDays(daysLocal);
				processTaskLocal.setPlAtribute(plaLocal);
				
				processTaskLocal.setJrFeries(this.jrFeries);
				processTaskLocal.setLiveState(this.liveState);
				processTaskLocal.setNoJustifyAbsence(this.noJustifyAbsence);
				
				processTaskLocal.setAno_TJF(this.Ano_TJF);
				processTaskLocal.setAno_TJR(this.Ano_TJR);
				processTaskLocal.setDT_HNuit_prm(this.DT_HNuit_prm);
				processTaskLocal.setDT_hsp_prm(this.DT_hsp_prm);
				processTaskLocal.setDT_mvt_prm(this.DT_mvt_prm);
				processTaskLocal.setDT_pause_prm(this.DT_pause_prm);
				processTaskLocal.setRpt_hnuit_prm(this.Rpt_hnuit_prm);
				processTaskLocal.setSl_hsup_cond_prm(this.Sl_hsup_cond_prm);
				
				processTaskLocal.setAnnoService(annoService);
				processTaskLocal.setMvmntService(mvmntService);
				taskExecutor.setBeanName("taskExecutor("+c+") -> " + userLocal.getNom() + " " + userLocal.getPrenom());
				taskExecutor.execute(processTaskLocal);	                       
				
				c++;
			}
			
			int count = taskExecutor.getActiveCount();
			logger.error("Start Active Threads : " + count);
			for (;;) {
				count = taskExecutor.getActiveCount();
				if (count == 0) {     
					//taskExecutor.shutdown();
					logger.error("No Active threads !");
					break;
				}
			}
			
			logger.error("Save all results of treatment !");
			for (ProcessTask task: tasks) {
				saveAllReseltsOfTask(task);
			}
			logger.error("Processiong finished");
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	private void setGlobalVars(CommonCriteria criteria, Planning plAtribute){
		this.userCriteria = new CommonCriteria();
		//BeanUtils.copyProperties(criteria, this.userCriteria,CommonCriteria.class);
		this.userCriteria = criteria;
		this.plAtribute = plAtribute;
		if (criteria.getTypeReport().equals("RECL_RPT") || criteria.getTypeReport().equals("RPT"))
			this.reporting = true;
		if (criteria.getTypeReport().equals("RECL_RPT") || criteria.getTypeReport().equals("RECL"))
			this.processing = true;
		
		logger.error("Period to analyse : ");
		logger.error("from : ["+Utils.toStrDate(userCriteria.getDateDebut(), "dd/MM/yyyy")+"]");
		logger.error("to : ["+Utils.toStrDate(userCriteria.getDateFin(), "dd/MM/yyyy")+"]");
	}
    
	public void process(CommonCriteria criteria, Planning plAtribute ){
    	try {
    		
    		// Set global vars
    		logger.error("0 - Set globals variables ...");
    		setGlobalVars(criteria, plAtribute);
    		
			// Check UserCriteria
			logger.error("1 - Check UserCriteria ...");
			if(!checkUserCriteria()) return;
			
			
			// Init all params
			logger.error("2 - Init All params ...");
			initAllParams();
			
			// Load all annotations
			logger.error("2 - Load all annotations ...");
			loadAllAnnots();
			
			// Clean Old Anomalies
			if (processing) {
				logger.error("3 - Clean Old Data ...");
				cleanOldData();
			}
			
			 // Build day list
			logger.error("4 - Build day List ...");
			buildDaysList();			
			
			// Detect smart Horaire
			logger.error("5 - Detect smart Horaire ...");
			detectSmartHoraire();

			// Process all user
			logger.error("6 - Process All users ...");
			processAllUsers();
			
			
		} catch (Exception e) {
			// TODO: handle exception
			logger.error(e.getMessage());
			
		}
    }
	

	public void setLiveState(boolean liveState) {
		this.liveState = liveState;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public void setAnnoService(AnnomalieManageableService annoService) {
		this.annoService = annoService;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public void setTypeAbsenceService(
			TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public void setMvmntService(MouvementManageableService mvmntService) {
		this.mvmntService = mvmntService;
	}

	public void setJrFerieService(JrFerieManageableService jrFerieService) {
		this.jrFerieService = jrFerieService;
	}

	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}


	public void setDynPlaDetailService(
			DynPlaDetailManageableService dynPlaDetailService) {
		this.dynPlaDetailService = dynPlaDetailService;
	}

	public void setAnnotService(AnnotationManageableService annotService) {
		this.annotService = annotService;
	}

	public void setInterService(InterruptionManageableService interService) {
		this.interService = interService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public void setAbsenceEffService(AbsenceEffManageableService absenceEffService) {
		this.absenceEffService = absenceEffService;
	}

	public void setCompensationService(
			CompensationManageableService compensationService) {
		this.compensationService = compensationService;
	}

	public void setRecuperationService(
			RecuperationManageableService recuperationService) {
		this.recuperationService = recuperationService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public void setHeurSuppService(HeurSuppManageableService heurSuppService) {
		this.heurSuppService = heurSuppService;
	}

   
    
}
