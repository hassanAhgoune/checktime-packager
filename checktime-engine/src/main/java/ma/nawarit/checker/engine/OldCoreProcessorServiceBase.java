package ma.nawarit.checker.engine;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import ma.nawarit.checker.common.Day;
import ma.nawarit.checker.common.MessageFactory;
import ma.nawarit.checker.compagnie.Noeud;
import ma.nawarit.checker.compagnie.ProfilMetier;
import ma.nawarit.checker.compagnie.User;
import ma.nawarit.checker.compagnie.UserImpl;
import ma.nawarit.checker.compagnie.crud.UserManageableService;
import ma.nawarit.checker.configuration.Annotation;
import ma.nawarit.checker.configuration.DynPlaDetail;
import ma.nawarit.checker.configuration.DynPlaDetailImpl;
import ma.nawarit.checker.configuration.JrFerie;
import ma.nawarit.checker.configuration.Planning;
import ma.nawarit.checker.configuration.crud.AdvParamManageableService;
import ma.nawarit.checker.configuration.crud.AnnotationManageableService;
import ma.nawarit.checker.configuration.crud.DynPlaDetailManageableService;
import ma.nawarit.checker.configuration.crud.JrFerieManageableService;
import ma.nawarit.checker.core.common.Plage;
import ma.nawarit.checker.core.common.Utils;
import ma.nawarit.checker.injection.Mouvement;
import ma.nawarit.checker.injection.MouvementImpl;
import ma.nawarit.checker.injection.crud.MouvementManageableService;
import ma.nawarit.checker.suivi.Absence;
import ma.nawarit.checker.suivi.AbsenceEffImpl;
import ma.nawarit.checker.suivi.AbsenceImpl;
import ma.nawarit.checker.suivi.Annomalie;
import ma.nawarit.checker.suivi.AnnomalieImpl;
import ma.nawarit.checker.suivi.Compensation;
import ma.nawarit.checker.suivi.CompensationImpl;
import ma.nawarit.checker.suivi.Conge;
import ma.nawarit.checker.suivi.CongeImpl;
import ma.nawarit.checker.suivi.ExceptionProfilMetier;
import ma.nawarit.checker.suivi.HeurSupp;
import ma.nawarit.checker.suivi.HeurSuppImpl;
import ma.nawarit.checker.suivi.Interruption;
import ma.nawarit.checker.suivi.InterruptionImpl;
import ma.nawarit.checker.suivi.Recuperation;
import ma.nawarit.checker.suivi.Retard;
import ma.nawarit.checker.suivi.RetardImpl;
import ma.nawarit.checker.suivi.Timing;
import ma.nawarit.checker.suivi.TypeAbsence;
import ma.nawarit.checker.suivi.crud.AbsenceEffManageableService;
import ma.nawarit.checker.suivi.crud.AbsenceManageableService;
import ma.nawarit.checker.suivi.crud.AnnomalieManageableService;
import ma.nawarit.checker.suivi.crud.CompensationManageableService;
import ma.nawarit.checker.suivi.crud.CongeManageableService;
import ma.nawarit.checker.suivi.crud.HeurSuppManageableService;
import ma.nawarit.checker.suivi.crud.InterruptionManageableService;
import ma.nawarit.checker.suivi.crud.RecuperationManageableService;
import ma.nawarit.checker.suivi.crud.RetardManageableService;
import ma.nawarit.checker.suivi.crud.TypeAbsenceManageableService;

import org.andromda.spring.CommonCriteria;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;


public class OldCoreProcessorServiceBase implements CoreProcessorService{
	
	private static final Logger logger = Logger.getLogger(OldCoreProcessorServiceBase.class);
	private Collection<Mouvement> mvmnts = null;
	private Collection<User> users = null;
	private Collection<Conge> congesValidates = null;
	private Collection<Absence> absencesValidates = null;
	private Collection<JrFerie> jrFeries = null;
	private Collection<Recuperation> recupsValidates = null;
	private Collection<Compensation> compValidates = null;
	private List<Recuperation> absRecs = null;
	private Collection<HeurSupp> hrSuppsValidates = null;
	private UserManageableService userService;
	private AnnomalieManageableService annoService;
	private AnnotationManageableService annotService;
	private InterruptionManageableService interService;
	private CongeManageableService congeService;
	private AbsenceManageableService absenceService;
	private AbsenceEffManageableService absenceEffService;
	private AdvParamManageableService advParamService;
	private TypeAbsenceManageableService typeAbsenceService;
	private CompensationManageableService compensationService;
	private RecuperationManageableService recuperationService;
	private RetardManageableService retardService;
	private MouvementManageableService mvmntService;
	private JrFerieManageableService jrFerieService;
	private HeurSuppManageableService heurSuppService;
	private DynPlaDetailManageableService dynPlaDetailService;
	private List<Day> days = new ArrayList<Day>();
	private TypeAbsence noJustifyAbsence;
	private boolean liveState = false;
	
	private static final String BEAN_REF_FACTORY_PATH = "conf/beanRefFactory.xml";
	private final String HORAIRE_LIBRE ="horaireLibre";
	private final String SANS_POI_COMPTEUR ="sansPointage";
	private final String OUT_SANS_POI_COMPTEUR ="sortieSansPointage";
	private final String VALIDATE_STATUT = "validate";
	private final String EFFECTUE_STATUT = "effectue";
	private final String NOJUSTIFY= "noJustify";
	
	private long DT_mvt_prm;
	private int DT_msFocus_prm; 
	private int DT_hsp_prm;
	private int DT_out_prm;
	private int DT_HNuit_prm;
	private double DT_pause_prm;
	private String Rpt_hnuit_prm;
	private String Ano_TJR;
	private String Ano_TJF;
	private Double Sl_hsup_cond_prm;
	
    /**
     * Gets the shared instance of this Class
     *
     * @return the shared AnnoTrucker instance.
     */
    public  OldCoreProcessorServiceBase (){	
    	
    }
    
    /**
     * methode initialisation des parametres 
     * @param mvmnts
     * @param users
     */
    public void firstInit(
    		List<User> users, 
    		Collection<JrFerie> jrFeries, 
    		List<TypeAbsence> noJustifyAbsences) {
    	this.users = users;    	
    	this.jrFeries = jrFeries;
    	if (noJustifyAbsences != null && !noJustifyAbsences.isEmpty())
    		this.noJustifyAbsence = (TypeAbsence)noJustifyAbsences.get(0);
    	try {
			if (advParamService != null) {
				Rpt_hnuit_prm = advParamService.getValueByName("report_horNuit");
				DT_pause_prm = new Double(advParamService.getValueByName("DeltaT_pause"));
				Sl_hsup_cond_prm = new Double(advParamService.getValueByName("seuil_hrSupp_cond"));
				DT_out_prm = new Integer(advParamService.getValueByName("DeltaT_Out"));
				DT_HNuit_prm = new Integer(advParamService.getValueByName("DeltaT_HorNuit"));
				DT_hsp_prm = new Integer(advParamService.getValueByName("DeltaT_Hsp"));
				DT_mvt_prm = new Integer(advParamService.getValueByName("DeltaT_Mvt"))  * 60000;
				DT_msFocus_prm = new Integer(advParamService.getValueByName("DeltaT_MasseFocus"))  * 60000;
				Ano_TJR = advParamService.getValueByName("anoTrJrRepos");
				Ano_TJF = advParamService.getValueByName("anoTrJrFerie");
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
    
    public void secondInit(
    		Collection<ma.nawarit.checker.injection.Mouvement> mvmnts, 
    		Collection<HeurSupp> hrSuppsValidates,
    		Collection<Conge> conges, 
    		Collection<Absence> absencesValidates,
    		Collection<Recuperation> recupsValidates,
    		Collection<Compensation> compValidates,
    		List<Recuperation> absRecs) {
    	this.mvmnts = null;
    	this.hrSuppsValidates = null;
    	this.congesValidates = null;
    	this.absencesValidates = null;
    	this.recupsValidates = null;
    	this.compValidates = null;
    	this.absRecs = null;
    	this.mvmnts = mvmnts;
    	this.hrSuppsValidates = hrSuppsValidates;
    	this.congesValidates = conges;
    	this.absencesValidates = absencesValidates;
    	this.recupsValidates = recupsValidates;
    	this.compValidates = compValidates;
    	this.absRecs = absRecs;
	}
    
    
    public void getMvtsByDate(Day d) {
    	d.getMouvements().clear();
    	if (this.mvmnts == null || this.mvmnts.isEmpty())
    		return;
     	Calendar c = new GregorianCalendar();
		for (Mouvement mvt : this.mvmnts) {
			c.setTime(mvt.getDate());
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.set(Calendar.MILLISECOND, 0);
			if(c.getTime().equals(d.getDate())) {
				d.getMouvements().add(mvt);
			}
		}
    }
    
    
    /**
     * on construit la liste des jours à traiter
     * @param debut
     * @param fin
     */
    public void buildDaysList(CommonCriteria userCriteria) {
    	try {
    		Calendar d1 = new GregorianCalendar();
    		Calendar d2 = new GregorianCalendar();
    		d1.setTime(userCriteria.getDateDebut());
	    	d1.set(Calendar.HOUR_OF_DAY, 0);
	    	d1.set(Calendar.MINUTE, 0);
	    	d1.set(Calendar.SECOND, 0);
	    	d1.set(Calendar.MILLISECOND, 0);
	    	
	    	d2.setTime(userCriteria.getDateFin());
	    	d2.set(Calendar.HOUR_OF_DAY, 0);
	    	d2.set(Calendar.MINUTE, 0);
	    	d2.set(Calendar.SECOND, 0);
	    	d2.set(Calendar.MILLISECOND, 0); 
	    	
    		days = new ArrayList<Day>();
    		Day day;
    		while (d1.compareTo(d2) <= 0) {
    			day = new Day();
    			day.setDate(d1.getTime());
				day.setRptHNuit(Rpt_hnuit_prm);
    			days.add(day);
    			d1.add(Calendar.DAY_OF_MONTH, 1);
    		}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
    }
    
    /**
     * Calculer le cumul des heures 
     * supplimentaires consomm�es par le collaborateur
     * @param d1
     * @param d2
     * @param hrSupps
     * @return
     */
    public void compareProgHrSupp(Day d,Calendar d1, Calendar d2, User user) throws Exception{
		Hashtable<String, String> props = new Hashtable<String, String>();
		Calendar cal = new GregorianCalendar();
    	for (Plage plage: d.getHrSupps()) {
			
	    	HeurSupp hrSuppEf = new HeurSuppImpl();
	    	long diff;
	    	if ((d1.compareTo(plage.getFin())>= 0) || (d2.compareTo(plage.getDebut()) <= 0)) //pas d'intersection entre d1<-->d2 et debut<-->fin 	
	    		continue;     	
	    	else if ((d1.compareTo(plage.getDebut()) >= 0) && (d2.compareTo(plage.getFin()) <= 0)) { // cumul : d1 <--> d2
	    		diff = Utils.differenceDates(d1.getTime(), d2.getTime());
	    		if (diff <= DT_hsp_prm * 60000)
					continue;
	    		cal.setTimeInMillis(diff);
	    		try {
	    			hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());
		    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
		    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
		    		hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		props.put("code", "OBL");
		    		if (plage.getPlageHor() == null)
		    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
		    		else
		    			hrSuppEf.setAnnotation(plage.getPlageHor().getAnnotation());
		    		if (d.isHoraireNuit())
		    			hrSuppEf.setHorNuit(true);
					heurSuppService.create(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		d1.set(Calendar.HOUR_OF_DAY, d2.get(Calendar.HOUR_OF_DAY));
		    	d1.set(Calendar.MINUTE, d2.get(Calendar.MINUTE));
	    		break;
	    	} else if ((d1.compareTo(plage.getDebut()) >= 0) && (d2.compareTo(plage.getFin()) >= 0)){ // cumul : d1 <--> fin
	    		diff = Utils.differenceDates(d1.getTime(), plage.getFin().getTime());
	    		if (diff <= DT_hsp_prm * 60000)
					continue;
	    		cal.setTimeInMillis(diff);
	    		try {
	    			hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());
		    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
		    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(plage.getFin().getTime()));
		    		hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		props.put("code", "HSUP100");
		    		if (plage.getPlageHor() == null)
		    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
		    		else
		    			hrSuppEf.setAnnotation(plage.getPlageHor().getAnnotation());
		    		if (d.isHoraireNuit())
		    			hrSuppEf.setHorNuit(true);
					heurSuppService.create(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		d1.set(Calendar.HOUR_OF_DAY, plage.getFin().get(Calendar.HOUR_OF_DAY));
		    	d1.set(Calendar.MINUTE, plage.getFin().get(Calendar.MINUTE));
	    	} else if ((d1.compareTo(plage.getDebut()) <= 0) && (d2.compareTo(plage.getFin()) >= 0)){ // cumul : debut <--> fin
	    		diff = Utils.differenceDates(plage.getDebut().getTime(), plage.getFin().getTime());
	    		if (diff <= DT_hsp_prm * 60000)
					continue;
	    		cal.setTimeInMillis(diff);
	    		try {
	    			hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());
		    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(plage.getDebut().getTime()));
		    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(plage.getFin().getTime()));
		    		hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		props.put("code", "HSUP100");
		    		if (plage.getPlageHor() == null)
		    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
		    		else
		    			hrSuppEf.setAnnotation(plage.getPlageHor().getAnnotation());
		    		if (d.isHoraireNuit())
		    			hrSuppEf.setHorNuit(true);
					heurSuppService.create(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		Calendar c = new GregorianCalendar();
	    		c.setTime(d1.getTime());
	    		compareProgHrSupp(d, c, plage.getDebut(), user);
	    		c.setTime(d2.getTime());
	    		compareProgHrSupp(d, plage.getFin(), c, user);
	    	} else if ((d1.compareTo(plage.getDebut()) <= 0) && (d2.compareTo(plage.getFin()) <= 0)){ // cumul : debut <--> d2
	    		diff = Utils.differenceDates(plage.getDebut().getTime(), d2.getTime());
	    		if (diff <= DT_hsp_prm * 60000)
					continue;
	    		cal.setTimeInMillis(diff);
	    		try {
	    			hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());
		    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(plage.getDebut().getTime()));
		    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));		    		
		    		hrSuppEf.setDuree(Utils.getHourDoubleFormat(cal.getTime()));	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		props.put("code", "HSUP100");
		    		if (plage.getPlageHor() == null)
		    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
		    		else
		    			hrSuppEf.setAnnotation(plage.getPlageHor().getAnnotation());
		    		if (d.isHoraireNuit())
		    			hrSuppEf.setHorNuit(true);
					heurSuppService.create(hrSuppEf);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	    		d2.set(Calendar.HOUR_OF_DAY, plage.getDebut().get(Calendar.HOUR_OF_DAY));
		    	d2.set(Calendar.MINUTE, plage.getDebut().get(Calendar.MINUTE));
	    	}
		}
    }
    
    public void projection(Plage plage, Day d, User user, Calendar input) throws Exception{
    	
    	long entree = 0;
		long sortie = 0;
    	
    	//indicateur de la suppression du mvnt de traitement finit
    	boolean deleted = false;
    	
    	//parametres de definiton d'interruption		
    	Date debutInter = null;
    	    	 
    	Calendar laDate;
    	
		
    	Interruption interruption = new InterruptionImpl();
    	
    	long tpsEff = 0;
    	Calendar c = Calendar.getInstance();
    	
    	plage.getZoneOut().add(Calendar.MINUTE, - DT_hsp_prm);
    	List<Mouvement> list = new ArrayList<Mouvement>(d.getMouvements());
    	
    	for (Mouvement mvmnt: list) {
			
			// on construit laDate par la date, l'heure et minute de pointage
    		laDate = new GregorianCalendar ();
			laDate.setTime(mvmnt.getDate());
			if (mvmnt.isTraited()) 
				continue;
			
			deleted = false;
			Retard retard = new RetardImpl();
			
			if (laDate.compareTo(plage.getDebut()) < 0) {	
				if (!plage.isEntree()) {
					if (plage.getLastPlage(d.getHrFixes()) == null
							|| (plage.getLastPlage(d.getHrFixes()) != null
								&& plage.getLastPlage(d.getHrFixes()).isSortie())){
						plage.setEntree(true);
						mvmnt.setTraited(true);	
						entree = -1;
						input.setTime(laDate.getTime());
					} else if (plage.getLastPlage(d.getHrFixes()) != null
							&& !plage.getLastPlage(d.getHrFixes()).isSortie()) {
						plage.getLastPlage(d.getHrFixes()).setSortie(true);

				    	tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
				    	c.setTimeInMillis(tpsEff);
				    	if (d.isFlexible()) {
				    		d.setTmworked(Utils.additionHeures(d.getTmworked() ,Utils.getHourDoubleFormat(c.getTime())));
				    		tpsEff = 0;
				    	}
						mvmnt.setTraited(true);	

						MouvementImpl fin = new MouvementImpl();
		    			fin.setDate(plage.getLastPlage(d.getHrFixes()).getFin().getTime());
		    			fin.setBadge(user.getBadge());
		    			fin.setTraited(true);
		    			if (!d.getMouvements().contains(fin))
		    				d.getMouvements().add(fin);
					}
					continue;
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else  if (laDate.compareTo(plage.getDebut()) == 0) {		
				if (!plage.isEntree()) {
					plage.setEntree(true);
					entree = 1;
					deleted = true;
					input.setTime(laDate.getTime());
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else if ((laDate.compareTo(plage.getDebut()) > 0) 
					&& (laDate.compareTo(plage.getDebutPlus()) <= 0) ){
				if (!plage.isEntree()) {	
					int Rt = (laDate.get(Calendar.HOUR_OF_DAY ) * 60 + laDate.get(Calendar.MINUTE )) - (plage.getDebut().get(Calendar.HOUR_OF_DAY) * 60 + plage.getDebut().get(Calendar.MINUTE));
					if ((plage.getDebutFlex() == null) || (plage.getDebutFlex() != null && plage.getDebutFlex().compareTo(laDate) < 0)) {						
						retard.setDate(d.getDate());
						retard.setIndexPlage(plage.getIndex());
		    			retard.setUser((UserImpl)user);
		    			retard.setMatricule(user.getMatricule() + "-ER");
		    			retard.setRetardTolere(Rt);
		    			retard.setDescription("Retard tol�r� de :" + Rt + " mn � " + Utils.getHourDateFormat(plage.getDebut().getTime()));
		    			if(d.isHoraireNuit())
		    				retard.setHorNuit(true);
		    			else
		    				retard.setHorNuit(false);
	    				retardService.create(retard);
					}
					plage.setEntree(true);
					input.setTime(laDate.getTime());
					entree = 1;
					deleted = true;
				} else {
					plage.setEntree(false);
					tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
					mvmnt.setTraited(true);
					entree = 0;
					continue;
				}
			} else if ((laDate.compareTo(plage.getDebutPlus()) > 0) 
					&& (laDate.compareTo(plage.getFinMoins()) < 0) ) {
	    		if (!plage.isEntree()) {
	    			if (laDate.compareTo(plage.getZoneOut()) <= 0){
		    			int Rt = (laDate.get(Calendar.HOUR_OF_DAY ) * 60 + laDate.get(Calendar.MINUTE )) - (plage.getDebut().get(Calendar.HOUR_OF_DAY) * 60 + plage.getDebut().get(Calendar.MINUTE));
		    			if ((plage.getDebutFlex() == null) || (plage.getDebutFlex() != null && plage.getDebutFlex().compareTo(laDate) < 0)) {		    			
			    			retard.setDate(d.getDate());
							retard.setIndexPlage(plage.getIndex());
			    			retard.setUser((UserImpl)user);
			    			retard.setMatricule(user.getMatricule() + "-ER");
			    			retard.setRetardNonTolere(Rt);
			    			retard.setDescription("Retard non tol�r� de :" + Rt + " mn � " + Utils.getHourDateFormat(plage.getDebut().getTime()));
			    			if(d.isHoraireNuit())
			    				retard.setHorNuit(true);
			    			else
			    				retard.setHorNuit(false);
		    				retardService.create(retard);
		    			}
		    			plage.setEntree(true);
		    			input.setTime(laDate.getTime());
		    			entree = 1;
		    			deleted = true;
	    			} else {
	    				plage.setSortie(true);
	    		    	sortie = 1;
	    		    	deleted = true;
	    			}
	    		} else {
	    			if (debutInter == null) {
		    			debutInter = new Date(laDate.getTimeInMillis());
		    			tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
		    			
		    			// Traitement EXCP PR&PO
		    			if (d.getExceptP() != null && (d.getExceptP() instanceof ProfilMetier)) {
		    				ProfilMetier pr = (ProfilMetier)d.getExceptP();
			    			Annomalie annomalie;
			    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0) {
			    				// ano de type EPM-HS
			    				annomalie = new AnnomalieImpl();
			    				annomalie.setDateAnomalie(d.getDate());
		    					annomalie.setDescription("travail en tant que " + pr.getCode() + "-" + pr.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
		    					annomalie.setUser(user);
		    					annomalie.setType("EPM-HS");
		    					annomalie.setMatricule("E-"+user.getMatricule());
		    					if (d.isHoraireNuit())
		    						annomalie.setHorNuit(true);
	    						annoService.create(annomalie);
			    			} 
			    			List<Timing> l;
			    			if(plage.getDebut().compareTo(input) > 0) 
			    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
			    			else
			    				l = plage.getEffTmgs(d.getDate(), input, laDate);
			    			
			    				// anos de type EPM
		    				for (Timing t: l) {
		    					annomalie = new AnnomalieImpl();
			    				annomalie.setDateAnomalie(d.getDate());
		    					annomalie.setDescription("travail en tant que '" + pr.getCode() + "'-'" + pr.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
		    					annomalie.setUser(user);
		    					annomalie.setType("EPM");
		    					annomalie.setMatricule("E-"+user.getMatricule());
		    					if (d.isHoraireNuit())
		    						annomalie.setHorNuit(true);
	    						annoService.create(annomalie);
			    			}
		    			} else
		    			if (d.getExceptP() != null && (d.getExceptP() instanceof Noeud)) {
		    				Noeud po = (Noeud)d.getExceptP();
			    			Annomalie annomalie;
			    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0) {
			    				// ano de type EPO-HS
			    				annomalie = new AnnomalieImpl();
			    				annomalie.setDateAnomalie(d.getDate());
		    					annomalie.setDescription("travail dans le position " + po.getCode() + "-" + po.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
		    					annomalie.setUser(user);
		    					annomalie.setType("EPO-HS");
		    					annomalie.setMatricule("E-"+user.getMatricule());
		    					if (d.isHoraireNuit())
		    						annomalie.setHorNuit(true);
	    						annoService.create(annomalie);
			    			} 
			    			List<Timing> l;
			    			if(plage.getDebut().compareTo(input) > 0) 
			    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
			    			else
			    				l = plage.getEffTmgs(d.getDate(), input, laDate);
			    			
			    				// anos de type EPO
		    				for (Timing t: l) {
		    					annomalie = new AnnomalieImpl();
			    				annomalie.setDateAnomalie(d.getDate());
		    					annomalie.setDescription("travail dans le position '" + po.getCode() + "'-'" + po.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
		    					annomalie.setUser(user);
		    					annomalie.setType("EPO");
		    					annomalie.setMatricule("E-"+user.getMatricule());
		    					if (d.isHoraireNuit())
		    						annomalie.setHorNuit(true);
	    						annoService.create(annomalie);
			    			}
		    			}
		    		} else {		    	
		    			Date [] outs = plage.getValideInterp(debutInter, laDate.getTime(), DT_pause_prm);
		    			Date d1;
		    			Date d2;
		    			if (outs != null) {
		    				d1 = outs[0];
		    				d2 = outs[1];
			    			double diff = Utils.differenceHeures(Utils.getHourDoubleFormat(d1), Utils.getHourDoubleFormat(d2));	 		
		    				int i = stateAbsComp(d, diff, user);
		    				Annomalie annomalie = new AnnomalieImpl();
		    				if (i == 1) {
		    					annomalie.setDateAnomalie(d.getDate());
		    					annomalie.setDescription("J.trv :La duree absent�e est sup�rieure � celle programm�e dans la demande compensation.");
		    					annomalie.setUser(user);
		    					annomalie.setType("NRT");
		    					annomalie.setMatricule("E-"+user.getMatricule());
		    					if (d.isHoraireNuit())
		    						annomalie.setHorNuit(true);
	    						annoService.create(annomalie);
		    				} else if (i == -1) {	    					
		    					AbsenceImpl absence = new AbsenceImpl();
		    					Calendar cal = new GregorianCalendar();
		    					cal.setTime(d1);
		    					cal.set(Calendar.HOUR_OF_DAY, 0);
		    					cal.set(Calendar.MINUTE, 0);
		    			    	cal.set(Calendar.SECOND, 0);
		    			    	cal.set(Calendar.MILLISECOND, 0);
		    					absence.setDateDebut(cal.getTime());
		    					absence.setHeureDebut(Utils.getHourDoubleFormat(d1));
		    					
		    					Calendar cal2 = new GregorianCalendar();
		    					cal2.setTime(d2);
		    					cal2.set(Calendar.HOUR_OF_DAY, 0);
		    					cal2.set(Calendar.MINUTE, 0);
		    			    	cal2.set(Calendar.SECOND, 0);
		    			    	cal2.set(Calendar.MILLISECOND, 0);
		    					absence.setDateReprise(cal2.getTime());
		    					absence.setHeureReprise(Utils.getHourDoubleFormat(d2));
		    					
								absence.setCollaborateur(user);
								absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
								if(d.isHoraireNuit())
									absence.setHorNuit(true);
								else
									absence.setHorNuit(false);
								List<Absence> l = getAbsencesIfValidated(absence);
								if (l == null || l.isEmpty()){
			    					String start = Utils.getHourDateFormat(d1);
				    				String end = Utils.getHourDateFormat(d2);
			    					interruption.setDate(d.getDate());
			    					interruption.setDuree(diff);				
			    					interruption.setHeureDebut(Double.parseDouble(start.substring(0, 2) + "." + start.substring(3, 5)));
			    					interruption.setHeureReprise(Double.parseDouble(end.substring(0, 2) + "." + end.substring(3, 5)));
			    					interruption.setDescription("interruption entre '" + start + "' et '" + end+"'");
			    					interruption.setUser(user);
			    					interruption.setMatricule("ITRS-"+user.getMatricule());
			    					if(d.isHoraireNuit())
			    						interruption.setHorNuit(true);
			    	    			else
			    	    				interruption.setHorNuit(false);
		    						interService.create(interruption);
			    				} else {
		    						d.getLegalAbs().addAll(l);
		    						for (Absence ab : l) {
		    							if (ab == null)
		    								continue;
		    							ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    							if (ab.getTypeAbsence().getLibelle() == null)
		    								continue;
	    								absenceService.create(ab);
		    						}
			    				}
		    				}
	    				}
	    				debutInter = null;
	    			}
	    			input.setTime(laDate.getTime());
	    			deleted = true;
	    		}
	    	} else if ((laDate.compareTo(plage.getFinMoins()) >= 0) 
					&& (laDate.compareTo(plage.getFin()) <= 0)
					&& !plage.isSortie()) {
		    	plage.setSortie(true);
		    	tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
		    	sortie = 1;
		    	deleted = true;
    			
    			// Traitement EXCPPM
		    	if (d.getExceptP() != null && (d.getExceptP() instanceof ProfilMetier)) {
    				ProfilMetier pr = (ProfilMetier)d.getExceptP();
	    			Annomalie annomalie;
	    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0)  {
	    				// ano de type EPM-HS
	    				annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que " + pr.getCode() + "-" + pr.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			} 
	    			List<Timing> l;
	    			if(plage.getDebut().compareTo(input) > 0) 
	    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
	    			else
	    				l = plage.getEffTmgs(d.getDate(), input, laDate);
	    			
	    				// anos de type EPM
					for (Timing t: l) {
						annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que '" + pr.getCode() + "'-'" + pr.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
						annomalie.setUser(user);
						annomalie.setType("EPM");
						annomalie.setMatricule("E-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			}
		    	} else
		    	if (d.getExceptP() != null && (d.getExceptP() instanceof Noeud)) {
    				Noeud po = (Noeud)d.getExceptP();
	    			Annomalie annomalie;
	    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0) {
	    				// ano de type EPO-HS
	    				annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
    					annomalie.setDescription("travail dans le position " + po.getCode() + "-" + po.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
    					annomalie.setUser(user);
    					annomalie.setType("EPO-HS");
    					annomalie.setMatricule("E-"+user.getMatricule());
    					if (d.isHoraireNuit())
    						annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			} 
	    			List<Timing> l;
	    			if(plage.getDebut().compareTo(input) > 0) 
	    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
	    			else
	    				l = plage.getEffTmgs(d.getDate(), input, laDate);
	    			
	    				// anos de type EPO
    				for (Timing t: l) {
    					annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
    					annomalie.setDescription("travail dans le position '" + po.getCode() + "'-'" + po.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
    					annomalie.setUser(user);
    					annomalie.setType("EPO");
    					annomalie.setMatricule("E-"+user.getMatricule());
    					if (d.isHoraireNuit())
    						annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			}
    			}
	    	} else if (!plage.isSortie() || debutInter != null) { 
	    		
	    		if (plage.getFolowPlage(d.getHrFixes()) == null) {
	    			MouvementImpl fin = new MouvementImpl();
	    			fin.setDate(plage.getFin().getTime());
	    			fin.setBadge(user.getBadge());
	    			fin.setTraited(true);
	    			if (!d.getMouvements().contains(fin))
	    				d.getMouvements().add(fin);
	    			sortie = 2;
	    		} 
//	    		else if (plage.getLastPlage(d.getHrFixes()) != null
//						&& plage.getLastPlage(d.getHrFixes()).isEntree()
//						&& plage.getLastPlage(d.getHrFixes()).isSortie()) {
//	    			Mouvement debut = new MouvementImpl();
//	    			debut.setDate(plage.getDebut().getTime());
//	    			debut.setBadge(user.getBadge());
//	    			debut.setTraited(true);
//	    			if (!d.getMouvements().contains(debut))
//	    				d.getMouvements().add(debut);
//	    			Mouvement fin = new MouvementImpl();
//	    			fin.setDate(plage.getFin().getTime());
//	    			fin.setBadge(user.getBadge());
//	    			fin.setTraited(true);
//	    			if (!d.getMouvements().contains(fin))
//	    				d.getMouvements().add(fin);
//	    		}
	    		
	    		Calendar cal = new GregorianCalendar();
	    		cal.setTime(laDate.getTime());
	    		Day nextDay = d.getNextDay(days);
	    		if (nextDay != null && nextDay.isHoraireNuit() && nextDay.getHrFixes() != null && !nextDay.getHrFixes().isEmpty()) {
		    		cal.setTime(nextDay.getHrFixes().get(0).getDebut().getTime());
		    		cal.add(Calendar.HOUR_OF_DAY, -1);
	    		}
    			if (plage.getFolowPlage(d.getHrFixes()) == null
    					|| laDate.compareTo(cal) < 0) {
					plage.setSortie(true);
					mvmnt.setTraited(true);
					tpsEff = tpsEff + Utils.differenceDates(input.getTime(), laDate.getTime());
				} 
    			
    			// Traitement EXCPPM
    			if (d.getExceptP() != null && (d.getExceptP() instanceof ProfilMetier)) {
    				ProfilMetier pr = (ProfilMetier)d.getExceptP();
	    			Annomalie annomalie;
	    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0)  {
	    				// ano de type EPM-HS
	    				annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que " + pr.getCode() + "-" + pr.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			} 
	    			if (plage.isFinTmg())  {
	    				// ano de type EPM-HS
	    				annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail en tant que " + pr.getCode() + "-" + pr.getLibelle() + " entre '" + Utils.getHourDoubleFormat(plage.getFin().getTime()) + "' et '" + Utils.getHourDoubleFormat(laDate.getTime()) +"'");
						annomalie.setUser(user);
						annomalie.setType("EPM-HS");
						annomalie.setMatricule("E-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			} 
	    			List<Timing> l;
	    			if(plage.getDebut().compareTo(input) > 0) 
	    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), plage.getFin());
	    			else
	    				l = plage.getEffTmgs(d.getDate(), input, plage.getFin());
	    			
	    				// anos de type EPM
					for (Timing t: l) {
						annomalie = new AnnomalieImpl();
	    				annomalie.setDateAnomalie(d.getDate());
	    				annomalie.setDescription("travail en tant que '" + pr.getCode() + "'-'" + pr.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
	    				annomalie.setUser(user);
						annomalie.setType("EPM");
						annomalie.setMatricule("E-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
	    			}
    			}
	    	} else
			if (d.getExceptP() != null && (d.getExceptP() instanceof Noeud)) {
				Noeud po = (Noeud)d.getExceptP();
    			Annomalie annomalie;
    			if (plage.isDebutTmg() && plage.getDebut().compareTo(input) > 0) {
    				// ano de type EPO-HS
    				annomalie = new AnnomalieImpl();
    				annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail dans le position " + po.getCode() + "-" + po.getLibelle() + " entre '" + Utils.getHourDoubleFormat(input.getTime()) + "' et '" + Utils.getHourDoubleFormat(plage.getDebut().getTime()) +"'");
					annomalie.setUser(user);
					annomalie.setType("EPO-HS");
					annomalie.setMatricule("E-"+user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					annoService.create(annomalie);
    			} 
    			if (plage.isFinTmg())  {
    				// ano de type EPO-HS
    				annomalie = new AnnomalieImpl();
    				annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail dans le position " + po.getCode() + "-" + po.getLibelle() + " entre '" + Utils.getHourDoubleFormat(plage.getFin().getTime()) + "' et '" + Utils.getHourDoubleFormat(laDate.getTime()) +"'");
					annomalie.setUser(user);
					annomalie.setType("EPO-HS");
					annomalie.setMatricule("E-"+user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					annoService.create(annomalie);
    			} 
    			List<Timing> l;
    			if(plage.getDebut().compareTo(input) > 0) 
    				l = plage.getEffTmgs(d.getDate(), plage.getDebut(), laDate);
    			else
    				l = plage.getEffTmgs(d.getDate(), input, laDate);
    			
    				// anos de type EPO
				for (Timing t: l) {
					annomalie = new AnnomalieImpl();
    				annomalie.setDateAnomalie(d.getDate());
					annomalie.setDescription("travail dans le position '" + po.getCode() + "'-'" + po.getLibelle() + "' entre '" + t.getHreDebut() + "' et '" + t.getHreFin() +"'");
					annomalie.setUser(user);
					annomalie.setType("EPO");
					annomalie.setMatricule("E-"+user.getMatricule());
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					annoService.create(annomalie);
    			}
			}
			if(deleted)
				d.getMouvements().remove(mvmnt);
			
			if (mvmnt.isTraited()) {
				d.getMouvements().remove(mvmnt);
				d.getMouvements().add(mvmnt);
			}
			if (plage.isSortie())
				break;
		}
    	
    	c.setTimeInMillis(tpsEff);
    	if (d.isFlexible()) {
    		d.setTmworked(Utils.additionHeures(d.getTmworked() ,Utils.getHourDoubleFormat(c.getTime())));
    		tpsEff = 0;
    	}
//		if (plage.getLastPlage(d.getHrFixes()) != null
//				&& plage.getLastPlage(d.getHrFixes()).isEntree()
//				&& !plage.getLastPlage(d.getHrFixes()).isSortie() 
//				&& entree == 0) 
//			entree = -1;
			
		if (entree == -1){
			MouvementImpl debut = new MouvementImpl();
			debut.setDate(plage.getDebut().getTime());
			debut.setBadge(user.getBadge());
			debut.setTraited(true);
			d.getMouvements().add(debut);
		} 
		
		if (debutInter != null) {
			plage.setSortie(true);
			
			double diff = Utils.differenceHeures(Utils.getHourDoubleFormat(debutInter), Utils.getHourDoubleFormat(plage.getFin().getTime()));
			
			AbsenceImpl absence = new AbsenceImpl();
			Calendar cal = new GregorianCalendar();
			cal.setTime(debutInter);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
	    	cal.set(Calendar.SECOND, 0);
	    	cal.set(Calendar.MILLISECOND, 0);
			absence.setDateDebut(cal.getTime());
			absence.setHeureDebut(Utils.getHourDoubleFormat(debutInter));
			
			Calendar cal2 = new GregorianCalendar();
			cal2.setTime(plage.getFin().getTime());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
			cal2.set(Calendar.MINUTE, 0);
	    	cal2.set(Calendar.SECOND, 0);
	    	cal2.set(Calendar.MILLISECOND, 0);
			absence.setDateReprise(cal2.getTime());
			absence.setHeureReprise(Utils.getHourDoubleFormat(plage.getFin().getTime()));
			
			absence.setDuree(0);
			absence.setCollaborateur(user);
			absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			if (d.isHoraireNuit())
				absence.setHorNuit(true);
			else
				absence.setHorNuit(false);
			List<Absence> l = getAbsencesIfValidated(absence);
			if ((l == null || l.isEmpty()) && (plage.getPlageHor().getFlexFin() <= diff)) {
				String start = Utils.getHourDateFormat(debutInter);
				String end = Utils.getHourDateFormat(plage.getFin().getTime());
				interruption.setDate(d.getDate());
				interruption.setDuree(diff);				
				interruption.setHeureDebut(Double.parseDouble(start.substring(0, 2) + "." + start.substring(3, 5)));
				interruption.setHeureReprise(Double.parseDouble(end.substring(0, 2) + "." + end.substring(3, 5)));
				interruption.setDescription("interruption sans retoure entre '" + start + "' et '" + end+"'");
				interruption.setUser(user);
				interruption.setMatricule("ITRS-"+user.getMatricule());
				if(d.isHoraireNuit())
					interruption.setHorNuit(true);
    			else
    				interruption.setHorNuit(false);
				interService.create(interruption);
			} else if (l != null && !l.isEmpty()) {
				d.getLegalAbs().addAll(l);
				for (Absence ab : l) {
					if (ab == null)
						continue;
					ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					if (ab.getTypeAbsence().getLibelle() == null)
						continue;
					absenceService.create(ab);
				}
			}
		} 
		
		
    }
    
    
    /**
     * Lancement de traitement apres l'extraction 
     * de plages horaires de type obligatoire et heure suplimentaire
     * @param horaire
     * @param laDate
     * @param user
     * @throws Exception
     */
    public void traiterParHoraire(Day d, User user) throws Exception {
		Annomalie annomalie = new AnnomalieImpl();
		if (d.isJrRepos()) {
			if (!d.isHrSuppDay() && !d.isCompensedDay() && !d.isRecuperedDay()) {
					if ((d.getNature()!=null && d.getNature() instanceof JrFerie) && Ano_TJF.equals("yes")) { // planning  condi ttes hsups sont valid�es
							JrFerie jf = (JrFerie) d.getNature();
							annomalie.setDateAnomalie(d.getDate());
							annomalie.setDescription("travail jour ferie :" + jf.getLibelle());
							annomalie.setUser(user);
							annomalie.setType("TJR");
							annomalie.setMatricule("JF-"+user.getMatricule());
							if (d.isHoraireNuit())
								annomalie.setHorNuit(true);
							annoService.create(annomalie);
			    	} else if (d.getNature()!=null && d.getNature() instanceof Conge) {
			    		annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail jour de cong�");
						annomalie.setUser(user);
						annomalie.setType("TJR");
						annomalie.setMatricule("JC-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
			    	}
					if (Ano_TJR.equals("yes")) {
						annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("travail jour de Repos");
						annomalie.setUser(user);
						annomalie.setType("TJR");
						annomalie.setMatricule("JR-"+user.getMatricule());
						if (d.isHoraireNuit())
							annomalie.setHorNuit(true);
						annoService.create(annomalie);
					}
					return;
			}
		} else if (d.isAbsCompensedDay()) {
			annomalie.setDateAnomalie(d.getDate());
			annomalie.setDescription("Travail jour d'un repos compensateur");
			annomalie.setUser(user);
			annomalie.setType("TJC");
			if (d.isHoraireNuit())
				annomalie.setHorNuit(true);
			annomalie.setMatricule("JC-"+user.getMatricule());
			annoService.create(annomalie);
		}
		
		if (d.getMouvements().size()%2 != 0) {
			annomalie = new AnnomalieImpl(); 
			annomalie.setDateAnomalie(d.getDate());
			annomalie.setDescription("Warning : Nombre impaire de pointage ("+d.getMouvements().size()+" mouvements)");
			annomalie.setUser(user);
			annomalie.setType("NIP");
			annomalie.setMatricule("NIP-"+user.getMatricule());
			annoService.create(annomalie);
		}
		if (d.getPlanning() == null)
			return;
		MouvementImpl mvtAdd = null;
		Calendar input = new GregorianCalendar();
		if (d.isFlexible())
			d.setTmworked(0.0);
		int index = 0;
		d.getMvtsAllDay().clear();
		d.getMvtsAllDay().addAll(d.getMouvements());
		for (Plage plage : d.getHrFixes()){ 
			index++;
			plage.setIndex(index);
			String anot = plage.getPlageHor().getAnnotation().getTypeAnnotation().getLibelle();
	    	if (anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE)))
	    		d.setLibelle(MessageFactory.getMessage(this.HORAIRE_LIBRE));
			if (this.liveState ) {
				Calendar cal = Calendar.getInstance();
				if (plage.getDebut().after(cal))
					break;
				else {
					if (!anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE))
							&& !anot.equals(MessageFactory.getMessage(this.SANS_POI_COMPTEUR)))
						projection(plage , d, user, input); 
					continue;
				} 
			} else if (!anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE))
						&& !anot.equals(MessageFactory.getMessage(this.SANS_POI_COMPTEUR)))
				projection(plage , d, user, input); 	// projeter la liste des mvts sur la plage horaire fixe	
			
			// ajouter les pointages des plages type "sans pointage heures supplimentaires" dans la liste des mouvements
			if (anot.equals(MessageFactory.getMessage(this.SANS_POI_COMPTEUR))) {
				mvtAdd = new MouvementImpl();
				mvtAdd.setBadge(user.getBadge());
				mvtAdd.setDate(plage.getDebut().getTime());
				d.getMouvements().add(mvtAdd);
				mvtAdd = new MouvementImpl();
				mvtAdd.setBadge(user.getBadge());
				mvtAdd.setDate(plage.getFin().getTime());
				d.getMouvements().add(mvtAdd);
			} if (anot.equals(MessageFactory.getMessage(this.OUT_SANS_POI_COMPTEUR))) {
				mvtAdd = new MouvementImpl();
				mvtAdd.setBadge(user.getBadge());
				mvtAdd.setDate(plage.getFin().getTime());
				d.getMouvements().add(mvtAdd);
			}
			Utils.sortMvtsByTime(d.getMouvements());
		}
		
		Calendar cal = new GregorianCalendar();
		cal.setTime(d.getDate());
		cal.add(Calendar.DAY_OF_MONTH, -1);
		boolean isAbs = false;
		index = 0;
		for (Plage plage : d.getHrFixes()) {
			index++;
			String anot = plage.getPlageHor().getAnnotation().getTypeAnnotation().getLibelle();
	    	if (anot.equals(MessageFactory.getMessage(this.HORAIRE_LIBRE)))
	    		continue;
	    	if (!plage.isEntree() || !plage.isSortie())
	    		isAbs = true;
	    	annomalie = new AnnomalieImpl();
			if (plage.isEntree() && !plage.isSortie() && !this.liveState) {				
				annomalie.setDateAnomalie(d.getDate());
				annomalie.setDescription("Omission de pointage � la sortie :"+plage.getEndHour());
				annomalie.setUser(user);
				annomalie.setType("OPJO");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("S-"+user.getMatricule());
				annoService.create(annomalie);
				
				AbsenceEffImpl absence = new AbsenceEffImpl();
				absence.setDateDebut(d.getDate());	
				absence.setDateReprise(d.getDate());
				absence.setDuree(0);
				absence.setHeureDebut(Double.parseDouble(plage.getStartHour().replace(':', '.')));
				absence.setHeureReprise(Double.parseDouble(plage.getEndHour().replace(':', '.')));
				absence.setCollaborateur(user);
				absence.setIndexPlage(index);
				absenceEffService.create(absence);
			} else if (!plage.isEntree() && plage.isSortie() && !this.liveState) {
				annomalie.setDateAnomalie(d.getDate());
				annomalie.setDescription("Omission de pointage � l'entr�e :"+plage.getStartHour());
				annomalie.setUser(user);
				annomalie.setType("OPJO");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("E-"+user.getMatricule());
				annoService.create(annomalie);
				AbsenceEffImpl absence = new AbsenceEffImpl();
				absence.setDateDebut(d.getDate());
				absence.setDateReprise(d.getDate());
				absence.setDuree(0);
				absence.setHeureDebut(Double.parseDouble(plage.getStartHour().replace(':', '.')));
				absence.setHeureReprise(Double.parseDouble(plage.getEndHour().replace(':', '.')));
				absence.setCollaborateur(user);
				absence.setIndexPlage(index);
				absenceEffService.create(absence);
			} else if (!plage.isEntree() && !plage.isSortie()) {
					AbsenceImpl absence = new AbsenceImpl();
					if (plage.isPlageNuit()) {
						absence.setDateDebut(cal.getTime());
						absence.setHorNuit(true);
					} else {
						absence.setDateDebut(d.getDate());
						absence.setHorNuit(false);
					}
					absence.setDateReprise(d.getDate());
					absence.setDuree(0);
					absence.setHeureDebut(Double.parseDouble(plage.getStartHour().replace(':', '.')));
					absence.setHeureReprise(Double.parseDouble(plage.getEndHour().replace(':', '.')));
					absence.setCollaborateur(user);
					absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					List<Absence> l = getAbsencesIfValidated(absence);
					if (l == null || l.isEmpty()) {
						absence.setTypeAbsence(noJustifyAbsence);	
						absenceService.create(absence);				
					} else {
						d.getLegalAbs().addAll(l);
						for (Absence ab : l) {
							if (ab == null)
								continue;
							ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
							if (ab.getTypeAbsence().getLibelle() == null)
								continue;
							absenceService.create(ab);
						}
					}
				}
		}
		
		if (!d.getLegalAbs().isEmpty()) {
			AbsenceImpl absence = new AbsenceImpl();
			absence.setDuree(d.getP_tmcontract());
			Double duree = absence.minus(d.getLegalAbs());
			if (duree > 0) {
				absence.setDateDebut(d.getDate());
				absence.setDateReprise(d.getDate());
				if(d.isHoraireNuit())
					absence.setHorNuit(true);
				else
					absence.setHorNuit(false);
				absence.setCollaborateur(user);
				absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
				absence.setTypeAbsence(noJustifyAbsence);
				absence.setDuree(duree);
				absenceService.create(absence);
			}
		}
		
		if (d.isFlexible() && !isAbs) {
			if (d.getTmworked() < d.getP_tmcontract()) {
				double diff = Utils.differenceHeures(d.getP_tmcontract(), d.getTmworked());
    			annomalie.setDateAnomalie(d.getDate());
    			annomalie.setDescription("Non respect de temps contractuel avec '" + diff +"' de difference le cas d'horaire flexibe !!");
    			annomalie.setUser(user);
    			annomalie.setType("NRT");
    			annomalie.setMatricule("JT-"+user.getMatricule());
    			if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annoService.create(annomalie);
			}
		}
			
				
    }
    
    
    
    public void traiterHSupp(Day d, User user) throws Exception {
    	Annomalie annomalie = new AnnomalieImpl();
		if (d.getLibelle().equals(MessageFactory.getMessage(this.HORAIRE_LIBRE))) {
			if (d.getMouvements().size()%2 != 0) {
				annomalie.setDateAnomalie(d.getDate());
				annomalie.setDescription("Omission de pointage cas d'horaire Libre.");
				annomalie.setUser(user);
				annomalie.setType("OPHL");
				if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annomalie.setMatricule("E-"+user.getMatricule());
				annoService.create(annomalie);
			} else if (d.getTmworked() < d.getP_tmcontract()) {
				double diff = Utils.differenceHeures(d.getP_tmcontract(), d.getTmworked());
    			annomalie.setDateAnomalie(d.getDate());
    			annomalie.setDescription("Non respect de temps contractuel avec '" + diff +"' de difference le cas d'horaire libre !!");
    			annomalie.setUser(user);
    			annomalie.setType("NRT");
    			annomalie.setMatricule("JT-"+user.getMatricule());
    			if (d.isHoraireNuit())
					annomalie.setHorNuit(true);
				annoService.create(annomalie);
			}
			if (!d.isJrRepos() || !(d.getNature() != null && d.getNature() instanceof JrFerie))
				return;
		}
		
    	if (d.isHrSuppDay()) {
			Plage plage;
			if (this.hrSuppsValidates != null && !this.hrSuppsValidates.isEmpty())
				for (HeurSupp hrSuppVal : hrSuppsValidates) { 
			    	if (d.getDate().compareTo(hrSuppVal.getDateDebut()) == 0) {
		    			plage = new Plage(d.getDate(), hrSuppVal);
		    			d.getHrSupps().add(plage);
			    	}
		    	}
			d.sortPlages(d.getHrSupps());
		}
		
		if (d.isEnMasse()) {
			getMvtsByDate(d);
    		Utils.sortMvtsByTime(d.getMouvements());
			Utils.filterMvtsByDelta(d.getMouvements(), DT_mvt_prm);
		}

		List<Mouvement> list = new ArrayList<Mouvement>(d.getMouvements());
    	Calendar d1 = new GregorianCalendar();
		Calendar d2 = new GregorianCalendar();
		
		if (list.size()%2 != 0) 
			list.remove(list.size() - 1);

		long somme = 0;
		boolean passSeuil = false;
		for (int i = 0; i < list.size(); i++) {
			
			Mouvement mvmnt1 = list.get(i);
			if (mvmnt1.isTraited())
				d.getMouvements().remove(mvmnt1);
			
			i++;
			
			Mouvement mvmnt2 = list.get(i);
			if (mvmnt2.isTraited())
				d.getMouvements().remove(mvmnt2);
			
			d1.setTime(mvmnt1.getDate());
			d2.setTime(mvmnt2.getDate());
			long diff = Utils.differenceDates(d1.getTime(), d2.getTime());	
			if (d.isEnMasse()) {
				if (d.isJrRepos() && !(d.getNature() instanceof JrFerie)){
					Hashtable<String, String> props = new Hashtable<String, String>();
					HeurSupp hrSuppEf = new HeurSuppImpl();
					hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
		    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
		    		hrSuppEf.setDuree(Utils.differenceHeures(Utils.getHourDoubleFormat(d1.getTime()), Utils.getHourDoubleFormat(d2.getTime())));
		    		if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
		    			hrSuppEf.setCategory("C");
		    		} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
		    			hrSuppEf.setCategory("P");
		    		}
		    		props.clear();
		    		props.put("code", "HSUP50");
	    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
		    		heurSuppService.create(hrSuppEf);
		    		continue;
				}
				
				if (d.getNature() != null && d.getNature() instanceof JrFerie && !isJrFerieHsuppEnabled(user, d.getDate()))
					return;
				
				if (d.getTmworked() > d.getP_tmcontract()) {					
					somme += diff;
					Calendar c = new GregorianCalendar();
					c.setTime(d.getDate());
					c.set(Calendar.HOUR_OF_DAY, Sl_hsup_cond_prm.intValue());
					c.set(Calendar.MINUTE, Utils.decimal(Sl_hsup_cond_prm));
					c.set(Calendar.SECOND, 0);
					c.set(Calendar.MILLISECOND, 0);
					long tmcontract = Utils.double2Mills(d.getP_tmcontract());
		    		HeurSupp hrSuppEf = new HeurSuppImpl();
					hrSuppEf.setCollaborateur(user);
		    		hrSuppEf.setDateDebut(d.getDate());	
		    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					Hashtable<String, String> props = new Hashtable<String, String>();
					if (somme > tmcontract) {
			    		if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
			    			hrSuppEf.setCategory("C");
			    		} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
			    			hrSuppEf.setCategory("P");
			    		}
						if (c.compareTo(d1) >= 0 && c.compareTo(d2) <= 0) {
							d2.add(Calendar.MILLISECOND, - (int)(somme - tmcontract));
				    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d2.getTime()));
				    		hrSuppEf.setHeureFin(Sl_hsup_cond_prm);
				    		hrSuppEf.setDuree(Utils.differenceHeures(Utils.getHourDoubleFormat(d2.getTime()), Sl_hsup_cond_prm));
				    		props.put("code", "HSUP25");
			    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
				    		heurSuppService.create(hrSuppEf);
				    		hrSuppEf = new HeurSuppImpl();
				    		d2.add(Calendar.MILLISECOND, (int)(somme - tmcontract));
							hrSuppEf.setCollaborateur(user);
				    		hrSuppEf.setDateDebut(d.getDate());	
				    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
				    		if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
				    			hrSuppEf.setCategory("C");
				    		} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
				    			hrSuppEf.setCategory("P");
				    		}
				    		hrSuppEf.setHeureDebut(Sl_hsup_cond_prm);
				    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
				    		hrSuppEf.setDuree(Utils.differenceHeures(Sl_hsup_cond_prm, Utils.getHourDoubleFormat(d2.getTime())));
				    		props.clear();
				    		props.put("code", "HSUP50");
			    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
				    		heurSuppService.create(hrSuppEf);
				    		passSeuil = true;
						} else if (passSeuil) {
							hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
				    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
				    		hrSuppEf.setDuree(Utils.differenceHeures(Utils.getHourDoubleFormat(d1.getTime()), Utils.getHourDoubleFormat(d2.getTime())));
				    		props.put("code", "HSUP50");
			    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
				    		heurSuppService.create(hrSuppEf);
						} else {
							d1.add(Calendar.MILLISECOND, (int)(tmcontract - somme + diff));							
							hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
				    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
				    		hrSuppEf.setDuree(Utils.differenceHeures(Utils.getHourDoubleFormat(d1.getTime()), Utils.getHourDoubleFormat(d2.getTime())));
				    		props.put("code", "HSUP25");
			    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
				    		heurSuppService.create(hrSuppEf);
						}
					}
				}
				continue;
			}
			if (diff <= DT_hsp_prm * 60000)
				continue;
			if  (d.getPlanning().getCategHrsp() == 1 || d.getPlanning().getCategHrsp() == 0) { // Planning-adm-agr-faraj
				Hashtable<String, String> props = new Hashtable<String, String>();
				HeurSupp hrSuppEf = new HeurSuppImpl();
				hrSuppEf.setCollaborateur(user);
	    		hrSuppEf.setDateDebut(d.getDate());	
	    		hrSuppEf.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
	    		hrSuppEf.setHeureDebut(Utils.getHourDoubleFormat(d1.getTime()));
	    		hrSuppEf.setHeureFin(Utils.getHourDoubleFormat(d2.getTime()));
	    		hrSuppEf.setDuree(Utils.differenceHeures(Utils.getHourDoubleFormat(d1.getTime()), Utils.getHourDoubleFormat(d2.getTime())));
	    		if (d.getPlanning().getCategHrsp() == 0) { // Planning-cond-m
	    			hrSuppEf.setCategory("C");
		    		props.clear();
		    		props.put("code", "HSUP50");
	    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
	    		} else if (d.getPlanning().getCategHrsp() == 1) { // Planning-cond-h-j
	    			hrSuppEf.setCategory("P");
		    		props.clear();
		    		props.put("code", "HSUP50");
	    			hrSuppEf.setAnnotation((Annotation)annotService.read(props).get(0));
	    		}
	    		heurSuppService.create(hrSuppEf);
	    		continue;
			}
			compareProgHrSupp(d, d1, d2, user);			
			annomalie = new AnnomalieImpl();
			diff = Utils.differenceDates(d1.getTime(), d2.getTime());	
			if (diff <= DT_hsp_prm * 60000)
				continue;
	    	if (!d1.equals(d2)) {
	    		HeurSupp h = getHrSuppInValidated(d1.getTime(), d2.getTime());
	    		if (h == null) {
		    		String heureSupp1 = Utils.toString(d1.get(Calendar.HOUR_OF_DAY)) + ":" + Utils.toString(d1.get(Calendar.MINUTE)); // si heure ou minute < 10 on ajout un 0 en amont exple : h = 7 => stringH = 07
		    		String heureSupp2 = Utils.toString(d2.get(Calendar.HOUR_OF_DAY)) + ":" + Utils.toString(d2.get(Calendar.MINUTE));
					annomalie.setDescription("Non autoris� aux heures supplimentaire de '" + heureSupp1 +"' � '" + heureSupp2 + "'");
					annomalie.setDateAnomalie(d.getDate());
					annomalie.setUser(user);
					annomalie.setType("NAHS");
					annomalie.setMatricule("NATR-"+user.getMatricule());	
					if (d.isHoraireNuit())
						annomalie.setHorNuit(true);
					try {
						annoService.create(annomalie);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	    		} 
	    	}
		}
    }
    
    public void saveHolidays(User user) throws Exception{ 
    	Conge c1 = null;
    	Conge c2 = null;
    	List<Date> jrRepos = new ArrayList<Date>();
    	for (int i = 0; i < days.size(); i++) {
			if (!days.get(i).getLibelle().equals("conge") || !days.get(i).getMvtsAllDay().isEmpty())
				continue;
			
			c1 = (Conge)days.get(i).getNature();
			int duree = 1;
			int j = i + 1;
			while(j < days.size()) {
				if (days.get(j).getLibelle().equals("conge") && days.get(i).getMvtsAllDay().isEmpty()){
					c2 = (Conge)days.get(j).getNature();
					if (c2.getTypeConge().getLibelle().equalsIgnoreCase(c1.getTypeConge().getLibelle())) {
						duree++;
						j++;
					} else
						break;
				} else
					break;
					
			}
			
			Conge cEff = new CongeImpl();
			cEff.setCollaborateur(user);
			cEff.setTypeConge(c1.getTypeConge());
			cEff.setDateDebut(days.get(i).getDate());
			cEff.setDateReprise(days.get(j - 1).getDate());
			cEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			cEff.setNbrJour(duree);
			congeService.create(cEff);
			i = j;
//			BeanUtils.copyProperties(c, cEff);
//			cEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
//			if (c.getDateDebut().before(days.get(0).getDate())) {
//				int diff = getNbrJour(days.get(0).getDate(), c.getDateDebut(), user);
//				cEff.setNbrJour(cEff.getNbrJour() - diff);
//				cEff.setDateDebut(days.get(0).getDate());
//			}
//			if (!c.getDateReprise().after(days.get(days.size() - 1).getDate())) {
//				if (!jrRepos.isEmpty() && contains(cEff, jrRepos) > 0) {
//					cEff.setNbrJour(cEff.getNbrJour() - contains(cEff, jrRepos));
//				}
//				congeService.create(cEff);					
//			} else {
//				int diff = getNbrJour(c.getDateReprise(), days.get(days.size() - 1).getDate(), user);
//				cEff.setNbrJour(cEff.getNbrJour() - diff);
//				cEff.setDateReprise(days.get(days.size() - 1).getDate());
//				if (!jrRepos.isEmpty() && contains(cEff, jrRepos) > 0) {
//					cEff.setNbrJour(cEff.getNbrJour() - contains(cEff, jrRepos));
//				}
//				cEff.setId(0);
//				congeService.create(cEff);
//				break;
//			}
//	    	while((i < days.size()) && !c.getDateReprise().equals(days.get(i).getDate()))
//	    		i++;
//			
			
    	}
	}    

    public int contains(Conge c, List<Date> jrRepos) {
    	Calendar c1 = new GregorianCalendar();
    	c1.setTime(c.getDateDebut());
    	Calendar c2 = new GregorianCalendar();
    	c2.setTime(c.getDateReprise());
    	int duree = 0;
    	while(!c1.equals(c2)) {
    		if (jrRepos.contains(c1.getTime()))
    				duree++;
    		c1.add(Calendar.DAY_OF_MONTH, 1);
    	}
    	return duree;
    }
    
    public int getNbrJour(Date d1, Date d2, User u){
    	Calendar c1 = new GregorianCalendar();
    	c1.setTime(d2);
    	Calendar c2 = new GregorianCalendar();
    	c2.setTime(d1);
    	int duree = 0;
    	Planning pl;
    	Day d;
    	while(!c1.equals(c2)) {
    		d = new Day();
    		d.setDate(c1.getTime());
    		pl = Utils.findAffectPlanning(c1.getTime(), u);
			if (pl != null) 
				d.setPlanning(pl);
    		if (!d.isJrRepos() && Utils.isJrFerie(d.getDate(), (List)this.jrFeries) != null)
    				duree++;
    		c1.add(Calendar.DAY_OF_MONTH, 1);
    	}
    	return duree;
    }
    
    public boolean existInDates(Calendar thisDate){
    	Calendar cal = new GregorianCalendar();
    	for (int i = 0; i < days.size(); i++) {
			Day d = days.get(i);
			cal.setTime(d.getDate());
			if (cal.equals(thisDate))
				return true;	
    	}
    	return false;
    }
    
	private boolean isJrFerieHsuppEnabled(User u, Date d) throws Exception{
		Calendar cal = new GregorianCalendar();
    	cal.setTime(d);
    	cal.add(Calendar.DAY_OF_MONTH, -1);
    	
    	CommonCriteria crit = new CommonCriteria();
    	crit.setMatriculeInf(u.getMatricule());
    	crit.setMatriculeSup(u.getMatricule());
    	crit.setDateDebut(cal.getTime());
    	crit.setDateFin(cal.getTime());
    	
    	int count = mvmntService.countWorkedDaysByCriteria(crit);
    	if (count > 0)
    		return true;
    	crit.setDateFin(cal.getTime());
    	cal.setTime(u.getDateEmb());
    	crit.setDateDebut(cal.getTime());
    	count = mvmntService.countWorkedDaysByCriteria(crit);
    	if (count >= 13)
    		return true;
    	return false;
    }

    public boolean isJrRepos(Day day) throws Exception {	
    	if (day.isJrRepos())
			return true;
    	Object o = Utils.isJrFerie(day.getDate(), (List)this.jrFeries);
    	if (o != null) {
    		day.setLibelle(((JrFerie)o).getLibelle());
    		day.setNature(o);
    		day.setJrRepos(true);
    		return true;
    	}
    	o = Utils.isHolidays(day.getDate(), (List)this.congesValidates);
    	if (o != null) {
    		day.setLibelle("conge");
    		day.setNature(o);
    		day.setJrRepos(true);
    		return true;
    	}
		return false;
    }
    
  
    public void isAbsCompDay(Day day) throws Exception {
    	if (this.compValidates == null || this.compValidates.isEmpty())
    		return;
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal = new GregorianCalendar();
    	cal.setTime(day.getDate());	
    	for (Compensation comp : compValidates) {
    		cal1.setTime(comp.getAbsence().getDateDebut());
	    	cal1.set(Calendar.HOUR_OF_DAY, 0);
	    	cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal) == 0) {				
				if (comp.getAbsence().getHeureDebut() == comp.getAbsence().getHeureReprise()) {					
		    		if (day.isJrAbsence()) {
						Absence abs = new AbsenceImpl();
						BeanUtils.copyProperties(comp.getAbsence(), abs);
						abs.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
						if (day.isHoraireNuit())
							abs.setHorNuit(true);
						abs.setId(0);
						absenceService.create(abs);
						
			    		Compensation compEff = new CompensationImpl();
						BeanUtils.copyProperties(comp, compEff);
						compEff.setId(0);
						compEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
			    		compEff.setAbsence(abs);
			    		compensationService.create(compEff);
		    		}
					day.setAbsCompensedDay(true);
				}				
				return;
			}
    	}
    	day.setAbsCompensedDay(false);
    }
    
    public void isAbsRecupDay(Day day) throws Exception {
    	if (this.recupsValidates == null || this.recupsValidates.isEmpty())
    		return;
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal = new GregorianCalendar();
    	cal.setTime(day.getDate());    	
    	for (Recuperation recup : recupsValidates) { 
    		cal1.setTime(recup.getAbsence().getDateDebut());
	    	cal1.set(Calendar.HOUR_OF_DAY, 0);
	    	cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal) == 0) {
				if (recup.getAbsence().getHeureDebut() == recup.getAbsence().getHeureReprise()) 
					day.setAbsRecuperedDay(true);				
				return;
			}
    	}
    	day.setAbsRecuperedDay(false);
    }
    	
	public void isCompDay(User user,Day day) throws Exception {
		if (this.compValidates == null || this.compValidates.isEmpty())
			return;
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal2 = new GregorianCalendar();
    	cal1.setTime(day.getDate());  
    	Double diff = 0.0;
    	for (Compensation comp : compValidates) { 
    		cal2.setTime(comp.getDateCom());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
	    	cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
	    	if (cal1.compareTo(cal2) == 0) {	    		    			
        		Annomalie annomalie = new AnnomalieImpl();
        		annomalie.setDateAnomalie(comp.getAbsence().getDateDebut());
    			annomalie.setUser(user);
    			annomalie.setType("NRT");
    			diff = Utils.differenceHeures(comp.getTmContract(), day.getTmworked());
        		if (day.getTmworked() < comp.getTmContract()) {
	    			annomalie.setDescription("annomalie non respect de temps contractuel avec '" + diff +"' de moins lors de 'compensation'!!");
	    			annomalie.setMatricule("JCM-"+user.getMatricule());	  
    				annoService.create(annomalie);
	        	} 
        		if (day.getTmworked() > comp.getTmContract()) {
        			HeurSupp hrSuppEff = new HeurSuppImpl();
	    			hrSuppEff.setCollaborateur(user);
	    			hrSuppEff.setDateDebut(day.getDate());
	    			hrSuppEff.setDuree(diff);
	    			Hashtable<String, String> props = new Hashtable<String, String>();
					props.put("code", "HSUP100");
					hrSuppEff.setAnnotation((Annotation)annotService.read(props).get(0));
	    			hrSuppEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
	    			hrSuppEff.setMotif("Compensation");
    				heurSuppService.create(hrSuppEff);
        		}
        		day.setCompensedDay(true);
	    		return;
	    	}
    	}    
    }
    
    public void isRecpDay(User user, Day day) throws Exception {
    	if (this.recupsValidates == null || this.recupsValidates.isEmpty())
    		return;
    	
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal2 = new GregorianCalendar();
    	cal1.setTime(day.getDate());    	
    	Double diff = 0.0;
    	for (Recuperation recup : recupsValidates) {
	    	cal2.setTime(recup.getDateRec());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
	    	cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
	    	if (cal1.compareTo(cal2) == 0) {
    			diff = Utils.differenceHeures(recup.getAbsence().getDuree(), day.getTmworked());
	    		if (day.getTmworked() < recup.getAbsence().getDuree()) {
	        		Annomalie annomalie = new AnnomalieImpl();
	    			annomalie.setDateAnomalie(recup.getAbsence().getDateDebut());
	    			annomalie.setDescription("annomalie non respect de temps contractuel avec '" + diff +"' de moins lors de 'recuperation'!!");
	    			annomalie.setUser(user);
	    			annomalie.setType("NRT");
	    			if (day.isHoraireNuit())
						annomalie.setHorNuit(true);
	    			annomalie.setMatricule("JRC-"+user.getMatricule());
    				annoService.create(annomalie);
	        	}
	    		if (day.getTmworked() > recup.getAbsence().getDuree()) {
        			HeurSupp hrSuppEff = new HeurSuppImpl();
	    			hrSuppEff.setCollaborateur(user);
	    			hrSuppEff.setDateDebut(day.getDate());
	    			hrSuppEff.setDuree(diff);
	    			Hashtable<String, String> props = new Hashtable<String, String>();
					props.put("code", "HSUP100");
					hrSuppEff.setAnnotation((Annotation)annotService.read(props).get(0));
	    			hrSuppEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
	    			hrSuppEff.setMotif("Compensation");
    				heurSuppService.create(hrSuppEff);
        		}
	    		return;
	        }
    	} 
    }
    
    public boolean isInRecuperation(Absence abs) {
    	if (this.absRecs == null || this.absRecs.isEmpty())
    		return false;
    	
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal2 = new GregorianCalendar();
    	cal1.setTime(abs.getDateDebut());   
    	Recuperation recbis = null;
    	for (Recuperation rec : absRecs) {
	    	cal2.setTime(rec.getAbsence().getDateDebut());
			cal2.set(Calendar.HOUR_OF_DAY, 0);
	    	cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
	    	if (cal1.compareTo(cal2) == 0) {
	    		recbis = rec;
	    		break;
	    	}
    	}
    	if (recbis != null) {
    		absRecs.remove(recbis);
    		return true;
    	}
    	return false;
    }
       
    
    public int stateAbsComp(Day day, Double duree, User user) throws Exception {
    	if (this.compValidates == null || this.compValidates.isEmpty())
    		return -1;
    	Calendar cal1 = new GregorianCalendar();
    	Calendar cal2 = new GregorianCalendar();
    	Hashtable<String, Object> props = new Hashtable<String, Object>();	
    	
    	for (Compensation comp : compValidates) {
    		cal1.setTime(comp.getAbsence().getDateDebut());
	    	cal1.set(Calendar.HOUR_OF_DAY, 0);
	    	cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
			
			cal2.setTime(day.getDate());
	    	cal2.set(Calendar.HOUR_OF_DAY, 0);
	    	cal2.set(Calendar.MINUTE, 0);
			cal2.set(Calendar.SECOND, 0);
			cal2.set(Calendar.MILLISECOND, 0);
			if (cal1.compareTo(cal2) == 0) {
				props.put("type", "NRT");
				props.put("dateAnomalie", comp.getDateCom());
				props.put("user", user);
				List list = annoService.read(props);
				Double d = null;
				if (list != null && !list.isEmpty()) {
					Annomalie anomalie = (Annomalie)list.get(0);
					StringTokenizer tokenizer 
						= new java.util.StringTokenizer(anomalie.getDescription(), "'");
					tokenizer.nextToken();
					String diff = tokenizer.nextToken();
					if (anomalie.getMatricule().substring(0, 3).equals("JCP")) 	
						d = Utils.additionHeures(comp.getTmContract(), new Double(diff));
					else if (anomalie.getMatricule().substring(0, 3).equals("JCM")) 
						d = Utils.differenceHeures(comp.getTmContract(), new Double(diff));					
				} else
					d = comp.getTmContract();
				if (d == 0) {
					return -1;
				}else if (duree > d) {
//					comp.getAbsence().setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
//		    		absenceService.update(comp.getAbsence());
//		    		comp.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
//		    		compensationService.update(comp);			
					return 1;
				} else {
		    		Absence abs = new AbsenceImpl();
					BeanUtils.copyProperties(comp.getAbsence(), abs);
					abs.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
					if (day.isHoraireNuit())
						abs.setHorNuit(true);
					abs.setId(0);
		    		absenceService.create(abs);
		    		Compensation compEff = new CompensationImpl();
					BeanUtils.copyProperties(comp, compEff);
					compEff.setId(0);
					compEff.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
		    		compEff.setAbsence(abs);
		    		compensationService.create(compEff);
					return 2;
				}
    		}
    	}
    	return -1;
    }
    
    public HeurSupp getHrSuppInValidated(Date d1, Date d2) {
    	if (this.hrSuppsValidates == null || this.hrSuppsValidates.isEmpty())
    		return null;
    	for (HeurSupp h : hrSuppsValidates) {
			if (!d1.before(Utils.getCompleteDate(h.getDateDebut(), h.getHeureDebut())) 
					&& !d1.after(Utils.getCompleteDate(h.getDateDebut(), h.getHeureFin()))
					&& !d2.before(Utils.getCompleteDate(h.getDateDebut(), h.getHeureDebut())) 
					&& !d2.after(Utils.getCompleteDate(h.getDateDebut(), h.getHeureFin())))
				return h;
			else if ((d1.before(Utils.getCompleteDate(h.getDateDebut(), h.getHeureDebut())) 
						&& !d2.before(Utils.getCompleteDate(h.getDateDebut(), h.getHeureDebut())) 
						&& !d2.after(Utils.getCompleteDate(h.getDateDebut(), h.getHeureFin())))
					 || (d2.after(Utils.getCompleteDate(h.getDateDebut(), h.getHeureFin())) 
							 && !d1.before(Utils.getCompleteDate(h.getDateDebut(), h.getHeureDebut())) 
							 && !d1.after(Utils.getCompleteDate(h.getDateDebut(), h.getHeureFin())))) {
				try {
					heurSuppService.delete(h);
					return null;
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
					
		}
    	return null;
    }
    
    public List<Absence> getAbsencesIfValidated(Absence abEff) {
    	if (this.absencesValidates == null || this.absencesValidates.isEmpty())
    		return null;
    	
    	List<Absence> l = new ArrayList<Absence>();
    	for (Absence av : absencesValidates) {  
			List<Absence> res = ((AbsenceImpl)av).intersect(abEff);
			if (res != null)
				l.addAll(res);
		}
    	return l;
    }   

 
	public void processOnline(CommonCriteria userCriteria, Planning plAtribute) {
		this.liveState =true;
		process(userCriteria, plAtribute);
		
	}

    public void process(CommonCriteria criteria, Planning plAtribute ){
    	try {
    		
			System.out.println("1 - Start Process ...");
    		CommonCriteria userCriteria = new CommonCriteria();
			//BeanUtils.copyProperties(criteria, userCriteria);
    		userCriteria = criteria;
			if (userCriteria.getDateDebut() == null 
					|| userCriteria.getDateFin() == null) // dateDebut et dateFin obligatoires
				return;
			
			
			Hashtable<String, String> typeAbsProps = new Hashtable<String, String>();
			typeAbsProps.put("code", MessageFactory.getMessage(NOJUSTIFY));
			
			firstInit(this.userService.readByCriteria(userCriteria), 
				jrFerieService.readAll(),
				typeAbsenceService.read(typeAbsProps));
			
			if ((this.users == null || this.users.isEmpty()))				
				return;
						
			logger.error("Period to analyse : ");
			logger.error("from : ["+Utils.toStrDate(userCriteria.getDateDebut(), "dd/MM/yyyy")+"]");
			logger.error("to : ["+Utils.toStrDate(userCriteria.getDateFin(), "dd/MM/yyyy")+"]");
			logger.error("NB users :: " + ((this.users == null || this.users.isEmpty())? 0: this.users.size()));
			
			// vocate database
			userCriteria.setStatut(MessageFactory.getMessage(this.EFFECTUE_STATUT));
			annoService.removeCriteria(userCriteria);
			logger.error("Clean old data...");
			
			/**
			 * counstruit la liste des jour à traiter
			 */
			buildDaysList(userCriteria);			
			CommonCriteria crit = new CommonCriteria();
			
			
			Map<Planning, List<Integer>> indexs;
			Planning pl = null;
			List<Mouvement> mvts;
			List<Day> ds = new ArrayList<Day>(days);
			int n = 0;
			DynPlaDetail dynPlaDetail;
			for (Day d: ds) {
				crit.setDateDebut(d.getDate());
				crit.setDateFin(d.getDate());
				indexs = new HashMap<Planning, List<Integer>>();
				for (User user: users) {
					pl = Utils.findAffectPlanning(d.getDate(), user);
					pl = Utils.deproxy(pl, Planning.class);
					if (pl == null || !Utils.inCyclicPlg(d.getDate(), pl))
						continue;
					d.setPlanning(pl);
					if (d.isEnMasse()) {
						if (indexs.containsKey(pl))
							indexs.get(pl).add(user.getId());
						else {
							indexs.put(pl, new ArrayList(user.getId()));
							indexs.get(pl).add(user.getId());
						}
					}
					d.cleanDay();
				}
				Set<Planning> keys = indexs.keySet();
				for (Planning pla: keys) {
					mvmnts = new ArrayList<Mouvement>();
					List<Integer> inds =indexs.get(pla);
					int eff = 0;
					for (int i: inds) {	
						crit.getIndexs().clear();	
						crit.getIndexs().add(i);
						mvts = mvmntService.readByCriteria(crit);
						if (mvts != null && !mvts.isEmpty())
							Utils.filterMvtsByDelta(mvts, DT_mvt_prm);
						else
							continue;
						mvmnts.addAll(mvts);
						eff++;
					}		
					if (!mvmnts.isEmpty()) {
						List<Date> inOut = Utils.getInOuts((List)mvmnts, DT_msFocus_prm, eff);
						if (!inOut.isEmpty()){ 
							days.get(n).getSmartHor().put(pla, inOut);
							//########### ?????? TEST AJOUTE PAR KHALID
							//d.setPlanning(pla);
							// clean Dyn_Pla_Detail
							if (!liveState) {
								logger.error("DETECT DYN PLANNING ...");
								dynPlaDetailService.removeByCriteria(d.getDate(), pla.getId());
								for (int i = 0; i < inOut.size(); i++) {
									if(i%2 == 1) {
										dynPlaDetail = new DynPlaDetailImpl();
										dynPlaDetail.setDatePla(d.getDate());
										dynPlaDetail.setPlanning(pla);
										dynPlaDetail.setDebut(Utils.getHourDoubleFormat(inOut.get(i - 1)));
										dynPlaDetail.setFin(Utils.getHourDoubleFormat(inOut.get(i)));
										dynPlaDetailService.create(dynPlaDetail);
									}
								}
							}
						}
				}
				}
				n++;
			}
			
			for (User user: users) {	
				logger.error("Analyse user :" + user.getMatricule());
				userCriteria.setMatriculeInf(user.getMatricule());
				userCriteria.setMatriculeSup(user.getMatricule());
				userCriteria.setIndexs(null);
				crit = new CommonCriteria();
				//BeanUtils.copyProperties(userCriteria, crit);
				crit = userCriteria;
				
				userCriteria.setStatut(MessageFactory.getMessage(this.VALIDATE_STATUT));
				secondInit(mvmntService.readByCriteria(userCriteria),
						heurSuppService.readByCriteria(userCriteria),
						congeService.readByCriteria(userCriteria), 
						absenceService.readByCriteria(userCriteria),
						recuperationService.readByCriteria(userCriteria),
						compensationService.readByCriteria(userCriteria),
						recuperationService.readIfAbsIn(userCriteria));
				
				
				/**
				 * Localiser le planning pour chaque journée 
				 * affecté au collaborateur
				 */				
				
				for (Day d : days) {
					d.cleanDay();		
					d.setCumulTrait(false);
					d.setLiveState(liveState);
					if (user.getDateEmb()!=null && d.getDate().before(user.getDateEmb())
							|| (user.getDateQuit()!=null && d.getDate().after(user.getDateQuit()))) {
						continue;
					}
	    			getMvtsByDate(d);
		    		Utils.sortMvtsByTime(d.getMouvements());
					Utils.filterMvtsByDelta(d.getMouvements(), DT_mvt_prm);
					pl = Utils.findAffectPlanning(d.getDate(), user);
					pl = Utils.deproxy(pl, Planning.class);
					if (pl != null) {
						if (!Utils.inCyclicPlg(d.getDate(), pl)){
							Annomalie annomalie = new AnnomalieImpl();
				    		annomalie.setDateAnomalie(d.getDate());
							annomalie.setDescription("Planning non affecte");
							annomalie.setUser(user);
							annomalie.setType("PNA");
							annomalie.setMatricule("PNA-"+user.getMatricule());
							annoService.create(annomalie);
							continue;
						} else {
							d.setPlanning(pl);
							if (!d.getHrFixes().isEmpty()) {
								ExceptionProfilMetier excp = Utils.getExceptProfilM(d.getDate(), (List)user.getExceptionProfilMetiers());
								if (excp != null)
									d.addTmsToPlages(excp.getTimings(), excp.getProfilMetier(), excp.getNoeud());
							}
							if (d.getLibelle().equals("dynamique") 
									&& d.getHoraire() == null
									&& (days.indexOf(d) < days.size() -1)) {
								Annomalie annomalie = new AnnomalieImpl();
					    		annomalie.setDateAnomalie(d.getDate());
								annomalie.setDescription("Pointages insuffisants pour fixer l'horaire dynamique");
								annomalie.setUser(user);
								annomalie.setType("PTIN");
								annomalie.setMatricule("PTIN-"+user.getMatricule());
								annoService.create(annomalie);
								continue;
							}
						}
					} else {
						Annomalie annomalie = new AnnomalieImpl();
			    		annomalie.setDateAnomalie(d.getDate());
						annomalie.setDescription("Planning non affecte");
						annomalie.setUser(user);
						annomalie.setType("PNA");
						annomalie.setMatricule("PNA-"+user.getMatricule());
						annoService.create(annomalie);
						continue;
					}
				}
				if (pl == null)
					continue;
				Day d = days.get(0);
				Day f = days.get(days.size() - 1);
				if (Rpt_hnuit_prm.equals("yes") && f.isHoraireNuit() && !f.getHrFixes().isEmpty()) {	    		
					Plage theLast = f.getHrFixes().get(f.getHrFixes().size() - 1);
					Calendar d1 = new GregorianCalendar();
					d1.setTime(userCriteria.getDateFin());
			    	d1.add(Calendar.DAY_OF_MONTH, 1);
					d1.set(Calendar.HOUR_OF_DAY, 0);
			    	d1.set(Calendar.MINUTE, 0);
			    	d1.set(Calendar.SECOND, 0);
			    	d1.set(Calendar.MILLISECOND, 0);
					crit.setDateDebut(d1.getTime());
			    	d1.setTime(theLast.getFin().getTime());
			    	d1.add(Calendar.MINUTE, DT_HNuit_prm);
			    	crit.setDateFin(d1.getTime());
			    	List mvnts = mvmntService.readByCriteria(crit);
			    	if (mvnts != null && !mvnts.isEmpty()) 
			    		f.getMouvements().addAll(mvnts);
			    	annoService.removeCriteria(crit); 
				} else if (Rpt_hnuit_prm.equals("no") && d.isHoraireNuit() && !d.getHrFixes().isEmpty()) {	
					Plage thefirst = d.getHrFixes().get(0);
					Calendar d1 = new GregorianCalendar();
			    	d1.setTime(thefirst.getDebut().getTime());
			    	d1.add(Calendar.MINUTE, -DT_HNuit_prm);
			    	crit.setDateDebut(d1.getTime());
					d1.setTime(userCriteria.getDateDebut());
					d1.set(Calendar.HOUR_OF_DAY, 0);
			    	d1.set(Calendar.MINUTE, 0);
			    	d1.set(Calendar.SECOND, 0);
			    	d1.set(Calendar.MILLISECOND, 0);
					crit.setDateFin(d1.getTime());
			    	List mvnts = mvmntService.readByCriteria(crit);
			    	if (mvnts != null && !mvnts.isEmpty()) 
			    		d.getMouvements().addAll(mvnts);
			    	annoService.removeCriteria(crit); 			
				}
				
				
				/**
				 * traite les mouvements jour par jour
				 * suivant l'horaire
				 */
				for (Day day : days) {	
		    		logger.error("#### jour :" +day.getDate());
		    		if (user.getDateEmb()!=null && day.getDate().before(user.getDateEmb())
							|| (user.getDateQuit()!=null && day.getDate().after(user.getDateQuit()))) {
						continue;
					}
		    		Utils.sortMvtsByTime(day.getMouvements());
		    		if (Rpt_hnuit_prm.equals("no")) {
			    		Day next = day.getNextDay(days);
						if (next != null && next.isHoraireNuit() && !day.getMouvements().isEmpty() && !next.getHrFixes().isEmpty()) {
							Calendar cal = new GregorianCalendar();
							cal.setTime(next.getHrFixes().get(0).getDebut().getTime());
				    		cal.add(Calendar.MINUTE, -DT_HNuit_prm);
				    		Calendar laDate = new GregorianCalendar();
				    		
							List<Mouvement> l = new ArrayList<Mouvement>();
							for(Mouvement m : day.getMouvements()) {
								laDate.setTime(m.getDate());
								if (laDate.compareTo(cal) > 0) {
									l.add(m);
									m.setTraited(false);
									next.getMouvements().add(m);
								}
							}
							day.getMouvements().removeAll(l);
						}
		    		} else if (Rpt_hnuit_prm.equals("yes")) {
			    		Day next = day.getNextDay(days);
						if (next != null && day.isHoraireNuit() && !next.getMouvements().isEmpty() && !day.getHrFixes().isEmpty()) {
							Calendar cal = new GregorianCalendar();
							cal.setTime(day.getHrFixes().get(day.getHrFixes().size() - 1).getFin().getTime());
				    		cal.add(Calendar.MINUTE, DT_HNuit_prm);
				    		Calendar laDate = new GregorianCalendar();
				    		
							List<Mouvement> l = new ArrayList<Mouvement>();
							for(Mouvement m : next.getMouvements()) {
								laDate.setTime(m.getDate());
								if (laDate.compareTo(cal) < 0) {
									l.add(m);
									m.setTraited(false);
									day.getMouvements().add(m);
								}
							}
							next.getMouvements().removeAll(l);
						}
		    		}
		    		Utils.sortMvtsByTime(day.getMouvements());
					Utils.filterMvtsByDelta(day.getMouvements(), DT_mvt_prm);
					
					if (plAtribute != null && !plAtribute.equals(day.getPlanning())) 
						continue;
					
					if (this.hrSuppsValidates != null && !this.hrSuppsValidates.isEmpty()
						&& Utils.isHrSuppDay(day.getDate(), (List)this.hrSuppsValidates) != null) {
						day.setHrSuppDay(true);
					}
			    	isJrRepos(day);
			    	Annomalie annomalie = new AnnomalieImpl();
					if (day.getTmworked() > 0) {
						isCompDay(user, day);
						isRecpDay(user, day);
					} else {
						isAbsCompDay(day);
						isAbsRecupDay(day);
					}
					if (day.isJrAbsence()) {
			    		if (!day.isJrRepos()) {
			    			if (!day.isAbsCompensedDay() && !day.isAbsRecuperedDay()) {
				    			AbsenceImpl absence = new AbsenceImpl();
								absence.setDateDebut(day.getDate());
								absence.setDuree(day.getP_tmcontract());
								absence.setDateReprise(day.getDate());
								absence.setCollaborateur(user);
								absence.setHeureReprise(new Double("23.59"));
								if(day.isHoraireNuit())
									absence.setHorNuit(true);
								else
									absence.setHorNuit(false);
								absence.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
								List<Absence> l = getAbsencesIfValidated(absence);
								if (l == null || l.isEmpty()) {
									absence.setTypeAbsence(noJustifyAbsence);
									absence.setHeureReprise(0);
									if (!isInRecuperation(absence))
										absenceService.create(absence);				
								} else {
									for (Absence ab : l) {
										if (ab == null)
											continue;
										ab.setStatut(MessageFactory.getMessage(EFFECTUE_STATUT));
										if (ab.getTypeAbsence().getLibelle() == null)
											continue;
										absenceService.create(ab);
									}
									absence.setTypeAbsence(noJustifyAbsence);
									Double duree = absence.minus(l);
									if (duree > 0) {
										absence.setDuree(duree);
										absence.setHeureReprise(0);
										absenceService.create(absence);
									}
								}
			    			}
			    		} else {
			    			if (day.isHrSuppDay()) {
								annomalie.setDateAnomalie(day.getDate());
								annomalie.setDescription("Absence le jour de " + day.getLibelle() + " avec une demande d'heures supplimentaires valid�es");
								annomalie.setUser(user);
								annomalie.setType("AJRHV");
								annomalie.setMatricule("JF-"+user.getMatricule());
								if (day.isHoraireNuit())
									annomalie.setHorNuit(true);
								annoService.create(annomalie);
			    			} else if (day.isCompensedDay()) {
								annomalie.setDateAnomalie(day.getDate());
								annomalie.setDescription("Absence le jour de " + day.getLibelle() + " avec une demande de compensation valid�e");
								annomalie.setUser(user);
								annomalie.setType("AJRCV");
								annomalie.setMatricule("JF-"+user.getMatricule());
								if (day.isHoraireNuit())
									annomalie.setHorNuit(true);
								annoService.create(annomalie);
			    			} else if (day.isRecuperedDay()) {
								annomalie.setDateAnomalie(day.getDate());
								annomalie.setDescription("Absence le jour de " + day.getLibelle() + " avec une demande de recuperation valid�e");
								annomalie.setUser(user);
								annomalie.setType("AJRRV");
								annomalie.setMatricule("JF-"+user.getMatricule());
								if (day.isHoraireNuit())
									annomalie.setHorNuit(true);
								annoService.create(annomalie);
			    			}
						}
						continue;
					}
					traiterParHoraire(day, user);
					traiterHSupp(day, user);
		    	}		
		    	
		    	if (absRecs != null && !absRecs.isEmpty()) 
		    		for (Recuperation rec : absRecs) {
		    			Annomalie annomalie = new AnnomalieImpl();
			    		annomalie.setDateAnomalie(new Date());
						annomalie.setDescription("Recuperation le jour de" + Utils.toStrDate(rec.getDateRec(), "dd-MM-yyyy") + " et l'absence associ�e le jour de " + Utils.toStrDate(rec.getAbsence().getDateDebut(), "dd-MM-yyyy") + "ont �t� supprim�es");
						annomalie.setUser(user);
						annomalie.setType("RAS");
						annomalie.setMatricule("RAS-"+user.getMatricule());
						annoService.create(annomalie);
						recuperationService.delete(rec);
		    			absenceService.delete(rec.getAbsence());
					}
		    	
		    	
		    	/**
		    	 * enregistre les jours de congé effectués
		    	 */
		    	saveHolidays(user); 
			}
			
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			
		}
    }

    
    

	public void setLiveState(boolean liveState) {
		this.liveState = liveState;
	}

	public UserManageableService getUserService() {
		return userService;
	}

	public void setUserService(UserManageableService userService) {
		this.userService = userService;
	}

	public AnnomalieManageableService getAnnoService() {
		return annoService;
	}

	public void setAnnoService(AnnomalieManageableService annoService) {
		this.annoService = annoService;
	}

	public AnnotationManageableService getAnnotService() {
		return annotService;
	}

	public void setAnnotService(AnnotationManageableService annotService) {
		this.annotService = annotService;
	}

	public CongeManageableService getCongeService() {
		return congeService;
	}

	public void setCongeService(CongeManageableService congeService) {
		this.congeService = congeService;
	}

	public AbsenceManageableService getAbsenceService() {
		return absenceService;
	}

	public void setAbsenceService(AbsenceManageableService absenceService) {
		this.absenceService = absenceService;
	}

	public AbsenceEffManageableService getAbsenceEffService() {
		return absenceEffService;
	}

	public void setAbsenceEffService(AbsenceEffManageableService absenceEffService) {
		this.absenceEffService = absenceEffService;
	}

	public AdvParamManageableService getAdvParamService() {
		return advParamService;
	}

	public void setAdvParamService(AdvParamManageableService advParamService) {
		this.advParamService = advParamService;
	}

	public TypeAbsenceManageableService getTypeAbsenceService() {
		return typeAbsenceService;
	}

	public void setTypeAbsenceService(
			TypeAbsenceManageableService typeAbsenceService) {
		this.typeAbsenceService = typeAbsenceService;
	}

	public CompensationManageableService getCompensationService() {
		return compensationService;
	}

	public void setCompensationService(
			CompensationManageableService compensationService) {
		this.compensationService = compensationService;
	}

	public RecuperationManageableService getRecuperationService() {
		return recuperationService;
	}

	public void setRecuperationService(
			RecuperationManageableService recuperationService) {
		this.recuperationService = recuperationService;
	}

	public RetardManageableService getRetardService() {
		return retardService;
	}

	public void setRetardService(RetardManageableService retardService) {
		this.retardService = retardService;
	}

	public MouvementManageableService getMvmntService() {
		return mvmntService;
	}

	public void setMvmntService(MouvementManageableService mvmntService) {
		this.mvmntService = mvmntService;
	}

	public JrFerieManageableService getJrFerieService() {
		return jrFerieService;
	}

	public void setJrFerieService(JrFerieManageableService jrFerieService) {
		this.jrFerieService = jrFerieService;
	}

	public HeurSuppManageableService getHeurSuppService() {
		return heurSuppService;
	}

	public void setHeurSuppService(HeurSuppManageableService heurSuppService) {
		this.heurSuppService = heurSuppService;
	}

	public InterruptionManageableService getInterService() {
		return interService;
	}

	public void setInterService(InterruptionManageableService interService) {
		this.interService = interService;
	}

	public void setDynPlaDetailService(
			DynPlaDetailManageableService dynPlaDetailService) {
		this.dynPlaDetailService = dynPlaDetailService;
	}

    
    
}
