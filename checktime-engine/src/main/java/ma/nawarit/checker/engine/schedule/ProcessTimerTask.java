package ma.nawarit.checker.engine.schedule;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimerTask;

import org.andromda.spring.CommonCriteria;

import ma.nawarit.checker.engine.LiveProcessingService;

public class ProcessTimerTask {
	LiveProcessingService liveProcessingService;
	
	public synchronized void execute() {
        System.out.println("Hello world ---" + new Date());
        if (liveProcessingService != null) {
        	CommonCriteria crit = new CommonCriteria();
        	crit.setTypeTreatment("RECL");
        	Calendar c1 = new GregorianCalendar();
        	c1.set(Calendar.HOUR, 0);
        	c1.set(Calendar.MINUTE, 0);
        	c1.set(Calendar.SECOND, 0);
        	c1.set(Calendar.MILLISECOND, 0);
        	Calendar c2 = new GregorianCalendar();
        	c2.set(Calendar.HOUR, 23);
        	c2.set(Calendar.MINUTE, 59);
        	c2.set(Calendar.SECOND, 59);
        	crit.setDateDebut(c1.getTime());
        	crit.setDateFin(c1.getTime());
        	liveProcessingService.process(crit, null);
        }
    }
	
	public void setLiveProcessingService(
			LiveProcessingService liveProcessingService) {
		this.liveProcessingService = liveProcessingService;
	}

}
