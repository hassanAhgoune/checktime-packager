package ma.nawarit.checker.engine;

import java.util.List;

import ma.nawarit.checker.configuration.Planning;

import org.andromda.spring.CommonCriteria;

public interface LiveProcessingService
{
	
	public List<UserReportData> process(CommonCriteria userCriteria, Planning plAtribute );

}
