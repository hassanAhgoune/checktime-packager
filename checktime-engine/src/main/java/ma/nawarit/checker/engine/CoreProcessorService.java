package ma.nawarit.checker.engine;

import ma.nawarit.checker.configuration.Planning;

import org.andromda.spring.CommonCriteria;

public interface CoreProcessorService
{
	
	public void process(CommonCriteria userCriteria, Planning plAtribute );
	public void processOnline(CommonCriteria userCriteria, Planning plAtribute );
	public void setLiveState(boolean liveState);
	

}
