package ma.nawarit.checker.jbpm;

import java.util.List;

import org.jbpm.JbpmException;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;

public interface JbpmManageable {
	
	
	public ProcessDefinition findProcessDefinition(String processDefName) throws JbpmException;
	
	public int createProcessDefinition(String processDefResource) throws JbpmException;
	
	public ProcessInstance loadProcessInstance(int processId) throws JbpmException;
	
	public String getStateName(int processId) throws JbpmException;
	
	public String getVariableValue(int processId, String variableName) throws JbpmException;

	public boolean isStateReached(JbpmStatable jbpmEntity, String stateName) throws JbpmException;
	
	public List<JbpmStatable> fetchObjectsByState(List<JbpmStatable> jbpmEntities, String stateName, String variableName) throws JbpmException;
	
	public void takeDecision(int processId, boolean accept, String varaibleName, String variableValue) throws JbpmException;
	
	public List<JbpmStatable> fetchObjectsWithVariables(List<JbpmStatable> jbpmEntities, String variableName1, String variableName2) throws JbpmException;

}
