package ma.nawarit.checker.jbpm;


public abstract interface JbpmStatable {
	public int getId();
	public int getIdProcess();
	public String getStatut();
	public String getWorkflowVariable();
	public void setWorkflowVariable(String variableValue);
	
}
