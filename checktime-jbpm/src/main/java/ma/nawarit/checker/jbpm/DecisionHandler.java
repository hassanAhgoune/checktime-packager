package ma.nawarit.checker.jbpm;

import java.util.HashMap;
import java.util.List;

public interface DecisionHandler {



	public int deployProcessDefinition(HashMap<String, String> hash, String processDef) throws Exception ;
	
	public int deployProcessDefinition(HashMap<String, String> hash, String processDef, boolean enableMailing) throws Exception ;
	
	public String getProcessImg(int processId) ;
	

	public void managerTakeDecision(int processId, boolean accept, String managerComment, HashMap<String, String> hash, boolean enableMailing) throws Exception;
	
	public void hrTakeDecision(int processId, boolean accept, String hrComment, HashMap<String, String> hash, boolean enableMailing) throws Exception;
	
	public boolean isManagerDecisionReached(JbpmStatable jbpmEntity);
	
	public boolean isHrDecisionReached(JbpmStatable jbpmEntity);
	
	public List<JbpmStatable> fetchObjects4Manager(List<JbpmStatable> jbpmEntities ) throws Exception;
	
	public List<JbpmStatable> fetchObjects4Hr(List<JbpmStatable> jbpmEntities ) throws Exception;
	
	public List<JbpmStatable> fetchObjectsWithVariables(List<JbpmStatable> jbpmEntities ) throws Exception;


}
