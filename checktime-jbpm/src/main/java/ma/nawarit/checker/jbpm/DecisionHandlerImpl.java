package ma.nawarit.checker.jbpm;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.JbpmException;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;

import ma.nawarit.checker.mail.MailDto;
import ma.nawarit.checker.mail.MailManager;
import ma.nawarit.checker.mail.VelocityMerger;

public class DecisionHandlerImpl implements DecisionHandler{

	private MailManager mailManager;
	private JbpmManageable jbpmManageable;
	
	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void setJbpmManageable(JbpmManageable jbpmManageable) {
		this.jbpmManageable = jbpmManageable;
	}

	public int deployProcessDefinition(HashMap<String, String> hash, String processDef) throws Exception {
		return deployProcessDefinition(hash, processDef, false);
	}
	
	public int deployProcessDefinition(HashMap<String, String> hash, String processDef, boolean enableMailing) throws Exception {		
		int processId = jbpmManageable.createProcessDefinition(processDef+"/processdefinition.xml");
		if(enableMailing) sendMail(hash, "/mail/user_2_manager_init.vm");
		return processId;

	}
	
	private void sendMail(HashMap<String, String> hash,String template) throws Exception{
		
		StringWriter message = VelocityMerger.merge(hash, template);
		
		MailDto mailDto = new MailDto();
		mailDto.setSender(hash.get("sender"));
		mailDto.setReceiver(hash.get("receiver"));
		mailDto.setSubject(hash.get("subject"));
		mailDto.setTextMsg(message.toString());
		mailDto.setAttachment(hash.get("attachement"));
		
		mailManager.notifyUser(mailDto);
		
	}

	public String getProcessImg(int processId) {
		  try {
		   String processImg = "/img/processimageNoWorkflow.png";
		  if(processId!=0){
			  String state = jbpmManageable.getStateName(processId);
			   String managerComment = jbpmManageable.getVariableValue(processId, "manager_comment");
			   String hrComment = jbpmManageable.getVariableValue(processId,"hr_comment");
			   
				
			   if (state.equalsIgnoreCase("") || (state == null))
					processImg = "/img/img_process.png";
			   else 
				   if (state.equalsIgnoreCase("manager_decision"))
					   processImg = "/img/img_process_manager.png";
				   else 
					   if (state.equalsIgnoreCase("hr_decision"))
						   processImg = "/img/img_process_hr.png";
					   else 
						   if (state.equalsIgnoreCase("declaration_rejected"))
						   {
							   if(hrComment!=null)
								   processImg = "/img/img_process_reject_hr.png";
							   else 
								   if(managerComment!=null)
									   processImg = "/img/img_process_reject_manager.png";
								   
						   }
						   else 
							   if (state.equalsIgnoreCase("declaration_accepted"))
								   processImg = "/img/processimageAccept.png";
			 
		   }
		  return processImg;
		  } catch (Exception e) {
		   e.printStackTrace();
		   return null;
		  }

		 }
	

	public void managerTakeDecision(int processId, boolean accept, String managerComment, HashMap<String, String> hash, boolean enableMailing) {
		try {
			jbpmManageable.takeDecision(processId, true, "manager_comment", managerComment);
			if (accept){
				if(enableMailing) sendMail(hash, "/mail/manager_2_hr.vm");
			}
			else{				
				if(enableMailing) sendMail(hash, "/mail/manager_2_user.vm");
				
			}

		} catch (Exception e) {
			e.printStackTrace();
			
		}

	}
	
	public void hrTakeDecision(int processId, boolean accept, String hrComment, HashMap<String, String> hash, boolean enableMailing) {
		try {
			
			jbpmManageable.takeDecision(processId, true, "hr_comment", hrComment);	       
			if (accept){
				if(enableMailing){
					sendMail(hash, "/mail/hr_2_user_accept.vm");
				}

			}
			else{
				if(enableMailing){
					sendMail(hash, "/mail/hr_2_user_reject.vm");
					sendMail(hash, "/mail/hr_2_manager_reject.vm");
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
	public List<JbpmStatable> fetchObjects4Manager(List<JbpmStatable> jbpmEntities ) throws Exception{
		return jbpmManageable.fetchObjectsByState(jbpmEntities, "manager_decision", null);
		
	}
	public List<JbpmStatable> fetchObjects4Hr(List<JbpmStatable> jbpmEntities ) throws Exception{
		return jbpmManageable.fetchObjectsByState(jbpmEntities, "hr_decision","manager_comment");
		
	}
	
	public List<JbpmStatable> fetchObjectsWithVariables(List<JbpmStatable> jbpmEntities ) throws Exception{
		return jbpmManageable.fetchObjectsWithVariables(jbpmEntities,"manager_comment", "hr_comment");
		
	}

	public boolean isManagerDecisionReached(JbpmStatable jbpmEntity){
		return jbpmManageable.isStateReached(jbpmEntity, "manager_decision");
	}
	public boolean isHrDecisionReached(JbpmStatable jbpmEntity){
		return jbpmManageable.isStateReached(jbpmEntity, "hr_decision");
	}


}
