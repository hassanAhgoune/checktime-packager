package ma.nawarit.checker.jbpm;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jbpm.JbpmConfiguration;
import org.jbpm.JbpmContext;
import org.jbpm.JbpmException;
import org.jbpm.db.GraphSession;
import org.jbpm.graph.def.ProcessDefinition;
import org.jbpm.graph.exe.ProcessInstance;
import org.jbpm.graph.exe.Token;

public class JbpmManageableImpl implements JbpmManageable{
	private static JbpmConfiguration jbpmConfig = JbpmConfiguration.getInstance();

	
	
	public ProcessDefinition findProcessDefinition(String processDefName) throws JbpmException {
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();

		ProcessDefinition processDef = jbpmContext.getGraphSession()
				.findProcessDefinition(processDefName, 1);
		jbpmContext.close();
		return processDef;
	}
	
	public int createProcessDefinition(String processDefResource) throws JbpmException{
		ProcessDefinition processDefinition = ProcessDefinition
				.parseXmlResource(processDefResource);

		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();

		ProcessDefinition pr = jbpmContext.getGraphSession()
				.findProcessDefinition(processDefinition.getName(), 1);
		if (pr == null)
			jbpmContext.deployProcessDefinition(processDefinition);
		else
			processDefinition = pr;
		// create a process instance
		ProcessInstance processInstance = processDefinition.createProcessInstance();
		Token token = processInstance.getRootToken();
		token.signal();
		token.signal();
		jbpmContext.close();
		return (int)processInstance.getId();
	}
	
	public ProcessInstance loadProcessInstance(int processId) throws JbpmException{
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		ProcessInstance processInstance = graphSession.loadProcessInstance(processId);
		jbpmContext.close();
		return processInstance;
	}
	
	public String getStateName(int processId) throws JbpmException{
		String state = null;
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		   ProcessInstance processInstance = graphSession.loadProcessInstance(processId);
		   if(processInstance!=null){
			   Token token = processInstance.getRootToken();
			   if(token != null && token.getNode()!=null){
				   state = token.getNode().getName();
			   }
		   }
				   
		   jbpmContext.close();
		   return state;
	}
	public String getVariableValue(int processId, String variableName) throws JbpmException{
		String value = null;
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		   ProcessInstance processInstance = graphSession.loadProcessInstance(processId);
		   if(processInstance!=null){
			   value = (String)processInstance.getContextInstance().getVariable(variableName);
			  
		   }
				   
		   jbpmContext.close();
		   return value;
	}


	public boolean isStateReached(JbpmStatable jbpmEntity, String stateName) throws JbpmException{
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		if(jbpmEntity.getIdProcess() > 0){
			ProcessInstance processInstance = graphSession.loadProcessInstance(jbpmEntity.getIdProcess());
			System.out.println("isStateReached stateName " +stateName+" " + processInstance.getRootToken().getNode().getName());
			if(processInstance!=null)
				if(processInstance.getRootToken()!=null) {
					System.out.println(processInstance.getRootToken().toString());
					if(processInstance.getRootToken().getNode()!=null) {
						System.out.println(processInstance.getRootToken().getNode().toString());
						if (stateName.equals(processInstance.getRootToken().getNode().getName())){
							System.out.println(processInstance.getRootToken().getNode().getName().toString());
							jbpmContext.close();
							return true;
						}
					}
				}
					
		}
		jbpmContext.close();
		return false;

	}
	public List<JbpmStatable> fetchObjectsByState(List<JbpmStatable> jbpmEntities, String stateName, String variableName) throws JbpmException{
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		List<JbpmStatable> toReturn = new ArrayList<JbpmStatable>();
	
		ProcessInstance processInstance;
		if(jbpmEntities==null) {
			jbpmContext.close();
			return toReturn;
		}
			
		Iterator<JbpmStatable> it = jbpmEntities.iterator();
		JbpmStatable jbpmState;
		String variableValue ="";
		String nodeName;
		while(it.hasNext()){
			jbpmState = it.next();
			processInstance = graphSession.loadProcessInstance(jbpmState.getIdProcess());
			if(processInstance!=null)
				if(processInstance.getRootToken()!=null)
					if(processInstance.getRootToken().getNode()!=null){
						nodeName = processInstance.getRootToken().getNode().getName();
						if (stateName.equals(nodeName)) {
							if(variableName!=null && !"".equals(variableName)){
								variableValue = (String)processInstance.getContextInstance().getVariable(variableName);
								jbpmState.setWorkflowVariable(variableValue);
							}
							toReturn.add(jbpmState);
							
						}
					}
		}
		jbpmContext.close();
		return toReturn;
	}
	public List<JbpmStatable> fetchObjectsWithVariables(List<JbpmStatable> jbpmEntities, String variableName1, String variableName2) throws JbpmException{
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		List<JbpmStatable> toReturn = new ArrayList<JbpmStatable>();
	
		ProcessInstance processInstance;
		Iterator<JbpmStatable> it = jbpmEntities.iterator();
		JbpmStatable jbpmState;
		String variableValue ="";

		while(it.hasNext()){
			jbpmState = it.next();
			if (jbpmState.getIdProcess() <= 0) {
//				toReturn.add(jbpmState);
				continue;
			}
			processInstance = graphSession.loadProcessInstance(jbpmState.getIdProcess());
			if(processInstance!=null)
				if(processInstance.getContextInstance()!=null){
					if(variableName1!=null && !"".equals(variableName1)){
						variableValue = (String)processInstance.getContextInstance().getVariable(variableName1);								
					}
					if(variableName2!=null && !"".equals(variableName2)){
						variableValue += "#" + (String)processInstance.getContextInstance().getVariable(variableName2);						
					}
					jbpmState.setWorkflowVariable(variableValue);
					toReturn.add(jbpmState);
							
				}
					
		}
		jbpmContext.close();
		return toReturn;
	}

	public void takeDecision(int processId, boolean accept,
			String varaibleName, String variableValue) throws JbpmException{
		JbpmContext jbpmContext = jbpmConfig.createJbpmContext();
		GraphSession graphSession = jbpmContext.getGraphSession();
		ProcessInstance processInstance = graphSession.loadProcessInstance(processId);
		
		Token token = processInstance.getRootToken();
		processInstance.getContextInstance().setVariable(varaibleName,variableValue);
   
		if (accept){
			System.out.println("takeDecision1 " + processInstance.getRootToken().getNode().getName());
			token.signal("OK");
			System.out.println("takeDecision2 " + processInstance.getRootToken().getNode().getName());
			token.signal();
			System.out.println("takeDecision3 " + processInstance.getRootToken().getNode().getName());
		}
		else{
			token.signal("KO");
			token.signal();

		}
		System.out.println("takeDecision varaibleName " +varaibleName+" " + processInstance.getRootToken().getNode().getName());
		jbpmContext.close();
		
	}
	

}
