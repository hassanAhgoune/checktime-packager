package ma.nawarit.checker.mail;

import javax.mail.MessagingException;

public interface MailManager {

	public final static String MAILER_VERSION = "Java";

	public void notifyUser(MailDto mailDto)
			throws MessagingException;
	public void notifyUser(boolean debug, MailDto mailDto)
			throws MessagingException;
}
