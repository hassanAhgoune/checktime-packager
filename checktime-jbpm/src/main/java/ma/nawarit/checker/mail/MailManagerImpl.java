package ma.nawarit.checker.mail;

import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class MailManagerImpl implements MailManager {
	private String smtpServer;
	private String userName;
	private String userPwd;
	private boolean starttlsEnabled;
	private String port;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public boolean isStarttlsEnabled() {
		return starttlsEnabled;
	}

	public void setStarttlsEnabled(boolean starttlsEnabled) {
		this.starttlsEnabled = starttlsEnabled;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}


	public String getSmtpServer() {
		return smtpServer;
	}

	public void setSmtpServer(String smtpServer) {
		this.smtpServer = smtpServer;
	}

	public void notifyUser(MailDto mailDto)
			throws MessagingException {
		notifyUser(true, mailDto);
	}
	public void notifyUser(boolean debug, MailDto mailDto)
			throws MessagingException {

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", starttlsEnabled);
		props.put("mail.smtp.host", smtpServer);
		props.put("mail.smtp.port", port);
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, userPwd);
			}
		  });
		
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(mailDto.getSender()));
		String[] receivers = mailDto.getReceiver().split(";");
		InternetAddress[] internetAddresses = new InternetAddress[receivers.length];
		for(int i=0; i < receivers.length; i++)
				internetAddresses[i]=new InternetAddress(receivers[i]);
		message.setRecipients(Message.RecipientType.TO, internetAddresses);
		message.setSubject(mailDto.getSubject());
		String txtMsg = mailDto.getTextMsg();
		message.setText(txtMsg);
		message.setContent(txtMsg, "text/html");
		if (mailDto.getAttachment() != null && !"".equals(mailDto.getAttachment())) {
			Multipart multipart = new MimeMultipart();

			// creation partie principale du message
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(txtMsg);
			multipart.addBodyPart(messageBodyPart);

			// creation et ajout de la piece jointe
			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(mailDto.getAttachment());
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName("Piece1");
			multipart.addBodyPart(messageBodyPart);

			// ajout des �l�ments au mail
			message.setContent(multipart);
		}
		message.setHeader("X-Mailer", MAILER_VERSION);
		message.setSentDate(new Date());
		session.setDebug(debug);
		Transport.send(message);

	}
	public static void main(String[] args) {
		 
		final String username = "k.lamhaddab@nawar.ma";
		final String password = "!Xuvuco1000!";
 
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");
 
		Session session = Session.getInstance(props,
		  new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		  });
 
		try {
 
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("coco@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("k.lamhaddab@gmail.com"));
			message.setSubject("Testing Checktime Mail");
			message.setText("Dear Mail Crawler,"
				+ "\n\n No spam to my email, please!");
 
			Transport.send(message);
 
			System.out.println("Done");
 
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
