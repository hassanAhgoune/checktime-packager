package ma.nawarit.checker.mail;

import java.io.StringWriter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;



public class VelocityMerger {
	
	public static StringWriter merge(HashMap<String, String> hash,String templatePath) throws Exception{
		VelocityContext context = new VelocityContext();
		context = new VelocityContext();
		List<String> keys = new ArrayList(hash.keySet());
		
		for (String key : keys) {
			context.put(key,hash.get(key));
		}
		Properties p = new Properties();
		p.setProperty("resource.loader", "class");
		p.setProperty("class.resource.loader.class","org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine engine = new VelocityEngine();
        engine.init(p);
        Template template = engine.getTemplate(templatePath);
        StringWriter strWriter = new StringWriter();
        template.merge(context, strWriter);
        return strWriter;
	}
}
